<?php 

include("protected/helpers/ImageHelper.php");

// debug
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

// default size
$w = 2000;
$h = 2000;

// size from url
if (isset($_GET["w"]) && isset($_GET["h"]))
{
    $w = $_GET["w"];
    $h = $_GET["h"];
}

// max size 2000
if ($w > 2000 ) $w = 2000;
if ($h > 2000 ) $h = 2000;

// original image URL /images/folder/.../file.jpg(?h=...&w=...)
$uri = urldecode($_SERVER['REQUEST_URI']);

// fix official/small
$uri = str_replace("official/small", "official/big", $uri);

// split
$uriParts = explode("?", $uri);
$urlParts = explode("/", $uriParts[0]);
if (count($urlParts) < 2)
{
    echo "Invalid URL!";
    exit;
}

// original file path
$imgName = $urlParts[count($urlParts) - 1];
$origPath = $urlParts[1] . "/" . $urlParts[2] . "/" . $imgName;

if (!file_exists($origPath))
{
    // target file path
    $origPath = "." . $uriParts[0];
}

if (!file_exists($origPath))
{
    echo "File Not Found: " . $origPath;
    exit;
}

// target file path
if (!empty($_GET["nospace"])) {
    $imgName = str_replace(array(' ','(',')'),'_',$imgName);
    $imgName = str_replace("png", "jpg", $imgName);
    $imgName = str_replace("gif", "jpg", $imgName);
    $imgName = str_replace("PNG", "jpg", $imgName);
    $imgName = str_replace("JPG", "jpg", $imgName);
    $imgName = str_replace("GIF", "jpg", $imgName);
    $imgName = str_replace("jpeg", "jpg", $imgName);
    $imgName = str_replace("JPEG", "jpg", $imgName);
} else {
    $imgName = str_replace("jpg", "png", $imgName);
    $imgName = str_replace("gif", "png", $imgName);
    $imgName = str_replace("JPG", "png", $imgName);
    $imgName = str_replace("GIF", "png", $imgName);
    $imgName = str_replace("jpeg", "png", $imgName);
    $imgName = str_replace("JPEG", "png", $imgName);
}
$filename = "img/cache/" . $w . "x" . $h . "/" . $imgName;

// create image if not exists
if (!file_exists($filename))
{
    if (!file_exists(dirname($filename)))
    {
        // create subfolder if not exists
        mkdir(dirname($filename));
    }

    $image = ImageHelper::imageCreateFromSrc($origPath);

    $width = imagesx($image);
    $height = imagesy($image);

    $logic = 1;
    if (!empty($_GET["logic"])) {
        $logic = $_GET["logic"];
    }
  
    if ($logic == 1)
    {
        /*if ($width < $w)
        {
            $aspect = $h / $w;
            $w = $width;
            $h = $width * $aspect;
        }

        if ($height < $h)
        {
            $aspect = $w / $h;
            $h = $height;
            $w = $height * $aspect;
        }*/

        $original_aspect = $width / $height;
        $thumb_aspect = $w / $h;

        if ($original_aspect >= $thumb_aspect )
        {
            // If image is wider than thumbnail (in aspect ratio sense)
            $new_height = $h;
            $new_width = $width / ($height / $h);
        }
        else
        {
            // If the thumbnail is wider than the image
            $new_width = $w;
            $new_height = $height / ($width / $w);
        }

       
        if ('profile' == $urlParts[count($urlParts) - 2]) {
            $thumb = imagecreatetruecolor($width, $height);
            $white = imagecolorallocate($thumb, 255, 255, 255);
            imagefill($thumb, 0, 0, $white);
            
            imagecopyresampled($thumb, $image,
                0, 0, 0, 0,
                $width, $height, $width, $height);
            imagepng($thumb, $filename, 1, PNG_ALL_FILTERS);
        } else {
            $thumb = imagecreatetruecolor($w, $h);
            $white = imagecolorallocate($thumb, 255, 255, 255);
            imagefill($thumb, 0, 0, $white);

            // Resize and crop
            imagecopyresampled($thumb, $image,
                0 - ($new_width - $w) / 2, // Center the image horizontally
                0 - ($new_height - $h) / 2, // Center the image vertically
                0, 0,
                $new_width, $new_height, $width, $height);
            if (!empty($_GET["nospace"])) {
                imagejpeg($thumb, $filename, 100);
            } else {
                imagepng($thumb, $filename, 1, PNG_ALL_FILTERS);
            }
        }
    }
    
    if ($logic == 2)
    {
        $new_width = $width;
        $new_height = $height;
        
        if ($new_width > $w)
        {            
            $new_height = $w * $new_height / $new_width;
            $new_width = $w;
        }
        
        if ($new_height > $h)
        {        	
        	$new_width = $h * $new_width / $new_height;
        	$new_height = $h;
        }    
        
        $thumb = imagecreatetruecolor($w, $h);        
        $white = imagecolorallocate($thumb, 255, 255, 255);
        imagefilledrectangle($thumb, 0, 0, $w, $h, $white);
        
        // Make the background transparent
        //imagecolortransparent($thumb, $white);
        
        // Resize and crop
        imagecopyresampled($thumb, $image,
            ($w - $new_width) / 2, // Center the image horizontally
            ($h - $new_height) / 2, // Center the image vertically 
            0, 0,
            $new_width, $new_height, $width, $height);
        
        if (!empty($_GET["nospace"])) {
            imagejpeg($thumb, $filename, 100);
        } else {
            imagepng($thumb, $filename, 1, PNG_NO_FILTER);
        }
    }
}

http_redirect("/" . $filename, array(), true, HTTP_REDIRECT_PERM);
?>