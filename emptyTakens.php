<?php
    // DB Connection
    $config = include(dirname(__FILE__) . '/protected/config/amazon.php');
    $dbSettings = $config['components']['db'];
    $database = new PDO($dbSettings['connectionString'], $dbSettings['username'], $dbSettings['password']);
    
    // Get List Users
    $users = $database->query('SELECT id, user_id, created FROM tbl_list')->fetchAll(PDO::FETCH_ASSOC);
    if (!empty($users)) {
        $emptyTakens = array();
        foreach ($users as $user) {
            // Find empty takens
            $issetTaken = $database->query('SELECT id FROM tbl_listtaken WHERE user_id = '.$user['user_id'])->fetch(PDO::FETCH_ASSOC);
            if (empty($issetTaken)) {
                $emptyTakens[] = $user['user_id'];
            }
        }
        if (sizeof($emptyTakens) > 0) {
            // Print empty takens;
            echo '<h2>Users were not found record in tbl_listtaken: </h2>';
            foreach ($emptyTakens as $taken) {
                echo "User ID: ".$taken."<br />";
            }
            echo "<br><hr><br>";
            
            // Generate SQL;
            echo '<h2>Please run this SQL to fix it: </h2>';
            foreach ($users as $user) {
                if (in_array($user['user_id'], $emptyTakens)) {
                    echo "INSERT INTO `tbl_listtaken` (
                        `id` ,
                        `user_id` ,
                        `list_id` ,
                        `favorite` ,
                        `shortlist` ,
                        `created` ,
                        `modified`
                        )
                        VALUES (
                        NULL , '".$user['user_id']."', '".$user['id']."', '0', '', '".$user['created']."', '".$user['created']."'
                    );<br>";
                }
            }
        } else {
            echo '<h2>Empty records in table tbl_listtaken not found</h2>';
        }
    } else {
        echo '<h2>Table tbl_list is empty</h2>';
    }
?>