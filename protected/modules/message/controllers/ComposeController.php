<?php

class ComposeController extends Controller
{

	public $defaultAction = 'compose';

	public function actionCompose($id = null) {            
            $userid = Yii::app()->user->getId();
            $user = User::model()->find("id=$userid");
            $name = $user['firstname'].' '.$user['lastname'];
            $reply = null;
            $this->layout = 'layout/authrized_user';
		$message = new Message();               
		if (Yii::app()->request->getPost('Message')) { ///echo "<pre>";print_r($_POST); exit;
			$receiverName = Yii::app()->request->getPost('receiver');
		        $message->attributes = Yii::app()->request->getPost('Message');  
			$message->sender_id = Yii::app()->user->getId();
                        if(isset($_POST['browsattachment']))
                        {
                          $message->attachment = $_POST['browsattachment'];
                        }
                        $result = null;
                         
                        if($_FILES['browse']['tmp_name'] != '')
                        {   
                               $size = $_FILES['browse']['size'];
                                 if($size <= 1048576)
                                  { 
                                       $file= CUploadedFile::getInstanceByName('browse');

                                        if(is_object($file) && get_class($file)== 'CUploadedFile') 
                                        {
                                            $message->attachment  = time().$file->name; 
                                            $result =$message->save(false);
                                        }
                                  }
                                  else
                                  {  
                                      Yii::app()->user->setFlash('onregistererror', "Incorrect Attachment Size.");
                                      $this->render(Yii::app()->getModule('message')->viewPath . '/compose', array('model' => $message,'reply' => $reply,'name' => $name, 'receiverName' => isset($receiverName) ? $receiverName : null));
                                      exit;
                                  }
                        }
                        if($result)
                        {
                            $file->saveAs('images/attachment'.'/'.$message->attachment);
                            Yii::app()->user->setFlash('messageModule', MessageModule::t('Message has been sent'));
			    $this->redirect($this->createUrl('inbox/'));
                        }                        
		       elseif ($message->save()) {
				Yii::app()->user->setFlash('messageModule', MessageModule::t('Message has been sent'));
			    $this->redirect($this->createUrl('inbox/'));
			} else if ($message->hasErrors('receiver_id')) {
				$message->receiver_id = null;
				$receiverName = '';
			}
		}                
                
                
                else {
                    
			/*if ($id) { 
				$receiver = call_user_func(array(call_user_func(array(Yii::app()->getModule('message')->userModel, 'model')), 'findByPk'), $id);
				if ($receiver) {
					$receiverName = call_user_func(array($receiver, Yii::app()->getModule('message')->getNameMethod));
					$message->receiver_id = $receiver->id;
				}
			}*/
		}
                
                $friend = Friend::model()->findAll("(user_id=$userid AND status = 'accept') OR (friend_id=$userid AND status = 'accept')");
                
                if($id)
                {
                    $reply = Message::model()->find("id=$id");                     
                }
		$this->render(Yii::app()->getModule('message')->viewPath . '/compose', array('model' => $message,'reply' => $reply,'friend' => $friend,'name' => $name, 'receiverName' => isset($receiverName) ? $receiverName : null));
	}
}
