<?php

class TrashController extends Controller
{
	public $defaultAction = 'trash';

	public function actiontrash() {
                $this->layout = 'layout/authrized_user';
                $messagesAdapter = Trash::getAdapterForTrash(Yii::app()->user->getId());
                
		$pager = new CPagination($messagesAdapter->totalItemCount);
		$pager->pageSize = 10;
                
		$messagesAdapter->setPagination($pager);
                
		$this->render(Yii::app()->getModule('message')->viewPath . '/trash', array(
			'messagesAdapter' => $messagesAdapter
		));
	}
}
