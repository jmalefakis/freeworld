<?php

class DeleteController extends Controller {

	public $defaultAction = 'delete';

	public function actionDelete($id = null) { 
            $this->layout = 'authrized';
            $uid = Yii::app()->user->getId();
		if (!$id) { 
			$messagesData = Yii::app()->request->getParam('Message');
			$counter = 0;
			if ($messagesData) { 
				foreach ($messagesData as $messageData) { 
					if (isset($messageData['selected'])) { 
                                            $trash = new Trash();                                            
						$message = Message::model()->findByPk($messageData['id']);
                                                
                                                $trash->message_id  = $message['id'];
                                                $trash->receiver_id = $message['receiver_id'];
                                                $trash->sender_id   = $message['sender_id'];
                                                $trash->subject     = $message['subject'];
                                                $trash->body        = $message['body'];
                                                $trash->created_at  = DateHelper::getCurrentSQLDateTime();
                                                $trash->modified    = DateHelper::getCurrentSQLDateTime();
                                                $trash->save(false);
                                                
						if ($message->deleteByUser(Yii::app()->user->getId())) {
							$counter++;
						}
					}
				}
			}
			if ($counter) {
				Yii::app()->user->setFlash('messageModule', MessageModule::t('{count} message'.($counter > 1 ? 's' : '').' has been deleted', array('{count}' => $counter)));
			}
			$this->redirect(Yii::app()->request->getUrlReferrer());
		} else { 
			$message = Message::model()->findByPk($id);

			if (!$message) {
				throw new CHttpException(404, MessageModule::t('Message not found'));
			}

			$folder = $message->receiver_id == Yii::app()->user->getId() ? 'inbox/' : 'sent/';

			if ($message->deleteByUser(Yii::app()->user->getId())) {
				Yii::app()->user->setFlash('messageModule', MessageModule::t('Message has been deleted'));
			}
			$this->redirect($this->createUrl($folder));
		}
	}
}
