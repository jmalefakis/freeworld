<?php
class SeeController extends Controller {
    public $defaultAction = 'See';
    public function actionSee($id=null) { 
      $baseurl  = Yii::app()->params['workingdir'];     
      $message=  Message::model()->find("id ='$id'");
      //echo getcwd() ."<br>";
       $file =  $baseurl .'/images/attachment/'.$message['attachment'];
      
        if (file_exists($file)) 
         {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
        else
        {
           echo "The file $file does not exist";
        }
                
 }
               
}