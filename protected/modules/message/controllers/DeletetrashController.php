<?php

class DeletetrashController extends Controller { 

	public $defaultAction = 'deletetrash';
        
        public function actiondeletetrash($id = null) { 
            $this->layout = 'authrized';
          
		if (!$id) { 
			$messagesData = Yii::app()->request->getParam('Trash');                        
			$counter = 0;
			if ($messagesData) { 
				foreach ($messagesData as $messageData) { 
					if (isset($messageData['selected'])) { 
                                            $messagesave = new Message();
						$message = Trash::model()->findByPk($messageData['id']); 
                                                
                                                if(isset($_POST['hideen']) && ($_POST['hideen']!='')){
                                                $messagesave->receiver_id = $message['receiver_id'];
                                                $messagesave->sender_id   = $message['sender_id'];
                                                $messagesave->subject     = $message['subject'];
                                                $messagesave->body        = $message['body'];
                                                $messagesave->is_read     = 0;
                                                $messagesave->created_at  = DateHelper::getCurrentSQLDateTime();
                                                $messagesave->deleted_by  = '';
                                                $messagesave->save(false);
                                                }
                                                
						if ($message->deleteByUser(Yii::app()->user->getId())) {
							$counter++;
						}
					}
				}
			}
			if ($counter) {
				Yii::app()->user->setFlash('messageModule', MessageModule::t('{count} message'.($counter > 1 ? 's' : '').' has been deleted', array('{count}' => $counter)));
			}
			$this->redirect(Yii::app()->request->getUrlReferrer());
		} else {
			$message = Trash::model()->findByPk($id);

			if (!$message) {
				throw new CHttpException(404, MessageModule::t('Message not found'));
			}

			$folder = $message->receiver_id == Yii::app()->user->getId() ? 'inbox/' : 'sent/';

			if ($message->deleteByUser(Yii::app()->user->getId())) {
				Yii::app()->user->setFlash('messageModule', MessageModule::t('Message has been deleted'));
			}
			$this->redirect($this->createUrl($folder));
		}
	}
        
        
}
