<?php

class SeemessageController extends Controller {

	//public $defaultAction = 'seemessage';
        
        public function actionSeemessage($sender_id=null) { 
                $this->layout = 'layout/authrized_user';
                $userid = Yii::app()->user->getId();
                
                $messagecount = Message::model()->findAll("receiver_id = '$userid' AND sender_id = $sender_id AND is_read = '0'"); 
                $messagecou = count($messagecount);
                for($i=0;$i<$messagecou;$i++)
                {
                    $messagecount[$i]->is_read = 1;
                    $messagecount[$i]->save(false);
                }
                
                $sender_id = (int)Yii::app()->request->getParam('sender_id'); 
		$viewedMessage = Message::model()->findAll(array('order'=>'id','condition'=>'(sender_id=:x AND receiver_id=:y) OR (sender_id=:y AND receiver_id=:x)', 
                                                                                             'params'=>array(':x'=>$sender_id,':y'=>$userid)));
		if (!$viewedMessage) {
			 //throw new CHttpException(404, MessageModule::t('Message not found'));
                         $this->redirect($this->createUrl('inbox/'));
		}
                $message = new Message();	
                
                if (Yii::app()->request->getPost('Message')) {
			$message->attributes  = Yii::app()->request->getPost('Message');
                        $message->subject     = $_POST['Message']['body'];
                        $message->receiver_id = $sender_id;                        
			$message->sender_id   = $userid;
                        $result = null;
                       
                        if($_FILES['browse']['tmp_name'] != '')
                        {
                            $size = $_FILES['browse']['size'];
                            
                                 if($size <= 1048576)
                                  {  
                                   $file= CUploadedFile::getInstanceByName('browse');

                                    if(is_object($file) && get_class($file)== 'CUploadedFile') 
                                    {
                                        $message->attachment  = time().$file->name; 
                                        $result =$message->save(false);
                                    }
                                  }
                                  else
                                  {   
                                      Yii::app()->user->setFlash('onregistererror', MessageModule::t('Incorrect Attachment Size.'));                                                   
                                      $this->render(Yii::app()->getModule('message')->viewPath . '/seemessage', array('viewedMessage' => $viewedMessage, 'message' => $message));
                                      exit;
                                  }
                        }
                        
                        if($result)
                        {
                            $file->saveAs('images/attachment'.'/'.$message->attachment);                            
			    $this->redirect(array('seemessage','sender_id'=>$sender_id));
                        } 
                        
			elseif ($message->save()) {
				$this->redirect(array('seemessage','sender_id'=>$sender_id));	    
			}
		}
		$this->render(Yii::app()->getModule('message')->viewPath . '/seemessage', array('viewedMessage' => $viewedMessage, 'message' => $message));
	}
        
        public function actionindex($sender_id=null) { 
            
            $this->redirect(array('seemessage','sender_id'=>$sender_id));
        }
        
        
        public function actiondeletereply() 
        { 
            //echo "<pre>";print_r($_POST);exit;
            $sender_id = $_POST['sender_id'];
            if(isset($_POST['checkbox']) && $_POST['checkbox']!='')
            {
                $count = count($_POST['checkbox']);
                for($i=0;$i<$count;$i++)
                {
                    $messageid  = $_POST['checkbox'][$i];
                    $message    = Message::model()->find("id = '$messageid'");
                    $message->delete();
                }
                
                Yii::app()->user->setFlash('onregistererror', MessageModule::t("$count Message deleted."));
            }
            $this->redirect(array('seemessage','sender_id'=>$sender_id));
        }
        
}
