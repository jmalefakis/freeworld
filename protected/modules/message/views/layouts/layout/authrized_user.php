<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->	
        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/innerpage.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
         <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/wayoworld.css" />
         <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker/jquery.datepick.css" />
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
       
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/favicon.ico"/>  
        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script>!window.jQuery && document.write(unescape('%3Cscript src="jquery/jquery-1.7.2.min.js"%3E%3C/script%3E'))</script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script>!window.jQuery.ui && document.write(unescape('%3Cscript src="jquery/jquery-ui-1.8.21.custom.min.js"%3E%3C/script%3E'))</script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mousewheel.min.js"></script>
	<!-- custom scrollbars plugin -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepicker/jquery.datepick.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/validate.js"></script>
        
        <style type="text/css">
        textarea:focus, input:focus{ outline: none;}
        </style>
        
        <script>
		(function($){
			$(window).load(function(){
				$("#content_1").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
                
                (function($){
			$(window).load(function(){
				$("#content_2").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
                
                (function($){
			$(window).load(function(){
				$("#content_4").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
               
                
                
</script>
        
        
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="body_gestuser">

<div class="container" id="page">         
            
                <?php include 'authrized_user_header.php';  ?>
            
            <div class="min_hight_outer">
                <?php echo $content; ?>
            

                <div class="footerdiv_auth_uswer">
                        <?php include 'authrized_user_footer.php';  ?>
                </div><!-- footer -->
           </div>
</div><!-- page -->

</body>
</html>