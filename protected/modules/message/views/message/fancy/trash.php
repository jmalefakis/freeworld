
<body>
<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Messages:trash"); ?>
<?php
	$this->breadcrumbs=array(
		MessageModule::t("Messages"),
		MessageModule::t("trash"),
	);
?>


<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_styles') ?>
<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/sidenavigation') ?>    
    <div class="featured_list_section_internal">
    <div class="outerdiv1_auth">
       
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_flash') ?>
	<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>
	<div class="content_inbox">
		<h2 style="color: #666666;"><?php echo MessageModule::t('Trash'); ?></h2>
                
		<?php if ($messagesAdapter->data){ ?>
                 <?php $baseurl = Yii::app()->request->baseUrl; ?>
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'message-delete-form',
				'enableAjaxValidation'=>false,
				'action'=>$baseurl.'/index.php/message/deletetrash',
			)); ?>

			<table class="bordered-table zebra-striped">
				<tr>
					<th class="from-to">From</th>
					<th>Subject</th>
					<th>Date</th>
				</tr>
				<?php foreach ($messagesAdapter->data as $index => $message): ?>
					<tr class="<?php //echo $message->is_read ? 'read' : 'unread' ?>">
						<td>
							<?php echo CHtml::checkBox("Trash[$index][selected]"); ?>
							<?php echo $form->hiddenField($message,"[$index]id"); ?>
							<?php //echo $message->getSenderName(); ?>
						</td>
						<td><a href="<?php echo $this->createUrl('viewtrash/', array('message_id' => $message->id)) ?>"><?php echo $message->subject ?></a></td>
						<td><span class="date"><?php echo $message->created_at; //date(Yii::app()->getModule('message')->dateFormat, strtotime($message->created_at)) ?></span></td>
					</tr>
				<?php endforeach  ?>                                 
			</table>

			<div style="float: left; width: 76%;">
				<button class="delete_select_mess"><?php echo MessageModule::t("Delete Selected") ?></button>
			</div> 
                
                
                        <div style="float: left; width: 19%;">
                            <a href="">
                               <input type="submit" value="Restore Selected" name="restore" style=" background:none repeat scroll 0 0 #F6F175;font-family: Geosanslight;border: none ;font-size: 22px;height: 40px;width: 150px;border-radius: 7px 7px 7px 7px; color: #403F34;" onclick="javascript: document.getElementById('hid').value = 'a'; "/>
                            </a>   
                            <input type="hidden" id="hid" name="hideen" value=""/>
			</div>

		<?php $this->endWidget(); ?>
		<div class="pagination">
			<?php $this->widget('CLinkPager', array('header' => '', 'pages' => $messagesAdapter->getPagination(), 'htmlOptions' => array('class' => 'pager'))) ?>
		</div>
		<?php } else { ?>
                    
                <div style="padding-top: 50px; font-size: 25px; padding-left: 35px; color: Grey;">
                    <span>
                        No Messages
                    </span>
                </div>
                <?php }?>
                
              </div> 
        
        </div>
        </div>
</body>