<script type="text/javascript">
function autoresize(textarea) {        
        var height = $("#compo").height();
        
        if(textarea.scrollHeight>height)
         {  
             document.getElementById("compo").style.height = height+40+'px' ;
         }        
        textarea.style.height = '61px';
        textarea.style.height = textarea.scrollHeight + 12 + 'px';
    }


</script>
<body>
<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Compose Message"); ?>
<?php
	$this->breadcrumbs=array(
		MessageModule::t("Messages"),
		MessageModule::t("Compose"),
	);
?>

<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_styles') ?>
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/sidenavigation') ?>          
    <div class="outerdiv1_auth">
 <div class="message"><span>Messages</span></div>
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_flash') ?>        
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation'); ?>
       
	<div class="content_inbox">
		

		<div class="form">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'message-form',
				'enableAjaxValidation'=>false,
			)); ?>

                   
			<?php echo $form->errorSummary($model, null, null, array('class' => 'alert-message block-message error')); ?>
                    <div class="compose"><span>
			<?php echo $form->labelEx($model,'receiver_id'); ?></span>
			<div class="compose_div1">
				<?php echo CHtml::textField('receiver', $receiverName,$htmlOptions=array('style'=>'height:10px')); ?>
				<?php echo $form->hiddenField($model,'receiver_id'); ?>
				<?php echo $form->error($model,'receiver_id'); ?>
			</div>
                       <div class="compose_div4"> <label><span>From:</span></label>
                            <input type="text" class="from" name="from" id="from">
			</div>
                       
			<?php echo $form->labelEx($model,'subject'); ?>
			<div class="compose_div2">
				<?php echo $form->textField($model,'subject',$htmlOptions=array('style'=>'height:10px')); ?>
				<?php echo $form->error($model,'subject'); ?> 
			</div>
                    </div>
                   
			<div class="compose_div3" id="compo">
                            <?php echo $form->labelEx($model,'body'); ?>
				<?php echo $form->textArea($model,'body',$htmlOptions=array('onkeyup'=>'autoresize(this)')); ?>
				<?php echo $form->error($model,'body'); ?>
                                   <div id="fileupload" >
                                       <div class="myfileupload-buttonbar "><span id ="attach">Attach a file:</span>
                                            <label class="myui-button">

                                              <span id ="file">Browse..</span> <input type="file" name="browse" id="browse">
                                            </label>
                                        </div>
                                   </div>
                                   </div>
			<div class="floatleft">
				<button class="delete_select_mess"><?php echo MessageModule::t("Send Message") ?></button>
			</div>

			<?php $this->endWidget(); ?>
                        </div>
		
	</div>
       
       </div>
    

<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_suggest'); ?>
</body>
<html>
