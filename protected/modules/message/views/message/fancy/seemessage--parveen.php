
<body>
<?php $this->pageTitle=Yii::app()->name . ' - ' . MessageModule::t("Compose Message");
      $baseurl  = Yii::app()->request->baseUrl;      
?>

<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_styles') ?>
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/sidenavigation') ?>          
    <div class="outerdiv1_auth">
         
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_flash') ?>
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>
     
	<div class="content_inbox">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'message-delete-form',
			'enableAjaxValidation'=>false,
			'action' => $this->createUrl('delete/', array('id' => 1))//$viewedMessage->id))
		)); ?>
		<!--<button class="delete_select_mess"><?php //echo MessageModule::t("Delete") ?></button>-->
		<?php $this->endWidget(); ?><div class="send"><span id="just">Subject just checking in</span></div><div class="check">
                <?php foreach ($viewedMessage as $value) {
                      $user = User::model()->find("id = '$value->sender_id'");
                ?>
             
                <div class="seemessage_div1">
                        <div class="seemessage_div2">
                            <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/user/userprofile?userid=<?php echo $user->id;?>">
                               <?php if($user->image!=''){?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/profile/<?php echo $user->image;?>"/>
                               <?php } else { 
                                  if($user->gender=='male')
                                  {
                                ?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($user->gender=='female') {?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                           </a>                            
                        </div>
                        <div class="seemessage_div3">
                            <div class="seemessage_div31">
                                <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/user/userprofile?userid=<?php echo $user->id;?>">
                                    <span><?php echo $user->firstname.' '.$user->lastname;?></span>
                                </a>
                            </div>
                            <div class="seemessage_div32">
                                <span><?php echo $value->body?></span> <div class="date_show"><span><?php echo $value->created_at?></span></div>
                            </div>

                        </div>
                </div>
                <?php }?>

		

		<div class="form">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'message-form',
				'enableAjaxValidation'=>false,
			)); ?>

			<?php echo $form->errorSummary($message, null, null, array('class' => 'alert-message block-message error')); ?>

                    <div class="left">
			<div class="compose_div3"><span id ="reply_msg">Reply-</span>
				<?php echo $form->textArea($message,'body'); ?>
				<?php echo $form->error($message,'body'); ?>
			</div>
                    </div></div> 
                <div class="myfi "><span id ="attach">Attach a file:</span>
            <label class="myui-btn"><input type="file" name="browse" id="browse">
            </label>
        </div>
			<div class="view_mesaage">
				<button class="delete_select_mess"><?php echo MessageModule::t("Send") ?></button>
			</div>

			<?php $this->endWidget(); ?>
		</div>
                
                
</div>
        </div>
</body>