<script>
		(function($){
			$(window).load(function(){
				$("#content_1").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
	</script>

<body >
<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Messages:inbox"); ?>
<?php
	$this->breadcrumbs=array(
		MessageModule::t("Messages"),
		MessageModule::t("Inbox"),
	);
?>


<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_styles') ?>
<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/sidenavigation') ?>  
    <div class="featured_list_section_internal">
    <div class="outerdiv1_auth">
       
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_flash') ?>
        
        <h2 class="message_h2inbox">Messages</h2>
	<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>
	<div class="content_inbox">
		

		<?php if ($messagesAdapter->data){ ?>
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'message-delete-form',
				'enableAjaxValidation'=>false,
				'action' => $this->createUrl('delete/')
			)); ?>
                        
            
			
                            
				<div>
					<div class="dromdiv">From:</div>
					<div class="dromdiv" style="width: 219px;">Subject:</div>
					<div class="dromdiv" style="width: 238px;">Date:</div>
				</div>
                                <div class="div_table_overflow" id="content_1">
                                <table id="inboxtable" class="bordered-table zebra-striped">
				<?php foreach ($messagesAdapter->data as $index => $message): ?>                                 
                               
					<tr class="<?php echo $message->is_read ? 'read' : 'unread' ?>">
						<td class="messagecontent_color" style="width: 185px;">
							<?php echo CHtml::checkBox("Message[$index][selected]"); ?>
							<?php echo $form->hiddenField($message,"[$index]id"); ?>
							<?php echo $message->getSenderName(); ?>
						</td>
						<td class="messagecontent_color"><a style="color: #256C89;" class="messagecontent_color"  href="<?php echo $this->createUrl('seemessage/', array('sender_id' => $message->sender_id)) ?>"><?php echo $message->subject ?></a></td>
						<?php $old_date = $message->created_at;
                                                      $new_date = date('Y-m-d', strtotime($old_date));?>
                                                <td class="messagecontent_color"><span class="date"><?php echo $new_date; ?></span></td>
					 </tr>
                                      
				<?php endforeach ?>
                                               
			      </table>
                        </div>
			<div class="delete_selected_inbox">
				<button onclick="return deletemessage();" class="delete_select_mess"><?php echo MessageModule::t("Delete Selected") ?></button>
			</div>

		<?php $this->endWidget(); ?>
		<div class="pagination">
			<?php $this->widget('CLinkPager', array('header' => '', 'pages' => $messagesAdapter->getPagination(), 'htmlOptions' => array('class' => 'pager'))) ?>
		</div>
		<?php } else { ?>
                    
                <div style="padding-top: 50px; font-size: 25px; padding-left: 35px; color: Grey;">
                    <span>
                        No Messages
                    </span>
                </div>
                <?php }?>
	</div>
        
        </div>
   
    
</div>
    
    
</body>