<script>
		/*(function($){
			$(window).load(function(){
				$("#content_1").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);*/
</script>
<body>
<?php $this->pageTitle=Yii::app()->name . ' - ' . MessageModule::t("Compose Message");
      $baseurl  = Yii::app()->request->baseUrl; 
      $userid   = Yii::app()->user->getId();
?>
   
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_styles') ?>
<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/sidenavigation') ?>     
    <div class="featured_list_section_internal">
    <div class="outerdiv1_auth">     
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_flash') ?>
        <h2 class="message_h2inbox">Messages</h2>
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ;
                $count = count($viewedMessage);
                $count = $count-1;
         ?>
        
	<div class="content_inbox">
            
            <div class="seemessage_div_sub">
                <div class="seemessage_div_sub_spn">
                    <?php $str =  $viewedMessage["$count"]['subject'];
                          $tempstr = substr($str, 0, 20);                          
                    ?>
                    <span>Subject: <?php echo $tempstr;?></span>
                </div>
                <div class="seemessage_div_delete" >
                    <span onclick="delete_reply()" class="delete_span">Delete</span>&nbsp;&nbsp;&nbsp;
                    <a id="anchor" style="color:white;" href="<?php echo $baseurl;?>/index.php/message/compose?id=">
                    <span onclick="return forword_message('<?php echo $baseurl;?>')" class="delete_span">Forward</span>
                    </a>
                </div>
            </div>
              
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id'=>'message-delete-form',
			'enableAjaxValidation'=>false,
			'action'=>$baseurl.'/index.php/message/seemessage/deletereply',
		)); ?>
		<!--<button class="delete_select_mess"><?php //echo MessageModule::t("Delete") ?></button>-->
		
            <input type="hidden" name="sender_id" value="<?php echo $_GET['sender_id'];?>"/>
            <div class="seemessage_overflow">
                <div class="seemessage_overflow_divmain" id="content_1">
                <?php foreach ($viewedMessage as $value) {
                      $user = User::model()->find("id = '$value->sender_id'");
                ?>
                
                <div class="seemessage_div1">
                    <div class="seemember_checkbox">
                        <input id="checkbox<?php echo $value->id?>" type="checkbox" name="checkbox[]" value="<?php echo $value->id?>"/>
                    </div>
                        <div class="seemessage_div2">
                            
                            <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/user/userprofile?userid=<?php echo $user->id;?>">
                               <?php if($user->image!=''){?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/header/<?php echo $user->image;?>"/>
                               <?php } else { 
                                  if($user->gender=='male' || $user['gender']=='Male')
                                  {
                                ?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($user->gender=='female' || $user['gender']=='Female') {?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="userimage" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                           </a>                            
                        </div>
                        <div class="seemessage_div3">
                            <div class="seemessage_div31">
                                <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/user/userprofile?userid=<?php echo $user->id;?>">
                                    <span><?php echo $user->firstname.' '.$user->lastname;?></span>
                                </a>
                            </div>
                           
                            <div class="attachment_show"> 
                                 <?php if($value->attachment!=''){?>
                                 <?php if($value->sender_id != $userid) { ?>
                                <a href="<?php echo $baseurl;?>/index.php/message/see/see?id=<?php echo $value->id?>" style="font-size: 15px;">
                                    <img src="<?php echo $baseurl; ?>/images/freeworld/attach-icon.png"/>
                                </a>
                                <?php } else {?>
                                    <img src="<?php echo $baseurl; ?>/images/freeworld/attach-icon.png"/>
                                  
                                    <?php } }?>
                            </div>                         
                            
                            <div class="seemessage_div32">
                                <?php if($value->body!=''){?>
                                <span><?php echo $value->body?></span>
                                <?php } else{?>
                                &nbsp;
                                <?php }?>
                            </div>
                            <div class="seemessage_div33">
                                
                                <?php $old_date = $value->created_at;
                                      $new_date = date('Y-m-d', strtotime($old_date));?>
                                <span><?php echo $new_date?></span>
                            </div>
                        </div>
                </div>
                <?php }?>
                </div>
                <?php $this->endWidget(); ?>
		

		<div class="reply_messahe_see">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'message-form',
				'enableAjaxValidation'=>false,
                                'htmlOptions'=>array(                      
                                'enctype'=> 'multipart/form-data',
                                'onsubmit'=>'return validate_replymsg();',
                                'name'    =>'compose'
                                ),
			)); ?>
                    
                    <div class="myfi "><span id ="attach">Attach a file:</span>
                        <label class="myui-btn" style="width: 110px;"><input type="file" name="browse" id="browse">
                        </label>
                    </div>

			<?php echo $form->errorSummary($message, null, null, array('class' => 'alert-message block-message error')); ?>

			<div class="seemessage_reply"><?php echo MessageModule::t('Reply-') ?></div>
			<div class="floatleft" style="padding-right: 15px;">
				<?php echo $form->textArea($message,'body',$htmlOptions=array('style'=>'height:63px;width:398px;resize: none;')); ?>
				<?php echo $form->error($message,'body'); ?>
			</div>

			<div class="seemessage_reply_mesaage">
				<button class="delete_select_mess"><?php echo MessageModule::t("Reply") ?></button>
			</div>

			<?php $this->endWidget(); ?>
		</div>
                </div>
                
</div>
        
        <!--<div class="pagination_seemsg">
            <div class="pagination_seemsg_previous">
                <span>
                    << Previous
                </span>
            </div>
            <div class="pagination_seemsg_next">
                <span>
                    Next>>
                </span>
            </div>
        </div>-->
        
        </div>
        
        </div>
</body>