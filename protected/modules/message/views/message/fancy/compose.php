<script type="text/javascript">
function autoresize(textarea) {        
        var height = $("#compo").height();
        
        if(textarea.scrollHeight>height)
         {  
             document.getElementById("compo").style.height = height+40+'px' ;
             document.getElementById("compo_main").style.height = height+200+'px' ;
         }        
        textarea.style.height = '150px';
        textarea.style.height = textarea.scrollHeight + 12 + 'px';
    }


</script>
<body>
<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Compose Message"); ?>
<?php
	$this->breadcrumbs=array(
		MessageModule::t("Messages"),
		MessageModule::t("Compose"),
	);
        
        $baseurl  = Yii::app()->request->baseUrl; 
        $userid  = Yii::app()->user->getID();
?>

<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_styles') ?>
<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/sidenavigation') ?>     
    <div class="featured_list_section_internal">
    <div class="outerdiv1_auth">
       
        <?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="flash' . $key . '">' . $message . "</div>\n";
            }
        ?>
        
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_flash') ?>  
         <h2 class="message_h2inbox">Messages</h2>
        <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation'); ?>
	<div class="content_inbox">	

		<div class="form">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'message-form',
				'enableAjaxValidation'=>false,
                                'htmlOptions'=>array(                      
                                'enctype'=> 'multipart/form-data',
                                'onsubmit'=>'return validate_composemsg();',
                                'name'    =>'compose'
                                ),
                            
			)); ?>

			

			<?php echo $form->errorSummary($model, null, null, array('class' => 'alert-message block-message error')); ?>
                    <div class="bacground_compose">
			<?php echo $form->labelEx($model,'receiver_id',$htmlOptions=array('class'=>'recevier_compose')); ?>
			<div class="compose_div1">
				<?php echo CHtml::textField('receiver', $receiverName,$htmlOptions=array('style'=>'height:19px;width:200px;')); ?>
				<?php echo $form->hiddenField($model,'receiver_id'); ?>
				<?php echo $form->error($model,'receiver_id'); ?>
                                <input type="hidden" id="hiddenbaseurl" value="<?php echo $baseurl; ?>" />
			</div>
                        
                        <div class="from_div">
                            <div class="from_span">
                                <span>From:</span>
                            </div>
                            <div class="from_input_type">
                                <input type="text" name="from" value="<?php echo $name;?>" class="compose_input" readonly="readonly"/>
                            </div>
                        </div>

			<?php echo $form->labelEx($model,'subject',$htmlOptions=array('class'=>'subject_compose','style'=>'width:82px;')); ?>
			<div class="compose_div2">
				<?php echo $form->textField($model,'subject',$htmlOptions=array('style'=>'height:19px;width:200px;')); ?>
				<?php echo $form->error($model,'subject'); ?> 
			</div>
                    </div>
                    <div class="compose_body" id="compo_main" style="margin-bottom: 72px;">
                        <div class="floatleft"  style="margin-bottom: 25px;">
                                <?php echo $form->labelEx($model,'body',$htmlOptions=array('class'=>'recevier_body')); ?>
                                <div class="compose_div3" id="compo" style="margin-bottom: 25px;" >
                                    <textarea id="Message_body" name="Message[body]" class="compose_textaria" style="width:630px;" onkeyup="autoresize(this)"><?php echo $reply['body'];?></textarea
                                    
                                        <?php //echo $form->textArea($model,'body',$htmlOptions=array('onkeyup'=>'autoresize(this)','class'=>'compose_textaria','style'=>'width:630px;')); ?>
                                        <?php echo $form->error($model,'body'); ?>
                                </div>
                        </div>
                        <?php if(isset($_GET['id'])){ ?>
                        <div class="forword_attach">
                            <a href="<?php echo $baseurl;?>/index.php/message/see/see?id=<?php echo $_GET['id']?>" style="font-size: 15px;">
                            <img src="<?php echo $baseurl; ?>/images/freeworld/attach-icon.png"/>
                            <input type="hidden" name="browsattachment" value="<?php echo $reply['attachment'];?>"/>
                            </a>
                        </div>
                        <?php }?>
                        
                        <div id="fileupload" >
                                       <div class="myfileupload-buttonbar "><span id ="attach">Attach a file:</span>
                                            <label class="myui-button" style="width: 138px; padding-top: 13px;">
                                              <span id ="file">Browse..</span> 
                                              <input type="file" name="browse" id="browse"/>
                                            </label>
                                        </div>
                            <div class="attachment_validation">
                                <span>Upto 1Mb</span>
                            </div>
                           </div>
                        
                    </div>

			<div class="view_mesaage">
				<button class="delete_select_mess"><?php echo MessageModule::t("Send") ?></button>
			</div>

			<?php $this->endWidget(); ?>
                    
                    
                        <?php $friendsserch ='';
                          for($i=0; $i<count($friend); $i++){
                          $friend_id = $friend[$i]['friend_id'];
                          $uiser_id  = $friend[$i]['user_id'];
                          if($friend_id!=$userid)
                           {
                               $friendsserch.= $friend_id .',';
                           }
                           else
                           {
                               $friendsserch.= $uiser_id .',';
                           }
                         }
                         $strfrnd = substr($friendsserch,0,strlen($friendsserch)-1); 
                 ?>
                        <input type="hidden" value="<?php echo $strfrnd;?>" id="strfrnd" />
		</div>
	</div>
       
       </div>
    </div>
</div>
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_suggest'); ?>
</body>