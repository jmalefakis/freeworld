<head>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
</head>

<?php 
    $userid  = Yii::app()->user->getID();
    $message = Message::model()->findAll("receiver_id = '$userid' AND is_read = '0'"); 
    $count   = count($message);   
?>

<div class="navigation_side">
	<div class="navigation_side_ul">
            <a <?php if(Yii::app()->getRequest()->pathInfo == 'message/compose'){ ?> class="nav_a_congg" <?php } else { ?> class="nav_a_con" <?php } ?>  href="<?php echo $this->createUrl('compose/') ?>">
                <div <?php if(Yii::app()->getRequest()->pathInfo == 'message/compose'){ ?> class="activetab" <?php } else { ?> class="nav_div_a" <?php } ?>>
                    Compose
                </div>
            </a>
            
            <a <?php if(Yii::app()->getRequest()->pathInfo == 'message/inbox'){ ?> class="nav_a_congg" <?php } else { ?> class="nav_a_con" <?php } ?>  href="<?php echo $this->createUrl('inbox/') ?>">
                <div <?php if(Yii::app()->getRequest()->pathInfo == 'message/inbox'){ ?> class="activetab" <?php } else { ?> class="nav_div_a" <?php } ?> >
                    Inbox<span style="color: red;"><?php if($count>0) {?>(<?php echo $count;?>)<?php }?></span>
                </div>
            </a>
            
            <!--<a <?php //if(Yii::app()->getRequest()->pathInfo == 'sent/sent'){ ?> class="nav_a_congg" <?php //} else { ?> class="nav_a_con" <?php //} ?>  href="<?php// echo $this->createUrl('sent/sent') ?>">
                <div<?php //if(Yii::app()->getRequest()->pathInfo == 'message/sent/sent'){ ?> class="activetab" <?php //} else { ?> class="nav_div_a" <?php// } ?>>
                    Sent
                </div>
            </a> -->
            
            <a <?php if(Yii::app()->getRequest()->pathInfo == 'message/trash'){ ?> class="nav_a_congg" <?php } else { ?> class="nav_a_con" <?php } ?>  href="<?php echo $this->createUrl('trash/') ?>">
                <div <?php if(Yii::app()->getRequest()->pathInfo == 'message/trash'){ ?> class="activetab" <?php } else { ?> class="nav_div_a" <?php } ?>>
                    Trash
                </div>
            </a>    
            
	</div>
</div>
