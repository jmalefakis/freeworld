<?php

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
                'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
                'name'=>'Free World',

                // preloading 'log' component
                'preload'=>array('log'),

                // autoloading model and component classes
                'import'=>array(
                                'application.models.*',
                                'application.components.*',
                                'application.helpers.*',
                                'application.extensions.CAdvancedArFindBehavior.CAdvancedArFindBehavior',
                ),

                'modules' => array(
                                'message' => array(
                                                'userModel' => 'User',
                                                'getNameMethod' => 'getFullName',
                                                'getSuggestMethod' => 'getSuggest',
                                                'viewPath' => '/message/fancy',

                                ),
                ),

                'defaultController'=>'post',

                // application components
                'components'=>array(
                'user'=>array(
                // enable cookie-based authentication
                'allowAutoLogin'=>true,
                'class' => 'WebUser',
                ),
                'image'=>array(
                'class'=>'application.extensions.image.CImageComponent',
                // GD or ImageMagick
                'driver'=>'GD',
                // ImageMagick setup path
                'params'=>array('directory'=>'/opt/local/bin'),
                ),

                'session' => array(
                'autoStart'=>true,
                ),

                'db'=>array(
                'connectionString' => 'mysql:host=192.168.111.175;dbname=freeworld_demo',
                'emulatePrepare' => true,
                'username' => 'freeworld',
                'password' => 'FreeWorld123',
                'charset' => 'utf8',
                'tablePrefix' => 'tbl_',
                ),

                'errorHandler'=>array(
                // use 'site/error' action to display errors
                'errorAction'=>'site/error',
                ),
                'urlManager'=>array(
                'urlFormat'=>'path',
                'rules'=>array(
                'post/<id:\d+>/<title:.*?>'=>'post/view',
                'posts/<tag:.*?>'=>'post/index',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
                ),
                'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                array(
                'class'=>'CFileLogRoute',
                'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                'class'=>'CWebLogRoute',
                ),
                */
                ),
                ),
                ),

                'params'=>array(
                // this is used in contact page
                'siteurl'=>'http://192.168.111.175:17555',
                'globelUrl'=>'http://192.168.111.175:17555/index.php',
                'workingdir'=>$_SERVER['DOCUMENT_ROOT'],
                'fbshare'=>'441443142563876',
                'searchurl'=>'http://192.168.111.175:17555/index.php/list/search?search',
                ),

                // application-level parameters that can be accessed
                // using Yii::app()->params['paramName']
);