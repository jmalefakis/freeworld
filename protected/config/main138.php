<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Free World',

	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
	),
	
	'modules' => array(
		'message' => array(
			'userModel' => 'User',
			'getNameMethod' => 'getFullName',
			'getSuggestMethod' => 'getSuggest',
			'viewPath' => '/message/fancy',
		),
	),

	'defaultController' => 'post',

	// application components
	'components' => array(
		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
			'class' => 'WebUser',
		),
		'image' => array(
			'class' => 'application.extensions.image.CImageComponent',
			// GD or ImageMagick
			'driver' => 'GD',
			// ImageMagick setup path
			'params' => array('directory' => '/opt/local/bin'),
		),
			
		'session' => array(
			'autoStart' => true,  
		),
		
		
		/*'db'=>array(
			'connectionString' => 'sqlite:protected/data/blog.db',
			'tablePrefix' => 'tbl_',
		),*/
		// uncomment the following to use a MySQL database
		
		'db' => array(
			'connectionString' => 'mysql:host=192.168.111.175;dbname=freeworld_demo',
			'emulatePrepare' => true,
			'username' => 'freeworld',
            'password' => 'FreeWorld123',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
			'enableProfiling' => true,
			'enableParamLogging' => true
		),
		
		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'rules' => array(
				'post/<id:\d+>/<title:.*?>' => 'post/view',
				'posts/<tag:.*?>' => 'post/index',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				/*
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				*/
			
				'db' => array( // -- CProfileLogRoute -----------------------
					'class' => 'CWebLogRoute',
					'categories' => 'system.db.CDbCommand',
					//'class' => 'CProfileLogRoute',
					'levels' => 'trace',
					'enabled' => false,
					//'showInFireBug' => true,
				),
			
				/*
				'db' => array(
					'class' => 'CWebLogRoute',
					'categories' => 'system.db.*',
					'enabled' => true,
					'showInFireBug' => true //Показывать в FireBug или внизу каждой страницы
				),
				*/
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class' => 'CWebLogRoute',
				),
				*/
			),
		),
	),
	
	//'params' => array(
		// this is used in contact page
		//'siteurl' => 'http://192.168.111.175:17555' /*'http://dev.freeworldco.com'*/, 
		//'globelUrl' => 'http://192.168.111.175:17555/index.php' /*'http://dev.freeworldco.com/index.php'*/, 
		//'workingdir' => $_SERVER['DOCUMENT_ROOT'],
		//'fbshare' => '441443142563876',
		//'searchurl' => 'http://192.168.111.175:17555/index.php/list/search?search' /*'http://dev.freeworldco.com/index.php/list/search?search'*/,  
	//),
	
	'params' => array(
		// this is used in contact page
		'siteurl' => 'http://192.168.111.38:3001', 
		'globelUrl' => 'http://192.168.111.38:3001/index.php', 
		'workingdir' => $_SERVER['DOCUMENT_ROOT'],
		'fbshare' => '441443142563876',
		'searchurl' => 'http://192.168.111.38:3001/index.php/list/search?search',
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	//'params'=>require(dirname(__FILE__).'/params.php'),
);