<?php

return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Cron',
 
    'preload'=>array('log'),
 
    'import'=>array(
        'application.components.*',
        'application.models.*',
        'application.helpers.*',
    ),
    'components'=>array(
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'logFile'=>'cron.log',
                    'levels'=>'error, warning',
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'logFile'=>'cron_trace.log',
                    'levels'=>'trace',
                ),
            ),
        ),
        
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName' => FALSE,
            'rules'=>array(),
        ),   
 
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=freeworld_demo',
            'emulatePrepare' => true,
            'username' => 'freeworld',
            'password' => 'FreeWorld123',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),
    ),
    'params'=>array(
        'siteurl'=>'http://dev.freeworldco.com',
    ),
);