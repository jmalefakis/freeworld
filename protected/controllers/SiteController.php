<?php

class SiteController extends Controller
{
    public $layout='guestuser';

    /**
     * Declares class-based actions.
     */


    public function filters()
    {
        return array(
                        'accessControl',
        );
    }
    public function accessRules() {
        return array(
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('aboutus','contact','help','privacypolicy','terms','login','Logout','emaillinklist','emailloginlist','Error','archive'),
                                        'users' => array('*'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('aboutus','contact','help','privacypolicy','terms','Logout','emaillinklist','emailloginlist','Error','archive'),
                                        'users' => array('@'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                         
                        array('deny', // deny all users
                                        'users' => array('*'),
                        ),
        );
    }


    public function actions()
    {
        return array(
                        // captcha action renders the CAPTCHA image displayed on the contact page
                        'captcha'=>array(
                                        'class'=>'CCaptchaAction',
                                        'backColor'=>0xFFFFFF,
                        ),
                        // page action renders "static" pages stored under 'protected/views/site/pages'
                        // They can be accessed via: index.php?r=site/page&view=FileName
                        'page'=>array(
                                        'class'=>'CViewAction',
                        ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {

        $this->layout = 'guestuser';
         
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionaboutus()
    {
        $currentid = Yii::app()->user->getID();
        if($currentid != '')
        {
            $this->layout = 'layout/authrized_user';
        }
        else
        {
            $this->layout = 'guestuser';
        }
        $this->render('aboutus');
    }
    public function actioncontact()
    {
        $currentid = Yii::app()->user->getID();
        if($currentid != '')
        {
            $this->layout = 'layout/authrized_user';
        }
        else
        {
            $this->layout = 'guestuser';
        }

        if(isset($_POST['name']))
        {
            $adminEmail = 'contact@wayoworld.com';
            
            $contact = new ContactUs;
            $contact->name = $_POST['name'];
            $contact->email = $_POST['email'];
            $contact->telephone = $_POST['telephone'];
            $contact->comment = $_POST['comment'];
            $contact->created = DateHelper::getCurrentSQLDateTime();
            $contact->save(false);
            if($contact->save(false))
            {
                $recipientemail = $contact->email;
                $subject = 'ContactUs - wayoworld.com';
                $message = " <img src='".Yii::app()->params['siteurl']."/images/newimages/header_logo_freworld-send2.png' style='height:180px;;width:160px;' /><br/><br/>
                <div style='padding: 16px 0 18px;'>
                Dear $contact->name, <br/><br/>
                Thank you for the feedback. <br/>
                We have received your contact details. We will contact you soon.  <br/><br/>
                Best Regards,<br/>
                Wayo Team
                </div>" ;

                MailHelper::sendEmail($recipientemail, $adminEmail, $subject, $message);
                
                $subject = 'ContactUs - wayoworld.com';
                $message = " <img src='".Yii::app()->params['siteurl']."/images/newimages/header_logo_freworld-send2.png' style='height:180px;;width:160px;' /><br/><br/>
                <div style='padding: 16px 0 18px;'>
                User ".$contact->name." added a comment: <br/><br/>
                ".nl2br($contact->comment).". <br/><br/>
                His phone number: ".$contact->telephone."  <br/><br/>
                Best Regards,<br/>
                Wayo Team
                </div>" ;

                MailHelper::sendEmail($adminEmail, $contact->email, $subject, $message);

                $result = 1;
                echo json_encode($result);
                exit;
            }
            else
            {
                $result = 0;
                echo json_encode($result);
                exit;
            }
        }
        $this->render('contact');
    }

    public function actionhelp()
    {
        $currentid = Yii::app()->user->getID();
        if($currentid != '')
        {
            $this->layout = 'layout/authrized_user';
        }
        else
        {
            $this->layout = 'guestuser';
        }
        $this->render('help');
    }
    public function actionprivacypolicy()
    {
        $currentid = Yii::app()->user->getID();
        if($currentid != '')
        {
            $this->layout = 'layout/authrized_user';
        }
        else
        {
            $this->layout = 'guestuser';
        }
        $this->render('privacypolicy ');
    }
    public function actionterms()
    {
        $currentid = Yii::app()->user->getID();
        if($currentid != '')
        {
            $this->layout = 'layout/authrized_user';
        }
        else
        {
            $this->layout = 'guestuser';
        }
        $this->render('terms');
    }

    public function actionlogin()
    {
        //echo 'jjjj';exit;
        $this->redirect(array('user/home'));
    }

    /**
     * Displays the contact page
     */
    public function actionContacts()
    {
        $this->layout = 'afterlogin';
        $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $headers="From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contacts',array('model'=>$model));
    }

    /**
     * Displays the login page
     */


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionemaillinklist($listid=null,$userid=null)
    {
        //$userid     = Yii::app()->user->getID();
        //echo $userid;exit;
        $myuserinfo = User::model()->find("id = '$userid'");
        $takenlist = ListTaken::model()->find("list_id = '$listid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;

        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $this->render('emaillinklist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow,'takenlist'=>$takenlist));
    }

    public function actionemailloginlist($listid=null,$userid=null)
    {
        //$this->layout = ('unauthrizedcol1');
        //$userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
         
        $takenlist = ListTaken::model()->find("list_id = '$listid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;

        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $this->render('emailloginlist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow,'takenlist'=>$takenlist));
    }

    public function actionArchive()
    {
        $currTime = time();
        
        $lastArchiveStat = ArchiveStat::model()->find(array("order" => "id DESC"));
        if (!$lastArchiveStat)
        {
            $lastArchiveStat = new ArchiveStat();
            $lastArchiveStat->lock = 0;
            $lastArchiveStat->created = DateHelper::getCurrentSQLDateTime();
            $lastArchiveStat->modified = DateHelper::getCurrentSQLDateTime();
            //$lastArchiveStat->save(false);
        }
        
        $lastArchiveTime = strtotime($lastArchiveStat->modified);
        
        // check if new archive procedure needed
        if ($currTime - $lastArchiveTime < 10000 && 1 != 1)
        {
            echo "Archive skiped!<br/>\n";
        }
        elseif ($lastArchiveStat->lock != 1)
        {
            // add stat and lock
            $archiveStat = new ArchiveStat();
            $archiveStat->lock = 1;
            $archiveStat->created = DateHelper::getCurrentSQLDateTime();
            $archiveStat->modified = DateHelper::getCurrentSQLDateTime();
            //$archiveStat->save(false);
            
            //$currTime += 86400; // for test only
            
            // make archive
            $lists = ListForm::model()->findAll(array(
                "condition" => "archive_schedule IS NOT NULL"
            ));
            
            $lc = 0;
            foreach ($lists as $list)
            {
                // check archive schedule
                $nextArchiveDateStr = ArchiveHelper::getNextArchiveDate($list->id);
                if (!$nextArchiveDateStr) continue;
                
                // check next archive time
                $nextArchiveDate = strtotime($nextArchiveDateStr); // not today
                if ($nextArchiveDate < 1 || $nextArchiveDate > $currTime) continue;
                
                // get archive entry
                $listArchive = Archive::model()->findByAttributes(array(
                    "list_id" => $list->id
                ));
                
                if (!$listArchive) {
                    $listArchive = new Archive();
                    $listArchive->list_id = $list->id;
                    $listArchive->count = 0;
                    $listArchive->created = DateHelper::getSQLDateTime($nextArchiveDate);
                    $listArchive->modified = DateHelper::getSQLDateTime($nextArchiveDate);
                } else {
                    // check last archive time for the lists
                    $llDate = strtotime($listArchive->modified);
                    // one archive in a day
                    if ($nextArchiveDate - $llDate < 86000) continue;
                }
                
                // archive all opinions
                $opinions = Opinion::model()->findAll(array(
                    "condition" => "list_id = :list AND archived = 0",
                    "params" => array(":list" => $list->id)
                ));                
                foreach ($opinions as $opinion)
                {
                    $opinion->archived = 1;
                    $opinion->modified = DateHelper::getCurrentSQLDateTime();
                    //$opinion->save(false);
                }
                
                // update list archive record
                $listArchive->count = $listArchive->count + 1;
                $listArchive->modified = DateHelper::getSQLDateTime($currTime);
                //$listArchive->save(false);
                
                $lc++;
            }
            
            echo "Lists archived: " . $lc . "<br/>\n";
            
            //remove lock
            $archiveStat->lock = 0;
            $archiveStat->modified = DateHelper::getCurrentSQLDateTime();
            //$archiveStat->save(false);
            
            $lastArchiveTime = strtotime($archiveStat->modified);
            
        } else echo "Archive in progress...<br/>\n";
        
        echo "Current Time:         "
            . date("m/d/Y H:i:s", DateHelper::toClientTime(time())) . "<br/>\n";
        echo "Last Archive Time:    " 
            . date("m/d/Y H:i:s", DateHelper::toClientTime($lastArchiveTime))
            . "<br/>\n\n";
    }
}