<?php
define("PAGE_LIMIT", 8);
define("PAGE_LIMIT_LISTS", 8);
define("PAGE_LIMIT_PIOPLE", 12);

include('recaptchalib.php');

class UserController extends Controller
{
	public $layout='column1'; 

        
         public function filters()
            {
                return array(
                    'accessControl',
                );        
            } 
                public function accessRules() {
                return array(
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('home','forgotpassword','activate','login_facebook','confermation','login_list','emailsharelogin','checkdatabase','resendemail','userinterest',
                                        'userInterestNew','users','searchinterestlist','friends','friendsWithoutList'),
                        'users' => array('*'),
                        //'expression'=>'$user->isAdmin',
                    ),
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array( 'userprofile','editprofile','setting', 'Logout','crew','removecrew','mycrew','listlogin','group','creategroup','seegroup'
                                          ,'addmembers', 'leavemember','acceptrequest','rejectrequest','officialtoday','facebook','facebooklisting','twitter_oauth', 'twitter',
                                           'twitterlisting','twitterinvitation','twitter_invitation_oauth','addfriend','acceptfrndrequest','rejectfrndrequest',
                                           'myNotifications','myDashboard','update_profilestatus','add_profilefriend','remove_profilefriend','unfollowCompany','readmessages','addfavorite',
                                           'update_profileinterest','update_displaylist','myFriends','myFriendsPagination','editnotification','addnotification','upload',
                                           'myDashboard', 'myDashboardPagination', 'myDashboardUpdateOrder',
										   'myTaken', 'myTakenPagination','dashboard',
                                           'myCreated','myCreatedPagination','myCreatedData',
                                           'myProfile','myProfileCompany','upoadImageProfile','changePassword','respondFrndRequest','removeFriendRequest',
                                           'changeStatus', 'toggleNotificationsOn', 'updateSummary', 'undoAcceptFriend', 'undoRejectFriend','readNotifications',
                                           'showUserBriefs','toogleSummary','getNotifications','setTimezone','inviteFriend','deactivate','getShortTitle','login',
                                        ),
                        'users' => array('@'),
                        //'expression'=>'$user->isAdmin',
                    ),
               
                    array('deny', // deny all users
                        'users' => array('*'),
                    ), 
                );
            }
        
        
        
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
        return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
        public function actionIndex()
	{
            $this->render('index');
            //$this->render('index');$this->redirect(array('user/home'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
              $this->layout = 'guestuser';
           
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			} 
		}
		$this->render('contact',array('model'=>$model));
	}
        
        public function actionView($id)
	{
            $this->layout = 'afterlogin';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
        
        public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model; 
	}
        
        
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $this->layout = 'afterlogin';
		$model=new Customer;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Customer']))
		{
			$model->attributes=$_POST['Customer'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->customer));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
     
        
	public function actionUpdate($id)
	{
                $this->layout = 'afterlogin';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->name     =$_POST['User']['name'];
                        $model->username =$_POST['User']['username'];
                        $model->password = md5($_POST['User']['password']);
                        $model->company  =$_POST['User']['company'];
                        $model->admin    =$_POST['User']['admin'];
                        $model->created  =$_POST['User']['created'];
                        $model->modified =$_POST['User']['modified'];
                        
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	    }
        
	    public function actiondashboard($userid = null)
	    {
	        $this->redirect("myDashboard?userid=" . $userid);
	    }
        
        public function actionupdate_profilestatus()
	    {
                $this->layout = 'authrized_dashboard';
                $userid   = Yii::app()->user->getID();
		$model=$this->loadModel($userid);
                
		if(isset($_POST['User']['profile_status']))
		{
                     if($_POST['User']['profile_status'] != 'Update Status')
                     {  
			$model->profile_status = $_POST['User']['profile_status'];
                        $model->save(false);
                        
			if($model->save(false))
                        {
                            $result =1;
                            echo json_encode($result); 
                            exit;
                        }
                        else
                        {
                            $result =0;
                            echo json_encode($result); 
                            exit;
                        }
                     }
                     else
                     {
                            $result = 'empty';
                            echo json_encode($result); 
                            exit;
                     }
		}
        }
        
        public function actionupdate_profileinterest()
	{
                $this->layout = 'authrized_dashboard';
                $userid   = Yii::app()->user->getID();
		$model=$this->loadModel($userid);
                
		if(isset($_POST['interest']))
		{       
                        $interest = $_POST['interest'];
                        $inter = implode(",", $interest);
                        $model->interest = $inter;
                        $model->save(false);
                        
			if($model->save(false))
                        {
                            $result =1;
                            echo json_encode($result); 
                            exit;
                        }
                        else
                        {
                            $result =0;
                            echo json_encode($result); 
                            exit;
                        }
                }
                else
                {
                       $result = 'empty';
                       echo json_encode($result); 
                       exit;
                }
        }
        
        public function actionupdate_displaylist()
	{
                $this->layout = 'authrized_dashboard';
                $userid   = Yii::app()->user->getID();
		$model=$this->loadModel($userid);
                
		if(isset($_POST['dplistid']))
		{       
                        $dplistid = $_POST['dplistid'];
                        $model->display_list = $dplistid;
                        $model->save(false);
                        
			if($model->save(false))
                        {
                            $result =1;
                            echo json_encode($result); 
                            exit;
                        }
                        else
                        {
                            $result =0;
                            echo json_encode($result); 
                            exit;
                        }
                }
        }
        
        public function actionadd_profilefriend()
        {
            $userid = Yii::app()->user->getID();
            $baseurl = Yii::app()->request->getBaseUrl(true);
            $user = User::model()->find("id = '$userid'");
            $name = $user['firstname'].' '.$user['lastname'];

            if (Yii::app()->request->getRequestType() == 'POST')
            {
                $friendid = $_POST['value'];
                $usertable = User::model()->find("id = '$friendid'");
                $message = 'Friend request from User "'. $name. '"';
                $notificationid = FriendHelper::inviteFriend($userid, $friendid, $message);
                
                $arry = array('id' => $notificationid, "result" => "Friend request sent.");
                echo json_encode($arry);
                
                exit;
            }
        }
        
        public function actionremove_profilefriend()
        {
            $userid = Yii::app()->user->getID();

            if(Yii::app()->request->getRequestType() == 'POST')
            {
                $friendid = $_POST['value'];
                FriendHelper::unfriendPeople($userid, $friendid);
                
                echo "Removed from your friend list.";
                
                exit;
            }
        }
        
        public function actionunfollowCompany()
        {
        	$userid = Yii::app()->user->getID();
        
        	if(Yii::app()->request->getRequestType() == 'POST')
        	{
        		$compid = $_POST['value'];
        		FriendHelper::unfollowCompany($userid, $compid);
        
        		echo "Removed from followed companies.";
        
        		exit;
        	}
        }
        
        public function actionrespondFrndRequest()
        {
            $userid  = Yii::app()->user->getID();

            if (Yii::app()->request->getRequestType() == 'POST')
            {
                $friendid = $_POST['value'];
                $friendnm = User::model()->find("id = '$friendid'");
                $namefrnd = $friendnm['firstname'] . ' ' . $friendnm['lastname'];
                
                FriendHelper::acceptFriendRequest($userid, $friendid);

                $value =  "You are friends with $namefrnd.";                
                echo $value;
                
                exit;
            }
        }
        
        public function actionremoveFriendRequest($notificationid = null)
        {
            $userid = Yii::app()->user->getID();

            if (Yii::app()->request->getRequestType() == 'POST')
            {
                $friendid = $_POST['value'];
                FriendHelper::removeFriendRequest($userid, $friendid, $notificationid);
                echo "Friend Request cancelled.";
                exit;
            }
        }        
        
        public function actioncheckdatabase()
        {      
            $workingdir   = Yii::app()->params['workingdir'];   
            $csv          = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));
            $value        = $_POST['email'];             
            $title        = User::model()->find("email = '$value'");
            
            if(empty($title))
            {
                        $result =1;
                        echo json_encode($result); 
                        exit;
            }
            else
            {
                        $result =0;
                        echo json_encode($csv[98]); 
                        exit;
            }
        }        

	/**
	 * Displays the login page 
	 */
	public function actionLogin()
	{       
        $this->layout = 'unauthrizedcol1';

        /*if($register){
        Yii::app()->user->setFlash('contact','Thank you for joining with us .'); 
        }*/
		$model=new LoginForm;
                
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
  
		// collect user input data
		if(isset($_POST['LoginForm']))
		{     
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(array('user/twitterlisting'));
                       
		} else {
            $this->redirect(array('user/home'));
        }
                 
		// display the login form
		$this->render('login',array('model'=>$model));               
	}
        
        
        public function actionlogin_facebook()
	{  
            $baseurl  = Yii::app()->request->baseUrl;
            Yii::import('application.vendors.*');
            require_once('facebook/facebook.php'); 
            # Creating the facebook object
            
            
            $config = array(
                'appId' => '493318360686506',
                'secret' => '2c2431e65c634a125ac40586b57b150b',
                
              );
           
            # Let's see if we have an active session
            $facebook = new Facebook($config);
            $user_id = $facebook->getUser();
            

            if(!empty($user_id)) { 
                # Active session, let's try getting the user id (getUser()) and user info (api->('/me')) 
                try{
                    $uid  = $facebook->getUser();
                    $user = $facebook->api('/me/friends');
                    $my_information = $facebook->api('/me');                    
                    //$temp =' https://graph.facebook.com/581913302/picture';
                } catch (Exception $e){}
       
                if(!empty($my_information)){
                    # User info ok? Let's print it (Here we will be adding the login and registering routines)                    
                    
                    //echo "<pre>";print_r($my_information);exit;
                    
                       $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_user')
                                            ->where('facebook_id=:value', array(':value' =>$my_information['id']))
                                            ->queryRow();
                        
                        
                    if(empty($usertable))                   
                      {    
                         $model = new User;
                         $model->firstname    = $my_information['first_name'];
                         $model->lastname     = $my_information['last_name'];   
                         //$model->location     = $my_information['location']['name'];  
                         $model->gender       = $my_information['gender'];  
                         $model->email        = $my_information['username'];
                         $model->password     = md5($my_information['id']);
                         $model->facebook_id  = $my_information['id'];                          
                         $model->created      = date('Y-m-d');
                         $model->modified     = date('Y-m-d');                         
                         $model->save(false);                     
                      } 
                      
                      $email     = $my_information['username'];
                      $usermodel = User::model()->find("email = '$email'");
                      $userid    = $usermodel['id'];
                      $count     = count($user['data']);
                      
                      #Notification when user register with Wayo World
                      if(empty($usertable))                   
                      {  
                          $fbid         = $my_information['id'];
                          $notification = Notification::model()->find("facebook_id = '$fbid'");                      
                          if(!empty($notification))
                            {
                                $fname = $my_information['first_name'];
                                $lname = $my_information['last_name'];
                                $name  = "<a href=$baseurl/index.php/user/userprofile?userid=$userid>$fname $lname</a>";
                                $notification->read_notification  = 0;  
                                $notification->message            = $name.' '."joined Wayo World";
                                $notification->created            = DateHelper::getCurrentSQLDateTime();      
                                $notification->modified           = DateHelper::getCurrentSQLDateTime();      
                                $notification->save(false);                            
                            }
                      }                      
                      #notification code end here
                      
                      for($i=0; $i<$count; $i++)
                        { 
                            $usertable = Yii::app()->db->createCommand()
                                                ->select('*')
                                                ->from('tbl_facebook')
                                                ->where('friend_facebookid=:value and user_id=:id', array(':value' =>$user['data'][$i]['id'],':id'=>$userid))
                                                ->queryRow();

                            
                        if(empty($usertable))                   
                          {    
                             $model = new FacebookForm;
                             $model->user_id            = $userid;
                             $model->facebook_id        = $my_information['id'];
                             $model->friend_facebookid  = $user['data'][$i]['id']; 
                             $model->friend_name        = $user['data'][$i]['name']; 
                             $facebookimg               = 'https://graph.facebook.com/'.$user['data'][$i]['id'].'/picture'; 
                             $model->friend_facebookimg = $facebookimg;
                             $model->invite             = 'invite member';
                             $model->image              = Yii::app()->params['siteurl'] . '/images/face_book.png';
                             $model->created            = date('Y-m-d');
                             $model->modified           = date('Y-m-d');                         
                             $model->save(false);                         
                          }   
                        } 
                      
                      $model=new LoginForm;		
                     
		// collect user input data
		if(isset($my_information))
		{     
                        
                        $model->email=$my_information['username'];
			$model->password=$my_information['id'];
                        
                        //echo "<pre>";print_r($model);exit;    
			// validate user input and redirect to the previous page if valid
			if($model->login() && $model->validate())
				$this->redirect(array('user/facebooklisting'));
                       
		}
                } else {
                    # For testing purposes, if there was an error, let's kill the script
                    die("There was an error.");
                }
            } else {
                # There's no active session, let's generate one
                 $login_url = $facebook->getLoginUrl();
                 header("Location: ".$login_url);
            }
        }
        
        public function actionlogin_list($listid=null)
	{  
            Yii::import('application.vendors.*');
            require_once('facebook/facebook.php'); 
            # Creating the facebook object
            
            $config = array(
                'appId' => '493318360686506',
                'secret' => '2c2431e65c634a125ac40586b57b150b',
              );
            
            
           
            
            # Let's see if we have an active session
            $facebook = new Facebook($config);
            $user_id = $facebook->getUser();
            

            if(!empty($user_id)) { 
                # Active session, let's try getting the user id (getUser()) and user info (api->('/me')) 
                try{
                    $uid = $facebook->getUser();
                    $user = $facebook->api('/me/friends');
                    $my_information = $facebook->api('/me');
                    
                    //$temp =' https://graph.facebook.com/581913302/picture';
                } catch (Exception $e){}

                if(!empty($my_information)){
                    # User info ok? Let's print it (Here we will be adding the login and registering routines)                    
                    
                    //echo "<pre>";print_r($my_information);exit;
                   
                       $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_user')
                                            ->where('facebook_id=:value', array(':value' =>$my_information['id']))
                                            ->queryRow();
                        
                        
                    if(empty($usertable))                   
                      {    
                         $model = new User;
                         $model->firstname    = $my_information['first_name'];
                         $model->lastname     = $my_information['last_name'];   
                         //$model->location     = $my_information['location']['name'];  
                         $model->gender       = $my_information['gender'];  
                         $model->email        = $my_information['username'];
                         $model->password     = md5($my_information['id']);
                         $model->facebook_id  = $my_information['id'];                          
                         $model->created      = date('Y-m-d');
                         $model->modified     = date('Y-m-d');                         
                         $model->save(false);                     
                      } 
                      
                      
                      $email = $my_information['username'];
                      $usermodel = User::model()->find("email = '$email'");
                      $userid = $usermodel['id'];
                      
                      $count = count($user['data']);
                      
                      for($i=0; $i<$count; $i++)
                        { 
                            $usertable = Yii::app()->db->createCommand()
                                                ->select('*')
                                                ->from('tbl_facebook')
                                                ->where('friend_facebookid=:value and user_id=:id', array(':value' =>$user['data'][$i]['id'],':id'=>$userid))
                                                ->queryRow();


                        if(empty($usertable))                   
                          {    
                             $model = new FacebookForm;
                             $model->user_id            = $userid;
                             $model->facebook_id        = $my_information['id'];
                             $model->friend_facebookid  = $user['data'][$i]['id']; 
                             $model->friend_name        = $user['data'][$i]['name']; 
                             $facebookimg               = 'https://graph.facebook.com/'.$user['data'][$i]['id'].'/picture'; 
                             $model->friend_facebookimg = $facebookimg;
                             $model->invite             = 'invite member';
                             $model->image              = Yii::app()->params['siteurl'] . '/images/face_book.png';
                             $model->created            = date('Y-m-d');
                             $model->modified           = date('Y-m-d');                         
                             $model->save(false);                         
                          }   
                        } 
                      
                      
                      $model=new LoginForm;		
                     
		// collect user input data
		if(isset($my_information))
		{     
                        $model->email=$my_information['username'];
			$model->password=$my_information['id'];
                        
                        //echo "<pre>";print_r($model);exit;    
			// validate user input and redirect to the previous page if valid
			if($model->login() && $model->validate())
                        {
                       ?>
                     <script type="text/javascript">
                        parent.tb_remove() ; 
                        window.parent.location.href = "<?php echo Yii::app()->params['siteurl']?>/index.php/list/takelist?listid=<?php echo $listid ;?>";
                    </script>
                    <?php 
                          //exit;
                        }
		}
                } else {
                    # For testing purposes, if there was an error, let's kill the script
                    die("There was an error.");
                }
            } else { 
                # There's no active session, let's generate one
                 $login_url = $facebook->getLoginUrl();
                 header("Location: ".$login_url);
                 
            }
        }
        
        
        public function actionemailsharelogin($listid=null)
	{  
            Yii::import('application.vendors.*');
            require_once('facebook/facebook.php');
            # Creating the facebook object
            $config = array(
                'appId' => '493318360686506',
                'secret' => '2c2431e65c634a125ac40586b57b150b',
              );
            
            
                        
            # Let's see if we have an active session
            $facebook = new Facebook($config);
            $user_id = $facebook->getUser();
            

            if(!empty($user_id)) { 
                # Active session, let's try getting the user id (getUser()) and user info (api->('/me')) 
                try{
                    $uid = $facebook->getUser();
                    $user = $facebook->api('/me/friends');
                    $my_information = $facebook->api('/me');
                    
                    //$temp =' https://graph.facebook.com/581913302/picture';
                } catch (Exception $e){}

                if(!empty($my_information)){
                    # User info ok? Let's print it (Here we will be adding the login and registering routines)                    
                    
                    //echo "<pre>";print_r($my_information);exit;
                   
                       $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_user')
                                            ->where('facebook_id=:value', array(':value' =>$my_information['id']))
                                            ->queryRow();
                        
                        
                    if(empty($usertable))                   
                      {    
                         $model = new User;
                         $model->firstname    = $my_information['first_name'];
                         $model->lastname     = $my_information['last_name'];   
                         //$model->location     = $my_information['location']['name'];  
                         $model->gender       = $my_information['gender'];  
                         $model->email        = $my_information['username'];
                         $model->password     = md5($my_information['id']);
                         $model->facebook_id  = $my_information['id'];                          
                         $model->created      = date('Y-m-d');
                         $model->modified     = date('Y-m-d');                         
                         $model->save(false);  
                         
                      } 
                      
                      
                      $email     = $my_information['username'];
                      $usermodel = User::model()->find("email = '$email'");
                      $userid    = $usermodel['id'];
                      
                      
                      $count = count($user['data']);
                      
                      for($i=0; $i<$count; $i++)
                        { 
                            $usertable = Yii::app()->db->createCommand()
                                                ->select('*')
                                                ->from('tbl_facebook')
                                                ->where('friend_facebookid=:value and user_id=:id', array(':value' =>$user['data'][$i]['id'],':id'=>$userid))
                                                ->queryRow();


                        if(empty($usertable))                   
                          {    
                             $model = new FacebookForm;
                             $model->user_id            = $userid;
                             $model->facebook_id        = $my_information['id'];
                             $model->friend_facebookid  = $user['data'][$i]['id']; 
                             $model->friend_name        = $user['data'][$i]['name']; 
                             $facebookimg               = 'https://graph.facebook.com/'.$user['data'][$i]['id'].'/picture'; 
                             $model->friend_facebookimg = $facebookimg;
                             $model->invite             = 'invite member';
                             $model->image              = Yii::app()->params['siteurl'] . '/images/face_book.png';
                             $model->created            = date('Y-m-d');
                             $model->modified           = date('Y-m-d');                         
                             $model->save(false);                         
                          }   
                        } 
                        
                        
                      
                      
                      $model=new LoginForm;		
                     
		// collect user input data
		if(isset($my_information))
		{     
                        $model->email=$my_information['username'];
			$model->password=$my_information['id'];
                        $_SESSION['name'] = $my_information['first_name'].' '.$my_information['last_name']; 
                        //echo "<pre>";print_r($model);exit;    
			// validate user input and redirect to the previous page if valid
			if($model->login() && $model->validate())
                            $this->redirect(array('list/takelist','listid'=>$listid));
		}
                } else {
                    # For testing purposes, if there was an error, let's kill the script
                    die("There was an error.");
                }
            } else { 
                # There's no active session, let's generate one
                 $login_url = $facebook->getLoginUrl();
                 header("Location: ".$login_url);
                 
            }
        }
        
        public function actionuserlisting()
	{    
                
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('userlisting',array(
			'model'=>$model,
		));
	} 
        
        
        public function actionuserprofile($userid=null)
	{
            $this->layout = 'authrized';
            if(isset($_GET['userid']))
            {
                $id     = $_GET['userid'];  
                $userid = Yii::app()->user->getID();
                $user   = User::model()->find("id = '$id'");
            }
            else
            {
             $userid   = Yii::app()->user->getID();
             $id       = $userid;
             $user     = User::model()->find("id = '$userid'");
            }
          
            $crewuser = Crew::model()->findByAttributes(array(),$condition  = 'user_id = :id1 AND otheruser_id = :id2',$params = array( ':id1' => $userid,':id2' => $id,));            
            $interest = Interest::model()->findAll(array("order" => "interest ASC"));
            $group    = GroupMembers::model()->findAll("otheruser_id = $id");
            
            $this->render('userprofile',array('user'=>$user,'interest'=>$interest,'crewuser'=>$crewuser,'id'=>$id,'group'=>$group));
        }   
        
        
        
        public function actioneditprofile()
        {
            $this->layout = 'plane';
            
            $workingdir   = Yii::app()->params['workingdir'];   
            $csv          = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));
            
            $currentid = Yii::app()->user->getID();
            $user      = User::model()->find("id = '$currentid'");
            $baseurl   = Yii::app()->request->baseUrl;
            
            if(Yii::app()->request->getRequestType() == 'POST' && (isset($_POST['User'])))
            {
                $firstname   = $_POST['User']['firstname'];
                $lastname    = $_POST['User']['lastname'];
                if($_POST['User']['password'] != '')
                {
                    $password  = md5($_POST['User']['password']);
                }
                else
                {
                    $password = $user->password;
                }
                $birthdate   = $_POST['User']['birthdate'];
                $gender      = $_POST['User']['gender'];
                $country     = $_POST['User']['country'];
                $city_state  = $_POST['User']['city_state'];
                $occupation  = $_POST['User']['occupation'];                
                $education   = $_POST['User']['education'];
                
                if(isset($_POST['User']['interest']) && $_POST['User']['interest']!='')
                {
                    $inter       = $_POST['User']['interest'];
                    $interest    = implode(",", $inter);
                }
                else
                {
                    $interest = $user['interest'];                  
                }
                
                if($_FILES['profilepic']['tmp_name'] != '')
                 {
                    
                   $file= CUploadedFile::getInstanceByName('profilepic');

                    if(is_object($file) && get_class($file)== 'CUploadedFile') 
                    {
                        $user->image  = time().$file->name; 
                        $result       = $user->save(false);
                    }
                    
                    if($result)
                    {
                        $file->saveAs('images/profile/user_thumbs/dashboard/'.$user->image);
                            $src = 'images/profile/user_thumbs/dashboard/'.$user->image;
                            $dest = 'images/profile/user_thumbs/dashboard/'.$user->image;
                            
                            $source_image = ImageHelper::imageCreateFromSrc($src);
                            
                            $width = imagesx($source_image);
                            $height = imagesy($source_image);
                            $newwidth=112;
                            $newheight= ($height/$width)*$newwidth;
                            $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                            imagejpeg($virtual_image,$dest,100);

                            $dest = 'images/profile/user_thumbs/listcreation/'.$user->image;                            
                            $width = imagesx($source_image);
                            $height = imagesy($source_image);
                            $newwidth=75;
                            $newheight= ($height/$width)*$newwidth;
                            $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                            imagejpeg($virtual_image,$dest,100);
                            
                            $dest = 'images/profile/user_thumbs/header/'.$user->image;                            
                            $width = imagesx($source_image);
                            $height = imagesy($source_image);
                            $newwidth=25;
                            $newheight= ($height/$width)*$newwidth;
                            $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                            imagejpeg($virtual_image,$dest,100);
                        
                    }
                    
                 }
                      $user->setAttributes(array('gender'=>$gender,'firstname'=>$firstname,'lastname'=>$lastname,'password'=>$password,'birthdate' => $birthdate,'country'=>$country,'city_state'=>$city_state,'occupation'=>$occupation,'education'=>$education,'interest' => $interest));
                      $user->save(false); ?>
                    
                      <script>
                          var baseurl = '<?php echo $baseurl; ?>';
                          var target = baseurl+"/index.php/user/userprofile";
                          parent.$.fn.colorbox.close();
                          self.parent.location=target;
                      </script>
            <?php
              Yii::app()->user->setFlash('colorsusseces', $csv[168]);  
            }
            $this->render('editprofile',array('user'=>$user));
        }
        
        public function actionsetting()
	{
            $userid = Yii::app()->user->getID();
            $user   = User::model()->find("id = '$userid'"); 
            //echo "<pre>";print_r($user['password']);exit;
            if(isset($_POST['oldpasword']) && $_POST['oldpasword']!='')
            { 
                $old = $_POST['oldpasword'];
                $new = $_POST['newpasword'];
                if($user['password']==md5($old))
                {
                    $user['password']= md5($new);                    
                    $user->save(false);
                    Yii::app()->user->setFlash('onregister', "Your password is changed.");
                }
                else
                {
                    Yii::app()->user->setFlash('onregister', "Please fill correctly your old password.");
                }
                
                
            }
            $this->render('setting');
        }
        

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{  
            $userid = Yii::app()->user->getID();
            $user   = User::model()->find("id = '$userid'"); 
            $user['searchlist']='';
            $user->save(false);
            Yii::app()->user->logout(); 
            $this->redirect(Yii::app()->params['globelUrl'].'/user/home');
	}  
        
        public function actioncrew()
        {
            $workingdir = Yii::app()->params['workingdir'];   
            $csv = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));
            $userid = Yii::app()->user->getID();
            
            if(Yii::app()->request->getRequestType() == 'POST')
            {
                    $crewid = $_POST['User']['crewid'];
                    $model  = new Crew;
                    $model->otheruser_id = $crewid;
                    $model->user_id      = $userid;
                    $model->created      = DateHelper::getCurrentSQLDateTime();
                    $model->modified     = DateHelper::getCurrentSQLDateTime();
                    $model->save(false);
                    Yii::app()->user->setFlash('colorsusseces', $csv[73]);
                    $this->redirect("userprofile?userid=".$crewid);
                    exit;
            }
        }
        
        public function actionremovecrew()
        {
            $userid = Yii::app()->user->getID();
            if(Yii::app()->request->getRequestType() == 'POST')
            {
                $crewid = $_POST['remove'];
                $crew   = Crew::model()->find("otheruser_id = '$crewid' AND user_id = '$userid' ");                
                $crew->delete();
                $result =1;
                echo json_encode($result); 
                exit;
           }
        }
        public function actionmycrew()
        {
            $this->layout = 'authrized';
            $userid   = Yii::app()->user->getID();
            $criteria = new CDbCriteria();
            $criteria->condition = "user_id = $userid";
           
            $count    = Crew::model()->count($criteria); 
            $pages    = new CPagination($count);
            $pages->pageSize = 5;
            $pages->applyLimit($criteria);
            $user  = Crew::model()->with('crew')->findAll($criteria);
            
            $this->render('mycrew',array('user'=>$user,'count'=>$count,'pages' => $pages));
            exit;
        }
        
        public function actionlistlogin($listid=null)
        { 
            $this->layout = 'plane';
            $this->render('listlogin');
        }
        
        public function actionsearch()
        { 
            if(isset($_GET['search']) && $_GET['search']!='')
            { 
             $keyword    = $_GET['search'];
             $criteria   = new CDbCriteria();
             $criteria->addSearchCondition('firstname',"$keyword",true,'OR'); 
             $criteria->addSearchCondition('lastname',"$keyword",true,'OR');
             $count      = User::model()->count($criteria);  
             $pages      = new CPagination($count);
             $pages->pageSize = 5 ;
             $pages->applyLimit($criteria);
             $searches        = User::model()->findAll($criteria); 
             //echo "<pre>";print_r($searches);exit;
             $this->render('search',array('searches'=>$searches,'keyword'=>$keyword,'pages' => $pages)); 
             exit;
            }
             $this->render('search');
        }
        
        public function actionuserinterest()
        {
            $string      = $_POST['string'];
            $arraystring = $_POST['arraystring'];
            $array       = explode(",",$arraystring);

            $sql         = "SELECT * FROM tbl_interest WHERE interest LIKE '%$string%' ORDER interest ASC";
            $connection  = Yii::app()->db;
            $command     = $connection->createCommand($sql);
            $searches    = $command->queryAll();
            $time        = time();

            if(!empty($searches))
            {
                $html = '';
                $html.="<ul  class='interest_div_ul' id='userselectinterest'>";
                for($i=0;$i<count($searches);$i++)
                {
                    $value = trim($searches[$i]['interest']);
                    if(!in_array($value, $array))
                    {
                        $id    = $searches[$i]['id'];
                        $html.= "
                        <li class='interest_div_li' id='li$id.$time' onmouseover='nameautofilover_interest(\"li$id.$time\")' onmouseout='nameautofilout_interest(\"li$id.$time\")' onclick='interstsuggestion(this)'>$value</li>
                        ";
                    }
                }
                $html.="</ul>";
                echo $html;
                exit;
            }
            else
            {
                echo '1';
                exit;
            }
        }
        
        public function actionuserInterestNew($term = null)
        {
            if ($term === null)
            {
                echo "[]";
                exit;
            }
            
            $term = trim($term);
            if ($term == "")
            {
            	echo "[]";
            	exit;
            }
            
            $interests = Interest::model()->findAll(array(
                "condition" => "interest RLIKE :term",
                "params" => array(":term" => RegexHelper::regex($term)),
                "order" => "interest ASC",
                "limit" => 30
            ));

            if (count($interests) == 0)
            {
                echo "[]";
                exit;
            }
            
            $resp = array();
            foreach ($interests as $interest)
            {
                array_push($resp, array(
                    "id" => $interest["id"],
                    "name" => $interest["interest"],
                    "value" => $interest["interest"]
                ));
            }
            
            echo json_encode($resp);
            exit;
       }
       
       private function formatUsersJson($users)
       {
           if (count($users) == 0)
           {
               echo "[]";
               exit;
           }

           $baseurl = Yii::app()->request->baseUrl;
           $resp = array();

           foreach ($users as $user)
           {
               $src = "/images/";
               if ($user->image != "")
               {
                   $src = "/images/profile/user_thumbs/listcreation/";
                   $image = $user->image;
               }
               elseif ($user->gender == 'male' || $user->gender == 'Male')
               {
                   $image = "defaultboy.jpg";
               }
               elseif ($user->gender == 'female' || $user->gender == 'Female')
               {
                   $image = "defaultgirl.jpg";
               }
               else
               {
                   $image = "default.jpg";
               }
                
               array_push($resp, array(
                   "id" => $user->id,
                   "name" => $user->getFullName(),
                   "value" => ImageHelper::imageShow($baseurl.$src,$image,24,18,array('class' => 'user-sumbnail')) . $user->getFullName()
               ));
           }
            
           echo json_encode($resp);
       }
       
       public function actionusers($term = null)
       {
           if ($term === null)
           {
               echo "[]";
               exit;
           }
           
           $term = trim($term);
           if ($term == "")
           {
               echo "[]";
           	   exit;
           }
            
           $users = User::model()->findAll(array(
                "condition" => "CONCAT_WS(' ', firstname, lastname) RLIKE :term",
                "params" => array(":term" => RegexHelper::regex($term)),
                "order" => "firstname ASC, lastname ASC",
                "limit" => 30
           ));
    
           $this->formatUsersJson($users);
           exit;
       }
       
       public function actionfriends($term = null)
       {
           if ($term === null)
           {
               echo "[]";
               exit;
           }
           
           $friendSearch = FriendHelper::getMyFriendsIdsStr();           
           $term = trim($term);
           if ($term == "" || $friendSearch == "")
           {
               echo "[]";
               exit;
           }
                       
           $friends = User::model()->findAll(array(
       			"condition" => "id IN (" . $friendSearch . ")
                    AND CONCAT_WS(' ', firstname, lastname) RLIKE :term",
       			"params" => array(
                    ":term" => RegexHelper::regex($term)
                ),
       			"order" => "firstname ASC, lastname ASC",
       			"limit" => 30
       	  ));
    
       	  $this->formatUsersJson($friends);
       	  exit;
       }
       
       public function actionfriendsWithoutList($listId,$term = null)
       {
           if ($term === null)
           {
               echo "[]";
               exit;
           }
           
           $friendSearch = FriendHelper::getMyFriendsIdsStr();           
           $term = trim($term);
           if ($term == "" || $friendSearch == "")
           {
               echo "[]";
               exit;
           }
                       
           $friends = User::model()->findAll(array(
       			"condition" => "id IN (" . $friendSearch . ")
                    AND CONCAT_WS(' ', firstname, lastname) RLIKE :term",
       			"params" => array(
                    ":term" => RegexHelper::regex($term)
                ),
       			"order" => "firstname ASC, lastname ASC",
       			"limit" => 30
       	   ));
           
           $takensList = ListTaken::model()->findAll(array(
                "select" => "user_id",
                "condition" => "list_id = :list_id",
                "params" => array(":list_id" => $listId)
           ));
           
           if (!empty($takensList)) {
               $takens = array();
               foreach ($takensList as $taken) {
                   $takens[] = $taken['user_id'];
               }
               
               foreach ($friends as $key=>$friend) {
                   if (in_array($friend->id,$takens)) {
                       unset($friends[$key]);
                   }
               }
           }
           
       	   $this->formatUsersJson($friends);
       	   exit;
       }
        
       public function actionsearchinterestlist()
        {
           $string      = $_POST['string'];  
           $baseurl     = Yii::app()->request->baseUrl;
           $sql         = "SELECT * FROM tbl_interest WHERE interest LIKE '%$string%' ORDER interest ASC";
           $connection  = Yii::app()->db;
           $command     = $connection->createCommand($sql);
           $searches    = $command->queryAll();
           $time        = time();
           
           if(!empty($searches))
            { 
                  $html = '';      
                   //echo $_POST['company'];exit;
                   $html.="<div class='interestapply'>";
                   if(isset($_POST['company'])){
                   $html.= "<input type='text'  id='serchint_com'  style='width: 93px;' name='searchinterst' value='$string'>";
                   }
                   else{
                       $html.= "<input type='text'  id='serchint'  style='width: 93px;' name='searchinterst' value='$string'>";
                   }
                   $html.= "<span class='searchspan' onclick='searchinterest(\"$baseurl\")'>search</span>";
                   $html.="</div>";
                   
                   $html.="<div class='selectallcheckbox'>";
                   if(isset($_POST['company'])){
                   $html.= "<input type='checkbox'  id='selectall_com'   name='selectall_com' value='' onclick='selectAllCheckBox_com(this)' class='checkbox_show_select'>";
                   $html.= "<span id='selectallspan_com'>Select all</span>";
                   }
                   else{
                       $html.= "<input type='checkbox'  id='selectall'   name='selectall' value='' onclick='selectAllCheckBox(this)' class='checkbox_show_select' >";
                       $html.= "<span id='selectallspan'>Select all</span>";
                   }
                   
                   $html.="</div>";
                   $html.="<div class='div_scroll'>";        
                     for($i=0;$i<count($searches);$i++)
                     {
                         $value = trim($searches[$i]['interest']);                            
                            $html.="<div class='interest_div_CHECK'>";
                            if(isset($_POST['company'])){
                                $html.= "<input type='checkbox' class='checkbox_show_com' id='checkbox$value'  value='$value' name='availablecheckbox_com'>";
                            }
                            else{
                                $html.= "<input type='checkbox' class='checkbox_show' id='checkbox$value'  value='$value' name='availablecheckbox'>";
                            }
                            $html.= "<span class='interest_span_div'>$value</span>";
                            $html.="</div>";                            
                     }
                  $html.="</div>";   
                  $html.="<div class='interestapply'>";                   
                   if($_POST['applyinterest'] && $_POST['applyinterest'] == 'apply'){
                    $html.= "<span class='cursorpointer applyinterest' onclick='applyinterest_myprofile(\"$baseurl\")'>Apply</span>";
                  }
                  else{
                    $html.= "<span class='cursorpointer applyinterest' onclick='applyinterest()'>Apply</span>";
                  }
                  $html.="</div>";
                 
                 echo $html;
                 exit;
            }
            else
            {
                   $html = '';      
                   
                   $html.="<div class='interestapply'>";
                   if(isset($_POST['company'])){
                   $html.= "<input type='text'  id='serchint_com'  style='width: 93px;' name='searchinterst' value='$string'>";
                   }
                   else{
                       $html.= "<input type='text'  id='serchint'  style='width: 93px;' name='searchinterst' value='$string'>";
                   }
                   $html.= "<span class='searchspan' onclick='searchinterest(\"$baseurl\")'>search</span>";
                   $html.="</div>";
                   
                   $html.="<div class='searchspan_listtypr'>";                   
                   $html.= "<span class='interest_span_div'>No result</span>";
                   $html.="</div>";
                   
                   echo $html;
                 exit;
            }
        }    
        
        public function actionforgotpassword()
        {
            $workingdir = Yii::app()->params['workingdir'];
            $csv = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));
            $this->layout = 'plane';
             
            if(isset($_POST['email']) && $_POST['email']!='')
            {
                $emailid = $_POST['email'];
                $model = User::model()->find("email = '$emailid'");
                $name  = $model['firstname']. ' '.$model['lastname'];
                if($model)
                {
                    $password = rand(10000,51500);

                    $recipientemail = $emailid;
                    $subject = 'Password details - wayoworld.com';
                    $message = ImageHelper::imageShow(Yii::app()->params['siteurl'].'/images/newimages/', "header_logo_freworld-send2.png", 
                                                      0, 0, array('style' => 'height:180px;width:160px;')) . "
                    <br/><br/>
                    <div style='font-family:Arial;font-size: 20pt; color: #333333;'>
                    </div>
                    <div style='padding: 16px 0 18px;'>
                    Dear $name,<br/>
                    You can login to the website using the password mentioned below. You can change it after signing in to the site  <br/>
                    <span style='font-family:Arial;font-size: 13pt; color: #333333;'> Your Password : $password</span> <br/>
                    You can login to the site by clicking on this link <br/>
                    Login URL: <b> ".Yii::app()->params['siteurl']."<br/><br/>
                                 Best Regards,<br/>
                                 Wayo Team
                                 </div>" ;

                    $res_send = MailHelper::sendEmail($recipientemail, "noreply@wayoworld.com", $subject, $message);
                    if ($res_send)
                    {
                        $model->password = md5($password);
                        $res_save = $model->save(false);
                        if ($res_save)
                        {
                            $arry = array('id'=>'colorwhite',"result"=>$csv[43]);
                            echo json_encode($arry);
                            exit;
                        }
                    }
                    
                    $arry = array('id'=>'colorerror',"result"=>$csv[48]);
                    echo json_encode($arry);
                    exit;                    
                }
                else
                {
                    $arry = array('id'=>'colorerror',"result"=>$csv[53]);
                    echo json_encode($arry);
                    exit;
                }
            }
            $this->render('forgotpassword');
        }
        
        public function actiongroup()
        {
            $this->layout = 'authrized';
            $userid       = Yii   ::app()->user->getID();
            $usergroup    = Group ::model()->findAll("user_id=$userid"); 
            if(empty($usergroup))
            {
                $othergroup  = GroupMembers::model()->findAll("otheruser_id=$userid"); 
                $this->render('group',array('usergroup'=>$usergroup,'othergroup'=>$othergroup));
                exit;
            }            
            $this->render('group',array('usergroup'=>$usergroup));
        }
        
        public function actioncreategroup()
        {
            $this->layout = 'authrized';
            $userid       = Yii::app()->user->getID();
            $this->layout = 'plane';
            
            if(isset($_POST['User']) && $_POST['User']['groupname']!='')
            {
                $name             = $_POST['User']['groupname'];
                $group            = New Group;
                $group->groupname = $name;
                $group->user_id   = $userid;
                $group->type      = $_POST['User']['type'];
                $group->created   = date('Y-m-d');
                $group->modified  = date('Y-m-d');
                $group->save(false);
                ?>
                     <script type="text/javascript">
                        parent.tb_remove() ; 
                        window.parent.location.href = "<?php echo Yii::app()->params['siteurl']?>/index.php/user/group";
                    </script>
               <?php 
                
            }
            $this->render('creategroup');
        }
        
        public function actionseegroup($groupid=null)
        {
            $this->layout = 'authrized';
            $group    = Group::model()->find("id = $groupid");
            $groupmem = GroupMembers::model()->findAll("group_id = $groupid");
            //echo "<pre>";print_r($group);exit;
            
            $this->render('seegroup',array('group'=>$group,'groupmem'=>$groupmem));
        }
        
        public function actionaddmembers($groupid=null)        
        {
            $workingdir   = Yii::app()->params['workingdir'];   
            $csv          = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));
            $this->layout = 'authrized';
            $baseurl  = Yii::app()->request->baseUrl;
            $userid   = Yii::app()->user->getID(); 
            $user     = User::model()->find("id = '$userid'");
            $name     = $user['firstname'].' '.$user['lastname'];
            
            $members  = Crew::model()->findAll("user_id = '$userid'");
            $group    = Group::model()->find("id = '$groupid'");
            //echo "<pre>";print_r($groupid);exit;
            if(isset($_POST['checkbox']) && $_POST['checkbox']!='')
            {
                $gpname   = $group['groupname'];
                $accept   = "<a href=$baseurl/index.php/user/acceptrequest?groupid=$groupid>Accept</a>";
                $reject   = "<a href=$baseurl/index.php/user/rejectrequest>Reject</a>";
                $nameurl  = "<a href=$baseurl/index.php/user/userprofile?userid=$userid>$name</a>";
                
                $count = count($_POST['checkbox']);
                for($i=0;$i<$count;$i++)
                {
                    $otherid  = $_POST['checkbox'][$i];
                    $noti     = Notification::model()->find("myuser_id = '$userid' AND user_id = '$otherid' AND group_id = '$groupid'");
                    if(empty($noti))
                    {
                        $notification = new Notification; 
                        $notification->user_id            = $otherid;
                        $notification->myuser_id          = $userid;
                        $notification->group_id           = $groupid;
                        $notification->message            = $nameurl.' '."wants to add you in".' '.$gpname .' &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;'.$accept .' &nbsp;&nbsp;  '.$reject;                            
                        $notification->read_notification  = 0;
                        $notification->created            = DateHelper::getCurrentSQLDateTime();      
                        $notification->modified           = DateHelper::getCurrentSQLDateTime();  
                        $notification->save(false);
                    }
                }
                Yii::app()->user->setFlash('colorsusseces',$csv[78]);
                //$this->redirect(array('seegroup','groupid'=>$groupid));
               
            }
            $this->render('addmembers',array('members'=>$members));
        }
        
        public function actionleavemember($groupid=null)        
        { 
            $userid = Yii::app()->user->getID();
            $group  = GroupMembers::model()->find("group_id = $groupid AND otheruser_id = $userid");
            $group->delete();
            $this->redirect('group');            
        }
        
        public function actionacceptrequest($groupid=null)        
        { 
            $baseurl   = Yii::app()->request->baseUrl;
            $userid    = Yii::app()->user->getID();              
            $user      = User::model()->find("id = '$userid'");
            $name      = $user['firstname'].' '.$user['lastname'];
            $group     = Group::model()->find("id = '$groupid'");
            $noti      = Notification::model()->find("user_id = '$userid'");
            $gpcreater = $noti['myuser_id'];
           
            $mymember  = GroupMembers::model()->find("otheruser_id = '$gpcreater' AND group_id = '$groupid'");
           
            if(empty($mymember))
                {
                    $gpmember               = new GroupMembers;
                    $gpmember->group_id     = $groupid;
                    $gpmember->user_id      = $gpcreater;
                    $gpmember->otheruser_id = $gpcreater;
                    $gpmember->created      = date('Y-m-d');
                    $gpmember->modified     = date('Y-m-d');
                    $gpmember->save(false);
                }
            
                
            $gpmember               = new GroupMembers;
            $gpmember->group_id     = $groupid;
            $gpmember->user_id      = $gpcreater;
            $gpmember->otheruser_id = $userid;
            $gpmember->created      = date('Y-m-d');
            $gpmember->modified     = date('Y-m-d');
            $gpmember->save(false);            
            
                
            $gpname   = $group['groupname'];
            $nameurl  = "<a href=$baseurl/index.php/user/userprofile?userid=$userid>$name</a>";
            $notification = new Notification; 
            $notification->user_id            = $noti['myuser_id'];
            $notification->message            = $nameurl.' '."joined".' '.$gpname ;                            
            $notification->read_notification  = 0;
            $notification->created            = DateHelper::getCurrentSQLDateTime();      
            $notification->modified           = DateHelper::getCurrentSQLDateTime();      
            $notification->save(false);
            
            $this->redirect(array('seegroup','groupid'=>$groupid));
            
        }
            
            public function actionrejectrequest($groupid=null)        
            { 
                $baseurl   = Yii::app()->request->baseUrl;
                $userid    = Yii::app()->user->getID();              
                $user      = User::model()->find("id = '$userid'");
                $name      = $user['firstname'].' '.$user['lastname'];                
                $noti      = Notification::model()->find("user_id = '$userid'");
                
                $nameurl  = "<a href=$baseurl/index.php/user/userprofile?userid=$userid>$name</a>";
                
                $notification = new Notification; 
                $notification->user_id            = $noti['myuser_id'];
                $notification->message            = $nameurl.' '."reject the group invitation" ;                            
                $notification->read_notification  = 0;
                $notification->created            = DateHelper::getCurrentSQLDateTime();      
                $notification->modified           = DateHelper::getCurrentSQLDateTime();      
                $notification->save(false);
                $this->redirect(array('list/mylist'));
            }
            
            
            
            /**************Wayo world ***********************/
            
            
            public function sortbydate($listid)
	{
            
            if(isset($listid))
            {
		$model=ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x AND created = :y', 'params'=>array(':x'=>1,':y'=>$listid)));
            }
	    if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		
		return $model;
        }
        
        public function sortbytopic($listid)
	{
            
            if(isset($listid))
            {
		$model=ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x AND interest_id = :y', 'params'=>array(':x'=>1,':y'=>$listid)));
            }
	    if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		
		return $model;
        }
        
        public function actionofficialtoday()
            {
                $this->layout = 'authrized';                
                $userid       = Yii::app()->user->getID();            
                $takenlist    = ListTaken::model()->findAll("user_id = '$userid'");
                $counttaken   = count($takenlist); 

                
                $allpoll      = Poll::model()->findAll(array('order'=>'id DESC','limit'=>'7','condition'=>'user_id !=:x', 'params'=>array(':x'=>$userid)));
                $data         = ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x', 'params'=>array(':x'=>1)));

                //echo "<pre>";print_r(count($data));exit;
                if(isset($_POST['User']) && $_POST['User']['birthdate']!='')
                {
                    
                    $sortdte  = $_POST['User']['birthdate'];
                    $data     = $this->sortbydate($sortdte);
                    //$data     = ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x AND created = :y', 'params'=>array(':x'=>1,':y'=>$sortdte)));

                }
                elseif(isset($_POST['Interest']) && $_POST['Interest']['user']!='')
                {
                    $sorttopic = $_POST['Interest']['user'];
                    $data      = $this->sortbytopic($sorttopic);
                    //$data      = ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x AND interest_id = :y', 'params'=>array(':x'=>1,':y'=>$sorttopic)));
                }
                $countlist    = count($data);
                
                $interest     = Interest::model()->findAll(array("order" => "interest ASC"));
                $this->render('officialtoday',array('interest'=>$interest,'counttaken'=>$counttaken,'takenlist'=>$takenlist,'data'=>$data,
                              'countlist'=>$countlist,'allpoll'=>$allpoll));
            }
            
            
        public function actionupload()
        {  
                Yii::import("ext.EAjaxUpload.qqFileUploader");

                $folder='images/profile/';// folder for uploaded files
                $allowedExtensions = array("jpg","jpeg","gif","png");//array("jpg","jpeg","gif","exe","mov" and etc...
                $sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
                $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload($folder);
                $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                
                $fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
                $fileName=$result['filename'];//GETTING FILE NAME

                echo $return;// it's array
        }
        
        private function sendConfirmationEmail()
        {
            $password = $_POST['password'];
            $recipientemail = $_POST['email'];
            $subject  = 'Login details - wayoworld.com';
            $message = ImageHelper::imageShow(Yii::app()->params['siteurl'].'/images/newimages/', "header_logo_freworld-send2.png") . " 
                <br/><br/>
                <div>
                Hey there, <br/><br/>
                Welcome to Wayo!  <br/><br/>
                Please keep this information safe as it contains your username and password. <br/><br/>
                Your Membership Info:<br/>
                U : <b> $recipientemail </b><br/>
                P : <b> $password </b><br/>
                Login URL: <b> " . Yii::app()->params['siteurl'] . "/index.php/user/login </b><br/><br/>
                                                         
                                Thank you for choosing Wayo.<br/><br/>

                                Sincerely, <br/>
                                Team Wayo

               </div>";
             
            MailHelper::sendEmail($recipientemail, "noreply@wayoworld.com", $subject, $message);             
        }
            
        public function actionhome()
        {
            $baseurl = Yii::app()->request->baseUrl;

            $workingdir   = Yii::app()->params['workingdir'];
            $csv          = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));

            $this->layout = 'guestuser';
            $list_id      = "9c3e792fa5";
            
            $user         = new User;
            $interest     = Interest::model()->findAll(array("order" => "interest ASC"));

            $featurelist = ListForm::model()->findAll(array('order'=>'position', 
                                                             'condition'=>'is_official =1 AND participation = "Everybody" AND page_no != ""',
                                                             'limit'=>10));

            /*if (empty($featurelist))
            {
                $featurelist  = ListForm::model()->findAll(array('order'=>'id DESC','limit'=>10));
            }*/

            $model        = new LoginForm;
            // collect user input data
            $userid = Yii::app()->user->getID();
            $email = null;
            
            if (stripos(Yii::app()->params['globelUrl'],'dev.') !== false) {
                $httpsUrl = Yii::app()->params['globelUrl'];
            } else {
                $httpsUrl = str_replace('http://', 'https://', Yii::app()->params['globelUrl']);
            }
            if ($userid)
            {
                //$this->redirect(array('preferences/recommended'));
                
                $this->redirect($httpsUrl.'/preferences/officialToday');
            }
            
            if (isset($_POST['LoginForm']))
            {   
                $model->email    = $_POST['LoginForm']['email'];
                //$model->password = $_POST['LoginFormPass']['password'];
                $model->password = $_POST['LoginForm']['password'];
                $email           = $_POST['LoginForm']['email'];
                $userinfo = User::model()->find("email= '$email'");

                if ($userinfo)
                {
                    if($userinfo->status == '0')
                    {
                        $userinfo->status = '1';
                        $userinfo->save(false);
                    }

					/*
                    if($model->validate() && $model->login())
                    {
                        $arry = 0;
                        echo json_encode($arry);
                        exit;
					*/
					// validate user input and redirect to the previous page if valid
					if($model->validate() && $model->login()) {
                        $visit = UserVisits::model()->find(array(
                            'select' => 'user_visit_id, visit_count',
                            'condition' => 'user_id = :user_id',
                            'params' => array(':user_id'=>$userinfo->id),
                        ));

                        if (empty($visit)) {

                            $visit = new UserVisits;
                            $visit->user_id = $userinfo->id;
                            $visit->visit_count = 0;
                            $visit->save();
                        }
                        
                        UserVisits::model()->updateByPk($visit->user_visit_id,
                            array('visit_count' => $visit->visit_count+1));
                        $arry = array('error' => false, 'redirect' => Yii::app()->user->returnUrl);
                    } else {
                        $arry = array('error' => true, 'id' => 'colorerror', 'result' => $csv[18]);
                    }
					
                    echo json_encode($arry);
                    exit;
					
                } else {
				
                    $arry = array('error' => true, 'id' => 'colorerror', 'result' => $csv[88]);
                    echo json_encode($arry);
                    exit;
                }
            }
                                 
            if (isset($_FILES['images']['tmp_name']))
            {
                $size = $_FILES['images']['size'];
                 
                if ($size[0] <= 3145728)
                {
                    $name = $_FILES["images"]["name"];
                     
                    if ($name)
                    {
                        $user->image = html_entity_decode(time().$name[0]);
                        $_SESSION['image'] = $user->image;
                        $result = 1;
                    }

                    if ($result)
                    {
                        $sourcefolder = $_FILES["images"]["tmp_name"];
                        move_uploaded_file($sourcefolder[0], Yii::app()->basePath."/../images/profile/" . $user->image);
                        echo ImageHelper::imageShow($baseurl.'/images/profile/', $user->image, 97, 78, array('class' => 'imageurluolod'));
                        exit;
                    }
                }
            }
             
            if (isset($_POST['User']) && ($_POST['email'] != 'Email') && ($_POST['password'] != 'Password') )
            {
                $resp = recaptcha_check_answer('6LfmV9kSAAAAACWGaiG3U_PHnozYSOvPgm4lg1dl',
                $_SERVER['REMOTE_ADDR'],
                $_POST['recaptcha_challeng_e_field'],
                $_POST['recaptcha_response_field']);

                $user->member    = 'member';
                $user->firstname = $_POST['firstname'];
                $user->lastname  = $_POST['lastname'];
                $user->gender    = $_POST['gender'];
                $user->interest  = $_POST['interest'];
                $user->email     = $_POST['email'];
                $user->password  = md5($_POST['password']);
                $user->emails_notice  = 1;
                $user->emails_weekly  = 1;
                $user->instaview  = 1;
                
                if (isset($_SESSION['image']))
                {
                    $user->image = $_SESSION['image'];
                }

                if ($_POST['occupation'] != 'Company, Industry')
                {
                    $company = explode(',', $_POST['occupation']);
                    $user->company  = $company[0];
                    $user->industry = $company[1];
                }

                $birthmonth      = $_POST['birthmonth'];
                $birthday        = $_POST['birthday'];
                $birthyeare      = $_POST['birthyear'];
                $user->birthdate = $birthmonth.'/'.$birthday.'/'.$birthyeare;

                if ($_POST['education']=='')
                {
                    $user->education_more = $_POST['education_more'];
                }
                elseif(trim($_POST['education'])== 'School, Degree, Year')
                {
    
                }
                else
             {

                   $education = explode(',', $_POST['education']);
                   $user->degreeyear   = $education[2];
                   $user->degree       = $education[1];
                   $user->school       = $education[0];
                }

                if($_POST['city_state']!='City, State/Province, Country')
                {
                    $location = explode(',', $_POST['city_state']);    
                    $user->country   = $location[2];
                    $user->state     = $location[1];
                    $user->city      = $location[0];
                }
               
                $user->created   = DateHelper::getCurrentSQLDateTime();
                $user->modified  = DateHelper::getCurrentSQLDateTime();

                if (!$resp->is_valid) 
                {
                    Yii::app()->user->setFlash('colorerror', $csv[28]);
                    $result = 'Captacha';
                    echo json_encode($result);
                    exit;
                }

                $_SESSION['colorbox'] = 'colorbox';
                
                if ($user->save(false))
                {
                    $visits = new UserVisits;
                    $visits->user_id = $user->id;
                    $visits->visit_count = 0;
                    $visits->save();

                    // bug #3656
                    LoginForm::loginUser($_POST['email'], $_POST['password']);
                    $this->sendConfirmationEmail();

                    $result = 'save';
                    echo json_encode($result);
                    exit;
                }
                else
             {
                    $result = 1;
                    echo json_encode($result);
                    exit;
                }
                $this->redirect('home',array('post'=>$_POST,'user'=>$user,'interest'=>$interest,'csv'=>$csv,));
            }

            if (isset($_POST['Company'])&& ($_POST['email'] != 'Email') && ($_POST['password'] != 'Password') )
            {
                $resp = recaptcha_check_answer('6LfmV9kSAAAAACWGaiG3U_PHnozYSOvPgm4lg1dl',
                $_SERVER['REMOTE_ADDR'],
                $_POST['recaptcha_challeng_e_field'],
                $_POST['recaptcha_response_field']);

                $user->member = 'company user';
         
                if (isset($_SESSION['image']))
                {
                    $user->image = $_SESSION['image'];
                }

                $user->firstname = $_POST['Company'];

                if($_POST['occupation']!='Industry')
                {
                    $company = explode(',', $_POST['occupation']);
                    $user->industry = $company[0];
                }

                $user->street    = $_POST['street'];
                $user->interest = $_POST['interest'];
                $user->email     = $_POST['email'];
                $user->password  = md5($_POST['password']);
                $user->emails_notice  = 1;
                $user->emails_weekly  = 1;
                $user->instaview  = 1;

                if($_POST['city_state']!='City, State/Province, Country')
                {
                    $location = explode(',', $_POST['city_state']);
                    $user->country   = $location[2];
                    $user->state     = $location[1];
                    $user->city      = $location[0];
                }
                
                $user->created   = DateHelper::getCurrentSQLDateTime();
                $user->modified  = DateHelper::getCurrentSQLDateTime();

                if (!$resp->is_valid) 
                {
                    Yii::app()->user->setFlash('colorerror', $csv[28]);
                    $result = 'Captacha';
                    echo json_encode($result);
                    exit;
                }

                if ($user->save(false))
                {
                    $visits = new UserVisits;
                    $visits->user_id = $user->id;
                    $visits->visit_count = 0;
                    $visits->save();

                    // bug #3656
                    LoginForm::loginUser($_POST['email'], $_POST['password']);
                    $this->sendConfirmationEmail();
                    $result = 'save';
                    echo json_encode($result);
                    exit;
                }
                else
             {
                    $result = 1;
                    echo json_encode($result);
                    exit;
               }
               $this->redirect('home',array('post'=>$_POST,'user'=>$user,'interest'=>$interest,'csv'=>$csv,));
            }
             
            // display the login form
            $this->render('home',array('user'=>$user,'interest'=>$interest,'email'=>$email,'csv'=>$csv,'featurelist'=>$featurelist));
        }
            
        public function actionconfermation()
        {
            $this->layout = 'plane';
            $workingdir   = Yii::app()->params['workingdir'];
            $csv          = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));
            $this->render('confermation',array('message'=>$csv[33]) );
        }
          
        public function actionactivate($email=null)
	{  
            $workingdir   = Yii::app()->params['workingdir'];   
            $csv          = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));  
            $model        = new LoginForm;          
            $this->layout = 'guestuser';
            if(isset($email))
                {
                    $userinfo = User::model()->find("email= '$email'");
                    $userinfo->status = 1;
                    $userinfo->save(false);
                }
                
		if(isset($_POST['LoginForm']))
		{     
			$model->attributes = $_POST['LoginForm'];
                        $email             = $_POST['LoginForm']['email'];
			// validate user input and redirect to the previous page if valid
                        $userinfo = User::model()->find("email= '$email'");
                        //echo "<pre>";print_r($userinfo);exit;
                        if($userinfo['status'] == 1)
                        {
                            if($model->validate() && $model->login())
                            {
                                if (stripos(Yii::app()->params['globelUrl'],'dev.') !== false) {
                                    $httpsUrl = Yii::app()->params['globelUrl'];
                                } else {
                                    $httpsUrl = str_replace('http://', 'https://', Yii::app()->params['globelUrl']);
                                }
                                $this->redirect($httpsUrl.'/preferences/officialToday');
                            }
                            else
                            {
                                Yii::app()->user->setFlash('colorerror', $csv[18]);
                            }
                        }
                        else
                        {
                            Yii::app()->user->setFlash('activateaccount', $csv[13]);
                        }
                       
		}
            
            
            $this->render('activate'); 
            
        }
        
        
        public function actionfacebook()
	{
            $this->layout = 'authrized';
            Yii::import('application.vendors.*');
            require_once('facebook/facebook.php'); 
            # Creating the facebook object
            
            $config = array(
                'appId' => '412800468768960',
                'secret' => 'aac75ccd8188feef28b272f4420c45cc',
              );
            
            $baseurl = Yii::app()->request->baseUrl;
            $userid = Yii::app()->user->getID(); 
            
            # Let's see if we have an active session
            //$friends = $facebook->api('/me/friends');
            //$session = $facebook->getSession();
            $facebook = new Facebook($config);
            $user_id = $facebook->getUser();
            //$access_token = $facebook->getAccessToken();
            //echo "<pre>";print_r($user_id);exit;            
            if(!empty($user_id)) { 
                # Active session, let's try getting the user id (getUser()) and user info (api->('/me')) 
                try{
                    $uid = $facebook->getUser();
                    $user = $facebook->api('/me/friends');
                    $my_information = $facebook->api('/me');
                    //$temp =' https://graph.facebook.com/581913302/picture';
                } catch (Exception $e){die("An error occurred! ".$e->getMessage());}
                
                if(!empty($user)){  
                    # User info ok? Let's print it (Here we will be adding the login and registering routines)
                    
                    $count = count($user['data']);
                    //echo "<pre>";print_r($my_information);exit;
                    for($i=0; $i<$count; $i++)
                    { 
                        $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_facebook')
                                            ->where('friend_facebookid=:value and user_id=:id', array(':value' =>$user['data'][$i]['id'],':id'=>$userid))
                                            ->queryRow();
                        
                        
                    if(empty($usertable))                   
                      {    
                         $model = new FacebookForm;
                         $model->user_id            = $userid;
                         $model->facebook_id        = $my_information['id'];
                         $model->friend_facebookid  = $user['data'][$i]['id']; 
                         $model->friend_name        = $user['data'][$i]['name']; 
                         $facebookimg               = 'https://graph.facebook.com/'.$user['data'][$i]['id'].'/picture'; 
                         $model->friend_facebookimg = $facebookimg;
                         $model->invite             = 'invite member';                         
                         $model->created            = date('Y-m-d');
                         $model->modified           = date('Y-m-d');                         
                         $model->save(false);                         
                      }   
                    } 
                    //echo "<pre>";print_r($user['data']);exit;
                    $this->redirect('facebooklisting');  
                    //echo "<pre>";print_r($my_information['id']);
                } else {
                    # For testing purposes, if there was an error, let's kill the script
                    die("There was an error.");
                }
            } else { 
                # There's no active session, let's generate one
                 $login_url = $facebook->getLoginUrl();                 
                 header("Location: ".$login_url);
            }
            //$this->redirect('facebook');  
            
        }

          public function actionfacebooklisting()
	{             
                $this->layout = 'authrized';
                $id   = Yii::app()->user->getID(); 
                $user = User::model()->findByPk($id);
                $face = Interest::model()->find();
                $_SESSION['name'] = $user['firstname'].' '.$user['lastname'];
                 
                $user_facebook= FacebookForm::model()->findAllByAttributes(
                    array(),
                    $condition  = 'user_id = :id',
                    $params     = array(
                            ':id' => $id, 
                            
                    )
                ); 
               
                $count = count($user_facebook);
                 
                for($i=0; $i<$count; $i++)
                {  
                    $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_user')
                                            ->where('facebook_id=:value', array(':value' =>$user_facebook[$i]->friend_facebookid))
                                            ->queryRow();                   
                    if(!empty($usertable))
                      {  
                         $idtype =$user_facebook[$i]->friend_facebookid;
                         $update  = FacebookForm::model()->deleteAll("friend_facebookid = '$idtype'");                         
                     }
                }
               
                
		/*$model=new FacebookForm('search'); 
                $q = Yii::app()->request->getParam('friend_name');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FacebookForm']))
			$model->attributes=$_GET['FacebookForm'];  */     
                
                 $criteria   = new CDbCriteria();
                 $criteria->condition = "user_id = $id";
                 $count      = FacebookForm::model()->count($criteria);  
                 //$pages      = new CPagination($count);
                 //$pages->pageSize = 20 ;
                 //$pages->applyLimit($criteria);
                 $model = FacebookForm::model()->findAll($criteria);                 

		$this->render('facebooklisting',array(
			'model'=>$model, 
                        //'pages' => $pages,
                        'face'=>$face
		));
	}
        
        
        public function actiontwitter_oauth()
	{    
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
            $userid = Yii::app()->user->getID();            
            
            if(!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){  
                // We've got everything we need  
               
            } 
            elseif(isset($_GET['denied']))
            {
                $this->redirect('twitterlisting');
            }
            else {  
                // Something's missing, go back to twitter
                $baseurl = Yii::app()->request->baseUrl;
                
                header("Location: " . Yii::app()->params['siteurl'] . "/index.php/user/twitterlisting");
            } 
            // TwitterOAuth instance, with two new parameters we got in twitter_login.php
            $twitteroauth = new TwitterOAuth('kQ46iHFE7DFXhvF8pV5bA', '90DwZvMJZbjkiRMz6CfdMYoVD1tytQu9wYSyefxbHbc', $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            // Let's request the access token
            $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            // Save it in a session var
            $_SESSION['access_token'] = $access_token;
            // Let's get the user's info
            $user_info = $twitteroauth->get('account/verify_credentials');

            $user_followers = $twitteroauth->get('statuses/followers');            
            
            //echo "<pre>";print_r($user_followers);exit;
            $count_follwer = count($user_followers);
            
                 for($i=0; $i<$count_follwer; $i++)
                 {                      
                     
                     $user = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_twitter')
                                            ->where('friend_twitterid=:value and user_id=:id', array(':value' =>$user_followers[$i]->id,':id'=>$userid))
                                            ->queryRow();
                     if(empty($user))
                      { 
                         $model = new Twitter;
                         $model->user_id           = $userid;
                         $model->twitter_id        = $user_info->id;
                         $model->friend_twitterid  = $user_followers[$i]->id;
                         $model->friend_name       = $user_followers[$i]->screen_name;
                         $model->friend_twitteimg  = $user_followers[$i]->profile_image_url;
                         $model->image             = Yii::app()->params['siteurl'] . '/images/twitter.png';
                         $model->created           = date('Y-m-d');
                         $model->modified          = date('Y-m-d');
                         $model->save(false); 
                     }
                 }                  
          
            $this->redirect('twitterlisting');            
        }
        
        
        public function actiontwitter()
	{
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
  
             // The TwitterOAuth instance  
            $twitteroauth = new TwitterOAuth('kQ46iHFE7DFXhvF8pV5bA', '90DwZvMJZbjkiRMz6CfdMYoVD1tytQu9wYSyefxbHbc');  
            // Requesting authentication tokens, the parameter is the URL we will be redirected to  
            $request_token = $twitteroauth->getRequestToken(Yii::app()->params['siteurl'] . '/index.php/user/twitter_oauth');  

            // Saving them into the session  
            $_SESSION['oauth_token'] = $request_token['oauth_token'];  
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];  

            // If everything goes well..  
            if($twitteroauth->http_code==200){  
                // Let's generate the URL and redirect  
                $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']); 
                header('Location: '. $url); 
            } else { 
                // It's a bad idea to kill the script, but we've got to know when there's an error.  
                die('Something wrong happened.');  
            } 
            $this->render('twitter');
        }
       
        
        public function actiontwitterlisting()
	{              
                $this->layout = 'authrized';
                $id = Yii::app()->user->getID(); 
                $user=User::model()->findByPk($id);
                $_SESSION['name'] = $user['firstname'].' '.$user['lastname']; 
                
                
                $user_twitter = Twitter::model()->findAllByAttributes(
                    array(),
                    $condition  = 'user_id = :id',
                    $params     = array(
                            ':id' => $id, 
                            
                    )
                ); 
                $count = count($user_twitter);
                
                for($i=0; $i<$count; $i++)
                {  
                    $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_user')
                                            ->where('twitter_id=:value', array(':value' =>$user_twitter[$i]->friend_twitterid))
                                            ->queryRow();
                    if(!empty($usertable))
                      {  
                         $idtype =$user_twitter[$i]->friend_twitterid;
                         $update  = Twitter::model()->deleteAll("friend_twitterid = '$idtype'");
                     }
                }
               
                /*$q = Yii::app()->request->getParam('friend_name');     
		$model=new Twitter('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Twitter']))
			$model->attributes=$_GET['Twitter'];*/
                
                
                 $criteria   = new CDbCriteria();
                 $criteria->condition = "user_id = $id";
                 $count      = Twitter::model()->count($criteria);  
                 //$pages      = new CPagination($count);
                 //$pages->pageSize = 20 ;
                 //$pages->applyLimit($criteria);
                 $model = Twitter::model()->findAll($criteria); 

		$this->render('twitterlisting',array(
			'model'=>$model,                        
                        //'pages' => $pages,
		));
	}
        
        
        public function actiontwitter_invitation_oauth($user=null)
	{   
            $workingdir   = Yii::app()->params['workingdir'];   
            $csv          = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));    
            
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
            $baseurl       = Yii::app()->request->baseUrl;  
            $userid        = Yii::app()->user->getID();            
            $usertwitterid = $_SESSION['usertwitterid'];
            $face          = Interest::model()->find();
            $message       = $face['twitter_message']; 
            //$message       = "<a href='$baseurl/index.php'>$messages</a>";            
            if(!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){  
                // We've got everything we need  
               
            } 
            elseif(isset($_GET['denied']))
            {
                $this->redirect('twitterlisting');
            }
            else {  
                // Something's missing, go back to twitter
                $baseurl = Yii::app()->request->baseUrl;               
                header("Location: " . Yii::app()->params['siteurl'] . "/index.php/user/twitterinvitation");
            } 
            
            // TwitterOAuth instance, with two new parameters we got in twitter_login.php 757023578
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk', $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            // Let's request the access token
            $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            // Save it in a session var
            $_SESSION['access_token'] = $access_token;
            // Let's get the user's info
            $user_info = $twitteroauth->get('account/verify_credentials');            
            $user_followers = $twitteroauth->post("direct_messages/new.json?text=$message&user=$usertwitterid&"); 
            
            Yii::app()->user->setFlash('invitation',$csv[58]);
            $this->redirect('twitterlisting');            
        }
        
        
        public function actiontwitterinvitation($user=null)
	{
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
  
             // The TwitterOAuth instance   
             $_SESSION['usertwitterid'] = $user;
             
             
             $userid = Yii::app()->user->getID();
                if(isset($user))
                {                     
                    /*$face = Notification::model()->find("twitter_id = '$user'");
                    if(empty($face)){
                    $notification                     = new Notification;
                    $notification->user_id            = $userid;
                    $notification->message            = 'You send an invitation to your twitter follower.';
                    $notification->read_notification  = 1;
                    $notification->notification_type  = 'invitations';
                    $notification->twitter_id         = $user;
                    $notification->created            = DateHelper::getCurrentSQLDateTime();      
                    $notification->modified           = DateHelper::getCurrentSQLDateTime();
                    $notification->save(false);
                    }*/
                }
            
             
             
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk');  
            // Requesting authentication tokens, the parameter is the URL we will be redirected to  
            $request_token = $twitteroauth->getRequestToken(Yii::app()->params['siteurl'] . "/index.php/user/twitter_invitation_oauth");  

            // Saving them into the session  
            $_SESSION['oauth_token'] = $request_token['oauth_token'];  
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];  

            // If everything goes well..  
            if($twitteroauth->http_code==200){  
                // Let's generate the URL and redirect  
                $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']); 
                
                header('Location: '. $url); 
            } else { 
                // It's a bad idea to kill the script, but we've got to know when there's an error.  
                die('Something wrong happened.');  
            } 
            //$this->render('twitterinvitation');
        }
                
        public function actionacceptfrndrequest($userid = null, $notificationid = null)
        {
            $userId = Yii::app()->user->getID();
            $friendId = $userid;
           
            $userfrnd = User::model()->find("id = '$friendId'");
            $fr_name  = $userfrnd['firstname'].' '.$userfrnd['lastname'];
          
            FriendHelper::acceptFriendRequest($userId, $friendId, $notificationid);

            echo $fr_name;
            
            exit;
        }
        
        public function actionrejectfrndrequest($userid = null, $notificationid = null)
        {            
            $user_id = Yii::app()->user->getID();            
            FriendHelper::removeFriendRequest($user_id, $userid, $notificationid);
            
            echo "delete";
            
            exit;
        }
        
        public function actionundoAcceptFriend($notificationid = null)
        {
        	FriendHelper::undoAcceptFriendRequest($notificationid);        
        	echo "ok";        
        	exit;
        }
        
        public function actionundoRejectFriend($notificationid = null)
        {
        	FriendHelper::undoRejectFriendRequest($notificationid);        
        	echo "ok";        
        	exit;
        }
        
        public function actionreadNotifications($tab = null)
        {
            // TODO: check notification date!
            $userid = Yii::app()->user->getID();
            $user = User::model()->findByPk($userid);
            
            $count = 0;
            $notifications = null;
            
            if ($tab == "nInvitationsTab")
            {
                $notifications = $user->friendsNotifications();                
            } else if ($tab == "nListsTab")
            {
            	$notifications = $user->listsNotifications();
            } else if ($tab == "nSystemTab")
            {
            	$notifications = $user->systemNotifications();
            } else if ($tab == "all")
            {
                $notifications = $user->unreadNotifications();
            }
            
            if ($notifications)
            {
                foreach ($notifications as $noti)
                {
                    if ($noti->read_notification == 0)
                    {
                        $noti->read_notification = 1;
                        $noti->save(false);
                        $count++;
                    }
                }
            }
            
            echo $count;
            exit;
        }        
              
        public function actionmyNotifications()
        {
            $this->layout   = 'layout/authrized_user';
            $userid         = Yii::app()->user->getID();
            
            $this->render("notifications", array());
        }
        
        
        public function actionreadmessages()
        {
               $userid         = Yii::app()->user->getID();  
               $messagecount = Message::model()->findAll("receiver_id = '$userid' AND is_read = '0'"); 
                $messagecou = count($messagecount);
                for($i=0;$i<$messagecou;$i++)
                {
                    $messagecount[$i]->is_read = 1;
                    $messagecount[$i]->save(false);
                }
                $this->redirect(Yii::app()->request->urlReferrer);
        }
        
        public function actionaddfavorite($userid=null)
        {
            $myuserid = Yii::app()->user->getID();
            $userfrnd = User::model()->find("id = '$userid'");
            $fr_name  = $userfrnd['firstname'].' '.$userfrnd['lastname'];
            $favorite = new Favorite; 
            $favorite->user_id       = $myuserid;
            $favorite->favorite_id   = $userid;
            $favorite->favorite_name = $fr_name;
            $favorite->created       = date('Y-m-d');
            $favorite->modified      = DateHelper::getCurrentSQLDateTime();
            $favorite->save(false);
            //Yii::app()->user->setFlash('onregister', "$fr_name is added in your favorite list.");
            $this->redirect("dashboard?userid=".$userid);
        }
        
        
         public function actionmyFriends()
         {
             $this->layout   = 'layout/authrized_user';       
             $userid = Yii::app()->user->getID();
             $user = User::model()->find("id = '$userid'");
            
             $myFriends = FriendHelper::findMyFriends();
             if ($user->isCompany()) {
                 $myFriends = FriendHelper::findMyFollowers();
             }
             
             $myCompanies = FriendHelper::findMyFriends("company user");
             
             $this->render('friends', array(
                 'user' => $user, 
                 'pageno' => 0,
                 'pages' => 0,
                 'friends' => $myFriends,
                 'companies' => $myCompanies,
                 '_ajax' => false
           ));
        }
        
        public function actionmyFriendsPagination()
        {
        	$this->layout   = 'layout/ajax';
        	
        	if (isset($_POST["pageno"]))
        	{
        	    $pageno = $_POST["pageno"];
        	    
            	$userid = Yii::app()->user->getID();
            	$user = User::model()->find("id = '$userid'");
            	
            	$myFriends = FriendHelper::findMyFriends($pageno * PAGE_LIMIT_PIOPLE, PAGE_LIMIT_PIOPLE);
            	$this->render('friends', array(
            			'user' => $user,
            			'pageno' => $pageno,
            			'data' => $myFriends,
            			'_ajax' => true
            	));
        	}
        }        
        
        public function actioneditnotification($userid=null)
	    {
            $this->layout = 'plane';
            
            $user = User::model()->find("id = '$userid'");
            
            if(isset($_POST['submit']))
            { 
               
                if(isset($_POST['emailnotification']) && $_POST['emailnotification']=='on')
                { 
                    $user['send_email'] = 'on';
                } 
                else
                {
                    $user['send_email'] = 'off';
                }
               
                if(isset($_POST['takealist']) && $_POST['takealist']=='on' )
                {
                    $user['take_list'] = 'on';
                } 
                else
                {
                    $user['take_list'] = 'off';
                }
                
                if(isset($_POST['youcreated']) && $_POST['youcreated']=='on')
                {
                    $user['you_createdlist'] = 'on';
                } 
                else
                {
                    $user['you_createdlist'] = 'off';
                }
                
                if(isset($_POST['otherstotake']) && $_POST['otherstotake']=='on')
                {
                    $user['you_invitedlist'] = 'on';
                } 
                else
                {
                    $user['you_invitedlist'] = 'off';
                }
                
                if(isset($_POST['listentry']) && $_POST['listentry']=='on')
                {
                    $user['list_entry']  = 'on';
                } 
                else
                {
                    $user['list_entry']   = 'off';
                }
             
                 $user->save(false);   
              ?>
                <script type="text/javascript">
                    parent.$.fn.colorbox.close();
                </script>
              <?php
                
            }            
            $this->render('editnotification',array('user'=>$user,));
        }
        
        
        
        public function processingList($user, $list, $friends_search, $pageno = 0, $limit = 8, $_ajax = false)
        {
            
            $thisUserId = Yii::app()->user->getID();
            $thisFriendIds = FriendHelper::getMyFriendsIds();
            
            if ($user->isCompany()) {
                $friends_search = FriendHelper::getMyFollowersIds();
            }
            
            $offset = $pageno * $limit;
            
            $total = count($list);
            $data  = array();
            if ($offset < $total) {
                $last  = ($limit < 1) ? $total : min($total, $offset + $limit);
                
                for($i = $offset; $i < $last; $i++) {
                    $t = $list[$i];

                    // skip invisible lists in the briefs!
                    if ($t->participation != "Everybody" && $t->user_id != $thisUserId && !in_array($thisUserId,explode(',',$t->participation)))
                    {
                        //$total--;
                        $last = ($limit < 1) ? $total : min($total, $offset + $limit);
                        continue;
                    }
                    
                    $r = $t->getAttributes(array('name', 'id', 'image', 'title', 'user_id', 'interest_id', 'favorite','badge','participation','link_title','link'));
                    
                    /*if (strlen($r['title']) > 50) {
                        $r['title'] = substr($r['title'], 0, 50) . '...';  
                    }*/
                    if (!((strpos($r['image'], 'profile/') === 0) || (strpos($r['image'], 'freeworld/') === 0))) {
                        $r['image'] = 'titleimages/list_thumbs/official/big/' . $r['image'];
                    }
                    
                    $u = User::model()->find('id = '.$r['user_id']);
                    $author = $u->firstname . ' ' . $u->lastname;
                    $r['author'] = strlen($author) < 16 ? $author : $u->firstname;
					if (strlen($r['author']) > 11) {
						$r['author'] = substr($r['author'], 0, 11) . '...';
					}
                    
                    $r['rows'] = $t->opinionRaiting();
                    
                    $r['friends'] = array();
                    if (!empty($friends_search)) {
                        foreach($t->user(array('condition' => 'user.id IN ('.implode(',', $friends_search).')')) as $friend) {
                            $r['friends'][] = $friend->getAttributes(array('id', 'firstname', 'lastname'));
                        }
                    }

                    $r['count_c'] = $t->countTaken(); // contributors count
                    
                    $f = $t->taken(array(
                        'condition'=>'user_id = :user_id',
                        'params' => array(':user_id' => Yii::app()->user->getID())
                    ));
                    //$r['is_taken'] = count($f) > 0 ? 'true' : 'false';
                    //$r['favorite'] = count($f) > 0 ? $f[0]->favorite : 0;
                    $r['is_taken'] = count($f) > 0;
                    $r['favorite'] = count($f) > 0 ? $f[0]->favorite : '0';
                    $r['is_favorite'] = $r['is_taken'] && (intval($f[0]->favorite) > 0);
                    
                    $data[] = $r;
                }
            }
            
            return array(
                '_ajax' => $_ajax,
                'user' => $user,
                'pageno' => $pageno,
                'pages' => ceil($total/$limit) - 1,
                'total' => $total,
                'data' => $data,
            );
        }
        
        public function actionchangeStatus() 
        {
            $this->layout = 'layout/ajax';
            if (isset($_POST["mystatus"]))
            {
                $userid = Yii::app()->user->getID();
                $user = User::model()->find("id = $userid");
                $user["status_text"] = $_POST["mystatus"];
                $user["modified"] = DateHelper::getCurrentSQLDateTime();
                $user->save(false);
                
                echo "Status was saved successfully!";
                exit;
            }
            
            echo "Status text not set!";
            exit;
        }
        
        public function actionupdateSummary()
        {
            $this->layout = 'layout/ajax';
            $this->render('summary', array("_ajax" => true));
        }
        
        public function actiontoogleSummary()
        {
            $_SESSION["dashboardSSPanel"] = $_POST["show"];
            exit;
        }
        
        public function actionsetTimezone()
        {
            if (!empty($_POST["tz"])) {
                $_SESSION["tz"] = $_POST["tz"];
            }
        }
        
        public function actioninviteFriend()
        {   
            $this->layout = 'layout/ajax';
            
            if (!isset($_POST["userid"]))
            {
                echo "Invalid request!";
                exit;
            }
            
            $friendUserId = $_POST["userid"];
            
            $userId = Yii::app()->user->getID();
            $user = User::model()->findByPk($userId);
            
            if ($user->isCompany()
                || $user->id === $friendUserId
                || $user->hasFriend($friendUserId))
            {
                echo "friend";
                exit;
            }
            
            $friendUser = User::model()->findByPk($friendUserId);
            
            $this->render('invite', array(
                'author' => $friendUser,
                'contentSel' => $_POST["contentsel"],
                'isPending' => $user->isPendingFriend($friendUserId)
            ));
        }
        
        public function actiontoggleNotificationsOn($friendid = null)
        {
        	$this->layout = 'layout/ajax';
        	if ($friendid)
        	{
        		$friend = Friend::model()->findByPk($friendid);
        		$val = intval($friend["notifications_on"]);
        		$friend["notifications_on"] = !$val;
        		$friend->save(false);
        
        		echo !$val;
        		exit;
        	}
        
        	echo "Error";
        }

        public function actionMyDashboardUpdateOrder()
        {
            if (isset($_POST['ids']) && is_array($_POST['ids'])) {
                $ids = $_POST['ids'];
            
                $n = count(Yii::app()->user->getModel()->favotiteListSorted()) + 1;
                
                $lists = ListTaken::model()->findall(array(
                    'condition' => 'user_id = :user_id AND list_id IN (' . implode(',', $ids) . ')', 
                    'params' => array(':user_id' => Yii::app()->user->getID())
                ));
                
                foreach ($lists as $list) {
                    $i = array_search($list->list_id, $ids);
                    if ($i !== false) {
                       $list->favorite = $n - $i;
                       $list->save(array('favorite'));
                    }
                }
            }
        }
        
        public function actionMyDashboard()
        {
            $userid = Yii::app()->user->getID();
            $user = User::model()->findByPk($userid);
            
            /*if ($user->isCompany()) {
                $this->redirect('myCreated');
            }*/
            
            $friends_search = FriendHelper::getMyFriendsIds();
            $list  = Yii::app()->user->getModel()->favotiteListSorted();
            
            $help = 0;
            $visit = UserVisits::model()->find(array(
                'select' => 'user_visit_id, visit_count',
                'condition' => 'user_id = :user_id AND visit_count = 0',
                'params' => array(':user_id'=>$userid),
            ));

            if (!empty($visit)) {
                UserVisits::model()->updateByPk($visit->user_visit_id,
                    array('visit_count' => $visit->visit_count+1));
                $help = 1;
            }
            
            $processes = $this->processingList($user, $list, $friends_search);
            $processes['help'] = $help;
				
            $this->layout = 'layout/authrized_user';
            $this->render('mydashboard', $processes);
        }
        
        public function actionMyDashboardPagination()
        {
            $userid = Yii::app()->user->getID();
            $user = User::model()->findByPk($userid);
            
            $pageno = isset($_POST['pageno']) ? $_POST['pageno'] : 0;
            $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : Yii::app()->user->getID();
            $user = User::model()->findByAttributes(array('id' => $user_id));
            $friends_search = FriendHelper::getFriendsIdsByUserId($user_id, 'member');
            $list  = $user->favotiteListSorted();
            
            $this->renderPartial('mydashboard', $this->processingList($user, $list, $friends_search, $pageno, 8, true), false, true);
        }
        
        public function actionMyTaken()
        {
            $userid = Yii::app()->user->getID();
            $user = User::model()->findByPk($userid);
            
            $friends_search = FriendHelper::getMyFriendsIds();
            $list  = Yii::app()->user->getModel()->takenListSorted();
            
            $this->layout = 'layout/authrized_user';
            $this->render('mytaken', $this->processingList($user, $list, $friends_search));
        }        
        
        public function actionMyTakenPagination()
        {
            $userid = Yii::app()->user->getID();
            $user = User::model()->findByPk($userid);
            
            $pageno = isset($_POST['pageno']) ? $_POST['pageno'] : 0;
            $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : Yii::app()->user->getID();
            $user = User::model()->findByAttributes(array('id' => $user_id));
            $friends_search = FriendHelper::getFriendsIdsByUserId($user_id, 'member');
            $list  = $user->takenListSorted();
            
            $this->renderPartial('mytaken', $this->processingList($user, $list, $friends_search, $pageno, 8, true), false, true);
        }

        public function actionMyCreated()
        {
            $userid = Yii::app()->user->getID();
            $user = User::model()->findByPk($userid);
            
            $friends_search = FriendHelper::getMyFriendsIds();
            $list  = Yii::app()->user->getModel()->listSorted();
            
            $help = 0;
            $visit = UserVisits::model()->find(array(
                'select' => 'user_visit_id, visit_count',
                'condition' => 'user_id = :user_id AND visit_count = 0',
                'params' => array(':user_id'=>$userid),
            ));

            if (!empty($visit)) {
                UserVisits::model()->updateByPk($visit->user_visit_id,
                    array('visit_count' => $visit->visit_count+1));
                $help = 1;
            }

            $this->layout = 'layout/authrized_user';
            $processes = $this->processingList($user, $list, $friends_search);
            $processes['help'] = $help;
            $this->render('mycreated', $processes);
        }        
        
        public function actionMyCreatedPagination()
        {
            $userid = Yii::app()->user->getID();
            $user = User::model()->findByPk($userid);
            
            $pageno = isset($_POST['pageno']) ? $_POST['pageno'] : 0;
            $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : Yii::app()->user->getID();
            $user = User::model()->findByAttributes(array('id' => $user_id));
            $friends_search = FriendHelper::getFriendsIdsByUserId($user_id, 'member');
            $list = $user->listSorted();
            
            $this->renderPartial('mycreated', $this->processingList($user, $list, $friends_search, $pageno, 8, true), false, true);
        }

        public  function actionmyProfile($userid=null)
        {
            $this->layout  = 'layout/authrized_user';
            $interest      = Interest::model()->findAll(array("order" => "interest ASC"));
            $flashmsg      = '';
            $notification  = array();

            if(isset($_POST['User']))
            {
                $userid          = Yii::app()->user->getID();
                $user            = User::model()->find("id = $userid");
                $oldname         = $user['firstname'].' '.$user['lastname'];
                $newname         = $_POST['User']['firstname'].' '.$_POST['User']['lastname'];
                $mynotification  = array();
                $mynotification  = Notification::model()->findAll("myuser_id=$userid");

                if($oldname!=$newname)
                {
                    $k=0;
                    foreach ($mynotification as $values)
                    {
                        $values->message = str_replace($oldname,$newname,$values->message);
                        $mynotification[$k]->message = $values->message;
                        $mynotification[$k]->save(false);
                        $k++;
                    }
                }

                $user->member    = $user->member;
                $user->firstname = $_POST['User']['firstname'];
                $user->lastname  = $_POST['User']['lastname'];
                $user->gender    = $_POST['gender'];
                $user->interest  = ltrim($_POST['interest_list'],",");
                $user->email     = $_POST['User']['email'];
                $user->emails_notice     = !empty($_POST['User']['emails_notice']) ? 1 : 0;
                $user->emails_weekly     = !empty($_POST['User']['emails_weekly']) ? 1 : 0;
                if(isset($_SESSION['image'])){
                     $user->image     = $_SESSION['image'];
                     }

                     if($_POST['User']['occupation'] != '')
                     {
                         $company = explode(',', $_POST['User']['occupation']);

                         if(isset($company[0]))
                             $user->company  = $company[0];
                         else
                             $user->company  = '';

                         if(isset($company[1]))
                             $user->industry = $company[1];
                         else
                             $user->industry = '';
                     } else {
                         $user->company  = '';
                         $user->industry = '';
					 }

					 $birthmonth      = $_POST['User']['birthmonth'];
					 $birthday        = $_POST['User']['birthday'];
					 $birthyeare      = $_POST['User']['birthyear'];
					 $user->birthdate = $birthmonth.'/'.$birthday.'/'.$birthyeare;



					 if(isset($_POST['education_list']) && ($_POST['education_list'] !=''))
					 {
					     $user->education_more = $_POST['education_list'];
					 }
					 else
					 {
					     $user->education_more = '';
					 }


					 if(isset ($_POST['User']['education']) && $_POST['User']['education']!='')
					 {
					     $education          = explode(',', $_POST['User']['education']);

					     if(isset($education[2]))
					         $user->degreeyear   = $education[2];
					     else
					         $user->degreeyear   = '';

					     if(isset($education[1]))
					         $user->degree       = $education[1];
					     else
					         $user->degree       = '';

					     if(isset($education[0]))
					         $user->school       = $education[0];
					     else
					         $user->school       = '';

					 }
					 else
					 {
					     $user->degreeyear   = ''; $user->degree       = ''; $user->school       = '';
					 }


					 if($_POST['city_state']!='')
					 {
					     $location = explode(',', $_POST['city_state']);

					     if(isset($location[2]))
					         $user->country   = $location[2];
					     else
					         $user->country   = '';

					     if(isset($location[1]))
					         $user->state     = $location[1];
					     else
					         $user->state     = '';

					     if(isset($location[0]))
					         $user->city      = $location[0];
					     else
					         $user->city      = '';
					 } else {
                         $user->country   = '';
                         $user->state     = '';
                         $user->city      = '';
                     }


					 $user->save(false);

					 PrivacySettingsHelper::setSettings($user->id, isset($_POST['Privacy']) ? $_POST['Privacy'] : array());

					 $flashmsg = "Your changes have been saved";

            }
            if(isset($_GET['userid']))
            {
                $id     = $_GET['userid'];
                $userid = Yii::app()->user->getID();

            }
            else
            {
                $id      = Yii::app()->user->getID();
                $userid  = Yii::app()->user->getID();
            }

            $user = User::model()->find("id = $id");

            $notification = Notification::model()->find(array(
            'condition' => 'myuser_id = :x AND user_id = :y AND notification_type = :k',
            'params' => array(':x' => $id, ':y' => $userid, ':k' => 'friends')
          ));

          $notificationResponce = Notification::model()->find(array(
            'condition' => 'myuser_id = :x AND user_id = :y AND notification_type = :k',
            'params' => array(':x' => $userid, ':y' => $id, ':k' => 'friends')
          ));

          if ($user->isCompany())
          {
              $this->redirect(array('myProfileCompany', 'userid' => $id));
          }

          $this->render('myprofile',array(
            'user'=>$user,
            'interest' => $interest,
            'flashmsg' => $flashmsg,
            'notification' => $notification,
            'notificationResponce' => $notificationResponce));
        }
        
        public  function actionmyProfileCompany($userid=null)
         {
            $this->layout  = 'layout/authrized_user'; 
            $interest      = Interest::model()->findAll(array("order" => "interest ASC"));
            $flashmsg      = '';
            $notification  = array();
            
            if(isset($_POST['User']))
                {
                     $userid          = Yii::app()->user->getID();   
                     $user            = User::model()->find("id = $userid");
                     $oldname         = $user['firstname'];
                     $newname         = $_POST['User']['firstname']; 
                     $mynotification  = array();
                     $mynotification  = Notification::model()->findAll("myuser_id=$userid");
                    
                     if($oldname!=$newname)
                     {    
                         $k=0;
                         foreach ($mynotification as $values)
                         {                   
                             $values->message = str_replace($oldname,$newname,$values->message);
                             $mynotification[$k]->message = $values->message;
                             $mynotification[$k]->save(false);
                             $k++;
                         }
                     }
                        
                     $user->member    = $user->member; 
                     $user->firstname = $_POST['User']['firstname'];                     
                     $user->interest  = ltrim($_POST['interest_list'],","); 
                     $user->email     = $_POST['User']['email']; 
                     $user->street    = $_POST['street'];  
                     $user->emails_notice     = !empty($_POST['User']['emails_notice']) ? 1 : 0;
                     $user->emails_weekly     = !empty($_POST['User']['emails_weekly']) ? 1 : 0;
                     
                     if(isset($_SESSION['image'])){
                     $user->image     = $_SESSION['image']; 
                     }
                    
                     if($_POST['User']['occupation']!='Industry')
                     {
                        $user->industry = $_POST['User']['occupation'];
                     }
                     
                     if($_POST['city_state']!='')
                     {
                         $location = explode(',', $_POST['city_state']);
                     
                         if(isset($location[2]))
                         $user->country   = $location[2];
                         else
                         $user->country   = '';
                         
                         if(isset($location[1]))
                         $user->state     = $location[1];
                         else
                         $user->state     = '';
                         
                         if(isset($location[0]))
                         $user->city      = $location[0];
                         else
                         $user->city      = '';
                     }                   
                    
                         
                         $user->save(false);
	 					 PrivacySettingsHelper::setSettings($user->id, isset($_POST['Privacy']) ? $_POST['Privacy'] : array());

                         $flashmsg = "Your changes have been saved";
                        
                  }
                  
            
            if(isset($_GET['userid']))
                  {
                      $id     = $_GET['userid'];  
                      $userid = Yii::app()->user->getID();   
                      
                  }
                  else{
                     $id      = Yii::app()->user->getID();   
                     $userid  = Yii::app()->user->getID();   
                  }
                  
                  
            
            $user          = User::model()->find("id = $id");
            
            $current_user = Friend::model()->find(array(
                'condition' => 'user_id =:x AND friend_id =:y', 
                'params'=>array(':x'=>$userid,':y' => $id)
            )); 
            
            $friendrequestsent = array();
            $notification = null;
            
            $friendrequestaccept = $current_user;
           
            $notificationResponce =null;       
            
            if ($user->isMember())
            {                
                $this->redirect(array('myProfile','userid'=>$id));
            }
            
            //$this->render('myprofilecompany',array('user'=>$user,'interest'=>$interest,'current_user'=>$current_user,'flashmsg'=>$flashmsg,'friendrequestsent'=>$friendrequestsent,'friendrequestaccept'=>$friendrequestaccept,'notification'=>$notification,'notificationResponce'=>$notificationResponce));
            $this->render('myprofile',array('user'=>$user,'interest'=>$interest,'current_user'=>$current_user,'flashmsg'=>$flashmsg,'friendrequestsent'=>$friendrequestsent,'friendrequestaccept'=>$friendrequestaccept,'notification'=>$notification,'notificationResponce'=>$notificationResponce));
        }
        
        public function actionupoadImageProfile()
        {
            $baseurl = Yii::app()->request->baseUrl;
            if (isset($_FILES['images']['tmp_name']))
            {
                $size = $_FILES['images']['size'];
                 
                if ($size[0] <= 3145728)
                {
                    $name = $_FILES["images"]["name"];
                     
                    if ($name)
                    {
                        $image = html_entity_decode(time().$name[0]);
                        $_SESSION['image'] = $image;
                        $result = 1;
                    }

                    if ($result)
                    {
                        $sourcefolder = $_FILES["images"]["tmp_name"];
                        move_uploaded_file($sourcefolder[0], Yii::app()->basePath."/../images/profile/" . $image);                        
                        echo ImageHelper::imageShow($baseurl.'/images/profile/', $image, 180, 180, array('class' => 'imageurluolod'));
                        exit;
                    }
                }
            }
        }
        
        public function actionchangePassword()
        {
            $this->layout  = 'plane';

            if(isset ($_POST['oldpassword'])){
            $oldpassword   = $_POST['oldpassword'];
            $newpassword   = $_POST['newpassword'];

            $userid        = Yii::app()->user->getID();

            $user          = User::model()->find("id = $userid");

            if($user['password']== md5($oldpassword))
            {
                $user['password'] = md5($newpassword);
                $user->save(false);

                $arry = array('id'=>'colorsucess',"result"=>"Your password is changed successfully.");
                echo json_encode($arry);
                exit;
            }
            else
            {
                $arry = array('id'=>'colorerror',"result"=>"Old password does not match.");
                echo json_encode($arry);
                exit;
            }

            }

            $this->render("changepassword");
        }


    public function actionShowUserBriefs()
    {
        if (isset($_POST['user_id'])) {
            $pageno = isset($_POST['pageno']) ? $_POST['pageno'] : 0;
            $user_id = $_POST['user_id'];        
            $user = User::model()->findByAttributes(array('id' => $user_id));
            
			if (count($user)) {
				$friends_ids = FriendHelper::getFriendsIdsByUserId($user_id, 'member');
				$companies_ids = FriendHelper::getFriendsIdsByUserId($user_id, 'company user');
				
				$is_friend = in_array(Yii::app()->user->getID(), $friends_ids) || in_array(Yii::app()->user->getID(), $companies_ids);
				
				$privacy = PrivacySettingsHelper::getSettings($user_id);
				//$privacy = Yii::app()->user->getModel()->isCompany() ? $privacy['companies'] : ($is_friend ? $privacy['friends'] : $privacy['everybody']);
				if ($user->isMember()) {
					$privacy = ($is_friend ? (Yii::app()->user->getModel()->isMember() ? $privacy['friends'] : $privacy['followees']) : $privacy['everybody']);
				} else {
					$myFolloweesIds = FriendHelper::getMyFolloweesIds();
					$privacy = (in_array($user_id, $myFolloweesIds) ? $privacy['followers'] : $privacy['everybody']);
				}
            
				//Yii::app()->log->routes['db']->enabled = true;
				//CVarDumper::dump('++++++++++++++++++++', 10, true); 
				//CVarDumper::dump($privacy, 10, true); 
				//CVarDumper::dump('+++++++++++++++++++', 10, true); 

				$is_pending = Yii::app()->user->getModel()->isPendingFriend($user_id);
				$output = array('favorited' => '', 'created' => '', 'taken' => '');
				
				if ($privacy['dashboard']) {
					$list = $user->favotiteListSorted();
					$data = $this->processingList($user, $list, $friends_ids, $pageno, 8, false);
					$data['container'] = 'sectionFavorited';
					$data['_popup'] = true;
					$data['current_user'] = $user_id;
					$output['favorited'] = $this->renderPartial('mydashboard', $data, true, true);
        
					$list = $user->listSorted();
					$data = $this->processingList($user, $list, $friends_ids, $pageno, 8, false);
					$data['container'] = 'sectionCreated';
					$data['_popup'] = true;
					$data['current_user'] = $user_id;
					$output['created'] = $this->renderPartial('mycreated', $data, true, true);
        
					$list = $user->takenListSorted();
					$data = $this->processingList($user, $list, $friends_ids, $pageno, 8, false);
					$data['container'] = 'sectionTaken';
					$data['_popup'] = true;
					$data['current_user'] = $user_id;
					$output['taken'] = $this->renderPartial('mytaken', $data, true, true);
					
					$myFriends = FriendHelper::getFriendsByUserId($user_id, 'member');
					$myCompanies = FriendHelper::getFriendsByUserId($user_id, 'company user');
                    $output['friends'] = $this->renderPartial('friends', array(
						'user' => $user,
						'pageno' => $pageno,
		                'pages' => ceil(count($myFriends)/PAGE_LIMIT_PIOPLE) - 1,
						'friends' => $myFriends,
						'companies' => $myCompanies,
						'_ajax' => false,
						'_popup' => true,
						'container' => 'sectionFriends',
						'current_user' => $user_id,
					), true, true);
				}
        
				$this->renderPartial('showUserBriefs', array(
					'output' => $output,
					'privacy' => $privacy,
					'user' => $user,
					'is_friend' => $is_friend,
					'is_pending' => $is_pending,
					//'friends_ids' => $companies_ids
				), false, true);
			}
        }
    }

	
	public function actionGetNotifications() {
		$user = Yii::app()->user->getModel();

		$notifications = $user->unreadNotifications();
		$data = array();
		foreach($notifications as $n) {
			$z = $n->getAttributes(array('id','notification_type','status','message','list_id'));
			$z['sender'] = $n->userSender->getAttributes(array('id','image','gender'));
			$z['sender']['fullName'] = $n->userSender->getFullName();
			
			if ($n->list_id > 0) 
            {
                $list = $opinion = null;
                if ($n->notification_type == 'opinion')
                {
                    $opinion = Opinion::model()->findByPk($n->list_id);
                    if (!$opinion || $opinion->archived != 0) continue;
                    
                    $z['opinion'] = $opinion->value;
                    $z['list_id'] = $opinion->list_id;
                    $z['notification_type'] = "lists";
                    $z['message'] = $z['message'] . '"' . StringsHelper::capitalize($opinion->value) . '" list ';
                    
                    $list = ListForm::model()->findByPk($opinion->list_id);
                } else $list = ListForm::model()->findByPk($n->list_id);
                
				$z['list_title'] = $list->getAttribute('title');
			}
			
			if ($z['message'] == '') {
				$z['message'] = ' wants to become a friend';
			} else if ($z['message'] == 'You are friends with ') {
				$z['message'] = ' is your friend';
			}
			
			$data[] = $z;
		}
		//CVarDumper::dump($data, 10, true); 
		//CVarDumper::dump('+++++++++++++++++++', 10, true); 
		echo CJSON::encode($data);
		exit;
	}
    
    public function actionDeactivate()
	{
        if(Yii::app()->request->isAjaxRequest) {
            $user = Yii::app()->user->getModel();
            PrivacySettingsHelper::setSettings($user->id, array());
            User::model()->updateByPk($user->id,
                array('firstname' => 'Inactive',
                       'lastname' => $user->isCompany() ? 'Company' : 'User',
                       'image' => ''));
            $_SESSION['image'] = '';
            die('1');
        }
        die('0');
    }
    
     public function getShortTitle($str,$len) {
        /*if (strlen($str) > $len) {
            list($left,, $right) = imageftbbox( 12, 0, Yii::app()->basePath.'/../css/FjallaOne-Regular.ttf', substr($str,0,1));
            $widthOne = $right - $left;
            if ($widthOne >= 6) {
                $str = substr($str, 0, round($len*57/100)).'...';
            } else {
                $str = substr($str, 0, $len).'...';
            }
        }*/
        return $str;        
    }
}
