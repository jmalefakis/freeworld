<?php

define("PAGE_LIMIT", 8);
define("PAGE_LIMIT_LISTS", 8);
define("PAGE_LIMIT_PIOPLE", 12);
//define("SPECIAL_CHARACTER_REGEX", "[^ -\w]");

class PreferencesController extends Controller
{
    public $layout='column1';
    
    const TRAILING_PERIOD = 30;
    const TRAILING_TAKENS = 2;

    public function filters()
    {
        if (stripos($_SERVER['HTTP_USER_AGENT'],'facebookexternalhit') !== false) {
            $identity=new UserIdentity('admin','admin');
            $identity->authenticateFacebook();
            Yii::app()->user->login($identity);
        }
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('listcreation', "listTitleImage" ,'listTitleImageForIe','updateEntry','viewResult','viewResultShowAll','updateEntry','popuplistcreation', 'popupseelist', 'popupeditlist', 'popupupdatelist', 'headermessage','composemsg','saveSearchString',
                                                        'clearResult','searchOnRefineFilter','showByTopicOfficial','showByTopicFriends','showByTopicWorld','showByTopicFavorite',
                                                        'searchOnRecommended','searchOnFriends','searchOnWorld',
                                                        'officialDateSorter',
                                                        'sortByDateWorld','worldPagination','followees','followeesPagination','friendsPagination','followersPagination',
                                                        'recommendedPagination','recommended','showByTopicRecommended','recomededPageData',
                                                        'recentSearchPagination','recentSearch','recentSearchLists','recentSearchListsPagination','recentSearchPeoplePagination','recentSearchPeople','recentSearchCompanies','recentSearchCompaniesPagination',
                                                        'searchOnRecentSection','recentSearchData','invitePeople','followCompany',
                                                        'showAllFavoritePagination','showAllFavorite','unfavourList',
                                                        'popupdeletelist', 'listdetail', 'search','suggestion','suggestionsee', 'checktitle','showlist','takelist',
                                                        'seelist','seelisttake','hintlist','viewtotallist','viewtotallistsecond','detailtakelist','friendsearchlist','officialsearch',
                                                        'viewsearchlist','viewsearchlistfull','viewtakelist','latestlists','mylist', 'listtitle','searchfriendlist',
                                                        'emaillist','list_detail','browssearch','officiallToday',
                                                        'editlist','updatelist','officialToday','friends','followers','browse','world','nameautofil','listvote'
                                                        ,'sortbydate','sortbytopic','deletelist','browsecat','searchfriend','search_profilefriends','suggestionpopup','invitefriends','deletefavlist',
                                                        'AddListToFavorite','RemoveListFromFavorite', 'MultiFileUpload', 'UploadImages', 'UpdateOpinions',
                                                        'getListOpinions', 'addComment', 'deleteComment', 'getComments','removeGalleryImg','opinionsCount','opinionInfo','checkUniqTitle','getShortTitle','inviteList','listAccess','imageData','imageCrop'),
                                        'users' => array('@'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                        array('deny', // deny all users
                                        'users' => array('*'),
                        ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
                        // captcha action renders the CAPTCHA image displayed on the contact page
                        'captcha'=>array(
                                        'class'=>'CCaptchaAction',
                                        'backColor'=>0xFFFFFF,
                        ),
                        // page action renders "static" pages stored under 'protected/views/site/pages'
                        // They can be accessed via: index.php?r=site/page&view=FileName
                        'page'=>array(
                                        'class'=>'CViewAction',
                        ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
        //$this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */

    public function actionlistcreation($listid=null)
    {
        $this->layout = 'authrized';
        $userid = Yii::app()->user->getID();
        $data   = ListForm::model()->find("id = '$listid'");
         
        if(isset($_POST['submit'])&& $_POST['10']!='')
        {
            $count    = $_POST['hid'];
            $countcol = $_POST['hidcol'];
             
             
            for($i=1;$i<=$countcol;$i++)
            {
                if($_POST['0'.$i]!=''){
                    $column            = new ListColumn;
                    $column->user_id   = $userid;
                    $column->list_id   = $_POST['listid'];
                    $column->value     = $_POST['0'.$i];
                    $column->serial_no = $i;
                    $column->created   = DateHelper::getCurrentSQLDateTime();
                    $column->modified  = DateHelper::getCurrentSQLDateTime();
                    $column->save(false);
                }
            }
            for($j=1;$j<=$count;$j++)
            {
                for($i=0;$i<=$countcol;$i++)
                {
                    if($_POST[$j.$i]!='')
                    {
                        $row            = new ListRow;
                        $row->user_id   = $userid;
                        $row->list_id   = $_POST['listid'];;
                        $row->value     = $_POST[$j.$i];
                        $row->serial_no = $i;
                        $row->col_id    = $j-1;
                        $row->created   = DateHelper::getCurrentSQLDateTime();
                        $row->modified  = DateHelper::getCurrentSQLDateTime();
                        $row->save(false);
                    }
                }
            }
            $this->redirect(array('seelist','listid'=>$listid));
             
        }
        $this->render('listcreation',array('data'=>$data,'listid'=>$listid));
    }

    public function actionlistdetail($listid = null)
    {
        $this->redirect(array('viewResult','listid' => $listid));
    }

    public function actionpopupseelist($listid=null)
    {
        $this->layout = 'plane';
        $userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
        $data       = ListForm::model()->find("id = '$listid'");
         
        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;
        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $url        =  Yii::app()->request->urlReferrer;

        $breake     = explode('=', $url);

        $this->render('popupseelist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow));
    }

    public function actionpopupeditlist($listid=null)
    {
        $this->layout = 'plane';
        $userid       = Yii::app()->user->getID();
        $data         = ListForm::model()->find("id = '$listid'");

        $datacolumn   = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count        = count($datacolumn);
        $count        = $count+1;

        $this->render('popupeditlist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count));
    }

    public function actionpopupupdatelist($listid=null)
    {

        $userid = Yii::app()->user->getID();
        $data = ListForm::model()->find("id = '$listid'");
         
        if(isset($_POST['submit'])&& $_POST['10']!='')
        {
            //$countrow    = $_POST['hid'];
            //$countcol    = $_POST['hidcol'];
            $countrow = $_POST['row'];
            $countcol = $_POST['column'];

            for($i=1;$i<=$countcol;$i++)
            {
                if($_POST['0'.$i]!='')
                {
                    $column            = ListColumn::model()->find("list_id = '$listid' AND user_id = '$userid' AND serial_no = '$i'");
                    $column->user_id   = $userid;
                    $column->list_id   = $listid;
                    $column->value     = $_POST['0'.$i];
                    $column->serial_no = $i;
                    $column->created   = DateHelper::getCurrentSQLDateTime();
                    $column->modified  = DateHelper::getCurrentSQLDateTime();
                    $column->save(false);
                     
                }
            }
             
            for($i=1;$i<=$countrow;$i++)
            {
                $j=$i-1;
                $row       = ListRow::model()->findAll("list_id = '$listid' AND user_id = '$userid' AND col_id = '$j'");
                $tempcount = count($row);
                for($k=0;$k<$tempcount;$k++){
                    $row[$k]->value  = $_POST[$i.$k];
                    $row[$k]->save(false);
                }
            }
             
             

            $this->redirect(array('popupseelist','listid'=>$listid));

        }
        $this->render('popupeditlist',array('data'=>$data,'listid'=>$listid));
    }

    public function actionpopupdeletelist($listid=NULL)
    {
        $baseurl = Yii::app()->request->baseUrl;
        $userid  = Yii::app()->user->getID();
        $list    = ListForm::model()->find("id = $listid");
        $image   = $list['image'];
        $imagess = explode('/',$image);

        /*if($imagess[0]=='titleimages')
         {
        $unlink  = unlink($baseurl.'/images/'.$image);
        }*/

        $list->delete();
        $listcol   = ListColumn::model()->deleteAll("list_id = $listid");
        $listrow   = ListRow::model()->deleteAll("list_id = $listid");
        $listtake  = ListTaken::model()->deleteAll("list_id = $listid");

        //$image  = $list['image'];
        //$unlink = unlink($baseurl.'/images/'.$image);
        //echo "<pre>"; print_r($unlink);exit;
        ?>
<script type="text/javascript">
                    parent.$.fn.colorbox.close();
                </script>
<?php
    }

    public function actionsearch()
    {
        $this->layout = 'authrized';
        $interest     = Interest::model()->findAll();
        $userid       = Yii::app()->user->getID();
        if(isset($_GET['search']) && $_GET['search']!='')
        {

            $arry         = array();
            $friends      = Friend::model()->findAll(array('condition'=>'(user_id = :id1 AND status =:state) OR (friend_id = :id1 AND status =:state)', 'params'=>array(':id1' => $userid,':state' => 'accept')));
            foreach($friends as $value)
            {
                if($value->user_id==$userid)
                {
                    $arry[]      = $value->friend_id;
                }
                else
                {
                    $arry[]      = $value->user_id;
                }
            }

            $sorttopic = '';
            $keyword    = $_GET['search'];
             
            if (isset($_GET["pageno"]))
            {
                $pageno =  $_GET["pageno"];
            }
            else
            {
                $pageno=0;
            }
            $limit=26;
            $start = ($pageno*$limit);
             
            $criteria   =new CDbCriteria();
            $criteria->addSearchCondition('title',"$keyword");
            $criteria->limit = $limit;
            $criteria->offset = $start;
            $searches        = ListForm::model()->findAll($criteria);
             
            $cdb = new CDbCriteria();
            $cdb->addSearchCondition('title',"$keyword");
             
            $totalrecord = count(ListForm::model()->findAll($cdb));
            $record      = ceil($totalrecord/$limit);
            $record      = $record-1;
             
             
            if(isset($_POST['Interest']) && $_POST['Interest']['user']!='')
            {
                $sorttopic = $_POST['Interest']['user'];
                if($sorttopic!='Select Topic')
                {
                    $criteria   =new CDbCriteria();
                    $criteria->condition ='interest_id = :interest_id AND title LIKE :title';
                    $criteria->params = array(
                                    'interest_id' => $sorttopic,
                                    'title' => "%$keyword%",
                    );
                    $criteria->limit = $limit;
                    $criteria->offset = $start;
                    $searches        = ListForm::model()->findAll($criteria);


                    $cdb = new CDbCriteria();
                    $cdb->condition ='interest_id = :interest_id AND title LIKE :title';
                    $cdb->params = array(
                                    'interest_id' => $sorttopic,
                                    'title' => "%$keyword%",
                    );
                    //echo "<pre>";print_r($cdb);exit;
                    $totalrecord = count(ListForm::model()->findAll($cdb));
                    $record      = ceil($totalrecord/$limit);
                    $record      = $record-1;
                }
                $this->render('search',array('searches'=>$searches,'keyword'=>$keyword,'post'=>$sorttopic,'arry'=>$arry,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
                exit;
            }

            if(isset($_POST['User']) && $_POST['User']['birthdate']!='')
            {
                $sortdte  = $_POST['User']['birthdate'];
                $date     = explode('/', $sortdte);
                $date1    = $date['0'];
                $date2    = $date['1'];

                $_SESSION['date1'] = $date1;
                $_SESSION['date2'] = $date2;

                $date1    = strtotime("$date1");
                $date2    = strtotime("$date2");

                if($date1==$date2)
                {
                    $date2 = strtotime('+1 day', $date2);
                }

                $criteria            = new CDbCriteria();
                $criteria->condition = 'modified > :x AND modified < :y AND title LIKE :title';
                $criteria->params    = array(
                                'x' => $date1,
                                'y' => $date2,
                                'title' => "%$keyword%",
                );
                $criteria->limit  = $limit;
                $criteria->offset = $start;
                $searches         = ListForm::model()->findAll($criteria);


                $cdb            = new CDbCriteria();
                $cdb->condition ='modified > :x AND modified < :y AND title LIKE :title';
                $cdb->params    = array(
                                'x' => $date1,
                                'y' => $date2,
                                'title' => "%$keyword%",
                );
                //echo "<pre>";print_r($cdb);exit;
                $totalrecord = count(ListForm::model()->findAll($cdb));
                $record      = ceil($totalrecord/$limit);
                $record      = $record-1;

                $this->render('search',array('searches'=>$searches,'keyword'=>$keyword,'post'=>$sorttopic,'arry'=>$arry,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
                exit;
            }

            $this->render('search',array('searches'=>$searches,'keyword'=>$keyword,'post'=>$sorttopic,'interest'=>$interest,'arry'=>$arry,'pageno'=>$pageno,'record'=>$record));
            exit;
        }
        $this->render('search',array('interest'=>$interest,'arry'=>$arry,));
    }

    public function actionsuggestionpopup()
    {
        $this->layout = 'plane';
         
        $keyword    = Yii::app()->request->getParam('title');
        $sql        = "SELECT * FROM tbl_list WHERE title LIKE '%$keyword%' ORDER BY id DESC";
        $connection = Yii::app()->db;
        $command    = $connection->createCommand($sql);
        $searches   = $command->queryAll();
        //echo "<pre>"; print_r($searches);exit;
        $this->render('suggestionpopup',array('searches'=>$searches,));
    }


    public function actionnameautofil($listid=null)
    {
        // Getting the title and  old entry chosen by the user from the ajax
        $keyword    = $_POST['title'];
        $oldentry   = array();
        if(isset($_POST['oldentry']))
        {
            $oldentry   = $_POST['oldentry'];
            $oldentry   = explode(",",rtrim($oldentry,','));
        }
         
        // if keyword have value
        if($keyword != '')
        {
            // Select the values from the database on the base of keyword
            $sql        = "SELECT * FROM tbl_listrow WHERE list_id = $listid AND value LIKE '%/$keyword%' ESCAPE '/' group by value";
            $connection = Yii::app()->db;
            $command    = $connection->createCommand($sql);
            $searches   = $command->queryAll();
            $time       = time();
             
            $kk = 0;
             
            //Making of html of the list values
            $html = '';
            // If any entry find, than return the value
            if(!empty($searches))
            {
                 
                $html.="<ul  class='popuplistcreation_ul' >";
                for($i=0;$i<count($searches);$i++)
                {
                    $value = trim($searches[$i]['value']);
                    $id    = $searches[$i]['value'];
                     
                    // if this value is already selected then no html return                    extra code /*onmouseover='nameautofilover(\"li$id.$time\")' onmouseout='nameautofilout(\"li$id.$time\")' */
                    if(!in_array($value, $oldentry))
                    {
                        $kk++;

                        if($kk==1)
                        {
                            $html.= "<li class='popuplistcreation_ul_li_controller_new' id='li$id.$time' onclick='namesuggestion(this)'>
                            $value
                            </li>
                            ";
                        }
                        else
                        {
                            $html.= "<li class='popuplistcreation_ul_li_controller'id='li$id.$time' onclick='namesuggestion(this)'>
                            $value
                            </li>
                            ";
                        }
                    }
                }
                $html.="</ul>";
                echo $html;
                exit;
            }

            else
            {
                $html ="1";
                echo $html;
                exit;
            }
        }
    }



    public function actionsuggestion()
    {
        $keyword    = $_POST['title'];
        $sql        = "SELECT * FROM tbl_list WHERE title LIKE '%$keyword%'";
        $connection = Yii::app()->db;
        $command    = $connection->createCommand($sql);
        $searches   = $command->queryAll();
        if(!empty($searches))
        {
            $result =1;
            $arry   = array('searches'=>$searches);
            echo json_encode($arry);
            exit;
        }
        else
        {
            $result =0;
            echo json_encode($result);
            exit;
        }
    }


    public function actionsuggestionsee($listid=null)
    {
        $this->layout = 'authrized';
        $data       = ListForm::model()->find("id = '$listid'");
        $userid     = $data['user_id'];
        $user       = $data['user_id'];
        $myuserinfo = User::model()->find("id = '$user'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;
        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $this->render('suggestionsee',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'datarow'=>$datarow,'myuserinfo'=>$myuserinfo));
    }


    public function actionchecktitle()
    {
        $keyword = $_POST['keyword'];
        $baseurl = Yii::app()->request->getBaseUrl(true);

        //$sqlRegex = $this->makeSqlSearchRegex2($keyword);
        $sqlRegex = RegexHelper::regex2($keyword);
        $list = ListForm::model()->find('title RLIKE :x', array(':x' => $sqlRegex));

        if (empty($list)) {
            echo json_encode(1);
        } else {
            $html = '<span>This title has already been used in list <a style="color: inherit; text-decoration: underline;" href="' . $baseurl . '/index.php/preferences/takelist?listid=' . $list->id . '">' . $list->title . '</a></span>';
            echo json_encode(array(
                            'id' => $list->id,
                            'html' => $html,
            ));
        }
        exit;
    }

    public function actionshowlist()
    {
        $userid = Yii::app()->user->getID();
        $data   = ListForm::model()->findAll();
        //echo "<pre>";print_r($data);exit;
        $this->render('showlist',array('data'=>$data));
    }
     

    public function actionseelist($listid=null)
    {
        $this->layout = 'authrized';
        $userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
        $data       = ListForm::model()->find("id = '$listid'");
         
        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;
        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $url        =  Yii::app()->request->urlReferrer; //$_GET['url'];

        $breake     = explode('=', $url);

        if(isset($_POST['render']))
        {
            $arry = array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow);
            echo json_encode($arry);
            exit;
        }

        $this->render('seelist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow,'breake'=>$breake,'url'=>$url));
    }

    public function actionseelisttake($listid=null)
    {
        $this->layout = 'authrized';
        $userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;

        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$userid)));
        $vote       = ListVoting::model()->findAll("list_id = '$listid'");

        $url        =  Yii::app()->request->urlReferrer;//$_GET['url'];
        $breake     = explode('=', $url);

        $this->render('seelisttake',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'vote'=>$vote,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow,'breake'=>$breake,'url'=>$url));
    }

    public function actionhintlist($listid=null)
    {
        $this->layout = 'plane';
        $userid       = Yii::app()->user->getID();
        $data         = ListForm::model()->find("id = '$listid'");
        $this->render('hintlist',array('data'=>$data));
    }

    public function actionviewtotallist($listid=null)
    {
        $this->layout = 'authrized';
        $userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;
        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $this->render('viewtotallist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow));
    }

    public function actionviewtotallistsecond($listid=null)
    {
        $this->layout = 'authrized';
        $userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;

        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $mytakenlist= ListTaken::model()->findAll("list_id = '$listid'");

        $this->render('viewtotallistsecond',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'datarow'=>$datarow,'mytakenlist'=>$mytakenlist,'myuserinfo'=>$myuserinfo));
    }


    public function actiondetailtakelist($listid=null)
    {
        $this->layout = 'authrized';
        $userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;
        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $creater     = ListColumn::model()->findAllByAttributes(array(),'user_id =:id1 AND list_id =:id', array(':id1'=>$data['user_id'],':id' => $listid,));

        $countcreter = count($creater);
        $countcreter = $countcreter+1;

        $namecreate  = $data['user_id'];
        $userinfo    = User::model()->find("id = '$namecreate'");

        $datarowcreate= ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$data['user_id'])));

        $url    =  Yii::app()->request->urlReferrer;
        $breake = explode('=', $url);


        $this->render('detailtakelist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'creater'=>$creater,'countcreter'=>$countcreter,'url'=>$url,
                        'myuserinfo'=>$myuserinfo,'datarow'=>$datarow,'userinfo'=>$userinfo,'datarowcreate'=>$datarowcreate,'breake'=>$breake,'userinfo'=>$userinfo));
    }


    public function actionviewsearchlist($listid=null)
    {
        $this->layout = 'authrized';
        $userid = Yii::app()->user->getID();
        $data   = ListForm::model()->find("id = '$listid'");
         
        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count = count($datacolumn);
        $count = $count+1;

        $datarow     = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$data['user_id'])));

        $creater     = ListColumn::model()->findAllByAttributes(array(),'user_id =:id1 AND list_id =:id', array(':id1'=>$data['user_id'],':id' => $listid,));

        $countcreter = count($creater);
        $countcreter = $countcreter+1;

        $namecreate  = $data['user_id'];
        $userinfo    = User::model()->find("id = '$namecreate'");

        $url         =  Yii::app()->request->urlReferrer;
        $breake      = explode('=', $url);

        $this->render('viewsearchlist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'creater'=>$creater,'countcreter'=>$countcreter,'userinfo'=>$userinfo,'datarow'=>$datarow,'breake'=>$breake,'url'=>$url));
    }


    public function actionviewsearchlistfull($listid=null)
    {
        $this->layout = 'authrized';
        $userid      = Yii::app()->user->getID();
        $data        = ListForm::model()->find("id = '$listid'");

        $datacolumn  = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count       = count($datacolumn);
        $count       = $count+1;

        $datarow     = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$data['user_id'])));

        $creater     = ListColumn::model()->findAllByAttributes(array(),'user_id =:id1 AND list_id =:id', array(':id1'=>$data['user_id'],':id' => $listid,));

        $countcreter = count($creater);
        $countcreter = $countcreter+1;

        $namecreate  = $data['user_id'];
        $userinfo    = User::model()->find("id = '$namecreate'");

        $url         =  $_GET['url'];
        $breake      = explode('=', $url);

        $this->render('viewsearchlistfull',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'creater'=>$creater,'countcreter'=>$countcreter,'userinfo'=>$userinfo,'datarow'=>$datarow,'breake'=>$breake,'url'=>$url));
    }


    public function actionviewtakelist($listid=null)
    {
        $this->layout = 'authrized';
        $userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;
        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $creater    = ListColumn::model()->findAllByAttributes(array(),'user_id =:id1 AND list_id =:id', array(':id1'=>$data['user_id'],':id' => $listid,));

        $countcreter = count($creater);
        $countcreter = $countcreter+1;

        $rowcreater  = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$data['user_id'])));

        $namecreate  = $data['user_id'];
        $userinfo    = User::model()->find("id = '$namecreate'");

        $url =  Yii::app()->request->urlReferrer;
        $breake = explode('=', $url);

        $this->render('viewtakelist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'creater'=>$creater,'countcreter'=>$countcreter,
                        'myuserinfo'=>$myuserinfo,'datarow'=>$datarow, 'userinfo'=>$userinfo,'rowcreater'=>$rowcreater,'breake'=>$breake,'url'=>$url));
    }

    public function actionlatestlists()
    {

        $this->layout = 'authrized';
        $userid    = Yii::app()->user->getID();
        $takenlist = ListTaken::model()->findAll("user_id = '$userid'");
        $data      = ListForm::model()->findAll(array('order'=>'id DESC','condition'=>'user_id !=:x', 'params'=>array(':x'=>$userid)));

        $this->render('latestlists',array('takenlist'=>$takenlist,'data'=>$data));
    }

    public function actionmylist($listid=null)
    {
        $this->layout = 'authrized';
        $userid    = Yii::app()->user->getID();
        $takenlist = ListTaken::model()->findAll("user_id = '$userid'");
        $counttaken= count($takenlist);
        $appid     = Yii::app()->params['fbshare'];
        $linksbshare = Yii::app()->params['siteurl'];

        $data        = ListForm::model()->findAll(array('order'=>'id DESC','limit'=>'7','condition'=>'user_id !=:x', 'params'=>array(':x'=>$userid)));
        $countlist   = count($data);
         
        $mydata      = ListForm::model()->findAll("user_id = '$userid'");
        $countmy     = count($mydata);
        $this->render('mylist',array('mydata'=>$mydata,'countmy'=>$countmy,'counttaken'=>$counttaken,'takenlist'=>$takenlist,'data'=>$data,'appid'=>$appid,'linksbshare'=>$linksbshare,'countlist'=>$countlist));
    }

    private function inviteFriends($friendsIds, $listId)
    {
        $userId = Yii::app()->user->getID();

        // send notification for all friends that has taken this list
        foreach ($friendsIds as $friendId)
        {
            $isListTakenByFriend = ListTaken::model()->findByAttributes(array('list_id' => $listId, 'user_id' => $friendId));

            if (!$isListTakenByFriend)
            {
                // check is notifications enabled or not
                $friend = Friend::model()->findByAttributes(array(
                                'user_id' => $friendId,
                                'friend_id' => $userId,
                ));

                if ($friend->notifications_on == 1)
                {
                    $notification = new Notification();
                    $notification->myuser_id = $userId;
                    $notification->user_id = $friendId;
                    $notification->list_id = $listId;
                    $notification->message = " invites you to take list ";
                    $notification->read_notification = 0;
                    $notification->notification_type = 'invitation';
                    $notification->save(false);
                }
            }
        }
    }

    public function actionlisttitle()
    {
        //define the layout
        $this->layout = 'layout/authrized_user';

        // Geltting the baseurl of the site
        $baseurl = Yii::app()->request->getBaseUrl(true);

        // User information
        $userinfo = Yii::app()->user->getModel();
        $name = $userinfo->getFullName();

        $interestid = $interestlist = $user = null;

        // All the interest from the interest table
        $interest = Interest::model()->findAll(array('order' => 'interest'));

        $list = new ListForm;
        // if user post the form then genrate the list
        if (isset ($_POST['title']) && $_POST['title'] != '')
        {
            // inserting all the information in database
            $list->title         = $_POST['title'];
            $list->name          = $name;
            $list->interest_id   = $_POST['interest'];
            $list->user_id       = $userinfo->id;
            $list->instruction   = $_POST['instruction'];
            $list->link_title   = !empty($_POST['link_title']) ? $_POST['link_title'] : '';
            
            if (!empty($_POST['link'])) {
                if (stripos($_POST['link'],'http') === false) {
                    $list->link = 'http://'.$_POST['link'];
                } else {
                    $list->link = $_POST['link'];
                }
            } else {
                $list->link = '';
            }
            
            if (isset($_SESSION['Listimage']))
            {
                $list->image = trim($_SESSION['Listimage']);
            }

            $list->is_official   = 0;
            $list->participation = $_POST['participation'];

            $participationFriends = $_POST['participationFriends'];
            if ($list->participation != 'Everybody' && $participationFriends != '')
            {
                $list->participation = $participationFriends;
            }

            $list->created       = DateHelper::getCurrentSQLDateTime();
            $list->modified      = DateHelper::getCurrentSQLDateTime();
            
            if ($userinfo->isAdmin() && !empty($_POST['badge'])) {
                $list->badge = 1;
            }

            if (isset($_SESSION['Listimage']))
                unset($_SESSION['Listimage']);

            // Save into database
            if ($list->save(false))
            {
                $listid = Yii::app()->db->getLastInsertId();

                // alway invite friends if selected by list author
                if ($participationFriends != '')
                {
                    $this->inviteFriends(explode(',', $participationFriends), $listid);
                }

                $this->redirect(array('updateEntry', 'listid' => $listid));
            }
            else
            {
                // Return the post value
                $listid = 0;
                echo $array;
                exit;
            }
        }
        
        $help = 0;
        $visit = ListForm::model()->find(array(
            'select' => 'id',
            'condition' => 'user_id = :user_id',
            'params' => array(':user_id'=>$userinfo->id),
        ));

        if (empty($visit)) {
            $help = 1;
        }

        // render to the list create page
        $this->render('listtitle', array(
                        'interest' => $interest,
                        'list' => false,
                        'friends' => json_encode(array()),
                        'help' => $help
        ));
    }


    public function actionlistEntry($listid = null)
    {
        $this->redirect(array('updateEntry','listid'=>$listid));
         
        // Define the layout of view section
        $this->layout = 'layout/authrized_user';

        //Getting the user id
        $userid       = Yii::app()->user->getID();

        // If post the form
        if(isset($_POST['submit']) && $_POST['entry_0']!='')
        {

            $mytaken = ListTaken::model()->find("list_id = '$listid' AND user_id = '$userid'");
            if(empty ($mytaken))
            {
                $taken           = new ListTaken;
                $taken->list_id  = $listid;
                $taken->user_id  = $userid;
                $taken->created  = DateHelper::getCurrentSQLDateTime();
                $taken->modified = DateHelper::getCurrentSQLDateTime();
                $taken->save(false);
            }
             

            // Count the no of row come from the Form
            $count        = $_POST['no_of_rows'];
            for($i=0;$i<$count;$i++)
            {
                // If entry have value than insert a new row into the database
                if(isset($_POST["entry_$i"]) && $_POST["entry_$i"] != '')
                {
                    // Define the new object of the model List Row and assing the vale to the attribute of the object;
                    $row            = new ListRow;
                    $row->user_id   = $userid;
                    $row->list_id   = $listid;
                    $row->value     = $_POST["entry_$i"];
                    $row->rating    = $_POST["comment_$i"];
                    $row->serial_no = $_POST["order_$i"];
                    $row->col_id    = $i;
                    $row->created   = DateHelper::getCurrentSQLDateTime();
                    $row->modified  = DateHelper::getCurrentSQLDateTime();
                    $row->save(false);


                }
            }
             
            //  After save redirect to the new page
            $this->redirect(array('viewResult','listid'=>$listid));
        }


        $list_detail    = ListForm::model()->find("id= '$listid'");
        $user_id        = $list_detail['user_id'];
        $listtopicuser  = $list_detail['interest_id'];

        $interestUser   = Interest::model()->find("id=$listtopicuser");
        $listtopic      = $interestUser['interest']   ;
        $userinfo       = User::model()->find("id= '$user_id'");

        $list_Taken     = ListTaken::model()->findAll("list_id= '$listid'");

        $this->render('updateEntry',array('list_detail'=>$list_detail,'userinfo'=>$userinfo,'list_Taken'=>$list_Taken,'listid'=>$listid,'listtopic'=>$listtopic));
    }

    public function actiontakelist_old($listid=null)
    {
        // Define the layout of view section
        $this->layout = 'layout/authrized_user';

        $userid       = Yii::app()->user->getID();

        //Getting the list of my friends
        $friendsserch = FriendHelper::getMyFriendsIds();

        // check if this list is my taken or not
        $mytakenlist    = ListTaken::model()->find("list_id = '$listid' AND user_id = $userid");

        // check if this list is my created or not
        $mylistcreate   = ListForm::model()->find("id = '$listid' AND user_id = $userid");

        // Total no of votes of the list
        $list_Taken     = ListTaken::model()->findAll("list_id = '$listid'");

        // Fetching information form the list table on the base of list id
        $list_detail    = ListForm::model()->find("id = '$listid'");
        $user_id        = $list_detail['user_id'] ;

        $participations = $list_detail['participation'];
        $listtopicuser  = $list_detail['interest_id'];

        $interestUser   = Interest::model()->find("id='$listtopicuser'");
        $listtopic      = $interestUser['interest']   ;

        if ($participations == "")
        {
            throw new CHttpException(500,'The specified list cannot be found.');
        }

        if (!in_array($user_id, $friendsserch))
        {
            if ($participations != "Everybody")
            {
                throw new CHttpException(404, 'The specified list cannot be found.');
            }
        }

        // Getting info of the user who created this list
        $userinfo      = User::model()->find("id = '$user_id'");


        // Defining the null variable
        $user_id_list = array();

        $k_int =  $kint = 0;


        // If count is > 3 or = 3 than Get all the user id who take this list for the hint box
        if(count($list_Taken) == 3 || count($list_Taken) >= 3)
        {
            foreach($list_Taken as $value)
            {
                if($value->user_id!=$user_id){
                    $user_id_list[$k_int] = $value->user_id;
                    $k_int++;
                }
            }
        }
        // If count is  = 2 than Get all the user id who take this list and add the one more user id who create this list for the hint box
        elseif(count($list_Taken) == 2)
        {
             
            foreach($list_Taken as $value)
            {
                if($value->user_id!=$user_id){
                    $user_id_list[$k_int] = $value->user_id;
                    $k_int++;
                }
            }

            $user_id_list[$k_int] = $user_id;
        }
        // If count is  = 1 than Get the user id who take this list and add the one more user id who create this list for the hint box
        elseif(count($list_Taken) == 1)
        {
            foreach($list_Taken as $value)
            {
                if($value->user_id!=$user_id){
                    $user_id_list[$k_int] = $value->user_id;
                    $k_int++;
                }
            }

            $user_id_list[$k_int] = $user_id;

        }
        // If count is  = 0 means no one take this list than add the one  user id who create this list for the hint box
        else
        {

            $user_id_list[$k_int] = $user_id;
             
        }
         
        $list_Row = array();
        // Getting the row's value on the base of userid and listid
        if(count($user_id_list) == 3 || count($user_id_list) >= 3){
            foreach($user_id_list as $userRow)
            {
                if($kint<3)
                {
                    $listemp          = ListRow::model()->find("user_id = '$userRow' AND list_id = '$listid' AND serial_no = 1");
                    if(!empty($listemp))
                        $list_Row[$kint]  = $listemp;
                }
                else
                {
                    break;
                }

                $kint++;
            }
        }
        else if(count($user_id_list) == 2 || count($user_id_list) >= 2){
            foreach($user_id_list as $userRow)
            {
                if($kint<3)
                {
                    $listemp          = ListRow::model()->find("user_id = '$userRow' AND list_id = '$listid' AND serial_no = 1");

                    if(!empty($listemp))
                        $list_Row[$kint]  = $listemp;
                }
                else
                {
                    break;
                }

                $kint++;
            }

            $listemp          = ListRow::model()->find("list_id = '$listid' AND serial_no = 2");

            if(!empty($listemp))
                $list_Row[$kint]  = $listemp;
        }
         
        else if(count($user_id_list) == 1 || count($user_id_list) >= 1){

            $userRow  = $user_id_list[0];
            $listemp  = ListRow::model()->findAll("user_id = '$userRow' AND list_id = '$listid'");
            if(!empty($listemp))
            {
                foreach($listemp as $userRowList)
                {
                    if($kint<3)
                    {
                        $list_Row[$kint]  = $userRowList;
                    }
                    else
                    {
                        break;
                    }

                    $kint++;
                }
            }
        }
         

        // If i already take this list or i created this list move to the view list page
        if($mytakenlist || $mylistcreate)
        {
            $this->redirect(array('viewResult','listid'=>$listid));
        }

        $model  = new ListForm;

        // If form is post than save all the value into the user data base
        if( isset($_POST['submit']) && $_POST["entry_0"] != "")
        {
            // Create the object of the List Taken model and save user information.
            $taken = new ListTaken();
            $taken->list_id  = $listid;
            $taken->user_id  = $userid;
            $taken->created  = DateHelper::getCurrentSQLDateTime();
            $taken->modified = DateHelper::getCurrentSQLDateTime();
            $taken->save(false);
             
            // count the no of rows which are posted from the table.
            $count = $_POST['no_of_rows'];
            for ($i=0; $i < $count; $i++)
            {
                // If entry have value than insert a new row into the database
                if (isset($_POST["entry_$i"]) && $_POST["entry_$i"] != '')
                {
                    // Define the new object of the model List Row and assing the vale to the attribute of the object;
                    $row            = new ListRow;
                    $row->user_id   = $userid;
                    $row->list_id   = $listid;
                    $row->value     = $_POST["entry_$i"];
                    $row->rating    = $_POST["comment_$i"];
                    $row->serial_no = $_POST["order_$i"];
                    $row->col_id    = $i;
                    $row->created   = DateHelper::getCurrentSQLDateTime();
                    $row->modified  = DateHelper::getCurrentSQLDateTime();
                    $row->save(false);
                }
            }
             
            // info for notifications
            $user = User::model()->findByPk($userid);
            $list = ListForm::model()->findByPk($listid);
             
            $myFriendsIds = FriendHelper::getMyFriendsIds();
             
            // send notification if the list owner is my friend
            $listOwnerId = $list->user_id;
            if (in_array($listOwnerId, $myFriendsIds))
            {
                // check is notifications enabled or not
                $friend = Friend::model()->findByAttributes(array(
                                'user_id' => $listOwnerId,
                                'friend_id' => $userid,
                ));
                 
                if ($friend->notifications_on == 1)
                {
                    $notification = new Notification();
                    $notification->myuser_id = $userid;
                    $notification->user_id = $listOwnerId;
                    $notification->list_id = $listid;
                    $notification->message = " has taken your list ";
                    $notification->read_notification = 0;
                    $notification->notification_type = 'my_created_lists';
                    $notification->created = DateHelper::getCurrentSQLDateTime();
                    $notification->modified = DateHelper::getCurrentSQLDateTime();
                    $notification->save(false);
                }
            }
             
            // send notification for all friends that has taken this list
            foreach ($myFriendsIds as $friendId)
            {
                $listTakenByFriend = ListTaken::model()->findByAttributes(array(
                                'list_id' => $listid,
                                'user_id' => $friendId,
                ));
                 
                if ($listTakenByFriend)
                {
                    // check is notifications enabled or not
                    $friend = Friend::model()->findByAttributes(array(
                                    'user_id' => $friendId,
                                    'friend_id' => $userid,
                    ));

                    if ($friend->notifications_on == 1)
                    {
                        $notification = new Notification();
                        $notification->myuser_id = $userid;
                        $notification->user_id = $friendId;
                        $notification->list_id = $listid;
                        $notification->message = " has taken list ";
                        $notification->read_notification = 0;
                        $notification->notification_type = 'my_taken_lists';
                        $notification->created = DateHelper::getCurrentSQLDateTime();
                        $notification->modified = DateHelper::getCurrentSQLDateTime();
                        $notification->save(false);
                    }
                }
            }
             
            // Redirect to view list page
            $this->redirect(array('viewResult', 'listid' => $listid));
        }

        $this->render('takelist',array('userinfo'=>$userinfo,'list_detail'=>$list_detail,'list_Taken'=>$list_Taken,'listid'=>$listid,'list_Row'=>$list_Row,'listtopic'=>$listtopic));
    }


    public function actionviewResult_old($listid = null)
    {
        // Define the layout of view section
        $this->layout = 'layout/authrized_user';

        //Getting the user id
        $userid = Yii::app()->user->getID();
        $user   = Yii::app()->user->getModel();

        $friendsserch = $friendname = $datarow = array();
        $friendsserch = FriendHelper::getMyFriendsIds();
         
        $k = 0;
        foreach($friendsserch as $friend)
        {
            $listTakeByMyFriend = ListTaken::model()->find("list_id='$listid' AND user_id = '$friend'");
            if(!empty($listTakeByMyFriend))
            {
                $friendinfo = User::model()->find("id=$friend");
                $friendname[$friend] = StringsHelper::capitalize($friendinfo['firstname']." ".$friendinfo['lastname']);
            }
        }

        asort($friendname);

        $ListTakenMe    = ListTaken::model()->findAll("list_id= '$listid' AND user_id = '$userid'");
        $ListcreatedMe  = ListForm::model()->find("id= '$listid' AND user_id = '$userid' ");
        if ($user->isMember() && empty($ListTakenMe) && empty($ListcreatedMe) )
        {
            $this->redirect(array('takelist','listid'=>$listid));
        }

        // Getting the list detail on the base of listid and user detail of the list creater
        $list_detail  = ListForm::model()->find("id= '$listid'");
        $user_id      = $list_detail['user_id'];
        $listtopicuser  = $list_detail['interest_id'];

        $interestUser   = Interest::model()->find("id=$listtopicuser");
        $listtopic      = $interestUser['interest']   ;

        $userinfo     = User::model()->find("id= '$user_id'");

        // Count the list taken for the no of votes   SELECT count( value ) as mostlychoosen, value,list_id,rating FROM tbl_listrow WHERE list_id =6 GROUP BY value order by mostlychoosen desc
        $list_Taken   = ListTaken::model()->findAll("list_id= '$listid'");

        // Getting all the  data ie values and comment of the user order by most chose entries


        $totalcontributers   = "select count(DISTINCT(user_id)) as totalcontributer from tbl_listrow where list_id=$listid";
        $connection          = Yii::app()->db;
        $command             = $connection->createCommand($totalcontributers);
        $totalcontributer_1  = $command->queryAll();
        $totalcontributer    = $totalcontributer_1[0]['totalcontributer'];


        $sql        = "SELECT count( value ) as mostlychoosen,serial_no,value,list_id,rating FROM tbl_listrow WHERE list_id = $listid GROUP BY value order by serial_no";
         
        $command     = $connection->createCommand($sql);
        $datarow_new = $command->queryAll();


        $allcomment = Array();
        $kk = 0;
        foreach($datarow_new as $socre_new)
        {
            $Score_first = '';

            $rating     = $socre_new['value'];
            $socre_newQQ = ListRow::model()->findAll(
                            'list_id = :x AND value = :y',
                            array(':x' => $listid, ':y' => $rating));
            //CVarDumper::dump(($socre_newQQ), 10, true);
             
            for($con = 0;$con<count($socre_newQQ);$con++)
            {
                $val            =  $socre_newQQ[$con]['serial_no'];
                $Score_first   +=  1/$val;

            }
            $Score_final    = number_format((1/$totalcontributer)*($Score_first),2,'.','');
            $datarow[$kk]   = $socre_new;
            $datarow[$kk]['score'] = $Score_final;
            $kk++;

        }

        if(!empty ($datarow))
        {
            $sort = array();
            foreach($datarow as $k=>$v) {
                $sort['score'][$k] = $v['score'];
                $sort['mostlychoosen'][$k] = $v['mostlychoosen'];
            }

            array_multisort($sort['score'], SORT_DESC, $sort['mostlychoosen'], SORT_DESC,$datarow);
        }
         
        // Getting the data of logged in user
        $Myentry      = ListRow::model()->findAll("list_id = '$listid' AND user_id = '$userid'");


        $this->render('viewresult',array('list_detail'=>$list_detail,'userinfo'=>$userinfo,'list_Taken'=>$list_Taken,'listid'=>$listid,'listtopic'=>$listtopic,'datarow'=>$datarow,'myentry'=>$Myentry,'allcomment'=>$allcomment,'ListTakenMe'=>$ListTakenMe,'friendsserch'=>$friendsserch,'friendname'=>$friendname));
    }



    public function actionviewResultShowAll($listid = null)
    {
        //Getting the user id
        $userid       = Yii::app()->user->getID();
        $baseurl      = Yii::app()->request->baseUrl;

        $friendsserch1 = $friendsserch = array();
        $friendsserch1 = FriendHelper::getMyFriendsIds();

        foreach($friendsserch1 as $ind=>$friendid) {

            $listTakeByMyFriend = ListRow::model()->find("list_id='$listid' AND user_id = '$friendid'");
            if(!empty($listTakeByMyFriend))
            {
                $friendsserch[] = $friendid;
            }
             
        }

        $friendsserch = implode(",",$friendsserch);
        $sql = '';
        // Getting the value from ajax function and make the quary on the base of value
        if(isset($_POST['value']) && $_POST['value']=='Show my friends')
        {
            if($friendsserch)
                $sql        = "SELECT count( value ) as mostlychoosen,serial_no,value,list_id,rating FROM tbl_listrow WHERE list_id = $listid AND user_id IN($friendsserch) GROUP BY value order by serial_no ";
            //$sql        = "SELECT count( value ) as mostlychoosen, value,list_id,rating FROM tbl_listrow WHERE list_id = $listid AND user_id IN($friendsserch) GROUP BY value order by mostlychoosen desc";
            else
                $sql        = "SELECT count( value ) as mostlychoosen,serial_no,value,list_id,rating FROM tbl_listrow WHERE list_id = $listid AND user_id IN(null) GROUP BY value order by serial_no ";
            //$sql        = "SELECT count( value ) as mostlychoosen, value,list_id,rating FROM tbl_listrow WHERE list_id = $listid AND user_id IN(null) GROUP BY value order by mostlychoosen desc";
        }
        if(isset($_POST['value']) && $_POST['value']=='Show all')
        {
            $sql        = "SELECT count( value ) as mostlychoosen,serial_no,value,list_id,rating FROM tbl_listrow WHERE list_id = $listid GROUP BY value order by serial_no ";
            //$sql        = "SELECT count( value ) as mostlychoosen, value,list_id,rating FROM tbl_listrow WHERE list_id = $listid GROUP BY value order by mostlychoosen desc";
        }
        if(isset($_POST['friendid']) && $_POST['friendid'] !='0')
        {
            $friendsserch = $_POST['friendid'];
            $sql        = "SELECT count( value ) as mostlychoosen,serial_no,value,list_id,rating FROM tbl_listrow WHERE list_id = $listid AND user_id IN($friendsserch) GROUP BY value order by serial_no ";
            //$sql          = "SELECT count( value ) as mostlychoosen, value,list_id,rating FROM tbl_listrow WHERE list_id = $listid AND user_id IN($friendsserch) GROUP BY value order by mostlychoosen desc";
        }

        if(isset($_POST['value']) && $_POST['value']=='Show all')
        {
            $totalcontributers   = "select count(DISTINCT(user_id)) as totalcontributer from tbl_listrow where list_id=$listid";
            $connection          = Yii::app()->db;
            $command             = $connection->createCommand($totalcontributers);
            $totalcontributer_1  = $command->queryAll();
            $totalcontributer    = $totalcontributer_1[0]['totalcontributer'];
        }
        else
        {
            $totalcontributer = count(explode(",",$friendsserch));
        }

        // Exicute the query
        $connection  = Yii::app()->db;
        $command     = $connection->createCommand($sql);
        $datarow_new = $command->queryAll();

        $kkk = 0; $allcomment = $datarow  = Array();

        foreach($datarow_new as $socre_new)
        {
            $Score_first = '';

            $rating     = $socre_new['value'];

            if(isset($_POST['value']) && $_POST['value']=='Show all')
            {
                $socre_newQQ = ListRow::model()->findAll(
                                'list_id = :x AND value = :y',
                                array(':x' => $listid, ':y' => $rating));
                //$socre_newQQ    = ListRow::model()->findAll("list_id = '$listid' AND value = '$rating'");
            }
            else
            {
                $criteria = new CDbCriteria;
                $criteria->condition = 'list_id = :x AND value = :y';
                $criteria->params = array(':x' => $listid, ':y' => $rating);
                if($friendsserch) {
                    $criteria->addInCondition('user_id', $friendsserch);
                }
                $socre_newQQ = ListRow::model()->findAll($criteria);
                /*
                 if($friendsserch)
                    $socre_newQQ    = ListRow::model()->findAll("list_id = '$listid' AND value = '$rating' AND user_id IN ($friendsserch)");
                else
                    $socre_newQQ    = ListRow::model()->findAll("list_id = '$listid' AND value = '$rating'");
                */
            }

            for($con = 0;$con<count($socre_newQQ);$con++)
            {
                $val            =  $socre_newQQ[$con]['serial_no'];
                $Score_first   +=  1/$val;
            }
            $Score_final    = number_format((1/$totalcontributer)*($Score_first),2,'.','');
            $datarow[$kkk]   = $socre_new;
            $datarow[$kkk]['score'] = $Score_final;
            $kkk++;

        }

        if(!empty ($datarow))
        {
            $sort = array();
            foreach($datarow as $k=>$v) {
                $sort['score'][$k] = $v['score'];
                $sort['mostlychoosen'][$k] = $v['mostlychoosen'];
            }

            array_multisort($sort['score'], SORT_DESC, $sort['mostlychoosen'], SORT_DESC,$datarow);
        }


        $kkk = 0;
        // Get the value from database of list row table
        foreach($datarow as $value)
        {
            $rating  = $value['value'];

            $criteria = new CDbCriteria;
            $criteria->condition = 'list_id = :x AND value = :y';
            $criteria->params = array(':x' => $listid, ':y' => $rating);
            if (isset($_POST['friendid']) && $_POST['friendid'] !='0') {
                $friendsserch = $_POST['friendid'];
            }
            if ((isset($_POST['friendid']) && $_POST['friendid'] !='0')
                            || ($friendsserch && isset($_POST['value']) && $_POST['value']=='Show my friends'))
            {
                $criteria->addInCondition('user_id', $friendsserch);
            }
            $allcomment[$kkk] = ListRow::model()->findAll($criteria);


            /* for delete
             if (isset($_POST['value']) && $_POST['value']=='Show my friends'){
            if($friendsserch)
                $sql1               = "SELECT * FROM tbl_listrow WHERE list_id = $listid AND user_id IN($friendsserch) AND value = '$rating'";
            else
                $sql1               = "SELECT * FROM tbl_listrow WHERE list_id = $listid AND user_id IN(null) AND value = '$rating'";
            }
            if(isset($_POST['value']) && $_POST['value']=='Show all')
            {
            $sql1              = "SELECT * FROM tbl_listrow WHERE list_id = $listid AND value = '$rating'";
            }

            if(isset($_POST['friendid']) && $_POST['friendid'] !='0')
            {
            $friendsserch      = $_POST['friendid'];
            $sql1              = "SELECT * FROM tbl_listrow WHERE list_id = $listid AND user_id IN($friendsserch) AND value = '$rating'";
            }
            $command               = $connection->createCommand($sql1);
            $allcomment[$kkk]      = $command->queryAll();
            */
            $kkk++;

        }

        //

        // if we get the values than  move it into for loop
        $HTML ="";
        if(!empty($datarow))
        {

            $odd = ''; $count = count($datarow);
            if($count>0)
            {
                for($i = 0;$i<$count;$i++) {
                    if($i%2 ==0 )
                    {
                        $odd = "even";
                    }
                    else
                    {
                        $odd = "odd";

                    }
                    $comment       =  $datarow[$i]['rating'];
                    $rating_coment = "";
                    $length        =  strlen($comment);

                    //if($length<90)
                    {
                        $rating_coment = $comment;
                    }
                     
                     
                    $srno      = $i+1;
                    $tempvalue = $datarow[$i]['value'];
                    $temprate  = $datarow[$i]['rating'];
                    $countcomment = count($allcomment[$i]);

                    $HTML      .= "<tr class='tr1_$odd'>
                    <td class='th1_listentry'> <span class='oder_listentry$odd' style='line-height: 28px;'>$srno </span>  </td>
                    <td class='th2_listentry'> <span class='span_entry_listentry$odd'>$tempvalue</span>   </td>
                    <td class='th3_listentry_comment'>
                    <span  class='span_comment_listentry$odd'>$rating_coment</span> ";
                    if($countcomment>1) {
                        $HTML .= "<span class='commentcount_viewlist' onclick='showviewcomment($i)'> $countcomment comments.. </span>
                        <div id='div_hide$i'  class='hiddendiv_viewcoments' style='display: none;'>
                        <div class='arrowimageviewresult'>
                            ".ImageHelper::imageShow($baseurl.'/images/', 'drop.png')."
                        </div>
                        <div class='comment_user_outerdiv0 scrollbox3'>";
                        $kkyy = 0; foreach($allcomment[$i] as $testvalue){

                            $ratingTest        = $testvalue['rating'];
                            $user_id_comment   = $testvalue['user_id'];
                            $usrercomment      = User::model()->find("id='$user_id_comment'");
                            $name_comment_user = StringsHelper::capitalize($usrercomment['firstname'].' '.$usrercomment['lastname']);
                             
                            $HTML .= "<div class='comment_user_outerdiv1'";
                            if($kkyy==0){
                                 
                                $HTML .= "style='border:0px;'";
                                 
                            }
                            $HTML .= ">   <div class='comment_user_outerdiv2'> <span>$name_comment_user</span></div>
                            <div class='comment_user_outerdiv3'><span>$ratingTest</span></div>
                            </div>";
                            $kkyy++;
                        }
                        $HTML .= "</div>
                        </div>";
                    }
                    $HTML .= "</td> </tr>";


                }
            } if($count>10) {
            } else{

                for($i = $count;$i<10;$i++) {
                    if($i%2 ==0 )
                    {
                        $odd = "even";
                    }
                    else
                    {
                        $odd = "odd";

                    }

                    $HTML .= "<tr class='tr1_$odd'>
                    <td class='th1_listentry'> <span class='oder_listentry$odd'>          </span></td>
                    <td class='th2_listentry'> <span class='span_entry_listentry$odd'>    </span>   </td>
                    <td class='th3_listentry_comment'> <span  class='span_comment_listentry$odd'> </span>   </td>
                    </tr>";

                }
            }
        }
        else
        {
            // return blank values
            for($i = 0;$i<10;$i++) {
                if($i%2 ==0 )
                {
                    $odd = "even";
                }
                else
                {
                    $odd = "odd";

                }

                $HTML .= "<tr class='tr1_$odd'>
                <td class='th1_listentry'> <span class='oder_listentry$odd'>          </span></td>
                <td class='th2_listentry'> <span class='span_entry_listentry$odd'>    </span>   </td>
                <td class='th3_listentry_comment'> <span  class='span_comment_listentry$odd'> </span>   </td>
                </tr>";

            }
        }
        echo $HTML;exit;

    }


    public function actionupdateEntry_old($listid=null)
    {
        // Define the layout of view section
        $this->layout = 'layout/authrized_user';

        //Getting the user id
        $userid       = Yii::app()->user->getID();



        if(isset($_POST['submit'])&& $_POST["entry_0"] != "")
        {

            $mytaken = ListTaken::model()->find("list_id = '$listid' AND user_id = '$userid'");
            if(empty ($mytaken))
            {
                $taken           = new ListTaken;
                $taken->list_id  = $listid;
                $taken->user_id  = $userid;
                $taken->created  = DateHelper::getCurrentSQLDateTime();
                $taken->modified = DateHelper::getCurrentSQLDateTime();
                $taken->save(false);
            }
             
             
            // count the no of rows which are posted from the table.
            $count        = $_POST['no_of_rows'];
            for($i=0;$i<$count;$i++)
            {

                // If entry have value than insert a new row into the database
                if(isset($_POST["entry_$i"]) && $_POST["entry_$i"] != '')
                {
                    $seriol = $_POST["order_$i"];
                    // Define the new object of the model List Row and assing the vale to the attribute of the object;
                    $row            =  ListRow::model()->find("list_id = '$listid' AND user_id = '$userid' AND serial_no= $seriol");

                    if($row){
                        $row->user_id   = $userid;
                        $row->list_id   = $listid;
                        $row->value     = $_POST["entry_$i"];
                        $row->rating    = $_POST["comment_$i"];
                        $row->serial_no = $_POST["order_$i"];
                        $row->col_id    = $i;
                        $row->save(false);
                    }
                    else
                    {
                        $row = New ListRow;
                        $row->user_id   = $userid;
                        $row->list_id   = $listid;
                        $row->value     = $_POST["entry_$i"];
                        $row->rating    = $_POST["comment_$i"];
                        $row->serial_no = $_POST["order_$i"];
                        $row->col_id    = $i;
                        $row->created   = DateHelper::getCurrentSQLDateTime();
                        $row->modified  = DateHelper::getCurrentSQLDateTime();
                        $row->save(false);
                    }
                }
                else
                {
                    $seriol = $_POST["order_$i"];
                    $row    =  ListRow::model()->find("list_id = '$listid' AND user_id = '$userid' AND serial_no = $seriol");
                     
                    if($row){
                        $row->delete();
                    }
                }

            }
             
             
            // Redirect to view list page
            $this->redirect(array('viewResult', 'listid' => $listid));
             
        }

        // Getting the list detail on the base of listid and user detail of the list creater
        $list_detail  = ListForm::model()->find("id= '$listid'");
        $user_id      = $list_detail['user_id'];
        $listtopicuser  = $list_detail['interest_id'];

        $interestUser   = Interest::model()->find("id=$listtopicuser");
        $listtopic      = $interestUser['interest']   ;

        $userinfo     = User::model()->find("id= '$user_id'");

        // Count the list taken for the no of votes
        $list_Taken   = ListTaken::model()->findAll("list_id= '$listid'");

        // Getting all the  data ie values and comment of the user
        $datarow      = ListRow::model()->findAll( array('order'=>'serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$userid)));


        $this->render('updateentry',array('list_detail'=>$list_detail,'userinfo'=>$userinfo,'listtopic'=>$listtopic,'list_Taken'=>$list_Taken,'listid'=>$listid,'datarow'=>$datarow));
    }


    public function actioneditlist($listid = null, $user_id = null)
    {
        // Define the layout of view section
        $this->layout = 'layout/authrized_user';

        //Getting the user id
        $userid = Yii::app()->user->getID();
        $user = Yii::app()->user->getModel();

        // if user post the form then genrate the list
        if (isset ($_POST['instruction']) && $_POST['instruction'] != '')
        {
            $model =  ListForm::model()->findByPk($listid);

            // check author
            if ($model->user_id != $userid && !$user->isAdmin()) exit;

            // inserting all the information in database
            if (isset($_POST['title']))
                $model->title = $_POST['title'];
            $model->interest_id = $_POST['interest'];
            $instruction = $_POST['instruction'];

            if(isset($_SESSION['Listimage']))
                $model->image =  trim($_SESSION['Listimage']);

            //$model->is_official = 0;

            if ($instruction != '')
            {
                $model->instruction = $_POST['instruction'];
            }
            
            if ($user->isAdmin()) {
                $model->badge = !empty($_POST['badge']) ? 1 : 0;
            }
            
            $model->link_title   = !empty($_POST['link_title']) ? $_POST['link_title'] : '';
            
            if (!empty($_POST['link'])) {
                if (stripos($_POST['link'],'http') === false) {
                    $model->link = 'http://'.$_POST['link'];
                } else {
                    $model->link = $_POST['link'];
                }
            } else {
                $model->link = '';
            }

            if(isset($_SESSION['Listimage']))
                unset($_SESSION['Listimage']);

            $inviteFriends = false;
            if ($model->participation != "Everybody" && isset($_POST["participationFriends"]))
            {
                $friendsIds = $_POST["participationFriends"];
                if ($friendsIds != "")
                {
                    $model->participation = $friendsIds;
                    $inviteFriends = true;
                }
                else {
                    $model->participation = "My Friends";
                }
            }

            // Save into database
            if ($model->save(false))
            {
                if ($inviteFriends)
                {
                    $this->inviteFriends(explode(',', $model->participation), $listid);
                }

                $this->redirect(array('viewResult', 'listid' => $listid));
            }
            else
            {
                // Return the post value
                $user = $_POST;
            }
        }

        $list_detail = ListForm::model()->findByPk($listid);

        // if not created this list than redirect
        if ($list_detail['user_id'] != $userid && !$user->isAdmin())
        {
            $this->redirect(array('viewResult', 'listid' => $listid));
        }

        // All the interest from the interest table
        $interest = Interest::model()->findAll(array('order' => 'interest'));

        $participationFriends = array();
        if ($list_detail->participation != "Everybody"
                        && $list_detail->participation != "My Friends")
        {
            $participationFriends = $this->getUsersData($list_detail->participation);
            $list_detail->participation = "My Friends";
        }

        $this->render('listtitle', array(
                        'interest' => $interest,
                        'list' => $list_detail,
                        'friends' => json_encode($participationFriends)
        ));
    }

    public function actionlist_detail($listid=null,$userid=null)
    {
        $this->layout = 'authrized';

        //$userid     = Yii::app()->user->getID();
        $myuserinfo = User::model()->find("id = '$userid'");
         
        $takenlist  = ListTaken::model()->find("list_id = '$listid'");
        $data       = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");

        $count      = count($datacolumn);
        $count      = $count+1;

        $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$listid,':y'=>$userid)));

        $this->render('list_detail',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count,'myuserinfo'=>$myuserinfo,'datarow'=>$datarow,'takenlist'=>$takenlist));
    }



    public function actionupdatelist($listid=null)
    {

        $userid = Yii::app()->user->getID();
        $data = ListForm::model()->find("id = '$listid'");
         
        if(isset($_POST['submit'])&& $_POST['value']['1']!='' && $_POST['rating']['1']!='')
        {
            $row  = ListRow::model()->findAll("list_id = '$listid' AND user_id = '$userid'");
            for($i=0;$i<count($row);$i++)
            {
                $j = $i+1;
                $row[$i]['value']  = $_POST['value'][$j];
                $row[$i]['rating'] = $_POST['rating'][$j];
                $row[$i]->save(false);
            }


            if(isset($_POST['editvalue']) && $_POST['editvalue'][1]!= 'Enter Selection.' && $_POST['editrating'][1]!= 'Enter 1-10 rating(10 highest).' )
            {
                $count = $_POST['hid'];

                for($j=1;$j<=$count;$j++)
                {
                    if($_POST['editvalue'][$j]!='Enter Selection.' && $_POST['editrating'][$j]!='Enter 1-10 rating(10 highest).')
                    {
                        $row            = new ListRow;
                        $row->user_id   = $userid;
                        $row->list_id   = $listid;
                        $row->value     = $_POST['editvalue'][$j];
                        $row->rating    = $_POST['editrating'][$j];
                        $row->serial_no = $j-1;
                        $row->col_id    = $j-1;
                        $row->created   = DateHelper::getCurrentSQLDateTime();
                        $row->modified  = DateHelper::getCurrentSQLDateTime();
                        $row->save(false);
                    }
                }

            }

             
            $arry = array('listid'=>$listid);
            echo json_encode($arry);
            exit;
        }
        $this->render('editlist',array('data'=>$data,'listid'=>$listid));
    }


    public function sortbydate($date1,$date2)
    {

        if(isset($date1))
        {
            $model=ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x AND modified > :y AND modified < :z', 'params'=>array(':x'=>1,':y'=>$date1,':z'=>$date2)));
        }
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');

        return $model;
    }

    public function sortbytopic($listid)
    {
        if(isset($listid))
        {
            $model=ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x AND interest_id = :y', 'params'=>array(':x'=>1,':y'=>$listid)));
        }

        if(isset($listid) && $listid=='Select Topic')
        {
            $model=ListForm::model()->findAll(array('order'=>'priority ASC','condition'=>'is_official =:x ', 'params'=>array(':x'=>1)));
        }

        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');

        return $model;
    }


    public function processingOfficialList($list, $friends_search, $pageno = 0, $limit = 5, $_ajax = false)
    {
        $data = array_fill(0, 2 * $limit, false);
        if (count($list) > 0) {
            for($i = 0; $i < count($list); $i++) {
                $t = $list[$i];
                $r = $t->getAttributes(array('name', 'id', 'image', 'title', 'user_id', 'position', 'favorite', 'badge', 'participation','link_title','link'));
               
                /*if (strlen($r['title']) > 50) {
                    $r['title'] = substr($r['title'], 0, 50) . '...';
                }*/
                 
                $big = ($r['position'] % 4 == 0) || (($r['position'] + 3) % 4 == 0);
                if (!((strpos($r['image'], 'profile/') === 0) || (strpos($r['image'], 'freeworld/') === 0))) {
                    $r['image'] = 'titleimages/list_thumbs/official/'.($big ? 'big' : 'small').'/' . $r['image'];
                }
                 
                $u = User::model()->find('id = '.$r['user_id']);
                $author = StringsHelper::capitalize($u->firstname . ' ' . $u->lastname);
                $r['author'] = strlen($author) < 16 ? $author : $u->firstname;

                $r['rows'] = $t->opinionRaiting();

                $r['friends'] = array();
                if (!empty($friends_search)) {
                    foreach($t->user(array('condition' => 'user.id IN ('.implode(',', $friends_search).')')) as $friend) {
                        $r['friends'][] = $friend->getAttributes(array('id', 'firstname', 'lastname'));
                    }
                }

                $r['count_c'] = $t->countTaken(); // contributors count

                $f = $t->taken(array(
                                'condition'=>'user_id = :user_id',
                                'params' => array(':user_id' => Yii::app()->user->getID())
                ));
                //$r['favorite'] = count($f) > 0 ? $f[0]->favorite : 0;
                $r['is_taken'] = count($f) > 0;
                $r['favorite'] = count($f) > 0 ? $f[0]->favorite : '0';
                $r['is_favorite'] = $r['is_taken'] && (intval($f[0]->favorite) > 0);

                $data[$r['position'] - 1] = $r;
            }
        }

        return array(
                        '_ajax' => $_ajax,
                        'pageno' => $pageno,
                        'pages' => Yii::app()->db->createCommand()->select('max(page_no) as max')->from('tbl_list')->queryScalar(),
                        'data' => $data,
        );
    }


    public function actionofficialToday()
    {
        $this->layout = 'layout/authrized_user';
        $userid = Yii::app()->user->getID();
        $user = User::model()->findByPk($userid);
        $friends_ids = $user->isMember() 
            ? FriendHelper::getMyFriendsIds() 
            : FriendHelper::getMyFollowersIds();

        if (isset($_POST['_ajax']) && ($_POST['_ajax'] == true)) {
            $pageno = $_POST['pageno'];
            $limit  = 5; //PAGE_LIMIT_LISTS;
            $_ajax  = true;
        } else {
            $pageno = isset($_GET['pageno']) ? $_GET['pageno'] : 0;
            $limit  = 5;
            $_ajax  = false;
        }

        $lists = ListForm::model()
        ->with(array('users' => array('select' => 'id')))
        ->findAll(array(
                        'condition' => 'is_official = 1 AND participation = "Everybody" AND page_no = :x',
                        'params' => array(':x' => $pageno + 1),
                        'order' => 'position',
        ));

        //CVarDumper::dump($list, 10, true);
        //Yii::app()->log->routes['db']->enabled = true;
        if ($_ajax) {
            $this->renderPartial('officialtoday', $this->processingOfficialList($lists, $friends_ids, $pageno, $limit, true), false, true);
        } else {
            $this->render('officialtoday', $this->processingOfficialList($lists, $friends_ids, 0, $limit));
        }
    }


    public function actionofficialDateSorter()
    {
        $userid               = Yii::app()->user->getID();
        $logeduser            = User::model()->find("id=$userid");
        $baseurl              = Yii::app()->request->baseUrl;
        $value                = $_POST['value'];
        $pageno               = $_POST['pageno'];
        $limit = 5;
         
        $date = explode("-",$value);
        $year ="20".$date[0];
        $newdate = $year."-".$date[1]."-".$date[2];
         
        $dataCheck         = Official::model()->findAll("created = '$newdate'");
         
        if(empty($dataCheck))
        {
            $HTML = "1";
            $arry = array('html'=>$HTML);
            echo json_encode($arry);
            exit;
        }
         
         
        $cdb1             = new CDbCriteria();
        $cdb1->condition  = "created = '$newdate' AND page_no = $pageno";
        $cdb1->order      = 'type_priority ASC';
        $cdb1->limit      = $limit*2;
         
        $dataPast         = Official::model()->findAll($cdb1);

        $friendsserch     = $searches = $dataOfficial = null;

        $friendsserch = FriendHelper::getMyFriendsIds();

        foreach($dataPast as $val)
        {
            if($val->type=="list"){
                $searches[]     = ListForm::model()->find("id=$val->type_id");
            }
            else{
                $dataOfficial[] = Advertise::model()->find("id=$val->type_id");
            }
             
        }


        $maxpageid     = Yii::app()->db->createCommand()->select('max(page_no) as max')->from('tbl_official')->where("created='$newdate'")->queryScalar();

        $pageno_s = null;
        if($maxpageid!=$pageno) {
            $pageno_s = $pageno + 1;
        }
         
        $HTML ="";
        if(!empty($searches) || count($dataOfficial)>0)
        {
            $odd = 'odd';$var = 0; $j = 0;  $list_count = 0; $ads_count = 0;
            for($kk=0;$kk<$limit*2;$kk++){
                 
                 
                $listtakencount =null;
                 
                if(isset ($searches[$list_count]['title'])){
                    $user_id   = $searches[$list_count]['user_id'];
                    $title     = $searches[$list_count]['title'];
                    $image     = $searches[$list_count]['image'];
                    $listid    = $searches[$list_count]['id'];
                     
                    $userName  = User::model()->find("id=$user_id");
                    $name      = ($userName['firstname'].' '.$userName['lastname']);
                     
                    $listtakencount =null;
                     
                    if($friendsserch){
                        $c = new CDbCriteria();
                        $c->addCondition('list_id = :listid' ,'','AND');
                        $c->params = array('listid' => $listid);
                        $c->addInCondition('user_id',$friendsserch,'AND');
                        $listtakencount = ListTaken::model()->findAll($c);
                    }
                     
                    $ListTaken = new CDbCriteria();
                    $ListTaken->addCondition('list_id = :listid');
                    $ListTaken->params = array('listid' => $listid);
                    $totalListTaken = ListTaken::model()->findAll($ListTaken);
                     
                    $listtake          = count($totalListTaken);
                    $listtakefriend    = count($listtakencount);
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                }
                 
                if(isset ($dataOfficial[$ads_count]['brandname'])){
                    $adslink    = $dataOfficial[$ads_count]['link'];
                    $adstitle   = $dataOfficial[$ads_count]['brandname'];
                    $adsimage   = $dataOfficial[$ads_count]['image'];
                    $adsid      = $dataOfficial[$ads_count]['id'];
                }
                 
                 
                 
                if($odd == 'odd'){
                     
                    if(isset ($searches[$list_count]['title'])){
                        $HTML.=   "<div class='part_left'>
                        <div class='big_img_part'>
                        <div class='nightife'>
                        <a class='nightife2_litle' href='$linkdetatil'>".StringsHelper::capitalize($title)."</a>
                        </div>
                        <div class='big_img'>
                        <a class='nightife2_litle' href='$linkdetatil'>";
                        if(strpos($image, 'profile/') === 0){
                            $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, array('class' => 'officialtodayimg', 
                                                                                                     'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title)); 
                        }
                        elseif(strpos($image, 'freeworld/') === 0){
                            $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, array('class' => 'officialtodayimg', 
                                                                                                     'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                        }else{
                            $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                            array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                        }
                         
                        $HTML .=    "      </a></div>
                        </div>
                        <div class='bottomimgleft'>
                        <div class='alice'>
                         
                        <a class='alisename' href='$baseurl/index.php/user/dashboard?userid=$user_id'>".StringsHelper::capitalize($name)."</a>
                         
                        </div>
                        <div class='alice_right'>
                        <div class='contri'>
                        <span class='integercolor'>$listtake</span>
                        <span class='contrspan'> Contributors</span>
                        </div>
                        <div class='contri'>
                        <span class='integercolor'>$listtakefriend</span>
                        <span class='contrspan'> Friends</span>
                        </div>
                        <div class='contri'>
                        <span class='integercolor'>$listtake</span>
                        <span class='contrspan'> Like</span>
                        </div>
                        </div>
                        </div>
                        </div>";
                         
                         
                    }
                    else
                    {
                        $HTML.=   "<div class='part_left_blank'></div>";
                    }
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                    $list_count++;
                }
                 
                 
                else
                {
                     
                    $odd = 'odd';
                     
                    if(isset ($dataOfficial[$ads_count]['brandname'])){
                        $HTML.=   "<div class='part_right'>

                        <div class='nightife2'>
                        <a class='nightife2_litle' target='_blank' href='$adslink'>$adstitle</a>
                        </div>
                        <div class='image_small'>
                        <a class='nightife2_litle' href='$adslink' target='_blank'>";
                        $HTML .= ImageHelper::imageShow($baseurl."/admin/images/advertise/small/",$adsimage, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $adstitle), $adstitle);
                        $HTML .=    "     </a> </div>

                        <div class='bottom2'>
                        <div class='alice'>
                        <a  class='alisename' href='$adslink' target='_blank'>$adstitle</a>
                        </div>
                        </div>
                        </div>";
                         
                        if($odd=='odd' && $var == 0){
                            $odd='even';$var=$var+1;
                        } else{$var=0;$odd='odd';
                        }   $ads_count++;
                    }
                    else { if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    } $ads_count++;

                    $HTML .= "<div class='part_right_adsblank'> </div>";
                    }
                     
                }
                $j++;
            }

             
            $arry = array('html'=>$HTML,'pageno_s'=>$pageno_s,'maxpageid'=>$maxpageid);
            echo json_encode($arry);
            exit;

            // echo $HTML;
            //exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                            No lists found
                            </div>";
            $arry = array('html'=>$HTML,'pageno_s'=>$pageno_s,'maxpageid'=>$maxpageid);
            echo json_encode($arry);
            exit;
        }
         
        exit;
    }

    public function actionsaveSearchString()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $string                = $_POST['value'];
         
        $logeduser->searchlist = $string;
        $logeduser->save(false);

        if (isset($_POST['recentSearch']) &&  $_POST['recentSearch'] == 'recentSearch')
        {
            $_SESSION['recentSearch'] = $string;
        }
    }

    public function getListsRecentSearchData($pageno,
                    $most = null, $myFriends = null, $users = null, $about = null,
                    $getCount = false)
    {
        $totaldata = null;
        $offset = $pageno * PAGE_LIMIT;
        $limit = PAGE_LIMIT;

        $userid = Yii::app()->user->getID();
        $string = $this->getSearchString();
        $friendsserch = FriendHelper::getMyFriendsIdsStr();
        
        $logeduser = User::model()->find("id = $userid");
        $loged_is_company = $logeduser->isCompany();
        
        if ($loged_is_company) {
            $ids = FriendHelper::getMyFollowersIds();
        } else {
            $ids = FriendHelper::getMyFriendsIds();
        }
        if (empty($ids)) {
            $ids[] = 0;
        }
        if ($getCount)
        {
            $totaldata = "SELECT COUNT(DISTINCT tbl_list.id) AS SRC";
        }
        else
        {
            $this->getRecentSearchDataCaunt();
             
            $totaldata = "
                                SELECT DISTINCT
                                tbl_interest.id AS interest_id,
                                tbl_interest.interest,
                                tbl_list.id,
                                tbl_list.name,
                                tbl_list.title,
                                tbl_list.participation,
                                tbl_list.image,
                                tbl_list.user_id,
                                tbl_list.badge,
                                tbl_list.link_title,
                                tbl_list.link,
                                count(tbl_listtaken.list_id) AS numgh, 
                                (SELECT Count(l.id) FROM tbl_listtaken as l WHERE l.list_id = tbl_listtaken.list_id AND l.user_id IN (".implode(',',$ids).")) AS numgh2

             ";
        }

        $totaldata .= "
                                                FROM tbl_interest
                                                LEFT JOIN tbl_list ON tbl_interest.id = tbl_list.interest_id
                                                LEFT JOIN tbl_listtaken ON tbl_list.id = tbl_listtaken.list_id
                                                WHERE tbl_list.user_id IS NOT NULL";

        // start of users filter
        if ($users)
        {
            $totaldata .= "
                                        AND (tbl_list.id IN (SELECT lt2.list_id FROM tbl_listtaken lt2 WHERE user_id IN  (" . $users . "))";
        }

        // my friends filter
        if ($myFriends == "true")
        {
            if ($friendsserch != "")
            {
                if ($users)
                {
                    $totaldata .= "
                        OR tbl_list.id IN (SELECT lt3.list_id FROM tbl_listtaken lt3 WHERE user_id IN (" . $friendsserch . ")))";
                }
                else
                {
                    $totaldata .= "
                            AND tbl_list.id IN (SELECT lt2.list_id FROM tbl_listtaken lt2 WHERE user_id IN (" . $friendsserch . "))";
                }
            }
            else
            {
                // end of users filter
                if ($users)
                {
                    $totaldata .= ")";
                }
                else
                {
                    $totaldata .= "
                                        AND 1=2"; // no users - no lists
                }
            }
        }
        else
        {
            // end of users filter
            if ($users)
            {
                $totaldata .= ")";
            }
        }

        $totaldata .= "
                        AND (tbl_list.participation = 'Everybody'";
        //if ($loged_is_company) {
            $totaldata .= " OR tbl_list.user_id IN (".$userid.") OR tbl_list.participation LIKE '%".$userid."%')";
        /*} else {
            $totaldata .= " OR tbl_list.user_id IN (";

            if ($friendsserch != "") {
                $totaldata .= $friendsserch . ",";
            } 
            $totaldata .= $userid . "))"; 
        }*/

        // About (interest) filter
        if ($about)
        {
            $totaldata .= "
                        AND tbl_interest.id IN (" . $about . ")";
        }

        if ($string != '')
        {
            // 1. search parts of the words, but don't search in the midle
            // 2. Ignore all special characters and letter case
            // 3. Search lists in the title and in the topic
            // "Sport" -> "(^[sS][^ -\w+]*[pP][^ -\w+]*[oO][^ -\w+]*[rR][^ -\w+]*[tT])|( [sS][^ -\w+]*[pP][^ -\w+]*[oO][^ -\w+]*[rR][^ -\w+]*[tT])"
            //$sqlRegex = $this->makeSqlSearchRegex($string);
            $sqlRegex = RegexHelper::regex($string);

            //$totaldata .= " AND (tbl_interest.interest RLIKE '" . $sqlRegex . "' OR tbl_list.title RLIKE '" . $sqlRegex . "')";
            
            $likes = '';
            foreach (explode(' ',$string) as $str) {
                $likes .= " AND (tbl_list.title LIKE '% ".$str."%' OR tbl_list.title LIKE '".$str."%')";
            }
            $likes = !empty($likes) ? substr($likes, 4) : '';
            $totaldata .= " AND (tbl_interest.interest RLIKE '" . $sqlRegex . "' OR (".$likes."))";
        }

        if (!$getCount)
        {
            $totaldata .= "
                        GROUP BY tbl_list.id";

            // most recent filter
            if ($most == "recent")
            {
                $totaldata .= "
                        ORDER BY tbl_list.created DESC, tbl_list.title ASC";
            }
            else
          {
                $totaldata .= "
                        ORDER BY numgh DESC, numgh2 DESC, tbl_list.title ASC";
            }

            $totaldata .= "
                                LIMIT " . PAGE_LIMIT . " OFFSET " . PAGE_LIMIT * $pageno . ";";
        }
        
        $connection = Yii::app()->db;
        $command2 = $connection->createCommand($totaldata);
        $data = $command2->queryAll();
        
        return $data;
    }

    public function getSearchString()
    {
        $userid = Yii::app()->user->getID();
        $logeduser = User::model()->findByPk($userid);
        $string = $logeduser->searchlist;
        $string = $string;
        $string = preg_replace('/' . RegexHelper::SPECIAL_CHARACTER_REGEX . '/i', '', $string);
        return $string;
    }

    /*
     public function makeSqlSearchRegex($string)
     {
    $reg = SPECIAL_CHARACTER_REGEX . '*';
    for ($i = 0; $i < strlen($string); $i++)
    {
    $reg .= '[' . strtolower($string[$i]) . strtoupper($string[$i]) . ']';
    if ($string[$i] == ' ') $reg .= '*';
    $reg .= SPECIAL_CHARACTER_REGEX . '*';
    }
    $reg = '(^' . $reg . ')|( ' . $reg . ')';
    return $reg;
    }

    public function makeSqlSearchRegex2($string)
    {
    $reg = SPECIAL_CHARACTER_REGEX . '*';
    for ($i = 0; $i < strlen($string); $i++)
    {
    if ($string[$i] == ' ') {
    //$reg .= '[ \W]+([ \W]*[\w]{0,2}[ \W]*)*[ \W]+';
    //$reg .= '([^\w]+([ \W]*[\w]{0,2}[ \W]*)*[^\w]+)';
    $reg .= '[^a-z0-9]+([^a-z0-9]*[a-z0-9]{0,2}[^a-z0-9]+)*';
    } else {
    $reg .= '[' . strtolower($string[$i]) . strtoupper($string[$i]) . ']';
    $reg .= SPECIAL_CHARACTER_REGEX . '*';
    }
    }
    $reg = '(^' . $reg . ')|( ' . $reg . ')';
    return $reg;
    }
    */

    private function getSearchFilterKey($sessionName, $posName) {
        $key = null;

        if (isset($_POST[$posName]))
        {
            $key = $_POST[$posName];
            $_SESSION[$sessionName] = $key;
        }

        if (isset($_SESSION[$sessionName]))
        {
            $key = $_SESSION[$sessionName];
        }

        return $key;
    }

    private function getUsersData($idsStr)
    {
        $usersData = array();
        if ($idsStr)
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'id, firstname, lastname';
            $criteria->addInCondition("id", explode(",", $idsStr));
            $users = User::model()->findAll($criteria);
            foreach ($users as $user)
            {
                array_push($usersData, array(
                "id" => $user->id,
                "name" => $user->getFullName()
                ));
            }
        }

        return $usersData;
    }

    private function getInterestsData($idsStr)
    {
        $interestsData = array();
        if ($idsStr)
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'id, interest';
            $criteria->addInCondition("id", explode(",", $idsStr));
            $interests = Interest::model()->findAll($criteria);
            foreach ($interests as $interest)
            {
                array_push($interestsData, array(
                "id" => $interest->id,
                "name" => $interest->interest
                ));
            }
        }

        return $interestsData;
    }

    public function actionrecentSearch()
    {
        $this->layout = 'layout/authrized_user';
        $string = $this->getSearchString();

        // filter
        $most = $this->getSearchFilterKey('searchListsMost', 'most');
        $myFriends = $this->getSearchFilterKey('searchListsMyFriends', 'myfriends');
        $users = $this->getSearchFilterKey('searchListsUsers', 'users');
        $usersData = $this->getUsersData($users);
        $about = $this->getSearchFilterKey('searchListsAbout', 'about');
        $aboutData = $this->getInterestsData($about);

        // data
        $pageno = 0;
        $data = $this->getListsRecentSearchData($pageno, $most, $myFriends, $users, $about);

        if (count($data) == 0 && $this->action->Id == "recentSearch")
        {
            http_redirect("/index.php/preferences/recentSearchPeople", array(), false);
            exit;
        }
        
        $this->render('recentsearch', array(
                        '_ajax' => false,
                        'string' => $string,
                        'lists' => $data,
                        'pageno' => $pageno,
                        'most' => $most,
                        'myFriends' => $myFriends,
                        'about' => json_encode($aboutData),
                        'users' => json_encode($usersData)
        ));
    }

    public function actionrecentSearchPagination()
    {
        if (isset($_POST["value"]))
        {
            $this->actionsaveSearchString();
        }

        $this->layout = 'layout/ajax';

        // filter
        $most = $this->getSearchFilterKey('searchListsMost', 'most');
        $myFriends = $this->getSearchFilterKey('searchListsMyFriends', 'myfriends');
        $users = $this->getSearchFilterKey('searchListsUsers', 'users');
        $usersData = $this->getUsersData($users);
        $about = $this->getSearchFilterKey('searchListsAbout', 'about');
        $aboutData = $this->getInterestsData($about);
         
        // data
        $pageno = $_POST['pageno'];
        $string = $this->getSearchString();
        $data = $this->getListsRecentSearchData($pageno, $most, $myFriends, $users, $about);
         
        $this->render('recentsearch', array(
                        '_ajax' => true,
                        'string' => $string,
                        'lists' => $data,
                        'pageno' => $pageno,
                        'most' => $most,
                        'myFriends' => $myFriends,
                        'about' => json_encode($aboutData),
                        'users' => json_encode($usersData)
        ));
    }

    public function actionrecentSearchLists() {
        $this->actionrecentSearch();
    }

    public function actionrecentSearchListsPagination() {
        $this->actionrecentSearchPagination();
    }

    private function getRecentSearchDataCaunt()
    {
        // caount of list search
        $most = null;
        $myFriends = null;
        $users = null;
        $about = null;

        if (isset($_SESSION['searchListsMost']))
        {
            $most = $_SESSION['searchListsMost'];
        }
        if (isset($_SESSION['searchListsMyFriends']))
        {
            $myFriends = $_SESSION['searchListsMyFriends'];
        }
        if (isset($_SESSION['searchListsUsers']))
        {
            $users = $_SESSION['searchListsUsers'];
        }
        if (isset($_SESSION['searchListsAbout']))
        {
            $about = $_SESSION['searchListsAbout'];
        }

        $listsCount = $this->getListsRecentSearchData(0, $most, $myFriends, $users, $about, true);
        $_SESSION["searchListCount"] = $listsCount[0]["SRC"];

        // count of people search
        $around = null;
        $studied = null;

        if (isset($_SESSION['searchPeopleAround']))
        {
            $around = $_SESSION['searchPeopleAround'];
        }
        if (isset($_SESSION['searchPeopleStudied']))
        {
            $studied = $_SESSION['searchPeopleStudied'];
        }
         
        $peopleCount = $this->getPeopleRecentSearchData("member", 0, $around, $studied, true);
        $_SESSION["searchPeopleCount"] = $peopleCount[0]["SRC"];

        // count of companies search
        $around = null;
        if (isset($_SESSION['searchCompaniesAround']))
        {
            $around = $_SESSION['searchCompaniesAround'];
        }

        $companiesCount = $this->getPeopleRecentSearchData("company user", 0, $around, null, true);
        $_SESSION["searchCompaniesCount"] = $companiesCount[0]["SRC"];
    }

    public function getPeopleRecentSearchData($type, $pageno, $around = null, $studied = null, $getCount = false)
    {
        $userid = Yii::app()->user->getID();
        $string = $this->getSearchString();
        $searchQuery = "SELECT ";

        if ($getCount)
        {
            $searchQuery .= "COUNT(DISTINCT tbl_user.id) AS SRC";
        }
        else
        {
            $this->getRecentSearchDataCaunt();

            $searchQuery .=
            "   DISTINCT tbl_user.*,
                        IF (tbl_friends.user_id = " . $userid . ", 1, 0) AS is_friend,
                        IF ((tbl_notification.myuser_id = " . $userid . " OR tbl_notification.user_id = " . $userid . ") AND tbl_notification.status = 'waiting', 1, 0) AS is_pending";
        }

        $searchQuery .=
        "
                                    FROM tbl_user
                                    LEFT JOIN tbl_friends ON tbl_user.id = tbl_friends.friend_id AND tbl_friends.user_id = " . $userid . "
                        LEFT JOIN tbl_notification ON (
                        (tbl_user.id = tbl_notification.user_id AND tbl_notification.myuser_id = " . $userid . "
                            AND tbl_notification.status != 'deleted' AND tbl_notification.notification_type = 'friends') OR
                            (tbl_user.id = tbl_notification.myuser_id AND tbl_notification.user_id = " . $userid . "
                                                AND tbl_notification.status != 'deleted' AND tbl_notification.notification_type = 'friends'))
                                                WHERE tbl_user.member = '" . $type . "'";

        if ($string != '')
        {
            // 1. search parts of the words, but don't search in the midle
            // 2. Ignore all special characters and letter case
            //$sqlRegex = $this->makeSqlSearchRegex($string);
            $sqlRegex = RegexHelper::regex($string);
            $searchQuery .= "
                                                                AND CONCAT_WS(' ', tbl_user.firstname, tbl_user.lastname) RLIKE '" . $sqlRegex . "'";
        }

        if ($around != "")
        {
            //$around = preg_replace('/' . RegexHelper::SPECIAL_CHARACTER_REGEX . '/i', '', $around);
            //$sqlRegex = $this->makeSqlSearchRegex($around);
            $sqlRegex = RegexHelper::regex($around, true);
            $searchQuery .= "
                                                        AND ((tbl_user.state RLIKE '" . $sqlRegex . "'
                        OR tbl_user.city RLIKE '" . $sqlRegex . "'
                            OR CONCAT_WS(' ', tbl_user.city, tbl_user.state) RLIKE '" . $sqlRegex . "')";
        }
        else
        {
            if ($studied != "") $searchQuery .= "AND ( 1=0 ";
        }

        if ($studied != "")
        {
            //$studied = preg_replace('/' . RegexHelper::SPECIAL_CHARACTER_REGEX . '/i', '', $studied);
            //$sqlRegex = $this->makeSqlSearchRegex($studied);
            $sqlRegex = RegexHelper::regex($studied, true);
            $searchQuery .= "
                            OR (tbl_user.school RLIKE '" . $sqlRegex . "' OR tbl_user.education_more RLIKE '" . $sqlRegex . "'))";
        }
        else
        {
            if ($around != "") $searchQuery .= ") ";
        }

        if ($getCount)
        {
            $searchQuery .= ";";
        }
        else
        {
            $searchQuery .=
            "ORDER BY is_friend DESC, is_pending DESC, tbl_user.id ASC
                        LIMIT " . PAGE_LIMIT_PIOPLE . " OFFSET " . PAGE_LIMIT_PIOPLE * $pageno . ";";
        }

        $connection = Yii::app()->db;
        $cmd = $connection->createCommand($searchQuery);
        $searchData = $cmd->queryAll();

        return $searchData;
    }

    public function actionrecentSearchPeople()
    {
        $this->layout = 'layout/authrized_user';
        $userid = Yii::app()->user->getID();
        $logeduser = User::model()->find("id = $userid");
        $loged_is_company = $logeduser->isCompany();
        $string = $logeduser->searchlist;
         
        $around = $this->getSearchFilterKey('searchPeopleAround', 'around');
        $studied = $this->getSearchFilterKey('searchPeopleStudied', 'studied');
         
        $data = $this->getPeopleRecentSearchData('member', 0, $around, $studied);

        if ($loged_is_company) {
        	$followers_ids = FriendHelper::getMyFollowersIds();
        } else {
            $friends = FriendHelper::getFriendsByUserId($userid,"member");
            if (is_array($friends) && sizeof($friends) > 0) {
                foreach ($friends as $friend) {
                    if (!empty($friend->friendUser->id)) {
                        $followers_ids[] = $friend->friendUser->id;
                    }
                }
            }
        }
        
        // bug #3701
        for ($i = 0; $i < count($data); $i++)
        {
            if ($data[$i]["id"] == $userid) $data[$i]["style"] = "display: none;";
            else $data[$i]["style"] = "";
            
            if ($loged_is_company) {
            	$data[$i]['is_friend'] = in_array($data[$i]['id'], $followers_ids) ? '1' : '0';
            } else if (!empty($followers_ids)) {
                $data[$i]['is_friend'] = in_array($data[$i]['id'], $followers_ids) ? '1' : '0';
            }
        }
        
        $this->render('recentsearchpeople', array(
                        'string' => $string,
                        'data' => $data,
                        'pageno' => 0,
                        'around' => $around,
                        'studied' => $studied,
                        '_ajax' => false));
    }

    public function actionrecentSearchPeoplePagination()
    {
        if (isset($_POST["value"]))
        {
            $this->actionsaveSearchString();
        }

        if (isset($_POST["pageno"]))
        {
            $around = $this->getSearchFilterKey('searchPeopleAround', 'around');
            $studied = $this->getSearchFilterKey('searchPeopleStudied', 'studied');

            $pageno = $_POST["pageno"];
            $this->layout = 'layout/ajax';
            $userid = Yii::app()->user->getID();
            $logeduser = User::model()->find("id = $userid");
            $loged_is_company = $logeduser->isCompany();
            $string = $logeduser->searchlist;
            	

            $data = $this->getPeopleRecentSearchData('member', $pageno, $around, $studied);
            	
            if ($loged_is_company) {
                $followers_ids = FriendHelper::getMyFollowersIds();
            }
            	
            // bug #3701
            for ($i = 0; $i < count($data); $i++)
            {
                if ($data[$i]["id"] == $userid) $data[$i]["style"] = "display: none;";
                else $data[$i]["style"] = "";

                if ($loged_is_company) {
                    $data[$i]['is_friend'] = in_array($data[$i]['id'], $followers_ids) ? '1' : '0';
                }
            }

            $this->render('recentsearchpeople', array(
                            'string' => $string,
                            'data' => $data,
                            'pageno' => $pageno,
                            'around' => $around,
                            'studied' => $studied,
                            '_ajax' => true));
            /*
             $this->renderPartial('/layouts/layout/ajax_json', array(
                             'content' => array(
                                             'string' => $string,
                                             'data' => $data,
                                             'pageno' => $pageno,
                                             '_ajax' => true,
                                             'searchListCount' => $_SESSION["searchListCount"],
                                             'searchPeopleCount' => $_SESSION["searchPeopleCount"],
                                             'searchCompaniesCount' => $_SESSION["searchCompaniesCount"]
                             )
             ), false, true);
            */
        }
    }

    public function actioninvitePeople()
    {
        $this->layout = 'layout/ajax';

        if (isset($_POST["people_id"]) && isset($_POST["message"]))
        {
            $people_id = $_POST["people_id"];
            $invite_message = $_POST["message"];
            $userid = Yii::app()->user->getID();

            FriendHelper::inviteFriend($userid, $people_id, $invite_message);

            echo "Invited!";
        }

        exit;
    }

    public function actionfollowCompany()
    {
        $this->layout = 'layout/ajax';

        if (isset($_POST["company_id"]))
        {
            $company_id = $_POST["company_id"];
            $userid = Yii::app()->user->getID();

            FriendHelper::followCompany($userid, $company_id);
             
            echo "Followed!";
        }

        exit;
    }

    public function actionrecentSearchCompanies()
    {
        $this->layout = 'layout/authrized_user';
        $userid = Yii::app()->user->getID();
        $logeduser = User::model()->find("id = $userid");
        $string = $logeduser->searchlist;
         
        $around = $this->getSearchFilterKey('searchCompaniesAround', 'around');
         
        $data = $this->getPeopleRecentSearchData('company user', 0, $around);
        $this->render('recentsearchcompanies', array(
                        'user' => $logeduser,
                        'string' => $string,
                        'data' => $data,
                        'pageno' => 0,
                        'around' => $around,
                        '_ajax' => false));
    }

    public function actionrecentSearchCompaniesPagination()
    {
        if (isset($_POST["value"]))
        {
            $this->actionsaveSearchString();
        }

        if (isset($_POST["pageno"]))
        {
            $pageno = $_POST["pageno"];
            $this->layout = 'layout/ajax';
            $userid = Yii::app()->user->getID();
            $logeduser = User::model()->find("id = $userid");
            $string = $logeduser->searchlist;

            $around = $this->getSearchFilterKey('searchCompaniesAround', 'around');

            $data = $this->getPeopleRecentSearchData('company user', $pageno, $around);
            $this->render('recentsearchcompanies', array(
                            'user' => $logeduser,
                            'string' => $string,
                            'data' => $data,
                            'pageno' => $pageno,
                            '_ajax' => true
            ));
        }
    }


    public function actionclearResult()
    {
        $userid  = Yii::app()->user->getID();
        $logeduser = User::model()->find("id=$userid");
        $logeduser->searchlist = '';
        $logeduser->save(false);
        $this->redirect("recentSearch");
    }

    public function recomededPageData($pageno = 0, $count = false)
    {
        $userid = Yii::app()->user->getID();
        $user = User::model()->findByPk($userid);
        
        if ($user->isCompany()) {
            $ids = FriendHelper::getMyFollowersIds();
            
        } else {
            $ids = FriendHelper::getMyFriendsIds();
        }
        $usersIds = '';
        if (!empty($ids)) {
            $ors = '';
            foreach ($ids as $id) {
               $ors .= ' OR user_id = '.$id; 
            }
            $usersIds = 'AND ('.substr($ors, 3).')';
        }

        $recommended = new CDbCriteria();
        $recommended->addCondition("participation = 'Everybody'");
        $recommended->addInCondition('t.user_id', FriendHelper::getMyFriendsIds(), 'OR');
        $recommended->addInCondition("interest_id", $user->interestIds());
        $recommended->addNotInCondition("t.id", $user->takenListsIds());
        $recommended->addCondition("t.user_id != " . $userid . "");

        $recommended->distinct = true;
        $recommended->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended->group = "t.id";
        $recommended->order = "tcc DESC, friendsTakens DESC";

        if ($count)
        {
            return ListForm::model()->with('taken')->count($recommended);
        }

        $recommended->together = true;
        $recommended->offset = $pageno * PAGE_LIMIT_LISTS;
        $recommended->limit = PAGE_LIMIT_LISTS;

        return ListForm::model()->with('taken')->findAll($recommended);
    }

    public function actionrecommended()
    {
        $this->layout = 'layout/authrized_user';
        $userid = Yii::app()->user->getID();

        $data = $this->recomededPageData(0);
        $count = $this->recomededPageData(0, true);

        $friendsserch = FriendHelper::getMyFriendsIds();

        $this->render('recommended', array(
                        'listPage' => 'recommended',
                        'listPageTitle' => "Recommended",
                        'lists' => $data,
                        'listsCount' => $count,
                        'friendsserch' => $friendsserch,
                        'pageno' => 0,
                        '_ajax' => false
        ));
    }

    public function actionrecommendedPagination($string = null)
    {
        if (isset($_POST["pageno"]))
        {
            $pageno = $_POST["pageno"];
            $this->layout = 'layout/ajax';

            $data = $this->recomededPageData($pageno);

            $friendsserch = FriendHelper::getMyFriendsIds();

            $this->render('recommended', array(
                            'lists' => $data,
                            'friendsserch' => $friendsserch,
                            'pageno' => $pageno,
                            '_ajax' => true
            ));
        }
    }


    public function followeesPageData($pageno = 0, $count = false)
    {
        $userid = Yii::app()->user->getID();
        $user = User::model()->findByPk($userid);
        
        /*if ($user->isCompany()) {
            $ids = FriendHelper::getMyFollowersIds();
            
        } else {
            $ids = FriendHelper::getMyFriendsIds();
        }
        $usersIds = '';
        if (!empty($ids)) {
            $ors = '';
            foreach ($ids as $id) {
               $ors .= ' OR user_id = '.$id; 
            }
            $usersIds = 'AND ('.substr($ors, 3).')';
        }*/

        $recommended = new CDbCriteria();
        $recommended->addCondition("participation = 'Everybody'");
        $recommended->addInCondition('t.user_id', FriendHelper::getMyFriendsIds(), 'OR');
        $recommended->addInCondition('t.user_id', FriendHelper::getMyFolloweesIds());

        //$recommended->select = "(SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended->distinct = true;
        $recommended->order = "t.created DESC";
        //$recommended->order = "t.created DESC, tcc DESC, friendsTakens DESC";

        if ($count)
        {
            return ListForm::model()->count($recommended); //->with('taken')
        }
         
        $recommended->together = true;
        $recommended->offset = $pageno * PAGE_LIMIT_LISTS;
        $recommended->limit = PAGE_LIMIT_LISTS;
        	
        return ListForm::model()->findAll($recommended); //with('taken')->
    }

    public function actionfollowees()
    {
        $this->layout = 'layout/authrized_user';
        $userid = Yii::app()->user->getID();

        $data = $this->followeesPageData(0);
        $count = $this->followeesPageData(0, true);

        $friendsserch = FriendHelper::getMyFriendsIds();

        $this->render('recommended', array(
                        'listPage' => 'followees',
                        'listPageTitle' => "Followees",
                        'lists' => $data,
                        'listsCount' => $count,
                        'friendsserch' => $friendsserch,
                        'pageno' => 0,
                        '_ajax' => false
        ));
    }

    public function actionfolloweesPagination()
    {
        if (isset($_POST["pageno"]))
        {
            $pageno = $_POST["pageno"];
            $this->layout = 'layout/ajax';

            $data = $this->followeesPageData($pageno);

            $friendsserch = FriendHelper::getMyFriendsIds();

            $this->render('recommended', array(
                            'lists' => $data,
                            'friendsserch' => $friendsserch,
                            'pageno' => $pageno,
                            '_ajax' => true
            ));
        }
    }


    public function worldPageData($pageno = 0, $count = false)
    {
        $userid = Yii::app()->user->getID();
        $user = User::model()->findByPk($userid);
        
        if ($user->isCompany()) {
            $ids = FriendHelper::getMyFollowersIds();
            
        } else {
            $ids = FriendHelper::getMyFriendsIds();
        }
        $usersIds = '';
        if (!empty($ids)) {
            $ors = '';
            foreach ($ids as $id) {
               $ors .= ' OR user_id = '.$id; 
            }
            $usersIds = 'AND ('.substr($ors, 3).')';
        }

        // last 48h (2 days)
        $recommended48 = new CDbCriteria();
        $recommended48->addCondition("participation = 'Everybody'");
        $recommended48->addInCondition('t.user_id', FriendHelper::getMyFriendsIds(), 'OR');
         
        $recommended48->addCondition("taken.created + INTERVAL 2 DAY >= '"
                        . DateHelper::getCurrentSQLDateTime() . "'");

        $recommended48->distinct = true;
        $recommended48->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended48->group = "t.id";
        $recommended48->order = "tcc DESC, t.title";

        $count48 = ListForm::model()->with('taken')->count($recommended48);
         
        // other
        $recommended = new CDbCriteria();
        $recommended->addCondition("participation = 'Everybody'");
        $recommended->addInCondition('t.user_id', FriendHelper::getMyFriendsIds(), 'OR');
         
        // TODO: refactor
        $ids48 = array();
        $tmpRes = ListForm::model()->with('taken')->findAll($recommended48);
        foreach ($tmpRes as $list) $ids48[] = $list->id;
        $recommended->addNotInCondition("t.id", $ids48);

        $recommended->distinct = true;
        $recommended->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended->group = "t.id";
        $recommended->order = "tcc DESC, friendsTakens DESC";

        if ($count)
        {
            return $count48 + ListForm::model()->with('taken')->count($recommended);
        }

        $limit = PAGE_LIMIT_LISTS;
        $offset = $pageno * $limit;
        $res = array();

        $recommended48->together = true;
        $recommended48->offset = $offset;
        $recommended48->limit = $limit;

        if ($count48 > $offset)
        {
            $res = ListForm::model()->with('taken')->findAll($recommended48);

            if ($count48 < $offset + $limit)
            {
                $limit -= $count48 - $offset;
            }
        }

        if ($count48 < $offset + PAGE_LIMIT_LISTS)
        {
            $offset -= $count48;
            if ($offset < 0) $offset = 0;

            $recommended->together = true;
            $recommended->offset = $offset;
            $recommended->limit = $limit;
             
            $res = array_merge($res, ListForm::model()->with('taken')->findAll($recommended));
        }
        
        if (sizeof($res) > 0) {
            foreach ($res as $k=>$t) {
                if ($t->participation != "Everybody" && $t->user_id != $userid && !in_array($userid,explode(',',$t->participation)))
                {
                    unset($res[$k]);
                    continue;
                }
            }
        }

        return $res;
    }

    public function actionworld()
    {
        $this->layout = 'layout/authrized_user';
        $userid = Yii::app()->user->getID();

        $data = $this->worldPageData(0);
        $count = $this->worldPageData(0, true);

        $friendsserch = FriendHelper::getMyFriendsIds();

        $this->render('recommended', array(
                        'listPage' => 'world',
                        'listPageTitle' => "World",
                        'lists' => $data,
                        'listsCount' => $count,
                        'friendsserch' => $friendsserch,
                        'pageno' => 0,
                        '_ajax' => false
        ));
    }

    public function actionworldPagination()
    {
        if (isset($_POST["pageno"]))
        {
            $pageno = $_POST["pageno"];
            $this->layout = 'layout/ajax';

            $data = $this->worldPageData($pageno);

            $friendsserch = FriendHelper::getMyFriendsIds();

            $this->render('recommended', array(
                            'lists' => $data,
                            'friendsserch' => $friendsserch,
                            'pageno' => $pageno,
                            '_ajax' => true
            ));
        }
    }


    public function friendsPageData($pageno = 0, $count = false)
    {
        $userid = Yii::app()->user->getID();
        $user = User::model()->findByPk($userid);
        
        if ($user->isCompany()) {
            $ids = FriendHelper::getMyFollowersIds();
            
        } else {
            $ids = FriendHelper::getMyFriendsIds();
        }
        $usersIds = '';
        if (!empty($ids)) {
            $ors = '';
            foreach ($ids as $id) {
               $ors .= ' OR user_id = '.$id; 
            }
            $usersIds = 'AND ('.substr($ors, 3).')';
        }
         
        $recommended48 = new CDbCriteria();
        $recommended48->addCondition("participation = 'Everybody'");
        $recommended48->addInCondition('t.user_id', FriendHelper::getMyFriendsIds(), 'OR');

        $friendIds = $user->isMember() ? FriendHelper::getMyFriendsIds() : FriendHelper::getMyFollowersIds();
        if (!$friendIds || count($friendIds) == 0)
        {
            // no friends - no lists
            if ($count) return 0;
            return array();
        }

        // created
        $friendsCondition = new CDbCriteria();
        $friendsCondition->addInCondition('t.user_id', $friendIds);
         
        // or taken by my friends / followers
        $listsTaken = ListTaken::model()->findAll(array(
                        'select' => 'list_id',
                        'condition' => 'user_id IN (' . implode(',', $friendIds) . ')',
                        'distinct' => true
        ));
        $listsTakenIds = array();
        foreach ($listsTaken as $taken)
        {
            $listsTakenIds[] = $taken->list_id;
        }
        $friendsCondition->addInCondition('t.id', $listsTakenIds, 'OR');        
        $recommended48->mergeWith($friendsCondition);
         
        // last 48h (2 days) created and taken
        $timeCondition = new CDbCriteria();
        $timeCondition->addCondition("t.created + INTERVAL 2 DAY >= '"
        		. DateHelper::getCurrentSQLDateTime() . "'");        
        $timeCondition->addCondition("taken.created + INTERVAL 2 DAY >= '"
                        . DateHelper::getCurrentSQLDateTime() . "'", "OR");
        $recommended48->mergeWith($timeCondition);

        $recommended48->distinct = true;
        $recommended48->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended48->group = "t.id";
        $recommended48->order = "tcc DESC, t.title";

        $count48 = ListForm::model()->with('taken')->count($recommended48);
         
        // other
        $recommended = new CDbCriteria();
        $recommended->addCondition("participation = 'Everybody'");
        $recommended->addInCondition('t.user_id', FriendHelper::getMyFriendsIds(), 'OR');

        // created or taken by my friends / followers
        $recommended->mergeWith($friendsCondition);
         
        // TODO: refactor
        $ids48 = array();
        $tmpRes = ListForm::model()->with('taken')->findAll($recommended48);
        foreach ($tmpRes as $list) $ids48[] = $list->id;
        $recommended->addNotInCondition("t.id", $ids48);

        $recommended->distinct = true;
        $recommended->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended->group = "t.id";
        $recommended->order = "tcc DESC, friendsTakens DESC";

        if ($count)
        {
            return $count48 + ListForm::model()->with('taken')->count($recommended);
        }

        $limit = PAGE_LIMIT_LISTS;
        $offset = $pageno * $limit;
        $res = array();

        $recommended48->together = true;
        $recommended48->offset = $offset;
        $recommended48->limit = $limit;

        if ($count48 > $offset)
        {
            $res = ListForm::model()->with('taken')->findAll($recommended48);

            if ($count48 < $offset + $limit)
            {
                $limit -= $count48 - $offset;
            }
        }

        if ($count48 < $offset + PAGE_LIMIT_LISTS)
        {
            $offset -= $count48;
            if ($offset < 0) $offset = 0;

            $recommended->together = true;
            $recommended->offset = $offset;
            $recommended->limit = $limit;

            $res = array_merge($res, ListForm::model()->with('taken')->findAll($recommended));
        }
        
        if (sizeof($res) > 0) {
            foreach ($res as $k=>$t) {
                if ($t->participation != "Everybody" && $t->user_id != $userid && !in_array($userid,explode(',',$t->participation)))
                {
                    unset($res[$k]);
                    continue;
                }
            }
        }

        return $res;
    }

    public function actionfriends()
    {
        $this->layout = 'layout/authrized_user';
        $userid = Yii::app()->user->getID();
        $user = User::model()->findByPk($userid);

        $data = $this->friendsPageData(0);
        $count = $this->friendsPageData(0, true);

        $friendsserch = FriendHelper::getMyFriendsIds();

        $this->render('recommended', array(
                        'listPage' => 'friends',
                        'listPageTitle' => $user->isMember() ? "Friends" : "Followers",
                        'lists' => $data,
                        'listsCount' => $count,
                        'friendsserch' => $friendsserch,
                        'pageno' => 0,
                        '_ajax' => false
        ));
    }

    public function actionfriendsPagination()
    {
        if (isset($_POST["pageno"]))
        {
            $pageno = $_POST["pageno"];
            $this->layout = 'layout/ajax';

            $data = $this->friendsPageData($pageno);
            $friendsserch = FriendHelper::getMyFriendsIds();

            $this->render('recommended', array(
                            'lists' => $data,
                            'friendsserch' => $friendsserch,
                            'pageno' => $pageno,
                            '_ajax' => true
            ));
        }
    }

    public function sortbydateant($date1,$date2)
    {

        if(isset($date1))
        {
            $model=ListForm::model()->findAll(array('order'=>'id DESC','condition'=>'modified > :y AND modified < :z', 'params'=>array(':y'=>$date1,':z'=>$date2)));
        }
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');

        return $model;
    }

    public function sortbytopicant($sorttopic)
    {
        if(isset($sorttopic))
        {
            $model=ListForm::model()->findAll(array('order'=>'id DESC','condition'=>'interest_id = :y', 'params'=>array(':y'=>$sorttopic)));
        }

        if(isset($sorttopic) && $sorttopic=='Select Topic')
        {
            $model=ListForm::model()->findAll(array('order'=>'id DESC','limit'=>'26'));
        }
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');

        return $model;
    }

    public function actionsortByDateWorld()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $baseurl               = Yii::app()->request->baseUrl;
        $string                = $_POST['value'];
        $pageno                = $_POST['pageno'];
         
        $date                  = explode('/', $string);
        $date1                 = $date['0'];
        $date2                 = $date['1'];

        $date1                  = strtotime("$date1");
        $date2                  = strtotime("$date2")+86359;
         
         
         
        $friendsserch        = FriendHelper::getMyFriendsIds();
         
        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        $sql =  "SELECT count( tbl_listtaken.list_id ) AS numgh, tbl_list.name,tbl_list.id, tbl_list.image, tbl_list.title, tbl_list.user_id, tbl_list.is_official, tbl_list.interest_id,
        tbl_list.instruction, tbl_list.favorite, tbl_list.listtype, tbl_list.permission ,tbl_list.priority, tbl_list.created, tbl_list.modified
        FROM tbl_list LEFT JOIN tbl_listtaken ON tbl_list.id = tbl_listtaken.list_id where(tbl_list.modified >= $date1 AND tbl_list.modified <= $date2 ) GROUP BY tbl_list.id ORDER BY numgh DESC,tbl_list.id LIMIT $start,$limit";


        $connection        = Yii::app()->db;
        $command           = $connection->createCommand($sql);
        $searches          = $command->queryAll();


         
        $cdb            = new CDbCriteria();
        $cdb->condition ='modified > :date1 AND modified < :date2';
        $cdb->params = array(
                        'date1' => $date1,
                        'date2' => $date2,
        );
         
        $totalrecord = count(ListForm::model()->findAll($cdb));
        $record              = ceil($totalrecord/$limit);
        $record              = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                 
                $user_id   = $list['user_id'];
                $title     = $list['title'];
                $image     = $list['image'];
                $listid    = $list['id'];
                 
                 
                $userName  = User::model()->find("id=$user_id");
                $name      = ($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $listid);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' => $listid);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$user_id;
                 
                 
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>".StringsHelper::capitalize($title)."</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, array('class' => 'officialtodayimg', 
                                                                                                     'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, array('class' => 'officialtodayimg', 
                                                                                                     'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, array('class' => 'officialtodayimg', 
                                                                                                     'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                     
                    $HTML .=    "      </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>".StringsHelper::capitalize($name)."</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, array('class' => 'officialtodayimg_small', 
                                                                                                 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, array('class' => 'officialtodayimg_small', 
                                                                                                 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }

             
            $arry = array('html'=>$HTML,'maxpageid'=>$record);
            echo json_encode($arry);
            exit;

        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                        No lists found
                        </div>";
            $arry = array('html'=>$HTML,'maxpageid'=>$record);
            echo json_encode($arry);
            exit;
        }
         
        exit;
    }


    public function actionbrowse()
    {
        $this->layout = 'layout/authrized_user';
        $interest     = Interest::model()->findAll();
        $this->render('browse',array('interest'=>$interest,));
    }


    public function actionbrowsecat($inters)
    {
        $workingdir = Yii::app()->params['workingdir'];
        $csv        = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));

        $this->layout = 'authrized';
        $interest     = Interest::model()->findAll();
        $userid       = Yii::app()->user->getID();
        $takenlist    = ListTaken::model()->findAll("user_id = '$userid'");
        $counttaken   = count($takenlist);
        $interestname = Interest::model()->find("id='$inters'");
        $arry         = array();
        $friends      = Friend::model()->findAll(array('condition'=>'(user_id = :id1 AND status =:state) OR (friend_id = :id1 AND status =:state)', 'params'=>array(':id1' => $userid,':state' => 'accept')));
        foreach($friends as $value)
        {
            if($value->user_id==$userid)
            {
                $arry[]      = $value->friend_id;
            }
            else
            {
                $arry[]      = $value->user_id;
            }
        }

        if (isset($_GET["pageno"]))
        {
            $pageno =  $_GET["pageno"];
        }
        else {
            $pageno=0;
        }

        $limit=10;
        $start = ($pageno*$limit);


        $criteria = new CDbCriteria();
        $criteria->condition = "interest_id = $inters";
        $criteria->limit = $limit;
        $criteria->offset = $start;
        $criteria->order = 'id DESC';

        $data        = ListForm::model()->findAll($criteria);
        $totalrecord = count(ListForm::model()->findAll("interest_id='$inters'"));
        $record      = ceil($totalrecord/$limit);
        $record      = $record-1;

        $sorttopic    = null;
        if(isset($_POST['User']) && $_POST['User']['birthdate']!='')
        {

            $sortdte  = $_POST['User']['birthdate'];
            $date     = explode('/', $sortdte);
            $date1    = $date['0'];
            $date2    = $date['1'];

            $_SESSION['date1'] = $date1;
            $_SESSION['date2'] = $date2;

            $date1    = strtotime("$date1");
            $date2    = strtotime("$date2");

            if($date1==$date2)
            {
                $date2 = strtotime('+1 day', $date2);
            }

            $criteria = new CDbCriteria();
            $criteria->condition = "modified > :y AND modified < :z AND interest_id = :x";
            $criteria->limit = $limit;
            $criteria->offset = $start;
            $criteria->order = 'id DESC';
            $criteria->params = array(
                            'x' => $inters,
                            'y' => $date1,
                            'z' => $date2,
            );

            $data        = ListForm::model()->findAll($criteria);


            $crit = new CDbCriteria();
            $crit->condition = "modified > :y AND modified < :z AND interest_id = :x";
            $crit->order = 'id DESC';
            $crit->params = array(
                            'x' => $inters,
                            'y' => $date1,
                            'z' => $date2,
            );

            $totalrecord = count(ListForm::model()->findAll($crit));
            $record      = ceil($totalrecord/$limit);
            $record      = $record-1;
            $interestname = 'date';
            $countlist    = count($data);
            $this->render('browsecat',array('countlist'=>$countlist,'interestnm'=>$interestname,'csv'=>$csv,'arry'=>$arry,'interest'=>$interest,'counttaken'=>$counttaken,'takenlist'=>$takenlist,'data'=>$data,'post'=>$sorttopic,'pageno'=>$pageno,'record'=>$record));
            exit;
        }

        $countlist    = count($data);


        $this->render('browsecat',array('countlist'=>$countlist,'interestname'=>$interestname,'csv'=>$csv,'interest'=>$interest,'arry'=>$arry,'counttaken'=>$counttaken,'takenlist'=>$takenlist,'data'=>$data,'post'=>$sorttopic,'pageno'=>$pageno,'record'=>$record));

    }



    public function actionbrowssearch($inters=null,$search=null)
    {
        $this->layout = 'authrized';
        $interest     = Interest::model()->findAll();
        $userid       = Yii::app()->user->getID();

        if(isset($_GET['search']) && $_GET['search']!='')
        {
            $arry         = array();
            $friends      = Friend::model()->findAll(array('condition'=>'(user_id = :id1 AND status =:state) OR (friend_id = :id1 AND status =:state)', 'params'=>array(':id1' => $userid,':state' => 'accept')));
            foreach($friends as $value)
            {
                if($value->user_id==$userid)
                {
                    $arry[]      = $value->friend_id;
                }
                else
                {
                    $arry[]      = $value->user_id;
                }
            }
            $sorttopic  = '';
            $keyword    = $_GET['search'];
             
            if (isset($_GET["pageno"]))
            {
                $pageno  =  $_GET["pageno"];
            }
            else
            {
                $pageno = 0;
            }
            $limit   = 10;
            $start   = ($pageno*$limit);
             
            $criteria            = new CDbCriteria();
            $criteria->condition ='interest_id =:y AND title LIKE :title ';
            $criteria->limit     = $limit;
            $criteria->offset    = $start;
            $criteria->params    = array(
                            'y' => $inters,
                            'title' => "%$keyword%",
            );
            $searches            = ListForm::model()->findAll($criteria);
             
            $cdb                 = new CDbCriteria();
            $cdb->condition      ='interest_id =:y AND title LIKE :title ';
            $cdb->params         = array(
                            'y' => $inters,
                            'title' => "%$keyword%",
            );
             
            $totalrecord         = count(ListForm::model()->findAll($cdb));
            $record              = ceil($totalrecord/$limit);
            $record              = $record-1;
             
             
            if(isset($_POST['Interest']) && $_POST['Interest']['user']!='')
            {
                $sorttopic    = $_POST['Interest']['user'];
                if($sorttopic!='Select Topic')
                {
                    $criteria     = new CDbCriteria();
                    $criteria->condition  ='interest_id = :interest_id AND title LIKE :title';
                    $criteria->params     = array(
                                    'interest_id'     => $sorttopic,
                                    'title'           => "%$keyword%",
                    );
                    $criteria->limit      = $limit;
                    $criteria->offset     = $start;
                    $searches             = ListForm::model()->findAll($criteria);


                    $cdb                  = new CDbCriteria();
                    $cdb->condition       = 'interest_id = :interest_id AND title LIKE :title';
                    $cdb->params          = array(
                                    'interest_id'     => $sorttopic,
                                    'title'           => "%$keyword%",
                    );

                    $totalrecord          = count(ListForm::model()->findAll($cdb));
                    $record               = ceil($totalrecord/$limit);
                    $record               = $record-1;
                }
                $this->render('browssearch',array('searches'=>$searches,'keyword'=>$keyword,'post'=>$sorttopic,'arry'=>$arry,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
                exit;
            }

            if(isset($_POST['User']) && $_POST['User']['birthdate']!='')
            {
                $sortdte     = $_POST['User']['birthdate'];
                $date        = explode('/', $sortdte);
                $date1       = $date['0'];
                $date2       = $date['1'];

                $_SESSION['date1'] = $date1;
                $_SESSION['date2'] = $date2;

                $date1       = strtotime("$date1");
                $date2       = strtotime("$date2");

                if($date1==$date2)
                {
                    $date2 = strtotime('+1 day', $date2);
                }


                $criteria            = new CDbCriteria();
                $criteria->condition = 'modified > :x AND modified < :y AND interest_id = :interest_id AND title LIKE :title';
                $criteria->params    = array(
                                'x'              => $date1,
                                'y'              => $date2,
                                'interest_id'    => $inters,
                                'title'          => "%$keyword%",
                );
                $criteria->limit     = $limit;
                $criteria->offset    = $start;
                $searches            = ListForm::model()->findAll($criteria);


                $cdb                 = new CDbCriteria();
                $cdb->condition      ='modified > :x AND modified < :y AND interest_id = :interest_id  AND title LIKE :title';
                $cdb->params         = array(
                                'x'              => $date1,
                                'y'              => $date2,
                                'interest_id'    => $inters,
                                'title'          => "%$keyword%",
                );

                $totalrecord = count(ListForm::model()->findAll($cdb));
                $record      = ceil($totalrecord/$limit);
                $record      = $record-1;
                $this->render('browssearch',array('searches'=>$searches,'keyword'=>$keyword,'post'=>$sorttopic,'arry'=>$arry,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
                exit;
            }



            $this->render('browssearch',array('searches'=>$searches,'keyword'=>$keyword,'post'=>$sorttopic,'arry'=>$arry,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
            exit;
        }
        $this->render('browssearch',array('interest'=>$interest,'arry'=>$arry,));
    }

    public function actiondeletelist($listid=NULL,$user_id=null)
    {
        $baseurl = Yii::app()->request->baseUrl;
        $userid  = Yii::app()->user->getID();
        $list    = ListForm::model()->find("id = $listid");
        $image   = $list['image'];
        $imagess = explode('/',$image);

        /*if($imagess[0]=='titleimages')
         {
        $unlink  = unlink($baseurl.'/images/'.$image);
        }*/

        if($user_id==$list['user_id'])
        {
            $list->delete();
            $listrow   = ListRow::model()->deleteAll("list_id = $listid");
            $listtake  = ListTaken::model()->deleteAll("list_id = $listid");
        }
        else
        {
            $listrow   = ListRow::model()->deleteAll("list_id = $listid  AND user_id = $user_id");
            $listtake  = ListTaken::model()->deleteAll("list_id = $listid AND user_id = $user_id");
        }
        $this->redirect('recommended');
    }

    public function actionsearchfriend()
    {
        $userid     = Yii::app()->user->getID();
        $baseurl    = Yii::app()->request->baseUrl;
        $keyword    = $_POST['title'];
        $strfrnd    = $_POST['frnd'];
        $sql        = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword%'OR lastname LIKE '%$keyword%')AND id IN  ($strfrnd)";
        $connection = Yii::app()->db;
        $command    = $connection->createCommand($sql);
        $searches   = $command->queryAll();
        //echo "<pre>";print_r($searches);exit;
        $HTML ="";
        if(!empty($searches))
        {

            for($i=0;$i<count($searches);$i++)
            {
                $friend_id  = $searches[$i]['id'];
                $name       = $searches[$i]['firstname'].' '.$searches[$i]['lastname'];
                $gender     = $searches[$i]['gender'];
                $image      = $searches[$i]['image'];
                $HTML.=   "<div class='invitefriends_newdesdiv221'>
                <div class='invitefriends_newdesdivcheckbox'>
                <input type='checkbox' value='$friend_id' name='friends[]' id='friends_$friend_id'>
                </div>
                <div class='invitefriends_newdesdiv22img'>";
                if($image!='')
                {
                    $HTML .= ImageHelper::imageShow($baseurl."/images/profile/user_thumbs/dashboard/",$image, 0, 0, 
                                                    array('class' => 'auth_header_user_img_invite'));
                }
                else
                {
                    if($gender=='male'|| $gender=='Male')
                    {
                        $HTML .= ImageHelper::imageShow($baseurl."/images/","defaultboy.jpg", 0, 0, 
                                                        array('class' => 'auth_header_user_img_invite'));
                    }
                    elseif($gender=='female' || $gender=='Female')
                    {
                        $HTML .= ImageHelper::imageShow($baseurl."/images/","defaultgirl.jpg", 0, 0, 
                                                        array('class' => 'auth_header_user_img_invite'));
                    }
                    else
                    {
                        $HTML .= ImageHelper::imageShow($baseurl."/images/","default.jpg", 0, 0, 
                                                        array('class' => 'auth_header_user_img_invite'));
                    }
                }
                 
                $HTML .=    " </div>
                <div class='invitefriends_newdesdiv22name'>
                <span>$name</span>
                </div>
                </div>
                <input type='hidden' value='$strfrnd' id='strfrnd' />
                ";

                 
            }
            echo $HTML;
            exit;
        }

        else
        {
            $result =0;
            echo json_encode($result);
            exit;
        }
    }

    public function actionsearchfriendlist()
    {
        $userid     = Yii::app()->user->getID();
        $baseurl    = Yii::app()->request->baseUrl;
        $keyword    = $_POST['title'];
        $strfrnd    = $_POST['frnd'];
        $sql        = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword%'OR lastname LIKE '%$keyword%')AND id IN  ($strfrnd)";
        $connection = Yii::app()->db;
        $command    = $connection->createCommand($sql);
        $searches   = $command->queryAll();
        //echo "<pre>";print_r($searches);exit;
        $HTML ="";
        if(!empty($searches))
        {

            for($i=0;$i<count($searches);$i++)
            {
                $friend_id  = $searches[$i]['id'];
                $name       = $searches[$i]['firstname'].' '.$searches[$i]['lastname'];
                $gender     = $searches[$i]['gender'];
                $image      = $searches[$i]['image'];
                $HTML.=   "<div class='listtitle_newdesdiv221'>
                <div class='listtitle_newdesdivcheckbox'>
                <input type='checkbox' value='$friend_id' name='friends[]' id='friends_$friend_id'>
                </div>
                <div class='listtitle_newdesdiv22img'>";
                if($image!='')
                {
                    $HTML .= ImageHelper::imageShow($baseurl."/images/profile/user_thumbs/dashboard/",$image, 0, 0, 
                                                    array('class' => 'auth_header_user_img'));
                }
                else
                {
                    if($gender=='male'|| $gender=='Male')
                    {
                        $HTML .= ImageHelper::imageShow($baseurl."/images/","defaultboy.jpg", 0, 0, 
                                                        array('class' => 'auth_header_user_img'));
                    }
                    elseif($gender=='female' || $gender=='Female')
                    {
                        $HTML .= ImageHelper::imageShow($baseurl."/images/","defaultgirl.jpg", 0, 0, 
                                                        array('class' => 'auth_header_user_img'));
                    }
                    else
                    {
                        $HTML .= ImageHelper::imageShow($baseurl."/images/","default.jpg", 0, 0, 
                                                        array('class' => 'auth_header_user_img'));
                    }
                }
                 
                $HTML .=    " </div>
                <div class='listtitle_newdesdiv22name'>
                <span>$name</span>
                </div>
                </div>
                <input type='hidden' value='$strfrnd' id='strfrnd' />
                ";

                 
            }
            echo $HTML;
            exit;
        }

        else
        {
            $result =0;
            echo json_encode($result);
            exit;
        }
    }

    public function actioncomposemsg()
    {
        $userid      = Yii::app()->user->getID();
        $baseurl     = Yii::app()->request->baseUrl;
        $keyword_1    = explode(" ",$_POST['title']);
         
        if(count($keyword_1)==1)
        {
            $keyword = $keyword_1[0];
        }
        else
        {
            $keyword1 = $keyword_1[0];
            $keyword2 = $keyword_1[1];
        }
        $strfrnd     = rtrim($_POST['frnd'],',');
        $arraystring = $_POST['arrayid'];
        $array       = explode(",",$arraystring);
        if($strfrnd){
            if(isset ($_POST['recent'])){
                if(count($keyword_1)==1){
                    $sql         = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword%'OR lastname LIKE '%$keyword%') order by firstname";
                }
                else{
                    $sql         = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword1%'OR lastname LIKE '%$keyword1%' OR firstname LIKE '%$keyword2%'OR lastname LIKE '%$keyword2%') order by firstname";
                }
            }
            else
            {
                $sql         = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword%'OR lastname LIKE '%$keyword%')AND id IN  ($strfrnd)";
            }
        }
        else if(isset ($_POST['recent'])){
            if(count($keyword_1)==1){
                $sql         = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword%'OR lastname LIKE '%$keyword%') order by firstname";
            }
            else{
                $sql         = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword1%'OR lastname LIKE '%$keyword1%' OR firstname LIKE '%$keyword2%'OR lastname LIKE '%$keyword2%') order by firstname";
            }
        }
        else
        {
            echo '1';
            exit;
        }
        $connection = Yii::app()->db;
        $command    = $connection->createCommand($sql);
        $searches   = $command->queryAll();
         
        $html ="";
        if(!empty($searches))
        {
             
            $html.="<ul  class='interest_div_ul' id='headercompgmsg'>";
            for($i=0;$i<count($searches);$i++)
            {
                $friend_id  = $searches[$i]['id'];
                $name       = $searches[$i]['firstname'].' '.$searches[$i]['lastname'];
                $gender     = $searches[$i]['gender'];
                $image      = $searches[$i]['image'];
                $id         = $searches[$i]['id'];
                $time       = time();
                 
                if(!in_array($id, $array))
                {
                    if($image!='')
                    {
                        $img .= ImageHelper::imageShow($baseurl."/images/profile/user_thumbs/header/",$image, 0, 0, 
                                                       array('class' => 'composemsgimge'));
                    }
                    else
                    {
                        if($gender=='male'|| $gender=='Male')
                        {
                            $img .= ImageHelper::imageShow($baseurl."/images/","defaultboy.jpg", 0, 0, 
                                                           array('class' => 'composemsgimge'));
                        }
                        elseif($gender=='female' || $gender=='Female')
                        {
                            $img .= ImageHelper::imageShow($baseurl."/images/","defaultgirl.jpg", 0, 0, 
                                                           array('class' => 'composemsgimge'));
                        }
                        else
                        {
                            $img .= ImageHelper::imageShow($baseurl."/images/","default.jpg", 0, 0, 
                                                           array('class' => 'composemsgimge'));
                        }
                    }
                    if(isset ($_POST['recent'])){
                        $html.= " <li class='interest_div_li_msg' id='li$id.$time' onmouseover='nameautofilover_interest(\"li$id.$time\")' onmouseout='nameautofilout_interest(\"li$id.$time\")' onclick='interstname_recentSearch(this)'>$img <span class='imagemsgspan'>$name</span><input type='hidden' value='$id' class='testclassheader'/></li> ";
                    }
                    else{
                        $html.= " <li class='interest_div_li_msg' id='li$id.$time' onmouseover='nameautofilover_interest(\"li$id.$time\")' onmouseout='nameautofilout_interest(\"li$id.$time\")' onclick='interstnameuser(this)'>$img <span class='imagemsgspan'>$name</span><input type='hidden' value='$id' class='testclassheader'/></li> ";
                    }
                }
            }
            $html.="</ul>";
            echo $html;
            exit;
        }
        else
        {
            echo '1';
            exit;
        }

    }


    public function actionsearch_profilefriends()
    {
         
        $baseurl    = Yii::app()->request->baseUrl;
        $keyword    = $_POST['title'];
        $profileid  = $_POST['profileid'];
        $strfrnd    = $_POST['frnd'];

        if($keyword != '' && $strfrnd !='')
        {
            //$sql        = "SELECT * FROM tbl_friends WHERE friend_name LIKE '%$keyword%' AND user_id = '$profileid' AND status = 'accept'";
            $sql        = "SELECT * FROM tbl_user WHERE (firstname LIKE '%$keyword%'OR lastname LIKE '%$keyword%')AND id IN  ($strfrnd)";
            $connection = Yii::app()->db;
            $command    = $connection->createCommand($sql);
            $searches   = $command->queryAll();
             
            $HTML ="";
            if(!empty($searches))
            {

                for($i=0;$i<count($searches);$i++)
                {
                    $friend_id  = $searches[$i]['id'];
                    $name       = $searches[$i]['firstname'].' '.$searches[$i]['lastname'];
                    $friendlistchosen = ListForm::model()->find(array('condition'=>'id =:x ', 'params'=>array(':x'=>$searches[$i]['display_list'])));
                    if(!empty($friendlistchosen))
                    {
                        $chosenlist = substr($friendlistchosen->title, 0, 32);
                    }
                    else { $chosenlist =  "No list chosen to display";
                    }
                     
                    $latestlist  = ListTaken::model()->find(array('order'=>'id DESC','condition'=>'user_id =:x ', 'params'=>array(':x'=>$friend_id)));
                    if(!empty($latestlist))
                    {
                        $friendlatestlisttaken = ListForm::model()->find(array('condition'=>'id =:x ', 'params'=>array(':x'=>$latestlist['list_id'])));
                        $friendtakenlist = substr($friendlatestlisttaken->title, 0, 32);
                    }
                    else
                    {
                        $friendtakenlist = 'No List Taken';
                    }
                     
                     
                    $HTML       .=  "<div class='dashboard_mainlowerdiv_left_friendinfo'>
                    <a href='$baseurl/index.php/user/dashboard?userid=$friend_id'>
                    <span class='dashboard_mainlowerdiv_left_friendname'>$name</span>
                    </a>
                    <span style='float: left;'>
                    ".ImageHelper::imageShow($baseurl."/images/freeworld/","arrow-1.png", 0, 0, 
                                             array('style' => 'background-color: white; height: 17px;'))."
                    </span>
                    <span class='dashboard_mainlowerdiv_left_friendlisttitle' style='width:248px;'>$chosenlist</span>
                    <span class='dashboard_mainlowerdiv_left_frienddplist'style='width:250px;'>$friendtakenlist</span>
                    </div>";
                }
                echo $HTML;
                exit;
            }
        }
        else
        {
            $result =0;
            echo json_encode($result);
            exit;
        }
    }
     
    public function actionlistvote($listid=null)
    {
        $workingdir = Yii::app()->params['workingdir'];
        $csv        = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));
        $listvote   = new ListVoting;
        $userid     = Yii::app()->user->getID();
        $listvote->user_id  = $userid;
        $listvote->list_id  = $listid;
        $listvote->vote     = 1;
        $listvote->created  = DateHelper::getCurrentSQLDateTime();
        $listvote->modified = DateHelper::getCurrentSQLDateTime();
        $listvote->save(false);
        Yii::app()->user->setFlash('colorsusseces', $csv[83]);
        $this->redirect(array('listdetail','listid'=>$listid));
    }

    public function actiondeletefavlist()
    {
        if(isset($_GET['listid']))
        {
            $listid = $_GET['listid'];
            $userid = $_GET['userid'];
            $type = $_GET['type'];
            if($type == 'simple')
            {
                $list = ListForm::model()->find("id = '$listid' AND user_id = '$userid'");
                $list->favorite = 0;
                $list->save(false);
                if($list->save(false))
                {
                    $this->redirect(array('user/dashboard'));
                }
            }
            else
            {
                $listtaken = ListTaken::model()->find("list_id = '$listid' AND user_id = '$userid'");
                $listtaken->favorite = 0;
                $listtaken->save(false);
                if($listtaken->save(false))
                {
                    $this->redirect(array('user/dashboard'));
                }
            }
        }
    }
     
    public function actionAddListToFavorite()
    {
        if(isset($_POST['list_id']))
        {
            $list_id = $_POST['list_id'];
            $user_id = Yii::app()->user->getID();

            $list = ListTaken::model()->find('list_id = :x AND user_id = :y', array('x' => $list_id, 'y' => $user_id));

            if (count($list) === 1) {
                $list->favorite = 1;
                $list->save(false);
                $list->refresh();
                $this->renderPartial('/layouts/layout/ajax_json', array('content' => (int)$list->favorite), false, true);
            } else {
                $this->renderPartial('/layouts/layout/ajax_json', array('content' => -1), false, true);
            }
        }
    }
     
    public function actionRemoveListFromFavorite()
    {
        if(isset($_POST['list_id']))
        {
            $list_id = $_POST['list_id'];
            $user_id = Yii::app()->user->getID();

            $list = ListTaken::model()->find('list_id = :x AND user_id = :y', array('x' => $list_id, 'y' => $user_id));

            if (count($list) === 1) {
                $list->favorite = 0;
                $list->save(false);
                $list->refresh();
                $this->renderPartial('/layouts/layout/ajax_json', array('content' => (int)$list->favorite), false, true);
            } else {
                $this->renderPartial('/layouts/layout/ajax_json', array('content' => -1), false, true);
            }
        }
    }
     
     
    public function actionofficialsearch($search=null)
    {
        $this->layout = 'authrized';
        $interest     = Interest::model()->findAll();
        $userid       = Yii::app()->user->getID();
        $arry         = array();
        if(isset($_GET['search']) && $_GET['search']!='')
        {

            $friends = Friend::model()->findAll(array('condition'=>'(user_id = :id1 AND status =:state) OR (friend_id = :id1 AND status =:state)', 'params'=>array(':id1' => $userid,':state' => 'accept')));
            foreach($friends as $value)
            {
                if($value->user_id==$userid)
                {
                    $arry[]      = $value->friend_id;
                }
                else
                {
                    $arry[]      = $value->user_id;
                }
            }

            $sorttopic = '';
            $keyword    = $_GET['search'];

            if (isset($_GET["pageno"]))
            {
                $pageno =  $_GET["pageno"];
            }
            else
            {
                $pageno=0;
            }
            $limit=10;
            $start = ($pageno*$limit);

            $criteria            = new CDbCriteria();
            $criteria->condition ='is_official = :is_official AND title LIKE :title';
            $criteria->params = array(
                            'is_official' => 1,
                            'title' => "%$keyword%",
            );
            $criteria->limit     = $limit;
            $criteria->offset    = $start;
            $searches        = ListForm::model()->findAll($criteria);
             
             

            $cdb            = new CDbCriteria();
            $cdb->condition ='is_official = :is_official AND title LIKE :title';
            $cdb->params    = array(
                            'is_official' => 1,
                            'title' => "%$keyword%",
            );

            $totalrecord = count(ListForm::model()->findAll($cdb));
            $record      = ceil($totalrecord/$limit);
            $record      = $record-1;


            if(isset($_POST['Interest']) && $_POST['Interest']['user']!='')
            {
                $sorttopic = $_POST['Interest']['user'];
                if($sorttopic!='Select Topic')
                {
                    $criteria   =new CDbCriteria();
                    $criteria->condition ='is_official = :is_official AND interest_id = :interest_id AND title LIKE :title';
                    $criteria->params = array(
                                    'interest_id' => $sorttopic,
                                    'is_official' => 1,
                                    'title' => "%$keyword%",
                    );
                    $criteria->limit = $limit;
                    $criteria->offset = $start;
                    $searches        = ListForm::model()->findAll($criteria);


                    $cdb = new CDbCriteria();
                    $cdb->condition ='is_official = :is_official AND interest_id = :interest_id AND title LIKE :title';
                    $cdb->params = array(
                                    'interest_id' => $sorttopic,
                                    'is_official' => 1,
                                    'title' => "%$keyword%",
                    );
                    //echo "<pre>";print_r($cdb);exit;
                    $totalrecord = count(ListForm::model()->findAll($cdb));
                    $record      = ceil($totalrecord/$limit);
                    $record      = $record-1;
                }

                $this->render('officialsearch',array('searches'=>$searches,'keyword'=>$keyword,'arry'=>$arry,'post'=>$sorttopic,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
                exit;

            }

            if(isset($_POST['User']) && $_POST['User']['birthdate']!='')
            {
                $sortdte  = $_POST['User']['birthdate'];
                $date     = explode('/', $sortdte);
                $date1    = $date['0'];
                $date2    = $date['1'];

                $_SESSION['date1'] = $date1;
                $_SESSION['date2'] = $date2;


                $date1    = strtotime("$date1");
                $date2    = strtotime("$date2");

                if($date1==$date2)
                {
                    $date2 = strtotime('+1 day', $date2);
                }

                $criteria   =new CDbCriteria();
                $criteria->condition ='is_official = :is_official AND modified > :x AND modified < :y AND title LIKE :title';
                $criteria->params = array(
                                'x' => $date1,
                                'y' => $date2,
                                'is_official' => 1,
                                'title' => "%$keyword%",
                );
                $criteria->limit  = $limit;
                $criteria->offset = $start;
                $searches         = ListForm::model()->findAll($criteria);

                $cdb = new CDbCriteria();
                $cdb->condition ='is_official = :is_official AND modified > :x AND modified < :y AND title LIKE :title';
                $cdb->params = array(
                                'x' => $date1,
                                'y' => $date2,
                                'is_official' => 1,
                                'title' => "%$keyword%",
                );
                //echo "<pre>";print_r($cdb);exit;
                $totalrecord = count(ListForm::model()->findAll($cdb));
                $record      = ceil($totalrecord/$limit);
                $record      = $record-1;

                $this->render('officialsearch',array('searches'=>$searches,'keyword'=>$keyword,'arry'=>$arry,'post'=>$sorttopic,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
                exit;
            }

            $this->render('officialsearch',array('searches'=>$searches,'keyword'=>$keyword,'arry'=>$arry,'post'=>$sorttopic,'interest'=>$interest,'pageno'=>$pageno,'record'=>$record));
            exit;
        }
        $this->render('officialsearch',array('interest'=>$interest,'arry'=>$arry,));
    }

    public function actionfriendsearchlist($search=null)
    { //echo "<pre>";print_r($_POST);exit;
        $this->layout = 'authrized';
        $interest     = Interest::model()->findAll();
        $userid       = Yii::app()->user->getID();
        $sorttopic  =  '';
        $arry         = array();
        $friends      = Friend::model()->findAll(array('condition'=>'(user_id = :id1 AND status =:state) OR (friend_id = :id1 AND status =:state)', 'params'=>array(':id1' => $userid,':state' => 'accept')));
        foreach($friends as $value)
        {
            if($value->user_id==$userid)
            {
                $arry[]      = $value->friend_id;
            }
            else
            {
                $arry[]      = $value->user_id;
            }
        }


        /*******For Taken**********/

        if (isset($_GET["pageno"]))
        {
            $pageno =  $_GET["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        $take = new CDbCriteria();
        $take->addInCondition('user_id',$arry,'OR');
        $take->limit = $limit;
        $take->offset = $start;
        $take->group = 'list_id';

        $cdb = new CDbCriteria();
        $cdb->addInCondition('user_id',$arry,'OR');
        $cdb->group = 'list_id';


        $listtaken   = ListTaken::model()->findAll($take);
        $totalrecord = count(ListTaken::model()->findAll($cdb));
        $record      = ceil($totalrecord/$limit);
        $record      = $record-1;


        /*******For Created**********/

        if (isset($_GET["pageno_list"]))
        {
            $pageno_list =  $_GET["pageno_list"];
        } else {
            $pageno_list=0;
        }
        $limit_list=10;
        $start_list = ($pageno_list*$limit_list);


        $value = new CDbCriteria();
        $value->addInCondition('user_id',$arry,'OR');
        $value->condition ='title LIKE :title';
        $value->params = array(
                        'title' => "%$search%",
        );
        $value->limit  = $limit_list;
        $value->offset = $start_list;


        $cdbtake = new CDbCriteria();
        $cdbtake->addInCondition('user_id',$arry,'OR');
        $cdbtake->condition ='title LIKE :title';
        $cdbtake->params = array(
                        'title' => "%$search%",
        );

        $list   = ListForm::model()->findAll($value);
        $totalrecord_list = count(ListForm::model()->findAll($cdbtake));
        $record_list      = ceil($totalrecord_list/$limit_list);
        $record_list      = $record_list-1;

        if(isset($_POST['Interest']) && $_POST['Interest']['user']!='' && $_POST['Interest']['user']!='Select Topic')
        {
            $sorttopic = $_POST['Interest']['user'];
            if($sorttopic!='Select Topic')
            {

                $value = new CDbCriteria();
                $value->addInCondition('user_id',$arry,'OR');
                $value->condition ='interest_id = :interest_id AND title LIKE :title';
                $value->params = array(
                                'title' => "%$search%",
                                'interest_id' => $sorttopic,
                );
                $value->limit  = $limit_list;
                $value->offset = $start_list;


                $cdbtake = new CDbCriteria();
                $cdbtake->addInCondition('user_id',$arry,'OR');
                $cdbtake->condition ='interest_id = :interest_id AND title LIKE :title';
                $cdbtake->params = array(
                                'title' => "%$search%",
                                'interest_id' => $sorttopic,
                );

                $list   = ListForm::model()->findAll($value);
                $totalrecord_list = count(ListForm::model()->findAll($cdbtake));
                $record_list      = ceil($totalrecord_list/$limit_list);
                $record_list      = $record_list-1;
            }
            $this->render('friendsearchlist',array('interest'=>$interest,'listtaken'=>$listtaken,'list'=>$list,'arry'=>$arry,'pageno'=>$pageno,'record'=>$record,
                            'pageno_list'=>$pageno_list,'record_list'=>$record_list,'sorttopic'=>$sorttopic,'post'=>$sorttopic,));
            exit;
        }
         
         
        if(isset($_POST['User']) && $_POST['User']['birthdate']!='')
        {
             
            $sortdte  = $_POST['User']['birthdate'];
            $date     = explode('/', $sortdte);
            $date1    = $date['0'];
            $date2    = $date['1'];

            $_SESSION['date1'] = $date1;
            $_SESSION['date2'] = $date2;


            $date1    = strtotime("$date1");
            $date2    = strtotime("$date2");

            if($date1==$date2)
            {
                $date2 = strtotime('+1 day', $date2);
            }

            $value = new CDbCriteria();
            $value->addInCondition('user_id',$arry,'OR');
            $value->condition ='modified > :x AND modified < :y AND title LIKE :title ';
            $value->params = array(
                            'x' => $date1,
                            'y' => $date2,
                            'title' => "%$search%",
            );
            $value->limit  = $limit_list;
            $value->offset = $start_list;


            $cdbtake = new CDbCriteria();
            $cdbtake->addInCondition('user_id',$arry,'OR');
            $cdbtake->condition ='modified > :x AND modified < :y AND title LIKE :title';
            $cdbtake->params = array(
                            'x' => $date1,
                            'y' => $date2,
                            'title' => "%$search%",
            );


            $list   = ListForm::model()->findAll($value);

            $totalrecord_list = count(ListForm::model()->findAll($cdbtake));
            $record_list      = ceil($totalrecord_list/$limit_list);
            $record_list      = $record_list-1;

            $this->render('friendsearchlist',array('interest'=>$interest,'listtaken'=>$listtaken,'list'=>$list,'arry'=>$arry,'pageno'=>$pageno,'record'=>$record,
                            'pageno_list'=>$pageno_list,'record_list'=>$record_list,'sortdte'=>$sortdte,'date1'=>$date1,'date2'=>$date2,));
            exit;

        }
         

        $this->render('friendsearchlist',array('interest'=>$interest,'listtaken'=>$listtaken,'list'=>$list,'arry'=>$arry,'pageno'=>$pageno,'record'=>$record,
                        'pageno_list'=>$pageno_list,'record_list'=>$record_list,));
    }


    public function actionheadermessage($search=null)
    {

        $userid   = Yii::app()->user->getID();
        $recever  = explode(',',$_POST['recever']);
        $count    = count($recever)-1;
        $resultss = null;
        for($kk = 0;$kk<$count;$kk++)
        {
            $message = new Message();
            $message->receiver_id = $recever[$kk];
            $message->body        = $_POST['body'];
            $message->sender_id   = $userid;
            $message->subject     = $_POST['body'];
            $message->is_read     = '0';
            $message->created_at  = DateHelper::getCurrentSQLDateTime();
            $resultss =  $message->save(false);

        }
        if($resultss)
        {
            $result = 'save';
            echo json_encode($result);
            exit;
        }
        else
        {
            $result = '1';
            echo json_encode($result);
            exit;
        }
    }



    public function actionshowByTopicOfficial()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $baseurl               = Yii::app()->request->baseUrl;
        $string                = $_POST['value'];
         
         
        $friendsserch        = FriendHelper::getMyFriendsIds();

        $interests = explode(",",rtrim($string,','));

        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        $criteria            = new CDbCriteria();
        $criteria->condition ='is_official = 1';
        if($interests[0]!='')
            $criteria->addInCondition('interest_id',$interests,'AND');
        $criteria->limit = $limit;
        $criteria->offset = $start;
        $criteria->order = 'priority ASC';

        $searches            = ListForm::model()->findAll($criteria);
         
        $cdb            = new CDbCriteria();
        $cdb->condition ='is_official = :is_official';
        $cdb->params    = array(
                        'is_official' => 1,
        );
        if($interests[0]!='')
            $cdb->addInCondition('interest_id',$interests,'AND');
         
        $totalrecord = count(ListForm::model()->findAll($cdb));
        $record              = ceil($totalrecord/$limit);
        $record              = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                $userName  = User::model()->find("id=$list->user_id");
                $name      = ($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $list->id);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' => $list->id);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$list->id;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$list->user_id;
                 
                $title      = $list->title;
                $image      = $list->image;
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>".StringsHelper::capitalize($title)."</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($list->image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                    elseif(strpos($list->image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                     
                    $HTML .=    "      </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>".StringsHelper::capitalize($name)."</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$list->id;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($list->image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($list->image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             
            if(count($searches)>0) {
                $HTML .=  "  <div class='clearboth'>&nbsp;</div>";
                $HTML .=    "  <div class='paging_officialpage' id='pagingdiv'>";
                $k = $pageno - 1; $j = $pageno + 1;
                if($pageno>0){
                    $HTML .= "<span class='cursorpointer' onclick='officialshuffel($k)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","previousimg.png")."
                             </span> ";
                }if($record>0 && $record!=$pageno) {

                    $HTML .= "<span class='cursorpointer'  onclick='officialshuffel($j)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","nextimg.png")."
                             </span> ";
                }
                $HTML .= "</div>";
            }
            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                                                No lists found
                                                </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }




    public function actionshowByTopicRecommended()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $userInterest          = explode(",",$logeduser->interest);
        $baseurl               = Yii::app()->request->baseUrl;
        $listTaken             = ListTaken::model()->findAll("user_id=$userid");
        $string                = $_POST['value'];
         
         
        $friendsserch         =  $myListTaken = $interests = $interests_new = $interests_new2 = null;
         
        foreach($listTaken as $listTaken1){
            $myListTaken[]      = $listTaken1->list_id;
        }
         
        foreach($userInterest as $value=>$ind){
            $interests_new[]              = Interest::model()->find("interest = '$ind'");
             
        }
         
        foreach($interests_new as $interests1){
            $interests_new2[]              = $interests1['id'];
        }
         

        $friendsserch = FriendHelper::getMyFriendsIds();

        if($string){
            $interests = explode(",",rtrim($string,','));
        }
        else
        {
            $interests = $interests_new2;
        }
         
         
        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id != :userid' ,'','AND');
        $criteria->params = array('userid' => $userid);
        $criteria->addInCondition('interest_id',$interests,'AND');
        $criteria->addNotInCondition('id',$myListTaken,'AND');
        $criteria->limit = $limit;
        $criteria->offset = $start;

        $criteria2 = new CDbCriteria();
        $criteria2->addCondition('user_id != :userid' ,'','AND');
        $criteria2->params = array('userid' => $userid);
        $criteria2->addInCondition('interest_id',$interests,'AND');
        $criteria2->addNotInCondition('id',$myListTaken,'AND');


        $searches          = ListForm::model()->findAll($criteria);
        $totalrecord       = count(ListForm::model()->findAll($criteria2));
        $record            = ceil($totalrecord/$limit);
        $record            = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                $userName  = User::model()->find("id=$list->user_id");
                $name      = ($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $list->id);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' => $list->id);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$list->id;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$list->user_id;
                 
                $title      = $list->title;
                $image      = $list->image;
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>".StringsHelper::capitalize($title)."</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($list->image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                    elseif(strpos($list->image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                     
                    $HTML .=    "      </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>".StringsHelper::capitalize($name)."</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$list->id;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($list->image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($list->image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "     </a> </div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             
            if(count($searches)>0) {
                $HTML .=  "  <div class='clearboth'>&nbsp;</div>";
                $HTML .=    "  <div class='paging_officialpage' id='pagingdiv'>";
                $k = $pageno - 1; $j = $pageno + 1;
                if($pageno>0){
                    $HTML .= "<span class='cursorpointer' onclick='officialshuffel($k)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","previousimg.png")."
                             </span> ";
                }if($record>0 && $record!=$pageno) {

                    $HTML .= "<span class='cursorpointer'  onclick='officialshuffel($j)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","nextimg.png")."
                              </span> ";
                }
                $HTML .= "</div>";
            }
            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                                No lists found
                                </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }

    public function actionshowByTopicFriends()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $baseurl               = Yii::app()->request->baseUrl;
        $listTaken             = ListTaken::model()->findAll("user_id=$userid");
        $string                = $_POST['value'];

        $friendsserch = FriendHelper::getMyFriendsIds();
        $friendsserchIN = FriendHelper::getMyFriendsIdsStr();

        $interests = rtrim($string,',');
         
        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);


        if($interests){
            $sql  = "select  tbl_list.id,tbl_list.image,tbl_list.title,tbl_list.interest_id,tbl_list.user_id from tbl_list  LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id=tbl_list.id where (tbl_listtaken.user_id IN ($friendsserchIN) OR tbl_list.user_id IN ($friendsserchIN)) AND tbl_list.interest_id IN($interests)  group by tbl_list.id  LIMIT $start,$limit";
            $sql2 = "select  tbl_list.id,tbl_list.image,tbl_list.title,tbl_list.interest_id,tbl_list.user_id from tbl_list  LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id=tbl_list.id where (tbl_listtaken.user_id IN ($friendsserchIN) OR tbl_list.user_id IN ($friendsserchIN)) AND tbl_list.interest_id IN($interests)  group by tbl_list.id ";
        }
        else{
            $sql  = "select  tbl_list.id,tbl_list.image,tbl_list.title,tbl_list.interest_id,tbl_list.user_id from tbl_list  LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id=tbl_list.id where (tbl_listtaken.user_id IN ($friendsserchIN) OR tbl_list.user_id IN ($friendsserchIN)) group by tbl_list.id  LIMIT $start,$limit";
            $sql2 = "select  tbl_list.id,tbl_list.image,tbl_list.title,tbl_list.interest_id,tbl_list.user_id from tbl_list  LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id=tbl_list.id where (tbl_listtaken.user_id IN ($friendsserchIN) OR tbl_list.user_id IN ($friendsserchIN)) group by tbl_list.id ";

        }
         
        $connection        = Yii::app()->db;
        $command           = $connection->createCommand($sql);
        $searches          = $command->queryAll();
         

        $command2           = $connection->createCommand($sql2);
        $totalrecord        = count($command2->queryAll());
         
        $record              = ceil($totalrecord/$limit);
        $record              = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                $user_id   = $list['user_id'];
                $title     = StringsHelper::capitalize($list['title']);
                $image     = $list['image'];
                $listid    = $list['id'];
                 
                 
                 
                $userName  = User::model()->find("id='$user_id'");
                $name      = StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $listid);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' =>$listid);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$user_id;
                 
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>".StringsHelper::capitalize($title)."</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => StringsHelper::capitalize($title)), StringsHelper::capitalize($title));
                    }
                     
                    $HTML .=    "      </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>".StringsHelper::capitalize($name)."</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             
            if(count($searches)>0) {
                $HTML .=  "  <div class='clearboth'>&nbsp;</div>";
                $HTML .=    "  <div class='paging_officialpage' id='pagingdiv'>";
                $k = $pageno - 1; $j = $pageno + 1;
                if($pageno>0){
                    $HTML .= "<span class='cursorpointer' onclick='officialshuffel($k)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","previousimg.png")."
                             </span> ";
                }if($record>0 && $record!=$pageno) {

                    $HTML .= "<span class='cursorpointer'  onclick='officialshuffel($j)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","nextimg.png")."
                              </span> ";
                }
                $HTML .= "</div>";
            }
            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                                    No lists found
                                    </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }




    public function actionshowByTopicWorld()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $baseurl               = Yii::app()->request->baseUrl;
        $string                = $_POST['value'];
         
         
        $friendsserch        = FriendHelper::getMyFriendsIds();

        $interests = explode(",",rtrim($string,','));
         
         
        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        $criteria            = new CDbCriteria();
        if($interests[0]!='')
            $criteria->addInCondition('interest_id',$interests,'AND');
        $criteria->limit = $limit;
        $criteria->offset = $start;
        $criteria->order = 'id DESC';

        $searches       = ListForm::model()->findAll($criteria);
         
        $cdb            = new CDbCriteria();
         
        if($interests[0]!='')
            $cdb->addInCondition('interest_id',$interests,'AND');
         
        $totalrecord = count(ListForm::model()->findAll($cdb));
        $record              = ceil($totalrecord/$limit);
        $record              = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                $userName  = User::model()->find("id=$list->user_id");
                $name      = StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $list->id);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' => $list->id);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$list->id;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$list->user_id;
                 
                $title      = StringsHelper::capitalize($list->title);
                $image      = $list->image;
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($list->image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                    elseif(strpos($list->image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$list->id;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($list->image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($list->image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$list->image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             
            if(count($searches)>0) {
                $HTML .=  "  <div class='clearboth'>&nbsp;</div>";
                $HTML .=    "  <div class='paging_officialpage' id='pagingdiv'>";
                $k = $pageno - 1; $j = $pageno + 1;
                if($pageno>0){
                    $HTML .= "<span class='cursorpointer' onclick='officialshuffel($k)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","previousimg.png")."    
                              </span> ";
                }if($record>0 && $record!=$pageno) {
                    $HTML .= "<span class='cursorpointer'  onclick='officialshuffel($j)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","nextimg.png")."
                              </span> ";
                }
                $HTML .= "</div>";
            }
            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                                    No lists found
                                    </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }

    public function actionshowAllFavorite()
    {
        $this->layout      = 'layout/authrized_user';
        $userid            = Yii::app()->user->getID();

        if(isset($_POST['submit']) && $_POST['submit']!='')
        {
            $PostedData  = array_filter($_POST);
            foreach($PostedData as $ind=>$value)
            {
                //echo "<pre>";print_r($PostedData);exit;
                $keys   =   array();
                $keys   = explode("_",$ind);

                if(isset($keys[1])){
                    $keyn = $keys[1];
                     
                    if(isset($PostedData["shortlist_$keyn"]) )
                    {
                        $valu = explode(",",$value);

                        $list      = ListTaken::model()->find("list_id = '$keyn' AND user_id = $userid");
                        if($valu[1]=='off'){
                            $list->shortlist = "off";
                        }
                        else{
                            $list->shortlist = "";
                        }
                        $list->save(false);
                         
                    }
                }
            }
        }

        $friendsserch      = '';
         

        if (isset($_GET["pageno"]))
        {
            $pageno =  $_GET["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        $totaldata = "SELECT count( tbl_listtaken.list_id ) AS numgh, tbl_list.name, tbl_list.id, tbl_list.image, tbl_list.title, tbl_list.user_id,tbl_list.interest_id FROM tbl_list LEFT JOIN tbl_listtaken ON tbl_list.id = tbl_listtaken.list_id GROUP BY tbl_list.id ORDER BY numgh DESC , tbl_list.id LIMIT $start,$limit";

        $sql       = "SELECT * FROM tbl_listtaken where user_id=$userid AND favorite = 1";


        $connection        = Yii::app()->db;
        $command           = $connection->createCommand($sql);
        $searches1         = $command->queryAll();

        $command2          = $connection->createCommand($totaldata);
        $searches2         = $command2->queryAll();

        $kk = 0;$officialPageList = array();
        foreach($searches2 as $hotvalue)
        {


            foreach($searches1 as $coolvalue)
            {
                if($coolvalue['list_id'] == $hotvalue['id'])
                {
                    $officialPageList[$kk]['name']        = $hotvalue['name'];
                    $officialPageList[$kk]['id']          = $hotvalue['id'];
                    $officialPageList[$kk]['image']       = $hotvalue['image'];
                    $officialPageList[$kk]['title']       = $hotvalue['title'];
                    $officialPageList[$kk]['user_id']     = $hotvalue['user_id'];
                    $officialPageList[$kk]['interest_id'] = $hotvalue['interest_id'];
                }
            }
            $kk++;
        }




        $criteria2 = new CDbCriteria();
        $criteria2->condition ='user_id = :userid AND favorite = :favorite';
        $criteria2->params = array(
                        'userid' => $userid,
                        'favorite' => 1,
        );

         
        $totalrecord       = count(ListTaken::model()->findAll($criteria2));
        $record            = ceil($totalrecord/$limit);
        $record            = $record-1;

        $friendsserch = FriendHelper::getMyFriendsIds();

        $this->render('showallfavorite',array('officialPageList'=>$officialPageList,'friendsserch'=>$friendsserch,'pageno'=>$pageno,'record'=>$record));
    }

    public function actionunfavourList($listid)
    {
        $userid    = Yii::app()->user->getID();
        $list      = ListTaken::model()->find("list_id = '$listid' AND user_id = $userid");
        $list->favorite = "0";
        $list->save(false);
        $this->redirect("showAllFavorite");
    }

    public function actionshowAllFavoritePagination()
    {

        $userid            = Yii::app()->user->getID();
        $baseurl           = Yii::app()->request->baseUrl;
        $friendsserch      = '';
         

        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        $totaldata = "SELECT count( tbl_listtaken.list_id ) AS numgh, tbl_list.name, tbl_list.id, tbl_list.image, tbl_list.title, tbl_list.user_id,tbl_list.interest_id FROM tbl_list LEFT JOIN tbl_listtaken ON tbl_list.id = tbl_listtaken.list_id GROUP BY tbl_list.id ORDER BY numgh DESC , tbl_list.id LIMIT $start,$limit";

        $sql = "SELECT * FROM tbl_listtaken where user_id=$userid AND favorite = 1";


        $connection        = Yii::app()->db;
        $command           = $connection->createCommand($sql);
        $searches1         = $command->queryAll();

        $command2          = $connection->createCommand($totaldata);
        $searches2         = $command2->queryAll();

        $kk = 0;$officialPageList = array();
        foreach($searches2 as $hotvalue)
        {


            foreach($searches1 as $coolvalue)
            {
                if($coolvalue['list_id'] == $hotvalue['id'])
                {
                    $officialPageList[$kk]['name']        = $hotvalue['name'];
                    $officialPageList[$kk]['id']          = $hotvalue['id'];
                    $officialPageList[$kk]['image']       = $hotvalue['image'];
                    $officialPageList[$kk]['title']       = $hotvalue['title'];
                    $officialPageList[$kk]['user_id']     = $hotvalue['user_id'];
                    $officialPageList[$kk]['interest_id'] = $hotvalue['interest_id'];

                }
            }
            $kk++;
        }

         


        $criteria2 = new CDbCriteria();
        $criteria2->condition ='user_id = :userid AND favorite = :favorite';
        $criteria2->params = array(
                        'userid' => $userid,
                        'favorite' => 1,
        );

         
        $totalrecord       = count(ListTaken::model()->findAll($criteria2));
        $record            = ceil($totalrecord/$limit);
        $record            = $record-1;

        $friendsserch = FriendHelper::getMyFriendsIds();

        $HTML ="";
        if(!empty($officialPageList))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($officialPageList as $list)
            {
                 
                $user_id   = $list['user_id'];
                $title     = StringsHelper::capitalize($list['title']);
                $image     = $list['image'];
                $listid    = $list['id'];
                 
                 
                $userName  = User::model()->find("id=$user_id");
                $name      = StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $listid);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' => $listid);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$user_id;
                 
                 
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             

            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                        No lists found
                        </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }


    public function actionshowByTopicFavorite()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $baseurl               = Yii::app()->request->baseUrl;
        $string                = $_POST['value'];
         
        if($string){
            $logeduser->searchlist = $string;
            $logeduser->save(false);
        }
         
        $friendsserch        = FriendHelper::getMyFriendsIds();

        $interests = rtrim($string,',');
         
        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);

        if($interests){
            $sql  = "select tbl_list.name,tbl_list.id, tbl_list.image,tbl_list.interest_id, tbl_list.title, tbl_list.user_id from tbl_list LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id= tbl_list.id where tbl_listtaken.favorite=1  and tbl_listtaken.user_id=$userid and tbl_list.interest_id IN($interests) LIMIT $start,$limit";
            $sql2 = "select tbl_list.name,tbl_list.id, tbl_list.image,tbl_list.interest_id, tbl_list.title, tbl_list.user_id from tbl_list LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id= tbl_list.id where tbl_listtaken.favorite=1  and tbl_listtaken.user_id=$userid and tbl_list.interest_id IN($interests)";
        }
        else{
            $sql  = "select tbl_list.name,tbl_list.id, tbl_list.image,tbl_list.interest_id, tbl_list.title, tbl_list.user_id from tbl_list LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id= tbl_list.id where tbl_listtaken.favorite=1  and tbl_listtaken.user_id=$userid  LIMIT $start,$limit";
            $sql2 = "select tbl_list.name,tbl_list.id, tbl_list.image,tbl_list.interest_id, tbl_list.title, tbl_list.user_id from tbl_list LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id= tbl_list.id where tbl_listtaken.favorite=1  and tbl_listtaken.user_id=$userid";

        }
         
        $connection        = Yii::app()->db;
        $command           = $connection->createCommand($sql);
        $searches          = $command->queryAll();
         

        $command2           = $connection->createCommand($sql2);
        $totalrecord        = count($command2->queryAll());
         
        $record              = ceil($totalrecord/$limit);
        $record              = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                $user_id   = $list['user_id'];
                $title     = StringsHelper::capitalize($list['title']);
                $image     = $list['image'];
                $listid    = $list['id'];
                 
                 
                 
                $userName  = User::model()->find("id=$user_id");
                $name      = StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $listid);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' =>$listid);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$user_id;
                 
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "     </a> </div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             
            if(count($searches)>0) {
                $HTML .=  "  <div class='clearboth'>&nbsp;</div>";
                $HTML .=    "  <div class='paging_officialpage' id='pagingdiv'>";
                $k = $pageno - 1; $j = $pageno + 1;
                if($pageno>0){
                    $HTML .= "<span class='cursorpointer' onclick='officialshuffel($k)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","previousimg.png")."
                              </span> ";
                }if($record>0 && $record!=$pageno) {

                    $HTML .= "<span class='cursorpointer'  onclick='officialshuffel($j)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","nextimg.png")."
                              </span> ";
                }
                $HTML .= "</div>";
            }
            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                                    No lists found
                                    </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }
    
    public function actionsearchOnFriends()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $baseurl               = Yii::app()->request->baseUrl;
        $string                = $_POST['value'];
         
        if($string){
            $logeduser->searchlist = $string;
            $logeduser->save(false);
        }
         
        $friendsserch = FriendHelper::getMyFriendsIds();
        $friendsserchIN = FriendHelper::getMyFriendsIdsStr();
         
        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);


         
        $sql  = "select  tbl_list.id,tbl_list.image,tbl_list.title,tbl_list.interest_id,tbl_list.user_id from tbl_list  LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id=tbl_list.id where (tbl_listtaken.user_id IN ($friendsserchIN) OR tbl_list.user_id IN ($friendsserchIN))  AND tbl_list.title LIKE '%$string%'  group by tbl_list.id  LIMIT $start,$limit";
        $sql2 = "select  tbl_list.id,tbl_list.image,tbl_list.title,tbl_list.interest_id,tbl_list.user_id from tbl_list  LEFT JOIN tbl_listtaken ON tbl_listtaken.list_id=tbl_list.id where (tbl_listtaken.user_id IN ($friendsserchIN) OR tbl_list.user_id IN ($friendsserchIN))  AND tbl_list.title LIKE '%$string%'  group by tbl_list.id ";
         
         
        $connection        = Yii::app()->db;
        $command           = $connection->createCommand($sql);
        $searches          = $command->queryAll();
         

        $command2           = $connection->createCommand($sql2);
        $totalrecord        = count($command2->queryAll());
         
        $record              = ceil($totalrecord/$limit);
        $record              = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                $user_id   = $list['user_id'];
                $title     = StringsHelper::capitalize($list['title']);
                $image     = $list['image'];
                $listid    = $list['id'];
                 
                 
                 
                $userName  = User::model()->find("id='$user_id'");
                $name      = StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $listid);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' =>$listid);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$user_id;
                 
                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "     </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             
            if(count($searches)>0) {
                $HTML .=  "  <div class='clearboth'>&nbsp;</div>";
                $HTML .=    "  <div class='paging_officialpage' id='pagingdiv'>";
                $k = $pageno - 1; $j = $pageno + 1;
                if($pageno>0){
                    $HTML .= "<span class='cursorpointer' onclick='serchofficiallist($k)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","previousimg.png")."    
                              </span> ";
                }if($record>0 && $record!=$pageno) {

                    $HTML .= "<span class='cursorpointer'  onclick='serchofficiallist($j)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","nextimg.png")."
                              </span> ";
                }
                $HTML .= "</div>";
            }
            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                                    No lists found
                                    </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }


    public function actionsearchOnWorld()
    {
        $userid                = Yii::app()->user->getID();
        $logeduser             = User::model()->find("id=$userid");
        $baseurl               = Yii::app()->request->baseUrl;
        $string                = $_POST['value'];
         
        if($string){
            $logeduser->searchlist = $string;
            $logeduser->save(false);
        }
         
        $friendsserch = FriendHelper::getMyFriendsIds();
         
        if (isset($_POST["pageno"]))
        {
            $pageno =  $_POST["pageno"];
        } else {
            $pageno=0;
        }
        $limit=10;
        $start = ($pageno*$limit);


        $sql = "SELECT count( tbl_listtaken.list_id ) AS numgh, tbl_list.name,tbl_list.id, tbl_list.image, tbl_list.title, tbl_list.user_id, tbl_list.is_official, tbl_list.interest_id,
        tbl_list.instruction, tbl_list.favorite, tbl_list.listtype, tbl_list.permission ,tbl_list.priority, tbl_list.created, tbl_list.modified
        FROM tbl_list LEFT JOIN tbl_listtaken ON tbl_list.id = tbl_listtaken.list_id  where tbl_list.title LIKE '%$string%' GROUP BY tbl_list.id ORDER BY numgh DESC,tbl_list.id LIMIT $start,$limit";


        $connection        = Yii::app()->db;
        $command           = $connection->createCommand($sql);
        $searches          = $command->queryAll();

        /*$criteria            = new CDbCriteria();
         $criteria->condition ='title LIKE :title ';
        $criteria->params    = array(
                        'title' => "%$string%",
        );
        $criteria->limit = $limit;
        $criteria->offset = $start;
        $criteria->order = 'id DESC';

        $searches       = ListForm::model()->findAll($criteria);   */
         
        $cdb            = new CDbCriteria();
        $cdb->condition ='title LIKE :title ';
        $cdb->params    = array(
                        'title' => "%$string%",
        );
        $totalrecord = count(ListForm::model()->findAll($cdb));
        $record              = ceil($totalrecord/$limit);
        $record              = $record-1;
         
        $HTML ="";
        if(!empty($searches))
        {
            $odd = 'odd';$var = 0; $j = 0;
            foreach($searches as $list)
            {
                 
                $user_id   = $list['user_id'];
                $title     = StringsHelper::capitalize($list['title']);
                $image     = $list['image'];
                $listid    = $list['id'];
                 
                 
                $userName  = User::model()->find("id=$user_id");
                $name      = StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
                 
                $listtakencount =null;
                 
                if($friendsserch){
                    $c = new CDbCriteria();
                    $c->addCondition('list_id = :listid' ,'','AND');
                    $c->params = array('listid' => $listid);
                    $c->addInCondition('user_id',$friendsserch,'AND');
                    $listtakencount = ListTaken::model()->findAll($c);
                }
                 
                $ListTaken = new CDbCriteria();
                $ListTaken->addCondition('list_id = :listid');
                $ListTaken->params = array('listid' => $listid);
                $totalListTaken    = ListTaken::model()->findAll($ListTaken);
                 
                $listtake          = count($totalListTaken);
                $listtakefriend    = count($listtakencount);
                $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                $namelink    = $baseurl."/index.php/user/dashboard?userid=".$user_id;

                if($odd == 'odd'){
                    $HTML.=   "<div class='part_left'>
                    <div class='big_img_part'>
                    <div class='nightife'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='big_img'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "      </a></div>
                    </div>
                    <div class='bottomimgleft'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right'>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    $odd = 'even'; if($j!=0){
                        if($odd=='even' && $var == 0){
                            $odd='odd';$var=$var+1;
                        } else{$var=0;$odd='even';
                        }
                    }
                }
                 
                else
                {
                    $linkdetatil = $baseurl."/index.php/preferences/listdetail?listid=".$listid;
                    $odd = 'odd';
                    $HTML.=   "<div class='part_right'>

                    <div class='nightife2'>
                    <a class='nightife2_litle' href='$linkdetatil'>$title</a>
                    </div>
                    <div class='image_small'>
                    <a class='nightife2_litle' href='$linkdetatil'>";
                    if(strpos($image, 'profile/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                    elseif(strpos($image, 'freeworld/') === 0){
                        $HTML .= ImageHelper::imageShow($baseurl."/images/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }else{
                        $HTML .= ImageHelper::imageShow($baseurl."/images/titleimages/list_thumbs/official/big/",$image, 0, 0, 
                                                        array('class' => 'officialtodayimg_small', 'title' => $title), $title);
                    }
                     
                    $HTML .=    "     </a></div>

                    <div class='bottom2'>
                    <div class='alice'>
                    <a class='alisename' href='$namelink'>$name</a>
                    </div>
                    <div class='alice_right2'>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Contributors</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtakefriend</span>
                    <span class='contrspan'> Friends</span>
                    </div>
                    <div class='contri2'>
                    <span class='integercolor'>$listtake</span>
                    <span class='contrspan'> Like</span>
                    </div>
                    </div>
                    </div>
                    </div>";
                     
                    if($odd=='odd' && $var == 0){
                        $odd='even';$var=$var+1;
                    } else{$var=0;$odd='odd';
                    }
                }
                $j++;
            }
             
            if(count($searches)>0) {
                $HTML .=  "  <div class='clearboth'>&nbsp;</div>";
                $HTML .=    "  <div class='paging_officialpage' id='pagingdiv'>";
                $k = $pageno - 1; $j = $pageno + 1;
                if($pageno>0){
                    $HTML .= "<span class='cursorpointer' onclick='serchofficiallist($k)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","previousimg.png")."
                              </span> ";
                }if($record>0 && $record!=$pageno) {

                    $HTML .= "<span class='cursorpointer'  onclick='serchofficiallist($j)'>
                                ".ImageHelper::imageShow($baseurl."/images/freeworld/","nextimg.png")."
                              </span> ";
                }
                $HTML .= "</div>";
            }
            echo $HTML;
            exit;
        }

        else
        {
            $HTML=   "<div class='nolistfound'>
                                        No lists found
                                        </div>";
            echo $HTML;
            exit;
        }
         
        exit;
    }

    public function actionlistTitleImage()
    {
        // Getting the baseurl
        $baseurl = Yii::app()->request->getBaseUrl(true);

        //Define the session variable so that put the name of image into session
        $_SESSION['Listimage'] = null;

        if (!empty($_FILES) && $_FILES['images']['tmp_name'] != '')
        {
            // getting the name of the image
            $name = $_FILES["images"]["name"];

            if ($name[0])
            {
                // Define the name of the image and put into the session variable
                $filename              = html_entity_decode(time().$name[0]);
                $_SESSION['Listimage'] = $filename;
                $result                = 1;
            }

            //if image name is in the the session
            if ($result)
            {
                // Source of the image
                $sourcefolder = $_FILES["images"]["tmp_name"];
                // move the uploaded file into the folder
                move_uploaded_file($sourcefolder[0],Yii::app()->basePath."/../images/titleimages/" . $filename);

                // Return the image to show in the box
                echo ImageHelper::imageShow($baseurl.'/images/titleimages/', $filename, 368, 238, array('class' => 'listdefaultimg'));
                exit;
            }
        }
    }

    public function actionlistTitleImageForIe()
    {
        // Getting the baseurl
        $baseurl = Yii::app()->request->getBaseUrl(true);

        //Define the session variable so that put the name of image into session
        $_SESSION['Listimage'] = null;


        if(!empty($_FILES) && $_FILES['images']['tmp_name'] != '')
        {

            // getting the name of the image
            $name = $_FILES["images"]["name"];

            if($name)
            {
                // Define the name of the image and put into the session variable
                $filename              = html_entity_decode(time().$name);
                $_SESSION['Listimage'] = $filename;
                $result                = 1;

            }

            //if image name is in the the session
            if($result)
            {
                // Source of the image
                $sourcefolder = $_FILES["images"]["tmp_name"];

                // move the uploaded file into the folder
                move_uploaded_file($sourcefolder,Yii::app()->basePath."/../images/titleimages/list_thumbs/listdetail/".$filename);

                // genrating the thumbnails of the image of different size
                $src = 'images/titleimages/list_thumbs/listdetail/'.$filename;

                // Destination
                $dest = 'images/titleimages/list_thumbs/official/big/'.$filename;

                // Genrating the image from there extension
                $source_image = ImageHelper::imageCreateFromSrc($src);

                $width = imagesx($source_image);
                $height = imagesy($source_image);
                $newwidth=548;
                $newheight= ($height/$width)*$newwidth;
                $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagejpeg($virtual_image,$dest,100);

                 
                $dest = 'images/titleimages/list_thumbs/featuerdlist/'.$filename;
                $width = imagesx($source_image);
                $height = imagesy($source_image);
                $newwidth=519;
                $newheight= ($height/$width)*$newwidth;
                $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagejpeg($virtual_image,$dest,100);


                $dest = 'images/titleimages/list_thumbs/official/small/'.$filename;
                $width = imagesx($source_image);
                $height = imagesy($source_image);
                $newwidth=343;
                $newheight= ($height/$width)*$newwidth;
                $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagejpeg($virtual_image,$dest,100);
                 


                $dest = 'images/titleimages/list_thumbs/listdetail/'.$filename;
                $width = imagesx($source_image);
                $height = imagesy($source_image);
                $newwidth=110;
                $newheight= ($height/$width)*$newwidth;
                $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagejpeg($virtual_image,$dest,100);

                $dest = 'images/titleimages/list_thumbs/listcreation/'.$filename;
                $width = imagesx($source_image);
                $height = imagesy($source_image);
                $newwidth=75;
                $newheight= ($height/$width)*$newwidth;
                $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagejpeg($virtual_image,$dest,100);


                $dest = 'images/titleimages/list_thumbs/world/'.$filename;
                $width = imagesx($source_image);
                $height = imagesy($source_image);
                $newwidth=60;
                $newheight= ($height/$width)*$newwidth;
                $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagejpeg($virtual_image,$dest,100);

                $dest = 'images/titleimages/list_thumbs/dashboard/'.$filename;
                $width = imagesx($source_image);
                $height = imagesy($source_image);
                $newwidth=22;
                $newheight= ($height/$width)*$newwidth;
                $virtual_image = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagejpeg($virtual_image,$dest,100);

                // Return the image to show in the box
                echo ImageHelper::imageShow($baseurl.'/images/titleimages/list_thumbs/official/small/', $filename, 
                                             368, 238, array('class' => 'listdefaultimg'));
                exit;
            }
        }

    }

    public function actionMultiFileUpload()
    {
        if (isset($_POST['cfg'])) {
            $baseurl = Yii::app()->request->getBaseUrl(true);
            echo $this->renderPartial('/controls/multifileupload', $_POST['cfg'], true, true);
            exit;
        }
    }

    public function actionUploadImages()
    {
        if ((!empty($_FILES)) && ($_FILES['images']['tmp_name'] != '')) {
            // getting the name of the image
            $name = $_FILES['images']['name'];

            if ($name[0]) {
                // Define the name of the image and put into the session variable
                $filename = html_entity_decode(time().$name[0]);

                // Source of the image
                $sourcefolder = $_FILES['images']['tmp_name'];

                // move the uploaded file into the folder
                move_uploaded_file($sourcefolder[0], Yii::app()->basePath."/../images/titleimages/" . $filename);

                // Return the image to show in the box
                echo  $filename;
                exit;
            }
        }
    }
     
     
    public function saveOpinions($list_id, $user_id, $opinions)
    {
        foreach($_POST['opinions'] as $o) {
            $id = intval($o['id']);
            $value = trim($o['value']);
            $image = trim($o['image']);
            $position = trim($o['position']);
            //$opinion = Opinion::model()->findByAttributes(array('list_id' => $list_id, 'user_id' => $user_id, 'position' => $position));
            /////$row =  ListRow::model()->find("list_id = '$listid' AND user_id = '$userid' AND serial_no= $seriol");
			$result = false;
            if ($id > 0) {
                $opinion = Opinion::model()->findByPk($id);
                if ($value === '') {
                    $opinion->delete();
                    //delete
                } else if (($opinion->position != $position) || ($opinion->value != $value) || ($opinion->image != $image)) {
                    $opinion->position = $position;
                    $opinion->value    = $value;
                    $opinion->image    = $image;
                }
                $result = $opinion->save(false);
            } else if ($value !== '') {
                $opinion = new Opinion();
                $opinion->list_id  = $list_id;
                $opinion->user_id  = $user_id;
                $opinion->position = $position;
                $opinion->value    = $value;
                $opinion->image    = $image;
                $result = $opinion->save(false);
            }

			if ($result) {
			    $this->saveComment($opinion->id, $o['comment_value'], $o['comment_id']);
			}
        }
    }
    
    public function actionTakeList($listid = null)
    {
        $this->layout = 'layout/authrized_user';
        if (!isset($listid)) exit;

        $user_id = Yii::app()->user->getID();
        $user = Yii::app()->user->getModel();
        $list_id = intval($listid);

        $list = ListForm::model()
            ->with(array(
                        'interest' => array('select' => 'interest'),
                        'users' => array('select' => 'firstname, lastname'),))
                        ->findByPk($list_id);
         
        $friends_ids = $user->isCompany() ? FriendHelper::getMyFollowersIds() : FriendHelper::getMyFriendsIds();
        if (empty($friends_ids)) 
        {
        	$takenFriends = 0;
        } 
        else 
       {
        	$takenFriends = $list->takenUsers(array(
        			'condition' => 'user_id in ('.implode(',', $friends_ids).')',
        			'order' => 'firstname, lastname'));
        }
        
        $gallery = $this->makeImageGallery($list, array());
        $rpgis = $this->get3RandomImageIndexes($gallery);
         
        if ($list->participation == '') 
        {
            throw new CHttpException(500, 'The specified list cannot be found.');
        }

        if (!in_array($list->user_id, $friends_ids)) 
        {
            if ($list->participation != 'Everybody' && $list->user_id != $user_id) 
            {
                throw new CHttpException(404, 'The specified list cannot be found.');
            }
        }

        //Yii::app()->log->routes['db']->enabled = true;
        $takenList = ListTaken::model()->findByAttributes(array('user_id' => $user_id, 'list_id' => $list_id));

        if ((count($takenList) > 0) || Yii::app()->user->getModel()->isCompany()) {
            $this->redirect(array('viewResult', 'listid' => $list_id));
        }

        if (isset($_POST['opinions'])) {

            $taken = new ListTaken();
            $taken->list_id = $list_id;
            $taken->user_id = $user_id;
            $taken->save(false);

            $this->saveOpinions($list_id, $user_id, $_POST['opinions']);
             
            // send notification if the list owner is my friend
            $listOwnerId = $list->user_id;
            if (in_array($listOwnerId, $friends_ids))
            {
                // check is notifications enabled or not
                $friend = Friend::model()->findByAttributes(array(
                                'user_id' => $listOwnerId,
                                'friend_id' => $user_id
                ));

                if ($friend->notifications_on == 1)	{
                    $notification = new Notification();
                    $notification->myuser_id = $user_id;
                    $notification->user_id = $listOwnerId;
                    $notification->list_id = $listid;
                    $notification->message = " has taken your list ";
                    $notification->read_notification = 0;
                    $notification->notification_type = 'my_created_lists';
                    $notification->save(false);
                }
            }

            // send notification for all friends that has taken this list
            foreach ($friends_ids as $friendId)
            {
                // bug #3804
                if ($listOwnerId == $friendId) continue;
                 
                $listTakenByFriend = ListTaken::model()->findByAttributes(array(
                                'list_id' => $listid,
                                'user_id' => $friendId
                ));

                if ($listTakenByFriend)	{
                    // check is notifications enabled or not
                    $friend = Friend::model()->findByAttributes(array(
                                    'user_id' => $friendId,
                                    'friend_id' => $user_id,
                    ));

                    if ($friend->notifications_on == 1) {
                        $notification = new Notification();
                        $notification->myuser_id = $user_id;
                        $notification->user_id = $friendId;
                        $notification->list_id = $list_id;
                        $notification->message = " has taken list ";
                        $notification->read_notification = 0;
                        $notification->notification_type = 'my_taken_lists';
                        $notification->created = DateHelper::getCurrentSQLDateTime();
                        $notification->modified = DateHelper::getCurrentSQLDateTime();
                        $notification->save(false);
                    }
                }
            }
             
            $this->redirect(array('viewResult', 'listid' => $list_id));
        }

        $opinions = Opinion::model()->findAllByAttributes(array('user_id' => $user_id, 'list_id' => $list_id, 'archived' => 0), array('order' => 'position'));

        $other_opinions = Opinion::model()->findAll(array(
                        'alias' => 'opinion',
                        'group' => 'value',
                        'select' => '(CASE user_id WHEN :x THEN 2 ELSE 1 END) AS col1, opinion.*',
                        'condition' => 'list_id = :y AND archived = 0',
                        'params' => array(':x' => $list->user_id, ':y' => $list_id),
                        'order' => 'col1, position',
                        'limit' => 3));

        $help = 0;
        $visit = ListTaken::model()->find(array(
            'select' => 'id',
            'condition' => 'user_id = :user_id',
            'params' => array(':user_id'=>$user_id),
        ));

        if (empty($visit)) {
            $help = 1;
        }
        
        $this->render('editOpinions', array(
            'action' => 'takeList', 
            'list' => $list, 
            'opinions' => $opinions, 
            'other_opinions' => $other_opinions, 
            'takenList'=> $takenList,
            'takenFriends' => $takenFriends,
            'gallery' => $gallery,
            'rpgis' => $rpgis,
            'help' => $help
        ));
    }

	
    public function actionUpdateEntry($listid = null)
    {
        $this->layout = 'layout/authrized_user';
        if (!isset($listid)) exit;

        $user = Yii::app()->user->getModel();
        $user_id = $user->id;
        $list_id = intval($listid);

        $list = ListForm::model()
            ->with(array(
                'interest' => array('select' => 'interest'),
                'users' => array('select' => 'firstname, lastname'),
            ))->findByPk($list_id);
        if ($user->isAdmin()) {
            $user_id = $list->user_id;
        }

        $friends_ids = $user->isCompany() ? FriendHelper::getMyFollowersIds() : FriendHelper::getMyFriendsIds();
        if (empty($friends_ids)) 
        {
        	$takenFriends = 0;
        } 
        else 
       {
        	$takenFriends = $list->takenUsers(array(
        			'condition' => 'user_id in ('.implode(',', $friends_ids).')',
        			'order' => 'firstname, lastname'));
        }
        
        $gallery = $this->makeImageGallery($list, array());
        $rpgis = $this->get3RandomImageIndexes($gallery);
        
        if ($user->isCompany() || ($list->participation != 'Everybody' 
                                    && !in_array($list->user_id, $friends_ids) 
                                    && $list->user_id != $user_id 
                                    && !$user->isAdmin())) {
            $this->redirect(array('viewResult', 'listid' => $list_id));
        }

        $takenList = ListTaken::model()->findByAttributes(array('user_id' => $user_id, 'list_id' => $list_id));
        if (isset($_POST['opinions'])) {

            if (empty($takenList)) {
                $takenList = new ListTaken();
                $takenList->list_id  = $list_id;
                $takenList->user_id  = $user_id;
                $takenList->save(false);
            }

            $this->saveOpinions($list_id, $user_id, $_POST['opinions']);
            
            if ($user->isAdmin()) {
                ListForm::model()->updateByPk($list_id, array('badge' => !empty($_POST['badge']) ? 1 : 0));
            }

            $this->redirect(array('viewResult', 'listid' => $list_id));
        }

        /*$opinions = Opinion::model()->findAll(array(
			'alias' => 'o1',
            'select' => array(
					'ROUND(SUM(1/position) / (SELECT COUNT(DISTINCT user_id) 
                                              FROM tbl_opinion o2 
                                              WHERE o2.list_id = o1.list_id AND o2.value = o1.value 
                                                AND o2.archived = 0), 2) AS score',
					'COUNT(value) AS mostly_chosen', 
					'id', 'position', 'value', 'list_id', 'user_id', 'image','created'),
			'condition' => 'list_id = :list_id AND user_id = :user_id AND o1.archived = 0',
			'params' => array(':list_id' => $list_id, 
                               ':user_id' => $user_id),
            'group' => 'value',
			'order' => 'score DESC, mostly_chosen DESC, position ASC, value ASC'));*/
        $opinions = Opinion::model()->findAllByAttributes(
			array('user_id' => $user_id, 'list_id' => $list_id, 'archived' => 0),
			array('order' => 'position'));
		
        $other_opinions = Opinion::model()->findAll(array(
			'alias' => 'opinion',
            'group' => 'value',
			'select' => '(CASE user_id WHEN :x THEN 2 ELSE 1 END) AS col1, opinion.*',
			'condition' => 'list_id = :y AND archived = 0',
			'params' => array(':x' => $list->user_id, ':y' => $list_id),
			'order' => 'col1, position',
			'limit' => 3));

		$this->render('editOpinions', array(
            'action' => 'updateEntry', 
            'list' => $list, 
            'opinions' => $opinions, 
            'other_opinions' => $other_opinions, 
            'takenList' => $takenList,
            'takenFriends' => $takenFriends,
            'gallery' => $gallery,
            'rpgis' => $rpgis
        ));
    }
	
    public function actiongetListOpinions($listid = null, $term = null)
    {
        //echo RegexHelper::regex2($term);
        //exit;
        if (!$listid || $term === null)
        {
            echo "[]";
            exit;
        }
         
        $term = trim($term);
        if ($term == "")
        {
            echo "[]";
            exit;
        }
         
        $opinions = Opinion::model()->findAll(array(
                "select" => "list_id, value",
	    		"condition" => "list_id = :lid AND archived = 0 AND value RLIKE :term",
	    		"params" => array(":lid" => $listid, ":term" => RegexHelper::regex2($term)),
	    		"order" => "value ASC",
	    		"limit" => 30,
                "distinct" => true
	    ));

	    if (count($opinions) == 0)
	    {
	        echo "[]";
	        exit;
	    }

	    $resp = array();
	    foreach ($opinions as $opinion)
	    {
	        array_push($resp, array(
	        "id" => $opinion["id"],
	        "name" => $opinion["value"],
	        "value" => StringsHelper::capitalize($opinion["value"])
	        ));
	    }

	    echo json_encode($resp);
	    exit;
    }
    
    private function makeImageGallery($list, $filterUserIds)
    {
        $gallery = array($list->getImageUrl());
        $opinions = $list->opinions($filterUserIds);
        foreach($opinions as $opinion) 
        {
        	$img = trim($opinion['image']);
        	if (!empty($img)) {
        		if ((strpos($img, 'profile/') === 0) || (strpos($img, 'freeworld/') === 0)) {
        			$img = '/images/' . $img;
        		} else {
        			$img = '/images/titleimages/list_thumbs/official/small/' . $img;
        		}
        		$gallery[] = Yii::app()->request->baseUrl . $img;
        	}
        }
        return $gallery;
    }
    
    private function get3RandomImageIndexes($gallery)
    {
        $rpgis = array();
        for ($i = 1; $i < count($gallery); $i++)
        {
            $rpgis[] = $i;
        }
        while (count($rpgis) > 3)
        {
            array_splice($rpgis, mt_rand(0, count($rpgis) - 1), 1);
        }
        
        return $rpgis;
    }

    public function actionViewResult($listid = null, $galleryOn = null)
    {
        // Define the layout of view section
        $this->layout = 'layout/authrized_user';
        if (!isset($listid)) exit;
        $_ajax = (isset($_POST['_ajax']) && ($_POST['_ajax'] == true));

        $list_id = intval($listid);
        //$user_id = Yii::app()->user->getID();
        $user = Yii::app()->user->getModel();
        
        $listsCreated = Yii::app()->user->getModel()->listSorted(0,0,self::TRAILING_PERIOD);
        $countCreated = count($listsCreated);
        
        //$countCreated = 10;
        
        $listsTaken = Yii::app()->user->getModel()->takenListSorted(0,0,self::TRAILING_PERIOD);
        $countTaken = count($listsTaken);
        $list = ListForm::model()
        ->with(array(
				'interest' => array('select' => 'interest'),
				'users' => array('select' => 'firstname, lastname'),))
			->findByPk($list_id);

        // bug #3901
        if (!$list) $this->redirect(array('url/notfound'));
      
        if (!$user->isAdmin() && $list->user_id != $user->id && $list->participation != "Everybody")
        {
            if ($user->isCompany()) 
            {
                throw new CHttpException(404, 'The specified list cannot be found.');
            }
            
            if (!in_array($list->user_id, FriendHelper::getMyFriendsIds(true)))
            {
                throw new CHttpException(404, 'The specified list cannot be found.');
            }
        }        
        
        if ($user->isAdmin()) {
            $takenList = ListTaken::model()->findByAttributes(array(
                'list_id' => $list_id
            ));
        } else {
            $takenList = ListTaken::model()->findByAttributes(array(
                'list_id' => $list_id, 
                'user_id' => $user->id
            ));
        }
        
        if ($list->participation != "Everybody" && $list->participation != "My friends") {
            if (in_array($user->id,explode(',',$list->participation))) {
               $countCreated = $countTaken = 0; 
            }
        }
        
        if (($countCreated+$countTaken) >= self::TRAILING_TAKENS || $user->instaview == 1) {
            $takenList = ListTaken::model()->findByAttributes(array(
                'list_id' => $list_id, 
                'user_id' => $user->id
            ));
            if (empty($takenList)) {
                if (isset($_REQUEST['take'])) {
                    // View Action
                    if ($_REQUEST['take'] == 0) {
                        $takenList = ListTaken::model()->findByAttributes(array(
                            'list_id' => $list_id
                        ));
                    }
                } else if ($user->isMember()) {
                    $listInfo = ListForm::model()->findByAttributes(array(
                        'id' => $list_id, 
                        'user_id' => $user->id
                    ));
                    if (empty($listInfo)) {
                        $this->redirect(array('listAccess', 'listid' => $listid));
                    }
                }
            }
        }

		if ($user->isMember() && empty($takenList)) {
			$this->redirect(array('takelist', 'listid' => $listid));
		}

		$friends_ids_filter = (isset($_POST['friends_ids'])) ? $_POST['friends_ids'] : array();
		$friends_ids = (count($friends_ids_filter) > 0) ? $friends_ids_filter : ($user->isCompany() ? FriendHelper::getMyFollowersIds() : FriendHelper::getMyFriendsIds());

		if (empty($friends_ids)) {
			$takenFriends = 0;
		} else {
			$takenFriends = $list->takenUsers(array(
				'condition' => 'user_id in ('.implode(',', $friends_ids).')',
				'order' => 'firstname, lastname'));
		}

		$gallery = $this->makeImageGallery($list, $friends_ids_filter);
		$rpgis = $this->get3RandomImageIndexes($gallery);
		
        if ($_ajax) {
            if (!empty($galleryOn)) {
                $this->renderPartial('resultgallery', array(
                    '_ajax' => true,
                    'list' => $list,
                    'user' => $user,
                    'friends_ids_filter' => $friends_ids_filter,
                    'takenFriends' => $takenFriends,
                    'takenList' => $takenList,
                    'gallery' => $gallery,
                    'rpgis' => $rpgis
                ), false, true);
            } else {
                $this->renderPartial('viewresult', array(
                    '_ajax' => true,
                    'list' => $list,
                    'user' => $user,
                    'friends_ids_filter' => $friends_ids_filter,
                    'takenFriends' => $takenFriends,
                    'takenList' => $takenList,
                    'gallery' => $gallery,
                    'rpgis' => $rpgis
                ), false, true);
            }
		} else {
			$this->render('viewresult', array(
				'_ajax' => false,
				'list' => $list,
				'user' => $user,
				'friends_ids_filter' => $friends_ids_filter,
				'takenFriends' => $takenFriends,
				'takenList' => $takenList,
				'gallery' => $gallery,
                'rpgis' => $rpgis
			));
		}
    }
    
    public function opinionInfo($listId, $value, $friends_ids_filter) {
        $query = Yii::app()->db->createCommand()
            ->select(array('op.id', 'op.position', 'op.value', 'op.list_id', 'op.user_id', 'op.image','op.created'))
            ->from('tbl_opinion as op, tbl_user as usr')
            ->where('usr.id = op.user_id AND op.value = :value AND op.list_id = :list_id', array(':value'=>$value,
                                                                                                  ':list_id' => $listId))
            ->order('usr.firstname, usr.lastname');
        
        /*if (count($friends_ids_filter) > 0) {
            $query->andWhere('user_id in (' . implode($friends_ids_filter, ',') . ')');
        }*/
        $opinions = $query->queryAll();
        
        return $opinions;
    }
    
    public function opinionsCount($listId, $value, $friends_ids_filter) {
        $query = Yii::app()->db->createCommand()
            ->select('COUNT(id)')
            ->from('tbl_opinion')
            ->where('value=:value AND image != "" AND list_id=:list_id', array(':value'=>$value,
                                                                ':list_id' => $listId));
        /*if (count($friends_ids_filter) > 0) {
            $query->andWhere('user_id in (' . implode($friends_ids_filter, ',') . ')');
        }*/
        return $query->queryScalar();
    }
	
    public function actionGetComments($list_id = -1, $opinion_value = '', $opinion_id = null, $start = 0, $limit = 15)//, $time)
    {
		/*
		$comments = OpinionComment::model()->with(array(
			'opinion' => array(
				'select' => false,
				'on' => 'opinion.list_id = :x AND opinion.value = :y'
				'params' => array(':x' => $list->id, ':y' => $opinion['value'])))
		)->findAll(array('alias'=>'comment', 'condition' => 'trim(comment.value) <> ""', 'order' => 'comment.created DESC'));
		*/
		//$delta = time() *1000 - $time;
		//echo '----- '.$delta.'-------';
        // Define the layout of view section
        $this->layout = 'layout/authrized_user';
        if (!isset($opinion_id)) exit;

        $opinion_id = intval($opinion_id);
        $user = Yii::app()->user->getModel();
         
		$comments = OpinionComment::model()->with(array(
			'user',
			'opinion' => array(
				'select' => false,
				'on' => 'opinion.list_id = :y AND opinion.value = :x',
				'params' => array(':x' => $opinion_value, ':y' => $list_id)))
				//'on' => 'opinion.id = :x',
				//'params' => array(':x' => $opinion_id)))
		)->findAll(array(
			'alias' => 'comment',
			'condition' => 'trim(comment.value) <> ""', 
			'order' => 'comment.created DESC',
			'offset' => $start,
			'limit' => $limit
		));
        //CVarDumper::dump($list, 10, true);
        //Yii::app()->log->routes['db']->enabled = true;
		
		$result = array();
		foreach($comments as $comment) {
			$result[] = array(
				'id' => intval($comment->id),
				'opinionId' => intval($comment->opinion_id),
				'userId' => intval($comment->user_id),
				'userName' => $comment->user->getFullName(),
				'userPhoto' => $comment->user->getImageUrl(),
				'value' => $comment->value,
				'time' => 1000 * DateHelper::toClientTime(strtotime($comment->created)),
				//'time' => 1000 * strtotime($comment->created) - $delta,
				//'time' => 1000 * DateHelper::toClientTime(0),
				//'elapsed' => DateHelper::datetimeToAgoStr($comment->created)
				//'time' => 1000* (strtotime($comment->created))
			);
		}
        echo CJSON::encode($result);
    }	

	public function saveComment($opinion_id, $comment_value, $comment_id = 0)
    {
        if (isset($opinion_id) && isset($comment_value)) {

			$opinion_id = intval($opinion_id);
			$comment_value = trim($comment_value);
			$comment_id = intval($comment_id);
			
			if ($comment_value != '' && $opinion_id > 0) {
                //CVarDumper::dump('saveComment:: start save!', 10, true);
				$user = Yii::app()->user->getModel();
				$user_id = $user->id;
				
				$opinion = Opinion::model()->findByPk($opinion_id);

				if (empty($opinion)) {
					return false;
				}

				if ($comment_id > 0) {
					$comment = OpinionComment::model()->findByPk($comment_id);
				} else {
					$comment = new OpinionComment();
                    $comment->created = DateHelper::getCurrentSQLDateTime();
                    $comment->modified = DateHelper::getCurrentSQLDateTime();
				}
				
				if (empty($comment)) {
					return false;
				}
				
				$comment->opinion_id = $opinion->id;
				$comment->user_id = $user_id;
				$comment->value = $comment_value;
				
				if ($comment->save(false)) {
					//CVarDumper::dump('saveComment:: SAVED!!!!', 10, true);
				    // case #3761 send notification on comment
				    $friendSearch = FriendHelper::getMyFriendsIdsStr();
				    if ($friendSearch != '') {
				        $friendOpinions = Opinion::model()->findAll(array(
                            'condition' => 'value = :val AND list_id = :listId AND archived = 0 AND user_id IN ('
                                . $friendSearch . ')',
                            'params' => array(':val' => $opinion->value,
                                               ':listId' => $opinion->list_id)
                        ));
                        
                        foreach ($friendOpinions as $fop) {
    				        $friendId = $fop->user_id;
    				         
    				        // check is notifications enabled or not
    				        $friend = Friend::model()->findByAttributes(array(
    		    				'user_id' => $friendId,
    		    				'friend_id' => $user_id,
				    		));
                            
                            $myLists = array();
                            $myListsSql = ListForm::model()->findAllByAttributes(array(
                                'user_id' => $friendId,
                            ));
                            foreach ($myListsSql as $list) {
                                $myLists[] = $list->id;
                            }

				    		if ($friend->notifications_on == 1 /*&& in_array($fop->list_id,$myLists)*/) {
				    		    $notification = new Notification();
				    		    $notification->myuser_id = $user_id;
				    		    $notification->user_id = $friendId;
				    		    $notification->list_id = $fop->id;
				    		    $notification->message = ' commented on entry ';
				    		    $notification->read_notification = 0;
				    		    $notification->notification_type = 'opinion';
				    		    $notification->save(false);
				    		}
    				    }
				    }
					return $comment;
				}
			}
			return false;
		}
    }


    public function actionAddComment()
    {
        //if (isset($_POST['list_id']) && isset($_POST['opinion']) && isset($_POST['comment'])) {
        if (isset($_POST['opinion_id']) && isset($_POST['comment'])) {
			if ($comment = $this->saveComment($_POST['opinion_id'], $_POST['comment'])) {
				// TODO: donot return HTML from controller!!!
				//$delta = time() *1000 - $_POST['time'];

				$user = Yii::app()->user->getModel();
				echo
				'<li class="grey-brd" data-comment-id="' . $comment->id . '">
					<img src="' . $comment->user->getImageUrl() . '?w=45&h=38" class="small-photo">
					<div class="comment-details">
						<span class="aut-date" data-comment-time="' . (DateHelper::toClientTime(strtotime($comment->created)) * 1000) . '"></span>
						<p class="aut-com">' . $comment->user->getFullName() . '</p>
						<div class="aut-mess">' . $comment->value . '</div>'
						. (($comment->user->id == $user->id) ?
						'<div class="change-bl clearfix">
							<div class="link" onclick="opinionShowConfirmDeleteComment(\'' . $comment->id . '\');">delete</div>
							<div class="delete-block" style="display: none;">Delete this comment? <span class="y-n" onclick="opinionDeleteComment(\'' . $comment->id . '\');">YES </span> <span class="y-n" onclick="opinionHideConfirmDeleteComment(\'' . $comment->id . '\');">No</span></div>
						</div>' : '') . '
					</div>
				</li>';
				/*
				'<li class="grey-brd" data-comment-id="' . $comment->id . '">
					<p class="aut-com">' . $comment->user->getFullName() . '</p>
					<div class="aut-mess">' . $comment->value . '</div>
					<div class="change-bl clearfix">
					<div class="delete-block" style="display: none;">Delete this comment? <span class="y-n" onclick="opinionDeleteComment(\'' . $comment->id . '\');">YES </span> <span class="y-n" onclick="opinionHideConfirmDeleteComment(\'' . $comment->id . '\');">No</span></div>
					<div class="b-trash" onclick="opinionShowConfirmDeleteComment(\'' . $comment->id . '\');"></div>
					</div>
					</li>';
				*/
			}
		}
    }

    public function actionDeleteComment()
    {
        if (isset($_POST['comment_id']) && !empty($_POST['currentUserId'])) {
            $user = Yii::app()->user->getModel();
			$comment_id = intval($_POST['comment_id']);
            
			$comment = OpinionComment::model()->findByPk($comment_id);
            if (!empty($comment->user_id) && $comment->user_id == !empty($_POST['currentUserId']) || $user->isAdmin()) {
                OpinionComment::model()->deleteByPk($comment_id);
            }
		}
    }
    
    public function actionRemoveGalleryImg() {
        $user = Yii::app()->user->getModel();
        if(Yii::app()->request->isAjaxRequest 
                && !empty($_POST['image']) 
                && !empty($_POST['list_id']) 
                && $user->isAdmin()) {
            ListForm::model()->updateAll(array('image'=>''),
                                         'id = :list_id AND image = :image',
                                         array(':list_id' => $_POST['list_id'],
                                                ':image' => $_POST['image']));
            Opinion::model()->updateAll(array('image'=>''),
                                         'list_id = :list_id AND image = :image',
                                         array(':list_id' => $_POST['list_id'],
                                                ':image' => $_POST['image']));
        }
    }
    
    public function actionCheckUniqTitle() {
        if (!empty($_POST['title'])) {
            $list = ListForm::model()->find('title LIKE :title', array(':title' => $_POST['title']));
            if (!empty($list)) {
                die('0');
            }
        }
        die('1');
    }
    
    public function getShortTitle($str,$len) {
        /*if (strlen($str) > $len) {
            list($left,, $right) = imageftbbox( 12, 0, Yii::app()->basePath.'/../css/FjallaOne-Regular.ttf', substr($str,0,1));
            $widthOne = $right - $left;
            if ($widthOne >= 6) {
                $str = substr($str, 0, round($len*57/100)).'...';
            } else {
                $str = substr($str, 0, $len).'...';
            }
        }    */
        return $str;        
    }
    
    public function actionInviteList() {
        $user = Yii::app()->user->getModel();
        if (!empty($_POST['listId']) && !empty($_POST['friends']) && $user->isMember()) {
            foreach (explode(',',$_POST['friends']) as $fr) {
                // check is notifications enabled or not
                $friend = Friend::model()->findByAttributes(array(
                                'user_id' => $user->id,
                                'friend_id' => $fr
                ));

                if ($friend->notifications_on == 1)	{
                    $notification = new Notification();
                    $notification->myuser_id = $user->id;
                    $notification->user_id = $fr;
                    $notification->list_id = $_POST['listId'];
                    $notification->message = " has invited you to the list ";
                    $notification->read_notification = 0;
                    $notification->notification_type = 'invite_list';
                    $notification->save(false);
                }
            }
            die('1');
        }
        die('0');
    }
    
    public function actionListAccess($listid = null) {
        $this->layout = 'layout/authrized_user';
        
        if (!isset($listid)) exit;
        
        $this->render('list_access', array(
            'listid' => $listid,
        ));
    }
    
    public function actionImageData() {
        try {
            // Check if the URL is set
            if(isset($_GET["url"])) {

                // Get the URL and decode to remove any %20, etc
                $url = urldecode($_GET["url"]);

                // Get the contents of the URL
                $file = file_get_contents($url);

                // Check if it is an image
                if(@imagecreatefromstring($file)) {

                    // Get the image information
                    $size = getimagesize($url);
                    // Image type
                    $type = $size["mime"];
                    // Dimensions
                    $width = $size[0];
                    $height = $size[1];

                    // Setup the data URL
                    $type_prefix = "data:" . $type . ";base64,";

                    // Encode the image into base64
                    $base64file = base64_encode($file);

                    // Combine the prefix and the image
                    $data_url = $type_prefix . $base64file;

                    // Setup the return data
                    $return_arr = array(
                        'width'=> $width,
                        'height'=> $height,
                        'data'=> $data_url,
                        'mime'=>$type
                    );

                    // Encode it into JSON
                    $return_val = json_encode($return_arr);

                    // If a callback has been specified
                    if(isset($_GET["callback"])) {

                        // Wrap the callback around the JSON
                        $return_val = $_GET["callback"] . '(' . $return_val . ');';

                        // Set the headers to JSON and so they wont cache or expire
                        header('Cache-Control: no-cache, must-revalidate');
                        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                        header('Content-type: application/json');

                        // Print the JSON
                        print $return_val;

                    // No callback was set
                    } else {
                        header('HTTP/1.0 400 Bad Request');
                        print "No callback specified";
                    }

                // The requested file is not an image
                } else {
                    header('HTTP/1.0 400 Bad Request');
                    print "Invalid image specified";
                }

            // No URL set so error
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo "No URL was specified";
            }

        } catch (Exception $e) {	
            header('HTTP/1.0 500 Internal Server Error');
            echo "Internal Server Error";
        }
    }
    
    public function actionImageCrop() {
        $maxWidth = 710;
        
        if(isset($_GET["url"])) {
            $url = urldecode($_GET["url"]);

            // For profile Images
            $pos = strpos($url, 'profile');
            if ($pos !== false) {
                $nw = 180;
                $nh = 180;
                $name = substr($url,($pos+8));
                $pos2 = strpos($name, '?');
                if ($pos2 !== false) {
                    $name = substr($name,0,$pos2);
                }

                $path = Yii::app()->basePath.'/../images/profile/'.$name;
            }
            
            // target file path
            $name = str_replace(".jpg", ".png", $name);
            $name = str_replace(".gif", ".png", $name);
            $name = str_replace(".JPG", ".png", $name);
            $name = str_replace(".GIF", ".png", $name);
            $name = str_replace(".jpeg", ".png", $name);
            $name = str_replace(".JPEG", ".png", $name);

            $pathImg = Yii::app()->basePath.'/../img/cache/'.$nw.'x'.$nh.'/'.$name;
            $file = file_get_contents($pathImg);

            if($vImg = @imagecreatefromstring($file)) {
                $size = getimagesize($pathImg);
                
                if ($size[0] > $maxWidth) {
                    $ratio = $maxWidth / $size[0]; 
                    $height = $size[1] * $ratio; 
                    $newImage = imagecreatetruecolor($size[0], $height); 
                    imagecopyresampled($newImage, $vImg, 0, 0, 0, 0, $maxWidth, $height, $size[0], $size[1]); 
                    $vImg = $newImage;
                    $size[0] = $maxWidth;
                    $size[1] = $height;
                }

                $x = (int) $_GET['x'];
                $y = (int) $_GET['y'];
                $w = (int) $_GET['w'] ? $_GET['w'] : $size[0];
                $h = (int) $_GET['h'] ? $_GET['h'] : $size[1];

                $dstImg = imagecreatetruecolor($nw, $nh);
                imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
                if (!empty($pathImg)) {
                    switch ($size["mime"]) {
                        case 'image/gif':
                            imagegif($dstImg, $pathImg);
                            imagegif($dstImg, $path);
                            break;
                        case 'image/jpeg':
                            imagejpeg($dstImg, $pathImg);
                            imagejpeg($dstImg, $path);
                            break;
                        case 'image/png':
                            imagepng($dstImg, $pathImg);
                            imagepng($dstImg, $path);
                            break;
                        case 'image/wbmp':
                            imagewbmp($dstImg, $pathImg);
                            imagewbmp($dstImg, $path);
                            break;
                    }
                }
                imagedestroy($dstImg);
            }
        }
    }
} 