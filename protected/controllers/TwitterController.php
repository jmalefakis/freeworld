<?php

class TwitterController extends Controller
{
	public $layout='column2';

        
        public function filters()
            {
                return array(
                    'accessControl',
                );        
            } 
                public function accessRules() {
                return array(
                    
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('twitter_oauth', 'twitter','connecttwitter','twitterlisting', 'twitter_invitation_oauth','twitterinvitation'),
                        'users' => array('@'),
                        //'expression'=>'$user->isAdmin',
                    ),
               
                    array('deny', // deny all users
                        'users' => array('*'),
                    ), 
                );
            }
        
        
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
        
        public function actionIndex()
	{
            $this->render('index');
            //$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
        
	public function actiontwitter_oauth()
	{    
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
            $userid = Yii::app()->user->getID();            
            
            if(!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){  
                // We've got everything we need  
               
            } 
            elseif(isset($_GET['denied']))
            {
                $this->redirect('twitterlisting');
            }
            else {  
                // Something's missing, go back to twitter
                $baseurl = Yii::app()->request->baseUrl;                
                header("Location: http://localhost/latestyii/newadminproject/index.php/user/twitterlisting");
            } 
            // TwitterOAuth instance, with two new parameters we got in twitter_login.php
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk', $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            // Let's request the access token
            $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            // Save it in a session var
            $_SESSION['access_token'] = $access_token;
            // Let's get the user's info
            $user_info = $twitteroauth->get('account/verify_credentials');

            $user_followers = $twitteroauth->get('statuses/followers');            
            
            //echo "<pre>";print_r($user_followers);exit;
            $count_follwer = count($user_followers);
            
                 for($i=0; $i<$count_follwer; $i++)
                 {                      
                     
                     $user = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_twitter')
                                            ->where('friend_twitterid=:value and user_id=:id', array(':value' =>$user_followers[$i]->id,':id'=>$userid))
                                            ->queryRow();
                     if(empty($user))
                      { 
                         $model = new Twitter;
                         $model->user_id           = $userid;
                         $model->twitter_id        = $user_info->id;
                         $model->friend_twitterid  = $user_followers[$i]->id;
                         $model->friend_name       = $user_followers[$i]->screen_name;
                         $model->friend_twitteimg  = $user_followers[$i]->profile_image_url;
                         $model->image             ='http://localhost/latestyii/newadminproject/images/twitter.png';
                         $model->created           = date('Y-m-d');
                         $model->modified          = date('Y-m-d');
                         $model->save(false); 
                     }
                 }                  
          
            $this->redirect(array('user/twitterlisting'));            
        }
        
        
        public function actiontwitter()
	{
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
  
             // The TwitterOAuth instance  
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk');  
            // Requesting authentication tokens, the parameter is the URL we will be redirected to  
            $request_token = $twitteroauth->getRequestToken('http://localhost/latestyii/newadminproject/index.php/twitter/twitter_oauth');  

            // Saving them into the session  
            $_SESSION['oauth_token'] = $request_token['oauth_token'];  
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];  

            // If everything goes well..  
            if($twitteroauth->http_code==200){  
                // Let's generate the URL and redirect  
                $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']); 
                header('Location: '. $url); 
            } else { 
                // It's a bad idea to kill the script, but we've got to know when there's an error.  
                die('Something wrong happened.');  
            } 
            $this->render('twitter');
        }
        
        public function actionconnecttwitter()
	{
            $this->render('connecttwitter');
        }
        
        public function actiontwitterlisting()
	{              
                $this->layout = 'authrized';
                $id = Yii::app()->user->getID(); 
                $user=User::model()->findByPk($id);
                $_SESSION['name'] = $user['firstname'].' '.$user['lastname']; 
                
                
                $user_twitter = Twitter::model()->findAllByAttributes(
                    array(),
                    $condition  = 'user_id = :id',
                    $params     = array(
                            ':id' => $id, 
                            
                    )
                ); 
                $count = count($user_twitter);
                
                for($i=0; $i<$count; $i++)
                {  
                    $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_user')
                                            ->where('twitter_id=:value', array(':value' =>$user_twitter[$i]->friend_twitterid))
                                            ->queryRow();
                    if(!empty($usertable))
                      {  
                         $idtype =$user_twitter[$i]->friend_twitterid;
                         $update  = Twitter::model()->deleteAll("friend_twitterid = '$idtype'");
                     }
                }
               
                /*$q = Yii::app()->request->getParam('friend_name');     
		$model=new Twitter('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Twitter']))
			$model->attributes=$_GET['Twitter'];*/
                
                
                 $criteria   = new CDbCriteria();
                 $criteria->condition = "user_id = $id";
                 $count      = Twitter::model()->count($criteria);  
                 //$pages      = new CPagination($count);
                 //$pages->pageSize = 20 ;
                 //$pages->applyLimit($criteria);
                 $model = Twitter::model()->findAll($criteria); 

		$this->render('twitterlisting',array(
			'model'=>$model,                        
                        //'pages' => $pages,
		));
	}
        
        
        public function actiontwitter_invitation_oauth($user=null)
	{   
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
            $baseurl       = Yii::app()->request->baseUrl;  
            $userid        = Yii::app()->user->getID();            
            $usertwitterid = $_SESSION['usertwitterid'];
            $face          = Interest::model()->find();
            $message       = $face['twitter_message']; 
            //$message       = "<a href='$baseurl/index.php'>$messages</a>";            
            if(!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){  
                // We've got everything we need  
               
            } else {  
                // Something's missing, go back to twitter
                $baseurl = Yii::app()->request->baseUrl;               
                header("Location: http://localhost/latestyii/newadminproject/index.php/twitter/twitterinvitation");
            } 
            
            // TwitterOAuth instance, with two new parameters we got in twitter_login.php 757023578
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk', $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            // Let's request the access token
            $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            // Save it in a session var
            $_SESSION['access_token'] = $access_token;
            // Let's get the user's info
            $user_info = $twitteroauth->get('account/verify_credentials');
            
            $user_followers = $twitteroauth->post("direct_messages/new.json?text=$message&user=$usertwitterid&"); 
            $this->redirect('twitterlisting');            
        }
        
        
        public function actiontwitterinvitation($user=null)
	{
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            //session_start();
  
             // The TwitterOAuth instance   
             $_SESSION['usertwitterid'] = $user;
             
             
             $userid = Yii::app()->user->getID();
                if(isset($user))
                {                     
                    $face = Notification::model()->find("twitter_id = '$user'");
                    if(empty($face)){
                    $notification                     = new Notification;
                    $notification->user_id            = $userid;
                    $notification->read_notification  = 1;
                    $notification->facebook_id        = $user;
                    $notification->created            = DateHelper::getCurrentSQLDateTime();      
                    $notification->modified           = time();      
                    $notification->save(false);
                    }
                }
            
             
             
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk');  
            // Requesting authentication tokens, the parameter is the URL we will be redirected to  
            $request_token = $twitteroauth->getRequestToken("http://localhost/latestyii/newadminproject/index.php/twitter/twitter_invitation_oauth");  

            // Saving them into the session  
            $_SESSION['oauth_token'] = $request_token['oauth_token'];  
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];  

            // If everything goes well..  
            if($twitteroauth->http_code==200){  
                // Let's generate the URL and redirect  
                $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']); 
                
                header('Location: '. $url); 
            } else { 
                // It's a bad idea to kill the script, but we've got to know when there's an error.  
                die('Something wrong happened.');  
            } 
            //$this->render('twitterinvitation');
        }
        
        
}