<?php

class FacebookController extends Controller
{
	public $layout='column2';
        
        
        public function filters()
            {
                return array(
                    'accessControl',
                );        
            } 
                public function accessRules() {
                return array(
                    
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('facebook', 'connectfacebook','addnotification','facebooklisting'),
                        'users' => array('@'),
                        //'expression'=>'$user->isAdmin',
                    ),
               
                    array('deny', // deny all users
                        'users' => array('*'),
                    ), 
                );
            }
        
        
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
         
        public function actionIndex()
	{
            $this->render('index');
            //$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
        
        public function actionfacebook()
	{
            $this->layout = 'authrized';
            Yii::import('application.vendors.*');
            require_once('facebook/facebook.php'); 
            # Creating the facebook object
            
            $config = array(
                'appId' => '493318360686506',
                'secret' => '2c2431e65c634a125ac40586b57b150b',
              );
            
            $baseurl = Yii::app()->request->baseUrl;
            $userid = Yii::app()->user->getID(); 
            
            # Let's see if we have an active session
            //$friends = $facebook->api('/me/friends');
            //$session = $facebook->getSession();
            $facebook = new Facebook($config);
            $user_id = $facebook->getUser();
            //$access_token = $facebook->getAccessToken();
            //echo "<pre>";print_r($user_id);exit;            
            if(!empty($user_id)) { 
                # Active session, let's try getting the user id (getUser()) and user info (api->('/me')) 
                try{
                    $uid = $facebook->getUser();
                    $user = $facebook->api('/me/friends');
                    $my_information = $facebook->api('/me');
                    //$temp =' https://graph.facebook.com/581913302/picture';
                } catch (Exception $e){die("An error occurred! ".$e->getMessage());}
                
                if(!empty($user)){  
                    # User info ok? Let's print it (Here we will be adding the login and registering routines)
                    
                    $count = count($user['data']);
                    //echo "<pre>";print_r($my_information);exit;
                    for($i=0; $i<$count; $i++)
                    { 
                        $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_facebook')
                                            ->where('friend_facebookid=:value and user_id=:id', array(':value' =>$user['data'][$i]['id'],':id'=>$userid))
                                            ->queryRow();
                        
                        
                    if(empty($usertable))                   
                      {    
                         $model = new FacebookForm;
                         $model->user_id            = $userid;
                         $model->facebook_id        = $my_information['id'];
                         $model->friend_facebookid  = $user['data'][$i]['id']; 
                         $model->friend_name        = $user['data'][$i]['name']; 
                         $facebookimg               = 'https://graph.facebook.com/'.$user['data'][$i]['id'].'/picture'; 
                         $model->friend_facebookimg = $facebookimg;
                         $model->invite             = 'invite member';
                         $model->image              ='http://localhost/latestyii/newadminproject/images/face_book.png';
                         $model->created            = date('Y-m-d');
                         $model->modified           = date('Y-m-d');                         
                         $model->save(false);                         
                      }   
                    } 
                    //echo "<pre>";print_r($user['data']);exit;
                    $this->redirect('facebooklisting');  
                    //echo "<pre>";print_r($my_information['id']);
                } else {
                    # For testing purposes, if there was an error, let's kill the script
                    die("There was an error.");
                }
            } else { 
                # There's no active session, let's generate one
                 $login_url = $facebook->getLoginUrl();                 
                 header("Location: ".$login_url);
            }
            //$this->redirect('facebook');  
            
        }
        
        
        public function actionconnectfacebook()
	{
            $this->render('connectfacebook');
        }
        
        public function actionaddnotification()
	{
            $userid = Yii::app()->user->getID();
            if(isset($_POST['fbid']))
            { 
                $fbid = $_POST['fbid'];
                $face = Notification::model()->find("facebook_id = '$fbid'");
                if(empty($face)){
                $notification                     = new Notification;
                $notification->user_id            = $userid;
                $notification->read_notification  = 1;
                $notification->facebook_id        = $fbid;                     
                $notification->save(false);
                }
            }
        }
        
        public function actionfacebooklisting()
	{             
                $this->layout = 'authrized';
                $id   = Yii::app()->user->getID(); 
                $user = User::model()->findByPk($id);
                $face = Interest::model()->find();
                $_SESSION['name'] = $user['firstname'].' '.$user['lastname'];
                 
                $user_facebook= FacebookForm::model()->findAllByAttributes(
                    array(),
                    $condition  = 'user_id = :id',
                    $params     = array(
                            ':id' => $id, 
                            
                    )
                ); 
               
                $count = count($user_facebook);
                 
                for($i=0; $i<$count; $i++)
                {  
                    $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_user')
                                            ->where('facebook_id=:value', array(':value' =>$user_facebook[$i]->friend_facebookid))
                                            ->queryRow();                   
                    if(!empty($usertable))
                      {  
                         $idtype =$user_facebook[$i]->friend_facebookid;
                         $update  = FacebookForm::model()->deleteAll("friend_facebookid = '$idtype'");                         
                     }
                }
               
                
		/*$model=new FacebookForm('search'); 
                $q = Yii::app()->request->getParam('friend_name');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FacebookForm']))
			$model->attributes=$_GET['FacebookForm'];  */     
                
                 $criteria   = new CDbCriteria();
                 $criteria->condition = "user_id = $id";
                 $count      = FacebookForm::model()->count($criteria);  
                 //$pages      = new CPagination($count);
                 //$pages->pageSize = 20 ;
                 //$pages->applyLimit($criteria);
                 $model = FacebookForm::model()->findAll($criteria);                 

		$this->render('facebooklisting',array(
			'model'=>$model, 
                        //'pages' => $pages,
                        'face'=>$face
		));
	}
        
        
}