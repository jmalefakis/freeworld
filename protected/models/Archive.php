<?php

class Archive extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'tbl_archive';
    }

    public function relations()
    {
        return array(
                        'list' => array(self::BELONGS_TO, 'ListForm', 'list_id', 'joinType' => 'INNER JOIN'),
        );
    }
}

?>