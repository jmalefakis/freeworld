<?php

class ListForm extends ImageBaseModel
{
	public $instruction;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
            
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			 array('image, name,title, user_id, is_official,created,interest, modified', 'required'),
                         array('title', 'unique'),
                         //array('link', 'url'),
                         array('link_title', 'length', 'max'=>30),
                         array('link', 'length', 'max'=>255),
                         array('image', 'file', 'types'=>'jpg, gif, png'),
		);
	}

	/** 
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'opinions' => array(self::HAS_MANY, 'Opinion', 'list_id', 'condition' => "archived = 0"),
			'taken' => array(self::HAS_MANY, 'ListTaken', 'list_id'),
			'countTaken' => array(self::STAT, 'ListTaken', 'list_id'),
			'takenUsers' => array(self::HAS_MANY, 'User', array('user_id'=>'id'), 'through'=>'taken'),
			'user' => array(self::HAS_MANY, 'User', array('user_id'=>'id'), 'through'=>'taken'),
			'users' => array(self::BELONGS_TO, 'User', 'user_id', 'joinType'=>'INNER JOIN'),
			'interest' => array(self::BELONGS_TO, 'Interest', 'interest_id')
		);
	}

	public function opinionRaiting($users_ids = array())
	{
        $user   = Yii::app()->user->getModel();
        $usersInSQL = '';
        if (count($users_ids) > 0) {
            $usersInSQL = ' AND user_id in (' . implode($users_ids, ',') . ')';
        }
        if (!empty($users_ids[0]) && sizeof($users_ids) == 1 && Yii::app()->user->getID() == $users_ids[0]) {
            $query  = Yii::app()->db->createCommand()
                    ->select(array(
                        'ROUND(SUM(1/position) / (SELECT COUNT(DISTINCT user_id) FROM tbl_opinion o2 WHERE o2.list_id = o1.list_id AND o2.value = o1.value AND o2.archived = 0), 4) AS score',
                        'COUNT(value) AS mostly_chosen', 
                        'id', 'position', 'value', 'list_id', 'user_id', 'image','created'))
                    ->from('tbl_opinion o1')
                    ->where('list_id = :list_id AND o1.archived = 0 AND user_id = :user_id', array(':list_id' => $this->id, ':user_id' => Yii::app()->user->getID()))
                    ->group('value')
                    ->order('score DESC, mostly_chosen DESC, position ASC, value ASC'); 
        } else {
            $query  = Yii::app()->db->createCommand()
                    ->select(array(
                        'ROUND(SUM(1/position) / (SELECT COUNT(DISTINCT user_id) FROM tbl_opinion o2 WHERE o2.list_id = o1.list_id AND o2.value = o1.value AND o2.archived = 0'.$usersInSQL.'), 4) AS score',
                        'COUNT(value) AS mostly_chosen', 
                        'id', 'position', 'value', 'list_id', 'user_id', 'image','created'))
                    ->from('tbl_opinion o1')
                    ->where('list_id = :list_id AND o1.archived = 0', array(':list_id' => $this->id))
                    ->group('value')
                    ->order('score DESC, mostly_chosen DESC, position ASC, value ASC');

            if (count($users_ids) > 0) {
                $query->andWhere('user_id in (' . implode($users_ids, ',') . ')');
            }
        }
		$opinions = $query->queryAll();
        if (sizeof($opinions) > 0) {
            foreach ($opinions as &$opinion) {
                $opinionImage = null;
                if (count($users_ids) == 0 && $user->isCompany()) {
                    $query  = Yii::app()->db->createCommand()
                        ->select(array('image'))
                        ->from('tbl_opinion')
                        ->where('value = :value AND image != "" AND list_id = :list_id', array(':value' => $opinion['value'],
                                                                                                ':list_id' => $this->id));
                    $opinionImage = $query->queryRow();
                } else if (count($users_ids) == 0) {
                    $query  = Yii::app()->db->createCommand()
                        ->select(array('image'))
                        ->from('tbl_opinion')
                        ->where('value = :value AND user_id = :user_id  AND list_id = :list_id', array(':value' => $opinion['value'],
                                                                                                        ':user_id' => Yii::app()->user->getID(),
                                                                                                        ':list_id' => $this->id));
                    $opinionImage = $query->queryRow();
                    if (empty($opinionImage['image'])) {
                        $query  = Yii::app()->db->createCommand()
                        ->select(array('image'))
                        ->from('tbl_opinion')
                        ->where('value = :value AND image != "" AND list_id = :list_id', array(':value' => $opinion['value'], 
                                                                                                ':list_id' => $this->id));
                        $opinionImage = $query->queryRow();
                    }
                } else if ($opinion['image'] == '') {
                    $query  = Yii::app()->db->createCommand()
                        ->select(array('image'))
                        ->from('tbl_opinion')
                        ->where('value = :value AND image != "" AND list_id = :list_id', array(':value' => $opinion['value'], 
                                                                                                ':list_id' => $this->id));
                    $opinionImage = $query->queryRow();
                }
                
                if (!empty($opinionImage['image'])) {
                    $opinion['image'] = $opinionImage['image'];
                }
            }
        }
        return $opinions;
        
		/* v2
		$amount = Yii::app()->db->createCommand()
				->select('COUNT(DISTINCT user_id)')
				->from('tbl_opinion')
				->where('list_id = :list_id', array(':list_id' => $this->id));
		if (count($users_ids) > 0) {
			$query->andWhere('user_id in (' . implode($users_ids, ',') . ')');
		}
		$amount = $amount->queryScalar();		

		$query  = Yii::app()->db->createCommand()
				->select('ROUND(SUM((1/:amount)*(1/position)), 4) AS score, COUNT(value) AS mostly_chosen, position, value, list_id, user_id, image')
				->from('tbl_opinion o')
				->where('list_id = :list_id', array(':amount' => $amount, ':list_id' => $this->id))
				->group('value')
				->order('score DESC, mostly_chosen DESC, position ASC, value ASC');
				
		if (count($users_ids) > 0) {
			$query->andWhere('user_id in (' . implode($users_ids, ',') . ')');
		}
		return $query->queryAll();
		*/
		/* v1
		$query = Yii::app()->db->createCommand()
				->select('ROUND(SUM((1/:amount)*(1/position)), 4) AS score, COUNT(o.value) AS mostly_chosen, position, o.value, list_id, user_id, image, c.value AS comment')
				->from('tbl_opinion o')
				->leftJoin('tbl_opinioncomment c', 'o.id = c.opinion_id')
				->where('list_id = :list_id', array(':amount' => $amount, ':list_id' => $this->id))
				->group('o.value')
				->order('score DESC, mostly_chosen DESC, position ASC, o.value ASC');
		*/
	}
    
	/*
	public function rowRaiting()
	{
		$amount = Yii::app()->db->createCommand()
				->select('COUNT(DISTINCT user_id)')
				->from('tbl_listrow')
				->where('list_id = :list_id', array(':list_id' => $this->id))
				->queryScalar();		
		
		return Yii::app()->db->createCommand()
				->select('ROUND(SUM((1/:amount)*(1/serial_no)), 2) AS score, COUNT(value) AS mostly_chosen, serial_no, value, list_id')
				->from('tbl_listrow')
				->where('list_id = :list_id', array(':amount' => $amount,':list_id' => $this->id))
				->group('value')
				->order('score DESC, mostly_chosen DESC, serial_no ASC, value ASC')
				->queryAll();		
	}
	*/
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'image' => 'Image',
			'name' => 'Name',
                        'title' => 'Title'
		);
	}
        
    public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name);
                $criteria->compare('title',$this->title);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
    public function getImage()
	{
		$img = $this->image;
		if ((strpos($img, 'profile/') === 0) || (strpos($img, 'freeworld/') === 0)) {
			$img = '/images/' . $img;
		} else {
			$img = '/images/titleimages/list_thumbs/official/small/' . $img;
		}
		return $img;
	}
}