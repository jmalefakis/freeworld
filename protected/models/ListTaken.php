<?php

class ListTaken extends BaseModel
{
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_listtaken';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			 array('list_id, user_id, created, modified', 'required')
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'list_id' => 'List Id'
			
		);
	}

	public function relations()
	{
		return array(
			//'list' => array(self::BELONGS_TO, 'ListForm', 'id'),
			'list' => array(self::BELONGS_TO, 'ListForm', 'list_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'count' => array(self::STAT, 'ListTaken', 'list_id'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}