<?php

class ListRow extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
	public function tableName()
	{
		return 'tbl_listrow';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
		return array( 
			 array('list_id, serial_no,value, user_id, col_id,created, modified', 'required')
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'list_id' => 'List Id'
			
		);
	}
        
        public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('name',$this->name);
                //$criteria->compare('title',$this->title);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}