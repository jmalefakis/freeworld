<?php

class ListColumn extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_listcolumn';
	}

	public function rules() 
	{
            
		return array( 
			 array('list_id, serial_no,value, user_id,created, modified', 'required')
		);
	}

	/** 
	 * @return array relational rules.
	 */
	
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'list_id' => 'List Id'
			
		);
	}
        
        public function search()
	{

		$criteria=new CDbCriteria;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}