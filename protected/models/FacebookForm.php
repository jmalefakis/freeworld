<?php
 
class FacebookForm extends CActiveRecord
{
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_facebook';
	}
        
        public function getFullName() {
            return $this->friend_name;
        }
        

        public function getSuggest($q) { 
            $userid = Yii::app()->user->getID();
            $criteria = new CDbCriteria();
            $criteria->condition = "friend_name LIKE :name AND user_id = :user";
            $criteria->params = array(':name' => trim($q) . '%',':user' => $userid,); 
            
            return $this->findAll($criteria);           
        }   

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('friend_name,friend_facebookid, friend_facebookimg', 'required'),	
                       array('friend_name', 'length', 'max'=>20,),	
		);
	} 
        
        
        
        public function attributeLabels()
	{
              return array(
			'id' => 'Id',
			'friend_name' => 'Friend Name',
			'friend_facebookid' => 'Friend Facebook id',
                        'friend_facebookimg' => 'Friend Facebook Image',	
			
		);
	}
        
        
        public function getImage() {
            return $this->image;
        }
        
       
        
        
        public function search($q=null)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
              
                $userid = Yii::app()->user->getID();                
                
		$criteria=new CDbCriteria;                
		$criteria->compare('friend_name',$this->friend_name ,true);
		$criteria->compare('friend_facebookid',$this->friend_facebookid,true);
		$criteria->compare('friend_facebookimg',$this->friend_facebookimg,true);		
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
                
                $criteria->addSearchCondition('friend_name', $q, true, 'AND');
                $criteria->condition="user_id = $userid";       
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=>20,
                        ),
                         ));
	}
	
}