<?php

class Official extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_official';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
            
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			 array('list_id, ads_id,list_priority, ads_priority,page_no,created, modified', 'required'),
		);
	}

	/** 
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'posts' => array(self::HAS_MANY, 'Post', 'author_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	
        
        
}