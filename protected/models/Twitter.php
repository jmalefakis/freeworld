<?php

class Twitter extends CActiveRecord 
{
    
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_twitter';
	}
        
        public function getFullName() {
            return $this->friend_name;
        }
        

        public function getSuggest($q) { 
            $userid = Yii::app()->user->getID();
            $criteria = new CDbCriteria();
            $criteria->condition = "friend_name LIKE :name AND user_id = :user";
            $criteria->params = array(':name' => trim($q) . '%',':user' => $userid,);
            return $this->findAll($criteria);           
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('friend_name,friend_twitterid, friend_twitteimg', 'required'),					
		);
	}
        
        
        public function getImage() {
            return $this->image;
        }
        
        
        public function search($q=null)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                $userid = Yii::app()->user->getID();
		$criteria=new CDbCriteria;
                
		$criteria->compare('friend_name',$this->friend_name ,true);
		$criteria->compare('friend_twitterid',$this->friend_twitterid,true);
		$criteria->compare('friend_twitteimg',$this->friend_twitteimg,true);		
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
                
                $criteria->addSearchCondition('friend_name', $q, true, 'AND'); 
                $criteria->condition="user_id = $userid";
                //$criteria->addSearchCondition('user_id', $userid, true, 'AND'); 
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=>10,
                        ),
                         ));
	}
	
}