<?php

class UserVisits extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user_visits';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
            
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			 array('user_id, visit_count', 'required'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_visit_id' => 'Id',
			'user_id' => 'User Id',   
            'visit_count' => 'Visit Count',  
            'total_time_on_site' => 'Total Time On Site',
		);
	}
}