<?php

class Favorite extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_favorites';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('favorite_id,user_id', 'required'),			
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'Id',			
		);
	} 
}