<?php

class Friend extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_friends';
    }

    public function relations()
    {
        return array(
                        'myUser' => array(self::BELONGS_TO, 'User', 'user_id'),
                        'friendUser' => array(self::BELONGS_TO, 'User', 'friend_id'),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                        array('friend_id, user_id', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
                        'id' => 'Id',
        );
    }
}
