<?php

class Crew extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_crew';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id,id,otheruser_id,created, modified', 'required'),				
		);
	}
        
        public function relations() {
        return array(
            'crew' => array(self::BELONGS_TO, 'User', 'otheruser_id'),            
            );
        }
        
        
        
}