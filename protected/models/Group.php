<?php

class Group extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('groupname,user_id', 'required'),
			array('groupname', 'max'=>228),			
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'groupname' => 'Group Name'
		);
	}        
}