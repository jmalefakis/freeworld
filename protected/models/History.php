<?php

class History extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_history';
    }

    public function relations()
    {
        return array();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {        
        return array();
    }
}
