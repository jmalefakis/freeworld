<?php

class OpinionComment extends BaseModel
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
		
	public function tableName()
	{
		return 'tbl_opinioncomment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
		return array( 
			 array('opinion_id, value, created, modified', 'required')
		);
	}
	
	public function relations()
	{
		return array(
			'opinion' => array(self::BELONGS_TO, 'Opinion', 'opinion_id', 'joinType' => 'INNER JOIN', 
			                'condition' => 'opinion.archived = 0'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id', 'joinType' => 'INNER JOIN'),
			'list' => array(self::BELONGS_TO, 'List', array('list_id'=>'id'), 'through'=>'opinion'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'opinion_id' => 'Opinion Id',
			'user_id' => 'User Id',
			'value'	=> 'Comment',
		);
	}
		
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('name',$this->name);
				//$criteria->compare('title',$this->title);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeSave()
	{
		if(parent::beforeSave()) {
			if($this->isNewRecord) {
				$this->value = preg_replace('/((http:\/\/|https:\/\/)([^ ]+))/', '<a target="_blank" href="$1">$3</a>', $this->value);
			}
			return true;
		} else
			return false;	
	}
}