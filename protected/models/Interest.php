<?php

class Interest extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_interest';
	}
	
	public function behaviors(){
		return array('CAdvancedArFindBehavior' => array(
				'class' => 'application.extensions.CAdvancedArFindBehavior.CAdvancedArFindBehavior'
		));
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('interest', 'required')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'interest' => 'Categories'
		);
	}
	
	/**
		 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lists' => array(self::HAS_MANY, 'ListForm', 'interest_id', 'joinType'=>'INNER JOIN'),
		);
	}
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('interest',$this->interest,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		return new CActiveDataProvider($this, array(
						'criteria'=>$criteria,
		));
	}
}