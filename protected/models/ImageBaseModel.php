<?php
abstract class ImageBaseModel extends BaseModel
{
	
    public function getImageUrl()
	{
		$img = trim($this->image);
		if ((strpos($img, 'profile/') === 0) || (strpos($img, 'freeworld/') === 0)) {
			$img = '/images/' . $img;
		} else {
			$img = '/images/titleimages/list_thumbs/official/small/' . $img;
		}
		return Yii::app()->request->baseUrl . $img;
	}

}