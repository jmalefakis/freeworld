<?php

class Privacy extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_privacy';
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, relation, rights', 'required'),
			array('relation', 'in', 'range' => RelationEnum::getValidValues()),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
		);
	}
}
