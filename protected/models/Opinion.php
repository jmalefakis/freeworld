<?php

class Opinion extends ImageBaseModel
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function anyImageByValue($forValue)
	{
		$op = Opinion::model()->find(array(
				'select' => 'image',
				'condition' => "value = :op AND image IS NOT NULL AND image != ''",
				'order' => 'position ASC',
				'params' => array(':op' => $forValue),
		));
		 
		if ($op) return $op->image;
		return '';
	}
		
	public function tableName()
	{
		return 'tbl_opinion';
	}

/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
		return array( 
			 array('list_id, user_id, position, value, created, modified', 'required')
		);
	}
	
	public function relations()
	{
		return array(
			'list' => array(self::BELONGS_TO, 'ListForm', 'list_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'comments' => array(self::HAS_ONE, 'OpinionComment', 'opinion_id'),
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'list_id' => 'List Id',
			'user_id' => 'User Id',
			'position' => 'Position',
			'value'	=> 'Opinion',
		);
	}
	
	public function anyImage()
	{
	    return Opinion::anyImageByValue($this-value);
	}
	
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			Notification::model()->deleteAllByAttributes(array('list_id' => $this->id, 'notification_type' => 'opinion'));
			OpinionComment::model()->deleteAllByAttributes(array('opinion_id' => $this->id));
			//OpinionComment::model()->deleteAll('opinion_id = ' . $this->id);
			return true;
		} else
			return false;
	}
	
	public function beforeSave()
	{
		if(parent::beforeSave()) {
			if($this->isNewRecord) {
				$this->value = preg_replace('/((http:\/\/|https:\/\/)([^ ]+))/', '<a target="_blank" href="$1">$3</a>', $this->value);
			}
			return true;
		} else
			return false;	
	}
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('name',$this->name);
				//$criteria->compare('title',$this->title);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}