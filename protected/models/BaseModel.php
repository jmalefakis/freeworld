<?php
abstract class BaseModel extends CActiveRecord
{
		
	public function beforeSave()
	{
		if (parent::beforeSave()) {
			$time = DateHelper::getCurrentSQLDateTime();
		
			if ($this->isNewRecord)
			$this->created = $time;
				
			$this->modified = $time;
 
			return true;
		} else
			return false;
	}
	
}