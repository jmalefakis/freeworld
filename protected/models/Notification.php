<?php

class Notification extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_notification';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
                        array('user_id', 'required'),
                        array('myuser_id', 'required'),
                        array('notification_type', 'required')
        );
    }

    public function relations()
    {
        return array(
                        'userSender' => array(self::BELONGS_TO, 'User', 'myuser_id'),
                        'userReceiver' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }
    
    public function beforeSave()
    {
        if($this->isNewRecord)
        {
            $user = User::model()->findByPk($this->user_id);
            if ($user->isEmailsNotice()) {
                if ($user->isCompany()) {
                    $name = StringsHelper::capitalize($user->firstname);
                } else {
                    $name = StringsHelper::capitalize($user->firstname .' '.$user->lastname);
                }
                
                $text = '';
                $textType = '';
                if ($this->notification_type == 'friends' 
                        || $this->notification_type == 'invitation') {
                    $textType = 'friends';
                    
                } else if ($this->notification_type == 'lists' 
                        || $this->notification_type == 'my_created_lists'
                        || $this->notification_type == 'invite_list'
                        || $this->notification_type == 'my_taken_lists'
                        || $this->notification_type == 'opinion') {
                     $textType = 'lists';
                } else if ($this->notification_type == 'system') {
                     $textType = 'system';
                }
                
                if ($textType == 'friends') {
                    $message = $this->message;
                    if ($message == "") 
                    {
                        $message = "wants to become a friend";
                    } 
                    else if ($message == "You are friends with ") 
                    {
                        $message = "is your friend";
                    }
                    $user2 = User::model()->findByPk($this->myuser_id);
                    $text = '<p><a href="'.Yii::app()->params['siteurl'].'/index.php/user/myDashboard?userid='.$this->myuser_id.'"><span><b>'.trim(StringsHelper::capitalize($user2->firstname.' '.$user2->lastname)).'</b></span></a> '.$message;
                                
                                if ($this->notification_type == 'invitation') {
                                    $list = ListForm::model()->findByPk($this->list_id);
                                    $text .= ' <a href="'.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$this->list_id.'"><b>'.trim(StringsHelper::capitalize($list->title)).'</b></a>'; 
                                }    
                    $text .='.</p>';
                } else if ($textType == 'lists') {
                    $user2 = User::model()->findByPk($this->myuser_id);
                    $text = '<p><a href="'.Yii::app()->params['siteurl'].'/index.php/user/myDashboard?userid='.$this->myuser_id.'"><span><b>'.trim(StringsHelper::capitalize($user2->firstname.' '.$user2->lastname)).'</b></span></a> '.$this->message;

                                if ($this->notification_type == "opinion") {
                                    $opinion = Opinion::model()->findByPk($this->list_id);
                                    $text .= ' "<span>' . trim(StringsHelper::capitalize($opinion->value)) . '</span>" list';

							        $list = ListForm::model()->findByPk($opinion->list_id);
                                    $text .= ' <a href="'.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$opinion->list_id.'"><b>'.StringsHelper::capitalize(trim($list->title)).'</b></a>';
							    } else {
                                    $list = ListForm::model()->findByPk($this->list_id);
                                    $text .= ' <a href="'.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$this->list_id.'"><b>'.StringsHelper::capitalize(trim($list->title)).'</b></a>';
                                } 
                    $text .= '.</p>';
                } else if ($textType == 'system') {
                    $text = '<p>'.$this->message.'.</p>';
                }
                
                if (!empty($text)) {
                    $subject = 'Notification - wayoworld.com';
                    $message = ImageHelper::imageShow(Yii::app()->params['siteurl'].'/images/newimages/', "header_logo_freworld-send2.png", 
                                                      0, 0, array('style' => 'height:180px;width:160px;')) . "
                    <br/><br/>
                    <div style='font-family:Arial;font-size: 20pt; color: #333333;'>
                    </div>
                    <div style='padding: 16px 0 18px;'>
                    Dear $name,<br/><br/>

                    You have received a new <a href='".Yii::app()->params['siteurl']."/index.php/user/myNotifications'><b>Notification</b></a>:
                    <br/>   
                    $text<br/>
                                 Best Regards,<br/>
                                 Wayo Team
                    <br /><br />
                    To stop receiving Notifications by email, please uncheck Email Notifications on your <a href='".Yii::app()->params['siteurl']."/index.php/user/myProfile'>My Account</a> page.

                    </div>" ;

                    $res_send = MailHelper::sendEmail($user->email, "noreply@wayoworld.com", $subject, $message);
                }
            }
            //sleep(1);
            $this->created = new CDbExpression('NOW()');
            $this->modified = new CDbExpression('NOW()');
        } else {
            $this->modified = new CDbExpression('NOW()');
        }
        return true;
    }
}