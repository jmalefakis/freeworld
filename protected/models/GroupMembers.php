<?php

class GroupMembers extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_groupmembers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id,otheruser_id,user_id', 'required'),				
		);
	}
        
        public function relations() {
        return array(
            'groupmembers' => array(self::BELONGS_TO, 'User', 'otheruser_id'),            
            );
        }
        
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',			
		);
	}        
}