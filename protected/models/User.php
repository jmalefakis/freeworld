<?php

class User extends CActiveRecord
{   
    public $validacion;
    /**
     * The followings are the available columns in table 'tbl_user':
     * @var integer $id
     * @var string $username
     * @var string $password
     * @var string $salt
     * @var string $email
     * @var string $profile
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                        array('email, password, interest,created, modified', 'required'),
                        array('firstname, lastname, birthdate, gender', 'required','on'=>'user'),
                        array('country', 'required','on'=>'company'),
                        array('email', 'email'),
                        array('firstname', 'match', 'pattern' => '/^[A-Za-z0-9_ ]+$/u','message' => "Incorrect symbols (A-z0-9)."),
                        array('email', 'unique','attributeName'=> 'email', 'className'=>'User','on'=>'register'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
                        'list' => array(self::HAS_MANY, 'ListForm', 'user_id', 'joinType' => 'INNER JOIN'),
                        'taken' => array(self::HAS_MANY, 'ListTaken', 'user_id', 'joinType' => 'INNER JOIN'),
                        'takenList' => array(self::HAS_MANY, 'ListForm', array('list_id' => 'id'),
                                        'through' => 'taken',
                                        'joinType' => 'INNER JOIN'),
                        'notifications' => array(self::HAS_MANY, "Notification", "user_id",
                                        'condition' => "notifications.status != 'deleted'",
                                        'order' => 'created DESC' ),
                        'interests' => array(self::HAS_MANY, "Interest", // a little hack :)
                                        array("interest` = `interests.interest` OR `interests.interest" => 'interest'),
                                        'on' => ":ypl0 LIKE CONCAT('%', interests.interest, '%')",
                                        'joinType' => "LEFT JOIN")
        );
    }
    
    public function behaviors(){
    	return array('CAdvancedArFindBehavior' => array(
    			'class' => 'application.extensions.CAdvancedArFindBehavior.CAdvancedArFindBehavior'
    	));
    }

    public function unreadNotifications() {
        return $this->notifications(array(
                        'condition' => 'read_notification = 0',
        ));
    }

    public function friendsNotifications() {
        return $this->notifications(array(
                        "condition" => "notification_type = 'friends' 
                            OR notification_type = 'invitation' 
                            OR notification_type = 'invite_list'",    
        ));
    }
    
    public function pendingNotifications() {
    	return $this->notifications(array(
    			"condition" => "notification_type = 'friends' AND status = 'waiting'",
    	));
    }

    public function listsNotifications() {
        return $this->notifications(array(
                        "condition" => "notification_type = 'lists'
                        OR notification_type = 'my_created_lists'   
                        OR notification_type = 'my_taken_lists'
                        OR notification_type = 'opinion'",
        ));
    }

    public function systemNotifications() {
        return $this->notifications(array(
                        "condition" => "notification_type = 'system'",
        ));
    }
    
    public function interestIds()
    {
        $ids = array();
        foreach ($this->interests(array("select" => "id")) as $interest)
        {
            $ids[] = $interest->id;
        }
        return $ids;
    }
    
    public function takenListsIds()
    {        
        $ids = array();
        foreach ($this->takenList(array("select" => "id")) as $takenList)
        {
            $ids[] = $takenList->id;
        }
        return $ids;
    }

    /**
     * @return sorted takenList.
     */
    public function takenListSorted($limit = 0, $offset = 0, $trailingPeriod = 0)
    {
        $condition = '';
        if (!empty($trailingPeriod)) {
            $condition = 'taken.created >= "'.date('Y-m-d H:i:s',mktime(0,0,0,date("m"),(date("d")-$trailingPeriod),date("Y"))).'"';
        }
        
        return $this->takenList(array(
                        'select' => '(SELECT COUNT(*) FROM tbl_listtaken WHERE list_id = takenList.id) AS xyz1, *',
                        'condition' => $condition,
                        'order' => '1 DESC, takenList.id',
                        'limit' => $limit,
                        'offset' => $offset,
        ));
    }

    /**
     * @return sorted favotiteList.
     */
    public function favotiteListSorted($limit = 0, $offset = 0)
    {
        return $this->takenList(array(
                        'select' => '(SELECT COUNT(*) FROM tbl_listtaken WHERE list_id = takenList.id) AS xyz1, *',
                        'condition' => 'taken.favorite > 0',
                        'order' => 'taken.favorite DESC, 1 DESC, takenList.id',
                        'limit' => $limit,
                        'offset' => $offset,
        ));
    }

    /**
     * @return sorted my List.
     */
    public function listSorted($limit=0, $offset=0, $trailingPeriod = 0)
    {        
        $condition = '';
        if (!empty($trailingPeriod)) {
            $condition = 'created >= "'.date('Y-m-d H:i:s',mktime(0,0,0,date("m"),(date("d")-$trailingPeriod),date("Y"))).'"';
        }
        
        return $this->list(array(
                        'select' => '(SELECT COUNT(*) FROM tbl_listtaken WHERE list_id = list.id) AS xyz1, *',
                        'condition' => $condition,
                        'order' => '1 DESC, list.id',
                        'limit' => $limit,
                        'offset' => $offset,
        ));
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
                        'id' => 'Id',
                        'email' => 'Email',
                        'password' => 'Password',
        );
    }
    
    public function isMember()
    {
        return $this->member == "member";
    }
    
    public function isCompany()
    {
        return $this->member == "company user";
    }
    
    public function isAdmin()
    {
        return $this->admin;
    }
    
    public function isEmailsNotice()
    {
        return $this->emails_notice;
    }
    
    public function isEmailsWeekly()
    {
        return $this->emails_weekly;
    }

    public function getFullName()
    {
        $fname = ucfirst($this->firstname);
        $lname = ucfirst($this->lastname);
        return $fname . ' ' . $lname;
    }
    
    public function hasFriend($friendUserId)
    {
        return Friend::model()->findByAttributes(array(
            'user_id' => $this->id,
            'friend_id' => $friendUserId
        ));
    }
    
    public function isPendingFriend($friendUserId)
    {
        return Notification::model()->count(array(
        		"condition" => "notification_type = 'friends' AND status = 'waiting' 
                        AND ((myuser_id = :fid AND user_id = :mid)
                            OR (myuser_id = :mid AND user_id = :fid))",
                "params" => array(
                                ":mid" => $this->id,
                                ":fid" => $friendUserId)
        ));
    }
    
    public static function getUserShortName($user, $wordLength)
    {
        $firstName = $user["firstname"];
        $lastName = $user["lastname"];
        $name = $firstName . ' ' . $lastName;
        
        /*if (strlen($firstName) > $wordLength)
        {
            $firstName = substr($firstName, 0, $wordLength) . "...";
        }
        
        if (strlen($lastName) > $wordLength)
        {
        	$lastName = substr($lastName, 0, $wordLength) . "...";
        }*/
        if (strlen($name) > $wordLength) {
            $name = substr($name, 0, $wordLength) . "...";
        }
        
        return $name;
        //return $firstName . ' ' . $lastName;
    }

    public function getShortName($wordLength)
    {
        return User::getUserShortName($this, $wordLength);
    }

    public function getSuggest($q)
    {
        $c = new CDbCriteria();
        $c->addSearchCondition('firstname', $q, true, 'OR');
        $c->addSearchCondition('lastname', $q, true, 'OR');
        return $this->findAll($c);
    }

    public function encrypt($value)
    {
        return md5($value);
    }

    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('firstname', $this->firstname);
        $criteria->compare('lastname', $this->lastname);
        $criteria->compare('email ', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('company', $this->company, true);
        $criteria->compare('admin', $this->admin, true);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('modified', $this->modified, true);

        return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
        ));
    }
	
    public function getImageUrl()
	{
		$img = trim($this->image);
		if ($img === '') {
			$img = '/images/default.jpg';
		} else {
			$img = '/images/profile/user_thumbs/listcreation/' . $img;
		}
		return Yii::app()->request->baseUrl . $img;
	}
}