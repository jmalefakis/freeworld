<?php

class PollAnswer extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_pollanswer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
            
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			 array('poll_id, value, user_id,serial_no ,created,modified', 'required'),                         
		);
	}

	/** 
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'Temp' => array(self::HAS_MANY, 'ListTaken', 'list_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',	
                        
		);
	}
        
        public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('value',$this->value);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}