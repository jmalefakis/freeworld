SELECT 
    tbl_interest.id AS interest_id,
    tbl_interest.interest,
    tbl_list.id,
    tbl_list.name, 
    tbl_list.title,
    tbl_list.participation,
    tbl_list.image, 
    tbl_list.user_id,
    count(tbl_listtaken.list_id) AS numgh
FROM tbl_interest 
LEFT JOIN tbl_list ON tbl_interest.id = tbl_list.interest_id
LEFT JOIN tbl_listtaken ON tbl_list.id = tbl_listtaken.list_id 
WHERE tbl_interest.interest RLIKE ''
    OR tbl_list.title RLIKE '(^[sS][^ -\w+]*[pP][^ -\w+]*[oO][^ -\w+]*[rR][^ -\w+]*[tT])|( [sS][^ -\w+]*[pP][^ -\w+]*[oO][^ -\w+]*[rR][^ -\w+]*[tT])'
GROUP BY tbl_list.id 
ORDER BY numgh DESC, tbl_list.id;
        