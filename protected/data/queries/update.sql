DELIMITER //GO

ALTER TABLE `freeworld_demo`.`tbl_user`
ADD `badge` BOOLEAN NOT NULL;
//GO

-- ALTER TABLE `freeworld_demo`.`tbl_list`
-- ADD `badge` BOOLEAN NOT NULL;
-- //GO

-- for version 1.1.0.37
-- CREATE TEMPORARY TABLE IF NOT EXISTS `freeworld_demo`.`wlt_ids` AS (
--     SELECT t.id FROM `freeworld_demo`.`tbl_listtaken` t
--     LEFT JOIN `freeworld_demo`.`tbl_opinion` o 
--         ON t.user_id = o.user_id AND t.list_id = o.list_id
--     WHERE o.id IS NULL
-- );
-- //GO

-- DELETE FROM `freeworld_demo`.`tbl_listtaken`
-- WHERE id IN (SELECT * FROM `freeworld_demo`.`wlt_ids`);
-- //GO

-- DROP TABLE IF EXISTS `freeworld_demo`.`wlt_ids`;
-- //GO

-- for version 1.0.3
-- DROP TABLE IF EXISTS `freeworld_demo`.`tbl_privacy`;
-- //GO

-- CREATE TABLE IF NOT EXISTS `freeworld_demo`.`tbl_privacy` (
--   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
--   `user_id` int(10) unsigned NOT NULL,
--   `relation` enum('Everybody','Friends','Followees','Followers') COLLATE utf8_unicode_ci NOT NULL,
--   `rights` set('Dashboard','About Me','Contact Info') COLLATE utf8_unicode_ci NOT NULL,
--  PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='users privacy settings';
-- //GO

-- for version 1.0
-- ALTER TABLE `freeworld_demo`.`tbl_opinioncomment`
-- ADD COLUMN `user_id` int(11) DEFAULT NULL;
-- //GO

-- UPDATE `freeworld_demo`.`tbl_opinioncomment` 
-- SET user_id = (SELECT user_id 
-- FROM `freeworld_demo`.`tbl_opinion` c1 
-- WHERE c1.id = `freeworld_demo`.`tbl_opinioncomment`.opinion_id);
-- //GO

-- for version 1.0.5.29
-- CREATE TABLE IF NOT EXISTS `freeworld_demo`.`tbl_archivestat` (
--     `id` int(11) NOT NULL AUTO_INCREMENT,
--     `lock` int(1) NOT NULL,    
--     `created` datetime NOT NULL,
--     `modified` datetime NOT NULL,
--     PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
-- //GO

-- CREATE TABLE IF NOT EXISTS `freeworld_demo`.`tbl_archive` (
--     `id` int(11) NOT NULL AUTO_INCREMENT,
--     `list_id` int(11) NOT NULL,
--     `count` int(11) NOT NULL,
--     `created` datetime NOT NULL,
--     `modified` datetime NOT NULL,
--     PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
-- //GO

-- for version 1.0.5.28
-- ALTER TABLE `freeworld_demo`.`tbl_opinion`
-- ADD COLUMN `archived` TINYINT(1) NOT NULL DEFAULT 0;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_list`
-- ADD COLUMN `archive_schedule` TEXT NULL;
-- //GO

-- for version 1.0.5.26
-- CREATE TABLE IF NOT EXISTS `freeworld_demo`.`tbl_opinion` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `user_id` int(11) NOT NULL,
--   `list_id` int(11) NOT NULL,
--   `position` int(11) NOT NULL,
--   `image` varchar(111) NOT NULL,
--   `value` text NOT NULL,
--   `created` datetime NOT NULL,
--   `modified` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
-- //GO

-- CREATE TABLE IF NOT EXISTS `freeworld_demo`.`tbl_opinioncomment` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `opinion_id` int(11) NOT NULL,
--   `value` text NOT NULL,
--   `created` datetime NOT NULL,
--   `modified` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
-- //GO

-- INSERT INTO `freeworld_demo`.`tbl_opinion`
-- SELECT DISTINCT
--     (SELECT min(`id`) FROM `freeworld_demo`.`tbl_listrow` t3 
--         WHERE t3.value = t2.value AND t3.user_id = t2.user_id AND t3.list_id = t2.list_id),
--     `user_id`,
--     `list_id`,
--     (SELECT min(`serial_no`) FROM `freeworld_demo`.`tbl_listrow` t6 
--         WHERE t6.value = t2.value AND t6.user_id = t2.user_id AND t6.list_id = t2.list_id),
--     '', -- images
--     `value`,
--     (SELECT min(`created`) FROM `freeworld_demo`.`tbl_listrow` t4 
--         WHERE t4.value = t2.value AND t4.user_id = t2.user_id AND t4.list_id = t2.list_id),
--     (SELECT max(`modified`) FROM `freeworld_demo`.`tbl_listrow` t5 
--         WHERE t5.value = t2.value AND t5.user_id = t2.user_id AND t5.list_id = t2.list_id)
-- FROM `freeworld_demo`.`tbl_listrow` t2
-- ORDER BY 1;
-- //GO

-- INSERT INTO `freeworld_demo`.`tbl_opinioncomment`
--     (`opinion_id`, `value`, `created`, `modified`)
-- SELECT
--     (SELECT `id` FROM `freeworld_demo`.`tbl_opinion` t3 
--         WHERE t3.value = t2.value AND t3.user_id = t2.user_id AND t3.list_id = t2.list_id 
--             AND t3.position = (SELECT min(`serial_no`) FROM `freeworld_demo`.`tbl_listrow` t6 
--                 WHERE t6.value = t2.value AND t6.user_id = t2.user_id AND t6.list_id = t2.list_id)
--     ),
--     `rating`,
--     `created`,
--     `modified`
-- FROM `freeworld_demo`.`tbl_listrow` t2
-- WHERE t2.rating IS NOT NULL AND t2.rating != ''
-- ORDER BY 1;
-- //GO

-- for version 1.0.4.25
-- DROP TRIGGER `freeworld_demo`.`take_list`;
-- //GO

-- CREATE TRIGGER `freeworld_demo`.`take_list`
-- AFTER INSERT ON `freeworld_demo`.`tbl_listtaken`
-- FOR EACH ROW
-- BEGIN
--     INSERT INTO `freeworld_demo`.`tbl_history`
--          (`object_id`, `actor_id`, `object_type`, `object_status`, `operation`, `created`, `modified`)
--     VALUES
--          (NEW.`list_id`, NEW.`user_id`, 'list', 'taken', 'take_list', NEW.`created`, NEW.`modified`);
-- END;
-- //GO

-- for version 1.0.4.24
-- DROP TRIGGER `freeworld_demo`.`user_update`;
-- //GO

-- CREATE TRIGGER `freeworld_demo`.`user_update`
-- AFTER UPDATE ON `freeworld_demo`.`tbl_user`
-- FOR EACH ROW
-- BEGIN
--     INSERT INTO `freeworld_demo`.`tbl_history`
--         (`object_id`, `actor_id`, `object_type`, `operation`, `created`, `modified`)
--     VALUES
--         (OLD.`id`, OLD.`id`, 'user', 'status_update', NEW.`modified`, NEW.`modified`);
-- END; 
-- //GO

-- for version 1.0.4.23
-- ALTER TABLE `freeworld_demo`.`tbl_list` 
-- DROP COLUMN `modified`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_list` 
-- DROP COLUMN `created`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_list`
-- ADD COLUMN `position` INT(11) NOT NULL DEFAULT 0;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_list`
-- ADD COLUMN `created` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_list`
-- ADD COLUMN `modified` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO


-- ALTER TABLE `freeworld_demo`.`tbl_listrow`
-- DROP COLUMN `modified`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_listrow`
-- ADD COLUMN `modified` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO


-- ALTER TABLE `freeworld_demo`.`tbl_user` 
-- DROP COLUMN `modified`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_user` 
-- DROP COLUMN `created`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_user`
-- ADD COLUMN `created` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_user`
-- ADD COLUMN `modified` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO


-- ALTER TABLE `freeworld_demo`.`tbl_friends` 
-- DROP COLUMN `created`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_friends`
-- ADD COLUMN `created` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO


-- ALTER TABLE `freeworld_demo`.`tbl_official` 
-- DROP COLUMN `modified`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_official` 
-- DROP COLUMN `created`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_official`
-- ADD COLUMN `created` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_official`
-- ADD COLUMN `modified` datetime NOT NULL 
-- DEFAULT '2013-09-19 00:00:00';
-- //GO

-- for version 1.0.3.16
-- ALTER TABLE `freeworld_demo`.`tbl_friends`
-- ADD COLUMN `notifications_on` TINYINT(1) NOT NULL DEFAULT 1;
-- //GO
    
-- for version 1.0.3.14
-- CREATE TABLE IF NOT EXISTS `freeworld_demo`.`tbl_privacy` (
--     `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
--     `user_id` int(10) unsigned NOT NULL,
--     `relation` enum('Everybody','Friends','Companies') COLLATE utf8_unicode_ci NOT NULL,
--     `rights` set('Dashboard','About Me','Contact Info') COLLATE utf8_unicode_ci NOT NULL,
--     PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='users privacy settings';


-- for version 1.0.3.13
-- ALTER TABLE `freeworld_demo`.`tbl_notification`
-- DROP COLUMN `modified`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_notification`
-- DROP COLUMN `created`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_notification`
-- ADD COLUMN `created` datetime NULL;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_notification`
-- ADD COLUMN `modified` datetime NULL;
-- //GO

-- CREATE TABLE IF NOT EXISTS `freeworld_demo`.`tbl_history` (
--     `id` int(30) NOT NULL AUTO_INCREMENT,  
--     `object_id` int(30) NOT NULL,
--     `actor_id` int(30) NOT NULL,
--     `object_type` varchar(50) NOT NULL,
--     `object_status` varchar(50) NULL,
--     `operation` varchar(50) NOT NULL,
--     `flags` int(30) NULL,
--     `created` datetime NOT NULL,
--     `modified` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1; 
-- //GO

-- CREATE TRIGGER `freeworld_demo`.`user_update` 
-- AFTER UPDATE ON `freeworld_demo`.`tbl_user`
-- FOR EACH ROW
-- BEGIN
--     IF NEW.`status_text` != OLD.`status_text` THEN
--         INSERT INTO `freeworld_demo`.`tbl_history`
--             (`object_id`, `actor_id`, `object_type`, `operation`, `created`, `modified`)
--         VALUES
--             (OLD.`id`, OLD.`id`, 'user', 'status_update', NOW(), NOW());
--     END IF;
-- END; 
-- //GO

-- CREATE TRIGGER `freeworld_demo`.`take_list`
-- AFTER INSERT ON `freeworld_demo`.`tbl_listtaken`
-- FOR EACH ROW
-- BEGIN
--     INSERT INTO `freeworld_demo`.`tbl_history`
--         (`object_id`, `actor_id`, `object_type`, `object_status`, `operation`, `created`, `modified`)
--     VALUES
--         (NEW.`id`, NEW.`user_id`, 'list', 'taken', 'take_list', NOW(), NOW());
-- END;
-- //GO

-- for version 1.0.2.12
-- ALTER TABLE `freeworld_demo`.`tbl_user`
-- ADD COLUMN `status_text` VARCHAR(200) NULL;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_notification`
-- ADD COLUMN `status` VARCHAR(30) NOT NULL DEFAULT "waiting";
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_friends` 
-- DROP COLUMN `friend_name`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_friends`
-- DROP COLUMN `status`;
-- //GO

-- ALTER TABLE `freeworld_demo`.`tbl_friends` 
-- DROP COLUMN `modified`;
-- //GO
