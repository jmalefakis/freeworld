CREATE TABLE `tbl_privacy` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(10) UNSIGNED NOT NULL,
	`relation` ENUM('Everybody','Friends','Followees','Followers') NOT NULL COLLATE 'utf8_unicode_ci',
	`rights` SET('Dashboard','About Me','Contact Info') NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COMMENT='users privacy settings'
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=92;
