SELECT DISTINCT
    tbl_user.*,
    IF (tbl_friends.user_id = 21, 1, 0) AS is_friend,
    IF (tbl_notification.myuser_id = 21, 1, 0) AS is_pending
FROM tbl_user
LEFT JOIN tbl_friends ON tbl_user.id = tbl_friends.friend_id
LEFT JOIN tbl_notification ON tbl_user.id = tbl_notification.user_id
WHERE tbl_user.member = 'member'
    AND CONCAT_WS(' ', tbl_user.firstname, tbl_user.lastname) RLIKE '.*' 
    AND ((tbl_user.state RLIKE 'R.*' 
            OR tbl_user.city RLIKE 'R.*'
            OR CONCAT_WS(' ', tbl_user.city, tbl_user.state) RLIKE 'R.*')
        OR (tbl_user.school RLIKE 'x.*' OR tbl_user.education_more RLIKE 'x.*'))
ORDER BY is_friend DESC, is_pending DESC, tbl_user.id ASC
LIMIT 90 OFFSET 0;
