<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{  //echo 'username'; exit;
                $email = $_POST['LoginForm']['username'];                
                $password = $_POST['LoginForm']['password'];
		$record=User::model()->findByAttributes(array('username'=>$email));
		if($record===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$record->encrypt($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id=$record->id;
			$this->username=$record->username;
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode==self::ERROR_NONE;
	}
        
        public function authenticate_facebook()
	{  
             //echo 'here'; exit;     
		$record=User::model()->findByAttributes(array('username'=>$this->username)); 
                if($record===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$record->encrypt($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id=$record->id;
			$this->username=$record->username;
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}