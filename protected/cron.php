<?php
date_default_timezone_set('UTC');

$yiic = dirname(__FILE__) . '/../framework/yii.php';

$config = dirname(__FILE__) . '/config/cron.php';
defined('YII_DEBUG') or define('YII_DEBUG',true);
require_once($yiic);
Yii::createConsoleApplication($config)->run();
?>