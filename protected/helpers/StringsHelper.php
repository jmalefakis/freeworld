<?php

class StringsHelper
{
	
	public static function capitalize($string)
	{
		$string =ucwords($string);

        foreach (array('-') as $delimiter) { //, '\''
          if (strpos($string, $delimiter)!==false) {
            $string =implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
          }
        }
        return $string;
	}
}
?>