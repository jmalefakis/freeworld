<?php 
class MailHelper
{
    public static function sendEmail($to, $from, $subject, $message)
    {        				
        $mail = Yii::app()->SmtpMail;
        $mail->ClearTo();
        $mail->SetFrom("contact@wayoworld.com", "Contact");
        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        $mail->AddAddress($to, "");
        return $mail->Send();

        // To send HTML mail, the Content-type header must be set
        /*$headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= "From: " . $from . "\r\n";

        // Try to Mail it
        return mail($to, $subject, $message, $headers);*/
    }
}
?>