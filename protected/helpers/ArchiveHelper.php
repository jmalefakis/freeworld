<?php
class ArchiveHelper 
{
    /* [
    {
        "name": "Schedule",
        "value": "daily"
    }, {
        "name": "days",
        "value": "1"
    }, {
        "name": "week",
        "value": "1"
    }, {
        "name": "weekdays",
        "value": "Friday"
    }, {
        "name": "monthdays",
        "value": "monthdays1"
    }, {
        "name": "monthlyDay",
        "value": "6st"
    }, {
        "name": "months",
        "value": "1"
    }, {
        "name": "monthlyNum",
        "value": "third"
    }, {
        "name": "monthlyWeekDay",
        "value": "Tuesday"
    },
    {
        "name": "monthlyMonth",
        "value": "1"
    },
    {
        "name": "yearlyMonth",
        "value": "August"
    },
    {
        "name": "yearlyDay",
        "value": "11st"
    },
    {
        "name": "startDate",
        "value": "10/23/2013"
    },
    {
        "name": "changeDate",
        "value": "endAfter"
    },
    {
        "name": "occurrences",
        "value": "1"
    },
    {
        "name": "stopDate",
        "value": "10/24/2013"
    }
    ] */
    public static function getNextArchiveDate($listId)
    {        
        $list = ListForm::model()->findByPk($listId);
        if ($list && $list->archive_schedule)
        {
            $dsc = 86400; // 1 day
            $schedule = json_decode($list->archive_schedule);
            
            // today by default, not tomorrow
            $currDate = $nextDate = intval(time() / $dsc) * $dsc - $dsc; // 00:00 AM

            // get archive entry with last archive date if exists
            $listArchive = Archive::model()->findByAttributes(array(
            		"list_id" => $listId
            ));
            if ($listArchive) {
            	$llDate = strtotime($listArchive->modified);
            	$currDate = $nextDate = intval($llDate / $dsc) * $dsc; // 00:00 AM
            }            
            
            $startDate = ArchiveHelper::asfind($schedule, 'startDate');
            if ($startDate) {
                $startDateTime = strtotime($startDate, $currDate);
                $startDateTime = intval($startDateTime / $dsc) * $dsc - $dsc; // 00:00 AM
                if ($startDateTime > $nextDate) {
                    $nextDate = $startDateTime;
                }
            }
            
            $sType = ArchiveHelper::asfind($schedule, 'Schedule');
            if (!$sType) return "Interval not selected!";
            if ($sType == 'daily') {
                $days = ArchiveHelper::asfind($schedule, 'days');
                if (!$days) return "Days count not set!";
                
                $nextDate += $days * $dsc;
                
            } elseif ($sType == 'weekly') {
                $weeks = ArchiveHelper::asfind($schedule, 'week');
                if (!$weeks) return "Weeks count not set!";
                $weekDay = ArchiveHelper::asfind($schedule, 'weekdays');
                if (!$weekDay) return "Week day not set!";
                
                $nextDate += ($weeks - 1) * 7 * $dsc + $dsc;
                while (date("l", $nextDate) != $weekDay) {
                    $nextDate += $dsc; // add 1 day
                }
                
            } elseif ($sType == 'monthly') {
                $monthdays = ArchiveHelper::asfind($schedule, 'monthdays');
                if (!$monthdays) return "Monthly archive type (row) not selected!";
                if ($monthdays == "monthdays1") {
                    $monthlyDay = ArchiveHelper::asfind($schedule, 'monthlyDay');
                    if (!$monthlyDay) return "Month Day not selected!";
                    $months = ArchiveHelper::asfind($schedule, 'months');
                    if (!$months) return "Month Count not set!";
                    
                    $monthlyDay = intval($monthlyDay);                    
                    $nextDate += $dsc;
                    $nextDate += ($months - 1) * 287 * $dsc / 10;
                    while (date("j", $nextDate) != $monthlyDay) {
                        $nextDate += $dsc;
                    }
                    
                } elseif ($monthdays == "monthdays2") {
                    $monthlyNum = ArchiveHelper::asfind($schedule, 'monthlyNum');
                    if (!$monthlyNum) return "Month Day number not selected!";                    
                    $monthlyWeekDay = ArchiveHelper::asfind($schedule, 'monthlyWeekDay');
                    if (!$monthlyWeekDay) return "Week Day not selected!";
                    $monthlyMonth = ArchiveHelper::asfind($schedule, 'monthlyMonth');
                    if (!$monthlyMonth) return "Month Count not set!";
                    
                    $monthlyNum = $monthlyNum == 'first' ? 1
                        : ($monthlyNum == 'second' ? 2 : ($monthlyNum == 'third' ? 3 : 4));
                    
                    $dStart = ($monthlyNum - 1) * 7 + 1;
                    $dStop = $monthlyNum * 7 + 1;
                    
                    $nextDate += $dsc;                    
                    $nextDate += ($monthlyMonth - 1) * 287 * $dsc / 10;
                    while (date("l", $nextDate) != $monthlyWeekDay
                        || !(date("j", $nextDate) >= $dStart && date("j", $nextDate) < $dStop)) {
                    	$nextDate += $dsc;
                    }
                    
                } else return "Invalid row!";                
            } elseif ($sType == 'yearly') {
                $yearlyMonth = ArchiveHelper::asfind($schedule, 'yearlyMonth');
                if (!$yearlyMonth) return "Month not selected!";                
                $yearlyDay = ArchiveHelper::asfind($schedule, 'yearlyDay');
                if (!$yearlyDay) return "Day number not selected!";
                                
                $yearlyDay = intval($yearlyDay);
                $nextDate += $dsc;
                while (date("F", $nextDate) !=  $yearlyMonth
                    || date("j", $nextDate) != $yearlyDay) {
                    $nextDate += $dsc;
                }                                
            } else return "Invalid interval!";
            
            $sRange = ArchiveHelper::asfind($schedule, 'changeDate');
            if (!$sRange) return "Date Range not selected!";
            
            if ($sRange == "stopAfter") {
                $stopDate = ArchiveHelper::asfind($schedule, 'stopDate');
                if (!$stopDate) return "Stop Date not selected!";
                $stopDateTime = strtotime($stopDate, $currDate);
                $stopDateTime = $stopDateTime / $dsc * $dsc; // 00:00 AM
                if ($nextDate > $stopDateTime) return "-";                
            } elseif ($sRange == "endAfter") {
                $stopDate = ArchiveHelper::asfind($schedule, 'occurrences');
                if (!$stopDate) return "Occurrences count not set!";
            }            
            
            return date("m/d/Y", $nextDate);
        }
        
        return "";
    }
    
    private static function asfind($sched, $key)
    {
        foreach ($sched as $opt)
        {
            if ($opt->name == $key) return $opt->value;
        }
        
        return null;
    }
    
    
}
?>