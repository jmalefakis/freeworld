<?php
class PrivacySettingsHelper {

    private static function _getMemberDefaults()
    {
        return array(
                        'everybody' => array(
                                        'dashboard' => false,
                                        'about me' => false,
                                        'contact info' => false,
                        ),
                        'friends' => array(
                                        'dashboard' => true,
                                        'about me' => true,
                                        'contact info' => true,
                        ),
                        'followees' => array(
                                        'dashboard' => false,
                                        'about me' => false,
                                        'contact info' => true,
                        ),
        );
    }

    private static function _getCompanyDefaults()
    {
        return array(
                        'everybody' => array(
                                        'dashboard' => true,
                                        'about me' => true,
                                        'contact info' => true,
                        ),
                        'followers' => array(
                                        'dashboard' => true,
                                        'about me' => true,
                                        'contact info' => true,
                        ),
        );
    }

    private static function _getDefaults($isMember)
    {
        return $isMember === true
        ? PrivacySettingsHelper::_getMemberDefaults()
        : PrivacySettingsHelper::_getCompanyDefaults();
    }

    /* get privacy settings for user = $userId */
    public static function getSettings($userId)
    {
        //$result = array_fill_keys(RelationEnum::getValidValues(), 0);
        $user = User::model()->findByPk($userId);
        if ($user) {
            $result = PrivacySettingsHelper::_getDefaults($user->isMember());
            	
            $data = Privacy::model()->findAll('user_id = :user_id', array(':user_id' => $userId));

            foreach($data as $privacy) {
                $rights = explode(',', strtolower($privacy->rights));
                $relation = strtolower($privacy->relation);
                if (isset($result[$relation])) {
                    foreach($result[$relation] as $rname => $rvalue) {
                        $result[$relation][$rname] = in_array($rname, $rights);
                    }
                }
            }
        } else {
            $result = array();
        }
        return $result;
    }

    /* set privacy settings for user = $userId */
    public static function setSettings($userId, $data)
    {
        if (!isset($data)) {
            $data = array();
        }

        $user = User::model()->findByPk($userId);
        if ($user) {
            $result = PrivacySettingsHelper::_getDefaults($user->isMember());
            foreach($result as $relation => $rights) {
                $privacy = Privacy::model()->findByAttributes(array('user_id' => $userId, 'relation' => $relation));
                if (!$privacy) {
                    $privacy = new Privacy();
                }
                $privacy->user_id  = $userId;
                $privacy->relation = $relation;
                $privacy->rights   = isset($data[$relation]) ? implode(',', array_keys($data[$relation])) : '';
                $privacy->save(false);
            }
        }
    }

}

/*
 public static function enumItem($model,$attribute)
 {
$attr=$attribute;
self::resolveName($model,$attr);
preg_match('/\((.*)\)/',$model->tableSchema->columns[$attr]->dbType,$matches);
foreach(explode(',', $matches[1]) as $value) {
$value=str_replace("'",null,$value);
$values[$value]=Yii::t('enumItem',$value);
}
return $values;
}


protected function _getColumnSQL()
{
return "enum('".
                implode("', '", $this->getDataForDropDown()).
                "') NOT NULL";
}
*/