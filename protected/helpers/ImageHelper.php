<?php
class ImageHelper {
    
	public static function imageCreateFromSrc($src)	{
		$source_image = false;
		$size = getimagesize($src);
		switch ($size["mime"]) {
			case "image/jpeg":
				$source_image = imagecreatefromjpeg($src); //jpeg file
				break;
			case "image/gif":
				$source_image = imagecreatefromgif($src); //gif file
				break;
			case "image/png":
				$source_image = imagecreatefrompng($src); //png file
				break;
			default:				
				break;
		}
		
		return $source_image;
	}
    
    public static function imageShow($src, $image, $width = 0, $height = 0, $html = array(), $alt = '') {
        if (strpos($src,'http') === false) {
            $src = Yii::app()->request->hostInfo.$src;
        }
        
        $withAndHeight = '';
        if (!empty($width) && !empty($height)) {
            $withAndHeight = '?w='.$width.'&h='.$height;
        }
        
        $src .= html_entity_decode($image).$withAndHeight;        
        return CHtml::image($src, $alt, $html);
    }
}
?>