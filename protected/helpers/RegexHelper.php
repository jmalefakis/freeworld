<?php

class RegexHelper
{
	//const SPECIAL_CHARACTER_REGEX = '[^ -#\w]';
	const SPECIAL_CHARACTER_REGEX = '[^ -#a-zA-Z0-9]';
	
	public static function prepare($string)
	{
		return preg_replace('/' . RegexHelper::SPECIAL_CHARACTER_REGEX . '/i', '', $string);
	}
	
    /**  
	 * 1. search parts of the words, but don't search in the midle
	 * 2. Ignore all special characters and letter case
	 * "Sport" -> "(^[sS][^ -\w+]*[pP][^ -\w+]*[oO][^ -\w+]*[rR][^ -\w+]*[tT])|( [sS][^ -\w+]*[pP][^ -\w+]*[oO][^ -\w+]*[rR][^ -\w+]*[tT])"
	 */
	public static function regex($string, $prepare = false)
	{
		if ($prepare) $string = RegexHelper::prepare($string);
		
		$reg = RegexHelper::SPECIAL_CHARACTER_REGEX . '*';
		for ($i = 0; $i < strlen($string); $i++) {
			$reg .= '[' . strtolower($string[$i]) . strtoupper($string[$i]) . ']';
			if ($string[$i] == ' ') $reg .= '*';
			$reg .= RegexHelper::SPECIAL_CHARACTER_REGEX . '*';
		}
		$reg = '(^' . $reg . ')|( ' . $reg . ')';
		return $reg;
	}
	
	public static function regex2($string, $prepare = false) 
	{
		if ($prepare) $string = RegexHelper::prepare($string);
		
		$reg = RegexHelper::SPECIAL_CHARACTER_REGEX . '*';
		for ($i = 0; $i < strlen($string); $i++) {
			if ($string[$i] == ' ') {
				//$reg .= '[ \W]+([ \W]*[\w]{0,2}[ \W]*)*[ \W]+';
				//$reg .= '([^\w]+([ \W]*[\w]{0,2}[ \W]*)*[^\w]+)';
				$reg .= '[^a-zA-Z0-9]+([^a-zA-Z0-9]*[a-zA-Z0-9]{0,2}[^a-zA-Z0-9]+)*';
			} else {
				$reg .= '[' . strtolower($string[$i]) . strtoupper($string[$i]) . ']';
				$reg .= RegexHelper::SPECIAL_CHARACTER_REGEX . '*';
			}
		}			
		$reg = '(^' . $reg . ')|( ' . $reg . ')';
		return $reg;
	}
}
?>