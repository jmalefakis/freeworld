<?php
class HistoryHelper
{
    public static function getUsersWithDailyUpdatedStatus($usersFromList = null)
    {
        $cmd = Yii::app()->db->createCommand()
        ->select("u.*")
        ->from("{{history}} h")
        ->leftJoin("{{user}} u", "h.object_id = u.id AND h.object_type = 'user'")
        ->where("u.status_text != ''
                        AND h.operation = 'status_update' 
                        AND h.created < :cdt
                        AND h.created + INTERVAL 1 DAY > :cdt",
                        array(':cdt' => DateHelper::getCurrentSQLDateTime())
        )
        ->order("h.created DESC");
        $cmd->distinct = true;

        if ($usersFromList !== null)
        {
            if (count($usersFromList) == 0) return array();
            
            $cmd->andWhere(array("IN", "h.object_id", $usersFromList));
        }

        $cmd->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'User');

        return $cmd->queryAll();
    }

    public static function getMyCreatedListsDailyContributionCount($usersFromList = null)
    {
        $userId = Yii::app()->user->getID();

        $cmd = Yii::app()->db->createCommand()
        ->select("COUNT(*)")
        ->from("{{history}} h")
        ->leftJoin("{{list}} l", "h.object_id = l.id AND h.object_type = 'list'")
        ->where("l.user_id = :uid
                        AND h.operation = 'take_list'
                        AND h.object_status = 'taken'
                        AND h.created < :cdt
                        AND h.created + INTERVAL 1 DAY > :cdt",
                        array(':uid' => $userId, ':cdt' => DateHelper::getCurrentSQLDateTime())
        )
        ->order("h.created DESC");
        
        if ($usersFromList !== null)
        {
            if (count($usersFromList) == 0) return 0;
            
            $cmd->andWhere(array("IN", "h.actor_id", $usersFromList));
        }

        return $cmd->queryScalar();
    }
    
    public static function getMyTakenListsDailyContributionCount($usersFromList = null)
    {
    	$userId = Yii::app()->user->getID();
    
    	$cmd = Yii::app()->db->createCommand()
    	->select("COUNT(*)")
    	->from("{{listtaken}} lt")
    	->leftJoin("{{history}} h", "lt.list_id = h.object_id AND h.object_type = 'list'")
    	->where("lt.user_id = :uid
                        AND h.operation = 'take_list'
                        AND h.object_status = 'taken'
                        AND h.created < :cdt
                        AND h.created + INTERVAL 1 DAY > :cdt",
    			array(':uid' => $userId, ':cdt' => DateHelper::getCurrentSQLDateTime())
    	)
    	->order("h.created DESC");
    
    	if ($usersFromList !== null)
    	{
    	    if (count($usersFromList) == 0) return 0;
    	    
    		$cmd->andWhere(array("IN", "h.actor_id", $usersFromList));
    	}
    
    	return $cmd->queryScalar();
    }
}
?>