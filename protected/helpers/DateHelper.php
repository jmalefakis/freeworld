<?php 
define('SQL_DATE_FORMAT', 'Y-m-d H:i:s');

class DateHelper {
    
    public static function getCurrentSQLDateTime()
    {
        return date(SQL_DATE_FORMAT);
    }
    
    public static function getSQLDateTime($time)
    {
    	return date(SQL_DATE_FORMAT, $time);
    }
    
    public static function toClientTime($time)
    {
        if (isset($_SESSION["tz"])) {
            //date_default_timezone_set('America/New_York');
            $dateTime = new DateTime("@".$time);
            $la_time = new DateTimeZone($_SESSION["tz"]);
            $dateTime->setTimezone($la_time);
            $time = strtotime($dateTime->format('Y-m-d H:i:s'));
            //date_default_timezone_set('UTC');
        }
        
        return $time;
    }
    
    public static function getClientTime($timeStr = null)
    {
        $time = time();
        
        if ($timeStr !== null)
        {
            $time = strtotime($timeStr);
        }
        
        return DateHelper::toClientTime($time);
    }
    
    public static function datetimeToAgoStr($timeStr)
    {
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    	$lengths = array("60","60","24","7","4.35","12","10");
    
    	$now = time();
    	$unix_date = strtotime($timeStr);
    
    	// is it future date or past date
    	if ($now > $unix_date) 
    	{
    		$difference = $now - $unix_date;
    		$tense = "ago";
    	} 
    	else 
    	{
    		return "just now!";
    		$difference = $unix_date - $now;
    		$tense = "from now";
    	}
    
    	for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) 
    	{
    		$difference /= $lengths[$j];
    	}
    
    	$difference = round($difference);
    
    	if ($difference != 1) 
    	{
    		$periods[$j] .= "s";
    	}
    
    	return "$difference $periods[$j] {$tense}";
    }

	public static function getTimeElapsedString($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		//return $string ? implode(', ', $string) . ' ago' : 'just now';
		return $string ? implode(', ', $string) . ' ago' : '1 second ago';
	}
}
?>