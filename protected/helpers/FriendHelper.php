<?php
class FriendHelper {

    private static function getLastIdentity()
    {
        $connection = Yii::app()->db;
        $cmd = $connection->createCommand("SELECT LAST_INSERT_ID() AS LIID;");
        $id = $cmd->queryAll();

        return $id[0]['LIID'];
    }

    public static function followCompany($userId, $companyId)
    {
        $friend = Friend::model()->findByAttributes(array(
                        'user_id' => $userId,
                        'friend_id' => $companyId
        ));

        if ($friend != null)
        {
            return $friend["id"];
        }

        // create new if not exists
        $friend = new Friend();
        $friend->user_id = $userId;
        $friend->friend_id = $companyId;
        $friend->created = DateHelper::getCurrentSQLDateTime();
        $friend->save(false);

        return FriendHelper::getLastIdentity();
    }

    public static function inviteFriend($userId, $friendId, $message)
    {
        $notification = Notification::model()->findByAttributes(array(
                        'myuser_id' => $userId,
                        'user_id' => $friendId,
                        'notification_type' => 'friends',
                        'status' => 'waiting'
        ));

        if ($notification != null)
        {
            $notification->message = $message;
            $notification->read_notification = 0;
            $notification->notification_type = 'friends';
            $notification->modified = DateHelper::getCurrentSQLDateTime();
            $notification->save(false);

            return $notification["id"];
        }

        $notification = new Notification();
        $notification->myuser_id = $userId;
        $notification->user_id = $friendId;
        $notification->message = $message;
        $notification->status = 'waiting';
        $notification->read_notification = 0;
        $notification->notification_type = 'friends';
        $notification->created = DateHelper::getCurrentSQLDateTime();
        $notification->modified = DateHelper::getCurrentSQLDateTime();
        $notification->save(false);

        return FriendHelper::getLastIdentity();
    }

    private static function getInvite($userId, $friendId, $notificationId = null)
    {
        $invite = null;

        if ($notificationId)
        {
            $invite = Notification::model()->findByPk($notificationId);
        }
        else
        {
            $invite = Notification::model()->find(array(
                            'condition' => "myuser_id = :y AND user_id = :x AND notification_type = 'friends' AND status = 'waiting'",
                            'params' => array(':x' => $userId, ':y' => $friendId)
            ));
        }

        return $invite;
    }

    public static function removeFriendRequest($userId, $friendId, $notificationId = null)
    {
        FriendHelper::unfriendPeople($userId, $friendId);

        // send denied Notification to the Friend User
        $notification = new Notification();
        $notification->myuser_id = $userId;
        $notification->user_id = $friendId;
        $notification->message = ' denied your friend request';
        $notification->read_notification  = 0;
        $notification->status = 'confirmation';
        $notification->notification_type = 'friends';
        $notification->created = DateHelper::getCurrentSQLDateTime();
        $notification->modified = DateHelper::getCurrentSQLDateTime();
        $notification->save(false);
    }

    public static function undoRejectFriendRequest($notificationId)
    {
        $notification = Notification::model()->findByPk($notificationId);
        if ($notification)
        {
            $userId = $notification->user_id;
            $friendId = $notification->myuser_id;

            FriendHelper::unfriendPeople($userId, $friendId);

            $notification = Notification::model()->findByPk($notificationId);
            $notification->read_notification  = 0;
            $notification->status = 'waiting';
            $notification->modified = DateHelper::getCurrentSQLDateTime();
            $notification->save(false);
        }
    }

    public static function acceptFriendRequest($userId, $friendId, $notificationId = null)
    {
        // create new friends if not exists
        $friend = Friend::model()->findByAttributes(array(
                        'user_id' => $userId,
                        'friend_id' => $friendId
        ));

        if (!$friend)
        {
            $friend = new Friend();
            $friend->user_id = $userId;
            $friend->friend_id = $friendId;
            $friend->created = DateHelper::getCurrentSQLDateTime();
            $friend->save(false);
        }

        $friend = Friend::model()->findByAttributes(array(
                        'user_id' => $friendId,
                        'friend_id' => $userId
        ));

        if (!$friend)
        {
            $friend = new Friend();
            $friend->user_id = $friendId;
            $friend->friend_id = $userId;
            $friend->created = DateHelper::getCurrentSQLDateTime();
            $friend->save(false);
        }

        // change friend request notification
        $invite = FriendHelper::getInvite($userId, $friendId, $notificationId);
        if ($invite)
        {
            $invite->status = 'accept';
            $_SESSION["_fh_lastInviteMessage"] = $invite->message;
            $invite->message = 'You are friends with ';
            $invite->read_notification  = 0;
            $invite->save(false);
        }

        // send confirmation Notification to the Friend User
        $notification = new Notification();
        $notification->myuser_id = $userId;
        $notification->user_id = $friendId;
        $notification->message = ' accepted your friend request';
        $notification->read_notification  = 0;
        $notification->status = 'confirmation';
        $notification->notification_type = 'friends';
        $notification->created = DateHelper::getCurrentSQLDateTime();
        $notification->modified = DateHelper::getCurrentSQLDateTime();
        $notification->save(false);
    }

    public static function undoAcceptFriendRequest($notificationId)
    {
        $notification = Notification::model()->findByPk($notificationId);
        if ($notification)
        {
            $userId = $notification->user_id;
            $friendId = $notification->myuser_id;

            FriendHelper::unfriendPeople($userId, $friendId);

            $notification = Notification::model()->findByPk($notificationId);
            if (isset($_SESSION["_fh_lastInviteMessage"]))
            {
                $notification->message = $_SESSION["_fh_lastInviteMessage"];
            }
            $notification->read_notification  = 0;
            $notification->status = 'waiting';
            $notification->modified = DateHelper::getCurrentSQLDateTime();
            $notification->save(false);
        }
    }

    public static function unfriendPeople($userId, $friendId)
    {
        $friends = Friend::model()->findall(array(
                        'condition' => '(user_id = :x AND friend_id = :y) OR (user_id = :y AND friend_id = :x)',
                        'params' => array(':x' => $userId, ':y' => $friendId)));

        // delete friend entries
        foreach ($friends as $friend)
        {
            $friend->delete();
        }

        $notifications = Notification::model()->findall(array(
                        'condition' => "((myuser_id = :y AND user_id = :x) OR (myuser_id = :x AND user_id = :y)) AND notification_type = 'friends'",
                        'params' => array(':x' => $userId, ':y' => $friendId)
        ));

        // delete friend notifications
        foreach ($notifications as $notification)
        {
            $notification->status = "deleted";
            $notification->save(false);
        }
    }
    
    public static function unfollowCompany($userId, $friendId)
    {
    	$friends = Friend::model()->findall(array(
    			'condition' => 'user_id = :x AND friend_id = :y',
    			'params' => array(':x' => $userId, ':y' => $friendId)));
    
    	// delete follow entries
    	foreach ($friends as $friend)
    	{
    		$friend->delete();
    	}
    }

    public static function getFriendsByUserId($userid, $type, $offset = null, $limit = null)
    {
        $queryData = array(
                        'params' => array(':x' => $userid),
                        'order' => 'friendUser.firstname ASC, friendUser.lastname ASC',
                        'distinct' => true,
        );
        
        if ($offset || $limit)
        {
            $queryData["offset"] = $offset;
            $queryData["limit"] = $limit;
        }
        
        $user = User::model()->findByAttributes(array('id' => $userid));
        $compMember = '';
        if ($user->member == 'company user' && $type == 'member') {
            $queryData['condition'] = "friend_id = :x";
            
            return Friend::model()->with(array(
                        'friendUser' => array(
                                        'joinType' => 'LEFT JOIN',
                                        'distinct' => true
                        )))->findall($queryData); 
        } else {
            $queryData['condition'] = "user_id = :x";
            
            return Friend::model()->with(array(
                        'friendUser' => array(
                                        'joinType' => 'LEFT JOIN',
                                        'condition' => "friendUser.member = '" . $type . "'",
                                        'distinct' => true
                        )))->findall($queryData); 
        }   
    }

    public static function getFriendsIdsByUserId($userid, $type, $offset = null, $limit = null)
    {
        $friends = FriendHelper::getFriendsByUserId($userid, $type, $offset, $limit);
        $friendsIds = array();
        foreach($friends as $friend) {
            $friendsIds[] = $friend->friend_id;
        }
        return $friendsIds;
    }

    public static function findMyFriends($type = "member", $offset = null, $limit = null)
    {
        $userId = Yii::app()->user->getID();
        return FriendHelper::getFriendsByUserId($userId, $type, $offset, $limit);
    }

    public static function findMyFollowees()
    {
        $userId = Yii::app()->user->getID();

        return Friend::model()->with("friendUser")->findall(array(
                        'condition' => "user_id = :x AND friendUser.member = 'company user'",
                        'params' => array(':x' => $userId),
                        'order' => 't.id ASC',
                        'distinct' => true));
    }

    public static function getMyFolloweesIds()
    {
        $userId = Yii::app()->user->getID();
        $res = array();

        foreach (Friend::model()->with("friendUser")->findall(array(
                        'select' => 'friend_id',
                        'condition' => "user_id = :x AND friendUser.member = 'company user'",
                        'params' => array(':x' => $userId),
                        'order' => 't.id ASC',
                        'distinct' => true)) as $followees)
        {
            $res[] = $followees->friend_id;
        }
        return $res;
    }

    public static function findMyFollowers()
    {
        $userId = Yii::app()->user->getID();

        return Friend::model()->findall(array(
                        'condition' => "friend_id = :x",
                        'params' => array(':x' => $userId),
                        'order' => 't.id ASC',
                        'distinct' => true));
    }

    public static function getMyFollowersIds()
    {
        $userId = Yii::app()->user->getID();
        $res = array();

        foreach (Friend::model()->findall(array(
                        'select' => 'user_id',
                        'condition' => "friend_id = :x",
                        'params' => array(':x' => $userId),
                        'order' => 't.id ASC',
                        'distinct' => true)) as $follower)
        {
            $res[] = $follower->user_id;
        }
        return $res;
    }
     
    public static function getMyFriendsIds($reloadFromDb = false)
    {
        $friendsserch = array();
        if (!$reloadFromDb && isset($_SESSION['myfrinds']))
        {
            // get friends ids from session
            if ($_SESSION['myfrinds'] != '')
            {
                $friendsserch = explode("," , rtrim($_SESSION['myfrinds'], ','));
            }
        }
        else
        {
            // reload all friends from db
            $fsss = '';
            $myfriends = FriendHelper::findMyFriends();
            foreach ($myfriends as $friend)
            {
                $fsss .= $friend['friend_id'] . ',';
            }
            $_SESSION['myfrinds'] = $fsss;
            $friendsserch = FriendHelper::getMyFriendsIds();
        }
         
        return $friendsserch;
    }

    public static function getMyFriendsIdsStr()
    {
        if (!isset($_SESSION['myfrinds']))
        {
            FriendHelper::getFriendsIds(true);
        }

        return rtrim($_SESSION['myfrinds'], ',');
    }
}
