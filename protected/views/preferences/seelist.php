<?php $this->renderPartial('sidenavigation'); 
$baseurl = Yii::app()->request->baseUrl;
// Get client script
$cs=Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/colorbox.css');

// Add JS
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?> 
<div class="outerdiv1_auth">
    <?php  $listid = $_GET['listid']; ?>
    <div class="outer_see_list">        
        <div class="list_name">
            <div class="list_name_div">
                <span><?php echo $data['name'] ?></span>
            </div>
            <div class="list_image_div">
                <?php if(strpos($data['image'], 'profile/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else if(strpos($data['image'], 'freeworld/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else{ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/listdetail/'.$data['image']; ?>" />
                    <?php } ?>
            </div>
            
            <div style="float: left;">
            <div class="latest_title_cre">
                        <span>Created by:</span>
            </div>
            <div class="latest_title_div">
                        <span><?php echo $myuserinfo->firstname.' '.$myuserinfo->lastname; ?></span>
            </div>
            </div>
            <div class="list_image_div">
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/profile/<?php echo $myuserinfo['image'];?>" class="image_mylist"/>
            </div>
            
        </div>
       
        <div class="maintable">
            <div class="list_title_div">
                <table class="tablediv">
                    <tr>
                        <td class="titlediv">
                                <span><?php echo $data['title'] ; ?></span>
                        </td>
                        <?php foreach ($datacolumn as $column){ ?>
                        <td class="column_div">
                                    <span> <?php echo $column->value ;?> </span>
                        </td>
                         <?php }?>
                    </tr>
                    
                    <tr>
                    <?php  $temp = 1;
                      foreach ($datarow as $row){ ?>
                     
                        <td class="row_div">
                            <span> <?php echo $row->value ;?> </span>
                        </td>
                       <?php 
                        if($temp%$count==0)
                        {
                            echo "</tr><tr>";
                        }  
                        $temp++;
                       ?>
                      
                      <?php  } ?>
                   </tr>
               
                </table>
           </div>
        </div>
        <div style="clear: both;">&nbsp;</div>
        <a  onclick="editlist_popup('<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/editlist?listid=<?php echo $listid; ?>');" href="#">
        <div class="official_today_createnew_margine" >
            <span>Edit List</span>
        </div>
        </a>
        
        <!--<a  href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/deletelist?listid=<?php echo $listid;?>">
        <div class="official_today_createnew_margine" onclick="return deletelist();">
            <span>Delete List</span>
        </div>
        </a>-->
    </div>
    
    


<?php //if($breake[0]==Yii::app()->params['searchurl']) { ?>
<!--<div class="backsuggestion"  >
 <a class="textdecoration" href="<?php //echo $url ;?>"> <input class="image_buttons" type="image"   src="<?php //echo Yii::app()->request->baseUrl;?>/images/buttons/back.png"/></a>
 </div>
<?php //} else { ?>
<div class="backsuggestion"  >
 <a class="textdecoration" href="<?php //echo Yii::app()->request->baseUrl; ?>/index.php/preferences/mylist"> <input class="image_buttons" type="image"   src="<?php echo Yii::app()->request->baseUrl;?>/images/buttons/back.png"/></a>
 </div>--?
<?php// } ?>
</div>