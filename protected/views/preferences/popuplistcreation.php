<script type="text/javascript">
    jQuery(document).ready(function(){
    var width =  document.getElementById("titlepop").offsetHeight 
     document.getElementById("ratepop").style.height = width+"px";
 }); 
</script>
<div class="popup_outerdiv1">
 <?php $baseurl = Yii::app()->request->getBaseUrl(true);
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'popuplistsave',
        'action'=>$baseurl.'/index.php/preferences/popuplistcreation?listid='.$_GET['listid'],
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'listsave',            
        'onsubmit'=>'return popuplistsaves();',
     ),
      )); 
if(isset($data))
{
    $listid = $data->id;
    $listname = $data->name;
    $listimage = $data->image;
    $listtitle = $data->title;
    $created_by = $list_creator;
    $interest = $list_interest;
}
else
{
    $listid = $_GET['listid'];
    $listname = $_GET['name'];
    $listimage = $_GET['image'];
    $listtitle = $_GET['title'];
    $created_by = $_GET['creator'];
    $interest = $_GET['interest'];
}
?>
<?php if(isset($_GET['friends'])){ ?>
        <input type="hidden" name="friendsmail" value="<?php echo $_GET['friends']; ?>" />
<?php } ?>
        
        <input type="hidden" name="listid" id="listid" value="<?php echo $listid;?>"/>
        <input type="hidden" id="hidden_baseurl" value="<?php echo $baseurl ?>"/>
            <div>            
            <div class="popup_list_title_div1">               
                <div class="popup_row_namefild">
                    <span style="color: white;"><?php echo StringsHelper::capitalize($listtitle); ?></span>
                </div>
            </div> 
            <div class="popup_listimage_maindiv">
                <div class="popup_row_textfild">
                    <?php if(strpos($listimage, 'profile/') === 0){ ?>
                        <img class="popup_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $listimage ?>" />
                    <?php } else if(strpos($listimage, 'freeworld/') === 0){ ?>
                        <img class="popup_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $listimage;?>" />
                    <?php } else{ ?>
                        <img class="popup_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/listcreation/'.$listimage; ?>" />
                    <?php } ?>
                </div>
                
                <?php 
                $createdtime = $data->modified;
                $currenttime = time();
                $difference  = $currenttime - $createdtime; 
                $years       = abs(floor($difference / 31536000));
                $days        = intval($difference / 86400); 
                $difference  = $difference % 86400;
                $hours       = intval($difference / 3600); 
                $difference  = $difference % 3600;
                $minutes     = intval($difference / 60); 
                $difference  = $difference % 60;
                $seconds     = intval($difference);                 
                ?>
                
                
                <div class="popup_listimage_authorinfo_takelist">
                    <span class="takelist_newdis_span1">Author:</span>&nbsp;
                    <span style="text-decoration: underline;" class="cursorpointer" onclick="autherclick('<?php echo $baseurl;?>','<?php echo $data->user_id?>')"><?php echo $created_by;?></span>                                                                      
                    <br/> 
                    <?php if($profilestatus!= null) {?>
                    <span class="takelist_newdis_span1">Status:</span>&nbsp;<span><?php echo $profilestatus;?></span><br/>
                    <?php } ?>
                    <span class="takelist_newdis_span1">Category:</span>&nbsp;<span><?php echo $interest; ?></span><br/>
                    <span class="takelist_newdis_span1">Created:</span>&nbsp;
                          <?php if($years>1){?>
                            <span><?php echo $years;?> years ago</span> <br/>

                            <?php }elseif($days>1){?>
                            <span ><?php echo $days;?> days &nbsp;ago</span><br/>

                            <?php }elseif($hours>24){?>
                            <span ><?php echo $hours;?> hours &nbsp;ago</span>  <br/>

                            <?php } elseif($minutes>1) {?>
                             <span ><?php echo $minutes?> minutes &nbsp;ago</span><br/>    

                           <?php }elseif($seconds<60) {?>
                            <span ><?php echo $seconds?> sec &nbsp;ago</span><br/>

                            <?php } ?> 
                    <span style="float: left; padding-right: 5px;" class="takelist_newdis_span1">Votes:</span><span class="popup_votingspan_outer"><span class="popup_votingspan_inner">0</span></span>
                </div>
            </div>
            <div class="popup_listmaindiv">
                
                    <table id="myTable" style="width: 150px;border: 2px solid;">
                      <tr>
                        <td colspan="2">
                            <div class="titlete_textfild" id="titlepop">
                                <div class="takelist_title_newdis" style="padding-left: 8px;text-align: left;">
                                <span style="padding: 0px 6px;" ><?php echo StringsHelper::capitalize($listtitle); ?></span>
                                </div>
                             </div>
                         </td> 
                            <td>
                                <div class="titlete_textfild" id="ratepop">
                                        <div class="popuplist_collumn1_div1_creation">
                                              <input name="collumerating" type="text" size="21" class="popuplist_collumn1" value="Rating" onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"/> 
                                       </div>
                                </div>
                            </td>
                            <!--<input size="21" class="popup_table_input_background" type="text" name="01"/></div></td>-->
                            <!--<td><div class="popup_titlete_textfild"><input size="21" class="popup_table_input_background" type="text"  name="02"/></div></td>  -->
                         
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span1" >1.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                              <li class="popuplistcreation_ul_li"  >
                                  <input size="21"  class="popup_table_input" id="input10" type="text"   autocomplete="off" name="value[1]" onkeyup="nameautofil('<?php echo $baseurl;?>','10')"  value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"/>                                   
                                  <div class="listdiv" id="div10">
                                      
                                  </div>
                              </li>
                            </ul>
                        </td>
                        <td><input size="21"  class="popup_table_input" autocomplete="off" id="input11" type="text"  name="rating[1]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" onkeyup="maximumvaluecheck('11')"/></td> 
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="12"/></td>                         -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span2" >2.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text"  autocomplete="off"  value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input20" name="value[2]" onkeyup="nameautofil('<?php echo $baseurl;?>','20')"/>
                                <div class="listdiv" id="div20">
                                      
                                 </div>
                              </li>
                            </ul>
                        </td>
                        <td><input size="21"  class="popup_table_input" type="text"   autocomplete="off"   name="rating[2]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input21" onkeyup="maximumvaluecheck('21')"/></td> 
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="22"/></td> -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span3" >3.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text" autocomplete="off"  value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"  id="input30" name="value[3]" onkeyup="nameautofil('<?php echo $baseurl;?>','30')"/>
                                <div class="listdiv" id="div30">
                                      
                             </div>
                              </li>
                            </ul>
                        </td> 
                        <td><input size="21"  class="popup_table_input" type="text"   autocomplete="off"  name="rating[3]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input31" onkeyup="maximumvaluecheck('31')"/></td>    
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="32"/></td>--> 
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span4" >4.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text" autocomplete="off" value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"   id="input40" name="value[4]" onkeyup="nameautofil('<?php echo $baseurl;?>','40')"/>
                                <div class="listdiv" id="div40">
                                      
                                  </div>
                              </li>
                            </ul>
                         </td>
                        <td><input size="21"  class="popup_table_input" type="text"   autocomplete="off"  name="rating[4]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input41" onkeyup="maximumvaluecheck('41')"/></td> 
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="42"/></td> -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span5" >5.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text" autocomplete="off" value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"  id="input50" name="value[5]" onkeyup="nameautofil('<?php echo $baseurl;?>','50')"/>
                                <div class="listdiv" id="div50">
                                      
                                  </div>
                              </li>
                            </ul>
                         </td> 
                        <td><input size="21"  class="popup_table_input" type="text"  autocomplete="off" name="rating[5]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input51" onkeyup="maximumvaluecheck('51')" /></td>
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="52"/></td> -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span6" >6.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text" autocomplete="off"  value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"  id="input60" name="value[6]" onkeyup="nameautofil('<?php echo $baseurl;?>','60')"/>
                                <div class="listdiv" id="div60">
                                      
                                 </div>
                              </li>
                            </ul>
                        </td>
                        <td><input size="21"  class="popup_table_input" type="text"   autocomplete="off" name="rating[6]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input61" onkeyup="maximumvaluecheck('61')" /></td>  
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="62"/></td> -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span7" >7.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text" autocomplete="off" value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"    id="input70" name="value[7]" onkeyup="nameautofil('<?php echo $baseurl;?>','70')"/>
                                <div class="listdiv" id="div70">
                                      
                                 </div>
                              </li>
                            </ul>
                        </td>
                        <td><input size="21"  class="popup_table_input" type="text"  autocomplete="off"  name="rating[7]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input71" onkeyup="maximumvaluecheck('71')" /></td>  
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="72"/></td> -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span8" >8.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text" autocomplete="off" value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"   id="input80" name="value[8]" onkeyup="nameautofil('<?php echo $baseurl;?>','80')"/>
                                <div class="listdiv" id="div80">
                                      
                                 </div>
                              </li>
                            </ul>
                        </td>
                        <td><input size="21"  class="popup_table_input" type="text"  autocomplete="off"  name="rating[8]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input81" onkeyup="maximumvaluecheck('81')"/></td> 
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="82"/></td> -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span9" >9.</span></div></div></td>
                        <td>
                            <ul class="popuplistcreation_ul" >
                                <li class="popuplistcreation_ul_li"  >
                                <input size="21"  class="popup_table_input" type="text" autocomplete="off" value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"  id="input90" name="value[9]" onkeyup="nameautofil('<?php echo $baseurl;?>','90')"/>
                                <div class="listdiv" id="div90">
                                      
                                 </div>
                              </li>
                            </ul>
                            
                        </td> 
                        <td><input size="21"  class="popup_table_input" type="text"  autocomplete="off"  name="rating[9]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input91" onkeyup="maximumvaluecheck('91')"/></td>
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="92"/></td> -->
                      </tr>
                      <tr>
                        <td><div class="listcreation_spancount"><div class="div_span_padding"><span id="span10" >10.</span></div></div></td>
                        <td>
                           <ul class="popuplistcreation_ul" >
                               <li class="popuplistcreation_ul_li"  >
                                    <input size="21"  class="popup_table_input" type="text" autocomplete="off" value="Enter Selection." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"  id="input100" name="value[10]" onkeyup="nameautofil('<?php echo $baseurl;?>','100')"/>
                                 <div class="listdiv" id="div100">
                                      
                                  </div>
                              </li>
                            </ul>
                        
                        </td>
                        <td><input size="21"  class="popup_table_input" type="text"  autocomplete="off"  name="rating[10]" value="Enter 1-10 rating(10 highest)." onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" id="input101" onkeyup="maximumvaluecheck('101')"/></td> 
                        <!--<td><input size="21"  class="popup_table_input" type="text"  name="102"/></td>  -->
                      </tr> 
                    </table>  
                
                <div id="testing" class="popup_addrowdiv"> 
                    <span class="popup_addspan1">Add Row</span>
                    <span id="addrow" class="popup_addspan2" onclick="addrowagain()">
                        <img style="height:14px;" src="<?php echo Yii::app()->request->baseUrl;?>/images/buttons/arrow-.png" />
                    </span>
                    <span class="popup_separator">|</span>
                    <span id="removerow" class="popup_removetd" style="float: left" onclick="removerowagain()">
                        Remove Row
                    </span>
                                        
                    <input type="hidden" name="hid" value="10" id="hid"/>
                 </div> 
                
                <!--<div id="addcolm" class="popup_addcoldiv"> 
                    <span class="popup_addspan1">Add Column</span>
                    <span id="addcol" class="popup_addspan2" style="cursor: pointer; padding-right: 15px; float: left;" onclick="addcolagain()">
                        <img style="height:14px;" src="<?php //echo Yii::app()->request->baseUrl;?>/images/buttons/arrow-.png" />
                    </span>
                    <span id="removecol" class="popup_removetd" style="display:none;" onclick="removecolagain()">
                        <span class="popup_separator">|</span>
                        Remove Column
                    </span>
                    
                    
                 </div>--> 
                <input type="hidden" name="hidcol" value="1" id="hidcol"/>
                 <div style="float: left; width: 100%; color: #c8c8c8; font-family: Geosanslight; font-weight: bold;font-size: 14px;"> 
                      <input type="checkbox" id="list_fav_check" name="list_favorite" value="1" style="float:left; margin-top:1px;" />&nbsp;
                      Add to Favorites
                 </div>
            </div>
           <div class="popup_submitdiv">
            <input class="image_buttons" type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/buttons/create_btn.png" style="float: left;"/>
            <img src="<?php echo $baseurl;?>/images/loading.gif" style="float: left;height: 25px; margin-top: 7px;display: none;" id="lodinggif"/>
           </div> 
            
        
         <input type="hidden" name="submit"/>
<?php $this->endWidget(); ?>     
</div>