<script type="text/javascript">
    jQuery(document).ready(function(){
    var width =  document.getElementById("mydislist").offsetWidth    
   $(".listdetail_titlete_textfild_first").width(width);   

 }); 
</script>

<?php //$this->renderPartial('sidenavigation');
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid     = Yii::app()->user->getID();
// Get client script
$cs=Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/colorbox.css');

// Add JS
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>
<div class="featured_list_section_internal">
<div class="list_detail_outerdiv" style="padding-top: 10px;">
<div class="popup_outerdiv1">
 <?php
/*$width1 = '162'*($count);
if($count == 1)
{ 
    $width2 = '0';
} 
else
{ 
    $width2 = '100';
} */

if(isset($data))
{
    $listid = $data->id;
    $listname = $data->name;
    $listimage = $data->image;
    $listtitle = $data->title;
    $created_by = $list_creator;
    $interest = $list_interest;

?>
        <input type="hidden" name="listid" value="<?php echo $listid;?>"
        <input type="hidden" id="hidden_baseurl" value="<?php echo $baseurl ?>"/>
            <div style="width: 100%;" class="popup_list_title_div1">               
                <div class="popup_row_namefild_listdetail">
                    <span style="color: #333232;"><?php echo StringsHelper::capitalize($listtitle); ?></span>
                </div>
            </div>
        
        <div class="listdetail_flashmsg">
            <?php foreach(Yii::app()->user->getFlashes() as $key => $message) {                                                        
                                echo '<div class="flash' . $key . '">' . $message . "</div>\n";                            
                        }
                    ?>
           </div>
        
            <div style="width: 600px;" class="popup_listimage_maindiv">
                <div class="list_detail_row_textfild" style="margin-left: 50px;" >
                    <?php if(strpos($listimage, 'profile/') === 0){ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $listimage ?>" />
                    <?php } else if(strpos($listimage, 'freeworld/') === 0){ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $listimage ?>" />
                    <?php } else{ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/listdetail/'.$listimage; ?>" />
                    <?php } ?>
                </div>
                
                <?php 
                $createdtime = $data->modified;
                $currenttime = time();
                $difference  = $currenttime - $createdtime; 
                $years       = abs(floor($difference / 31536000));
                $days        = intval($difference / 86400); 
                $difference  = $difference % 86400;
                $hours       = intval($difference / 3600); 
                $difference  = $difference % 3600;
                $minutes     = intval($difference / 60); 
                $difference  = $difference % 60;
                $seconds     = intval($difference);                 
                ?>
                
                <div class="list_detail_listimage_authorinfo" style="width: 400px;">
                    <span class="list_detail_author_attr">Author:</span>&nbsp;
                    <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/user/dashboard?userid=<?php echo $data->user_id;?>">
                    <span style="text-decoration: underline;width: 300px;" class="list_detail_author_val"><?php echo $created_by; ?></span>
                    </a>
                    <br/>
                    <?php if($profilestatus!= null) {?>
                    <span class="list_detail_author_attr" style="padding-right: 5px;">Status:</span>&nbsp;<span style="width: 300px; float: left;"><?php if($profilestatus!= null){ echo $profilestatus;} else { echo 'No status updated' ; }?></span><br/>
                    <?php } ?>
                    <span class="list_detail_author_attr">Category:</span>&nbsp;<?php if(empty($interest)){ echo "<br/>";}?><span style="width: 300px;" class="list_detail_author_val"><?php echo $interest; ?></span>   <br/>                 
                    <span class="list_detail_author_attr" style="width: 72px;">Created:</span>&nbsp;
                          <?php if($years>1){?>
                            <span><?php echo $years;?> years ago</span> <br/>

                            <?php }elseif($days>1){?>
                            <span ><?php echo $days;?> days &nbsp;ago</span><br/>

                            <?php }elseif($hours>24){?>
                            <span ><?php echo $hours;?> hours &nbsp;ago</span>  <br/>

                            <?php } elseif($minutes>1) {?>
                             <span ><?php echo $minutes?> minutes &nbsp;ago</span><br/>    

                           <?php }elseif($seconds<60) {?>
                            <span ><?php echo $seconds?> sec &nbsp;ago</span><br/>

                            <?php } ?> 
                    <span class="list_detail_author_attr">Votes:</span>
                    <div style="font-size: 11px;" class="popup_votingspan_outer">
                        <div style="padding: 4px 0 0;" class="popup_votingspan_inner"><?php echo count($vote);?></div>
                    </div>
                </div>
                
                <?php if($data['instruction']!='' && $data['instruction']!='Enter List Instruction') { ?>
                    <div class="instruction_listdetail">
                        <span><?php echo $data['instruction'];?></span>
                    </div>
               <?php }?>
                
                <?php if($data['user_id']==$userid || !empty($mytakenlist)) { ?>
                <?php /*if(empty($myvote)) {?>
                    <div class="listdetail_vote">            
                        <div class="floatleft" style="margin-top: 15px;">
                            <a href="<?php echo $baseurl;?>/index.php/preferences/listvote/listid/<?php echo $_GET['listid']?>">
                            <div class="official_today_createnew_margine" style="padding-top:6px;margin-left: 0px;margin-bottom: 0;">
                            <span>Vote</span>
                            </div>
                            </a>
                        </div>            
                    </div>
                  <?php }*/?>
                
                      <div style="margin-top: 20px; width:100%;margin-left: 0px;" class="popup_submitdiv" id="editdiv">
                            <a onclick="editlist_popup('<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/editlist?listid=<?php echo $listid;?>&user_id=<?php echo $userid;?>');" href="#">
                                <div class="official_today_createnew_margine" style="padding-top:6px;margin-left: 60px;" >
                                    <span style="font-size: 22px;">Edit List</span>
                                </div>
                            </a>

                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/deletelist?listid=<?php echo $listid;?>&user_id=<?php echo $userid;?>">
                                <div class="official_today_createnew_margine" style="padding-top:6px;margin-left: 60px;" onclick="return deletelist();">
                                    <span style="font-size: 22px;">Delete List</span>
                                </div>
                            </a>

                       </div>
                
                <?php } ?>
                
            </div>
        
            <div style="min-width: 400px;margin-left: 0px;" class="popup_listmaindiv">
                <div style="float: left;">
                             <div class="listdetail_titlete_disign" id="mydislist">
                                <?php $length = strlen($listtitle) ;
                                       if($length>25)
                                        {
                                          $title = substr($listtitle,0,25);  
                                 ?>
                                <span style="float: left; padding: 4px 7px 0 2px;color: white;" ><?php echo StringsHelper::capitalize($title); ?>..</span>
                                <?php } else { ?>
                                <span style="float: left; padding: 4px 7px 0 2px;color: white;" ><?php echo StringsHelper::capitalize($listtitle); ?></span>
                                <?php } ?>
                             </div>
                                <div class="listdetail_titlete_textfild_rate" style="background: #789bda; padding-left: 3px;text-align: left;">
                                    <div style="padding-top: 4px;">
                                    <span style="color: white;">  <?php if($collumnnm['value']!='') {echo $collumnnm['value'];} else{ echo 'Rating';}?></span>              
                                    </div>
                                </div>
                                <div class="floatleft cursorpointer" style="margin-top: -2px; margin-left: 8px;">
                                 <img id="downarrow" onclick="changearrowlistdetail('downarrow')" src="<?php echo $baseurl;?>/images/freeworld/down-arrow.png"/>
                                </div>
                </div>
                
            <div style="width: 370px;">
                
                    <table id="myTable" style="width: 150px;float: left; ">                     
                    <tr>
                   <?php $i=0; 
                      for($val=0;$val<count($avgrating);$val++) { 
                         if($data['user_id']!=$userid && empty($mytakenlist))
                         { 
                             if($i > 2)
                             { 
                                 break;
                             }
                         }
                          
                    ?>
                     
                        <td style="background-color: white;">
                            <div class="listdetail_titlete_textfild_first">
                                 <span style="float: left; padding: 4px 0 0 2px;" ><?php echo $avgrating[$val]['value'] ;?></span>
                            </div>
                        </td>
                        <td style="background-color: white;">
                            <div class="listdetail_titlete_textfild">
                                 <span style="float: left; padding: 4px 0 0 2px;" ><?php echo rtrim(rtrim(number_format($avgrating[$val]['avrg'],1),"0"),".") ;?></span>
                            </div>
                        </td>
                       <?php
                        echo "</tr><tr>";
                       ?>
                      
                      <?php  $i++; } ?>
                     </tr>
                                        
                    </table>  
                </div>
                
                 <?php if($data['user_id']!=$userid && empty($mytakenlist)) { ?>
                
                <div class="seemore_listdetail">
                    <a class="textdecoration" href="#" onclick="takelist_popup('<?php echo $baseurl;?>/index.php/preferences/takelist?listid=<?php echo $listid;?>');">
                    <span> Take the list</span>
                    </a>
                </div>
                <?php } ?>
                
            </div>
        
       
<?php
}
else
{ ?>
   
          <div style="width: 100%; padding: 80px 0 0 230px;" class="popup_list_title_div1">               
                <div class="popup_row_namefild">
                    <span style="color: grey; font-size: 32px;">No List created</span>
                </div>
          </div> 
                
<?php                
}
?>
</div>
</div>
    </div>