<?php
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid  = Yii::app()->user->getID();
$globelurl = Yii::app()->params['globelUrl'];
?>


<script>
$(document).ready(function(){
  $("#searchfriend").keyup(function(){
    var c    = document.getElementById("searchfriend").value; 
    var frnd = document.getElementById("strfrnd").value; 
    if(trim(c).length>=0){     
    var url = '<?php echo $baseurl;?>'+"/index.php/preferences/searchfriend";
    document.getElementById('lodingforfrind').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        //dataType:'json',
        data:{
                title:c,   
                frnd:frnd
             },
             success:function(value){ 
                 document.getElementById('lodingforfrind').style.display = "none";
                 if(value!=0)
                     {
                         document.getElementById('friendser').style.display = "block";
                         document.getElementById('nofrnd').style.display = "none";
                         $('#friendser').html('').append(value)
                     }
                     else
                        {
                            document.getElementById('friendser').style.display = "none";
                            document.getElementById('nofrnd').style.display = "block";
                        }
             
    }
    }); 
   }
  });
});
</script>

<div>
    
    <div class="invitefriends_h2">&nbsp;</div>
    
 <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$globelurl.'/preferences/invitefriends?listid='.$_GET['listid'],
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',
        'onsubmit'=>'return invitefriends();',
        //'validateOnSubmit'=>true,
     ),
      ));
	  $friendsserch ='';
?>
               <div class="invitefriends_newdesdiv11">
                   <span>
                       Search Friends
                   </span>
               </div>
              <div class="listtitle_newdesdiv13">
                   <div class="floatleft">
                       <input id="searchfriend" type="text" name="search_tag" value="Search friends"  onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" class="invitefriends_newdesdiv13_input"/>
                   </div>
                       <div class="listtitle_newdesdiv13_imgdiv">
                           <img class="cursorpointer" style="height: 30px;" src="<?php echo $baseurl;?>/images/freeworld/arrow-1.png"/>
                        </div>
                       <div id="lodingforfrind"  style="margin-top: 6px; margin-left: -38px; float: left;display: none;">
                       <img src="<?php echo $baseurl;?>/images/ajax-loader.gif" />
                       </div>
               </div>
    
  <div class="invitefriends_div1">
   <div class="invitefriends_overflow_div" id="content_1">
    <div id="friendser" class="widthauto">
                   
                  <?php $friendsserch ='';
                     if(count($registered_users)>0){                      
                        for($i=0; $i<count($registered_users); $i++){ 
                        $friend_id = $registered_users[$i]['friend_id'];
                        $uiser_id  = $registered_users[$i]['user_id'];
                           if($friend_id!=$userid)
                           {
                               $friendsserch.= $friend_id .',';
                               $user = User::model()->find("id='$friend_id'");
                   ?>
                   
                       <div class="invitefriends_newdesdiv221">
                           <div class="invitefriends_newdesdivcheckbox">
                               <input id="friends_<?php echo $friend_id; ?>" type="checkbox" name="friends[]" value="<?php echo $friend_id; ?>" />
                           </div>
                           <div class="invitefriends_newdesdiv22img">
                              <?php if($user['image']!=''){?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/dashboard/<?php echo $user['image'];?>"/>
                               <?php } else { 
                                  if($user['gender']=='male' || $user['gender']=='Male')
                                     {
                                ?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($user['gender']=='female' || $user['gender']=='Female') {?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                           </div>                              
                           <div class="invitefriends_newdesdiv22name"> 
                           <span><?php echo $user['firstname'].' '.$user['lastname']; ?></span>
                           </div>
                       </div>
                   <?php } else {  $user = User::model()->find("id='$uiser_id'"); $friendsserch.= $uiser_id .',';?>
                   
                      <div class="listtitle_newdesdiv221">
                           <div class="invitefriends_newdesdivcheckbox">
                               <input id="friends_<?php echo $uiser_id; ?>" type="checkbox" name="friends[]" value="<?php echo $uiser_id; ?>" />
                           </div>
                           <div class="invitefriends_newdesdiv22img">
                              <?php if($user['image']!=''){?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/dashboard/<?php echo $user['image'];?>"/>
                               <?php } else { 
                                  if($user['gender']=='male' || $user['gender']=='Male')
                                     {
                                ?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($user['gender']=='female' || $user['gender']=='Female') {?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="auth_header_user_img_invite" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                           </div>                              
                           <div class="invitefriends_newdesdiv22name"> 
                           <span><?php echo $user['firstname'].' '.$user['lastname']; ?></span>
                           </div>
                       </div>
                       
                   <?php } } } else { ?>   
                   <span class="listtitle_newdesdiv22name">No friends</span>
                  <?php }?>
               </div>
       
                <div style="display: none;" id="nofrnd" class="listtitle_newdesdiv21">
                     <span class="listtitle_newdesdiv22name"> 
                       No friends
                      </span>
                     <?php $strfrnd = substr($friendsserch,0,strlen($friendsserch)-1); ?>
                     <input type="hidden" value="<?php echo $strfrnd;?>" id="strfrnd" />
                 </div>
       </div>
      <div class="invitefriends_text_para">
                   <p class="invitefriends_imgpara3">Don’t see your friend on the list? Enter email addresses separated by commas in the field below to send invites to your friends!</p>
               </div>
               <div class="are">
                   <textarea id="instruction2" rows="6" cols="50" name="instructin" onfocus="hidetextemail(this.value,this.id);" onblur="hidetextemail(this.value,this.id);" class="invitefriends_are" style="color: black;width: 565px;">Enter email addresses</textarea>
               </div>
           
      </div>
    <input type="hidden" name="submit"/>
    <div class="send_invitationbtn">
    <input id="submit" type="image" src="<?php echo $baseurl;?>/images/buttons/send-invitation.png"/>
    </div>
    <?php $this->endWidget(); ?>
</div>