<?php $this->renderPartial('sidenavigation'); 
$baseurl = Yii::app()->request->baseUrl;
// Get client script
$cs=Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/colorbox.css');

// Add JS
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>
<div class="outerdiv1_auth">
<div id='fb-root'></div>
<script src='http://connect.facebook.net/en_US/all.js'></script>
<script> 
                      FB.init({appId: "<?php echo $appid;?>", status: true, cookie: true});

                      function postToFeed(title,siteurl,pic,value,link) {  
                          var image=siteurl+'/images/'+pic; 
                          image = 'http://fbrell.com/f8.jpg';
                          //alert(link);
                          var obj = {
                          method: 'feed',
                          link: link,
                          picture: "http://fbrell.com/f8.jpg",  //'http://fbrell.com/f8.jpg', //'http://localhost/latestyii/newadminproject/images/1348225437Wallpapers-of-Nature.jpg',  //"'"+image+"'",
                          name: title,
                          caption: 'wayoworld.com',
                          description: value
                        };

                        function callback(response) {
                          document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
                        }

                        FB.ui(obj,callback); 
                      }

</script>

<?php   $userid = Yii::app()->user->getID();
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/preferences/search',
        'method'=>'GET',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'searchform',   
        'onsubmit'=>'return searchresult();',
        'validateOnSubmit'=>true,
     ),
      )); 
?>
                <div class="search_list">
                    <div class="search_list_div1">
                        <input id="searchres" type="text" name="search"  class="forgotpasinput"/>
                    </div>
                    <div class="search_list_div2" style="margin-top: -4px;">
                        <input class="image_buttons" type="image"   src="<?php echo $baseurl;?>/images/buttons/search-list.png"/>
                    </div>  
                    <div id="requred" class="search_list_div3" >
                        <span >Please fill first.</span>
                    </div>
                </div>

<?php $this->endWidget(); ?>


<div class="my_listdiv">
    <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/listtitle"><span> Create List</span></a>
</div>
<div class="mainouterdiv">
    <div class="outerdiv11" style="width: 380px;">
        <h2>Latest Lists</h2>
         <?php             
               if(!empty($takenlist))
               {         
                   $coun = 0;
                   foreach ($data as $value){ 
                      if($value->listtype =='public'){
                        $take = ListTaken::model()->find("user_id = $userid AND list_id = $value->id"); 
                         if($take)
                            {
                                continue;
                            }  
                        $coun++;                    
          ?>
        
          <div class="outer_see_list">
            <div class="img_outer">
            <div class="latest_image">
                <img class="image_mylist"  src="<?php echo $baseurl;?>/images/<?php echo $value->image;?>"/>
            </div>
                <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/takelist?listid=<?php echo $value->id;?>"> <span>Take this list</span></a>
            </div>
            
            <div class="outer_latesttitle_div" style="width: 225px;">
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>Title:</span>
                    </div>
                    <div class="latest_title_div">
                        <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/list_detail?listid=<?php echo $value->id;?>&userid=<?php echo $value->user_id;?>"><span><?php echo $value->title; ?></span></a>
                    </div>
                </div>
                
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>Author:</span>
                    </div>
                    <div class="latest_title_div">
                        <?php $user = User::model()->find("id = '$value->user_id'");?>
                        <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/user/userprofile?userid=<?php echo $user->id;?>">
                            <span><?php echo $user->firstname.' '.$user->lastname;?></span>
                        </a>
                    </div>
                </div>
                
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>No. of Takers:</span>
                    </div>
                    <?php $countpeople = ListTaken::model()->findAll("list_id = '$value->id'"); 
                    $count = count($countpeople);
                    ?>
                    <div class="latest_title_div">
                        <span><?php echo $count; ?></span>
                    </div>
                </div>
                
                 <?php 
                $createdtime = $value->modified;
                $currenttime = time();
                $difference  = $currenttime - $createdtime; 
                $years       = abs(floor($difference / 31536000));
                $days        = intval($difference / 86400); 
                $difference  = $difference % 86400;
                $hours       = intval($difference / 3600); 
                $difference  = $difference % 3600;
                $minutes     = intval($difference / 60); 
                $difference  = $difference % 60;
                $seconds     = intval($difference);                 
                ?>
                
                
                <div class="outer_latest_title" style="padding-top: 10px;">
                                 
                    <div class="latest_title_div">
                        <?php if($years>1){?>
                            <span class="notification_time"><?php echo $years;?> years ago</span> 

                            <?php }elseif($days>1){?>
                            <span class="notification_time"><?php echo $days;?>days &nbsp;ago</span> 

                            <?php }elseif($hours>24){?>
                            <span class="notification_time"><?php echo $hours;?>hours &nbsp;ago</span>  

                            <?php } elseif($minutes>1) {?>
                             <span class="notification_time" ><?php echo $minutes?>minutes &nbsp;ago</span>    

                           <?php }elseif($seconds<60) {?>
                            <span class="notification_time" ><?php echo $seconds?>sec &nbsp;ago</span>

                            <?php } ?> 
                    </div>
                </div>
                
            </div>
        </div>
               <?php } } ?> 
    
          <?php } else   {
                 //$data = ListForm::model()->findAll(array('order'=>'id DESC', 'condition'=>'user_id !=:x', 'params'=>array(':x'=>$userid)));
                         
                         //("user_id != '$userid'");
              $coun = 0;
                 foreach ($data as $value){ 
                     if($value->listtype =='public'){
                     $coun++;
               ?>
        <div class="outer_see_list">
            <div class="img_outer">
            <div class="latest_image">
                <img class="image_mylist" src="<?php echo $baseurl;?>/images/<?php echo $value->image;?>"/>
            </div>
                <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/takelist?listid=<?php echo $value->id;?>"> <span>Take this list</span></a>
            </div>
            
            <div class="outer_latesttitle_div" style="width: 225px;">
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>Title:</span>
                    </div>
                    <div class="latest_title_div">
                        <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/list_detail?listid=<?php echo $value->id;?>&userid=<?php echo $value->user_id;?>"><span><?php echo $value->title; ?></span></a>
                    </div>
                </div>
                
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>Author:</span>
                    </div>
                    <div class="latest_title_div">
                        <?php $user = User::model()->find("id = '$value->user_id'");?>
                        <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/user/userprofile?userid=<?php echo $user->id;?>"><span><?php echo $user->firstname.' '.$user->lastname;?></span></a>
                    </div>
                </div>
                
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>No. of Takers:</span>
                    </div>
                    <?php $countpeople = ListTaken::model()->findAll("list_id = '$value->id'"); 
                    $count = count($countpeople); ?>
                    <div class="latest_title_div">
                        <span><?php echo $count; ?></span>
                    </div>
                </div>
                
                <?php 
                $createdtime = $value->modified;
                $currenttime = time();
                $difference  = $currenttime - $createdtime; 
                $years       = abs(floor($difference / 31536000));
                $days        = intval($difference / 86400); 
                $difference  = $difference % 86400;
                $hours       = intval($difference / 3600); 
                $difference  = $difference % 3600;
                $minutes     = intval($difference / 60); 
                $difference  = $difference % 60;
                $seconds     = intval($difference);                 
                ?>
                
                
                <div class="outer_latest_title" style="padding-top: 10px;">
                                 
                    <div class="latest_title_div">
                        <?php if($years>1){?>
                            <span class="notification_time"><?php echo $years;?> years ago</span> 

                            <?php }elseif($days>1){?>
                            <span class="notification_time"><?php echo $days;?>days &nbsp;ago</span> 

                            <?php }elseif($hours>24){?>
                            <span class="notification_time"><?php echo $hours;?>hours &nbsp;ago</span>  

                            <?php } elseif($minutes>1) {?>
                             <span class="notification_time" ><?php echo $minutes?>minutes &nbsp;ago</span>    

                           <?php }elseif($seconds<60) {?>
                            <span class="notification_time" ><?php echo $seconds?>sec &nbsp;ago</span>

                            <?php } ?> 
                    </div>
                </div>
                
            </div>
        </div>
                      
        <?php } } } if($coun>6) {?>
        <div class="floatleft">
            <a class="textdecoration" href="<?php echo $baseurl?>/index.php/preferences/latestlists"><span>View more</span></a>
        </div>
        <?php }?>
    </div>
    
    <div class="outerdiv11" style="width: 380px;">
        <h2>My Created Lists</h2>
        <?php  if($countmy>0)
                {
                foreach ($mydata as $my){
                   
                $mytakenlist = ListTaken::model()->findAll("list_id = '$my->id'");
                $mycount = count($mytakenlist);
            ?>
        <div class="outer_see_list">
            <div class="img_outer">
            <div class="latest_image">
                <img class="image_mylist"  src="<?php echo $baseurl;?>/images/<?php echo $my->image;?>"/>
            </div>
                <?php if($mycount>0){ ?>
                <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/viewtotallistsecond?listid=<?php echo $my->id;?>"> <span>View Details</span></a>
                <?php  } else { ?>
                <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/seelisttake?listid=<?php echo $my->id;?>&url=<?php echo Yii::app()->request->urlReferrer;?>"> <span>View Details</span></a>
                <?php } ?>
                <?php $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0)', 'params'=>array(':x'=>$my->id,':y'=>$userid)));
                       $p=0;
                        $status = array();
                        foreach ($datarow as $row){ 
                                  if($p==3)
                                    {
                                       break;
                                    }     
                                   $status[] = $row->value ;
                                   $p++;
                        }
                       
                       $countstatus = count($status);
                ?>
                    <p><a  onclick='postToFeed("<?php echo $my->title;?>","<?php echo Yii::app()->params['siteurl'];?>","<?php echo $my->image;?>","<?php if($countstatus>0){ for($k=0;$k<$countstatus;$k++){  echo $status[$k].''. ',' ; }} else echo 'no result'; ?>","<?php echo $linksbshare;?>/index.php/site/emaillinklist?listid=<?php echo $my->id;?>&userid=<?php echo $userid;?>"); return false;'><img style="height: 25px;cursor: pointer;" src="<?php echo $baseurl; ?>/images/facebook.png" /></a></p>
                    <!--<p style="display: none;" id='msg'></p>                  -->
                    <div style="float: left; margin-top: 5px;">
                       <a class="thickbox" id="textdecoration" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/emaillist?listid=<?php echo $my->id;?>&KeepThis=true&TB_iframe=true&height=185&width=600"> <span> Share with email</span></a>
                    </div>
                    
            </div>
            
            <div class="outer_latesttitle_div" style="width: 225px;">
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>Title:</span>
                    </div>
                    <div class="latest_title_div">
                        <span><?php echo $my->title; ?></span>
                    </div>
                </div>
               
                
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>No. of Takers:</span>
                    </div>
                    <div class="latest_title_div">
                        <span><?php echo $mycount; ?></span>
                    </div>
                </div> 
                
                <div class="outer_latest_title" style="margin-top: 20px;">                    
                    <div class="latest_title_div">
                        <?php if($my->listtype == 'public') { ?>
                        <span class="mylist_listtypegreen">
                          <?php } else {?>
                          <span class="mylist_listtypered">
                          <?php }?>      
                        <?php echo $my->listtype; ?>
                        </span>
                    </div>
                </div>
                
                <div class="outer_latest_title">
                    <div class="latest_title" style="width: 100px; margin-top: 25px;">
                        <a id="textdecoration" onclick="editlist_popup('<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/editlist?listid=<?php echo $my->id;?>');" href="#"><span>Edit List</span></a>
                    </div>
                    
                </div>
                
            </div>
        </div>
        <?php } } else {?>
        <div class="mylist_mydata">
            <span>
                No list created
            </span>
        </div>
        <div class="floatleft">
            <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/listtitle"><span>Create Now</span></a>
        </div>
        <?php }?>
    </div>
    
    <div class="outerdiv11" style="width: 380px;">
        <h2>My Taken Lists</h2>
        <?php if($countlist>0)
                {
              if($counttaken>0)
                {
                foreach ($takenlist as $mytakenlist){ 
                    $listform = ListForm::model()->findAll("id = '$mytakenlist->list_id'");
                
        foreach ($listform as $my){   ?>
        <div class="outer_see_list">
            <div class="img_outer">
            <div class="latest_image">
                <img class="image_mylist"  src="<?php echo $baseurl;?>/images/<?php echo $my->image;?>"/>
            </div>
                <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/detailtakelist?listid=<?php echo $my->id;?>"> <span>View Details</span></a>
            </div>
            
            <div class="outer_latesttitle_div" style="width: 225px;">
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>Title:</span>
                    </div>
                    <div class="latest_title_div">
                        <span><?php echo $my->title; ?></span>
                    </div>
                </div>
                
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>Author:</span>
                    </div>
                    <div class="latest_title_div">
                        <?php $user = User::model()->find("id = '$my->user_id'");?>
                        <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/user/userprofile?userid=<?php echo $user->id;?>">
                            <span><?php echo $user->firstname.' '.$user->lastname;?></span>
                        </a>
                    </div>
                </div>
                
                <div class="outer_latest_title">
                    <div class="latest_title">
                        <span>No. of Takers:</span>
                    </div>
                    
                    <?php $countpeople = ListTaken::model()->findAll("list_id = '$my->id'"); 
                    $count = count($countpeople); ?>
                    <div class="latest_title_div">
                        <span><?php echo $count; ?></span>
                    </div>
                </div>
                
            </div>
            </div>
           <?php } } }else {?>
        <div class="mylist_mydata">
            <span>
                No list taken
            </span>
        </div>
        <div class="floatleft">
            <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/preferences/latestlists"><span>Take Now</span></a>
        </div>
        <?php } }?>
                
    </div> 
    </div>
</div>
