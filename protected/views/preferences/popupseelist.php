<?php
$listid = $_GET['listid'];
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid     = Yii::app()->user->getID();
$myuserinfo = User::model()->find("id = '$userid'");
$data       = ListForm::model()->find("id = '$listid'");
$datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$userid'");
$count      = count($datacolumn);
$count      = $count+1;
$datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y', 'params'=>array(':x'=>$listid,':y'=>$userid)));
            
?>
<div class="popup_outerdiv1_auth">
    <div class="popup_outer_see_list">
        <div class="popup_newupper_textdiv">You can edit or delete the list</div>
        <div class="popup_list_name">
            <div style="color: #C8C8C8; font-family: Geosanslight; font-weight: bold; font-size: 16px;" class="list_name_div">
                <span><?php echo $data['name'] ?></span>
            </div>
            <div class="list_image_div">
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'];?>" class="popup_image_mylist"/>
            </div>
            
            <div style="color: #C8C8C8; float: left; font-family: Geosanslight; font-weight: bold; font-size: 16px;">
            <div class="latest_title_cre">
                        <span>Created by:</span>
            </div>
            <div class="latest_title_div">
                        <span><?php echo $myuserinfo->firstname.' '.$myuserinfo->lastname; ?></span>
            </div>
            </div>
            <div class="list_image_div">
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/profile/<?php echo $myuserinfo['image'];?>" class="popup_image_mylist"/>
            </div>
            
        </div>
       
        <div class="maintable">
            <div style="color: white;" class="list_title_div">
                <table class="tablediv">
                    <tr>
                        <td class="popup_editlist_titlediv">
                                <span><?php echo $data['title'] ; ?></span>
                        </td>
                        <?php foreach ($datacolumn as $column){ ?>
                        <td class="column_div">
                                    <span> <?php echo $column->value ;?> </span>
                        </td>
                         <?php }?>
                    </tr>
                    
                    <tr>
                    <?php  $temp = 1;
                      foreach ($datarow as $row){ ?>
                     
                        <td class="popup_seelist_row_div">
                            <span> <?php echo $row->value ;?> </span>
                        </td>
                       <?php 
                        if($temp%$count==0)
                        {
                            echo "</tr><tr>";
                        }  
                        $temp++;
                       ?>
                      
                      <?php  } ?>
                   </tr>
               
                </table>
           </div>
        </div>
        <div style="clear: both; padding-bottom: 40px;">&nbsp;</div>
        <a  href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/popupeditlist?listid=<?php echo $listid;?>">
        <div class="official_today_createnew_margine" style="padding-top:0px;" >
            <span style="font-family: Geosanslight; font-size: 25px;">Edit List</span>
        </div>
        </a>
        
        <a  href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/popupdeletelist?listid=<?php echo $listid;?>">
        <div class="official_today_createnew_margine" style="padding-top:0px;" onclick="return deletelist();">
            <span style="font-family: Geosanslight; font-size: 25px;">Delete List</span>
        </div>
        </a>
    </div>
