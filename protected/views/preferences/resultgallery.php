<h3><?=StringsHelper::capitalize($list->title)?></h3>
<div id="gallery" class="content">
    <div class="slideshow-container">
        <div id="loading" class="loader"></div>
        <div id="slideshow" class="slideshow"></div>
    </div>
    <div id="caption" class="caption-container"></div>
</div>
<div id="thumbs" class="navigation">
    <?php  
        $rowsLi = array();
        $scrollIndex = 0;
        global $rowsLi;
        global $scrollIndex;
        $opinions = $list->opinionRaiting($friends_ids_filter);
        $subOpinions = array();
        foreach($opinions as $key=>$opinion) 
        {
            $opsInfo = $this->opinionInfo($opinion['list_id'], $opinion['value'],$friends_ids_filter);
            if (sizeof($opsInfo) > 0) {
                foreach ($opsInfo as $k=>$opt) {
                    if ($user->id == $opt['user_id']) {
                        $_ = $opt;
                        unset($opsInfo[$k]);
                        array_unshift($opsInfo, $_);
                    }
                }
                $subOpinions[$key] = $opsInfo;
            }                                                 
        }

        function outOpinion($opinion,$currentChoice,$key) {
            $user = Yii::app()->user->getModel();
            global $rowsLi;
            global $scrollIndex;
            if (!empty($opinion['image'])) {
                $img = Yii::app()->request->baseUrl . '/images/titleimages/list_thumbs/official/small/' . $opinion['image'];
                $userInfo = User::model()->find(array(
                    'select' => 'firstname, lastname',
                    'condition' => 'id=:id',
                    'params' => array(':id'=>$opinion['user_id']),
                ));
                
                $userFullName = '';
                if (!empty($userInfo->firstname) && !empty($userInfo->lastname)) {
                    $userFullName = StringsHelper::capitalize($userInfo->firstname.' '.$userInfo->lastname);
                } else if (!empty($userInfo->firstname)) {
                    $userFullName = StringsHelper::capitalize($userInfo->firstname);
                }

                if (strtolower($currentChoice) !== strtolower($opinion['value'])) {
                    if ($currentChoice != '_') {
                        echo '</ul>';
                    }          
                    ?>
                    <?php if (!empty($opinion['value']) && $opinion['value'] != '_') {?>
                    <p class="group"><span><?=$key?></span> <?=$opinion['value']?></p>
                    <?php }
                    $scrollIndex++;?>
                    <ul class="thumbs noscript" id="trmb<?=$key?>" data-index="<?=$key?>">
                <?php
                }
                ?>
                    <li data-index="<?=$key?>" class="captionTop6">
                        <a class="thumb" name="leaf" href="<?=$img?>?w=528&h=340&logic=2" title="" data-scroll-index="<?=$scrollIndex?>">
                            <img src="<?=$img?>?w=150&h=84" alt="" />
                            <?php if($user->isAdmin()) {?>
                            <div class="removeGalleryImg" data-image="<?=$opinion['image']?>" data-list="<?=$opinion['list_id']?>">
                                <img src="<?=Yii::app()->request->baseUrl;?>/images/cross1.png" alt="" />
                            </div>
                            <?php }?>
                        </a>
                        <div class="caption">
                            <div class="image-desc">
                                <span class="author" onclick="showUserBriefs(<?=$opinion['user_id']; ?>, '#wrapper'); closeFancy();">Added by: <strong><?=$userFullName?></strong></span>
                                <span class="date">On: <strong><?=date('m/d/Y',  strtotime($opinion['created']))?></strong></span>
                                <?php if (!empty($opinion['link_title'])) {?>
                                <span class="link"><a href="<?=!empty($opinion['link']) ? $opinion['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($opinion['link_title'])?></a></span>
                                <?php }?>
                            </div>
                        </div>
                    </li>  
                <?php    
                $rowsLi[$scrollIndex] = empty($rowsLi[$scrollIndex]) ? 1 : $rowsLi[$scrollIndex] + 1; 
                $currentChoice = $opinion['value'];
            }
            return $currentChoice;
        }

        $currentChoice = ''; 
        outOpinion(array('user_id' => $list->user_id,
                          'created' => $list->modified,
                          'image'   => $list->image,
                          'list_id' => $list->id,
                          'link_title' => $list->link_title, 
                          'link' => $list->link,
                          'value'   => '_'),'',0);
        $scrollIndex--;
        foreach($opinions as $key=>$opi) 
        {
            if (!empty($subOpinions[$key])) {
                foreach($subOpinions[$key] as $opinion) {
                    $currentChoice = outOpinion($opinion,$currentChoice,($key+1));
                }
            }
        }
        if (sizeof($rowsLi) > 0) {
            foreach ($rowsLi as &$value) {
                $value = ceil($value/3);
                if ($value == 0) {
                    $value = 1;
                }
            }
        }
    ?>
</div>
<input type="hidden" value='<?=json_encode($rowsLi,true)?>' id="rowsLi" name="rowsLi" />
<input type="hidden" value="0" id="pageRedirect" name="pageRedirect" />