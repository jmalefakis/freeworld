<?php $this->renderPartial('sidenavigation'); 
$baseurl = Yii::app()->request->baseUrl;
// Get client script
$cs=Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/colorbox.css');

// Add JS
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>   
<div class="outerdiv1_auth">
    <?php $listid = $_GET['listid'];     
    $userid = Yii::app()->user->getID();
    ?>
    <div class="outer_see_list">
        <div style="width: 100%;" class="popup_list_title_div1">               
                <div class="popup_row_namefild">
                    <span style="color: #333232;"><?php echo $data['title']; ?></span>
                </div>
        </div>
        
        <div style="width: 100%; padding-top: 20px;" class="popup_listimage_maindiv">
                <div class="list_detail_row_textfild">
                    <?php if(strpos($data['image'], 'profile/') === 0){ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else if(strpos($data['image'], 'freeworld/') === 0){ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else{ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/listdetail/'.$data['image']; ?>" />
                    <?php } ?>
                </div>
                
                <?php 
                $createdtime = $data->modified;
                $currenttime = time();
                $difference  = $currenttime - $createdtime; 
                $years       = abs(floor($difference / 31536000));
                $days        = intval($difference / 86400); 
                $difference  = $difference % 86400;
                $hours       = intval($difference / 3600); 
                $difference  = $difference % 3600;
                $minutes     = intval($difference / 60); 
                $difference  = $difference % 60;
                $seconds     = intval($difference);                 
                ?>
                
                <div class="list_detail_listimage_authorinfo">
                    <span class="list_detail_author_attr">Author:</span>&nbsp;
                    <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/user/dashboard?userid=<?php echo $myuserinfo->id;?>">
                    <span style="text-decoration: underline;" class="list_detail_author_val"><?php  echo $myuserinfo->firstname.' '.$myuserinfo->lastname;; ?></span>
                    </a>
                    <br/>
                    
                    <span class="list_detail_author_attr"  style="width: 75px;">Status:</span>&nbsp;<span><?php if($myuserinfo->profile_status!= null){ echo $myuserinfo->profile_status;} else { echo 'No status updated' ; }?></span><br/>
                    <span class="list_detail_author_attr">Category:</span>&nbsp;<span class="list_detail_author_val"><?php echo $myuserinfo->interest; ?></span>   <br/>                 
                    <span class="takelist_newdis_span1">Created:</span>&nbsp;
                          <?php if($years>1){?>
                            <span><?php echo $years;?> years ago</span> <br/>

                            <?php }elseif($days>1){?>
                            <span ><?php echo $days;?> days &nbsp;ago</span><br/>

                            <?php }elseif($hours>24){?>
                            <span ><?php echo $hours;?> hours &nbsp;ago</span>  <br/>

                            <?php } elseif($minutes>1) {?>
                             <span ><?php echo $minutes?> minutes &nbsp;ago</span><br/>    

                           <?php }elseif($seconds<60) {?>
                            <span ><?php echo $seconds?> sec &nbsp;ago</span><br/>

                            <?php } ?> 
                    <span class="list_detail_author_attr">Votes:</span>
                    <div style="font-size: 11px;" class="popup_votingspan_outer">
                        <div style="padding: 4px 0 0;" class="popup_votingspan_inner"><?php echo count($vote);?></div>
                    </div>
                </div>
            </div>
        
        
       
       
        <div class="maintable">
            <div class="list_title_div" style="margin-right: 20px;border: none;margin-left: 100px;">
                <table class="tablediv" id="myTable">
                    <tr>
                        <td>
                             <div class="listdetail_titlete_textfild" style="background: #789bda; padding-left: 3px;">
                                <span style="float: left; padding: 4px 0 0 2px;" ><?php echo $data['title']; ?></span>
                             </div>
                        </td>
                        
                        <td>
                                <div class="listdetail_titlete_textfild" style="background: #789bda; padding-left: 3px;text-align: left;">
                                    <span>  Rating</span>              
                                </div>
                        </td>
                        
                        <?php /*$i=0;
                              foreach ($datacolumn as $column){
                                     if($i==3)
                                     {
                                         break;
                                     }
                        ?>                        
                        <td class="column_div">
                                    <span> <?php echo $column->value ;?> </span> 
                                    <?php $i++; ?> 
                        </td>
                         <?php }*/?>
                    </tr>
                    
                    <tr>
                    <?php $temp = 1;
                       foreach ($datarow as $row){ ?>
                        <td>
                           <div class="listdetail_titlete_textfild">
                                 <span style="float: left; padding: 4px 0 0 2px;" ><?php echo $row->value ;?></span>
                            </div>
                        </td>
                        
                        <?php 
                        if($temp%2==0)
                        {
                            echo "</tr><tr>";
                        }  
                        $temp++;
                       ?>
                        
                       <?php 
                       /*if($count<4){
                            if($temp%$count==0)
                            {
                                echo "</tr><tr>";
                            }
                       }
                        else
                        {
                            if($temp%4==0)
                            {
                            echo "</tr><tr>";
                            }
                        }
                        $temp++;*/
                       ?>
                      
                      <?php  }  ?>
                   </tr>
               
                </table>
           </div>
            <!--<div  class="showmor_take">
                <a class="textdecoration" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/seelist?listid=<?php //echo $listid;?>&url=<?php //echo $url;?>"><span>Show More</span></a>
            </div>-->
            <div style="clear: both;">&nbsp;</div>
            <a  onclick="editlist_popup('<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/editlist?listid=<?php echo $listid; ?>');" href="#">
            <div class="official_today_createnew_margine" >
                <span>Edit List</span>
            </div>
            </a>
        </div>
       
    </div>

</div>
    