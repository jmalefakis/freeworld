


<div class="userprofilemain">
     <?php $this->renderPartial('sidenavigation'); 
           $baseurl = Yii::app()->request->baseUrl; 
           $userid   = Yii::app()->user->getID(); 
           // Get client script
            $cs=Yii::app()->clientScript;

            // Add CSS
            $cs->registerCSSFile($baseurl.'/css/colorbox.css');

            // Add JS
            $cs->registerScriptFile($baseurl.'/js/jquery.form.js');
            $cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
    ?>
    <div class="official_today">
        <div class="upper_official_today">
            <div class="floatleft">
<?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/preferences/officialtoday',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',            
        'validateOnSubmit'=>true,
     ),
      )); 
?>  
            <div class="official_today_date">
                <div class="official_today_sortdate" style="width: 113px;">
                    <span> Sort by date</span>   
                </div>
                <div class="floatleft">
                    <?php                          
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[birthdate]', 
                    'value'=> '', 
                    // additional javascript options for the date picker plugin
                    'options'=>array(                                           
                         'yearRange'=> '-102:+0',
                        'dateFormat'=>'yy-mm-dd',                        
                        //'defaultDate'=>'01/01/2012',
                        //'yearRange'=>'1960:2012', 
                        'showAnim'=>'fold',                        
                        'debug'=>true,
                        'showOn'=> "button",
                        'buttonImage'=> "$baseurl/images/freeworld/right-arrow.png",
                        'buttonImageOnly'=> true
                    ),
                    'htmlOptions'=>array(
                         'style'=>'height:0px;width:0px;',
                         'readonly'=>"readonly",
                         'onchange'=>"sortbydate('$baseurl')",
                    ),
                )); ?> 
                </div>
            </div>
            
            <div class="official_today_date_top">
                <div class="official_today_sortdate">
                    <span> Sort by topic:</span>   
                </div>
                <div class="floatleft OptionselectBoxContainerdiv">
                   <select onchange="sortbydate()"  id="interest" name="Interest[user]"  class="official_today_droup select-list">
                         <option selected=selected>Select your interest</option>
                         <?php foreach ($interest as $inter){?>
                          <option <?php if(isset($post) && $post==$inter->id){?> value="<?php echo $post;?>" selected=selected" <?php } else {?> value="<?php echo $inter->id;?>" <?php } ?> ><?php echo $inter->interest?></option>
                          <?php }?>
                     </select>
                </div>
            </div>
            
                        
<?php $this->endWidget(); ?> 
                
            </div>
            
            <div class="official_search">
                   <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/preferences/search',
        'method'=>'GET',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'searchform',   
        'onsubmit'=>'return searchresult();',
        'validateOnSubmit'=>true,
     ),
      )); 
?>
                <div class="search_list_official">
                    <div class="search_list_div1_official">
                        <input placeholder="Search Preferences"  id="searchres" type="text" name="search"  class="forgotpasinput_official"/>
                    </div>
                    <div class="search_list_div2_official">
                        <input type="image"   src="<?php echo $baseurl;?>/images/freeworld/arrow-1.png" style="height: 27px;"/>
                    </div>  
                    <div id="requred" class="search_list_div3_official" >
                        <span >Please fill first.</span>
                    </div>
                </div>

<?php $this->endWidget(); ?>
                </div>
        </div>
        <div class="floatleft" style="width: 945px;">
            <div class="notificationcenter_page">
                Notification Center
            </div>
            <div class="notificationcenter_outerdiv">
                <div class="notificationcenter_firstdiv">
                    <div class="notificationcenter_firstinner">
                            <span>Invitations</span>
                    </div>
                    
                     <div class="notificationcenter_secondinner" id="content_1">
                        
                        <?php $counttoday     = 0;
                              $countyesterday = 0; 
                              $countold       = ' ';
                               foreach($invitationnoti as $value) {                         
                               $date = explode('-', $value->created);   
                               $user = User::model()->find("id = '$value->myuser_id'");
                        ?>
                        
                        <div class="notificationcenter_notidiv">
                            <div class="notificationcenter_todaydiv"> 
                                <?php $today = date('d');
                                      $yesterday = $today-01;                                      
                                      if($date[2]== $today) {  
                                      if($counttoday==0){
                                 ?>
                                 <span> Today</span>
                                <div style="height: 10px;"></div>
                                <?php  $counttoday++; }  } elseif($date[2]==$yesterday){ if($countyesterday==0){?>
                                 <div style="height: 15px;"></div>
                                 <span> Yesterday</span>
                                  <div style="height: 10px;"></div>
                                <?php $countyesterday++; } } else { $old = date("M,d,Y",$value->modified);  if($countold!=$old){ ?>
                                 <div style="height: 15px;"></div>
                                   <span> <?php echo $old;?></span>
                                   <div style="height: 10px;"></div>
                                 <?php $countold=$old;}}?>
                            </div>
                            <div class="notificationcenter_imagediv">
                                <div class="userimage_notification">
                                    <?php if($user['image']!=''){?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $user['image'];?>"/>
                                       <?php } else { 
                                          if($user['gender']=='male')
                                          {
                                        ?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                                       <?php } elseif($user['gender']=='female') {?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                                       <?php } else {?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                                       <?php } }?>
                                </div>
                                <div class="notificationcenter_message">
                                    <span><?php echo $value->message;?></span>
                                </div>
                            </div>
                        </div>
                        
                        <?php }  ?>
                     </div>
                    
                </div>
                <div class="notificationcenter_secdiv">
                    <div class="notificationcenter_firstinner">
                            <span>Lists</span>
                    </div>
                    <div class="notificationcenter_secondinner" id="content_2">
                        
                        <?php $counttoday     = 0;
                              $countyesterday = 0; 
                              $countold       = ' ';
                               foreach($listnoti as $value) {                         
                               $date = explode('-', $value->created);   
                               $user = User::model()->find("id = '$value->myuser_id'");
                        ?>
                        
                        <div class="notificationcenter_notidiv">
                            <div class="notificationcenter_todaydiv"> 
                                <?php $today = date('d');
                                      $yesterday = $today-01;                                      
                                      if($date[2]== $today) {  
                                      if($counttoday==0){
                                 ?>
                                 <span> Today</span>
                                <div style="height: 10px;"></div>
                                <?php  $counttoday++; }  } elseif($date[2]==$yesterday){ if($countyesterday==0){?>
                                 <div style="height: 15px;"></div>
                                 <span> Yesterday</span>
                                  <div style="height: 10px;"></div>
                                <?php $countyesterday++; } } else { $old = date("M,d,Y",$value->modified);  if($countold!=$old){ ?>
                                 <div style="height: 15px;"></div>
                                   <span> <?php echo $old;?></span>
                                   <div style="height: 10px;"></div>
                                 <?php $countold=$old;}}?>
                            </div>
                            <div class="notificationcenter_imagediv">
                                <div class="userimage_notification">
                                    <?php if($user['image']!=''){?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $user['image'];?>"/>
                                       <?php } else { 
                                          if($user['gender']=='male')
                                          {
                                        ?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                                       <?php } elseif($user['gender']=='female') {?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                                       <?php } else {?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                                       <?php } }?>
                                </div>
                                <div class="notificationcenter_message">
                                    <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/preferences/listdetail?listid=<?php echo $value->list_id;?>">
                                    <span><?php echo $value->message;?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <?php }  ?>
                     </div>
                </div>
                <div class="notificationcenter_thirddiv">
                    <div class="notificationcenter_firstinner">
                            <span>Mentions</span>
                    </div>
                    <div>
                        
                     </div>
                </div>
                <div class="notificationcenter_forthdiv">
                    <div class="notificationcenter_firstinner">
                            <span>Friends</span>
                    </div>
                    <div class="notificationcenter_secondinner" id="content_4">
                        
                        <?php $counttoday     = 0;
                              $countyesterday = 0; 
                              $countold       = ' ';
                               foreach($friendnoti as $value) {                         
                               $date = explode('-', $value->created);   
                               $user = User::model()->find("id = '$value->myuser_id'");
                        ?>
                        
                        <div class="notificationcenter_notidiv">
                            <div class="notificationcenter_todaydiv"> 
                                <?php $today = date('d');
                                      $yesterday = $today-01;                                      
                                      if($date[2]== $today) {  
                                      if($counttoday==0){
                                 ?>
                                 <span> Today</span>
                                <div style="height: 10px;"></div>
                                <?php  $counttoday++; }  } elseif($date[2]==$yesterday){ if($countyesterday==0){?>
                                 <div style="height: 15px;"></div>
                                 <span> Yesterday</span>
                                  <div style="height: 10px;"></div>
                                <?php $countyesterday++; } } else { $old = date("M,d,Y",$value->modified);  if($countold!=$old){ ?>
                                 <div style="height: 15px;"></div>
                                   <span> <?php echo $old;?></span>
                                   <div style="height: 10px;"></div>
                                  <?php $countold=$old;}}?>
                            </div>
                            <div class="notificationcenter_imagediv">
                                <div class="userimage_notification">
                                    <?php if($user['image']!=''){?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $user['image'];?>"/>
                                       <?php } else { 
                                          if($user['gender']=='male')
                                          {
                                        ?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                                       <?php } elseif($user['gender']=='female') {?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                                       <?php } else {?>
                                       <img class="notificationcenter_userimg" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                                       <?php } }?>
                                </div>
                                <div class="notificationcenter_message">
                                    <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/user/dashbord?userid=<?php echo $value->myuser_id;?>&notificationid=<?php echo $value->id;?>">
                                    <span><?php echo $value->message;?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <?php }  ?>
                     </div>
                </div>
            </div>
        </div>
        
        <div class="official_today_div3">
            <div class="official_today_date">                
                
            </div>
            <div class="floatleft">
                <img stylse="width: 157px; height: 515px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/advertisment.png"/>
            </div>
        </div>
        
    </div>
</div>