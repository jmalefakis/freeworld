<?php 
$baseurl = Yii::app()->request->baseUrl;
$path = Yii::app()->getRequest()->pathInfo;
$url1 = explode("/", $path);
$url  = $url1[0];

$userid = Yii::app()->user->getID();
$user = User::model()->findByPk($userid);

if (!$_ajax)
{
?>

<input type="hidden" name="pageno_off" id="pageno_off"
	value="1" />
<input type="hidden" name="baseurl"
	id="baseurl" value="<?php echo $baseurl?>" />
<input type="hidden"
	name="serchheader_recentsearch" id="serchheader_recentsearch"
	value="<?php echo $string;?>" />
<input type="hidden" name="currenturl"
	id="currenturl" value="<?php echo $url1[1]?>" />

<input type="hidden"
	name="listcount" id="listcount" value="<?php echo count($lists);?>" />

<script>
$(document).ready(function(){
    $('title').html('Wayo Search');
    
    setSearchValues();

    initPagination_old($("#contentdiv"), 
            "<?php echo $baseurl;?>/index.php/preferences/recentSearchPagination");

    // filter control
    $("#textUsers").bind("dataChanged", refineFilterSearch);
    $("#textAbout").bind("dataChanged", refineFilterSearch);
});

function clearFilter()
{
    $("#checkMyFriends").prop("checked", false);
    $('#radioRecent').prop("checked", false);
    $('#radioTaken').prop("checked", true);

    $("#tagsAbout span.tag").remove();
    $("#textAbout").data("ids", []);
    $("#textAbout").val('');
    
    $("#tagsUsers span.tag").remove();
    $("#textUsers").data("ids", []);
    $("#textUsers").val('');
     
    refineFilterSearch();
}

function refineFilterSearch()
{
    var url = "<?php echo $baseurl; ?>/index.php/preferences/recentSearchListsPagination";
	ajaxSearch(url, "contentdiv", getSearchValues(), {
	    most: $("input[type='radio'][name='radioMost']:checked").val(),
	    myfriends: $("input[type='checkbox'][name='checkMyFriends']").prop("checked"),
	    users: $("#textUsers").data("ids").join(","),
	    about: $("#textAbout").data("ids").join(","),
	});    
}

</script>

<div class="featured_list_section_internal">
	<div class="official_today">
	<div id="wrapper">
	<div id="mainContent">
		<div class="outer_reinesearch">
			<div class="hidden_filter" id="hidden_filter">
				<div class="hidden_filter22">
					<div class="takenby_div" id="takendviv">

						<div class="spantaken_div"></div>
						<div class="clearboth"></div>
						<div class="floatleft" style="width: 100%; margin-bottom: 15px;">
							<div class="floatleft" style="margin-top: 12px;">
								<input type="checkbox" name="checkMyFriends" id="checkMyFriends"  <?php if (Yii::app()->user->getModel()->isCompany()) echo 'style="visibility: hidden;"'; ?>
									class="inputcheckbox_recent" value="myFriends"
									<?php if ($myFriends == "true") { ?>checked="true"<?php } ?> 
									onclick="refineFilterSearch();"/>
							</div>
							<div class="myfriendsdiv_recent" <?php if (Yii::app()->user->getModel()->isCompany()) echo 'style="visibility: hidden;"'; ?>>
								<span>My Friends</span>
							</div>
							<div class="space_recentdiv" id="space_recentdiv">
								<span class="thesepeople_recentsearch">These people </span>
								<div class="floatleft">
									<div class="div_1erecentSearch">
										<div class="recever_div_recent1" id="tagsUsers">
											<input type="text" name="textUsers" value=""
											    class="input_receveir" id="textUsers" />
											    
											    <?php 
                                				    $this->renderPartial("/controls/autocomplete", array(
                                                        'searchUrl' => $baseurl . '/index.php/user/users',
                                                        'targetDivId' => "tagsUsers",
                                                        'minLength' => 1,
                                                        'data' => $users
                                                    ));
                                				?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="twinty_most" id="twinty_most">
						<div class="spanmost_div"></div>
						<div class="recentserch_div_twinty" style="margin-top: 5px;">
							<div class="floatleft" style="margin-left: 42px;">
								<input type="radio" name="radioMost" id="radioTaken"
									class="twinty_recent_radio" value="taken"
									<?php if ($most != "recent") { ?>checked="true"<?php } ?>
									onclick="refineFilterSearch();" />
							</div>
							<div class="floatleft">
								<span>Taken</span>
							</div>
						</div>

						<div class="recentserch_div_twinty">
							<div class="floatleft" style="margin-left: 42px;">
								<input type="radio" name="radioMost" id="radioRecent"
									class="twinty_recent_radio" value="recent"
									<?php if ($most == "recent") { ?>checked="true"<?php } ?>
									onclick="refineFilterSearch();" />
							</div>
							<div class="floatleft">
								<span>Recent</span>
							</div>
						</div>
					</div>

					<div class="specillay_recent" id="specillay_recent">
						<div class="spanSpecifically_div"></div>
						<div class="recentserch_div_twinty" style="margin-top: 5px;">
							<div class="recent_about_search">
								<span>About</span>
							</div>
							<div class="floatleft" style="margin-left: 45px;">
								<div id="tagsAbout" 
								    class="recever_div_recent1" style="margin-bottom: 12px;">
									<input class="input_receveir" type="text" name="textAbout"
											id="textAbout"/> 
											
								    <?php 
                    				    $this->renderPartial("/controls/autocomplete", array(
                                            'searchUrl' => $baseurl . '/index.php/user/userInterestNew',
                                            'targetDivId' => "tagsAbout",
                                            'minLength' => 1,
                                            'data' => $about
                                        ));
                    				?>
                    				
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="clear"></div>
				
				<div class="blue-btn marg-left"
					onclick="clearFilter();">Clear filters</div>

				<div class="blue-btn marg-right"
					onclick="refineFilterSearch();">Run on !</div>
			</div>
		</div>

		<div class="content" id="contentdiv">
		
<?php 
} //!_ajax
?>

			<?php            
			if ($pageno == 0)
			{
			?>
			
			    <input id="searchListCount" type="hidden"
				    value="<?php echo $_SESSION["searchListCount"]; ?>" /> 
				<input id="searchPeopleCount" type="hidden"
				    value="<?php echo $_SESSION["searchPeopleCount"]; ?>" /> 
				<input id="searchCompaniesCount" type="hidden"
				    value="<?php echo $_SESSION["searchCompaniesCount"]; ?>" />
				
			<?php 
			}

			if (count($lists) > 0) 
            {   
                $var = 0; $j = 0;
                $odd = 'odd'; 
                
                foreach($lists as $list) 
                {
    			    $user_id   = $list['user_id'];
    			    $title     = $list['title'];
    			    $image     = $list['image'];
    			    $listid    = $list['id'];
    			    $listRecord = ListForm::model()->findByPk($listid);

    			    $userName = User::model()->findByPk($user_id);
    			    $listtakencount = 0;

			        $friendsserch = FriendHelper::getMyFriendsIds();
			        $friendLabel = "Friend";
			    
                    if ($user->isCompany()) 
                    {
                        $friendLabel = "Follower";
                    
                        $c = new CDbCriteria();
                        $c->addInCondition('user_id', FriendHelper::getMyFollowersIds());
                        $listtakencount = $listRecord->countTaken($c);
                    } 
                    elseif ($friendsserch) 
                    {
				       $c = new CDbCriteria();
				       $c->addInCondition('user_id', $friendsserch);
				       $listtakencount = $listRecord->countTaken($c);
				    }

                    $totalListTaken = $listRecord->countTaken();
                   
                    $myCriteria = new CDbCriteria();
                    $myCriteria->addCondition('user_id = :uid');
                    $myCriteria->params = array(':uid' => $userid);
                    $isTakenByMe = $listRecord->countTaken($myCriteria);
                    
                    $isFavorite = false;
                    if ($isTakenByMe) {
                        $taken = $listRecord->taken($myCriteria);
                        $isFavorite = $taken[0]->favorite;
                    }
                    
                    if ($listRecord) {
                    	$rows_html = '';
                    	$rows = $listRecord->opinionRaiting();
                    	foreach($rows as $row_value) {
                    		$rows_html .= '<li><div>' . $row_value['value'] . '</div></li>';
                    	}
                    	$rows_html = '<ol class="list-box-rows ' . ($odd == 'odd' ? 'left-side' : 'right-side') . '">' . $rows_html . '</ol>';
                    }
            ?>

			<?php if($odd == 'odd'){ ?>
			<div class="part_left"
			        data-subject-type="list"
					data-id="<?php echo $listid; ?>" 
					data-is-taken="<?php echo $isTakenByMe ? "true" : "false"; ?>" 
					data-is-favorite="<?php echo $isFavorite ? "true" : "false"; ?>"
			>
			    <div class="list-box-alert-panel">Remove this list from your Dashboard?
				    <div class="btn-close-red"  onclick="javascript:$(this).parent().fadeOut(500);"></div>
					<div class="btn-gray-small" onclick="javascript:$(this).parent().fadeOut(500);">NO</div>
					<div class="btn-gray-small" onclick="javascript:removeListFromFavorite(this);">YES</div>
				</div>
				<div class="big_img_part">
					<div class="nightife">
						<a href="<?php echo $baseurl;?>/index.php/preferences/listdetail?listid=<?php echo $listid;?>" class="nightife2_litle fL size"><?php echo StringsHelper::capitalize($title);?></a>
                        <?php if ($list['participation'] != 'Everybody') {?>
                            <div class="list-box-private-check fR" title="Private List"></div>
                        <?php } else if (!empty($list['badge'])) { ?>
                            <div class="list-box-badge-eagle fR" title="Wayo Official"></div>
                        <?php } ?>
                        <div class="clear"></div>
					</div>
					<div class="big_img captionTop6">
						<a
							href="<?php echo $baseurl;?>/index.php/preferences/listdetail?listid=<?php echo $listid;?>">
							<?php if(strpos($image, 'profile/') === 0){ ?> <img
							class="officialtodayimg"
							src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $image ?>"
							alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" /> <?php } else if(strpos($image, 'freeworld/') === 0){ ?>
							<img class="officialtodayimg"
							src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $image ?>"
							alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" /> <?php } else{ ?>
							<img class="officialtodayimg"
							src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/official/big/' . $image  . '?w=548&h=304'; ?>"
							alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" /> <?php } ?>
							<?php if ($list && count($rows) > 0) { echo $rows_html; } ?>
						</a>
                        <?php if (!empty($list['link_title'])) {?>
                        <div class="caption6">
                            <div class="captionInner6">
                                <a href="<?=!empty($list['link']) ? $list['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list['link_title'])?></a>
                            </div>
                        </div>
                        <?php }?>
					</div>
				</div>
				<div class="bottomimgleft">
					<div class="alice">
						<a class="alisename"
							href="javascript:void(0);" onclick="listAuthorClick(this, <?php echo $list['user_id']; ?>, '#wrapper');">
							<?php $lengthname = strlen($userName['firstname'].' '.$userName['lastname']) ; if($lengthname<16) { 
							    echo StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
							} else { echo StringsHelper::capitalize($userName['firstname']);
                            }?>
						</a>
					</div>
					
					<div class="block-hidden"></div>
                   					
					<div class="list-box-star tooltip-pointer-simple" onclick="javascript:toggleFavorite(this);"></div>

				    <?php 
				    if ($isTakenByMe)
				    {
                    ?>
				    <div class="icon-list tooltip-pointer-list"></div>
				    <?php
				    } 
                    ?>
					
					<div class="alice_right">
						<div class="contri" style="float: right; padding: 0 8px 0 24px; white-space: nowrap;">
							<span class="integercolor tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"><?php echo $totalListTaken;?> </span>
							<span class="contrspan tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"> In </span>
							<span class="integercolor tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $listtakencount;?> </span>
							<span class="contrspan tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $friendLabel; if ($listtakencount != 1) { ?>s<?php } ?>
							</span>
						</div>
					</div>
				</div>
			</div>
			
			<?php  
			    $odd = 'even'; 
			    if ($j != 0) { 
			    if ($odd=='even' && $var == 0) {
                    $odd='odd';$var=$var+1;
                } else { $var=0; $odd='even';
                }
			    }

                }else { $odd = 'odd';

            ?>

			<div class="part_right"
			        data-subject-type="list"
					data-id="<?php echo $listid; ?>" 
					data-is-taken="<?php echo $isTakenByMe > 0 ? "true" : "false"; ?>" 
					data-is-favorite="<?php echo $isFavorite ? "true" : "false"; ?>"
			    >
			    
			    <div class="list-box-alert-panel">Remove this list from your Dashboard?
				    <div class="btn-close-red"  onclick="javascript:$(this).parent().fadeOut(500);"></div>
					<div class="btn-gray-small" onclick="javascript:$(this).parent().fadeOut(500);">NO</div>
					<div class="btn-gray-small" onclick="javascript:removeListFromFavorite(this);">YES</div>
				</div>

				<div class="nightife2">
					<a href="<?php echo $baseurl;?>/index.php/preferences/listdetail?listid=<?php echo $listid;?>" class="nightife2_litle fL size"><?php echo StringsHelper::capitalize($title);?></a>
                    <?php if ($list['participation'] != 'Everybody') {?>
                                <div class="list-box-private-check fR" title="Private List"></div>
                    <?php } else if (!empty($list['badge'])) { ?>
                            <div class="list-box-badge-eagle fR" title="Wayo Official"></div>
                    <?php } ?>
                    <div class="clear"></div>
				</div>
				<div class="image_small captionTop7">
					<a
						href="<?php echo $baseurl;?>/index.php/preferences/listdetail?listid=<?php echo $listid;?>">
						<?php if(strpos($image, 'profile/') === 0){ ?> <img
						class="officialtodayimg_small"
						src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $image ?>"
						alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" /> <?php } else if(strpos($image, 'freeworld/') === 0){ ?>
						<img class="officialtodayimg_small"
						src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $image ?>"
						alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" /> <?php } else{ ?>
						<img class="officialtodayimg_small"
						src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/official/small/' . $image . '?w=351&h=306'; ?>"
						alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" /> <?php } ?>
						<?php if ($list && count($rows) > 0) { echo $rows_html; } ?>
					</a>
                    <?php if (!empty($list['link_title'])) {?>
                    <div class="caption7">
                        <div class="captionInner7">
                            <a href="<?=!empty($list['link']) ? $list['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list['link_title'])?></a>
                        </div>
                    </div>
                    <?php }?>
				</div>
				<div class="bottom2">
					<div class="alice">
						<a class="alisename"
							href="javascript:void(0);" onclick="listAuthorClick(this, <?php echo $list['user_id']; ?>, '#wrapper');">
							<?php $lengthname = strlen($userName['firstname'].' '.$userName['lastname']) ; if($lengthname<16) { 
							    echo StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);
							} else { echo StringsHelper::capitalize($userName['firstname']);
                            }?>
						</a>
					</div>
					
					<div class="block-hidden"></div>
                    
					<div class="list-box-star tooltip-pointer-simple" onclick="javascript:toggleFavorite(this);"></div>

				    <?php 
				    if ($isTakenByMe)
				    {
                    ?>
				    <div class="icon-list tooltip-pointer-list"></div>
				    <?php
				    } 
                    ?>
                    
					<div class="alice_right2">
						<div class="contri2" style="float: right; padding: 0 8px 0 24px; white-space: nowrap;">
							<span class="integercolor tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"><?php echo $totalListTaken;?> </span>
							<span class="contrspan tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"> In </span>
							<span class="integercolor tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $listtakencount;?> </span>
							<span class="contrspan tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $friendLabel; if ($listtakencount != 1) { ?>s<?php } ?>
							</span>
						</div>

					</div>
				</div>
			</div>
			
			<?php  
			    if($odd=='odd' && $var == 0)
                {
			        $odd='even';
			        $var=$var+1;
			    } 
			    else 
              {
                    $var=0;$odd='odd';
                }
            } 
            ?>

	<?php 
			$j++;
		}
    } 
    else if (!$_ajax || $pageno == 0)
   {
    ?>
            <div class="nolistfound">No lists found</div>
	<?php 
    } 
    ?>
		
<?php 
if (!$_ajax)
{
?>	
        </div>
		
		<div class="officialtodatloding" id="officialtodatlodingdiv">
			<div class="officialtodatlodingimg">
				<img
					src="<?php echo Yii::app()->request->baseUrl;?>/images/loading.gif" />
			</div>
			<div class="officialtodatlodingtext">Loading...</div>
		</div>
    </div>
    </div>
	</div>
</div>

<?php 
}
?>
