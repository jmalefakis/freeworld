<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/upload.js"></script>
<script>
jQuery(document).ready(function(){ 
     $('title').html('Edit List');
});
</script>
<?php
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid  = Yii::app()->user->getID();
$path =  Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url  = $url1[0];
$siteurl = Yii::app()->params['siteurl'];
?>


<input type="hidden" name="onclick" value="personalclick" id="onclick"/>
 <input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>"/>
 <input type="hidden" id ="hidden_baseurl" value="<?php echo $baseurl ?>"/>
 <input type="hidden" name="returnfalse" value="" id="returnfalse"/>
<div class="featured_list_section_internal">
<div class="widthauto">
    <?php //$this->renderPartial('sidenavigation'); ?>
         <div class="outerdiv" id="liscreation">
 <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$siteurl.'/index.php/preferences/editlist?listid='.$listid,
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',
        'onsubmit'=>'return listtitle();',
        'validateOnSubmit'=>true,
     ),
      ));
	  $friendsserch ='';
?>
         <div class="inputtitle_listcreate">
             <div class="div_textfild">
                 <input id="title" type="text" name="ListForm[title]"  value="<?php if(isset($list_detail)) {echo $list_detail['title'];} else { echo "List title";}?>"  class="title_listtitle"  />
                 <div id="loding" style="float: right; width: 9px; height: 20px; margin-top: -29px;display: none;">
                    <img src="<?php echo $baseurl;?>/images/ajax-loader.gif"/>
                </div>
             </div>
             
                <div style="margin: 4px 0 0 62px;" class="validtitle" id="valid">
                    <span >This is valid title</span>
                </div>
                <div style="margin: 4px 0 0 62px;" class="invalidtitle" id="invalid">
                    <span style="color: red;">This is not valid title</span>
                </div>
                <div class="preferences_listtype_requretitlee" id="titlereq">
                    <span>This field is required </span>
                </div>
                 <div class="preferences_listtype_seesuggestion" id="seesuggestion">
                     
                     <a class="textdecoration" href="#" onclick="suggestion_popup('<?php echo Yii::app()->params['globelUrl'];?>/preferences/suggestionpopup');" >
                         <span > See Suggestion </span> 
                     </a>
                     
                </div>
         </div>
             
             <div class="outer_listcreatediv">
                 <div class="leftside_listcreate">
                     <div class="listtopic_listcreate">
                         List topic
                     </div>                     
                     
                     <div class="topic_listnew OptionselectBoxContan_listtitle" id="div_typing">
                                     <select  id="interest_topic" name="ListForm[interest]"  class="select-list_listtitle_new">
                                         <option value="select" selected="selected" style="padding-left: 3px;">Select</option>
                                         <?php foreach ($interest as $inter){?>
                                          <option value="<?php echo $inter->id?>" style="padding-left: 3px;" <?php if(isset($list_detail) && $list_detail['interest_id'] == $inter->id ){echo "selected='selected'";}?>><?php echo $inter->interest?></option>
                                          <?php } ?>
                                     </select>
                     </div>
                     
                     <span class="numaicclass" id="spaninterest" style="width: 150px;">Please select the list topic</span>
                     
                     <div class="liast_picture">
                         <span class="span-uploadimg_listtile"> Select a picture for your list</span>
                         <div class="myfileupload-listtitle" style="width: 197px;">
                                  <label class="myui-button" style="width: 138px; padding-top: 13px;">
                                      <span id="file_uplod" class="browseimg_listtitle" style="padding: 8px; cursor: default;" tabindex="3"  >BROWSE</span>
                                      <?php $u_agent =  $_SERVER['HTTP_USER_AGENT'];
                                            if(preg_match('/MSIE/i',$u_agent))
                                            {                                             
                                     ?>
                                      
                                       <input   onchange="ajaxUpload(this.form,'<?php echo $baseurl?>/index.php/preferences/listTitleImageForIe','response','loding','Error in Upload, check settings and path info in source code.','<?php echo $baseurl;?>')" id="images" type="file" name="images" value="" class="resettext" style="transform: translate(-138px, 7px) scale(1.2);"/> 
                                      
                                      <?php } else {?>
                                      <input   onclick="listtitle_image('<?php echo $baseurl?>')" id="images" type="file" name="User[profilepic]" value="" class="resettext" /> 
                                      <?php }?>
                                  </label>
    		                  <button type="submit" id="btn" style="display: none;">Upload Files!</button>
                       </div>
                         
                     </div>
                     
                     <div class="image_outer_listtitle" id="response">
                         <?php if(isset ($list_detail) && $list_detail['image']!='') { ?>
                         
                          <?php if(strpos($list_detail['image'], 'profile/') === 0){ ?>
                            <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $list_detail['image'] ?>" />
                          <?php } else if(strpos($list_detail['image'], 'freeworld/') === 0){ ?>
                            <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $list_detail['image']?>" />
                          <?php } else{ ?>
                            <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/official/small/'.$list_detail['image']; ?>" />
                          <?php } ?>
                           
                         
                         <?php } ?>
                     </div>
                     <input type="hidden" id="imguplod" value="<?php echo $list_detail['image']?>"/>
                     <div id="image_upolad" class="city_state_error" style="display: none;">Please upload correct image </div>
                     
                 </div>
                 <div class="rightside_listcreate">
                     
                     <div class="rulles_div_listtitle">
                         Rules for participants
                     </div>
                     <div class="textarea_liattitle">
                         <textarea class="instructiontextarea" id="instruction" name="instruction"><?php if(isset($list_detail)) { echo $list_detail['instruction'];}?></textarea>
                         <div class="preferences_listtype_requretitlee" id="instructionreq" style=" padding-left: 0px;">
                                <span>This field is required </span>
                         </div>
                         <div class="wordlimit_listtitle" id="wordlimit">
                             Rules for participants should be less than 4000 characters.
                         </div>
                         
                     </div>
                     
                     <div class="rulles_div_listtitle">
                         <span>Participation is open for</span>
                     </div>
                     <div>
                         <?php if($list_detail['participation']=='My friends') { echo "My friends";?>
                          <!--<div class="radio" style="width: 100%; margin-left:0px;">
                              <span id="chekspante" class="spanradio_home">
                                  <input type="radio" id="radiobtn1" value="My friends" name="myfriends" class="btn"  style=" margin-top: -2px;" <?php if(isset($list_detail) && $list_detail['participation'] == "My friends") { echo "checked='checked'";} else{ echo "disabled='true'";}?>/> 
                              </span>
                             My friends
                          </div>
                          <div class="radio" style="width: 100%; margin-left:0px;">
                              <span id="chekspante_2" class="spanradio_home">
                                <input type="radio" id="radiobtn2" value="Everybody" name="myfriends" class="btn"   style="margin-top: -2px;" <?php if(isset($list_detail) && $list_detail['participation'] == "Everybody") { echo "checked='checked'";} else{ echo "disabled='true'";}?>/>
                              </span>
                             Everybody
                          </div>-->
                         <?php } else { echo "Everybody";}?>
                         
                   </div>
                  
                     </div>
                 </div>
             
             <div class="editlist_delete">
                 
             </div>
             
             <div class="floatright" style="width: 462px;">
                 
                 <?php if(isset($list_detail) && $list_detail['participation'] == "Everybody") { ?>
                 <div class="deletelistspan_listtitle_gray"></div>               
                   
                 <?php } else {?>
                    <a href="<?php echo $baseurl?>/index.php/preferences/deletelist?listid=<?php echo $listid;?>&user_id=<?php echo $userid;?>">
                      <div class="deletelistspan_listtitle_active" onclick="return deletelist('<?php echo $list_detail['title']?>');"></div>  
                    </a>
                 <?php } ?>
                 
                 <span onclick="javascript:history.go(-1);" class="cursorpointer">
                     <div class="canclespan_listtitle">CANCEL</div> 
                 </span>
                 <input type="submit" value="SUBMIT" class="uploadimg_listtitle"/>
                  <img src="<?php echo $baseurl?>/images/loading.gif" style="float: left; margin-top: 5px;display: none;" id="loding_listtitle"/>
             </div>    

<?php $this->endWidget(); ?>
             
    </div>
</div>
</div>