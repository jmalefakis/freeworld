
<script>
jQuery(document).ready(function(){
    
    $('title').html('Update Entries');
    
    $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
        $(document).mouseup(function (e)
        {
            var container = $(".listdiv_takelist");

            if (container.has(e.target).length === 0)
            {
                container.hide();
            }
        });
        
        
  $(document).ajaxComplete(function(){
    
        $(document).mouseup(function (e)
        {
            var container = $(".listdiv_takelist");

            if (container.has(e.target).length === 0)
            {
                container.hide();
            }
        });
        
       
    
 }); 
        
  
        
     }); 
</script>


<?php
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid  = Yii::app()->user->getID();
$path =  Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url  = $url1[0];
$siteurl = Yii::app()->params['siteurl'];
?>
 <input type="hidden" name="onclick" value="personalclick" id="onclick"/>
 <input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>"/>
 <input type="hidden" id ="hidden_baseurl" value="<?php echo $baseurl ?>"/>
 <input type="hidden" name="listid" value="<?php if(isset($_GET['listid'])){ echo $listid;}?>" id="listid"/>

<div class="featured_list_section_internal">
<div class="widthauto">
    <?php //$this->renderPartial('sidenavigation'); ?>
         <div class="outerdiv" id="liscreation">
 <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$siteurl.'/index.php/preferences/updateEntry?listid='.$listid,
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',
        'onsubmit'=>'return list_createEntry();',
        'validateOnSubmit'=>true,
     ),
      ));
	  
?>
        
             <div class="leftdiv_listentry">
                 <div class="content_part_left">
                     <span><?php echo $list_detail['title'];?></span>
                 </div>
                 <div id="response" class="image_outer_listtitle">
                     
                     <?php if(strpos($list_detail['image'], 'profile/') === 0){ ?>
                        <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $list_detail['image'] ?>" />
                    <?php } else if(strpos($list_detail['image'], 'freeworld/') === 0){ ?>
                        <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $list_detail['image']?>" />
                    <?php } else{ ?>
                        <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/official/small/' . $list_detail['image'] . "?w=368&h=238"; ?>" />
                    <?php } ?>
                        
                     
                 </div>
                 <div class="listcreated_listentry">
                     <span class="createdspan_listentry">Created by:&nbsp;&nbsp;</span><span class="createname_listentry"><?php echo ($userinfo['firstname'].' '.$userinfo['lastname']);?></span><br/>
                     <span class="createdspan_listentry">Created on:&nbsp;&nbsp;</span><span class="createname_listentry"><?php echo date('m/d/Y', strtotime($list_detail['modified']));?></span><br/>
                     <span class="createdspan_listentry">Contributors:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo count($list_Taken);?></span><br/>
                     <span class="createdspan_listentry">Topic:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo $listtopic;?></span><br/>
                     <div class="instriuctiondiv_list">
                          <span class="createdspan_listentry">Instructions:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo $list_detail['instruction'];;?></span><br/>
                     </div>
                 </div>
             </div>
             <div class="rightdiv_listentry">
                 <div class="contentpart_right">
                     <span>Please provide entries</span>
                    
                 </div>
                 
                 <div class="usercomment_listentry">
                     <table style="border-collapse: collapse;" id="myTable">
                         <tr class="tr1_thlistentry">
                             <th class="th1_listentry">#</th>
                             <th class="th2_listentry">Me</th>
                             <th class="th3_listentry">Comments</th>
                         </tr>
                         
                         <?php $hidendata = '';
                            $odd = '';$count = count($datarow);if($count>0)
                            {
                              for($i = 0;$i<count($datarow);$i++) { 
                                if($i%2 ==0 )
                                {
                                    $odd = "even";                                   
                                }
                                else
                                {
                                      $odd = "odd";
                                      
                                }
                                $hidendata .= $datarow[$i]['value'].",";
                                
                        ?>
                            <tr class='tr1_<?php echo $odd;?>'>
                             <td class="th1_listentry">
                                 <input type="text" name="order_<?php echo $i;?>"   id="order<?php echo $i;?>"   class="updatelistOrder oder_listentry<?php echo $odd;?> testclassa"   value="<?php echo $datarow[$i]['serial_no'];?>"/>   
                             </td>
                             
                             <td class="th2_listentry">
                                 <input type="text" name="entry_<?php echo $i;?>"   id="entry<?php echo $i;?>"   class="updatelist entry_listentry<?php echo $odd;?>" autocomplete="off"  value="<?php echo $datarow[$i]['value'];?>" onkeyup="nameautofil('<?php echo $baseurl;?>','<?php echo $i;?>',event)" <?php if($i+1==count($datarow)){ echo "onfocus='addRowListEntry($i)'";}?>  />                                    
                                 <div class="listdiv_takelist" id="div<?php echo $i;?>" style="display: none;">
                                      
                                 </div>
                             </td>
                             
                             <td class="th3_listentry_comment">
                                 <input type="text" name="comment_<?php echo $i;?>" id="comment<?php echo $i;?>" class="comment_listentry<?php echo $odd;?>" autocomplete="off" value="<?php echo $datarow[$i]['rating'];?>" onfocus="hideautonameList('<?php echo $i;?>')" />   
                             </td>
                         </tr>
                         <?php } } if($count>10) {} else{ ?>
                         <?php for($i = $count;$i<10;$i++) { 
                                if($i%2 ==0 )
                                {
                                    $odd = "even";                                   
                                }
                                else
                                {
                                      $odd = "odd";
                                      
                                } 
                         ?>
                           <tr class='tr1_<?php echo $odd;?>'>
                                <td class="th1_listentry">   <input type="text" name="order_<?php echo $i;?>"   id="order<?php echo $i;?>"  value="<?php echo $i+1?>" class="updatelistOrder oder_listentry<?php echo $odd;?> testclassa" onblur="increaseValidation('<?php echo $i+1;?>')"/>   </td>
                                <td class="th2_listentry">
                                    <input type="text" name="entry_<?php echo $i;?>"   id="entry<?php echo $i;?>"   class="updatelist entry_listentry<?php echo $odd;?>" autocomplete="off" value=""  onkeyup="nameautofil_take('<?php echo $baseurl;?>','<?php echo $i;?>',event)" <?php if($i==9){ echo "onfocus='addRowListEntry($i)'";}?> />     
                                    <div class="listdiv_takelist" id="div<?php echo $i;?>" style="display: none;">
                                      
                                    </div>
                                </td>
                                <td class="th3_listentry_comment">   <input type="text" name="comment_<?php echo $i;?>" id="comment<?php echo $i;?>" autocomplete="off" class="comment_listentry<?php echo $odd;?>" value="" onfocus="hideautonameList('<?php echo $i;?>')"/>    </td>
                           </tr>
                         
                         <?php } } ?>
                         
                     </table>
                 </div>
             </div>
             
             <input type="hidden" name="" id="addrow" value="<?php if($count>10) { echo count($datarow)-1;} else{ echo "9";}?>"/>
             
             <input type="hidden" name="validation" id="validation" value="<?php echo count($datarow);?>"/>
              
             <input type="hidden" id="listvalue_hidden" name="listvalue_hidden" value="<?php echo $hidendata?>"/>
             
             <input type="hidden" id="no_of_rows" name="no_of_rows" value="<?php if($count>11) {  echo count($datarow);} else{ echo "10";}?>"/>
           <div class="floatright">
               <div id="opnerequred" style="display: none;" class="error_listentry">
                   This field is required
               </div>
                  <span onclick="javascript:history.go(-1);" class="cursorpointer">
                     <div class="canclespan_listtitle">cancel</div> 
                 </span>
                 <input type="submit" value="SUBMIT" name="submit" class="uploadimg_listEntry"/>
             </div>  

<?php $this->endWidget(); ?>
             
    </div>
</div>
</div>