<?php 
$baseurl = Yii::app()->request->baseUrl;
$path = Yii::app()->getRequest()->pathInfo;
$url1 = explode("/", $path);
$url  = $url1[0];
?>

<?php 
if (!$_ajax)
{    
?>
<input type="hidden" name="pageno_off" id="pageno_off" 
	value="1" />
<input type="hidden" name="baseurl" 
    value="<?php echo $baseurl?>" id="baseurl" />
<input type="hidden" name="serchheader_recentsearch"
	value="<?php echo $string;?>" id="serchheader_recentsearch" />
<input type="hidden" name="currenturl" id="currenturl"
	value="<?php echo $url1[1]?>" />

<script>
jQuery(document).ready(function() {    
    $('title').html('Wayo Search');
    
    setSearchValues();

    initPagination_old($("#_sr"), "<?php echo $baseurl;?>/index.php/preferences/recentSearchCompaniesPagination");

    $("#around").keypress(function(e) {
        if (e.keyCode == '13') {
            runon();
        }
    });
});

function runon() {
    var url = "<?php echo $baseurl;?>/index.php/preferences/recentSearchCompaniesPagination";
    ajaxSearch(url, "_sr", getSearchValues(), {
	    around: $("#around").val()
    });
}

function followCompany(event, id) {
    event.preventDefault();
    event.stopPropagation();
    
    $.ajax({ 
        type: 'POST',
        url: '<?php echo $baseurl;?>/index.php/preferences/followCompany',
        dataType: 'text',
        data: { company_id: id }, 
        success: function (resp) {
            // hide follow button
            $('#followBtn_' + id).hide();

            // show "Followed" text
            $('#companyItem_' + id).removeClass("status-follow");
            $('#companyItem_' + id).addClass("status-followed-alert");

            // show operation status
            $('#followStatus_' + id).show();
        }
    });
}

function hideFollowAlert(event, alertEl, id) {
    event.preventDefault();
    event.stopPropagation();
    
    $(alertEl).parent().hide();
    $('#companyItem_' + id).removeClass("status-followed-alert");
    $('#companyItem_' + id).addClass("status-followed");  
}

function openProfile(id) {
    window.location = "<?php echo $baseurl; ?>/index.php/user/myDashboard?userid=" + id;
}

</script>

<div id="pagingdiv" style="display: none;"></div>
<div class="featured_list_section_internal">
	<div class="official_today">
	
	    <div class="outer_reinesearch">
            <div id="searchByLocation" >
                <table class="advanced-search" style="width: inherit;">
                    <tr>
                        <td>Show those around&nbsp;</td>
                        <td><input id="around" type="text" value="<?php echo $around; ?>" placeholder="City, State, or Zip"/></td>
                        <td><div id="runon" class="blue-btn show-nearby" onclick="runon();">Show Nearby</div></td>
                    </tr>
                </table>                    
            </div>
        </div>
        <div class="clear"></div>
        
        <div id="_sr" class="search-list search-three-column">
	
<?php } //!_ajax ?>		

        <?php            
        if ($pageno == 0) 
        {            
        ?>
        <input id="searchListCount" type="hidden" value="<?php echo $_SESSION["searchListCount"]; ?>"/>
        <input id="searchPeopleCount" type="hidden" value="<?php echo $_SESSION["searchPeopleCount"]; ?>"/>
        <input id="searchCompaniesCount" type="hidden" value="<?php echo $_SESSION["searchCompaniesCount"]; ?>"/>
        <?php 
        }
        
		if (count($data) > 0) 
        {                       
            foreach ($data as $companies)
            {
                $photo = $baseurl . "/images/default.jpg";
                if ($companies["image"] != "") 
                {
                    $photo = $baseurl . "/images/profile/user_thumbs/dashboard/" . $companies["image"] . "?w=108&h=108";
                }
            ?>
            
            <div id="companyItem_<?php echo $companies['id']; ?>"
                class="people-item <?php if ($companies['is_friend']) echo 'status-followed'; else echo 'status-follow'; ?>">
                <?php if ($companies['badge']) {?>
                    <span class="list-box-badge-eagle-users fR" title="Wayo Official"></span>
                <?php }?>
                <img src="<?php echo $photo; ?>" onclick="openProfile(<?php echo $companies['id']; ?>);" />
                <div class="people-item-details" 
                     <?php if ($companies['badge']) {?>
                        title="Wayo Official"
                      <?php }?>
                      >
                    <div class="people-item-name">
                        <span>
                            <a href="javascript:openProfile(<?php echo $companies['id']; ?>);"><?php echo StringsHelper::capitalize(User::getUserShortName($companies, 16)); ?></a>
                        </span>
                    </div>
                    <div class="people-item-location">
                        <span><?php echo $companies['city'];?>, <?php echo $companies['state'];?></span>
                    </div>
                </div>
                                
                <?php if (!$companies['is_friend'] && $companies['id'] != $user->id)
                {  
                ?>        
                
                <div id="followBtn_<?php echo $companies['id']; ?>" class="companies-item-follow-button"
                    onclick="followCompany(event, '<?php echo $companies['id']; ?>');">   Follow                 
                </div>
  
                <div id="followStatus_<?php echo $companies['id']; ?>" class="companies-item-alert" style="display: none">
                    You're following this company
                    <div class="btn-close-popup" onclick="hideFollowAlert(event, this, '<?php echo $companies['id']; ?>');"></div>
                </div>
                
                
                
                <?php 
                }
                ?>
                
                <span class="followed">Followed</span>
                
                <div class="followed-shadow"></div>
                
                <div class="clear"></div>     
            </div>            
            
            <?php 
            }
            ?>
            
            <div class="clear"></div>
        <?php
        }
        else if (!$_ajax || $pageno == 0)
        {
        ?>
		    <div class="nolistfound">No companies found</div>	
		<?php 
        }
		?>
		
<?php 
if (!$_ajax)
{    
?>
        </div>
		<div class="officialtodatloding" id="officialtodatlodingdiv">
			<div class="officialtodatlodingimg"><img
				src="<?php echo Yii::app()->request->baseUrl;?>/images/loading.gif" />
		    </div>			
			<div class="officialtodatlodingtext">Loading...</div>
		</div>
	</div>
</div>
<?php } //!_ajax ?>
