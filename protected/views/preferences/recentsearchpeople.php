<?php 
$baseurl = Yii::app()->request->baseUrl;
$path = Yii::app()->getRequest()->pathInfo;
$url1 = explode("/", $path);
$url  = $url1[0];

$user = Yii::app()->user->getModel();

if (!$_ajax) {    
?>
<pre><?php 
	//var_dump($data); 
	?></pre>

<input type="hidden" name="hiddenpage" id="pageno_off" value="1" />
<input type="hidden" name="baseurl" value="<?php echo $baseurl?>" id="baseurl" />
<input type="hidden" value="<?php echo $string;?>" id="serchheader_recentsearch" />
<input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>" />

<script>
$(document).ready(function() {	
	$('title').html('Wayo Search');
	
	setSearchValues();

	initPagination_old($("#_sr"), "<?php echo $baseurl;?>/index.php/preferences/recentSearchPeoplePagination");
	
	$("#around").keypress(function(e) {
        if (e.keyCode == '13') {
            runon();
        }
    });

	$("#studied").keypress(function(e) {
        if (e.keyCode == '13') {
            runon();
        }
    });
	
});


function runon() {
	var url = "<?php echo $baseurl;?>/index.php/preferences/recentSearchPeoplePagination";
	ajaxSearch(url, "_sr", getSearchValues(), {
	    around: $("#around").val(),
        studied: $("#studied").val()
	});
}

function showInvitePopup(event, el, id) {
    event.preventDefault();
    event.stopPropagation();
    
	$me = $('#search-invite-popup');
	if ($me.css('display') === 'block') {
		$('html, body').animate({
			scrollTop: $me.offset().top - 100
		}, 100);
		
		$me.animate({opacity:'0.5'}, 200);
		$me.animate({opacity:'1.0'}, 100);
		$me.animate({opacity:'0.5'}, 100);
		$me.animate({opacity:'1.0'}, 100);
	} else {
		if ($me.parent()[0] !== document.body) {
			$(document.body).append($me.detach());
			$me.children('.btn-close-popup').click(function() { $(this).parent().hide(); });
		}
		//console.log($(el).offset());
		var pos = $(el).offset();
		pos.top += 18;
		pos.left += 18;
		$me.css(pos)
			.fadeIn()
			.children('.btn-send')
			.click(function() {
				inviteUser(id, $(el).parent()); 
			});
	}

	return false;
}

function hideInviteAlert(event, el) {
    event.preventDefault();
    event.stopPropagation();
    
	$(el).parent().parent().removeClass('status-pending-alert').addClass('status-pending');
}

function inviteUser(id, $itemEl) {
	$.ajax({ 
		type: 'POST',
		url: '<?php echo $baseurl;?>/index.php/preferences/invitePeople',
		dataType: 'text',
		data: {
			people_id: id,
			message: $('#search-invite-popup').children('textarea').val()
		}, 
		success: function (resp) {
			$('#search-invite-popup').hide();
			$itemEl.removeClass('status-invite').addClass('status-pending-alert');
		}
	});
}

function openProfile(id) {
    window.location = "<?php echo $baseurl;?>/index.php/user/myDashboard?userid=" + id;
}

</script>

<div id="pagingdiv" style="display: none;"></div>

<div class="featured_list_section_internal">
	<div class="official_today" id="container">
	
		<div class="outer_reinesearch">
			<div id="searchByLocationAndShool">
				<table class="advanced-search">
					<tr>
						<td>Show those who live around</td>
						<td><input id="around" type="text" value="<?php echo $around; ?>" placeholder="City, State, or Zip" /></td>							
						<td>or studied at</td>
						<td><input id="studied" type="text" value="<?php echo $studied; ?>" placeholder="School" /></td>
						<td><div id="runon" class="blue-btn search-table" onclick="runon();">Run On</div></td>
					</tr>
				</table>					
			</div>
		</div>
		<div class="clear"></div>
	
		<div id="_sr" class="search-list search-three-column">
			
<?php } //!_ajax ?>		
		
        <?php            
        if ($pageno == 0) {            
        ?>
        <input id="searchListCount" type="hidden" value="<?php echo $_SESSION["searchListCount"]; ?>"/>
        <input id="searchPeopleCount" type="hidden" value="<?php echo $_SESSION["searchPeopleCount"]; ?>"/>
        <input id="searchCompaniesCount" type="hidden" value="<?php echo $_SESSION["searchCompaniesCount"]; ?>"/>
        <?php 
        }
        
		if (count($data) > 0) 
        {             
            foreach ($data as $people) {
                $photo = $baseurl . '/images/default.jpg';
                if ($people['image'] != '') {
                    $photo = $baseurl . '/images/profile/user_thumbs/dashboard/' . $people['image'] . '?w=108&h=108';
                }
                
				$status = $people['is_friend'] != '0' 
						? ($user->isCompany() ? 'status-friend status-follower' : 'status-friend' )
						: ($people['is_pending'] != '0' ? 'status-pending' : 'status-invite');
                
				if (empty($people['city'])) {
					$address = $people['state'];
				} else if (empty($people['state'])) {
					$address = $people['city'];
				} else {
					$address = $people['city'] . ', ' . $people['state'];
				};
            ?>
            
			<div class="people-item <?php echo $status; ?>" >
                <?php if ($people['badge']) {?>
                    <span class="list-box-badge-eagle-users fR" title="Wayo Official"></span>
                <?php }?>
				<img src="<?php echo $photo ?>" onclick="openProfile(<?php echo $people['id']; ?>);" />
				<div class="people-item-details" 
                     <?php if ($people['badge']) {?>
                        title="Wayo Official"
                      <?php }?>
                      >
					<div class="people-item-name">
						<a href="javascript:openProfile(<?php echo $people['id'] ?>);">
                            <?php echo StringsHelper::capitalize(User::getUserShortName(array(
                                'firstname' => $people['firstname'], 
                                'lastname' => $people['lastname']
                            ), 16)); ?>
						</a>
					</div>
					<div class="people-item-location"><?php echo $address; ?></div>
				</div>
                <?php if (!$user->isCompany() && $people['is_friend'] == '1') {?>
                    <div class="people-friend-button2">
                        <span>Friend</span>
                    </div>
                <?php }?>
				<div class="people-item-invite-button <?php if ($user->isCompany()) { echo 'disabled'; } ?>" style="<?php echo $people['style']; ?>"
					<?php if ($user->isMember()) { ?>onclick="showInvitePopup(event, this, <?php echo $people['id']; ?>);"<?php } ?>><span>Add</span></div>
				<div class="people-item-alert">Your Invitation has been sent
					<div class="btn-close-popup" onclick="hideInviteAlert(event, this);"></div>
				</div>
			</div><!-- .people-item -->
            
            <?php 
            }
            ?>
            
            <div class="clear"></div>
        <?php
        } else if (!$_ajax || $pageno == 0) {
        ?>
			<div class="nolistfound">No people found</div>
		<?php 
        }
		?>
		
		
<?php 
if (!$_ajax) {    
?>
        </div><!-- #_sr -->
		
		<div class="officialtodatloding" id="officialtodatlodingdiv">
			<div class="officialtodatlodingimg"><img
				src="<?php echo Yii::app()->request->baseUrl;?>/images/loading.gif" />
			</div>			
			<div class="officialtodatlodingtext">Loading...</div>
		</div>
		

	</div><!-- .official_today -->
</div><!-- .featured_list_section_internal -->

<div class="invite-popup" id="search-invite-popup">
	<div class="btn-close-popup" onclick="hideInviteAlert(event, this);"></div>
	<textarea placeholder="Explain why you're inviting them (optional)."></textarea>
	<div class="btn-send"></div>
</div>

<?php } //!_ajax ?>

