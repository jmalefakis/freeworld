<?php
//$baseurl = Yii::app()->request->getBaseUrl(true);
$baseurl = Yii::app()->request->baseUrl;
?>

<script type="text/javascript" src="<?php echo $baseurl; ?>/js/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/jquery.getimagedata.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/moxie.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/plupload.dev.js"></script>

<script>
$(function(){
	$('title').html('Take List');
	
	$(window).keydown(function(event){
		if(event.keyCode == 1111113) {
			event.preventDefault();
			return false;
		}
	});
	
	$(document).mouseup(function (e)
	{
		var container = $(".listdiv_takelist");

		if (container.has(e.target).length === 0) {
			container.hide();
		}
	});
}); //ready


function opinionFocus(that) {
	var $that = $(that),
		$row  = $that.closest('.list-opinion'),
		$main = $row.closest('.list-opinions'),
		$rows = $main.find('tr');
		data  = $row.data(),
		count = $rows.size();
	
	return;
}


function opinionBlur(that) {
	var $that = $(that),
		$row  = $that.closest('.list-opinion'),
		$main = $row.closest('.list-opinions'),
		$rows = $main.find('tr');
		data  = $row.data(),
		count = $rows.size();
		
	//alert($row[0].rowIndex);
	return;
}


function opinionRowTemplate(i, position) {
	var s = '<tr class="list-opinion" data-index="' + i + '">'
		+ '<td class="list-opinion-position">'
		+ '<input type="hidden" name="opinions[' + i + '][id]" value="" />'
		+ '<input type="text" autocomplete="off" name="opinions[' + i + '][position]" onfocus="opinionFocus(this)" value="' + position + '" />' 
		+ '</td>'
		+ '<td class="list-opinion-image">'
		+ '<input type="hidden" name="opinions[' + i + '][image]" value="" />'
		+ '<div id="listImageTarget' + i + '" class="conteuner-photo" style="display: none;"></div>'
		+ '<div id="listImageUploader' + i + '" class="uploader-wrapper"></div>'
		+ '</td>'
		+ '<td class="list-opinion-title">'
		+ '<input type="text" autocomplete="off" name="opinions[' + i + '][value]" value="" onfocus="opinionFocus(this)" onblur="opinionBlur(this)" onkeyup="opinionKeyUp(this)" />'
		+ '</td>'
		+ '</tr>';
	return s;
}



function opinionKeyUp(that) {
	var $that = $(that),
		$row  = $that.closest('.list-opinion'),
		$main = $row.closest('.list-opinions').find('tbody'),
		$rows = $main.find('.list-opinion');
		data  = $row.data(),
		val   = $.trim($that.val());
		
	if (val !== '') {
		if (data.index === $rows.size() - 1) {
			var pos = parseInt($row.find('.list-opinion-position input[type="text"]').val());
			//console.log($rows.last().find('.list-opinion-position input[type="text"]').val());
			//console.log($row.find('.list-opinion-position input[type="text"]').val());
			$main.append(opinionRowTemplate(data.index + 1, pos + 1));
		}

		if (($row.find('.uploader-wrapper').children().size() === 0) && (!data.uploaderGetting)) {
			$row.data('uploaderGetting', true);
			$.ajax({
				type: 'POST',
				url: '<?php echo $baseurl; ?>/index.php/preferences/multiFileUpload',
				dataType: 'html',
				data: {
					cfg: {
						uploadContainerId: 'listImageContainer' + data.index,
						uploaderId: 'listImageUploader' + data.index,
						uploadUrl: '<?php echo $baseurl; ?>/index.php/preferences/uploadImages',
						deleteUrl: '<?php echo $baseurl; ?>/index.php/preferences/deleteImage',
						uploadButtonClientId: 'fileUpload' + data.index,
						//responseTargetId: 'listImageTarget' + data.index,
						dragAndDropText: 'Drop a picture or <br/><a href="#" id="fileUpload' + data.index + '" class="ctrl-upload-link">click to upload</a>',
						onClientResponseReceived: 'opinionClientResponseReceived'
					}
				}, 
				success: function(resp) {
					$row.data('uploaderGetting', false);
					$('#listImageUploader' + data.index).html(resp);
				},
				error: function(error) {
				}
			});
		}
		
	} else {
		$nextRow = $($rows.get(data.index + 1));
		if ($nextRow.find('.list-opinion-title input').val() === '') {
			$nextRow.remove();
		};
	}
}


function opinionClientResponseReceived(file, respObj, uploaderId, targetId) {
	//console.log('resp = ', respObj.response);
	var s =   '<img src="<?php echo $baseurl; ?>/images/titleimages/' + respObj.response + '?w=310&amp;h=200" class="small-photo" onclick="opinionShowBigImage(this);">'
		+ '<div class="conteuner-photo-div">'
		+ '<img src="<?php echo $baseurl; ?>/images/titleimages/' + respObj.response + '?w=310&amp;h=200" alt="Small Photo" class="big-photo"/>'
		+ '<div class="close-x" onclick="opinionHideBigImage(this);"></div><div class="trash" onclick="opinionDeleteImage(this);"></div></div>';

	var $wrapper = $('#' + uploaderId).hide().closest('.list-opinion-image');
    $(' > input',$wrapper).val(respObj.response);
    $('.conteuner-photo',$wrapper).append(s).show();
}
//z.match(/[^\[\]]+/g)

function opinionShowBigImage(that) {
	$(that).closest('.list-opinion-image').css('z-index', 2000).find('.conteuner-photo-div').addClass('hidden');
	return false;
}

function opinionHideBigImage(that) {
	$(that).closest('.list-opinion-image').css('z-index', 1).find('.conteuner-photo-div').removeClass('hidden');
	return false;
}

function opinionDeleteImage(that) {
	var $wrapper = $(that).closest('.list-opinion-image');
	$wrapper.find('input').val();
	//$wrapper.find('.conteuner-photo').hide().find('.conteuner-photo-div').removeClass('hidden');
	$wrapper.find('.conteuner-photo').empty().hide();
	$wrapper.find('.uploader-wrapper').show();
	return false;
}
</script>

<?php

$userid	= Yii::app()->user->getID();
$path =	Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url = $url1[0];
$siteurl = Yii::app()->params['siteurl'];
?>
<input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>"/>
<input type="hidden" id ="hidden_baseurl" value="<?php echo $baseurl ?>"/>

<div class="featured_list_section_internal">
<div class="widthauto">
	<?php //$this->renderPartial('sidenavigation'); ?>
	<div class="outerdiv" id="liscreation">
 <?php
$form=$this->beginWidget('CActiveForm', array(
	'id' => 'listcreate',
	'action' => $siteurl.'/index.php/preferences/updateOpinions?listid='.$list->id,
	'method' => 'post',
	'enableAjaxValidation' => true,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'name' => 'listcreate',
		//'onsubmit' => 'return list_createEntry();',
		'onsubmit' => 'return true;',
		'validateOnSubmit' => true,
	),
));
		
?>
		
		<div class="leftdiv_listentry">
			<div class="content_part_left">
				<span><?php echo $list->title; ?></span>
			</div>
			<div class="image_outer_listtitle_new">
				<img class="listdefaultimg_new" src="<?php echo $baseurl . $list->getImageUrl(); ?>" />
			</div>
			<div class="listcreated_listentry">
				<span class="createdspan_listentry">Created by:&nbsp;&nbsp;</span><span class="createname_listentry"><?php echo $list->users->getFullName(); ?></span><br/>
				<span class="createdspan_listentry">On:&nbsp;&nbsp;</span><span class="createname_listentry"><?php echo date('m/d/Y', strtotime($list->modified)); ?></span><br/>
				<span class="createdspan_listentry">Contributors:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo $list->countTaken(); ?></span><br/>
				<span class="createdspan_listentry">Topic:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo $list->interest->interest; ?></span><br/>
				<div class="instriuctiondiv_list">
					<span class="createdspan_listentry">Instructions:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo $list->instruction; ?></span><br/>
				</div>
			</div>
		</div><!-- .leftdiv_listentry -->
		
		<div class="rightdiv_listentry">
			<div class="contentpart_right">
				<span>Please provide entries</span>
				<span class="whatother_listentry" id="secret-why1">Check what others think</span>
				
				<div id="popupContact1" > 
					<div class="arrowimagetakelist">
						<img src="<?php echo $baseurl?>/images/latest_img/drop.png"/>
					</div>
					<div id="popupCont1">
								<div id="messge_mergine" class="notification_mergine">
									<?php 
									if (count($other_opinions) > 0) {
										foreach($other_opinions as $opinion) { 
									?>
									<div class="rowvalue_takelist">
										<?php
											echo (strlen($opinion->value) < 90 ? $opinion->value : substr($opinion->value, 0, 90) . '...');
										?>
									</div>
									<?php }
									} else { ?>
									<div class="rowvalue_takelist">
										You are first to take this list
									</div>
									<?php } ?>
									<div class="notediv_takelist">
										<b><span>Note:</span></b>
										<span> This is just a sample input provided by the other users.The order does not reflect the actual rating </span>
									</div>
								</div>
					</div><!-- .popupCont1 -->

				</div><!-- .popupContact1 -->
				<div id="backgroundPopup1"></div>
			</div><!-- .contentpart_right -->
			
			<table class="list-opinions box-sizing">
				<thead>
				<tr class="tr1_thlistentry">
					<th class="list-opinion-counter"><div style="position: relative;width: 40px;">#</div></th>
					<th class="list-opinion-image">Picture</th>
					<th class="list-opinion-text">My Opinion</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$position = 0;
				$opinions_count = count($opinions);
				//$opinion_default = array('id' => '', 'position' => '', 'value' => '');
				$n = $opinions_count < 4 ? 4 : $opinions_count + 1;
				for($i = 0; $i < $n; $i++) {
					$opinion = $i < $opinions_count ? $opinions[$i] : false;
					$has_image = ($opinion && $opinion->image !== '') ? true : false;
					$position = $opinion ? $opinion->position : $position + 1;
				?>
				<tr class="list-opinion" data-index="<?php echo $i;?>">
					<td class="list-opinion-position">
						<input type="hidden" name="opinions[<?php echo $i; ?>][id]" value="<?php if($opinion) echo $opinion->id; ?>" /> 
						<input type="text" autocomplete="off"
							name="opinions[<?php echo $i; ?>][position]"
							value="<?php echo $position; ?>"
							onfocus="opinionFocus(this)" /> 
					</td>
					
					<td class="list-opinion-image">
						<input type="hidden" name="opinions[<?php echo $i; ?>][image]" value="<?php if($opinion) echo $opinion->image; ?>" /> 
						<div id="listImageTarget<?php echo $i; ?>" class="conteuner-photo"<?php if(!$has_image) echo ' style="display: none;"'; ?>>
							<?php if ($has_image) { ?>
							<img src="<?php echo $opinion->getImageUrl(); ?>" class="small-photo" onclick="opinionShowBigImage(this);">
							<div class="conteuner-photo-div">
								<img src="<?php echo $opinion->getImageUrl(); ?>" alt="Small Photo" class="big-photo">
								<div class="close-x" onclick="opinionHideBigImage(this);"></div>
								<div class="trash" onclick="opinionDeleteImage(this);"></div>
							</div>
							<?php } ?>
						</div>
						
						<div id="listImageUploader<?php echo $i; ?>" class="uploader-wrapper"<?php if($has_image) echo ' style="display: none;"'; ?>>
						<?php
							$this->renderPartial('/controls/multifileupload', array(
								'uploaderId' => 'listImageUploader' . $i,
								'uploadUrl' => $baseurl . '/index.php/preferences/uploadImages',
								'deleteUrl' => $baseurl . '/index.php/preferences/deleteImage',
								'uploadButtonClientId' => 'fileUpload' . $i,
								//'responseTargetId' => 'listImageTarget' . $i,
								'dragAndDropText' => 'Drop a picture or <br/><a href="#" id="fileUpload' . $i . '" class="ctrl-upload-link">click to upload</a>',
								'onClientResponseReceived' => 'opinionClientResponseReceived'
							));
						?>
						</div>
					</td>
					
					<td class="list-opinion-title">
						<input type="text" autocomplete="off"
							name="opinions[<?php echo $i;?>][value]" 
							value="<?php if($opinion) echo $opinion->value; ?>"
							onfocus="opinionFocus(this)" 
							onkeyup="opinionKeyUp(this)"
							onblur="opinionBlur(this)"/> 
					</td>
				</tr>
				<?php
				}
				?>
				</tbody>
			</table>
		</div><!-- .rightdiv_listentry -->
			
				<input type="hidden" name="" id="addrow" value="9"/>
				<input type="hidden" name="validation" id="validation" value="1"/>
				<input type="hidden" id="listvalue_hidden" name="listvalue_hidden" value=""/>
			
			<div class="floatright">
				<div id="opnerequred" style="display: none;" class="error_listentry">
					This field is required
				</div>
				<span onclick="javascript:history.go(-1);" class="cursorpointer">
				<div class="canclespan_listtitle">Cancel</div> 
				</span>
				<input type="submit" value="submit" name="submit" class="uploadimg_listEntry"/>
			</div>	

<?php $this->endWidget(); ?>
			
	</div><!-- .outerdiv -->
</div><!-- .widthauto -->
</div><!-- .featured_list_section_internal -->