<?php
if (!empty($_SESSION['lastItem'])) {
    //echo "<pre>" . print_r($_SESSION['lastItem'], true) . "</pre>";
}
$baseurl = Yii::app()->request->baseUrl;

$userid = Yii::app()->user->getID();   
$user = User::model()->findByPk($userid);

$path = Yii::app()->getRequest()->pathInfo; 

$url1 = explode("/", $path);
$currenturl  = $url1[1];

if (!$_ajax)
{
     $_SESSION['lastItemI'] = 1;
?>

<input type="hidden" name="pageno_off" id="pageno_off"
	value="1" />
<input type="hidden" name="baseurl"
	id="baseurl" value="<?php echo $baseurl?>" />
<input type="hidden" name="currenturl"
	id="currenturl" value="<?php echo $url1[1]?>" />

<script>
$(function() {	
	$('title').html('<?php echo $listPageTitle; ?>');

	initPagination_old($("#contentdiv"), 
	        "<?php echo $baseurl;?>/index.php/preferences/<?php echo $listPage; ?>Pagination");	
	
}); //ready
</script>

<div class="featured_list_section">

	<div id="wrapper">
		<div class="sort_section">
			<div class="foundlist_recom"
			<?php if ($listsCount == 0){echo "style='display:none;'";}?>
				id="foundlistdiv">
				<span id="foundlistid" style="font-family: ArchivoNarrow-Regular !important;">Found <?php echo $listsCount;?>
					<?php if ($listsCount > 1) { ?>lists<?php } else {?>list <?php } ?>
				</span>
			</div>
		</div>
		<div id="mainContent">
			<div class="content" id="contentdiv">
<?php 
} //!_ajax
?>
			
			<?php
            $lastItem = '';
			if (count($lists) > 0) 
			{
                $j = 1;
                if (!empty($_SESSION['lastItem']) && $_SESSION['lastItem'] == 'right') {
                    $odd = 'odd';
                } else if (!empty($_SESSION['lastItem']) && $_SESSION['lastItem'] == 'left') {
                    $odd = 'even';
                } else {
                    $odd = 'odd';
                }
                
                foreach ($lists as $list) 
                {
                    $user_id = $list['user_id'];
					$title = $list['title'];
					$image = $list['image'];
					$listid	= $list['id'];
                    
                    $toLeft = 0;
                    if (!empty($list['toLeft'])) {
                        $toLeft = 1;
                    }
						
					$rows_html = '';						
					$rows = $list->opinionRaiting();						
					foreach($rows as $row_value) {
						$rows_html .= '<li><div>' . $row_value['value'] . '</div></li>';
					}
					$rows_html = '<ol class="list-box-rows ' . ($j % 2 !== 0 ? 'left-side' : 'right-side') . '">' . $rows_html . '</ol>';
					
				   
				    /*$lengthtitle = strlen($title) ;
				    if ($lengthtitle > 50)
				    {
					    $title = substr($title,0,50) . "...";  
				    } */
				   
				    $userName  = $list->users();
				    $friendLabel = "Friend";
				    $listtakencount = 0;
				   
				    if ($user->isCompany()) 
                    {
                        $friendLabel = "Follower";
                    
                        $c = new CDbCriteria();
                        $c->addInCondition('user_id', FriendHelper::getMyFollowersIds());
                        $listtakencount = $list->countTaken($c);
                    } 
                    elseif ($friendsserch) 
                    {
				       $c = new CDbCriteria();
				       $c->addInCondition('user_id', $friendsserch);
				       $listtakencount = $list->countTaken($c);
				    }
				   		
				    $totalListTaken = $list->countTaken();
				   
				    $myCriteria = new CDbCriteria();
				    $myCriteria->addCondition('user_id = :uid');
				    $myCriteria->params = array(':uid' => $userid);	
				    $isTakenByMe = $list->countTaken($myCriteria);
				   
				    $isFavoriteByMe = false;
				    if ($isTakenByMe) 
                    {
                        $myCriteria->addCondition("favorite > 0");
                        $isFavoriteByMe = $list->countTaken($myCriteria);
                    }
		   ?>

				<?php if($odd == 'odd'){ $_SESSION['lastItemI']++;?>
				<div class="part_left"
				    data-subject-type="list"
					data-id="<?php echo $listid; ?>" 
					data-is-taken="<?php echo $isTakenByMe ? "true" : "false"; ?>"
					data-is-favorite="<?php echo $isFavoriteByMe ? "true" : "false"; ?>"
                    <?php if ($_SESSION['lastItemI']%2 == 0) {?>
                    style="float:left; margin-left:8px; margin-right:8px;"
                    <?php } else {?>
                    style="float:right;"
                    <?php }?>
                    >
					
					<div class="list-box-alert-panel">Remove this list from your Dashboard?
						<div class="btn-close-red"  onclick="javascript:$(this).parent().fadeOut(500);"></div>
						<div class="btn-gray-small" onclick="javascript:$(this).parent().fadeOut(500);">NO</div>
						<div class="btn-gray-small" onclick="javascript:removeListFromFavorite(this);">YES</div>
					</div>
					
					<div class="big_img_part">
						<div class="nightife">
							<a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $listid;?>" class="nightife2_litle fL size"><?php echo StringsHelper::capitalize($title);?></a>
                            <?php if ($list['participation'] != 'Everybody') {?>
                                <div class="list-box-private-check fR" title="Private List"></div>                      
                            <?php } else if (!empty($list['badge'])) { ?>
                                <div class="list-box-badge-eagle fR" title="Wayo Official"></div>
                            <?php } ?>
                            <div class="clear"></div>
						</div>
						<div class="big_img captionTop4">
							<a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $listid;?>">
								<img class="officialtodayimg"
								    src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/official/big/' . $image . '?w=548&h=304'; ?>"
								    alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" />
								<?php if ($list && count($rows) > 0) { echo $rows_html; } ?>
							</a>
                            <?php if (!empty($list['link_title'])) {?>
                            <div class="caption4">
                                <div class="captionInner4">
                                    <a href="<?=!empty($list['link']) ? $list['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list['link_title'])?></a>
                                </div>
                            </div>
                            <?php }?>
						</div>
					</div>
					<div class="bottomimgleft">
						<div class="alice">
							<a class="alisename" href="javascript:void(0)"
								onclick="listAuthorClick(this, <?php echo $userName['id']; ?>, '#wrapper')">
								<?php $lengthname = strlen($userName['firstname'].' '.$userName['lastname']) ; if($lengthname<16) { echo StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);} else { echo StringsHelper::capitalize($userName['firstname']); }?>
							</a>
						</div>

						<div class="block-hidden"></div>

						<div class="list-box-star tooltip-pointer-simple"
							onclick="javascript:toggleFavorite(this);"></div>

					    <?php 
					    if ($isTakenByMe)
					    {
                        ?>
					    <div class="icon-list tooltip-pointer-list"></div>
					    <?php
					    } 
                        ?>

						<div class="alice_right">
							<div class="contri" style="float: right; padding: 0 8px 0 24px; white-space: nowrap;">
								<span class="integercolor tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"><?php echo $totalListTaken;?> </span>
								<span class="contrspan tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"> In </span>
								<span class="integercolor tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $listtakencount;?> </span>
								<span class="contrspan tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $friendLabel; if($listtakencount != 1) { ?>s<?php } ?>
								</span>
							</div>
						</div>
					</div>
				</div>
				<?php  /*$odd = 'even'; if($j!=0){ 
                                        if($odd=='even' && $var == 0){$odd='odd';$var=$var+1;} 
                                        else{$var=0;$odd='even';} } 
                                       else  {
                                           if(!empty($countPrev) && $odd=='even' && $var == 0 && count($countPrev)%2 != 0){$odd='odd';$var=$var+1;}
                                       }*/
                        $odd='even';
                        $lastItem = 'left';
				   }else { 
                       $odd='odd';
                       $lastItem = 'right';
				   ?>

				<div class="part_right custom-right-part" 
				    data-subject-type="list"
					data-id="<?php echo $listid; ?>" 
					data-is-taken="<?php echo $isTakenByMe ? "true" : "false"; ?>"
					data-is-favorite="<?php echo $isFavoriteByMe ? "true" : "false"; ?>"
                    <?php if ($_SESSION['lastItemI']%2 == 0) {?>
                    style="float:right;"
                    <?php } else {?>
                    style="float:left; margin-left:8px; margin-right:8px;"
                    <?php }?>
                    >
					
					<div class="list-box-alert-panel">Remove this list from your Dashboard?
						<div class="btn-close-red"  onclick="javascript:$(this).parent().fadeOut(500);"></div>
						<div class="btn-gray-small" onclick="javascript:$(this).parent().fadeOut(500);">NO</div>
						<div class="btn-gray-small" onclick="javascript:removeListFromFavorite(this);">YES</div>
					</div>

					<div class="nightife2">
						<a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $listid;?>" class="nightife2_litle fL size"><?php echo StringsHelper::capitalize($title);?></a>
                        <?php if ($list['participation'] != 'Everybody') {?>
                                <div class="list-box-private-check fR" title="Private List"></div>
                        <?php } else if (!empty($list['badge'])) { ?>
                            <div class="list-box-badge-eagle fR" title="Wayo Official"></div>
                        <?php } ?>
                        <div class="clear"></div>
					</div>
					<div class="image_small captionTop5">
						<a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $listid;?>">							
							<img class="officialtodayimg_small"
							src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/official/small/' . $image . '?w=351&h=306'; ?>"
							alt="<?php echo StringsHelper::capitalize($title);?>" title="<?php echo StringsHelper::capitalize($title);?>" />
							<?php if ($list && count($rows) > 0) { echo $rows_html; } ?>
						</a>
                        <?php if (!empty($list['link_title'])) {?>
                        <div class="caption5">
                            <div class="captionInner5">
                                <a href="<?=!empty($list['link']) ? $list['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list['link_title'])?></a>
                            </div>
                        </div>
                        <?php }?>
					</div>
					<div class="bottom2">
						<div class="alice">
							<a class="alisename" href="javascript:void(0)"
								onclick="listAuthorClick(this, <?php echo $userName['id']; ?>, '#wrapper')">
								<?php $lengthname = strlen($userName['firstname'].' '.$userName['lastname']) ; if($lengthname<16) { echo StringsHelper::capitalize($userName['firstname'].' '.$userName['lastname']);} else { echo StringsHelper::capitalize($userName['firstname']); }?>
							</a>
						</div>

						<div class="block-hidden"></div>

						<div class="list-box-star tooltip-pointer-simple"
							onclick="javascript:toggleFavorite(this);"></div>

						<?php 
					   if ($isTakenByMe)
					   {
                       ?>
						<div class="icon-list tooltip-pointer-list"></div>
						<?php
					   } 
                       ?>

						<div class="alice_right2">
							<div class="contri2" style="float: right; padding: 0 8px 0 24px; white-space: nowrap;">
								<span class="integercolor tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"><?php echo $totalListTaken;?> </span>
								<span class="contrspan tooltip-pointer-in<?=!empty($isTakenByMe) ? '-t' : ''?>"> In </span>
								<span class="integercolor tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $listtakencount;?> </span>
								<span class="contrspan tooltip-pointer-friends<?=!empty($isTakenByMe) ? '-t' : ''?>"> <?php echo $friendLabel; if($listtakencount != 1) { ?>s<?php } ?>
								</span>
							</div>

						</div>
					</div>
				</div>
				<?php  /*if($odd=='odd' && $var == 0){$odd='even';$var=$var+1;} else{$var=0;$odd='odd';}*/  } 
                ?>


				<?php $j++;}} elseif (!$_ajax || $pageno == 0) { ?>
				<div class="nolistfound">No lists found</div>
				<?php } 
                $_SESSION['lastItem'] = $lastItem;
                ?>
<?php				
if (!$_ajax)
{
?>
				
			</div>
			<!-- #contentdiv -->
		</div>
		<!-- #mainContent -->


		<div class="officialtodatloding" id="officialtodatlodingdiv">
			<img
				src="<?php echo Yii::app()->request->baseUrl;?>/images/loading.gif" />
		</div>

	</div>
	<div style="clear: both;">&nbsp;</div>

<script type="text/javascript">
    setTimeout(function() {
    	$('ol.list-box-rows li').each(function() {
    		var h = $(this).height() + 1,
    			$ch = $(this).children();
    		while ($ch.outerHeight() > h) {
    			$ch.text(function (index, text) {
    				return text.replace(/\W*\s(\S)*$/, '...');
    			});
    		}
    	});
    });
</script>

<?php 
} //!_ajax
?>
