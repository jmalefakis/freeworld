<?php
    $baseurl = Yii::app()->request->baseUrl;
?>
<div class="featured_list_section_internal">
	<div class="outerdiv" style="text-align:center; min-height:550px;">
        <div class="result-title size"> Please make a selection </div>
        <div class="clear"></div><br /><br /><br />
        <div class="list-access-block">
            <a class="createnewanchor" title="Take and View" href="<?=$baseurl?>/index.php/preferences/viewResult?listid=<?=$listid?>&take=1">
            <span class="create_new">Take and View</span>
        </a>
        <a class="createnewanchor" title="View" href="<?=$baseurl?>/index.php/preferences/viewResult?listid=<?=$listid?>&take=0">
            <span class="create_new">View</span>
        </a>
        </div>    
    </div>
</div>        