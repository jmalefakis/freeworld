<img src="<?php echo Yii::app()->params['siteurl'].$gallery[0];?>?w=158&h=158&nospace=1" style="display:none;" />
<?php 
if (!isset($showFilter)) $showFilter = false;

$friendCount = $takenFriends ? count($takenFriends) : 0;

$img = explode('.',$list->image);
$ext = '';
if (stripos($list->image,'.')) {
    $ext = '.jpg';
}
$dtext = 'Wayo is your personalized search and share engine, connecting you to your friends through your favorite things.';
if (!empty($list->link_title)) {
    $dtext .= ' Photo Credit: '.StringsHelper::capitalize($list->link_title);
}
$dtext;
Yii::app()->clientScript->registerMetaTag('Wayo - '.StringsHelper::capitalize($list->title),'og:title');
Yii::app()->clientScript->registerMetaTag($dtext,'og:description');
Yii::app()->clientScript->registerMetaTag(Yii::app()->params['siteurl'].'/img/cache/158x158/'.str_replace(array(' ','(',')'),'_',$img[0]).$ext,'og:image');
if (!empty($take)) {
    Yii::app()->clientScript->registerMetaTag(Yii::app()->params['siteurl'].'/index.php/preferences/takelist?listid='.$list->id,'og:url');
} else {
    Yii::app()->clientScript->registerMetaTag(Yii::app()->params['siteurl'].'/index.php/preferences/viewResult?listid='.$list->id,'og:url');   
}
?>
<div class="titleblock">
	<div class="block-right-info" 
	    data-subject-type="list"
		data-id="<?php echo $list->id; ?>"
		data-is-taken="<?php echo (count($takenList) > 0 ? 'true' : 'false'); ?>"
		data-is-favorite="<?php echo (count($takenList) > 0 && intval($takenList->favorite) > 0 ? 'true' : 'false'); ?>">
		<?php if (!empty($list->participation) && $list->participation != 'Everybody') {?>
            <div class="list-box-private-check fR" title="Private List"></div>
        <?php } else if ($list->badge) {?>
            <span class="list-box-badge-eagle fR" title="Wayo Official"></span>
        <?php }?>
        
        <span class="list-box-star res tooltip-pointer-simple"
			onclick="javascript:toggleFavorite(this);"></span>
		<!-- <span class="icon-list res"></span> -->
		<span class="list-box-friends friends-num"><i><?php echo $friendCount ?>
		</i> <?php echo $user->isMember() ? "Friend" : "Follower"; if ($friendCount != 1) { ?>s<?php } ?>
			<?php if ($friendCount) { ?>
			<div class="list-box-friends-drop-down">
			<ul >
				<?php foreach($takenFriends as $friend) { ?>
				<li><?php if ($friend['image'] != '') 
				{
				    ?> <img alt="User Photo" class="small-photo"
					src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $friend['image'];?>?w=27&h=23" 
					onclick="showUserBriefs(<?php echo $friend['id']; ?>, '#wrapper');" />
					<?php 
				}
				else
				{
				    ?> <img alt="User Photo" class="small-photo"
					src="<?php echo $baseurl;?>/images/default.jpg"
					onclick="showUserBriefs(<?php echo $friend['id']; ?>, '#wrapper');" />
					<?php 
				}
				?> <a
					href="javascript:showUserBriefs(<?php echo $friend['id']; ?>, '#wrapper');">
						<?php echo $friend->getFullName(); ?>
				</a></li>
				<?php } ?>
			</ul> </div><?php } ?> </span> <span class="contributors-num"><i><?php echo $list->countTaken(); ?>
		</i> In</span> <span class="user-name" onclick="showUserBriefs(<?php echo $list->user_id; ?>, '#wrapper');"><?php echo $list->users->getFullName(); ?>
		</span>
	</div>
	<div class="block-left-info">
		<div class="result-title size">
			<?php echo StringsHelper::capitalize($list->title); ?>
		</div>
	</div>
</div>

<div class="infoblock">
    
    <script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.sendInvite', function(e) {
                $('#sendInviteHide').val($("#sendInvite").data("ids").join(","));
                $.ajax({ 
                    type: 'POST',
                    url: '<?php echo $baseurl;?>/index.php/preferences/inviteList',
                    /*dataType: 'text',*/
                    data: {
                        listId: $(this).data("list"),
                        friends: $('#sendInviteHide').val()
                    }, 
                    success: function (resp) {
                        if (resp == '1') {
                            $('#sendInviteDiv').hide();
                            $('.sendInvite').hide();
                            $('#sendInviteMsg').show();
                        }
                        //$('#search-invite-popup').hide();
                        //$itemEl.removeClass('status-invite').addClass('status-pending-alert');
                    }
                });
            });
     });       
    </script>       
            
	<?php if($showFilter) { ?>		
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/multiple-select.css" />
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.multiple.select.js"></script>
	<script type="text/javascript">
        function fixFilter() {
        	if ($('input[name="selectItem"]:checked').length == 0 
        		    && $('input[name="selectGroup"]:checked').length == 0) {
        	    $('input[name="selectItem"]').first().prop('checked', true);
            }
            
            var filterText = $('button.ms-choice span').text();
            //console.log(filterText);
            $('button.ms-choice span').text(filterText.replace('Show all,', ''));

            if (filterText.trim().length == 0) {
            	$('button.ms-choice span').text('Show all');
            	$('button.ms-choice span').removeClass('placeholder');
            }
        }
	
        $(document).ready(function() {
            $('#listResultsFilter').multipleSelect({
                 width: 280,
                 selectAll: false,
                 filter: true,
                 onClose: function() {
                     var selectedIds = $('#listResultsFilter').multipleSelect('getSelects');
                     if (selectedIds[0] == "all") {
                         selectedIds.splice(0, 1);
                     }
                     opinionUpdateResults(selectedIds);
                 },
                 onClick: function(el) {
                     if (el.value == "all" && el.checked) {
                         $('input[name="selectItem"]').prop('checked', false);
                         $('input[name="selectItem"]').first().prop('checked', true);
                     } else if (el.value != "all" && el.checked) {
                         $('input[name="selectItem"]').first().prop('checked', false);
                     }

                     fixFilter();
                 },
                 onOptgroupClick: function(el) {
                     if (el.checked) {
                         $('input[name="selectItem"]').first().prop('checked', false);
                     }

                     fixFilter();
                 }
            });

            $('li label input[data-group="group_2"]').parent().parent().addClass("filter-group-item");
        });
	</script>
	
	<div class="block-right-info-content">
		<div class="friend-block">
			<select multiple="multiple" id="listResultsFilter" class="friend-input2" name="listResultsFilter" style="width: 280px">
			    <option selected="selected" value="all">Show all</option>
			    <option value="<?php echo $user->id; ?>">Show me</option>			    
			    <?php if ($friendCount) { ?>
			    <optgroup value="frined" label="Show my <?php echo (Yii::app()->user->getModel()->isCompany() ? 'followers' : 'friends'); ?>">   
                    <?php
    				if ($takenFriends) foreach($takenFriends as $friend)
                    {
    				?>    				
    			    <option value="<?php echo $friend->id; ?>">
    				    <?php echo StringsHelper::capitalize($friend->getFullName()); ?>
    			    </option>
    				<?php
    				}
    				?>
				</optgroup>
				<?php } ?>
			</select>			
		</div>
        
	<?php } else { ?>
	<div class="block-right-info-content">
		<div class="whatother_listentry" id="secret-why1">Check what others think</div>
		
				<?php if (isset($other_opinions)) { ?>
				<div class="contentpart_right">					
					<div id="popupContact1" class="popup7"> 
						<div class="arrowimagetakelist">
							<img src="<?php echo $baseurl?>/images/latest_img/drop.png"/>
						</div>
						<div id="popupCont1">
									<div id="messge_mergine" class="notification_mergine">
										<?php 
										if (count($other_opinions) > 0) {
											foreach($other_opinions as $opinion) { 
										?>
										<div class="rowvalue_takelist">
											<?php echo (strlen($opinion->value) < 90 ? StringsHelper::capitalize($opinion->value) : StringsHelper::capitalize(substr($opinion->value), 0, 90) . '...'); ?>
										</div>
										<?php }
										} else { ?>
										<div class="rowvalue_takelist">
											You are first to take this list
										</div>
										<?php } ?>
										<div class="notediv_takelist">
											<b><span>Note:</span></b>
											<span> This is just a sample input provided by the other users.The order does not reflect the actual rating </span>
										</div>
									</div>
						</div><!-- .popupCont1 -->

					</div><!-- .popupContact1 -->
					<div id="backgroundPopup1"></div>
				</div><!-- .contentpart_right -->
				<?php } ?>
				
				
	<?php } ?>
    
    <?php if ($user->isMember() && $list->participation == 'Everybody') { ?>
     <?php if(!empty($showFilter)) { ?>	
        <br /><br />        
     <?php }?>           
     <div class="ctrl-box">
        <div class="ctrl-wrapper" id="sendInviteDiv">
            <input tabindex="10" type="text" id="sendInvite" name="sendInvite" 
                placeholder="Enter specific friends who can take (be invited)"
                style="height: 32px;"/>
            <input type="hidden" name="sendInviteHide" id="sendInviteHide" value="" />
        </div><!-- .ctrl-wrapper -->

        <?php 
            $this->renderPartial("/controls/autocomplete", array(
                'searchUrl' => $baseurl . '/index.php/user/friendsWithoutList?listId='.$list->id,
                'targetDivId' => "sendInviteDiv",
                'minLength' => 1,
                'data' => json_encode(array())
            ));
        ?>
        <br clear="all" />
        <div class="blue-btn sendInvite" data-list="<?=$list->id?>">Send Invite</div>
        <div style="display:none; float:right;" id="sendInviteMsg">Invitation(s) sent.</div>
    </div>
    <?php }?>
    <?php if ($list->participation == 'Everybody') {?>
        <?php if (!empty($take)) {?>
            <div class="fb-like" style="float:right; margin-top:10px;" data-href="<?=Yii::app()->params['siteurl']?>/index.php/preferences/takelist?listid=<?=$list->id?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
        <?php } else {?>
            <div class="fb-like" style="float:right; margin-top:10px;" data-href="<?=Yii::app()->params['siteurl']?>/index.php/preferences/viewResult?listid=<?=$list->id?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
        <?php }?>    
    <?php }?>    
    
    </div>            
    
	<div class="block-left-info-content">
		<div class="gallery-result-page">
			<div class="activeImg captionTop1">
				<a href="javascript:;" class="openGallery" data-index="0" rel="nofollow"> <img
                        src="<?php echo $gallery[0]; ?>?w=236&h=139" alt="image" />
				</a>
                <?php if (!empty($list->link_title)) {?>
                <div class="caption1">
                    <div class="captionInner1">
                        <a href="<?=!empty($list->link) ? $list->link : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list->link_title)?></a>
                    </div>
                </div>
                <?php }?>
			</div>
			<ul class="list-img">
				<?php 
                $oi = 0;
                $opinions = $list->opinionRaiting($friends_ids_filter);
                $iso = $is = array();
                foreach($opinions as $opinion) 
                {
                    $oi++;
                    $is[$oi] = trim($opinion['image']);
                    $iso[$oi] = $opinion['value'];
                }
                
				$in = 1;
                foreach($rpgis as $gii)
				{
				    $do = 0;
                    $dataImg = $gallery[$gii];
                    if (sizeof($is) > 0) {
                        foreach ($is as $k=>$v) {
                            if (stripos($dataImg,$v) !== false) {
                                $do = $k;
                            } 
                        }
                    }
                    
                    $p = strrpos($gallery[$gii],'/');
                    if ($p !== false) {
                        $dataImg = substr($gallery[$gii],($p+1));
                    }
                    if ($do == 0) {
                        $opt = Opinion::model()->find("image = '".$dataImg."'");
                        if (!empty($opt->value)) {
                            foreach ($iso as $k=>$v) {
                                if (stripos($opt->value,$v) !== false) {
                                    $do = $k;
                                } 
                            }
                        }
                    }
                    
                    ?>
				<li><a href="javascript:;" data-image="<?=$dataImg?>" class="openGalleryMin active" data-option="<?=$do?>" data-index="0" rel="nofollow"> <img
						src="<?=$gallery[$gii];?>?w=71&h=42" alt="image" />
				</a></li>
				<?php 
                $in++;
				}
				?>
			</ul>
		</div>

		<div class="result-page-info">
			<div class="time-topic">
				<i>On:</i> <span><?php echo date('m/d/Y', strtotime($list->modified)); ?>
				</span>
			</div>
			<div class="name-topic">
				<i>Topic:</i> <span><?php echo $list->interest->interest; ?> </span>
			</div>
			<div class="name-topic">&nbsp;</div>
			<div class="name-topic">
				<i>Instructions:</i> <span><?php echo implode('<wbr>',str_split($list->instruction,50)); ?> </span>
			</div>
            <?php if ($user->isAdmin() && Yii::app()->controller->action->id == 'updateEntry') {?>
                <div class="name-topic badge-block">
                    <i>Official Badge:</i> <span><input type="checkbox" name="badge" value="1" <?php if ($list->badge) {?>checked="checked"<?php }?> /></span>
                </div>
            <?php } ?>
		</div>
	</div>
</div>