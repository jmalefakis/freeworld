<?php $listid = $_GET['listid'];      
    $userid = Yii::app()->user->getID();
    $baseurl = Yii::app()->request->baseUrl;
    $cs=Yii::app()->clientScript;

    // Add CSS
    $cs->registerCSSFile($baseurl.'/css/colorbox.css');

    // Add JS
    $cs->registerScriptFile($baseurl.'/js/jquery.form.js');
    $cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>

<?php $this->renderPartial('sidenavigation'); ?>   
<div class="outerdiv1_auth">
    <h2 class="detail_take" >Your list</h2>
  <div  class="showmor_take">
    <a class="textdecoration" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/viewtakelist?listid=<?php echo $listid;?>&url=<?php echo Yii::app()->request->urlReferrer;?>"><span>Show Full List</span></a>
  </div>
    <div class="blankdivtake">
        &nbsp;
    </div>
    
    <div class="outer_see_list">        
        <div class="list_name">
            <div class="list_name_div">
                <span><?php echo $data['name'] ?></span>
            </div>
            <div class="list_image_div">
                    <?php if(strpos($data['image'], 'profile/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else if(strpos($data['image'], 'freeworld/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else{ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/listdetail/'.$data['image']; ?>" />
                    <?php } ?>
            </div>
            <div style="float: left;">
            <div class="latest_title_cre">
                        <span>Created by:</span>
            </div>
            <div class="latest_title_div">
                        <span><?php echo $myuserinfo->firstname.' '.$myuserinfo->lastname; ?></span>
            </div>
            </div>
            <div class="list_image_div">
                <?php if($myuserinfo['image']!=''){?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $myuserinfo['image'];?>"/>
                               <?php } else { 
                                  if($myuserinfo['gender']=='male' || $myuserinfo['gender']=='Male')
                                     {
                                ?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($myuserinfo['gender']=='female' || $myuserinfo['gender']=='Female') {?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                
            </div>
        </div>
       
        <div class="maintable">
            <div class="list_title_div">
                <table class="tablediv">
                    <tr>
                        <td class="titlediv">
                            
                                <span><?php echo $data['title'] ; ?></span>
                           
                        </td>
                        
                        <td class="column_div" style="background: #95BCF2;">
                                    <span>  Rating</span>
                                    
                        </td>
                         
                    </tr>
                    
                    <tr>
                    <?php $temp = 1;
                      foreach ($datarow as $row){ ?>                     
                        <td class="row_div">
                            <span> <?php echo $row->value ;?> </span>
                        </td>
                      <?php 
                        if($temp%2==0)
                        {
                            echo "</tr><tr>";
                        }  
                        $temp++;
                       ?>
                      
                      <?php  } ?>
                   </tr>
               
                </table>
           </div>
        </div>
        
        <div class="outer_latest_title">
                    <div class="latest_title" style="width: 100px; margin-top: 10px;margin-left: 18px;">
                        <a id="textdecoration" onclick="editlist_popup('<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/editlist?listid=<?php echo $listid; ?>');" href="#"><span>Edit List</span></a>
                    </div>
       </div>
       
    </div>
    
    
    
    <div class="outer_see_list_other">        
        <div class="list_name">
            <div class="list_name_div">
                <span><?php echo $data['name'] ?></span>
            </div>
            <div class="list_image_div">
                    <?php if(strpos($data['image'], 'profile/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else if(strpos($data['image'], 'freeworld/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else{ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/listdetail/'.$data['image']; ?>" />
                    <?php } ?>
            </div>
            <div style="float: left;">
            <div class="latest_title_cre">
                        <span>Created by:</span>
            </div>
            <div class="latest_title_div">
                        <span><?php echo $userinfo->firstname.' '.$userinfo->lastname; ?></span>
            </div>
            </div>
            <div class="list_image_div">
                 <?php if($userinfo['image']!=''){?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $userinfo['image'];?>"/>
                               <?php } else { 
                                  if($userinfo['gender']=='male' || $userinfo['gender']=='Male')
                                     {
                                ?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($userinfo['gender']=='female' || $userinfo['gender']=='Female') {?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="image_mylist" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                               
                
            </div>
        </div>
       
        <div class="maintable">
            <div class="list_title_div">
                <table class="tablediv">
                    <tr>
                        <td class="titlediv">
                            
                                <span><?php echo $data['title'] ; ?></span>
                           
                        </td>
                        <?php $i=0;
                             foreach ($creater as $column){
                              if($i==3)
                                {
                                         break;
                                }
                            ?>
                        <td class="column_div">
                                    <span> <?php echo $column->value ;?> </span>
                                    <?php $i++; ?> 
                        </td>
                         <?php }?>
                    </tr>
                    
                    <tr>
                    <?php $temp = 1;
                      foreach ($datarowcreate as $row){ ?>
                     
                        <td class="row_div">
                            
                            <span> <?php echo $row->value ;?> </span>
                
                        </td>
                       <?php 
                       if($countcreter<4){ 
                            if($temp%$countcreter==0)
                            {
                                echo "</tr><tr>";
                            }
                       }
                        else
                        { 
                            if($temp%4==0)
                            {
                            echo "</tr><tr>";
                            }
                        }
                        $temp++;
                       ?>
                      
                      <?php  } ?>
                   </tr>
               
                </table>
           </div>
        </div>
       
    </div>
    
    
    
    <?php  $mytakenlist = ListTaken::model()->findAll("list_id = '$listid' AND user_id!=$userid");
               foreach ($mytakenlist as $tepra){
                   $otherdata = ListColumn::model()->findAllByAttributes(array(),'user_id =:id1 AND list_id =:id', array(':id1'=>$tepra->user_id,':id' => $tepra->list_id,));
                    $counttc = count($otherdata);
                    $counttc = $counttc+1; 
                    $userinfo= User::model()->find("id = '$tepra->user_id'");
    ?>
    
    <div class="outer_see_list_other">        
        <div class="list_name">
            <div class="list_name_div">
                <span><?php echo $data['name'] ?></span>
            </div>
            <div class="list_image_div">
                    <?php if(strpos($data['image'], 'profile/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else if(strpos($data['image'], 'freeworld/') === 0){ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['image'] ?>" />
                    <?php } else{ ?>
                        <img class="image_mylist" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/listdetail/'.$data['image']; ?>" />
                    <?php } ?>
            </div>
            <div style="float: left;">
            <div class="latest_title">
                        <span>Created by:</span>
            </div>
            <div class="latest_title_div">
                        <span><?php echo $userinfo->firstname .' '.$userinfo->lastname; ?></span>
            </div>
            </div>
            <div class="list_image_div">
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/profile/<?php echo $userinfo['image'];?>" class="image_mylist"/>
            </div>
        </div>
       
        <div class="maintable">
            <div class="list_title_div">
                <table class="tablediv">
                    <tr>
                        <td class="titlediv">
                            
                                <span><?php echo $data['title'] ; ?></span>
                           
                        </td>
                        <?php $i=0; foreach ($otherdata as $column){ 
                                if($i==3)
                                 {
                                        break;
                                 }
                       ?>
                        <td class="column_div">
                                    <span> <?php echo $column->value ;?> </span>
                                    <?php $i++;?>
                        </td>
                         <?php }?>
                    </tr>
                    
                    <tr>
                    <?php 
                     $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id =:y AND (serial_no=0 OR serial_no=1 OR serial_no=2 OR serial_no=3 )', 'params'=>array(':x'=>$tepra->list_id,':y'=>$tepra->user_id)));
                     $temp = 1;
                      foreach ($datarow as $row){ ?>
                        <td class="row_div">
                            <span> <?php echo $row->value ;?> </span>
                        </td>
                       <?php if($counttc<4){ 
                                if($temp%$counttc==0)
                                {
                                    echo "</tr><tr>";
                                }
                            }
                            else
                             { 
                                if($temp%4==0)
                                 {
                                   echo "</tr><tr>";
                                 }
                             }
                       
                        $temp++;
                       ?>
                      
                      <?php  } ?>
                   </tr>
               
                </table>
           </div>
        </div>
       
    </div>
    <?php  }  ?>
    


<?php //if($breake[0]==Yii::app()->params['searchurl']) { ?>
<!--<div class="backsuggestion" >
 <a class="textdecoration" href="<?php //echo $url ;?>">  <input class="image_buttons" type="image"   src="<?php //echo Yii::app()->request->baseUrl;?>/images/buttons/back.png"/></a>
 </div>
<?php //} else { ?>
<div class="backsuggestion"  >
 <a class="textdecoration" href="<?php //echo Yii::app()->request->baseUrl; ?>/index.php/preferences/mylist">  <input class="image_buttons" type="image"   src="<?php// echo Yii::app()->request->baseUrl;?>/images/buttons/back.png"/></a>
 </div>-->
<?php //} ?>

    
</div>
