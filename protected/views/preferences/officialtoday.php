<?php
$baseurl = Yii::app()->request->baseUrl;
$userid  = Yii::app()->user->getID();
$user = User::model()->findByPk($userid);
$path	=  Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$currenturl  = $url1[1];

if (!isset($_ajax)) { $_ajax = false; }
if (!isset($container)) { $container = 'contentdiv'; }
if (!isset($current_user)) { $current_user = Yii::app()->user->getID(); }
if (!isset($current_page)) { $current_page = 'officialtoday'; }

?>
<?php if (!$_ajax) { ?>

<script>
$(function() {

	initGlobals(
		'<?php echo $current_user; ?>',
		{
			'<?php echo $current_page; ?>': {
				pageno: 1,
				maxpageid: <?php echo $pages; ?>,
				container: '#<?php echo $container; ?>',
				action: 'preferences/officialToday'
			}
		}
	);
	FW.users[<?php echo $current_user; ?>].currentPage = '<?php echo $current_page; ?>';
	
	$(window).scroll(function() {
		initPagination(FW.currentUser, FW.users[FW.currentUser].currentPage);
	});
	
	//$('#popupDatepicker').datepick({onSelect: currentdatechange,onClose: sortbydate,minDate: -7, maxDate: -1,selectWeek:true,showTrigger: '#calImgsl',dateFormat:'yy-mm-dd',rangeSeparator:'/',changeYear:false,changeMonth: false,yearRange:'-102:+0',prevText:'<img style="position: absolute; margin-left: 53px;margin-top: 46px;" src=<?php echo Yii::app()->request->baseUrl;?>/images/freeworld/calenderarrow-left.png>',nextText:'<img style="position: absolute; margin-top: 46px;margin-left: -62px;"  src=<?php echo Yii::app()->request->baseUrl;?>/images/freeworld/calenderarrow-right.png>'});
	
}); //ready
	
	/*rangeSelect: true,*/
</script>

<input type="hidden" name="currentpage" value="<?php echo $currenturl;?>" id="currentpage"/>

<div class="featured_list_section">
	<div id="wrapper">
		<div class="sort_section">
		<!--
			<div class="sort_by_date">
				<input name="callender" value="" type="hidden" id="popupDatepicker" />
				<input name="popupcalender"  value=""  type="hidden" id="value_calender"  />
				<span class="sort_date_official" id="calImgsl" onclick="openCalenders()">Sort By Date
					<!--<select class="sort_date"><option>Sort By Date</option></select>->
					<div class="drop_down2" style="margin-top: 13px;padding-right: 13px;">
						<img  src="<?php echo $baseurl?>/images/drop_down.png" onclick="javascript:jQuery('#calImgsl').click();"/>
					</div>
				</span>
			</div>
			
			<div class="topics">
				<a href="#">Show topics</a>
			</div>
			
			<div class="sports" onclick="focusontopics()">
				<ul>
					<span id="mainspan_com"></span>
					<li>
						<input class="sports2" type="text"  id="companytypings_off"/>
						<img src="<?php echo $baseurl?>/images/ajax-loader.gif" class="loding_image_off" id="loding_li_img"/>
						<div class="official_ulli_interest home_ulli_interest" id="editdiv">
						</div>
						<input type="hidden" name="interest_list" value="" id="hiddeninterest"/>
					</li>
				</ul>
				<input type="hidden" name="interest_li"  id="onclick" value="official" />
				<input type="hidden" name="baseurl" id="baseurlclick" value="<?php echo $baseurl;?>"/>
				<input type="hidden" name="interest_list_com" value="" id="hiddeninterest_company"/>
			</div>
			<!--<div class="shuffle">
				<span onclick="officialshuffel()">SHUFFLE</span>
			</div>->
		-->
		</div><!-- .sort_section -->
		
		<div id="mainContent">
			<div class="content" id="contentdiv">
<?php } //if (!$_ajax) ?>

	<?php 
    
	if (count($data) > 0) {
		$i = 0;
		//for ($i=0; i<count($data); $i++) {
		foreach($data as $list) {
            if (empty($list)) {
                continue;
            }
            
            $i++;
			
			if ($list) {
				$rows_html = '';
				foreach($list['rows'] as $row_value) {
					$rows_html .= '<li><div>' . $row_value['value'] . '</div></li>';
				}
				$rows_html = '<ol class="list-box-rows ' . ($i % 2 === 0 ? 'left-side' : 'right-side') . '">' . $rows_html . '</ol>';
			}
			
			$friendsLabel = $user->isMember() ? "Friend" : "Follower";
			
			if (($i % 4 === 0) || (($i + 3) % 4 === 0)) {
	?>
				<div class="part_left"
					data-subject-type="list"
					data-id="<?php echo $list['id']; ?>" 
					data-is-taken="<?php echo CJavaScript::encode($list['is_taken']); ?>" 
					data-is-favorite="<?php echo CJavaScript::encode($list['is_favorite']); ?>"
					data-is-my="<?php echo CJavaScript::encode($list['user_id'] == $userid); ?>"
				>
					<div class="list-box-alert-panel">Remove this list from your Dashboard?
						<div class="btn-close-red"  onclick="javascript:$(this).parent().fadeOut(500);"></div>
						<div class="btn-gray-small" onclick="javascript:$(this).parent().fadeOut(500);">NO</div>
						<div class="btn-gray-small" onclick="javascript:removeListFromFavorite(this);">YES</div>
					</div>
					
					<div class="big_img_part">
						<div class="nightife">
							<?php if ($list) { ?>
							    <a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $list['id']; ?>" class="nightife2_litle fL size"><?php echo StringsHelper::capitalize($list['title']); ?></a>
							<?php } ?>
                            <?php if (!empty($list['badge'])) { ?>
                                <div class="list-box-badge-eagle fR" title="Wayo Official"></div>
                            <?php } ?>      
                            <div class="clear"></div>
						</div>
						<div class="big_img captionTop2">
							<?php if ($list) { ?>
							<a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $list['id']; ?>">
								<img class="officialtodayimg" src="<?php echo $baseurl; ?>/images/<?php echo $list['image'] . '?w=548&h=304'; ?>" alt="<?php echo StringsHelper::capitalize($list['title']); ?>" title="<?php echo StringsHelper::capitalize($list['title']); ?>"/>
								<?php if ($list && count($list['rows']) > 0) { echo $rows_html; } ?>
							</a>
                            <?php if (!empty($list['link_title'])) {?>
                            <div class="caption2">
                                <div class="captionInner2">
                                    <a href="<?=!empty($list['link']) ? $list['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list['link_title'])?></a>
                                </div>
                            </div>
                            <?php }?>
                            
							<?php } ?>
						</div>
					</div><!-- .big_img_part -->
					<div class="bottomimgleft">
						<div class="alice">
							<?php if ($list) { ?>
							<div class="alisename" onclick="listAuthorClick(this, <?php echo $list['user_id']; ?>, '#wrapper')">
								<?php echo StringsHelper::capitalize($list['author']); ?>
							</div>
							<?php } ?>
						</div>
						
						<div class="block-hidden"></div>
                            
                        <?php 
						if ($list) 
                        {   
                        ?>
						<div class="list-box-star tooltip-pointer-simple" onclick="javascript:toggleFavorite(this);"></div>
						<?php 
						    if ($list['is_taken'])
						    {
                        ?>
						<div class="icon-list tooltip-pointer-list"></div>
						<?php
                            $taken = 1;
						    }
                        } 
                        ?>
						
						<div class="alice_right">
							<?php if ($list) { ?>							
							<div class="contri" style="float: right; padding: 0 8px 0 24px; white-space: nowrap;">
								<span class="integercolor tooltip-pointer-in<?=!empty($taken) ? '-t' : ''?>"><?php echo $list['count_c']; ?> </span>
								<span class="contrspan tooltip-pointer-in<?=!empty($taken) ? '-t' : ''?>"> In </span>
								<span class="integercolor tooltip-pointer-friends<?=!empty($taken) ? '-t' : ''?>"> <?php echo count($list['friends']); ?> </span>
								<span class="contrspan tooltip-pointer-friends<?=!empty($taken) ? '-t' : ''?>"> <?php echo $friendsLabel; if (count($list['friends']) != 1) { ?>s<?php } ?></span>
							</div>
							<?php 
                            $taken = 0;
                                } ?>
						</div>
					</div><!-- .bottomimgleft -->
				</div><!-- .part_left -->
				<?php  
				} else { //$list['position'] % 2 == 0
				?>
					
				<div class="part_right"
					data-subject-type="list"
					data-id="<?php echo $list['id']; ?>" 
					data-is-taken="<?php echo CJavaScript::encode($list['is_taken']); ?>" 
					data-is-favorite="<?php echo CJavaScript::encode($list['is_favorite']); ?>"
					data-is-my="<?php echo CJavaScript::encode($list['user_id'] == $userid); ?>"
				>
					<div class="list-box-alert-panel">Remove this list from your Dashboard?
						<div class="btn-close-red"  onclick="javascript:$(this).parent().fadeOut(500);"></div>
						<div class="btn-gray-small" onclick="javascript:$(this).parent().fadeOut(500);">NO</div>
						<div class="btn-gray-small" onclick="javascript:removeListFromFavorite(this);">YES</div>
					</div>
					
					<div class="nightife2">
						<?php if ($list) { ?>
						    <a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $list['id']; ?>" class="nightife2_litle fL size"><?php echo StringsHelper::capitalize($list['title']); ?></a>
						<?php } ?>
                        <?php if (!empty($list['badge'])) { ?>
                            <div class="list-box-badge-eagle fR" title="Wayo Official"></div>
                        <?php } ?>
                        <div class="clear"></div>                            
					</div>
					<div class="image_small captionTop3">
						<?php if ($list) { ?>
						<a href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $list['id']; ?>">
							<img class="officialtodayimg_small" src="<?php echo $baseurl; ?>/images/<?php echo $list['image'] . '?w=351&h=306'; ?>" alt="<?php echo StringsHelper::capitalize($list['title']); ?>" title="<?php echo StringsHelper::capitalize($list['title']); ?>"/>
							<?php if ($list && count($list['rows']) > 0) { echo $rows_html; } ?>
						</a>
                        <?php if (!empty($list['link_title'])) {?>
                        <div class="caption3">
                            <div class="captionInner3">
                                <a href="<?=!empty($list['link']) ? $list['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list['link_title'])?></a>
                            </div>
                        </div>
                        <?php }?>
						<?php } ?>
					</div>
					<div class="bottom2">
						<div class="alice">
						<?php if ($list) { ?>
							<div class="alisename" onclick="listAuthorClick(this, <?php echo $list['user_id']; ?>, '#wrapper')">
								<?php echo StringsHelper::capitalize($list['author']); ?>
							</div>
						<?php } ?>
						</div>
						
						<div class="block-hidden"></div>
                        
						<?php 
						if ($list) 
                        {   
                        ?>
						<div class="list-box-star tooltip-pointer-simple" onclick="javascript:toggleFavorite(this);"></div>
						<?php 
						    if ($list['is_taken'])
						    {
                        ?>
						<div class="icon-list tooltip-pointer-list"></div>
						<?php 
                            $taken = 1;
						    }
                        } 
                        ?>
                        
						<div class="alice_right2">
							<?php if ($list) { ?>
							<div class="contri2" style="float: right; padding: 0 8px 0 24px; white-space: nowrap;">
                                <span class="integercolor tooltip-pointer-in<?=!empty($taken) ? '-t' : ''?>"><?php echo $list['count_c']; ?> </span>
								<span class="contrspan tooltip-pointer-in<?=!empty($taken) ? '-t' : ''?>"> In</span>
								<span class="integercolor tooltip-pointer-friends<?=!empty($taken) ? '-t' : ''?>"><?php echo count($list['friends']); ?> </span>
								<span class="contrspan tooltip-pointer-friends<?=!empty($taken) ? '-t' : ''?>"><?php echo $friendsLabel; if (count($list['friends']) != 1) { ?>s<?php } ?></span>
							</div>
							<?php } ?>
						</div>
					</div><!-- .bottom2 -->
				</div><!-- .part_right-->
				<?php
			} /// if (($i % 4 == 0) || (($i + 3) % 4 == 0))
		} /// foreach
	} else { /// if (count($data) > 0)
	?>
				<div class="nolistfound">No lists found </div>
	<?php
	} /// if (count($data) > 0)
	?>
			
<?php if (!$_ajax) { ?>

			</div><!-- #contentdiv -->
		</div><!-- #mainContent -->

		<div class="clear"></div>
		
		<div class="officialtodatloding" id="officialtodatlodingdiv">
			<img src="<?php echo Yii::app()->request->baseUrl;?>/images/loading.gif" />
		</div>
		
	</div><!-- #wrapper -->
</div><!-- .featured_list_section -->
<div class="clear"></div>

<?php } //if (!$_ajax) ?>

<script type="text/javascript">
setTimeout(function() {
	$('ol.list-box-rows li').each(function() {
		var h = $(this).height() + 1,
			$ch = $(this).children();
		while ($ch.outerHeight() > h) {
			$ch.text(function (index, text) {
				return text.replace(/\W*\s(\S)*$/, '...');
			});
		}
	});
});
</script>
