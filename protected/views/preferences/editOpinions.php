<?php
$baseurl = Yii::app()->request->baseUrl;
?>

<script type="text/javascript" src="<?php echo $baseurl; ?>/js/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/jquery.getimagedata.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/moxie.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/plupload.dev.js"></script>


<script>
var autocompleteData = {
	    source: "<?php echo $baseurl; ?>/index.php/preferences/getListOpinions?listid=<?php echo $list->id; ?>",
        minLength: 1,
        autoFocus:true,
        open: function( event, ui ) {
            $('.list-opinions tbody').data('selectListDisabled', true);
		},
		close: function( event, ui ) {
			$('.list-opinions tbody').data('selectListDisabled', false);
            var menu = $(this).data("uiAutocomplete").menu.element,
            focused = menu.find("li:has(a.ui-state-focus)");
            if (focused.text() != '') {
                $(this).val($(this).data('enter'));
            }
		},
        focus: function (event, ui) {
            var menu = $(this).data("uiAutocomplete").menu.element,
            focused = menu.find("li:has(a.ui-state-focus)");
            $(this).data('enter',focused.text());
        }        
	};


$(function(){
	$('title').html('Take List');
	
	$(window).keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
    
    $(".opinion-input").keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            $(this).focus();
            $('.list-opinions tbody').selectList.setClick($(this));
 		}
	});
	
	$(document).mouseup(function (e)
	{
		var container = $(".listdiv_takelist");

		if (container.has(e.target).length === 0) {
			container.hide();
		}
	});
    
	$('.list-opinions tbody').selectList('.list-opinion-title .opinion-input');
    
	$('.list-opinion-title .opinion-input').autocomplete(autocompleteData);

}); //ready

function opinionRowTemplate(i, position) {
	var s = '<tr class="list-opinion" data-index="' + i + '">'
		+ '<td class="list-opinion-position">'
		+ '	<input type="hidden" name="opinions[' + i + '][id]" value="" />'
		+ '	<input type="text" autocomplete="off" name="opinions[' + i + '][position]" onfocus="opinionFocus(this)" value="' + position + '" onkeyup="opinionPositionKeyUp(this)" />' 
		+ '</td>'
		+ '<td class="list-opinion-image" style="position: relative; z-index: ' + (1000 - i) + ';">'
		+ '	<input type="hidden" name="opinions[' + i + '][image]" value="" />'
		+ '	<div id="listImageTarget' + i + '" class="conteuner-photo" style="display: none;"></div>'
		+ '	<div id="listImageUploader' + i + '" class="uploader-wrapper"></div>'
		+ '</td>'
		+ '<td class="list-opinion-title">'
		+ '	<input type="text" autocomplete="off" class="opinion-input" name="opinions[' + i + '][value]" value="" placeholder="Your #' + position + ' choice" onkeyup="opinionKeyUp(this)" />'
		+ '	<input type="hidden" autocomplete="off" name="opinions[' + i + '][comment_id]" value="" />'
		+ '	<input type="text" autocomplete="off" class="comment-input" name="opinions[' + i + '][comment_value]" value=""  placeholder="Add a comment" />'
		+ '</td>'
		+ '</tr>'
		+ '<tr><td colspan="3" class="list-opinion-empty" data-index="' + i + '"></td></tr>';
	return s;
}


function opinionPositionKeyUp(that) {
	$(that)
		.closest('.list-opinion')
		.find('.list-opinion-title input')
		.first()
		.prop('placeholder', 'Your #' + that.value + ' choice')
}


function opinionKeyUp(that) {
    var $that = $(that),
		$row  = $that.closest('.list-opinion'),
		$main = $row.closest('.list-opinions').find('tbody'),
		$rows = $main.find('.list-opinion');
		data  = $row.data(),
		val   = $.trim($that.val());
		
	if (val !== '') {
		if (data.index === $rows.size() - 1) {
			var v, p = [],
				pos = parseInt($row.find('.list-opinion-position input[type="text"]').val()) + 1;
				
			$rows.find('.list-opinion-position input[type="text"]').each(function(){
				v = $.trim(this.value);
				if (!isNaN(v)) p.push(parseInt(v));
			});
			
			while ($.inArray(pos, p) !== -1) pos++;
			
			$main.append(opinionRowTemplate(data.index + 1, pos));
			$main.find(".list-opinion-title input[type=text]").autocomplete(autocompleteData);
		}
        
        $main.find('.list-opinion').each(function(){
            var row = this;
            var data  = $(row).data();
            if (($(row).find('.uploader-wrapper').children().size() === 0) && (!data.uploaderGetting)) {
                $(row).data('uploaderGetting', true);
                $.ajax({
                    type: 'POST',
                    url: '<?php echo $baseurl; ?>/index.php/preferences/multiFileUpload',
                    dataType: 'html',
                    data: {
                        cfg: {
                            uploadContainerId: 'listImageContainer' + $(row).data('index'),
                            uploaderId: 'listImageUploader' + $(row).data('index'),
                            uploadUrl: '<?php echo $baseurl; ?>/index.php/preferences/uploadImages',
                            deleteUrl: '<?php echo $baseurl; ?>/index.php/preferences/deleteImage',
                            uploadButtonClientId: 'fileUpload' + $(row).data('index'),
                            dragAndDropText: 'Drop a picture or <br/><a tabindex="-1" href="#" id="fileUpload' + $(row).data('index') + '" class="ctrl-upload-link">click to upload</a>',
                            onClientResponseReceived: 'opinionClientResponseReceived',
                            onInit: 'opinionUploaderInit'
                        }
                    }, 
                    success: function(resp) {
                        $(row).data('uploaderGetting', false);
                        $('#listImageUploader' + $(row).data('index')).html(resp);
                    },
                    error: function(error) {
                    }
                });
            }
        });

		/*if (($row.find('.uploader-wrapper').children().size() === 0) && (!data.uploaderGetting)) {
            alert('1');
			$row.data('uploaderGetting', true);
			$.ajax({
				type: 'POST',
				url: '<?php echo $baseurl; ?>/index.php/preferences/multiFileUpload',
				dataType: 'html',
				data: {
					cfg: {
						uploadContainerId: 'listImageContainer' + data.index,
						uploaderId: 'listImageUploader' + data.index,
						uploadUrl: '<?php echo $baseurl; ?>/index.php/preferences/uploadImages',
						deleteUrl: '<?php echo $baseurl; ?>/index.php/preferences/deleteImage',
						uploadButtonClientId: 'fileUpload' + data.index,
						//responseTargetId: 'listImageTarget' + data.index,
						dragAndDropText: 'Drop a picture or <br/><a tabindex="-1" href="#" id="fileUpload' + data.index + '" class="ctrl-upload-link">click to upload</a>',
						onClientResponseReceived: 'opinionClientResponseReceived',
						onInit: 'opinionUploaderInit'
					}
				}, 
				success: function(resp) {
					$row.data('uploaderGetting', false);
					$('#listImageUploader' + data.index).html(resp);
				},
				error: function(error) {
				}
			});
		}*/
		
	} else {
		if ((data.index > 2) && (data.index == $rows.size() - 2)) {
			$nextRow = $($rows.get(data.index + 1));
			if ($nextRow.find('.list-opinion-title input').val() === '') {
				$nextRow.remove();
                $main.find('.list-opinion-empty').each(function(){
                    if ($(this).data('index') == data.index + 1) {
                        $(this).remove();
                    }
                });
			}
		}
	}
}


function opinionUploaderInit(uploader) {
	$('#' + uploader.settings.drop_element)
		.closest('.uploader-wrapper')
		.data('uploaderContainerId', uploader.id + '_' + uploader.runtime + '_container');
}


function opinionClientResponseReceived(file, respObj, uploaderId, targetId) {
	console.log(file, respObj, uploaderId, targetId);
	var s =   '<img src="<?php echo $baseurl; ?>/images/titleimages/' + respObj.response + '?w=87&amp;h=77" class="small-photo" onclick="opinionShowBigImage(this);">'
		+ '<div class="conteuner-photo-div">'
		+ '<img src="<?php echo $baseurl; ?>/images/titleimages/' + respObj.response + '?w=326&amp;h=190" alt="Small Photo" class="big-photo"/>'
		+ '<div class="close-x" onclick="opinionHideBigImage(this);"></div><div class="trash" onclick="opinionDeleteImage(this);"></div></div>';

	var $uploader = $('#' + uploaderId).hide();
	uploaderContainerId = $uploader.data('uploaderContainerId');
	uploaderContainerId && $('#' + uploaderContainerId).hide();
	var $wrapper = $uploader.closest('.list-opinion-image');
	$(' > input',$wrapper).val(respObj.response);
	$('.conteuner-photo',$wrapper).append(s).show();
}


function opinionShowBigImage(that) {
	//$(that).closest('.list-opinion-image').css('z-index', 2000).find('.conteuner-photo-div').addClass('hidden');
	$(that).closest('.list-opinion-image').find('.conteuner-photo-div').addClass('hidden');
	return false;
}

function opinionHideBigImage(that) {
	//$(that).closest('.list-opinion-image').css('z-index', 1).find('.conteuner-photo-div').removeClass('hidden');
	$(that).closest('.list-opinion-image').find('.conteuner-photo-div').removeClass('hidden');
	return false;
}

function opinionDeleteImage(that) {
    var $wrapper = $(that).closest('.list-opinion-image');
    $wrapper.find('input').val('');
    //$wrapper.find('.conteuner-photo').hide().find('.conteuner-photo-div').removeClass('hidden');
    $wrapper.find('.conteuner-photo').empty().hide();
    uploaderContainerId = $wrapper.find('.uploader-wrapper').show().data('uploaderContainerId');
    uploaderContainerId && $('#' + uploaderContainerId).show();
	return false;
}
</script>

<?php

$userid	= Yii::app()->user->getID();
$user = User::model()->findByPk($userid);
$path =	Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url = $url1[0];
$siteurl = Yii::app()->params['siteurl'];
?>
<input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>"/>
<input type="hidden" id ="hidden_baseurl" value="<?php echo $baseurl ?>"/>

<div class="featured_list_section_internal">
<div id="wrapper">
<div id="mainContent">
<div class="widthauto">
	<?php //$this->renderPartial('sidenavigation'); ?>
	<div class="outerdiv" id="liscreation">
 <?php
$form=$this->beginWidget('CActiveForm', array(
	'id' => 'editOptions',
	'action' => $siteurl.'/index.php/preferences/' . $action . '?listid='.$list->id,
	'method' => 'post',
	'enableAjaxValidation' => true,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'name' => 'editOptions',
		'onsubmit' => 'return check_opinions();',
		//'onsubmit' => 'return true;',
		'validateOnSubmit' => true,
	),
));
		
?>
		<div class="box-white">
			<?php 
			// header
			$this->renderPartial('/preferences/partial/listResultsHeader', array(
                'baseurl' => $baseurl,
				'list' => $list,
				'user' => $user,
				'takenFriends' => $takenFriends,
				'takenList' => $takenList,
				'gallery' => $gallery,
                'rpgis' => $rpgis,
				'showFilter' => false, 
				'other_opinions' => $other_opinions,
                'take' => 1,
                'friends_ids_filter' => array()
			));
			?>
			
			<div style="clear: both;">
				<table class="list-opinions no-hover box-sizing">
					<tbody>
					<?php
					$position = 0;
					$opinions_count = count($opinions);
					$n = $opinions_count < 4 ? 4 : $opinions_count + 1;
					for($i = 0; $i < $n; $i++) {
						$opinion = $i < $opinions_count ? $opinions[$i] : false;
						$has_image = ($opinion && $opinion->image !== '') ? true : false;
						$position = $opinion ? $opinion->position: $position + 1;
						if ($opinion) {
							$comment = OpinionComment::model()->findByAttributes(
								array('opinion_id' => $opinion->id, 'user_id' => $opinion->user_id),
								array('order' => 'created DESC'));
						} else {
							$comment = false;
						}
						//CVarDumper::dump('=========<br>', 10, true);
						//CVarDumper::dump($comment, 10, true);
					?>
					<tr class="list-opinion" style="margin-bottom:17px;" data-index="<?php echo $i;?>">
						<td class="list-opinion-position">
							<input type="hidden" name="opinions[<?php echo $i; ?>][id]" value="<?php if($opinion) echo $opinion->id; ?>" /> 
							<input type="text" autocomplete="off"
								name="opinions[<?php echo $i; ?>][position]"
								value="<?php echo $position; ?>"
								onkeyup="opinionPositionKeyUp(this)" /> 
						</td>
						
						<td class="list-opinion-image" style="position: relative; z-index: <?php echo (1000 - $i); ?>;">
							<input type="hidden" name="opinions[<?php echo $i; ?>][image]" value="<?php if($opinion) echo $opinion->image; ?>" /> 
							<div id="listImageTarget<?php echo $i; ?>" class="conteuner-photo"<?php if(!$has_image) echo ' style="display: none;"'; ?>>
								<?php if ($has_image) { ?>
								<img src="<?php echo $opinion->getImageUrl(); ?>?w=87&h=77" class="small-photo" onclick="opinionShowBigImage(this);">
								<div class="conteuner-photo-div">
									<img src="<?php echo $opinion->getImageUrl(); ?>?w=326&h=190" alt="Small Photo" class="big-photo">
									<div class="close-x" onclick="opinionHideBigImage(this);"></div>
									<div class="trash" onclick="opinionDeleteImage(this);"></div>
								</div>
								<?php } ?>
							</div>
							
							<div id="listImageUploader<?php echo $i; ?>" class="uploader-wrapper"<?php if($has_image) echo ' style="display: none;"'; ?>>
							<?php
								$this->renderPartial('/controls/multifileupload', array(
									'uploaderId' => 'listImageUploader' . $i,
									'uploadUrl' => $baseurl . '/index.php/preferences/uploadImages',
									'deleteUrl' => $baseurl . '/index.php/preferences/deleteImage',
									'uploadButtonClientId' => 'fileUpload' . $i,
									//'responseTargetId' => 'listImageTarget' . $i,
									'dragAndDropText' => 'Drop a picture or <br/><a tabindex="-1" href="#" id="fileUpload' . $i . '" class="ctrl-upload-link">click to upload</a>',
									'onClientResponseReceived' => 'opinionClientResponseReceived',
									'onInit' => 'opinionUploaderInit'
								));
							?>
							</div>
						</td>
						
						<td class="list-opinion-title">
							<input type="text" autocomplete="off" class="opinion-input" placeholder="Your #<?php echo $position; ?> choice"
								name="opinions[<?php echo $i;?>][value]" 
								value="<?php if($opinion) echo strip_tags($opinion->value); ?>"
								onkeyup="opinionKeyUp(this)" 
                                 /> 
							<input type="hidden" name="opinions[<?php echo $i; ?>][comment_id]" value="<?php if($comment) echo $comment->id; ?>" /> 
							<input type="text" autocomplete="off" class="comment-input" placeholder="Add a comment"
								name="opinions[<?php echo $i;?>][comment_value]" 
								value="<?php if($comment) echo strip_tags($comment->value); ?>" /> 
						</td>
					</tr>
					<tr><td colspan="3" class="list-opinion-empty" data-index="<?php echo $i;?>"></td></tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div><!-- .rightdiv_listentry -->
		</div>
		
		<div class1="floatright" class="rightdiv_listentry ctrl-box ctrl-box-button" style="float: right;">
			<span id="opnerequred" style="display: none; margin-right: 0px; width: 332px" class="error_listentry"></span>
			<input type="submit" value="submit" name="submit" class="blue-btn ctrl-button" style="float: right;"/>
			<div tabindex="8" class="gray-btn ctrl-button" onclick="javascript:history.go(-1);">Cancel</div>
		</div>	

<?php $this->endWidget(); ?>
			
	</div><!-- .outerdiv -->
</div><!-- .widthauto -->
</div><!-- #mainContent -->
</div><!-- #wrapper -->
</div><!-- .featured_list_section_internal -->

<?php if (!empty($help)) {?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/gallery.css" />
    <link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
    <script>
        $(function(){
            $.fancybox({
               'href' :'#modalHelp',
               afterShow: function() {},
               beforeClose: function() {}
           });
     });   
    </script>
    <div class="customModal clear" id="modalHelp" style="display:none;">
        <div id="galleryContainer" class="galleryContainer">
            <h3>Welcome to Wayo</h3>
            <div id="gallery" class="content">
                <div class="slideshow-container help" style="height: 130px;">
                    <p>Please check out a brief overview of how to use Wayo:</p>
                    <p><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf">http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf</a></p>
                    <p>The overview is always available on the Help page.</p>
                    <p>Have fun!</p>
                </div>
            </div>
        </div>
    </div>
<?php }?>