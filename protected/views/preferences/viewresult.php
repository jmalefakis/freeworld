<?php
$baseurl = Yii::app()->request->baseUrl;
if (!isset($_ajax)) { $_ajax = false; }
?>

<?php if (!$_ajax) { ?>
<script>

var currentUserId = <?php echo $user->id; ?>;
var isAdmin = <?= $user->isAdmin() ? 1 : 0; ?>;

function updateTimeElapsedString($cc) {
	if ($cc.children().size() > 0) {
		$cc.find('.aut-date[data-comment-time]').each(function() {
			$this = $(this);
			$this.text(getTimeElapsedString($this.data('commentTime')));
		});
	}
}

function showviewcomment(id) {
	var $cc = $('#commentsContainer' + id);
	$('#commentInputField' + id).val('');
	$cc.empty().data({stop: false, loading: false, start: 0, limit: 15}).closest('.bodycomment').mCustomScrollbar('update');

	var timerId = setInterval(function() {
		//console.log('run timer = ', timerId, $cc);
		if ($cc.parent().closest('[data-opinion-id="' + $cc.data('opinionId') + '"]').css('visibility') === 'visible') {
			updateTimeElapsedString($cc);
		} else {
			//console.log('stop timer = ', timerId, $cc);
			timerId && clearInterval(timerId);
		}
	}, 60 * 1000);
		
	$('#div_hide' + id).css('visibility', 'visible').data('needUpdateResults', false);
	
	opinionGetComments($cc);
}

function closeviewcomment(id) {
	var $w = $('#div_hide' + id);
	if ($w.css('visibility') === 'visible') {
		if ($w.css('visibility', 'hidden').data('needUpdateResults') === true) {
			$w.data('needUpdateResults', false);
			opinionUpdateResults();
		};
	}
}

function showallFilterZ() {
	var visibility = $('#openshowallZ').css('visibility');
	$('#openshowallZ').css('visibility', (visibility == 'hidden' ? 'visible' : 'hidden'));
}


function opinionShowConfirmDeleteComment(comment_id) {
	var $o = $('li[data-comment-id="' + comment_id + '"]');
	$o.parent().find('.delete-block').hide();
	$o.find('.delete-block').show();
	return false;
};


function opinionHideConfirmDeleteComment(comment_id) {
	$('li[data-comment-id="' + comment_id + '"]').find('.delete-block').hide();
	return false;
};


function opinionGetComments($cc) {
	var data = $cc.data();
	//console.log('opinionGetComments:: data = ', data);
	
	if (!data.stop && !data.loading && !isNaN(data.opinionId)) {
		var $sb = $cc.data('loading', true).closest('.bodycomment');
		$sb.find('.ajax-loading').show();
		$sb.mCustomScrollbar('update');
		$sb.mCustomScrollbar('scrollTo', '.ajax-loading');

		$.ajax({
			url: '<?php echo $baseurl; ?>/index.php/preferences/getComments',
			type: 'GET',
			dataType: 'json',
			data: {
				list_id: <?php echo $list->id; ?>,
				opinion_id: data.opinionId,
				opinion_value: data.opinionValue,
				start: data.start,
				limit: data.limit
			},
			success: function(comments) {
				//setTimeout(function() {
					opinionShowComments($cc, comments);
					$cc.closest('.bodycomment').find('.ajax-loading').hide().closest('.bodycomment').mCustomScrollbar('update');
					var data = $cc.data();
					$cc.data({stop: comments.length < data.limit, loading: false, start: data.start + data.limit});
				//}, 1000);
			}
		})
	}
};


function opinionHtmlComment(comment, isHidden) {
	var s =  
		  '<li class="grey-brd' + ( (isHidden === true) ? ' hidden-comment' : '' ) + '" data-comment-id="' + comment.id + '">'
		+ '	<a href="javascript:showUserBriefs(' + comment.userId + ', \'#wrapper\');">'
		+ '		<img src="' + comment.userPhoto + '?w=45&h=38" class="small-photo">'
		+ '	</a>'
		+ '	<div class="comment-details">'
		+ '		<span class="aut-date" data-comment-time="' + comment.time + '">' + getTimeElapsedString(comment.time) + '</span>'
		+ '		<a class="aut-com" href="javascript:showUserBriefs(' + comment.userId + ', \'#wrapper\');">' + comment.userName + '</a>'
		+ '		<div class="aut-mess">' + comment.value + '</div>'
		+ ((comment.userId == currentUserId || isAdmin) ?
		  '		<div class="change-bl clearfix">'
		+ '			<div class="link" onclick="opinionShowConfirmDeleteComment(' + comment.id + ');">delete</div>'
		+ '			<div class="delete-block" style="display: none;">Delete this comment? <span class="y-n" onclick="opinionDeleteComment(' + comment.id + ');">YES </span> <span class="y-n" onclick="opinionHideConfirmDeleteComment(' + comment.id + ');">No</span></div>'
		+ '		</div>' : '')
		+ '	</div>'
		+ '</li>';
	return s;
};


function opinionShowComments($cc, comments, filter) {
	if (!comments && $.isArray(comments)) return;
	
	var n = comments.length;
	
	if (n < 1) return;
	
	var i, s = '', h = '', html = '', filter = [], comment, isHidden = false, $last = $cc.children(':last');

	//if (!$last.hasClass('hidden-comments-link')) $last = false;
	if ($last.size() === 0) $last = false;
	
	$('input[name="selectItem"]:checked').each(function(){ var v = $.trim($(this).val()); filter[v] = true; });
	
	for(i = 0; i < n; i++) {
		comment = comments[i];
		isHidden = !(filter.all || filter[comment.userId]);
		s = opinionHtmlComment(comment, isHidden);
		if (filter.all || filter[comment.userId]) {
			if (h) {
				if ($last && $last.hasClass('hidden-comments-link')) {
					$last.find('.hidden-comments').append(h);
				} else if ($last && $last.hasClass('hidden-comment')) {
					html += h;
				} else {
					html += '<li class="grey-brd link hidden-comments-link" onclick="opinionMoreComments(this);">See hidden comments<ul class="hidden-comments" style="display: none;">' + h + '</ul></li>';
				}
			}
			
			h = '';
			$last = false;
			html += s;
		} else {
			h += s;
		}
	}
	if (h) {
		if ($last && $last.hasClass('hidden-comments-link')) {
			$last.find('.hidden-comments').append(h);
		} else if ($last && $last.hasClass('hidden-comment')) {
			html += h;
		} else {
			html += '<li class="grey-brd link hidden-comments-link" onclick="opinionMoreComments(this);">See hidden comments<ul class="hidden-comments" style="display: none;">' + h + '</ul></li>';
		}
	}
	$cc.append(html);
};

function opinionMoreComments(that) {
	var $that = $(that),
		$h = $that.find('li').detach();
	
	$that.replaceWith($h);
	$h.closest('.bodycomment').mCustomScrollbar('update');
}

function opinionAddComment(idx, opinion_id) {
	opinion_id = $.trim(opinion_id);
	comment = $.trim($('#commentInputField' + idx).val());
	if ((!isNaN(opinion_id)) && (comment !== '')) {
		$.ajax({
			url: '<?php echo $baseurl; ?>/index.php/preferences/addComment',
			type: 'POST',
			dataType: 'html',
			data: {
				opinion_id: parseInt(opinion_id),
				comment: comment
			},
			success: function(result) {
				$('#commentInputField' + idx).val('').closest('.hiddendiv_viewcoments').data('needUpdateResults', true);
				var $cc = $('#commentsContainer' + idx);
				var n = $cc.prepend(result).children().size();
				updateTimeElapsedString($cc);

				$('#commentsAddButton' + idx).text(n > 1 ? n + ' comments..' : 'add comments..');
				var sb = $cc.closest('.bodycomment');
				sb.mCustomScrollbar('update');
				sb.mCustomScrollbar('scrollTo', 'first');
			}
		})
	}
};


function opinionDeleteComment(comment_id) {
	if (!isNaN(comment_id)) {
		$.ajax({
			url: '<?php echo $baseurl; ?>/index.php/preferences/deleteComment',
			type: 'POST',
			dataType: 'html',
			data: {
				comment_id: comment_id,
                currentUserId: currentUserId        
			},
			success: function(result) {
				var $o = $('li[data-comment-id="' + comment_id + '"]'),
					$p = $o.parent();
					n = $p.children().size() - 1;
				$p.closest('.td3_listentry_comment').find('.commentcount_viewlist').text(n > 1 ? n + ' comments..' : 'add comments..');
				$o.closest('.bodycomment').mCustomScrollbar('update');
				$o.closest('.hiddendiv_viewcoments').data('needUpdateResults', true);
				$o.remove();
			}
		})
	}
};


function opinionUpdateResults(ids) {
	if (ids === false) return false;
	if (typeof FW === 'undefined') window.FW = {};
	if (typeof FW.friends_ids_filter === 'undefined') FW.friends_ids_filter = [];
	if ($.isArray(ids)) FW.friends_ids_filter = ids;
	
	var data = { _ajax: true };
	if (FW.friends_ids_filter.length > 0) {
		data.friends_ids = FW.friends_ids_filter;
	};
	
	$('#loading').show();
	$('#tableResults').empty();
	$.ajax({
		url: '<?php echo $baseurl; ?>/index.php/preferences/viewResult?listid=<?php echo $list->id; ?><?php if (isset($_REQUEST['take'])) { echo '&take='.$_REQUEST['take'];}?>',
		type: 'POST',
		dataType: 'html',
		data: data,
		success: function(result) {
			$('#loading').hide();
			$('#tableResults').html(result);
			
			$('.bodycomment').mCustomScrollbar({
				scrollButtons: {
					enable: true
				},
				callbacks: {
					//onTotalScrollOffset: 60,
					onTotalScroll: function() {
						opinionGetComments(this.find('ul[data-opinion-id]'));
					}
				}
			});
            $('#galleryContainer').empty();
            $.ajax({
                url: '<?php echo $baseurl; ?>/index.php/preferences/viewResult?galleryOn=1&listid=<?php echo $list->id; ?><?php if (isset($_REQUEST['take'])) { echo '&take='.$_REQUEST['take'];}?>',
                type: 'POST',
                dataType: 'html',
                data: data,
                success: function(result2) {
                    $('#galleryContainer').html(result2);
                    $.initGallery();
                }
            });
		}
	});
}


$(function() {
	$('title').html('List Results');
	
	$('body').mouseup(function (e) {
		var container = $('.hiddendiv_viewcoments, #openshowallZ');
		if ((container.has(e.target).length == 0) && (!container.is(e.target))) {
			$('.hiddendiv_viewcoments').trigger('close');
			$('#openshowallZ').css('visibility', 'hidden')
		}
	});
	
	$('body').on('keydown', function(e) {
		$('.hiddendiv_viewcoments[style*="visible"]').find('.block-input-run input').focus();
	});
	
	$(document).on('close', '.hiddendiv_viewcoments', function(e) {
		var $w = $(this);
		if ($w.css('visibility') === 'visible') {
			if ($w.css('visibility', 'hidden').data('needUpdateResults') === true) {
				$w.data('needUpdateResults', false);
				opinionUpdateResults();
			};
		}
	});

	$('#tableResults').selectList('tr.opinion-row');
	
	$('.bodycomment').mCustomScrollbar({
		scrollButtons: {
			enable: true
		},
		callbacks: {
			//onTotalScrollOffset: 60,
			onTotalScroll: function() {
				opinionGetComments(this.find('ul[data-opinion-id]'));
			}
		}
	});
	
	
	$('body').on('keydown', '.add-comment-input', function(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			event.stopPropagation();
			event.stopImmediatePropagation();
			var $btn = $(this).parent().find('.add-btn-blue');
			if ($btn.size() > 0) {
				$btn[0].onclick();
			}
		}
	});
	
}); //ready

</script>
<?php } //if (!$_ajax) ?>

<?php
$path =	Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url = $url1[0];
$siteurl = Yii::app()->params['siteurl'];
?>

<?php if (!$_ajax) { ?>

<input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>"/>
<input type="hidden" id ="hidden_baseurl" value="<?php echo $baseurl ?>"/>

<div class="featured_list_section_internal">
<div id="wrapper">	
<div id="mainContent">	
<div class="widthauto">
	<div class="outerdiv" id="liscreation">		
	    
	    <?php 
	    $showArchived = false;
	    $archiveTime = time();
	    /*$archive = Archive::model()->findByAttributes(array("list_id" => $list->id));
	    if ($archive) {
            $currTime = time();
            $archiveTime = strtotime($archive->modified);
            $showArchived = $currTime - $archiveTime < 3 * 86400;
            
            $archiveTime = DateHelper::toClientTime($archiveTime);
        }*/
	    ?>
	    <div class="delete-block" style="width: 550px; top: -15px; float: right;<?php if (!$showArchived) { ?> display: none;<?php } ?>">
	        The results where flushed by moderator on 
	        <?php echo date("m/d/Y",  $archiveTime); ?> at <?php echo date("ga", $archiveTime); ?>
	    </div>
	    
	    <div class="clear"></div>
				
		<div class="box-white list-result-block">		    
			<?php 
			// header
			$this->renderPartial('/preferences/partial/listResultsHeader', array(
                'baseurl' => $baseurl,
				'list' => $list,
				'user' => $user,				
				'takenFriends' => $takenFriends,
				'takenList' => $takenList,
				'gallery' => $gallery,
                'rpgis' => $rpgis,
				'showFilter' => true,
                'friends_ids_filter' => $friends_ids_filter
			));
            ?>
            
			<div class="contentblock">
			<table class="list-opinions box-sizing" id="tableResults">
				<tbody>
				
<?php } //if (!$_ajax) ?>
					
				<?php
				$oi = 0;
                $ii = 1;
                $isc = 1;
				$opinions = $list->opinionRaiting($friends_ids_filter);
				foreach($opinions as $d=>$opinion) 
                {
                    $last = '';
                    if (($d+1) == sizeof($opinions)) {
                        $last = 1;
                    }
                    //$img = trim(Opinion::anyImageByValue($opinion['value']));
                    $img = trim($opinion['image']);
                    $img_name = $img;
                    if (!empty($img)) 
                    {
                    	if ((strpos($img, 'profile/') === 0) || (strpos($img, 'freeworld/') === 0)) 
                        {
                    		$img = '/images/' . $img;
                    	} 
                    	else 
                    	{
                    		$img = '/images/titleimages/list_thumbs/official/small/' . $img;
                    	}
                    	$img = Yii::app()->request->baseUrl . $img . '?w=260&h=172';
                    }
                    
					$comments = OpinionComment::model()->with(array(
						'user',
						'opinion' => array(
							'select' => false,
							'on' => 'opinion.list_id = :x AND opinion.value = :y',
                                    // we should show all user comments for any opinion
									//. (count($friends_ids_filter) == 0 
                                    //    ? '' 
                                    //    : ' AND opinion.user_id IN (' . implode($friends_ids_filter, ',') . ')'),
							'params' => array(':x' => $list->id, ':y' => $opinion['value'])
                        ))
					)->findAll(array(
                        'alias' => 'comment', 
                        'condition' => "trim(comment.value) <> ''"
                            . (count($friends_ids_filter) == 0
                                ? ''
                                : ' AND comment.user_id IN (' . implode($friends_ids_filter, ',') . ')'),
                        'order' => 'comment.created DESC'
                    ));
					
                    $comments_count = count($comments);
                    
                    $oi++;
                    
                    
				?>
				
				<tr class="list-opinion opinion-row" data-index="<?php echo $oi; ?>">
					<td class="list-opinion-position">
						<span class="positions"><?php echo $oi; ?></span>
					</td>
					
                    <?php if (!empty($img)) {?>
                    <td class="list-opinion-image">
                        <div id="listImageTarget" class="openGallery conteuner-photo" 
                            data-index="<?=$ii?>" 
                            data-scroll-index="<?=$isc?>" 
                            data-option="<?=$oi?>"
                            <?php if (!empty($last)) {?>
                            data-last="1"
                            <?php }?>
                            data-image="<?php echo $img_name; ?>"
                        >
                        <?php 
                            $opsCount = $this->opinionsCount($opinion['list_id'], $opinion['value'],$friends_ids_filter);
                            $ii = $ii + $opsCount;
                            $isc++;
                        ?>
                            <img onclick="javascript:void(0);" src="<?php echo $img;?>"/>
                        </div>    
                    <?php } else {?>
                    <td class="list-opinion-title">    
                        <div class="list-opinion-item">
                            <div class="list-opinion-item-title noImage">No Image Yet</div>
                        </div>
                    <?php }?>
					</td>
					<td class="list-opinion-title">
						<div style="position: relative;">
						<div class="list-opinion-item">
							<div class="list-opinion-item-comments" id="commentsAddButton<?php echo $oi; ?>" onclick="showviewcomment('<?php echo $oi; ?>')">
							    <i class="icon-comments"></i><span><?php echo $comments_count;?></span> Comment<?php if ($comments_count != 1) { ?>s<?php } ?>
							</div>
							
							<div id="div_hide<?php echo $oi; ?>" class="hiddendiv_viewcoments" style="visibility: hidden;"
								data-opinion-id="<?php echo $opinion['id']; ?>"
								data-opinion-value="<?php echo $opinion['value']; ?>">
								<div id="div_close0" class="close-com" onclick="closeviewcomment('<?php echo $oi; ?>')" ></div>
								<div class="block-current-user">
									<span class="block-current-user-text">Leave a Comment</span>
									<div class="block-current-user-name">
										as <a href="javascript:showUserBriefs(<?php echo $user->id; ?>, '#wrapper');"><?php echo $user->getFullName(); ?></a>
									</div>
									<img class="small-photo" src="<?php echo $user->getImageUrl(); ?>?w=27&h=23" />
								</div>
								<div class="block-input-run">
									 <!--div class="add-btn-blue" onclick="opinionAddComment('<?php echo $oi; ?>', '<?php echo $list->id; ?>','<?php echo $opinion['value']; ?>');">Add</div-->
									 <div class="add-btn-blue" onclick="opinionAddComment('<?php echo $oi; ?>', '<?php echo  $opinion['id']; ?>');">Add</div>
									 <input class="add-comment-input" id="commentInputField<?php echo $oi; ?>" type="text" placeholder="Would you like to say something" />  
								</div>
								<div class="bodycomment">
									<ul id="commentsContainer<?php echo $oi; ?>" style="max-width: 249px;"
										data-id="<?php echo $oi; ?>"
										data-opinion-id="<?php echo $opinion['id']; ?>"
										data-opinion-value="<?php echo $opinion['value']; ?>">
									</ul>
									<div class="ajax-loading"></div>
								</div>
							</div>	
                            <?php $mx = 50;
                                   if (strlen($opinion['value']) > $mx) {
                                       $ex = explode(' ',$opinion['value']);        
                                       $p = 0;
                                       $l = array();
                                       $r = '';
                                       foreach ($ex as $v) {
                                           if (strlen($v) > $mx) {
                                               $v = implode('<wbr>',str_split($v,50));
                                           }
                                           
                                           if ($p < $mx) {
                                               $r .= $v.' ';
                                               $p = strlen($r);
                                           } else {
                                               $l[] = $r;
                                               $r = $v.' ';
                                               $p = 0;
                                           } 
                                       }
                                       /*if (sizeof($l) == 0) {
                                           $l[] = $r;
                                       }*/
                                       if (!empty($r)) {
                                           $l[] = $r;
                                       }
                                       $o = implode('<wbr>',$l);
                                   } else {
                                       $o = $opinion['value'];
                                   }?>
                            
							
							<div class="list-opinion-item-title"><?=$o?></div>
						</div><!-- .list-opinion-item -->
						</div><!-- position:relative -->
						<table class="list-opinion-comments">
						    <?php
						    $rcis = array();
						    $ci = 0;
						    foreach ($comments as $comment)
						    {
						        $rcis[] = $ci++;
						    }
						    
						    while (count($rcis) > 3)
						    {
						        array_splice($rcis, mt_rand(0, count($rcis) - 1), 1);
						    }
						    
						    foreach ($rcis as $rci)
						    {
						        $comment = $comments[$rci];
						        
						        //$cv = implode('<br />',str_split($comment->value,60));
						        $cv = strip_tags($comment->value);
                                if (strlen($cv) > 120) $cv = substr($cv, 0, 120) . ' [..]';
						        
						        $caImage = $baseurl . '/images/default.jpg';
						        if ($comment->user->image != '')
						        {
						        	$caImage = '/images/profile/user_thumbs/listcreation/'
	                                    . $comment->user->image .'?w=22&h=22';
						        }						        					
						    ?>
						
							<tr>
							<td class="list-comments"> 
								<ul><li><?php echo implode('<wbr>',str_split($cv,50)); ?></li></ul>
							</td>
							<td class="list-author-comments">
								<div class="info">
									<span class="comments-date"><?php echo DateHelper::datetimeToAgoStr($comment->created); ?></span>
                                    <span class="comments-name" onclick="showUserBriefs(<?php echo $comment->user->id; ?>, '#wrapper');"><i class="icon-comments <?php echo strtolower($comment->user->gender); ?>"></i><?php echo $comment->user->getFullName();?></span>
									<span class="comments-user"><img  onclick="showUserBriefs(<?php echo $comment->user->id; ?>, '#wrapper');" src="<?php echo $caImage; ?>"/></span>							
                                </div>
							</td>
							</tr>							
							<?php 
						    }
							?>
						</table>
					</td>
				</tr>
				<tr class="list-opinion">
					<td colspan="3" class="list-opinion-empty"></td>
				</tr>				
				
				<?php } ?>

<?php if (!$_ajax) { ?>

				</tbody>
			</table>
			</div>	
			
			<div class="ajax-loading" id="loading"></div>
					
		</div>

		<div class="clear"></div>
			
		<div class1="floatright" class="rightdiv_listentry ctrl-box ctrl-box-button" style="float: right;">
	
			<div class="blue-btn ctrl-button" onclick="goBack();">Back</div>
			
			<?php 
			if (!empty($takenList)) 
			{         
			    $friends_ids = FriendHelper::getMyFriendsIds(true);
			    if ($user->isMember() && ($list->participation == 'Everybody' || in_array($list->user_id, $friends_ids) || $list->user_id == $user->id)) 
                { 
            ?>
				<a class="gray-btn ctrl-button" href="<?php echo $baseurl?>/index.php/preferences/updateEntry?listid=<?php echo $list->id; ?>">
					Update Entries
				</a>
			<?php 
                } else { 
            ?>
				<div class="gray-btn ctrl-button ctrl-button-disable">Update Entries</div> 
			<?php 
                }
			} 
			?>
			
			<?php if ($user->id == $list->user_id || $user->isAdmin()) { ?>
			<a class="gray-btn ctrl-button" href="<?php echo $baseurl?>/index.php/preferences/editlist?listid=<?php echo $list->id; ?>">
				EDIT
			</a>
			<?php } ?>
			
		</div>


	</div><!-- .outerdiv -->
</div><!-- .widthauto -->
</div><!-- #mainContent -->
</div><!-- #wrapper -->
</div><!-- .featured_list_section_internal -->

<?php } if (!$_ajax) { ?>
<!-- Gallery -->
<div class="customModal clear" id="modal" style="display:none;">
    <div id="galleryContainer" class="galleryContainer">
        <h3><?=StringsHelper::capitalize($list->title)?></h3>
        <div id="gallery" class="content">
            <div class="slideshow-container">
                <div id="loading" class="loader"></div>
                <div id="slideshow" class="slideshow"></div>
            </div>
            <div id="caption" class="caption-container"></div>
        </div>
        <div id="thumbs" class="navigation">
            <?php  
                $rowsLi = array();
                $scrollIndex = 0;
                global $rowsLi;
                global $scrollIndex;
                $opinions = $list->opinionRaiting($friends_ids_filter);
                $subOpinions = array();
                foreach($opinions as $key=>$opinion) 
                {
                    $opsInfo = $this->opinionInfo($opinion['list_id'], $opinion['value'],$friends_ids_filter);
                    if (sizeof($opsInfo) > 0) {
                        foreach ($opsInfo as $k=>$opt) {
                            if ($user->id == $opt['user_id']) {
                                $_ = $opt;
                                unset($opsInfo[$k]);
                                array_unshift($opsInfo, $_);
                            }
                        }
                        $subOpinions[$key] = $opsInfo;
                    }                                                 
                }      
                
                function outOpinion($opinion,$currentChoice,$key) {
                    $user = Yii::app()->user->getModel();
                    global $rowsLi;
                    global $scrollIndex;
                    if (!empty($opinion['image'])) {
                        $img = Yii::app()->request->baseUrl . '/images/titleimages/list_thumbs/official/small/' . $opinion['image'];
                        $userInfo = User::model()->find(array(
                            'select' => 'firstname, lastname',
                            'condition' => 'id=:id',
                            'params' => array(':id'=>$opinion['user_id']),
                        ));
                        
                        $userFullName = '';
                        if (!empty($userInfo->firstname) && !empty($userInfo->lastname)) {
                            $userFullName = StringsHelper::capitalize($userInfo->firstname.' '.$userInfo->lastname);
                        } else if (!empty($userInfo->firstname)) {
                            $userFullName = StringsHelper::capitalize($userInfo->firstname);
                        }

                        if (strtolower($currentChoice) !== strtolower($opinion['value'])) {
                            if ($currentChoice != '_') {
                                echo '</ul>';
                            }          
                            ?>
                            <?php if (!empty($opinion['value']) && $opinion['value'] != '_') {?>
                            <p class="group"><span><?=$key?></span> <?=$opinion['value']?></p>
                            <?php }
                            $scrollIndex++;?>
                            <ul class="thumbs noscript" id="trmb<?=$key?>" data-index="<?=$key?>">
                        <?php
                        }
                        ?>
                            <li data-index="<?=$key?>" class="captionTop6">
                                <a class="thumb" name="leaf" href="<?=$img?>?w=528&h=340&logic=2" title="" data-scroll-index="<?=$scrollIndex;?>">
                                    <img src="<?=$img?>?w=150&h=84" alt="" height="84" style="height: 84px;" />
                                    <?php if($user->isAdmin()) {?>
                                    <div class="removeGalleryImg" data-image="<?=$opinion['image']?>" data-list="<?=$opinion['list_id']?>">
                                        <img src="<?=Yii::app()->request->baseUrl;?>/images/cross1.png" alt="" />
                                    </div>
                                    <?php }?>
                                </a>
                                <div class="caption">
                                    <div class="image-desc">
                                        <span class="author" onclick="showUserBriefs(<?=$opinion['user_id']; ?>, '#wrapper'); closeFancy();">Added by: <strong><?=$userFullName?></strong></span>
                                        <span class="date">On: <strong><?=date('m/d/Y',  strtotime($opinion['created']))?></strong></span>
                                        <?php if (!empty($opinion['link_title'])) {?>
                                        <span class="link"><a href="<?=!empty($opinion['link']) ? $opinion['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($opinion['link_title'])?></a></span>
                                        <?php }?>
                                    </div>
                                </div>
                            </li>  
                        <?php  
                        $rowsLi[$scrollIndex] = empty($rowsLi[$scrollIndex]) ? 1 : $rowsLi[$scrollIndex] + 1; 
                        $currentChoice = $opinion['value'];
                    }
                    return $currentChoice;
                }
                
                $currentChoice = ''; 
                outOpinion(array('user_id' => $list->user_id,
                                  'created' => $list->modified,
                                  'image'   => $list->image,
                                  'list_id' => $list->id,
                                  'link_title' => $list->link_title, 
                                  'link' => $list->link,
                                  'value'   => '_'),'',0);
                $scrollIndex--;
                foreach($opinions as $key=>$opi) 
                {
                    if (!empty($subOpinions[$key])) {
                        foreach($subOpinions[$key] as $opinion) {
                            $currentChoice = outOpinion($opinion,$currentChoice,($key+1));
                        }
                    }
                }
                if (sizeof($rowsLi) > 0) {
                    foreach ($rowsLi as &$value) {
                        $value = ceil($value/3);
                        if ($value == 0) {
                            $value = 1;
                        }
                    }
                }
            ?>
        </div>
        <input type="hidden" value='<?=json_encode($rowsLi,true)?>' id="rowsLi" name="rowsLi" />
        <input type="hidden" value="0" id="pageRedirect" name="pageRedirect" />
    </div>
</div>
<!-- END Gallery -->
<?php }?>

