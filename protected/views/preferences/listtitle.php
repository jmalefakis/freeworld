<?php
$baseurl = Yii::app()->request->getBaseUrl(true);
?>

<script type="text/javascript" src="<?php echo $baseurl; ?>/js/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/jquery.getimagedata.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/moxie.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/plupload.dev.js"></script>

<script>
$(document).ready(function() {
	$('title').html('Create List');
    
    $('input[name=participation]', '.form-list').change(function() {
        checkDatabaseListTitle($('#listTitle'), '<?php echo $baseurl;?>');
    });
    
    $('.photoCredit').change(function() {
        setPhotoCredit();
    });
    setPhotoCredit();
});

function setPhotoCredit() {
    if ($('#link_title').val() != '') {
        var link = 'javascript:;';
        if ($('#listLink').val() != '') {
            link = $('#listLink').val();
            i = link.indexOf('http');
            if (i<0) {
               link = 'http://'+link;     
            }
            
        }
        $('.captionInner10').html('<a href="'+link+'" target="_blank">'+$('#link_title').val()+'</a>');
    } else {
        $('.captionInner10').empty();
    }
}

function listtitleValidate() {
    if (list_validate() !== false) {
        if ($('#listTitle').data('error') == '1') 
            return false;
        $('#participationFriends').css("height", "0px")
            .val($("#participationFriends").data("ids").join(","));
        return true;
    }    
    return false;
}
</script>

<?php
$baseurl = Yii::app()->request->baseUrl;
$user_id  = Yii::app()->user->getID();
$user = User::model()->findByPk($user_id);
$path =  Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url  = $url1[0];
$siteurl = Yii::app()->params['siteurl'];
// Get client script
$cs=Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/colorbox.css');

// Add JS
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>

<input type="hidden" name="onclick" value="personalclick" id="onclick" />
<input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>" />
<input type="hidden" id="hidden_baseurl" value="<?php echo $baseurl ?>" />
<input type="hidden" name="returnfalse" value="" id="returnfalse" />

<div class="featured_list_section_internal">
	<div class="widthauto">
		<div class="outerdiv" id="liscreation">
		    <form action="<?php echo $siteurl . ($list ? '/index.php/preferences/editlist?listid=' . $list->id 
		                    : '/index.php/preferences/listtitle');?>" 
		        name="listcreate" class="form-list" method="post"
		        onsubmit="return listtitleValidate();">
            
            <?php
            $is_public = true;
            if ($list) $is_public = $list->participation == 'Everybody';
            ?>
            
			<div class="ctrl-box">
				<div class="ctrl-wrapper ctrl-list-title<?php if($list && $is_public) echo ' disabled'; ?>">
					<input tabindex="1" type="text" id="listTitle" name="title" placeholder="List title" maxlength="120"
						onblur="checkDatabaseListTitle(this, '<?php echo $baseurl;?>')"
						value="<?php if($list) echo $list->title; ?>"<?php if($list && $is_public) echo ' disabled="disabled"'; ?> />
				</div><!-- .ctrl-wrapper -->
				<span class="ctrl-wrapper-error"></span>
			</div>

			<fieldset style="float: left;">
				<div class="ctrl-box">
					<label class="ctrl-label">List topic:</label>
					<div class="ctrl-wrapper ctrl-list-topic">
						<select tabindex="2" id="listInterest" name="interest" placeholder="List topic">
							<option value="" disabled="disabled" selected="selected">Select</option>
							<?php foreach ($interest as $inter){?>
							<option value="<?php echo $inter->id?>"
								<?php if($list && ($list->interest_id == $inter->id)) { echo 'selected="selected"';} ?>
								>
								<?php echo $inter->interest?>
							</option>
							<?php } ?>
						</select> 
					</div><!-- .ctrl-wrapper -->
				</div>
				<div class="ctrl-box">
					<div id="listImage" class="ctrl-wrapper ctrl-list-image">
						<?php if ($list) {?>
						<img class="listdefaultimg" src="<?php echo $list->getImageUrl(); ?>?w=368&h=238" />
                        <?php } ?>
					</div>
                    <div class="caption10">
                        <div class="captionInner10"></div>
                    </div>
				</div>
				<div class="ctrl-box">
					<div class="ctrl-wrapper-drop-image">
						<?php
							$this->renderPartial('/controls/multifileupload', array(
								'uploaderId' => 'uploaderContainer',
								'uploadUrl' => $baseurl . '/index.php/preferences/listTitleImage',
								'deleteUrl' => $baseurl . '/index.php/preferences/deleteImage',
								'uploadButtonClientId' => 'fileUpload',
								'responseTargetId' => 'listImage',
								'dragAndDropText' => 'Drop a picture or <a id="fileUpload" href="javascript:;" class="ctrl-upload-link">click to upload</a>'
							));
						?>
					</div>
				</div>
			</fieldset>
			
			<fieldset style="float: right;">
				<div class="ctrl-box">
					<label class="ctrl-label">Rules for participants:</label>
					<div class="ctrl-wrapper ctrl-list-rules">
						<textarea tabindex="4" id="listInstruction" name="instruction" placeholder="Please explain how you would like your participants to format their input. E.g. for the best vacation spot, you may recommend them to enter results as Country, City, State."><?php if($list) echo $list['instruction']; ?></textarea>
					</div><!-- .ctrl-wrapper -->
				</div>
				
				<div class="ctrl-box">
					<div class="ctrl-label">Participation is open for:</div>
				</div>

				<div class="ctrl-box">
				<?php if($list) { ?>
					<label class="ctrl-label"><?php echo $list->participation; ?></label>
				<?php } else { ?>
					<div class="ctrl-wrapper ctrl-wrapper-group">
						<div class="ctrl-checkbox">
							<label>
								<input tabindex="5" type="radio" name="participation" value="Everybody" checked="checked" />
								<div>Everybody</div>
							</label>
						</div>
						<div class="ctrl-checkbox">
							<label>
								<input tabindex="6" type="radio" name="participation" value="My friends" />
								<div>My friends</div>
							</label>							
						</div>						
					</div>
				<?php } ?>
				</div>
				
				<?php if (!$list || $list->participation != 'Everybody') { ?>
				<div class="ctrl-box">
    				<div class="ctrl-wrapper" id="participationFriendsDiv"
    			        style="float: left; width: 570px; margin-top: -4px; height: auto;">
                        <input tabindex="10" type="text" id="participationFriends" name="participationFriends" 
                            placeholder="Enter specific friends who can take (be invited)"
                            style="height: 32px;"/>
                    </div><!-- .ctrl-wrapper -->
    	                    
    	            <?php 
    				    $this->renderPartial("/controls/autocomplete", array(
                            'searchUrl' => $baseurl . '/index.php/user/friends',
                            'targetDivId' => "participationFriendsDiv",
                            'minLength' => 1,
                            'data' => $friends
                        ));
    			    ?>
			    </div>
			    <?php }?>
                <?php if ($user->isAdmin()) {?>
			    <div class="ctrl-box">
                    <label class="ctrl-label badge"><input type="checkbox" name="badge" value="1" <?php if (!empty($list->badge)) {?>checked="checked"<?php }?> /> Official Badge</label>
                </div>
                <?php }?>
                
                <div class="ctrl-box">
					<div class="ctrl-label">Optional photo credit:</div>
				</div>
                <div class="ctrl-box">
                    <label style="float:left;">Link title: </label>
                    <div class="ctrl-wrapper ctrl-list-title" style="float:left; width:498px!important;margin:-6px 5px 5px 5px;">
                        <input tabindex="11" type="text" value="<?php if ($list) { echo $list->link_title;} ?>" id="link_title" class="photoCredit" name="link_title" style="height:32px;" maxlength="30" />
                    </div>
                    <span class="ctrl-wrapper-error"></span>
			    </div>
                <div class="ctrl-box">
                    <label style="float:left;">Link: </label>
                    <div class="ctrl-wrapper ctrl-list-title" style="float:left; width:532px!important;margin:-6px 5px 5px 5px;">
                        <input tabindex="12" type="text" value="<?php if ($list) { echo $list->link;} ?>" id="listLink" name="link" class="photoCredit" style="height:32px;" maxlength="255" />
                    </div>
                    <span class="ctrl-wrapper-error"></span>
			    </div>
            </fieldset>
            
            
            <div class="clear"></div>
			<div class="ctrl-box ctrl-box-button">
				<input tabindex="7" type="submit" value="SAVE" class="blue-btn ctrl-button" />
				<div tabindex="8" class="gray-btn ctrl-button" onclick="javascript:history.go(-1);">Cancel</div>
				<img src="<?php echo $baseurl?>/images/loading.gif"	style="float: right; margin: 5px; display: none;" id="saveListAjaxLoading" /> 
				<?php if ($list) { ?>
					<?php if ($is_public) { ?>
						<div class="gray-btn ctrl-button ctrl-button-disable" style="margin-right: 80px;">Delete List</div>               
					<?php } else {?>
						<a class="gray-btn ctrl-button" style="margin-right: 80px;" onclick="return deletelist('<?php echo $list->title; ?>');" href="<?php echo $baseurl?>/index.php/preferences/deletelist?listid=<?php echo $list->id;?>&user_id=<?php echo $user_id;?>">Delete List</a>
					<?php } ?>
				<?php } ?>
			</div>
			
			</form>
			
		</div><!-- .liscreation -->		
	</div>
</div>


<?php if (!empty($help)) {?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/gallery.css" />
    <link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
    <script>
        $(function(){
            $.fancybox({
               'href' :'#modalHelp',
               afterShow: function() {},
               beforeClose: function() {}
           });
     });   
    </script>
    <div class="customModal clear" id="modalHelp" style="display:none;">
        <div id="galleryContainer" class="galleryContainer">
            <h3>Welcome to Wayo</h3>
            <div id="gallery" class="content">
                <div class="slideshow-container help" style="height: 130px;">
                    <p>Please check out a brief overview of how to use Wayo:</p>
                    <p><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf">http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf</a></p>
                    <p>The overview is always available on the Help page.</p>
                    <p>Have fun!</p>
                </div>
            </div>
        </div>
    </div>
<?php }?>
