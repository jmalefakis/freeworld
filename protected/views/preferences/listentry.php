<script>
jQuery(document).ready(function(){
    $('title').html('List Entries');
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  
  }); 
</script>
<?php
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid  = Yii::app()->user->getID();
$path =  Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url  = $url1[0];
$siteurl = Yii::app()->params['siteurl'];
?>
 <input type="hidden" name="onclick" value="personalclick" id="onclick"/>
 <input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>"/>
 <input type="hidden" id ="hidden_baseurl" value="<?php echo $baseurl ?>"/>
<div class="featured_list_section_internal">
<div class="widthauto">
    <?php //$this->renderPartial('sidenavigation'); ?>
         <div class="outerdiv" id="liscreation">
 <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$siteurl.'/index.php/preferences/listEntry?listid='.$listid,
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',
        'onsubmit'=>'return list_createEntry();',
        'validateOnSubmit'=>true,
     ),
      ));
	  
?>
        
             <div class="leftdiv_listentry">
                 <div class="content_part_left">
                     <span><?php echo $list_detail['title'];?></span>
                 </div>
                 <div id="response" class="image_outer_listtitle">
                     
                     <?php if(strpos($list_detail['image'], 'profile/') === 0){ ?>
                        <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $list_detail['image'] ?>" />
                    <?php } else if(strpos($list_detail['image'], 'freeworld/') === 0){ ?>
                        <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $list_detail['image'] ?>" />
                    <?php } else{ ?>
                        <img class="listdefaultimg" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'titleimages/list_thumbs/official/small/' . $list_detail['image'] . '?w=368&h=238'; ?>" />
                    <?php } ?>
                        
                     
                 </div>
                 <div class="listcreated_listentry">
                     <span class="createdspan_listentry">Created by:&nbsp;&nbsp;</span><span class="createname_listentry"><?php echo ($userinfo['firstname'].' '.$userinfo['lastname']);?></span><br/>
                     <span class="createdspan_listentry">Created on:&nbsp;&nbsp;</span><span class="createname_listentry"><?php echo date('m/d/Y', strtotime($list_detail['modified']));?></span><br/>
                     <span class="createdspan_listentry">Contributors:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo count($list_Taken);?></span><br/>
                     <span class="createdspan_listentry">Topic:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo $listtopic;?></span><br/>
                     <div class="instriuctiondiv_list">
                          <span class="createdspan_listentry">Instructions:&nbsp;&nbsp;</span ><span class="createname_listentry"><?php echo $list_detail['instruction'];;?></span><br/>
                     </div>
                 </div>
             </div>
             <div class="rightdiv_listentry">
                 <div class="contentpart_right">
                     <span>Please provide entries</span>
                     <!--<span class="whatother_listentry">Check what other think</span>-->
                 </div>
                 
                 <div class="usercomment_listentry">
                     <table style="border-collapse: collapse;" id="myTable">
                         <tr class="tr1_thlistentry">
                             <th class="th1_listentry">#</th>
                             <th class="th2_listentry">Me</th>
                             <th class="th3_listentry">Comments</th>
                         </tr>
                         
                         <?php $odd = '';for($i = 0;$i<10;$i++) { 
                                if($i%2 ==0 )
                                {
                                    $odd = "even";                                   
                                }
                                else
                                {
                                      $odd = "odd";
                                      
                                }
                        ?>
                          <tr class='tr1_<?php echo $odd;?>'>
                             <td class="th1_listentry"><input type="text" name="order_<?php echo $i;?>" value="<?php echo $i+1?>"  id="order<?php echo $i;?>"   class="updatelistOrder oder_listentry<?php echo $odd;?>" onblur="increaseValidation('<?php echo $i+1;?>')"/>   </td>
                             <td class="th2_listentry"><input type="text" name="entry_<?php echo $i;?>"  autocomplete="off"  id="entry<?php echo $i;?>"   class="updatelist entry_listentry<?php echo $odd;?>" <?php if($i==9){ echo "onfocus='addRowListEntry($i)'";}?>  />   </td>
                             <td class="th3_listentry_comment"><input type="text" name="comment_<?php echo $i;?>" autocomplete="off" id="comment<?php echo $i;?>" class="comment_listentry<?php echo $odd;?>"   /> </td>
                         </tr>
                         <?php } ?>
                         
                     </table>
                 </div>
             </div>
             
             <input type="hidden" name="" id="addrow" value="9"/>
             
              <input type="hidden" name="validation" id="validation" value="1"/>
             <input type="hidden" id="no_of_rows" name="no_of_rows" value="10"/>
           <div class="floatright">
               <div id="opnerequred" style="display: none;" class="error_listentry">
                   This field is required
               </div>
                   <span onclick="javascript:history.go(-1);" class="cursorpointer">
                      <div class="canclespan_listtitle"> CANCEL</div>
                   </span>
                 <input type="submit" value="SUBMIT" name="submit" class="uploadimg_listEntry"/>
             </div>  

<?php $this->endWidget(); ?>
             
    </div>
</div>
</div>