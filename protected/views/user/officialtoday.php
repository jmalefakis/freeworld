<?php $baseurl = Yii::app()->request->baseUrl; 
      $userid = Yii::app()->user->getID();
?>
<div class="widthauto">    
        <?php $this->renderPartial('sidenavigation'); ?> 
    
    
    <div class="official_today">
        
        <div class="upper_official_today">
            <div class="floatleft">
<?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/preferences/officialtoday',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',            
        'validateOnSubmit'=>true,
     ),
      )); 
?>  
            <div class="official_today_date">
                <div class="official_today_sortdate" style="width: 113px;">
                    <span> Sort by date</span>   
                </div>
                <div class="floatleft">
                    <?php                          
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[birthdate]', 
                    'value'=> '', 
                    // additional javascript options for the date picker plugin
                    'options'=>array(                                           
                         'yearRange'=> '-102:+0',
                        'dateFormat'=>'yy-mm-dd',                        
                        //'defaultDate'=>'01/01/2012',
                        //'yearRange'=>'1960:2012', 
                        'showAnim'=>'fold',                        
                        'debug'=>true,
                        'showOn'=> "button",
                        'buttonImage'=> "$baseurl/images/freeworld/right-arrow.png",
                        'buttonImageOnly'=> true
                    ),
                    'htmlOptions'=>array(
                         'style'=>'height:0px;width:0px;',
                         'readonly'=>"readonly",
                         'onchange'=>"sortbydate('$baseurl')",
                    ),
                )); ?> 
                </div>
            </div>
            
            <div class="official_today_date_top">
                <div class="official_today_sortdate">
                    <span> Sort by topic:</span>   
                </div>
                <div class="floatleft OptionselectBoxContainerdiv">
                   <select onchange="sortbydate()"  id="interest" name="Interest[user]"  class="official_today_droup select-list">
                         <option selected=selected>Select your interest</option>
                         <?php foreach ($interest as $inter){?>
                          <option <?php if(isset($post)){ ?> value="<?php echo $post['interest']; ?>" selected=selected" <?php } else { ?> value="<?php echo $inter->id;?>" <?php } ?> ><?php echo $inter->interest?></option>
                          <?php }?>
                     </select>
                </div>
            </div>
            
                        
<?php $this->endWidget(); ?> 
                
            </div>
            
            <div class="official_search">
                   <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/preferences/search',
        'method'=>'GET',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'searchform',   
        'onsubmit'=>'return searchresult();',
        'validateOnSubmit'=>true,
     ),
      )); 
?>
                <div class="search_list_official">
                    <div class="search_list_div1_official">
                        <input placeholder="Search Preferences"  id="searchres" type="text" name="search"  class="forgotpasinput_official"/>
                    </div>
                    <div class="search_list_div2_official">
                        <input type="image"   src="<?php echo $baseurl;?>/images/freeworld/arrow-1.png" style="height: 27px;"/>
                    </div>  
                    <div id="requred" class="search_list_div3_official" >
                        <span >Please fill first.</span>
                    </div>
                </div>

<?php $this->endWidget(); ?>
                </div>
        </div>
     
        
        <div class="floatleft" style="width: 935px;">   
        
        
        <div class="official_today_div1">
             
            
            
             <?php             
               
                   $countlisthalf    = $countlist/2;
                   
                    if (floor($countlisthalf) != $countlisthalf) { 
                            $countlisthalf = abs(floor($countlisthalf+1));
                    }  
                    
                    
                    $start = 0;
                    for($pp=0;$pp<$countlisthalf;$pp++ )
                    {
                      $start++;
                      $listcol  = $data[$pp]->id;
                      $user_idd = $data[$pp]->user_id;
                      
                     $takenlist = ListTaken::model()->find("user_id = $userid AND list_id = '$listcol'");  
                     $mylist    = ListForm::model()->find("user_id = $userid AND id = '$listcol'");
                      //echo "<pre>";print_r($mylist);exit;
                     //foreach ($data as $value){ 
                     if($data[$pp]->listtype =='public'){                          
                         
               ?>
        <div class="outer_see_list">
         <div class="img_outer_official">
            <div class="latest_image_official">
                <img  src="<?php echo $baseurl;?>/images/<?php echo $data[$pp]->image;?>"/>
            </div>
                <!--<a class="textdecoration" href="<?php //echo $baseurl;?>/index.php/list/takelist?listid=<?php //echo $value->id;?>"> <span>Take this list</span></a>-->
         </div>
            
            <div class="outer_latesttitle_div">
                 <?php if(!empty($mylist))
                    {                        
                  ?>        
                    <div class="latest_title_div_official">
                       <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/preferences/seelist?listid=<?php echo $listcol;?>"><span><?php echo $data[$pp]->title; ?></span></a> <!--<a class="textdecorationblack" href="<?php //echo $baseurl;?>/index.php/list/list_detail?listid=<?php //echo $data[$pp]->id;?>&userid=<?php //echo $value->user_id;?>"></a>-->
                    </div>
                <?php } elseif(empty ($takenlist)) {?>
                    <div class="latest_title_div_official">
                       <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/preferences/takelist?listid=<?php echo $listcol;?>"><span><?php echo $data[$pp]->title; ?></span></a> <!--<a class="textdecorationblack" href="<?php //echo $baseurl;?>/index.php/list/list_detail?listid=<?php //echo $data[$pp]->id;?>&userid=<?php //echo $value->user_id;?>"></a>-->
                    </div>
                <?php } else{ ?>
                    <div class="latest_title_div_official">
                       <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/preferences/detailtakelist?listid=<?php echo $listcol;?>"><span><?php echo $data[$pp]->title; ?></span></a> <!--<a class="textdecorationblack" href="<?php //echo $baseurl;?>/index.php/list/list_detail?listid=<?php //echo $data[$pp]->id;?>&userid=<?php //echo $value->user_id;?>"></a>-->
                    </div>
                <?php }?>
            </div>
            <div class="floatleft">
                    <img id="downarrow<?php echo $data[$pp]->id;?>" onclick="changearrowofficial('<?php echo $data[$pp]->id;?>')" src="<?php echo $baseurl;?>/images/freeworld/down-arrow.png"/>
            </div>
            
            <div style="clear: both;"></div>
            
            <?php if(!empty($mylist))
                    {
                        
              ?>
            
            <div class="floatleft" id="listdetail<?php echo $data[$pp]->id;?>" style="display: none;width: 350px;">
                <div class="already_taken">
                    <span>You have created this list. For stats click the arrow</span>
                </div> 
            
                <div class="maintable">
                    <div class="list_title_div" style="margin-right: 20px;">
                        <table class="tablediv">
                            <tr>
                                <td class="titlediv">
 
                                        <span><?php echo $data[$pp]->title ; ?></span>

                                </td>
                                <?php   $datacolumn = ListColumn::model()->findAll("list_id = '$listcol' AND user_id = '$user_idd'");
                                        $count      = count($datacolumn);
                                        $count      = $count+1;
                                        $i=0; 
                                      foreach ($datacolumn as $column){
                                             if($i==3)
                                             {
                                                 break;
                                             }
                                ?>                        
                                <td class="column_div">
                                            <span> <?php echo $column->value ;?> </span> 
                                            <?php $i++; ?> 
                                </td>
                                 <?php }?>
                            </tr>

                            <tr>
                            <?php $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2)', 'params'=>array(':x'=>$listcol,':y'=>$user_idd)));

                                $temp = 1;$p = 0;
                               foreach ($datarow as $row){ 

                                  if($p==3)
                                   {
                                      break;
                                   }
                              ?>
                                <td class="row_div">
                                    <span> <?php echo $row->value ;?> </span>
                                </td>
                               <?php 
                               if($count<4){
                                    if($temp%$count==0)
                                    {
                                        echo "</tr><tr>";
                                         $p++;
                                    }
                               }
                                else
                                {
                                    if($temp%4==0)
                                    {
                                     echo "</tr><tr>";
                                     $p++;
                                    }
                                }
                                $temp++;
                               ?>

                              <?php  }  ?>
                           </tr>

                        </table>
                   </div>
                
            </div>
            </div>
            
            <?php } elseif(empty($takenlist))
            {?>
                <div class="floatleft" id="listdetail<?php echo $data[$pp]->id;?>" style="display: none;width: 350px;">
                <div class="already_taken">
                    <span>What do you think? Click on the text to get started</span>
                </div> 
            
                <div class="official_today_list">
                  
                </div>
                
              </div>
            <?php }
                        
            else { ?>
                <div class="floatleft" id="listdetail<?php echo $data[$pp]->id;?>" style="display: none;width: 350px;">
                     <div class="already_taken">
                        <span>You have already taken this list. For stats click the arrow</span>
                    </div> 
                    <div class="maintable">
                    <div class="list_title_div" style="margin-right: 20px;">
                        <table class="tablediv">
                            <tr>
                                <td class="titlediv">
 
                                        <span><?php echo $data[$pp]->title ; ?></span>

                                </td>
                                <?php   $datacolumn = ListColumn::model()->findAll("list_id = '$listcol' AND user_id = '$user_idd'");
                                        $count      = count($datacolumn);
                                        $count      = $count+1;
                                        $i=0; 
                                      foreach ($datacolumn as $column){
                                             if($i==3)
                                             {
                                                 break;
                                             }
                                ?>                        
                                <td class="column_div">
                                            <span> <?php echo $column->value ;?> </span> 
                                            <?php $i++; ?> 
                                </td>
                                 <?php }?>
                            </tr>

                            <tr>
                            <?php $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2)', 'params'=>array(':x'=>$listcol,':y'=>$user_idd)));

                                $temp = 1;$p = 0;
                               foreach ($datarow as $row){ 

                                  if($p==3)
                                   {
                                      break;
                                   }
                              ?>
                                <td class="row_div">
                                    <span> <?php echo $row->value ;?> </span>
                                </td>
                               <?php 
                               if($count<4){
                                    if($temp%$count==0)
                                    {
                                        echo "</tr><tr>";
                                         $p++;
                                    }
                               }
                                else
                                {
                                    if($temp%4==0)
                                    {
                                     echo "</tr><tr>";
                                     $p++;
                                    }
                                }
                                $temp++;
                               ?>

                              <?php  }  ?>
                           </tr>

                        </table>
                   </div>

                </div> 
            
            
                <div class="official_today_list">
                  
                </div>
                
            </div>
            <?php } ?>
        </div>
            
            <?php }
                  
               if($start==$countlisthalf)
               {   
                   $countlisthalf = $countlist;
               ?>
                  
              </div>
                <div class="official_today_div2">           
                  
              <?php }
                
             }?>
            
          </div>
            
            <div class="connecttwitter">
                &nbsp;
            </div>
            <a class="textdecoration"  href="<?php echo $baseurl;?>/index.php/preferences/listtitle">
                <div class="official_today_createnew_margine">
                    <span> Create New</span>
                </div>
             </a>
        
    </div>
        
        <div class="official_today_div3">
            <div class="official_today_date">                
                
            </div>
            <div class="floatleft">
                <img stylse="width: 157px; height: 515px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/advertisment.png"/>
            </div>
        </div>
    </div>
