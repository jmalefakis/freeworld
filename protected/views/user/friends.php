<?php 
if (!isset($_ajax)) { $_ajax = false; }
if (!isset($_popup)) { $_popup = false; }
if (!isset($container)) { $container = 'contentdiv'; }
if (!isset($current_user)) { $current_user = Yii::app()->user->getID(); }
if (!isset($current_page)) { $current_page = 'friends'; }

$baseurl = Yii::app()->request->baseUrl;
$user = User::model()->findByPk($current_user);

?>

<script>
$(function() {	
    $('title').html('My Friends & followed Companies');
    // initPagination($("#_friends"), "<?php echo $baseurl;?>/index.php/user/myFriendsPagination");

	<?php if ($user->isMember()) { ?>
	$('.div-content, .company-content').hover(function() {
		$(this).addClass('active')
			.find('.options-menu').show();
	}, function() {
		$(this).removeClass('active')
			.find('.options-menu').hide()
			.find('.options-menu-container').hide();
	});
	<?php } ?>
	
	initGlobals(
		'<?php echo $current_user; ?>',
		{
			'<?php echo $current_page; ?>': {
				pageno: 1,
				maxpageid: <?php echo $pages; ?>,
				container: '#<?php echo $container; ?>',
				action: 'user/myCreatedPagination'
			}
		}
	);
	
	FW.baseurl = '<?php echo Yii::app()->request->baseUrl; ?>';
	FW.users[<?php echo $current_user; ?>].currentPage = '<?php echo $current_page; ?>';

}); //ready

function showOptionsMenu(el, id) {
	$('#contextMenu_' + id).toggle();
}

function showOptionsMenu2(el, id) {
	$('#contextMenu2_' + id).toggle();
}

var toggleNotificationsFlag = false;
function toggleNotificationsOn(el, id) {
	if (toggleNotificationsFlag) return;

	toggleNotificationsFlag = true;
	
	$.ajax({
		type: 'POST',
		url: '<?php echo $baseurl; ?>/index.php/user/toggleNotificationsOn?friendid=' + id,
		dataType: 'text',
		data: {
		}, 
		success: function (resp) {
			$(el).toggleClass("check");
			toggleNotificationsFlag = false;
		},
		error: function (error) {
			toggleNotificationsFlag = false;
		}
	});
}

var removeFriendFlag = false;
function removeFriend(itemId, friendId) {
	if (removeFriendFlag) return;

	removeFriendFlag = true;
	
	jQuery.ajax({
		type: 'POST',
		url: '<?php echo $baseurl; ?>/index.php/user/remove_profilefriend',		
		data: {
			value: friendId
		},
		success: function (resp) {
			updateSummaryPanel();
			removeFriendFlag = false;
		},
		error: function (error) {
			removeFriendFlag = false;
		}
	});
	
	$("#peopleItem_" + itemId).fadeOut(500, function() {
		$("#peopleItem_" + itemId).parent().remove();
	});
}

function openProfile(id) {
	window.location = "<?php echo $baseurl; ?>/index.php/user/myDashboard?userid=" + id;
}

var unfollowCompanyFlag = false;
function unfollowCompany(companyId) {
	if (unfollowCompanyFlag) return;

	unfollowCompanyFlag = true;
	
	jQuery.ajax({
		type: 'POST',
		url: '<?php echo $baseurl; ?>/index.php/user/unfollowCompany',		
		data: {
			value: companyId
		},
		success: function (resp) {
		    unfollowCompanyFlag = false;
		},
		error: function (error) {
		    unfollowCompanyFlag = false;
		}
	});
	
	$("#companyItem_" + companyId).fadeOut(500, function() {
		$("#companyItem_" + companyId).parent().remove();
	});
}

</script>

<?php if(!$_popup) { ?>
<input type="hidden" name="hiddenpage" id="pageno_off" value="1" />
<input type="hidden" name="baseurl" value="<?php echo $baseurl?>" id="baseurl" />

<div class="featured_list_section">  
	<div id="wrapper">
	<div id="mainContent">
	<?php
	if ((!$_ajax) && (!$_popup)) {
		$this->renderPartial("/user/summary", array());
	}
	?>
<?php } //if(!$_popup) ?>


		<div id="<?php echo $container; ?>">

			<div class="b-section">
			    <div class="section-title">
			    <?php if ($user->isMember()) { ?>
			        Friends
			    <?php } else { ?>
			        Followers
			    <?php } ?>
			    </div>
			</div>

			<div class="user-section">
				<div  class="box-shadow-top">	
					<div class="box-shadow-bottom">  
						<div class="user-block">
						
							<div class="section-table">
							<?php
							$ic = 0;
							foreach ($friends as $friend) 
                            {
                                $friendUser = $friend->friendUser;
                                if ($user->isCompany()) $friendUser = $friend->myUser; 
                                
								$photo = $baseurl . "/images/default.jpg";
								if ($friendUser["image"] != "") {
									$photo = $baseurl . "/images/profile/user_thumbs/dashboard/" . $friendUser["image"] . '?w=70&h=70';
								}
								$fullName = $friendUser->getFullName();
							?>
							
							    <?php 
							    if ($ic % 2 == 0) 
                                {
							    ?>
							    
							    <div class="friend-row">
							    
							    <?php 
                                }
							    ?>
							
								<div class="greyborder" style="padding: 3px 0; width: 50%; float: left;">
									<div id="peopleItem_<?php echo $friend['id']; ?>" class="div-content">
                                        <?php if ($friendUser['badge']) {?>
                                        <span class="list-box-badge-eagle" title="Wayo Official"></span>
                                        <?php }?>
										<img src="<?php echo $photo; ?>" class="friend-img-short" alt="<?php echo $fullName; ?>"/>
										<div class="friend-ihfo-short">
											<p class="friend-name-short">
												<a href="javascript:void(0);" onclick="showUserBriefs(<?php echo $friendUser['id']; ?>, '#wrapper')">
													<?php echo StringsHelper::capitalize($fullName); ?>
												</a>
											</p>
											<p class="friend-link-short">
												<?php echo ($friendUser['status_text'] != '' ? $friendUser['status_text'] : 'No status'); ?>
											</p>
										</div>
										<?php if (!$_popup) { ?>
										<div class="options-menu" >
											<span class="opt-button"
												onclick="showOptionsMenu(this, <?php echo $friendUser['id']; ?>);">
												Options
											</span>
											<div id="contextMenu_<?php echo $friendUser['id']; ?>"
												class="options-menu-container">
												<i class="opt-button-angle"></i>
												<div class="options-menu-item-br <?php if ($friend["notifications_on"] == 1) { ?>check<?php }?>"
													onclick="toggleNotificationsOn(this, <?php echo $friend['id']; ?>);">
													Notifications
												</div>
												<div class="options-menu-item"
													onclick="removeFriend(<?php echo $friend['id']; ?>,<?php echo $friendUser['id']; ?>);">
													Unfriend
												</div>
											</div>
										</div>
										<?php } //if (!$_popup) ?>
									</div>
								</div><!-- .greyborder -->
								
								<?php 
								if ($ic % 2 == 1)
								{
								    
								?>
								
								</div>
								
								<?php
								}
								$ic++;
								?>
							
							<?php 
							} //foreach ($friends as $friend)
							
							if (count($friends) == 0) {
							?>
								<div class="nolistfound" style="height: 70px; padding-top: 10px;">
								<?php if ($user->isMember()) { ?>
                			        No friends found
                			    <?php } else { ?>
                			        No followers found                			         
                			    <?php } ?>
								</div>
							<?php 
							}
							?>
							
							<div class="clear"></div>
							
							</div><!-- .section-table -->		
						</div><!-- .user-block -->
					</div>
				</div>
			</div>

			<div class="empty-bl"></div><div class="empty-bl"></div><div class="empty-bl"></div>
			<div class="b-section"><div class="section-title">Companies</div></div>

			<div class="user-section">
				<div  class="box-shadow-top">	
					<div class="box-shadow-bottom">  
						<div class="user-block">
							<table class="section-table">
							<colgroup><col width="50%"><col width="50%"></colgroup>
							<?php 
							$itr = 1;								 
							foreach ($companies as $friend)	{
								$companyFriend = $friend->friendUser;
								
								$photo = $baseurl . "/images/default.jpg";
								if ($companyFriend["image"] != "") {
									$photo = $baseurl . "/images/profile/user_thumbs/dashboard/" . $companyFriend["image"] . '?w=70&h=70';
								}
								$fullName = $companyFriend->getFullName();
							?>
								<?php if ($itr % 2 == 1) { ?><tr class="greyborder"><?php } ?>
									<td>
										<div id="companyItem_<?php echo $companyFriend['id']; ?>" class="company-content">
                                            <?php if ($companyFriend['badge']) {?>
                                            <span class="list-box-badge-eagle" title="Wayo Official"></span>
                                            <?php }?>
											<img src="<?php echo $photo; ?>" class="company-img-short" alt="<?php echo $fullName; ?>"/>
											<div class="company-ihfo-short">
												<p class="company-name-short">
													<a href="javascript:void(0);" onclick="showUserBriefs(<?php echo $companyFriend['id']; ?>, '#wrapper')">
														<?php echo StringsHelper::capitalize($fullName); ?>
													</a>
												</p>
												<p class="company-link-short">
													<?php echo ($companyFriend['status_text'] != '' ? $companyFriend['status_text'] : 'No status'); ?>
												</p>
											</div>
											<?php if (!$_popup) { ?>
    										<div class="options-menu" >
    											<span class="opt-button"
    												onclick="showOptionsMenu2(this, <?php echo $companyFriend['id']; ?>);">
    												Options
    											</span>
    											<div id="contextMenu2_<?php echo $companyFriend['id']; ?>" class="options-menu-container" style="margin-left: 40px;">
    												<i class="opt-button-angle"></i>    											
    												<div class="options-menu-item" style="padding-left: 15px;"
    													onclick="unfollowCompany(<?php echo $companyFriend['id']; ?>);">
    													Unfollow
    												</div>
    											</div>
    										</div>
    										<?php } //if (!$_popup) ?>
										</div>
									</td>
								<?php if ($itr % 2 == 0) { ?></tr><?php } ?>
							<?php
								$itr++; 
							}
							
							if (count($companies) == 0) {
							?>
							<div class="nolistfound" style="height: 70px; padding-top: 10px;">No companies found</div>	
							<?php 
							}
							?>
							
							</table>			
						</div>	
					</div>
				</div>
			</div><!-- .user-section -->

		</div><!-- #contentdiv -->
		
		<div class="officialtodatloding" id="officialtodatlodingdiv">
			<img src="<?php echo $baseurl; ?>/images/loading.gif" />
		</div>

	<?php if (!$_popup) { ?>
	</div><!-- .mainContent -->
	</div><!-- #wrapper -->

	<div style="clear: both;">&nbsp;</div>
</div> <!-- .featured_list_section -->
	<?php } //if (!$_popup) ?>
