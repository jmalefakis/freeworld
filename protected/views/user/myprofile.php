<?php 
$this->renderPartial("/user/summary", array());

$baseurl = Yii::app()->request->getBaseUrl(true);

$isCompany = $user->isCompany();
?>

<script type="text/javascript" src="<?php echo $baseurl; ?>/js/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/jquery.getimagedata.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/moxie.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/plupload.dev.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/plupload/jquery.imgareaselect.pack.js"></script>

<script>
$(function(){
	var nameofuser = $('#nameofuser').val();
	if (nameofuser != '')
		$('title').html(nameofuser + "'s profile");
	
	$('#privacySettings').trigger('change');
    
    $('#deactivate').click(function(){
		$.confirm({
			'title'		: 'Deactivate Confirmation',
			'message'	: 'You are about to deactivate this profile. <br />Reactivation may not be possible! Continue?',
			'buttons'	: {
				'Yes'	: {
					'class'	: 'blue',
					'action': function(){
						$.ajax({
                            url: '<?php echo $baseurl; ?>/index.php/user/deactivate',
                            type: 'POST',
                            success: function(result) {
                                if (result == 1) {
                                    document.location.href = document.location.href;
                                }
                            }
                        });
					}
				},
				'No'	: {
					'class'	: 'gray',
					'action': function(){}	
				}
			}
		});
	});
    
    $(".btn-crop").fancybox({
        'href' :'#modal',
		afterShow: function() { 
            //var ias = $('#uploadedImage').imgAreaSelect({ instance: true, handles: true,  show: true, persistent : true });
            //ias.setOptions({ show: true });
            //ias.update();
            $('#uploadedImage').imgAreaSelect({ 
                aspectRatio: '1:1', 
                handles: true,
                fadeSpeed: 200,
                resizeable:false,
                maxHeight:540,
                maxWidth:540,			
                minHeight:180,
                minWidth:180,
                show: true,
                instance: true,
                x1: 0,
                y1: 0,
                x2:180,
                y2:180,
                onSelectEnd: setInfo
            });
            
        },
		beforeClose: function() {
            $('#uploadedImage').imgAreaSelect({hide:true});
        }
	});
    
    $('#set-crop-btn').click(function(){
        $.ajax({
            url: "<?=Yii::app()->request->baseUrl?>/index.php/preferences/imageCrop",
            data: {url: escape($('#uploadedImage>img').attr('src')), 
                    x:$('#x').val(),
                    y:$('#y').val(),
                    w:$('#w').val(),
                    h:$('#h').val()
            },
            success: function(result) {
                parent.$.fancybox.close();
                $('#uploadedImage').imgAreaSelect({hide:true});
                $('#response').find('img').attr('src', $('#uploadedImage>img').attr('src')+'&r='+Math.floor((Math.random() * 100) + 1));
                $('#uploadedImage').find('img').attr('src', $('#response>img').attr('src')+'&r='+Math.floor((Math.random() * 100) + 1));
                $('#btn-crop').hide();
            }
        });
    });
    
    function setInfo(i, e) {
        $('#x').val(e.x1);
        $('#y').val(e.y1);
        $('#w').val(e.width);
        $('#h').val(e.height);
    }  
}); 

function selectRelation(that) {
	$('.rights-group').hide();
	$('#group-' + $(that).val().toLowerCase()).show();
}

function fancyConfirm(msg, callback) {
    var ret;
    jQuery.fancybox({
        'modal' : true,
        'content' : "<div style=\"margin:1px;width:240px;\">"+msg+"<div style=\"text-align:right;margin-top:10px;\"><input id=\"fancyConfirm_cancel\" style=\"margin:3px;padding:0px;\" type=\"button\" value=\"Cancel\"><input id=\"fancyConfirm_ok\" style=\"margin:3px;padding:0px;\" type=\"button\" value=\"Ok\"></div></div>",
        onComplete : function() {
            jQuery("#fancyConfirm_cancel").click(function() {
                ret = false;
                jQuery.fancybox.close();
            })
            jQuery("#fancyConfirm_ok").click(function() {
                ret = true;
                jQuery.fancybox.close();
            })
        },
        onClosed : function() {
             if (typeof callback == 'function'){ callback.call(this, ret); } ;
        }
    });
}
</script>	   
<?php 
$userid  = Yii::app()->user->getID();
$baseurl = Yii::app()->request->baseUrl;

$cs = Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/datepicker/colorbox.css');
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
	  
$path =  Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url  = $url1[0];
$datebirth   = explode("/",$user['birthdate']) ;

$city_state = $city_state3 = $city_state2 = $city_state1 = '';
if(isset($user['country']) && $user['country']!='')
	$city_state1  = $user['country'];
if(isset($user['state']) && $user['state']!='')
	$city_state2  = $user['state'].",";
if(isset($user['city']) && $user['city']!='')
	$city_state3  = $user['city'].",";

$city_state = rtrim($city_state3.$city_state2.$city_state1,",");

$education = $education3 =  $education2 = $education1 = '';

if(isset($user['degreeyear']) && $user['degreeyear']!='')
	$education1  = $user['degreeyear'];
if(isset($user['degree']) && $user['degree']!='')
	$education2  = $user['degree'].",";
if(isset($user['school']) && $user['school']!='')
	$education3  = $user['school'].",";


$education = rtrim($education3.$education2.$education1,",");

if ($isCompany) {
	$occupation = $user['industry'];
} else {
	if(isset($user['industry']) && ($user['industry']!=''))
		$occupation = $user['company'].",".$user['industry'];
	else
		$occupation = $user['company'];
}

?>
<input type="hidden" name="onclick" value="personalclick" id="onclick" />
<input type="hidden" name="onclick" value="<?php echo $baseurl;?>" id="baseurl" />
<input type="hidden" name="nameofuser" value="<?php echo StringsHelper::capitalize(trim($user['firstname']." ".$user['lastname']));?>" id="nameofuser" />
<input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>" />
<title>
	<?php echo StringsHelper::capitalize($user['firstname'] . ' ' . $user['lastname']);?>
</title>

<div class="featured_list_section"> 
	<div id="wrapper">
   
<?php  
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'listcreate',
	'action' => $baseurl . '/index.php/user/' . ($isCompany ? 'myProfileCompany' : 'myProfile'),
	'method' => 'post',
	'enableAjaxValidation' => true,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'name' => 'listcreate',
		'validateOnSubmit' => true,			
		'onsubmit' => 'return myprofile_vaidate(' . ($isCompany ? 'true' : 'false') . ');',	
	 ),
)); 
?>
		<div class="profile-box profile-box-about<?php if ($isCompany) echo ' profile-company'; ?> box-shadow-top"><div class="box-shadow-bottom">
			<div class="profile-box-header"><?php echo ($isCompany ? 'About My Company' : 'About Me'); ?></div>
			<div class="profile-box-content">
				<!--img class="profile-box-photo" src="" /-->


				<div class="profile-box-photo" id="response">
					<?php if($user['image']!='') { ?>
					<img src="<?php echo $baseurl;?>/images/profile/user_thumbs/listcreation/<?php echo $user['image']?>?w=180&h=180" />
					<?php } else if (strpos($user['gender'],'female') !== false) { ?>
					<img src="<?php echo $baseurl;?>/images/defaultgirl.jpg?w=180&h=180" />
					<?php } else { ?>
					<img src="<?php echo $baseurl;?>/images/defaultboy.jpg?w=180&h=180" />
					<?php } ?>
				</div>
				
				<ul id="image-list"></ul>
				<a id="btn-upload" class="blue-btn btn-upload btn_up">Upload</a>
                <a id="btn-crop" class="blue-btn btn-crop" style="display:none;">Crop</a>
				<div id="image_upolad" class="city_state_error" style="display: none;margin-top: 60px;">Please upload correct image </div>
				
				<?php 
				    $this->renderPartial("/controls/multifileupload", array(
                        'uploaderId' => 'profileImageUploader',
                        'uploadUrl' => $baseurl . "/index.php/user/upoadImageProfile", // TODO: fix "upoad"
                        'deleteUrl' => $baseurl . "/index.php/user/deleteImage",
                        'uploadButtonClientId' => "btn-upload",
                        'responseTargetId' => "response",
                        'responseTargetId2' => "uploadedImage",
                    ));
				?>
				
				<div style="profile-box-container">
					<div class="ctrl-box ctrl-inline">
						<label class="ctrl-label"><?php echo ($isCompany ? 'My Company is' : 'Name'); ?>:</label>
						<div class="ctrl-wrapper ctrl-name">
							<input type="text" id="firstname" name="User[firstname]" placeholder="First Name" value="<?php echo $user['firstname'];?>" />
						</div>
						<?php if (!$isCompany) { ?>
						<div class="ctrl-wrapper ctrl-name">
							<input type="text" id="lastname" placeholder="Last Name" name="User[lastname]" value="<?php echo $user['lastname'];?>" />
						</div>
						<?php } ?>
					</div><!-- .ctrl-box -->
					
					<?php if (!$isCompany) { ?>
					
					<div class="ctrl-box ctrl-inline">
						<label class="ctrl-label">Born:</label>
						<div class="ctrl-wrapper ctrl-month">
							<select id="monthsid" name="User[birthmonth]" placeholder="Month" onchange="monthchange()">
								<option value="" disabled="disabled" selected="selected">Month</option>
							
							<?php
							$months = array('January','February','March','April','May','June','July','August','September','October','November','December');
							$month = (isset($datebirth) && isset($datebirth[0])) ? $datebirth[0] : '';
							foreach($months as $m) {
								?><option <?php if ($month == $m) { ?> selected="selected" <?php } ?>><?php echo $m; ?></option>
							<?php
							}
							?>
							</select> 
						</div><!-- .ctrl-wrapper -->
					
						<div class="ctrl-wrapper ctrl-day">
							<select id="daysid" name="User[birthday]" placeholder="Day">
								<option value="" disabled="disabled" selected="selected">Day</option>
								<?php 
								$day = (isset($datebirth) && isset($datebirth[1])) ? $datebirth[1] : 0;
								for($d = 1; $d < 32; $d++) { ?>
								<option value="<?php echo $d; ?>" <?php if ($day == $d) { ?> selected="selected" <?php } ?> ><?php echo $d; ?></option>
								<?php } ?>
							</select> 
						</div><!-- .ctrl-wrapper -->

						<div class="ctrl-wrapper ctrl-year">
							<input type="text" id="yearsid" name="User[birthyear]" placeholder="Year" value="<?php if(isset($datebirth) && isset($datebirth[2])){ echo $datebirth[2]; } ?>" maxlength="4" />					 
						</div>
					</div><!-- .ctrl-box -->
					
					<?php } else { //if (!$isCompany) ?>

					<div class="ctrl-box ctrl-inline">
						<label class="ctrl-label">Street address:</label>
						<div class="ctrl-wrapper ctrl-street">
							<input type="text" id="street" placeholder="Street address" value="<?php if(isset($user)){ echo $user['street']; }; ?>" maxlength="120" name="street" />
						</div><!-- .ctrl-wrapper -->
					</div><!-- .ctrl-box -->
					
					<?php } //if ($isCompany) ?>


					<div class="ctrl-box ctrl-inline">
						<label class="ctrl-label"><?php echo ($isCompany ? 'Company address' : 'I live in'); ?>:</label>
						<div class="ctrl-wrapper ctrl-country">
							<input type="text" id="country2" placeholder="City, State/Province, Country" value="<?php if(isset($user)) { echo $city_state; }; ?>" maxlength="120" name="city_state" />
						</div><!-- .ctrl-wrapper -->
					</div><!-- .ctrl-box -->


					<?php if (!$isCompany) { ?>
					<div class="ctrl-box ctrl-inline">
						<label class="ctrl-label">I am a:</label>
						<div class="ctrl-wrapper ctrl-wrapper-group-inline">
							<div class="ctrl-checkbox">
								<label>
									<input type="radio" name="gender" id="radiobtn1" value="female" <?php if(isset($user) && ($user['gender'] =='Female' || $user['gender'] =='female' ) ){ echo "checked='true'";} ?> />							  
									<div>Female</div>
								</label>
							</div>
							<div class="ctrl-checkbox">
								<label>
									<input type="radio" name="gender" id="radiobtn2" value="male" <?php if(isset($user) && ($user['gender'] =='Male' || $user['gender'] =='male')){ echo "checked='true'";} ?> />
									<div>Male</div>
								</label>
							</div>
						</div><!-- .ctrl-wrapper-group -->
					</div><!-- .ctrl-box -->
					<?php } //if (!$isCompany) ?>

				</div><!-- .profile-box-container -->
			</div><!-- .profile-box-content -->
		</div></div><!-- .profile-box -->
		
		
		<div class="profile-box profile-box-info box-shadow-top"><div class="box-shadow-bottom">
			<div class="profile-box-header">Detailed Info</div>
			<div class="profile-box-content">

				<div class="ctrl-box" style="overflow: visible; z-index: 100;">
					<label class="ctrl-label">My Interests are:</label>
					<div class="ctrl-wrapper" style="height: auto;">
				<div class="first_name2_interst ctrl-typing" id="div_typing">
					<ul>
						<span id="mainspan">
						<?php
						if (isset($user)) { $countint = explode(',', $user['interest']); }
						if (isset($user) && $user['interest'] != '' ) {
							for($int = 0; $int < count($countint); $int++) { ?>
								<li><span><?php echo $countint[$int]; ?></span><a onclick="removeli(this)" href="javascript:void(0);"><img class='imagespan_profile' src="<?php echo $baseurl;?>/images/cross.png" /></a></li>
						<?php
							}
						}
						?>
						</span>
						<li>
							<img src="<?php echo $baseurl?>/images/ajax-loader.gif" class="loding_image_li" id="loding_li_img"/>
							<input value="<?php if(isset($user)){ echo '';} else { echo 'Click the arrow'; }?>" size="29" name="User[interest]" readonly="readonly"  id="typings" onblur="typing4_profile(this.value)" autocomplete="off" maxlength="120" />
						</li>
						<div class="home_ulli_interest " id="editdiv" style="position: relative; margin-top: -5px;float:left;">
						</div>
					</ul>
					<input type="hidden" class="resettext" id="hiddeninterest" name="interest_list" placeholder="Interests" <?php if(isset($user)){ if(preg_match('/^[,]+/', $user['interest'])) { ?>value="<?php echo $user['interest'];?>"<?php } else { ?> value=",<?php echo $user['interest'];?>" <?php } }?> />						
				</div><!-- .first_name2_interst -->
				
				<div class="floatleft cursorpointer born_s_int">
					<img src="<?php echo $baseurl?>/images/interestdrpdwn.png" onclick="showavailable()" />
					<div class="show_avl_div show_avl_div_prof" id="showdiv_interest" style="display: none;">
						<div class="interestapply">
							<input type="text" name="searchinterst" style="width: 93px;" id="serchint" class="resettext" />
							<span class="searchspan" onclick="searchinterest('<?php echo $baseurl?>')">
								search
							</span>	
						</div>
						<div class="selectallcheckbox">
							<input type="checkbox" name="selectall" class="checkbox_show_select" value="" onclick="selectAllCheckBox(this)" id="selectall"/>
							<span id="selectallspan" style="line-height:30px;"> Select all</span>
						</div>
							   
						<div class="div_scroll">
						<?php foreach($interest as $value_show ) { ?>
							<div class="interest_div_CHECK">
								<input type="checkbox" name="availablecheckbox" value="<?php echo $value_show->interest;?>" <?php if(isset($user) && in_array($value_show->interest,$countint)){?>checked='checked' <?php } ?> id="checkbox<?php echo $value_show->interest;?>" class="checkbox_show"/>
								<span class="interest_span_div"><?php echo $value_show->interest;?></span>
							</div> 

						<?php } ?>
						 </div>
						<div class="interestapply">
							<span class="cursorpointer applyinterest" onclick="applyinterest_myprofile('<?php echo $baseurl;?>')">Apply</span>
						</div>
					</div><!-- #showdiv_interest -->
				</div>
				
				<span class="numaicclass" id="spaninterest" style="width: 200px;">Interests field is missing</span>
					</div><!-- .ctrl-wrapper -->
				</div><!-- .ctrl-box -->

				<div class="clear"></div>
				
				
				<div class="ctrl-box">
					<label class="ctrl-label"><?php echo ($isCompany ? 'We work in' : 'I work for'); ?>:</label>
					<div class="ctrl-wrapper ctrl-industry">
						<input type="text" id="industry5" name="User[occupation]" placeholder="<?php echo ($isCompany ? 'Industry' : 'Company, Industry'); ?>" value="<?php if(isset($user)){ echo $occupation; } ?>" maxlength="120" />
					</div><!-- .ctrl-wrapper -->
				</div><!-- .ctrl-box -->


				<?php if (!$isCompany) { ?>
				<div class="ctrl-box">
					<label class="ctrl-label">I graduated/will graduate from:</label>
					<div class="ctrl-wrapper ctrl-degree" style="margin-bottom: 14px;">
						<input type="text" id="degree2" name="User[education]" placeholder="School, Degree, Year" value="<?php if (isset($user)) { echo $education; } ?>" maxlength="120" />								
					</div><!-- .ctrl-wrapper -->
				</div><!-- .ctrl-box -->

				
				<div class="ctrl-box" id="addmore_div2" <?php if (isset($user) && $user['education_more'] != '') { ?>style="display: block; padding-bottom: 22px;"  <?php } else { ?> style="display: none; padding-bottom: 22px;" <?php } ?>>
					<div class="ctrl-wrapper ctrl-degree">
						<input type="text" id="hiddeneducation" name="education_list" placeholder="School, Degree, Year" value="<?php if (isset($user)) { echo $user['education_more']; } ?>" maxlength="120" />
					</div><!-- .ctrl-wrapper -->
				</div><!-- .ctrl-box -->
				
				
				<div>
					<span class="link" onclick="showmoreeducation()" id="addmor"><?php if(isset($user) && $user['education_more'] == ''){?>Add more<?php } else { ?>Less<?php } ?></span>						  
				</div>
				<?php } //if (!$isCompany) ?>
				
				<div class="clear"></div>
				
			</div><!-- .profile-box-content -->
		</div></div><!-- .profile-box-info -->
		
		
		<div class="profile-box profile-box-account box-shadow-top"><div class="box-shadow-bottom">
			<div class="profile-box-header">My Account</div>
			<div class="profile-box-content">
			
				<div class="ctrl-box ctrl-inline">
					<label class="ctrl-label">My email:</label>
					<div class="ctrl-wrapper ctrl-email">
						<input type="text" id="User_email" name="User[email]" placeholder="Email" value="<?php echo $user['email'];?>" /> 
					</div><!-- .ctrl-wrapper -->
				</div><!-- .ctrl-box -->
			
				<div class="clear" style="padding: 10px 0 16px 0;">
					<a class="link" href="javascript:forgotpassword_popup('<?php echo $baseurl;?>/index.php/user/changePassword');">Change Password</a>
				</div>

				<?php
					$settings = PrivacySettingsHelper::getSettings($userid);
					/*
					if ($isCompany) {
						unset($settings['friends']);
					}
					*/
				?>
				
				<div class="ctrl-box">
					<label class="ctrl-label">Privacy Settings:</label>
					<div class="ctrl-wrapper ctrl-privacy-settings">
						<select id="privacySettings" onchange="selectRelation(this)">
							<option selected="selected"><?php echo implode('</option><option>', array_map('ucfirst', array_keys($settings))); ?></option>
						</select>
					</div><!-- .ctrl-wrapper -->
					<span class="ctrl-label" style="float: left; margin-left: 12px;">can see</span>
                    <div class="clear"></div>
                    <span class="profile-note">Note: Connect with friends of friends by viewing public lists they have taken from your friends' Dashboards.  Pictures and Comments uploaded to public lists are always public.</span>
				</div><!-- .ctrl-box -->

				<div class="ctrl-box">
					<?php
					foreach($settings as $relation => $rights) {
					?>
					<div class="ctrl-wrapper ctrl-wrapper-group2 rights-group" id="group-<?php echo $relation; ?>" style="display:none;">
						<?php foreach($rights as $rname => $rvalue) { ?>
						<div class="ctrl-checkbox">
							<label>
								<input type="checkbox" name="Privacy[<?php echo $relation; ?>][<?php echo $rname; ?>]" <?php echo ($rvalue ? 'checked="checked"' : ''); ?> />
								<div><?php echo StringsHelper::capitalize($rname); ?></div>
							</label>
						</div>
						<?php }	?>
					</div><!-- .ctrl-wrapper-group -->
					<?php }	?>
				</div><!-- .ctrl-box -->
                <div class="ctrl-box">
					<label class="ctrl-label">Notifications:</label>
                    <div class="ctrl-wrapper ctrl-wrapper-group2">
                        <div class="ctrl-checkbox">
                            <label>
                                <input type="checkbox" name="User[emails_notice]" <?php echo ($user['emails_notice'] ? 'checked="checked"' : ''); ?> value="1" />
                                <div style="width:270px;"><?php echo StringsHelper::capitalize('Email Notifications'); ?></div>
                            </label>
                        </div>
                        <div class="ctrl-checkbox">
                            <label <?php if ($isCompany) { echo 'style="display:none;"'; }?>>
                                <input type="checkbox" name="User[emails_weekly]" <?php echo ($user['emails_weekly'] ? 'checked="checked"' : ''); ?> value="1" />
                                <div style="width:270px;"><?php echo StringsHelper::capitalize('Weekly Summary Email'); ?></div>
                            </label>
                            
                        </div>
                    </div>    
                </div><!-- .ctrl-box -->    

			</div><!-- .profile-box-content -->
		</div></div><!-- .profile-box-account -->
		
		
		<div class="clear"></div>

		
		<div class="updateprofile">
			<input type="submit" name="submit_profile" value="UPDATE"  class="blue-btn ctrl-button"
			    style="width: 150px" />
			<?php if (isset ($flashmsg) && $flashmsg != '') { ?>
			<span class="aftersubmitdata">
				<?php echo $flashmsg;?>
			</span>	
			<?php } ?>
		</div>
        <?php if (!Yii::app()->user->getModel()->isAdmin()) {?>
        <div class="link deactivate" id="deactivate">Deactivate</div>
        <?php } else {?>
        <div class="link deactivateAdmin">Deactivate</div>
        <?}?>
		
<?php $this->endWidget(); ?> 
	</div><!-- #wrapper -->
</div><!-- .featured_list_section -->

<div class="cropModal clear" id="modal" style="display:none;">
    <div id="galleryContainer" class="cropContainer">
        <h3>Crop Image</h3>
        <div class="content" id="uploadedImage"></div>
        <a href="javascript:;" class="blue-btn crop-btn-inner" id="set-crop-btn">Crop</a>
        <input type="hidden" id="x" name="x" />
		<input type="hidden" id="y" name="y" />
		<input type="hidden" id="w" name="w" />
		<input type="hidden" id="h" name="h" />
    </div>    
 </div>    
