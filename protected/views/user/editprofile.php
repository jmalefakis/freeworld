
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker/jquery.datepick.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepicker/jquery.datepick.js"></script>
    
<script class="secret-source">
        jQuery(document).ready(function(jQuery) {
          jQuery('#popupDatepicker').datepick({dateFormat:'mm/dd/yy',rangeSeparator:'/',yearRange:'-102:+0',prevText:'<img style="position: absolute; margin-left: 53px;margin-top: 46px;" src=<?php echo Yii::app()->request->baseUrl;?>/images/freeworld/calenderarrow-left.png>',nextText:'<img style="position: absolute; margin-top: 46px;margin-left: -62px;"  src=<?php echo Yii::app()->request->baseUrl;?>/images/freeworld/calenderarrow-right.png>'});        
        });
      </script>
</head>


<div class="edituserprofile_popup">
 <?php
    $baseurl = Yii::app()->request->baseUrl;
    // Get client script
    $cs=Yii::app()->clientScript;
    // Add JS
    $cs->registerScriptFile($baseurl.'/js/countries2.js');
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'editprofile-form',
        'action'=>$baseurl.'/index.php/user/editprofile',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'class'=>'aboutprofileform', 
        'name'=>'editprofileform',  
        'enctype'=> 'multipart/form-data',
        //'validateOnSubmit'=>true,
     ),
      )); 
?>      
        <h2 class="edituserprofile_h2">User Settings</h2>
        <div class="edituserprofile_maindiv">
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1">
                        <span>
                            Firstname
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <input type="text" name="User[firstname]" value="<?php echo $user['firstname'];?>" class="edituserprofile_input"/>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1">
                        <span>
                            Lastname
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <input type="text" name="User[lastname]" value="<?php echo $user['lastname'];?>" class="edituserprofile_input" />
                    </div>
                </div>
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1">
                        <span>
                            Birthdate
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <input readonly="readonly" name="User[birthdate]" class="edituserprofile_input" value="<?php echo $user['birthdate'] ; ?>" type="text" id="popupDatepicker"/>
                        <?php
                             /*$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                             'name'=>'User[birthdate]',
                             'value'=>$user['birthdate'],
                              // additional javascript options for the date picker plugin
                             'options'=>array(
                             'changeMonth'=>true,
                             'dateFormat'=>'mm/dd/yy',
                             'changeYear'=>true,
                             'yearRange'=>'1960:1996', 
                             'showAnim'=>'fold',
                             //'showButtonPanel'=>true,
                             'debug'=>true,
                             ),
                             'htmlOptions'=>array(
                             'readonly'=>"readonly",
                             'class'=>"edituserprofile_input",
                             ),
                             ));*/
                         ?>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv" style="padding-left: 106px;">
                    <div style="margin-top: 2px; width: 190px;" class="edituserprofile_usernamediv1">
                        <span>
                            Change Password
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <input type="password" name="User[password]" value="" class="edituserprofile_input"/>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1" style="padding-left: 14px; width: 128px;">
                        <span>
                            Gender
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <select name="User[gender]" class="edituserprofile_input">
                             <option  <?php if($user['gender'] == 'male' || $user['gender'] == 'Male'){ ?> value="<?php echo $user['gender']; ?>" selected=selected" <?php } else { ?> value="Male" <?php } ?> >Male</option>
                             <option  <?php if($user['gender'] == 'female' || $user['gender'] == 'Female'){ ?> value="<?php echo $user['gender']; ?>" selected=selected" <?php } else { ?> value="Female" <?php } ?> >Female</option>
                        </select>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1" style="padding-left: 10px; width: 131px;">
                        <span>
                                Country
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <select class="edituserprofile_input" onchange="print_state('state',this.selectedIndex);" id="country" name ="User[country]"></select>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1">
                        <span>
                                City/State
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <select class="edituserprofile_input"  name ="User[city_state]" id ="state"></select>
                        <script language="javascript">print_country("country","<?php echo $user['country']?>", "<?php echo $user['city_state']?>");</script>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1" style="margin-left: -8px; width: 150px;">
                        <span>
                            Occupation
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <select name="User[occupation]" class="edituserprofile_input">
                        <option <?php if($user['occupation'] == 'Accounting/Finance'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Accounting/Finance" <?php }  ?> >Accounting/Finance</option>
                        <option <?php if($user['occupation'] == 'Advertising'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Advertising" <?php } ?> >Advertising</option>
                        <option <?php if($user['occupation'] == 'Builder'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Builder" <?php } ?> >Builder</option>
                        <option <?php if($user['occupation'] == 'Computer Related'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Computer Related" <?php } ?> >Computer Related</option>
                        <option <?php if($user['occupation'] == 'Designer'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Designer" <?php } ?> >Designer</option>
                        <option <?php if($user['occupation'] == 'Doctor'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Doctor" <?php } ?> >Doctor</option>
                        <option <?php if($user['occupation'] == 'Engineering/Architecture'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Engineering/Architecture" <?php } ?> >Engineering/Architecture</option>
                        <option <?php if($user['occupation'] == 'Farmer'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Farmer" <?php } ?> >Farmer</option>
                        <option <?php if($user['occupation'] == 'Government'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Government" <?php } ?> >Government</option>
                        <option <?php if($user['occupation'] == 'Industrial'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Industrial" <?php } ?> >Industrial</option>
                        <option <?php if($user['occupation'] == 'Lawyer'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Lawyer" <?php } ?> >Lawyer</option>
                        <option <?php if($user['occupation'] == 'Media'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Media" <?php } ?> >Media</option>
                        <option <?php if($user['occupation'] == 'Political'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Political" <?php } ?> >Political</option>
                        <option <?php if($user['occupation'] == 'Self Employed'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Self Employed" <?php } ?> >Self Employed</option>
                        <option <?php if($user['occupation'] == 'Sports'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Sports" <?php } ?> >Sports</option>
                        <option <?php if($user['occupation'] == 'Student'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Student" <?php } ?> >Student</option>
                        <option <?php if($user['occupation'] == 'Teaching'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Teaching" <?php } ?> >Teaching</option>
                        <option <?php if($user['occupation'] == 'Unemployed'){ ?> value="<?php echo $user['occupation']; ?>" selected=selected" <?php } else { ?> value="Unemployed" <?php } ?> >Unemployed</option>
                    </select>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv">
                    <div class="edituserprofile_usernamediv1">
                        <span>
                               Education
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <select name="User[education]" class="edituserprofile_input">
                            <option  <?php if($user['education'] == 'PhD'){ ?> value="<?php echo $user['education']; ?>" selected=selected" <?php } else { ?> value="PhD" <?php } ?> >PhD</option>
                            <option  <?php if($user['education'] == 'Master'){ ?> value="<?php echo $user['education']; ?>" selected=selected" <?php } else { ?> value="Master" <?php } ?>  >Master</option>
                            <option  <?php if($user['education'] == 'Bachelor'){ ?> value="<?php echo $user['education']; ?>" selected=selected" <?php } else { ?> value="Bachelor" <?php } ?> >Bachelor</option>
                            <option  <?php if($user['education'] == 'College'){ ?> value="<?php echo $user['education']; ?>" selected=selected" <?php } else { ?> value="College" <?php } ?> >College</option>
                            <option  <?php if($user['education'] == 'High School'){ ?> value="<?php echo $user['education']; ?>" selected=selected" <?php } else { ?> value="High School" <?php } ?> >High School</option>
                            <option  <?php if($user['education'] == 'School'){ ?> value="<?php echo $user['education']; ?>" selected=selected" <?php } else { ?> value="School" <?php } ?> >School</option>
                        </select>
                    </div>
                </div>
                <div class="edituserprofile_usernamediv" style="margin-bottom: 60px;padding-left: 105px;">
                    <div class="edituserprofile_usernamediv1" style="padding-top: 5px; width: 192px;">
                        <span>
                            Upload New Image
                        </span>
                    </div>
                    <div class="edituserprofile_usernamediv2">
                        <input style="display: none;" type="file" id="edituserprofile_pic" name="profilepic" value="" class="edituserprofile_image_upload_reg"/> 
                        <img onclick="document.getElementById('edituserprofile_pic').click();" class="edituserprofile_uploadpicture" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/brows-btn.png" />           
                    </div>
                </div>
        </div>
        <div class="edituserprofile_savebutton">
            <a href="javascript:void(0);">
                <input class="image_buttons" type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/buttons/savechanges.png"/>
            </a>
        </div>
        
 <?php $this->endWidget(); ?>      
</div>