<?php if ($user->isCompany()) {?>
<style>
   #accordionHandle3.handleSelected {
        background: url("<?=Yii::app()->request->getBaseUrl(true)?>/img/btn41_h.jpg") no-repeat scroll center center #FFFFFF;
    }
   #accordionHandle3 {
        background: url("<?=Yii::app()->request->getBaseUrl(true)?>/img/btn41.jpg") no-repeat scroll center center #F7F8FA;
        width: 50px;
    } 
</style>    
<?php
}
	$baseurl = Yii::app()->request->getBaseUrl(true);

	$current_user = Yii::app()->user->getModel();
	
	$user_name = $user->firstname . ' ' . $user->lastname;
	
    if ($user->image != '') { 
		$user_photo = $baseurl . '/images/profile/user_thumbs/listcreation/' . $user->image . '?w=128&h=141';
	} else {
		$user_photo = $baseurl . '/images/default.jpg';
	}
	
	$ub = explode('/', $user->birthdate);
	$user_birthdate = '';
	if (isset($ub[0]))
		$user_birthdate = $ub[0];
	if (isset($ub[1]))
		$user_birthdate .= ' ' . $ub[1];
		
	
	$address = '';
	if (isset($user['city']) && $user['city'] != '')
		$address = $user['city'];
	if (isset($user['state']) && $user['state'] != '')
		$address .= ',' . $user['state'];
	if (isset($user['country']) && $user['country'] != '')
		$address .= ',' . $user['country'];
/*
	$occupation = '';
	if (isset($user['company']) && $user['company'] != '')
		$occupation = $user['company'];
	if(isset($user['industry']) && $user['industry'] != '')
		$occupation .= ',' . $user['industry'];
*/
	$occupation = '';
	if ($user->isCompany()) {
		if(isset($user['industry']) && $user['industry'] != '')
			$occupation = $user['industry'];
	} else {
		if (isset($user['company']) && $user['company'] != '')
			$occupation = $user['company'];
		if(isset($user['industry']) && $user['industry'] != '')
			$occupation .= ',' . $user['industry'];
	}
		
		
	$education = '';
	if (isset($user['school']) && $user['school'] != '')
		$education = $user['school'];
	if (isset($user['degree']) && $user['degree'] != '')
		$education .= ',' . $user['degree'];
	if (isset($user['degreeyear']) && $user['degreeyear'] != '')
		$education .= ',' . $user['degreeyear'];
?>

<script>
$("html").click(function(event){
	$('#search-invite-popup-briefs').hide();
});

$('html').on('click', '#search-invite-popup-briefs', function(e) {
    e.preventDefault();
    e.stopPropagation();
});

function showInvitePopupBriefs(event, el, id) {
    event.preventDefault();
    event.stopPropagation();
    
	$me = $('#search-invite-popup-briefs');
	if ($me.css('display') === 'block') {
		$me.animate({opacity:'0.5'}, 200);
		$me.animate({opacity:'1.0'}, 100);
		$me.animate({opacity:'0.5'}, 100);
		$me.animate({opacity:'1.0'}, 100);
	} else {
		if ($me.parent()[0] !== document.body) {
			$(document.body).append($me.detach());
			$me.children('.btn-close-popup').click(function() { $(this).parent().hide(); });
		}
		//console.log($(el).offset());
		var pos = $(el).offset();
		//var pos = $(el).position();
		pos.top += 18;
		pos.left += 18;
		$me.css(pos)
			.fadeIn()
			.children('.btn-send')
			.click(function() {
				inviteUserBriefs(id, el); 
			});
	}

	return false;
}

function inviteUserBriefs(id, el) {
	$.ajax({ 
		type: 'POST',
		url: FW.baseurl +'/index.php/preferences/invitePeople',
		dataType: 'text',
		data: {
			people_id: id,
			message: $('#search-invite-popup-briefs').children('textarea').val()
		}, 
		success: function (resp) {
			el.onclick = null;
			$(el).removeClass('people-invite-button').addClass('people-pending-button').children().text('Pending');
			$('#search-invite-popup-briefs').hide();
		}
	});
}


</script>
<?php //var_dump($privacy); ?>
<div class="user-content">
	<div class="box-shadow-top">	
		<div class="box-shadow-bottom">  
			<div class="user-block-pop"> 
			<ul class="accordion">
				<li data-section-id="dashboard" <?php if ($user->isCompany()) {echo 'style="display:none"';} ?>>
					<div class="user-allinfo position">
                        <?php if ($user->badge) {?>
                            <span class="list-box-badge-eagle" title="Wayo Official"></span>
                        <?php }?>
						<div class="user-allinfo-img"><img src="<?php echo $user_photo; ?>" alt="My Photo" /></div>
						<div class="user-allinfo-content">
							<div class="name-user"><?php echo StringsHelper::capitalize($user_name); ?></div>
							<?php if ($privacy['contact info']) { ?><div class="icon-email"><?php echo $user->email; ?></div><?php } ?>
							<div class="icon-home"><?php echo $address; ?></div>
							<?php if ($user->isCompany()) {?>
								<div class="icon-street"><?php echo $user->street; ?></div>
							<?php } else { ?>
								<div class="icon-birthday"><?php echo $user_birthdate; ?></div>
							<?php } ?>
							<div class="status-user">
								<?php if ($user->id != $current_user->id && $user->isMember() && $current_user->isMember()) { ?>
									<?php if ($is_friend) { ?>
									<div class="people-friend-button">
										<span>Friend</span>
									</div>
									<?php } else if ($is_pending) { ?>
									<div class="people-pending-button">
										<span>Pending</span>
									</div>
									<?php } else { ?>
									<div class="people-invite-button"
									    onclick="showInvitePopupBriefs(event, this, <?php echo $user->id; ?>);">
										<span>Add</span>
									</div>
									<?php } ?>
								<?php } else if ($is_friend && ($user->id != $current_user->id) && $user->isMember() && (!$current_user->isMember())) { ?>
									<div class="company-follower-button">
										<span>Follower</span>
									</div>
								<?php } ?>
								
								<?php echo $user->status_text; ?>
							</div>
						</div>
						
						<div class="invite-popup" id="search-invite-popup-briefs">
							<div class="btn-close-popup" onclick="$(this).parent().hide();" style="top: 26px;"></div>
							<textarea placeholder="Explain why you're inviting them (optional)."></textarea>
							<div class="btn-send"></div>
						</div>
					</div>
				</li>
				<li data-section-id="created">
					<div class="user-allinfo position">
                        <?php if ($user->badge) {?>
                            <span class="list-box-badge-eagle" title="Wayo Official"></span>
                        <?php }?>
						<div class="user-allinfo-img"><img src="<?php echo $user_photo; ?>" alt="My Photo" /></div>
						<div class="user-allinfo-content">
							<div class="name-user"><?php echo StringsHelper::capitalize($user_name); ?></div>
							<?php if ($privacy['contact info']) { ?><div class="icon-email"><?php echo $user->email; ?></div><?php } ?>
							<div class="icon-home"><?php echo $address; ?></div>
							<?php if ($user->isCompany()) {?>
								<div class="icon-street"><?php echo $user->street; ?></div>
							<?php } else { ?>
								<div class="icon-birthday"><?php echo $user_birthdate; ?></div>
							<?php } ?>
							<div class="status-user"><?php echo $user->status_text; ?></div>
						</div>
					</div>
				</li>
				<li data-section-id="taken" <?php if ($user->isCompany()) echo 'style="display:none;"'?>>
					<div class="user-allinfo position">
                        <?php if ($user->badge) {?>
                            <span class="list-box-badge-eagle" title="Wayo Official"></span>
                        <?php }?>
						<div class="user-allinfo-img"><img src="<?php echo $user_photo; ?>" alt="My Photo" /></div>
						<div class="user-allinfo-content">
							<div class="name-user"><?php echo StringsHelper::capitalize($user_name); ?></div>
							<?php if ($privacy['contact info']) { ?><div class="icon-email"><?php echo $user->email; ?></div><?php } ?>
							<div class="icon-home"><?php echo $address; ?></div>
							<?php if ($user->isCompany()) {?>
								<div class="icon-street"><?php echo $user->street; ?></div>
							<?php } else { ?>
								<div class="icon-birthday"><?php echo $user_birthdate; ?></div>
							<?php } ?>
							<div class="status-user"><?php echo $user->status_text; ?></div>
						</div>
					</div>
				</li>
				<li data-section-id="friends">
					<div class="user-allinfo position">
                        <?php if ($user->badge) {?>
                            <span class="list-box-badge-eagle" title="Wayo Official"></span>
                        <?php }?>
						<div class="user-allinfo-img"><img src="<?php echo $user_photo; ?>" alt="My Photo" /></div>
						<div class="user-allinfo-content">
							<div class="name-user"><?php echo StringsHelper::capitalize($user_name); ?></div>
							<?php if ($privacy['contact info']) { ?><div class="icon-email"><?php echo $user->email; ?></div><?php } ?>
							<div class="icon-home"><?php echo $address; ?></div>
							<?php if ($user->isCompany()) {?>
								<div class="icon-street"><?php echo $user->street; ?></div>
							<?php } else { ?>
								<div class="icon-birthday"><?php echo $user_birthdate; ?></div>
							<?php } ?>
							<div class="status-user"><?php echo $user->status_text; ?></div>
						</div>
					</div>
				</li>
				<li data-section-id="aboutMe">
					<div class="user-allinfo position">
                        <?php if ($user->badge) {?>
                            <span class="list-box-badge-eagle" title="Wayo Official"></span>
                        <?php }?>
						<div class="user-allinfo-img"><img src="<?php echo $user_photo; ?>" alt="My Photo" /></div>
						<div class="user-allinfo-content">
							<div class="name-user"><?php echo StringsHelper::capitalize($user_name); ?></div>
							<?php if ($privacy['contact info']) { ?><div class="icon-email"><?php echo $user->email; ?></div><?php } ?>
							<div class="icon-home"><?php echo $address; ?></div>
							<?php if ($user->isCompany()) {?>
								<div class="icon-street"><?php echo $user->street; ?></div>
							<?php } else { ?>
								<div class="icon-birthday"><?php echo $user_birthdate; ?></div>
							<?php } ?>
							<div class="status-user"><?php echo $user->status_text; ?></div>
						</div>
					</div>
				</li>
			</ul><!-- .accordion -->
			<div class="btn-close-popup" onclick="closeUserBriefs();"></div>

			<div class="clear"></div>
			</div>
		</div>
	</div>
</div><!-- .user-content -->

<ul class="content list-boxes">
	
	<?php if ($privacy['dashboard']) { ?>
		<li class="page-section" style="display: none;">
            <?php if (!$user->isCompany()) { echo $output['favorited']; }?>
		</li><!-- #sectionFavorited -->
		<li class="page-section" style="display: none;">
			<?php echo $output['created']; ?>
		</li><!-- #sectionCreated -->
		<li class="page-section" style="display: none;">
			<?php echo $output['taken']; ?>
		</li><!-- #sectionTaken -->
		<li class="page-section" style="display: none;">
			<?php echo $output['friends']; ?>
		</li><!-- #sectionFriends -->
	<?php } ?>
	
	<?php if ($privacy['about me']) { 
	?>
		<li class="page-section" style="display: none;">
			<div class="more-info-about-user" id="sectionAboutMe">
			<?php if ($user->isCompany()) {?>
				<p><span style="font-size: smaller">We work in:</span> <?php echo $occupation; ?></p>
			<?php } else { ?>
				<p><span style="font-size: smaller">I am a:</span> <?php echo $user->gender; ?></p>
				<p><span style="font-size: smaller">Birthday:</span> <?php echo $user_birthdate; ?></p>
			<?php } ?>
				<p><span style="font-size: smaller">Interests:</span> <?php echo $user->interest; ?></p>
			<?php if ($user->isCompany()) {?>
				<p><span style="font-size: smaller">Street:</span> <?php echo $user->street; ?></p>
			<?php } else { ?>
				<p><span style="font-size: smaller">I work for:</span> <?php echo $occupation; ?></p>
				<p><span style="font-size: smaller">Graduated:</span> <?php echo $education; ?></p>
			<?php } ?>
				<p><span style="font-size: smaller">Location:</span> <?php echo $address; ?></p>
				<?php if ($privacy['contact info']) { ?><p><span style="font-size: smaller">Email:</span> <a href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a></p><?php } ?>
			</div>
			<!--
			<div class="more-info-about-user" id="sectionAboutMe">
				<p><b>Hello Friend,</b></p>
				<p>My name is <?php echo $user_name; ?>.</p>

				<?php if ($privacy['about me']) { ?>
				<p>I was born on <?php echo $user_birthdate; ?>, I graduated from <?php echo $education; ?> and currently I live in <?php echo $address; ?>. 
				I work for <?php echo $occupation; ?>. My Interest are in arts, sports.</p>
				<?php } ?>

				<?php if ($privacy['contact info']) { ?><p>If you would to get in touch with me please shoot me an email to <a href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a></p><?php } ?>
			</div>
			-->
		</li><!-- #sectionAboutMe -->
	<?php } ?>
	
</ul><!-- .list-boxes -->

<script type="text/javascript">
$(function() {

	initGlobals(
		'<?php echo $user->id; ?>',
		{
			'aboutMe': {
				container: '#sectionAboutMe'
			}
		}
	);
    
    $(window).scroll(function() {
		initPagination('<?php echo $user->id; ?>', FW.users['<?php echo $user->id; ?>'].currentPage);
	});
	
	$('.accordion').hrzAccordion({
		handlePosition: 'right',
		containerClass: 'user-info-wrapper',
		handleClass: 'handle-btn',
        openOnLoad: <?php if ($user->isCompany()) { echo 2; } else {echo 1;} ?>,
		fixedWidth: '<?php if ($user->isCompany()) { echo 768; } else {echo 728;} ?>',
		closeOpenAnimation: 2,
		completeAction: function() {
			var sectionId = $(this).parent().data('section-id');
			switchUserBriefs(sectionId);
		}
	});
}); //ready
</script>
