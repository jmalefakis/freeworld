<?php 

$baseurl = Yii::app()->request->baseUrl;

// target --> <div class="block-hidden">

?>

<div id="invitePopup_<?php echo $author["id"]; ?>"
	class="alisename-info-block" onclick="event.stopPropagation();">
	<div class="alisename-info-block-cont">
		<div class="_ov">
			<?php if ($author['image'] != '') 
			{
			    ?>
			<img
				src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $author['image'];?>?w=100&h=100"
				alt="Author's Photo" />
			<?php 
			}
			else
			{
			    ?>
			<img alt="Author's Photo"
				src="<?php echo $baseurl;?>/images/default.jpg" />
			<?php 
			}
			?>

			<div class="user-sh-info">
				<div class="name-us">
					<a
						href="javascript:showUserBriefs(<?php echo $author["id"]; ?>, '<?php echo $contentSel; ?>');">
						<?php echo StringsHelper::capitalize($author->getFullName()); ?>
					</a>
				</div>
				<div class="icon-home-u margin-none">
					<?php 
					$location = $author["city"] . ',' . $author["state"] . ',' . $author["country"];
					if (strlen($location) > 12)
					{
					    $location = substr($location, 0, 12) . "...";
					}
					echo $location;
					?>
				</div>
				<div class="icon-birthday-u margin-none">
					<?php 
					if ($author["birthdate"])
					{
					    $bp = explode('/', $author["birthdate"]);
					    $birthday = '';
					    if (isset($bp[0])) $birthday = $bp[0];
					    if (isset($bp[1])) $birthday .= ' ' . $bp[1];
					    echo $birthday;
					}
					?>
				</div>
			</div>

			<?php 
			if ($author->isCompany())
			{
			    ?>

			<div class="btn-invite-blue"
				onclick="listAuthorFollowClick(this, <?php echo $author["id"]; ?>);">Follow</div>

			<?php 
			}
			elseif (!$isPending)
			{

			    ?>

			<div class="btn-invite-blue"
				onclick="listAuthorInviteClick(this, <?php echo $author["id"]; ?>);">Invite</div>

			<?php
			} else {
			?>

			<div id="invitePendingBtn_<?php echo $author["id"]; ?>"
				class="pending-btn">Pending</div>

			<?php 
			}
			?>
		</div>

		<div id="inviteSendConfirmation_<?php echo $author["id"]; ?>"
			style="display: none" class="attention">
			<span>Your incitation has been send</span>
		</div>
	</div>

	<div class="_ov" id="inviteMessageBlock_<?php echo $author["id"]; ?>"
		style="display: none;">
		<textarea id="inviteMessage_<?php echo $author["id"]; ?>"
			placeholder="Explain why you're inviting them" class="userMessfield"></textarea>
		<div class="btn-invite-blue send-btn"
			onclick="listAuthorInviteSendClick(this, <?php echo $author["id"]; ?>);">Send</div>
	</div>

	<span class="cl-kr"
		onclick="$('#invitePopup_<?php echo $author["id"]; ?>').remove();"></span>
	<i class="alisename-info-angle"></i>
</div>
