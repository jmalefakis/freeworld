<div class="mainouterdiv">
    <div class="forgot_pass_div1">
        <span>
            Change password #
        </span>
    </div>
 <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash' . $key . '">' . $message . "</div>\n";
    }
?>
    
    <div id="q3" class="changepas_span">
        
    </div>
<?php
    $baseurl = Yii::app()->request->baseUrl; 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'action'=>$baseurl.'/index.php/user/setting',
        'method'=>'post',        
        'enableClientValidation'=>true,
        'htmlOptions'=>array(        
        'name'=>'mform',  
        'enctype'=> 'multipart/form-data',
        'onsubmit'=>'return changepassword();',
        'validateOnSubmit'=>true,
 ),
    )); 
?> 
        <div class="change_pass_div2">
            <span class="forgot_pass_span" id="oldspan">
                Enter your old password :
            </span>
            <input type="password" id="oldpasword" name="oldpasword" value="" class="forgotpasinput"/>
              <span id="spanoldpwd" style="color: red;">
                  
              </span>
        </div>
        <div class="change_pass_div2">
            <span class="change_password" id="newspan">
                Enter your new password :
            </span>
            <input type="password" id="newpasword" name="newpasword" value="" class="forgotpasinput"/>
              <span id="spannewpwd" style="color: red;">
                  
              </span>      
        </div>
        <div class="change_pass_div2">
            <span class="change_password2" id="newrespan">
                Re-enter your new password :
            </span>
            <input type="password" id="connewpasword" name="connewpasword" value="" class="forgotpasinput"/>
              <span id="spanconnewpwd" style="color: red;">
                  
              </span>     
        </div>
    
    <div class="changepassword_submit">
            <input type="submit" name="submit" value="Submit"/>
    </div>
 <?php $this->endWidget(); ?>  
    <div id="emailreq" class="forgot_pass_div3">
    
    </div>
    <?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash' . $key . '">' . $message . "</div>\n";
    }
    ?>
</div>