<?php 
$this->renderPartial("/user/summary", array());

$baseurl = Yii::app()->request->baseUrl;

$userid = Yii::app()->user->getID();
$user = User::model()->find("id = '$userid'");

$invitations = $user->friendsNotifications();
$lists = $user->listsNotifications();
$system = $user->systemNotifications();

$c = new CDbCriteria();
$c->addSearchCondition('read_notification', '0', false);
$c->addInCondition('notification_type', array('friends', 'invitation','invite_list'));
$newInvitations = $user->notifications($c);

$c = new CDbCriteria();
$c->addSearchCondition('read_notification', '0', false);
$c->addInCondition('notification_type', array('lists', 'my_created_lists', 'my_taken_lists', 'opinion'));
$newLists = $user->notifications($c);

$c = new CDbCriteria();
$c->addSearchCondition('read_notification', '0', false);
$c->addSearchCondition('notification_type', 'system', false);
$newSystem = $user->notifications($c);

?>

<script
	src="<?php echo $baseurl;?>/js/jquery-ui-1.10.3.custom.js"></script>

<script>
	$(function() {
		$("#tabs" ).tabs({
		    activate: function(event, ui) {
		        var budget = ui.newTab.find("span");
		        if (budget.length > 0 && budget.css("display") != "none") {		        
    		        $.ajax({
    					type: 'POST',
    					url: '<?php echo $baseurl; ?>/index.php/user/readNotifications?tab='
    						+ ui.newTab.attr("id"),
    					dataType: 'text',
    					data: { }, 
    					success: function (resp) {
							getNotifications();
    					    updateSummaryPanel();
    					}
    				});
		        }

		        ui.oldTab.find("span").hide();
		    }
		});

		updateSummaryPanel();		
	});

	function accept(id, fid) {
	    $("#noti_" + id + " .btn-change").hide();

	    $.ajax({
			type: 'POST',
			url: '<?php echo $baseurl; ?>/index.php/user/acceptfrndrequest?userid='
				+ fid + "&notificationid=" + id,
			dataType: 'text',
			data: { }, 
			success: function (resp) {
			    $("#noti_" + id + " .accepted").show();
			    updateSummaryPanel();
			}
		});
	}

	function reject(id, fid) {
	    $("#noti_" + id + " .btn-change").hide();

	    $.ajax({
			type: 'POST',
			url: '<?php echo $baseurl; ?>/index.php/user/rejectfrndrequest?userid='
				+ fid + "&notificationid=" + id,
			dataType: 'text',
			data: { }, 
			success: function (resp) {
			    $("#noti_" + id + " .rejected").show();
			    updateSummaryPanel();
			}
		});
	}

	function undoAccept(id) {
	    $("#noti_" + id + " .accepted").hide();

	    $.ajax({
			type: 'POST',
			url: "<?php echo $baseurl; ?>/index.php/user/undoAcceptFriend?notificationid=" + id,
			dataType: 'text',
			data: { }, 
			success: function (resp) {
			    $("#noti_" + id + " .btn-change").show();
			    updateSummaryPanel();
			}
		});
	}

	function undoReject(id) {
	    $("#noti_" + id + " .rejected").hide();
	    
	    $.ajax({
			type: 'POST',
			url: "<?php echo $baseurl; ?>/index.php/user/undoRejectFriend?notificationid=" + id,
			dataType: 'text',
			data: { }, 
			success: function (resp) {
			    $("#noti_" + id + " .btn-change").show();
			    updateSummaryPanel();
			}
		});
	}
</script>

<div class="user-content">
	<div class="box-shadow-top">
		<div class="box-shadow-bottom">
			<div class="user-tabs">
				<div id="tabs">
					<ul>
						<li id="nInvitationsTab"><a href="#tabs-1">
					        Invitations 
					        <?php if (count($newInvitations) > 0) { ?>
					        <span><?php echo count($newInvitations); ?></span>
						    <?php } ?>
						</a></li>
						<li id="nListsTab"><a href="#tabs-2">
						    List 
						    <?php if (count($newLists) > 0) { ?>
						    <span><?php echo count($newLists); ?></span>
						    <?php } ?>
						</a></li>
						<li id="nSystemTab"><a href="#tabs-3">
						    System 
						    <?php if (count($newSystem) > 0) { ?>
						    <span><?php echo count($newSystem); ?></span>
						    <?php } ?>
						</a></li>
					</ul>

					<div id="tabs-1">
						<table class="user-invitation">
							<colgroup>
								<col width="2%">
								<col width="76%">
								<col width="22%">
							</colgroup>
							
							<?php 
							foreach ($invitations as $inoti)
							{
							    $sender = $inoti->userSender;
							    
							    $image = "";
							    if ($sender['image'] != '')
							        $image = $baseurl . "/images/profile/user_thumbs/listcreation/" . $sender['image'] . "?w=70&h=70";
							    else $image = $baseurl . "/images/default.jpg";
							    
							    $message = $inoti["message"];
							    if ($message == "") 
							    {
							        $message = "wants to become a friend";
							    } 
							    else if ($message == "You are friends with ") 
                                {
                                    $message = " is your friend";
                                }
							?>
							
							<tr class="greyborder">
								<td class="frs-col"><img src="<?php echo $image; ?>"
									alt="<?php echo $sender->getFullName(); ?>" />
								</td>
								<td class="sec-col">
									<div class="message-info" id="noti_<?php echo $inoti["id"];?>">
										<p>
										    <a href="<?php echo $baseurl; ?>/index.php/user/myDashboard?userid=<?php echo $sender['id']; ?>">
										        <span><?php echo $sender->getFullName(); ?></span>
										    </a>
											<?php echo $message; ?>
											
											<?php 
											if ($inoti["notification_type"] == "invitation" || $inoti["notification_type"] == "invite_list")
											{
											    $list = ListForm::model()->findByPk($inoti->list_id);
											?>
											"<a class="capitalize" href="<?php echo $baseurl; ?>/index.php/preferences/listdetail?listid=<?php echo $list->id; ?>">
        				                        <?php echo $list->title; ?>
        				                    </a>"
											<?php 
											}
											?>
										</p>
										
										<?php
										if ($inoti["notification_type"] == "friends" && $inoti["status"] == "waiting")
										{
										?>
										
										<div class="btn-change">
											<span class="accept" onclick="accept(<?php echo $inoti["id"];?>, <?php echo $sender["id"];?>);">Accept</span>
										</div>
										<div class="btn-change">
											<span class="reject" onclick="reject(<?php echo $inoti["id"];?>, <?php echo $sender["id"];?>);">Reject </span>
										</div>
										
										<div class="accepted" style="display: none;">
											<div class="yes">Accepted</div>
											<div class="friend-message"><?php echo $sender->getFullName(); ?> is your friend now</div>
											<div class="btn-undo">
												<span class="undo" onclick="undoAccept(<?php echo $inoti["id"];?>);">Undo </span>
											</div>
										</div>
										
										<div class="rejected" style="display: none;">
											<div class="no">Rejected</div>
											<!--<div class="friend-message">No further requests will be
												allowed</div>-->
											<div class="btn-undo">
												<span class="undo" onclick="undoReject(<?php echo $inoti["id"];?>);">Undo </span>
											</div>
										</div>
										
										<?php 
										}
										?>
										
									</div>

								</td>
								<td class="trd-col">
									<div>
										<i class="icon-shedule"></i><span class="date">
										    <?php echo date('d F Y', DateHelper::getClientTime($inoti["created"])); ?>
										</span>
									</div>
									<div>
										<i class="icon-time"></i><span class="time">
										    <?php echo date('g:i A', DateHelper::getClientTime($inoti["created"])); ?>
										</span>
									</div>
								</td>
							</tr>
							
							<?php
							    $inoti->read_notification = 1;
							    $inoti->save(false);
							}
							?>
							
							<?php 
							if (count($invitations) == 0)
							{
							?>
							
							<tr class="greyborder">							    
							    <td class="frs-col">
							    </td>
							    <td class="sec-col">
							        <div class="message-info">
							            <p>No invitations</p>
							        </div>
							    </td>
							    <td class="trd-col">
							    </td>
							</tr>    
							
							<?php 
							}
							?>
							
						</table>
						<div class="clear"></div>
					</div>
					<div id="tabs-2">
						<table class="user-lists">
							<colgroup>
								<col width="2%">
								<col width="76%">
								<col width="22%">
							</colgroup>
							
							<?php 
							foreach ($lists as $lnoti)
							{
							    $sender = $lnoti->userSender;
							    
							    $image = "";
							    if ($sender['image'] != '')
							        $image = $baseurl . "/images/profile/user_thumbs/listcreation/" . $sender['image'] . "?w=70&h=70";
							    else $image = $baseurl . "/images/default.jpg";
							    
							    $message = $lnoti["message"];
							    
							    $list = $opinion = null;
							    if ($lnoti["notification_type"] == 'opinion')
							    {
							        $opinion = Opinion::model()->findByPk($lnoti->list_id);
							        if (!$opinion)
                                    { 
                                        // TODO: delete notifications with opinion!
                                        //$lnoti->delete();
                                        continue;
                                    }
							        $list = ListForm::model()->findByPk($opinion->list_id);
							    }
							    else
							   {
							    $list = ListForm::model()->findByPk($lnoti->list_id);
							    }
							    
							?>
							
							<tr class="greyborder">
								<td class="frs-col">
								    <img src="<?php echo $image;?>" alt="<?php echo $sender->getFullName(); ?>" />
								</td>
								<td class="sec-col">
									<div class="message-info">
										<p>
										    <a href="<?php echo $baseurl; ?>/index.php/user/myDashboard?userid=<?php echo $sender['id']; ?>">
										        <span><?php echo $sender->getFullName(); ?></span>
										    </a>
											<?php echo $message; ?>
											
											<?php 
											if ($lnoti["notification_type"] == "opinion") {
											    echo '"<span class="capitalize">' . $opinion->value . '</span>" list ';
											} ?>
											
											"<a class="capitalize" href="<?php echo $baseurl; ?>/index.php/preferences/listdetail?listid=<?php echo $list->id; ?>">
        				                        <?php echo $list->title; ?>
        				                    </a>"
										</p>
									</div>
								</td>
								<td class="trd-col">
									<div>
										<i class="icon-shedule"></i><span class="date">
										    <?php echo date('d F Y', DateHelper::getClientTime($lnoti["created"])); ?>
										</span>
									</div>
									<div>
										<i class="icon-time"></i><span class="time">
										 <?php echo date('g:i A', DateHelper::getClientTime($lnoti["created"])); ?>
										</span>
									</div>
								</td>
							
                            <?php
							}
                            ?>

							<?php 
							if (count($lists) == 0)
							{							
							?>
							
							<tr class="greyborder">
							    <td class="frs-col">
							    </td>
								<td class="sec-col">
									<div class="system-info">
										<p>No Lists Notifications</p>
									</div>
								</td>
								<td class="trd-col">									
								</td>
							</tr>
							
							<?php 
							}
							?>
						</table>
						<div class="clear"></div>
					</div>
					<div id="tabs-3">
						<table class="user-system">
							<colgroup>
								<col width="78%">
								<col width="22%">
							</colgroup>
							
						    <?php 
							foreach ($system as $snoti)
							{
							    $message = $snoti["message"];    							    
							?>							
							
							<tr class="greyborder">
								<td class="sec-col">
									<div class="system-info">
										<div><?php echo $message; ?></div>
									</div>
								</td>
								<td class="trd-col" style="padding-left: 10px;">
									<div>
										<i class="icon-shedule"></i><span class="date">
										    <?php echo date('d F Y', DateHelper::getClientTime($snoti["created"])); ?>
										</span>
									</div>
									<div>
										<i class="icon-time"></i><span class="time">
										    <?php echo date('g:i A', DateHelper::getClientTime($snoti["created"])); ?>
										</span>
									</div>
								</td>
							</tr>
							
							<?php
							}
							
							if (count($system) == 0)
							{							
							?>
							
							<tr class="greyborder">
								<td class="sec-col">
									<div class="system-info">
										<p>No System Notifications</p>
									</div>
								</td>
								<td class="trd-col">									
								</td>
							</tr>
							
							<?php 
							}
							?>
						</table>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>
