


<div class="editnotification_notifi">
 <?php
    $baseurl = Yii::app()->request->baseUrl;
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'editprofile-form',
        'action'=>$baseurl.'/index.php/user/editnotification?userid='.$_GET['userid'],
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'class'=>'aboutprofileform', 
        'name'=>'editnotificationform',  
        'enctype'=> 'multipart/form-data',
        //'validateOnSubmit'=>true,
     ),
      )); 
   
?>      
        <h2 class="edituserprofile_h2">User Settings</h2>
          
        <div class="editnotification_emaildiv">
            <div class="editnotification_checkbox">
                <input type="checkbox" name="emailnotification" id="send_email" <?php if($user['send_email']=='on'){ ?>  checked="checked"<?php } ?> />
            </div>
            <div class="editnotification_emailspan">
                <span> Email me when I get Wayo notifications.</span>
            </div>
        </div>
        
         
        <div class="editnotification_customize">
            <div class="editnotification_emailspan">
                <span>- Customize</span>
            </div>
        </div>
        <div class="editnotification_notidiv1">
            <div class="editnotification_checkbox">
                <input type="checkbox" name="takealist" id="take_list" <?php if($user['take_list']=='on'){ ?>checked="checked"<?php } ?> />
            </div>
            <div class="editnotification_takelist">
                <span>Notifi me when users invite me to take a list.</span>
            </div>
        </div>
        <div class="editnotification_notidiv1">
            <div class="editnotification_checkbox">
                <input type="checkbox" name="youcreated" id="you_createdlist" <?php if($user['you_createdlist']=='on'){ ?>  checked="checked"<?php } ?> />
            </div>
            <div class="editnotification_takelist">
                <span>Notifi me when users take lists I've created.</span>
            </div>
        </div>
        <div class="editnotification_notidiv3">
            <div class="editnotification_checkbox">
                <input type="checkbox" name="otherstotake" id="you_invitedlist" <?php if($user['you_invitedlist']=='on'){ ?> checked="checked"<?php } ?>  />
            </div>
            <div class="editnotification_takelist">
                <span>Notifi me when users take lists I invited them to take.</span>
            </div>
        </div><div class="editnotification_notidiv1">
            <div class="editnotification_checkbox">
                <input type="checkbox" name="listentry" id="list_entry" <?php if($user['list_entry']=='on'){ ?> checked="checked"<?php } ?> />
            </div>
            <div class="editnotification_takelist">
                <span>Notifi me if I've been picked as a list entry.</span>
            </div>
        </div>
        
        
       <input type="hidden" name="submit"/>
        <div class="edituserprofile_savebutton">
            <a href="javascript:void(0);">
                <input class="image_buttons" type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/buttons/savechanges.png"/>
            </a>
        </div>
        
 <?php $this->endWidget(); ?>      
</div>