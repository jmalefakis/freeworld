<?php 

//if (isset($_GET['userid'])) return;
if (!isset($_ajax)) $_ajax = false;

if ($_ajax)
{
    FriendHelper::getMyFriendsIds(true);
}

$userid = Yii::app()->user->getID();
$user = User::model()->find("id = '$userid'");

$baseurl = Yii::app()->request->baseUrl;

$tab = null;
if ($_ajax && isset($_POST["tab"]))
{
    $tab = $_POST["tab"];
}
else
{
    $path = Yii::app()->getRequest()->pathInfo;
    $urlParts = explode("/", $path);
    $tab = $urlParts[1];
}

$friends_search = $user->isMember() 
    ? FriendHelper::getMyFriendsIds() : FriendHelper::getMyFollowersIds();

// created lists data
$listsCreated = Yii::app()->user->getModel()->listSorted();
$countCreated = count($listsCreated);

$createdPeopleIn = 0;
$createdPeopleInFriends = 0;

foreach ($listsCreated as $list)
{
    $createdPeopleIn += $list->countTaken();
    
    if ($friends_search) 
    {
    	$c = new CDbCriteria();
    	$c->addInCondition('user_id', $friends_search, 'AND');
    	$createdPeopleInFriends += $list->countTaken($c);
    }
}

$createdDailyCount = HistoryHelper::getMyCreatedListsDailyContributionCount();
if ($createdDailyCount == 0) $createdDailyCount = "=";
else $createdDailyCount = "+" . $createdDailyCount;

$createdDailyCountFriends = HistoryHelper::getMyCreatedListsDailyContributionCount($friends_search);
if ($createdDailyCountFriends == 0) $createdDailyCountFriends = "=";
else $createdDailyCountFriends = "+" . $createdDailyCountFriends;

// taken lists data
$listsTaken = Yii::app()->user->getModel()->takenListSorted();
$countTaken = count($listsTaken);

$takenPeopleIn = 0;
$takenPeopleInFriends = 0;

foreach ($listsTaken as $list)
{
    $takenPeopleIn += $list->countTaken();
    
    if ($friends_search) 
    {
    	$c = new CDbCriteria();
    	$c->addInCondition('user_id', $friends_search, 'AND');
    	$takenPeopleInFriends += $list->countTaken($c);
    }
}

$takenDailyCount = HistoryHelper::getMyTakenListsDailyContributionCount();
if ($takenDailyCount == 0) $takenDailyCount = "=";
else $takenDailyCount = "+" . $takenDailyCount;

$takenDailyCountFriends = HistoryHelper::getMyTakenListsDailyContributionCount($friends_search);
if ($takenDailyCountFriends == 0) $takenDailyCountFriends = "=";
else $takenDailyCountFriends = "+" . $takenDailyCountFriends;

// friends data
$friends = FriendHelper::findMyFriends();
// company followers
if ($user->isCompany())
{
    $friends = FriendHelper::findMyFollowers();
}
$friendsCount = count($friends);

$statusUpdates = HistoryHelper::getUsersWithDailyUpdatedStatus($friends_search);
$statusUpdatesCount = count($statusUpdates);

// notification data
$notifications = $user->unreadNotifications();
$notificationsCount = count($notifications);

// user settings
$showSummary = !isset($_SESSION["dashboardSSPanel"]);
if (!$showSummary)
{
    $showSummary = $_SESSION["dashboardSSPanel"] == "true";
}


// bug #3736;
$user["status_text"] = str_replace("'", '"', $user["status_text"]);
?>

<?php 
if(!$_ajax)
{
?>

<script>

var currStatus = '<?php echo $user["status_text"]; ?>';

function changeStatus() {
    if ($('#btnStatusRun').hasClass('disabled')) return false;
    
	$.ajax({
		type: 'POST',
		url: '<?php echo $baseurl; ?>/index.php/user/changeStatus',
		dataType: 'text',
		data: {
			mystatus: $('#mystatus').val()
		}, 
		success: function (resp) {
		    $('#btnStatusRun').addClass('disabled');
		    currStatus = $('#mystatus').val();
		}
	});
}

function updateSummaryPanel() {
    $.ajax({
		type: 'POST',
		url: '<?php echo $baseurl; ?>/index.php/user/updateSummary',
		dataType: 'text',
		data: {
		    tab: $('#summaryTab').val()
		}, 
		success: function (resp) {
		    $('#summaryPanel').html(resp);
		}
	});
}

$(document).ready(function() {
    $("#mystatus").keyup(function() {
        var newStatus = $(this).val();
        if (newStatus == currStatus) {
            $('#btnStatusRun').addClass('disabled');
        } else {
            $('#btnStatusRun').removeClass('disabled');
        }
    });

    $('.user-block-bottom').click(function() {
        $("#summaryPanel").slideToggle("slow");
    	$(this).find('.arrow-click-up').toggleClass("active");

    	$.ajax({
    		type: 'POST',
    		url: '<?php echo $baseurl; ?>/index.php/user/toogleSummary',
    		dataType: 'text',
    		data: { show: $(this).find('.arrow-click-up').hasClass("active") }, 
    		success: function (resp) {
    		}
    	});
    	
    	return false;
    });

    $('#btnStatusRun').tooltip();
});

</script>


<input type="hidden" id="summaryTab" value="<?php echo $tab; ?>" />

<div style="position:relative; margin:0px 0px 20px 0px;">

<div id="summaryPanel"  class="user-content up" 
    <?php if (!$showSummary) { ?>style="display: none;"<?php } ?>>

<?php 
} //_ajax
?>

    <div class="box-shadow-top">
    <div class="box-shadow-bottom">
        <div class="user-block">
        <?php if ($user['badge']) {?>    
            <span class="list-box-badge-eagle" title="Wayo Official"></span>
        <?php }?>
		<div class="position">
            <div class="user-img<?php if ($tab == "myDashboard") echo " active"; ?>">
            <?php if ($user['image'] != '') 
            { 
            ?>
                <img alt="User Photo" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/listcreation/<?php echo $user['image'];?>?w=132&h=132" />
            <?php 
            } 
            else if (strpos($user['gender'],'female') !== false) {
    
            ?>
                <img alt="User Photo" src="<?php echo $baseurl;?>/images/defaultgirl.jpg?w=132&h=132"/>
            <?php 
            }  else {
            ?>
                <img alt="User Photo" src="<?php echo $baseurl;?>/images/defaultboy.jpg?w=132&h=132"/>
            <?php    
            }
            ?>
            </div>
        
            <div class="user-module<?php if ($tab == "myCreated") echo " active"; ?>">
                <div class="title">
                    <?php echo $countCreated;?>&nbsp;list<?php if ($countCreated != 1) echo "s"; ?>
                </div>
                <ul class="list-ul">
                    <li>People In: <span><?php echo $createdPeopleIn; ?> (<?php echo $createdDailyCount; ?>)</span></li>
                    <li><?php if ($user->isMember()) { ?>Friends: <?php } else { ?>Followers: <?php } ?><span>
                        <?php echo $createdPeopleInFriends; ?> (<?php echo $createdDailyCountFriends; ?>)</span></li>
                </ul>
                <div class="title">
                    Top 3
                </div>
                <ul class="list-ol">               
                    <?php 
                    if ($countCreated > 3) $countCreated = 3; 
                    for ($i = 0; $i < $countCreated; $i++)
                    {
                    ?>
                    <li>
                        <a href="<?php echo $baseurl; ?>/index.php/preferences/viewResult?listid=<?php echo $listsCreated[$i]['id']; ?>">
                            <?php echo $listsCreated[$i]['title']; ?>
                        </a>
                    </li>
                    <?php 
                    }
                    ?>               
                </ul>
            </div>
        
            <?php if ($user->isMember()) { ?>        
            <div class="user-module<?php if ($tab == "myTaken") echo " active"; ?> takenCol">
                <div class="title">
                    <?php echo $countTaken;?>&nbsp;list<?php if ($countTaken > 1) echo "s"; ?>
                </div>
                <ul class="list-ul">
                    <li>People In: <span><?php echo $takenPeopleIn; ?> (<?php echo $takenDailyCount; ?>)</span></li>
                    <li>Friends: <span><?php echo $takenPeopleInFriends; ?> (<?php echo $takenDailyCountFriends; ?>)</span></li>
                </ul>
                <div class="title">
                    Top 3
                </div>
                <ul class="list-ol">
                    <?php 
                    if ($countTaken > 3) $countTaken = 3; 
                    for ($i = 0; $i < $countTaken; $i++)
                    {
                    ?>
                    <li>                        
                        <a href="<?php echo $baseurl; ?>/index.php/preferences/viewResult?listid=<?php echo $listsTaken[$i]['id']; ?>">
                            <?php echo $listsTaken[$i]['title']; ?>
                        </a>
                    </li>
                    <?php 
                    }
                    ?>
                </ul>
            </div>
            <?php } ?>
        
            <div class="user-module button-update<?php if ($tab == "myFriends") echo " active"; ?> friendsCol">
                <div class="title">
                    <?php echo $friendsCount; ?>
                    <?php if ($user->isMember()) { ?>
                     Friends
                    <?php } else { ?>
                     Followers
                    <?php } ?> 
                </div>
                <ul class="friends-list">              
                    <?php
                    $moreUpdatesCount = 0;
                    if ($statusUpdatesCount > 3)
                    {
                        $moreUpdatesCount = $statusUpdatesCount - 3;
                        $statusUpdatesCount = 3;
                    }
                    
                    for ($i = 0; $i < $statusUpdatesCount; $i++)
                    {
                        $friendUser = $statusUpdates[$i];
                        
                        $friendPhoto = $baseurl . "/images/default.jpg";
                        if ($friendUser["image"] != "")
                        {
                        	$friendPhoto = $baseurl . "/images/profile/user_thumbs/dashboard/" . $friendUser["image"] . "?w=40&h=40";
                        }
                    ?>
                        <li><a href="<?php echo $baseurl; ?>/index.php/user/myDashboard?userid=<?php echo $friendUser["id"]; ?>">
                            <img src="<?php echo $friendPhoto;?>" />
                            <?php 
                            if ($friendUser["status_text"] != "")
                            {
                                echo $friendUser["status_text"];
                            }
                            else
                        {
                                echo "View Profile";
                            }
                            ?>
                        </a></li>
                    <?php 
                    }
                    
                    if ($statusUpdatesCount == 0)
                    {
                    ?>
                              
                        <li>
                            No daily status updates
                        </li>
                    
                    <?php 
                    }
                    ?>
                    </ul>
                    
                    <?php if ($moreUpdatesCount)
                    {
                    ?>
                    
                    <div class="btn-silver">
                        <a class="btn-update"><span>+<?php echo $moreUpdatesCount;?></span> more updates</a>
                    </div>
                
                    <?php 
                    }
                    ?>          
            </div>
        
            <div class="user-module<?php if ($tab == "myNotifications") echo " active"; ?> button-update">
                    <div class="title">
                        <?php echo count($user->pendingNotifications()); ?> Pending
                    </div>
        			<ul class="friends-list friends-event">
        			
        			<?php
                    $moreUpdatesCount = 0;
                    if ($notificationsCount > 3)
                    {
                        $moreUpdatesCount = $notificationsCount - 3;
                        $notificationsCount = 3;
                    }
                    
                    for ($i = 0; $i < $notificationsCount; $i++)
                    {
                        $notification = $notifications[$i];
                        $sender = $notification->userSender;
                        
                        $opinion = null;
                        if ($notification->notification_type == "opinion")
                        {
                            $opinion = Opinion::model()->findByPk($notification->list_id);
                            if (!$opinion) continue;
                        }
                    ?>        			
        				<li>
        				    <?php 
        				        if ($notification->notification_type != "system")
        				        {
        				    ?>
        				
        				    <span><?php echo $sender->getFullName(); ?></span>
        				    
        				    <?php
        				        }
        				    
        				        if ($notification["message"] != "")
        				        {
        				            $message = $notification["message"];
        				            if ($message == "You are friends with ")
        				            {
        				            	$message = " is your friend";
        				            }
        				            
        				            echo $message;
        				        }
        				        else if ($notification["notification_type"] == "friends") 
        				            echo " wants to become a friend";
        				                				        
        				        if ($opinion || strpos($notification->notification_type, "lists") >= 0)
        				        {
        				            if ($opinion)
        				            {
        				                $list = ListForm::model()->findByPk($opinion->list_id);
        				                echo '"' . $opinion->value . '" list ';
        				            }
        				            else
        				              $list = ListForm::model()->findByPk($notification->list_id);

        				            if ($list)
        				            {
        				    ?>
        				    
            				    "<a class="capitalize" href="<?php echo $baseurl; ?>/index.php/preferences/listdetail?listid=<?php echo $list->id; ?>">
            				        <?php echo $list->title; ?>
            				    </a>"
        				    
        				    <?php 
        				            }
        				        }
        				    ?>
        				</li>
        				
    				<?php
                    } 
    				
    				if ($notificationsCount == 0)
                    {
                    ?>
                              
                        <li>
                            No new notifications
                        </li>
                    
                    <?php 
                    }
                    ?>
        			
        			</ul>
        			
        			<?php if ($moreUpdatesCount)
                    {
                    ?>
    			    
    			    <div class="btn-silver">
    			        <a class="btn-update" href="<?php echo $baseurl; ?>/index.php/user/myNotifications">
    			            <span>+<?php echo $moreUpdatesCount; ?></span> more updates</a>
    			    </div>
    			    
    			    <?php 
                    }
    			    ?>
    	    </div>
        </div>
		
        <div class="clear"></div>
        
            <?php
            if ($tab == "myDashboard" || ($tab == 'myCreated' && $user->isCompany()))
            {
            ?>
            
            <div class="my-status">
			<div class="my-status-separator"></div>
            <div class="my-status-text">Status</div>
            <div class="my-status-value">
                <input id="mystatus" class="input-css" style="width: 740px;"
                    placeholder="Let'em know your status here"
                    type="text"  value='<?php echo $user["status_text"]; ?>'/>
            </div>
            <div class="my-status-btn">
                <div id="btnStatusRun" class="blue-btn btn-run disabled" title="Change status and run!"
                    onclick="changeStatus();">RUN ON!</div>
            </div>
            </div>
        
            <div class="clear"></div>
            
            <?php 
            } 
			else
			{
            ?>
			
			<div class="my-status">
				<div class="my-status-separator2"></div>
				<br/>
			</div>
			
			<?php 
			}
			?>
            
            
		</div>
		
        
		
    </div>
    </div>

<?php 
if (!$_ajax)
{
?>
</div>

<div class="user-block-bottom"> 
    <div class="arrow-click-up <?php if ($showSummary) { ?>active<?php } ?>">
</div>
</div>   

</div>

<div class="clear"></div>
<div class="empty-bl"></div>

<?php 
}
?>
