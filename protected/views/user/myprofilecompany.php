<?php $this->renderPartial("/user/summary", array()); ?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/upload.js"></script>
<script>
jQuery(document).ready(function(){
         var n = 0;
        $(".hiddendiv_viewcoments").mouseenter(function() {
        n = 0;

        }).mouseleave(function() {
        n = 1;
        });

        $("html").click(function(){ 
        if (n == 1) {
        jQuery(".hiddendiv_viewcoments").hide(); 
        n=0;
        }
        });
 var nameofuser = $("#nameofuser").val();
 if(nameofuser!='')
 $('title').html(nameofuser+"'s profile");
 
 
    $('#deactivate').click(function(){
		$.confirm({
			'title'		: 'Deactivate Confirmation',
			'message'	: 'You are about to deactivate this profile. <br />Reactivation may not be possible! Continue?',
			'buttons'	: {
				'Yes'	: {
					'class'	: 'blue',
					'action': function(){
						$.ajax({
                            url: '<?php echo $baseurl; ?>/index.php/user/deactivate',
                            type: 'POST',
                            success: function(result) {
                                if (result == 1) {
                                    document.location.href = document.location.href;
                                }
                            }
                        });
					}
				},
				'No'	: {
					'class'	: 'gray',
					'action': function(){}	
				}
			}
		});
	});
 }); 
</script>  

<?php 
if(isset ($_GET['userid']))
$id      = $_GET['userid'];
else {
    $id=Yii::app()->user->getID();
}
$userid  = Yii::app()->user->getID();
$baseurl = Yii::app()->request->baseUrl;

$cs=Yii::app()->clientScript;

      // Add CSS
$cs->registerCSSFile($baseurl.'/css/datepicker/colorbox.css');
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
      
$path =  Yii::app()->getRequest()->pathInfo; $url1 = explode("/", $path);$url  = $url1[0];


$city_state = $city_state3= $city_state2 = $city_state1 = '';
if(isset($user['country']) && $user['country']!='')
  $city_state1  = $user['country'];
if(isset($user['state']) && $user['state']!='')
  $city_state2  = $user['state'].",";
if(isset($user['city']) && $user['city']!='')
  $city_state3  = $user['city'].",";

$city_state = rtrim($city_state3.$city_state2.$city_state1,",");


  $occupation = $user['industry'];
  
?>
 <input type="hidden" name="onclick" value="personalclick" id="onclick"/>
 
  <input type="hidden" name="onclick" value="<?php echo $baseurl;?>" id="baseurl"/>
  
  <input type="hidden" name="nameofuser" value="<?php echo StringsHelper::capitalize(trim($user['firstname']));?>" id="nameofuser"/>
 
 <input type="hidden" name="baseurl" id="currenturl" value="<?php echo $url1[1]?>"/>
<div class="featured_list_section"> 
    <div id="wrapper">
   
<?php  $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/user/myProfileCompany',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',
        'validateOnSubmit'=>true,            
        'onsubmit'=>'return  myprofilecompany_vaidate();',    
     ),
      )); 
?>
        
        <div class="profile_outer">
            <div class="leftprofile_part">
                <div class="username_myprofile">
                    <span>My Company is: </span>
                </div>
                 <?php if($id==$userid){?>
                <div class="usernametext_myprofile">
                    <input type="text" name="User[firstname]" value="<?php echo $user['firstname'];?>"  class="country2" id="firstname" style="color: #555555"/>
                    
                    <span id="spanfirst" class="frstnamespan" style="display: none;">
                        Company name field is missing
                    </span> 
                    
                </div>               
                <?php } else { ?>
                <div class="height30px">
                    <span><?php echo ($user['firstname']);?></span>
                </div>
                <?php } ?>
                <div class="username_myprofile" style="color: #555555;">
                    <span>My email: </span>
                </div>
                 <?php if($id==$userid){?>
                <div class="usernametext_myprofile">
                    <input type="text" name="User[email]" value="<?php echo $user['email'];?>"  class="country2" id="User_email" style="color: #555555"/> 
                    <span class="numaicclass" id="spanemail" style="width: 140px;margin-left: 0px;"></span>
                </div>                
                <?php } else { ?>
                    <div class="height30px">
                          <span><?php if(isset($user)) { echo $user['email'];}?></span>
                   </div>
                 <?php } ?>
                <?php if($id==$userid){?>
                <div class="username_myprofile">
                    <a onclick="forgotpassword_popup('<?php echo $baseurl;?>/index.php/user/changePassword');" href="#" class="changepassword_link"> Change Password...</a>
                </div>
                <?php } ?>
                <div class="name">
                    <div class="born_interest" style="color: #555555;">
                          My Interests are:
                      </div>
                      
                </div>
                
                <?php if($id==$userid){?>
                <div class="first_name2_interst" id="div_typing">
                          <ul>
                              <span id="mainspan">
                                  <?php if(isset($user)){$countint=explode(',',$user['interest']);}?>
                                  <?php if(isset($user) && $user['interest']!=''){ for($int = 0;$int<count($countint);$int++) {?>
                                      <li><span><?php echo $countint[$int]; ?></span><a onclick="removeli(this)" href="javascript:void(0);"><img class='imagespan_profile' src="<?php echo $baseurl;?>/images/cross.png" /></a></li>
                                  <?php }}?>
                              </span>
                              <li>
                                  <img src="<?php echo $baseurl?>/images/ajax-loader.gif" class="loding_image_li" id="loding_li_img"/>
                                  <input value="<?php if(isset($user)){ echo '';} else { echo 'Click the arrow'; }?>" size="29" name="User[interest]" readonly="readonly" id="typings" onclick="start_typing3_profile()" onblur="typing4_profile(this.value)" autocomplete="off" maxlength="120" />
                                  
                              </li>
                              <div class="home_ulli_interest" id="editdiv">
                                     
                              </div>
                          </ul>
                          
                          <input type="hidden" name="interest_list" <?php if(isset($user)){ if(preg_match('/^[,]+/', $user['interest'])) { ?>value="<?php echo $user['interest'];?>"<?php }else { ?> value=",<?php echo $user['interest'];?>" <?php } }?> id="hiddeninterest" class="resettext"/>                        
               </div>
              <div class="floatleft cursorpointer born_s_int" >
                      <img src="<?php echo $baseurl?>/images/interestdrpdwn.png" onclick="showavailable()"/>
                      
                      <div class="show_avl_div show_avl_div_prof" id="showdiv_interest" style="display: none;">
                              <div class="interestapply">
                                  <input type="text" name="searchinterst" style="width: 93px;" id="serchint" class="resettext"/>
                                  <span class="searchspan" onclick="searchinterest('<?php echo $baseurl?>')">
                                      search
                                  </span>    
                              </div>
                          <div class="selectallcheckbox">
                              <input type="checkbox" name="selectall" class="checkbox_show_select" value="" onclick="selectAllCheckBox(this)" id="selectall"/>
                              <span id="selectallspan" style="border:1px red solid; margin-top:5px;"> Select all</span>
                          </div>
                               
                              <div class="div_scroll">
                                  <?php foreach($interest  as $value_show ) { ?>

                                      <div class="interest_div_CHECK">
                                      <input type="checkbox" name="availablecheckbox" value="<?php echo $value_show->interest;?>" <?php if(isset($user) && in_array($value_show->interest,$countint)){?>checked='checked' <?php } ?> id="checkbox<?php echo $value_show->interest;?>" class="checkbox_show"/>
                                      <span class="interest_span_div"><?php echo $value_show->interest;?></span>
                                      </div> 

                                  <?php } ?>
                              </div>
                              <div class="interestapply">
                                  <span class="cursorpointer applyinterest" onclick="applyinterest_myprofile('<?php echo $baseurl;?>')">Apply</span>
                              </div>
                      </div>
                  </div>
                
                <span class="numaicclass" id="spaninterest" style="width: 200px;">Interests field is missing</span>
                <?php } else { ?>
                
                    <div class="first_name2_interst" style="background: none;width: 400px;">
                          <table style="width: 425px;">
                                  <?php if(isset($user)){$countint=explode(',',$user['interest']);}?>
                        
                                  <?php if(isset($user) && $user['interest']!=''){ $var = 0; for($int = 0;$int<count($countint);$int++) {?>
                                    
                                     <?php 
                                           if($var==0) {
                                               echo "<tr style='height: 25px;color: #555555;font-family:ArchivoNarrow-Regular; font-size: 15px;'>";
                                           }
                                             if($var<4){
                                                echo "<td>";
                                                  echo $countint[$int];
                                                echo "</td>";
                                            }
                                            if($var==2) {
                                               echo "</tr>";
                                               $var=-1;
                                           }
                                     ?>
                                      <!--<li style="margin-left: 2px; background: none;padding: 0px;"><span style="padding: 0px;color: #555555;font-family:ArchivoNarrow-Regular; font-size: 15px;"><?php //if($int==0){echo $countint[$int];} else { echo ','.$countint[$int];}  ?></span></li>-->
                                  
                                 <?php $var++; }   }?>
                              </table>                         
                                       
                  </div>
                
                <?php } ?>
            </div>
            <div class="rightprofile_part">
                          <div class="born" >
                                     <span style="color: #555555;">
                                         Company logo: &nbsp;
                                     </span>
                              </div>
                
                <?php if($id==$userid){?>
                <div class="myimage_profilecom" >
                              
                              <div class="myfileupload-buttonbar_profile">
                                  <label class="myui-button" style="width: 138px; padding-top: 13px; text-decoration: underline;">
                                      <span id="file_uplod" class="cursorpointer">Upload image</span>
                                      <input onclick="onclickimageper('<?php echo $baseurl?>')" id="images" type="file" name="User[profilepic]" value="" class="resettext"/> 
                                  </label>
    		                  <button type="submit" id="btn" style="display: none;">Upload Files!</button>
                              </div>
                    
                    <div id="image_upolad" class="city_state_error" style="display: none;">Please upload correct image </div>
               </div>
                <?php } ?>
                <div class="flaotleft" id="response" style="margin-top: 10px;">
                     <?php if($user['image']!='') { ?>
                                 <img src="<?php echo $baseurl;?>/images/profile/user_thumbs/listcreation/<?php echo $user['image']?>?w=180&h=180" style="width: 75px; height: 56px; margin-top: 7px;"/>
                     <?php } else if (strpos($user['gender'],'female') !== false) { ?>
                                 <img src="<?php echo $baseurl;?>/images/defaultgirl.jpg?w=180&h=180" style=" margin-top: 7px;"/>
                     <?php } else { ?>
                                 <img src="<?php echo $baseurl;?>/images/defaultboy.jpg?w=180&h=180" style=" margin-top: 7px;"/>
                     <?php } ?>
                                 <ul id="image-list">

                                </ul>
                </div>
                
                <div class="name" style="margin-top: -6px;">
                                  <div class="born" style="color: #555555;">
                                      Street address:
                                  </div>
                                  </div>
                <?php if($id==$userid){?>
                                 <div class="fields">
                                    <input value="<?php if(isset($user)){ echo $user['street'];} else { echo "Street address" ; } ?>" size="29" class="country2" id="companyStreet" maxlength="120" name="street" style="color: #555555"/>
                                    <div class="city_state_error" id="streeterror">
                                        
                                    </div>
                                 </div>
                <?php } else{?>
                                  <div class="height30px" style="margin-bottom: 20px;">
                                       <span><?php if(isset($user) && $user['street'] != ''){ echo $user['street'];} else { echo "Not provided";}?></span>
                                  </div>    
                <?php } ?>
                
                                 <div class="name" style="margin-top: -10px;">
                                  <div class="born" style="color: #555555;">
                                      Company address:
                                  </div>
                                  </div>
                <?php if($id==$userid){?>
                                 <div class="fields">
                                    <input value="<?php if(isset($user)){ echo $city_state;} else { echo "City, State/Province, Country" ; } ?>" size="29" class="country2" id="country2" maxlength="120" name="city_state" style="color: #555555"/>
                                    <div class="city_state_error" id="cityerror">
                                        
                                    </div>
                                 </div>
                <?php } else{?>
                                  <div class="height30px" style="margin-bottom: 20px;">
                                       <span><?php if(isset($city_state) && $city_state != ''){ echo $city_state;} else { echo "Not provided";}?></span>
                                  </div>    
                <?php } ?>
                                
                                     <div class="name">
                                        <div class="born" style="color: #555555;">
                                          We work in:
                                        </div>
                                    </div>
                <?php if($id==$userid){?>
                             <div class="inputs_outer">
                               <input value="<?php if(isset($user)){ echo $occupation;} else { echo "Industry" ; } ?>" size="29" class="industry5" id="industry5"  maxlength="120" name="User[occupation]" style="color: #555555"/>
                               <div class="city_state_error" id="industry_error" style="margin-top: 0px;">
                                        
                                </div>
                             </div>  
                              
                <?php } else {?>
                
                            <div class="height30px">
                                <span><?php if(isset($occupation) && $occupation != ''){ echo $occupation;} else { echo "Not provided";}?></span>
                            </div>
                
                <?php } ?>
                
                
  <?php if($id !=$userid){      

   if(empty($current_user)){
  
  ?>              
                <div class="addimageclass">
                       
					   <div class="floatleft cursorpointer btn-remove-friend-blue" id="addimage" onclick="addfriend('<?php echo $id;?>')" >Add friend</div>
                       <span id="msgaddfrnd" class="msgaddfrnd">
                           
                       </span>
                       <img class="lodingaddfrnd" src="<?php echo $baseurl?>/images/loading.gif" id="addfrndloding"/> 
                       
                 <div id="div_hide"  class="hiddendiv_viewcoments" style="display: none;"> 
                      <!--div class="arrowimageviewresult">
                           <img src="<?php echo $baseurl?>/images/latest_img/drop.png"/>
                      </div-->
                      <div class="fakeboxdiv1">
                              <span id="fakeboxspan">Cancel Request !</span>
                      </div>

                      <div class="floatright">
                              <img id="yescancel" class="myprofileaccept" src="<?php echo $baseurl;?>/images/latest_img/yes.png"   onclick="friendRequestSentYes()"/>
                              <img  id="nocancel"  class="myprofiledecline" src="<?php echo $baseurl;?>/images/latest_img/no.png" onclick="friendRequestSentNo()"/>
                      </div>                      
                  </div>
                </div>
                
  <?php } elseif($friendrequestaccept) { ?>
                
                <div class="addimageclass">
                       
                       <div class="cursorpointer btn-remove-friend-blue" id="addimage" onclick="removefriend('<?php echo $id;?>')">Remove Friend</div>
					   <span id="msgaddfrnd" class="msgaddfrnd">
                           
                       </span>
                       <img class="lodingaddfrnd" src="<?php echo $baseurl?>/images/loading.gif" id="addfrndloding"/> 
                       
                   <div id="div_hide"  class="hiddendiv_viewcoments" style="display: none;"> 
                      <!--div class="arrowimageviewresult">
                           <img src="<?php echo $baseurl?>/images/latest_img/drop.png"/>
                      </div-->
                      <div class="fakeboxdiv1">
                              <span id="fakeboxspan">Do you want to remove '<?php echo ($user['firstname']." ".$user['lastname']);?>' from your friends list !</span>
                      </div>

                      <div class="floatright">
                              <img  id="yescancel" class="myprofileaccept" src="<?php echo $baseurl;?>/images/latest_img/yes.png"   onclick="removefriendYes('<?php echo $id;?>')"/>
                              <img  id="nocancel" class="myprofiledecline" src="<?php echo $baseurl;?>/images/latest_img/no.png" onclick="removefriendNo()"/>
                      </div>                      
                  </div>
                </div>                
                
  <?php } } ?>
                         
              </div>
            
            <div style="clear:both;"></div>
            <?php if($id ==$userid){  ?>
                   <div class="submitprofile">
                       <input type="submit" name="submit_profile" value=""  class="submit_profile_input"/>
                       
                           <?php if(isset ($flashmsg) && $flashmsg !='') { ?>
                           <span class="aftersubmitdata">
                               <?php echo $flashmsg;?>
                           </span>    
                          <?php } ?>
                       
                   </div>
                <?php } ?>  
            <?php $this->endWidget(); ?> 
            </div>
            <?php if (!Yii::app()->user->getModel()->isAdmin()) {?>
                <div class="link deactivate" id="deactivate">Deactivate</div>
            <?php } else {?>
                <div class="link deactivateAdmin">Deactivate</div>
            <?}?>
        </div>
    </div>
    
</div>