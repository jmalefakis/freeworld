<?php
if (!isset($_ajax)) { $_ajax = false; }
if (!isset($_popup)) { $_popup = false; }
if (!isset($container)) { $container = 'contentdiv'; }
if (!isset($current_user)) { $current_user = Yii::app()->user->getID(); }
if (!isset($current_page)) { $current_page = 'created'; }

$baseurl = Yii::app()->request->baseUrl;
$userid  = Yii::app()->user->getID();
$path    = Yii::app()->getRequest()->pathInfo;
$url1    = explode('/', $path);
$currenturl = $url1[1];

if (!$_ajax) {
?>

<script type="text/javascript">
$(function() {

	initGlobals(
		'<?php echo $current_user; ?>',
		{
			'<?php echo $current_page; ?>': {
				pageno: 1,
				maxpageid: <?php echo $pages; ?>,
				container: '#<?php echo $container; ?>',
				action: 'user/myCreatedPagination'
			}
		}
	);
	
	FW.baseurl = '<?php echo Yii::app()->request->baseUrl; ?>';
	FW.users[<?php echo $current_user; ?>].currentPage = '<?php echo $current_page; ?>';
	
	//$(window).scroll(function() {
	<?php if (!$_popup) { ?>
	$(window).scroll(function() {
		initPagination(FW.currentUser, FW.users[FW.currentUser].currentPage);
	});
	<?php } ?>
	
}); //ready

</script>

<?php if(!$_popup) { ?>
<input type="hidden" name="hiddenpage" id="pageno_off" value="1" />
<input type="hidden" name="maximaumpageno" id="maximaumpageno" value="<?php echo $pages; ?>" />
<input type="hidden" name="currentpage" id="currentpage" value="<?php echo $currenturl; ?>" />

<div class="featured_list_section">  
	<div id="wrapper">
	<div id="mainContent">
	
	<?php
	if ((!$_ajax) && (!$_popup)) {
		$this->renderPartial("/user/summary", array());
	}
	?>
<?php } //if(!$_popup) ?>
	<!--
		<div class="sort_section">
			<div class="foundlist_recom" <?php if($total==0){echo "style='display:none;'";}?>  style="float: right; width: 130px;" id="foundlistdiv">
				<span id="foundlistid">Found <?php echo $total;?> <?php if($total>1) { ?>lists <?php } else{ echo "list";};?></span>
			</div>
		</div>
		-->
		<ul class="content list-boxes" id="<?php echo $container; ?>">
		
		<?php
} //if (!$_ajax)

		if (count($data) > 0 ) {
			foreach($data as $list) {
				$rows_html = '';
				foreach($list['rows'] as $row_value) {
					$rows_html .= '<li>' . $row_value['value'] . '</li>';
				}
		?>
			<li class="list-box box-shadow-top" 
				data-subject-type="list"
				data-id="<?php echo $list['id']; ?>" 
				data-is-taken="<?php echo CJavaScript::encode($list['is_taken']); ?>" 
				data-is-favorite="<?php echo CJavaScript::encode($list['is_favorite']); ?>"
			><div class="box-shadow-bottom">
				<div class="list-box-alert-panel">Remove this list from your Dashboard?
					<div class="btn-close-red"  onclick="javascript:$(this).parent().fadeOut(500);"></div>
					<div class="btn-gray-small" onclick="javascript:$(this).parent().fadeOut(500);">NO</div>
					<div class="btn-gray-small" onclick="javascript:removeListFromFavorite(this);">YES</div>
				</div>
				<div class="list-box-container">
					<div class="list-box-main captionTop8">
						<a class="list-box-title" href="<?php echo $baseurl; ?>/index.php/preferences/listdetail?listid=<?php echo $list['id']; ?>">
                            <span class="user-title-eagle">
                                <span title="<?php echo StringsHelper::capitalize($list['title']); ?>" class="size titleWithBadge"><?php echo StringsHelper::capitalize($list['title']); ?></span>
                            <?php if (!empty($list['participation']) && $list['participation'] != 'Everybody') {?>
                                <div class="list-box-private-check" title="Private List"></div>
                            <?php } else if (!empty($list['badge']))
                            {   
                            ?>
                                <span class="list-box-badge-eagle" title="Wayo Official"></span>
                            <?php 
                            } 
                            ?>
                                <span class="clear"></span>
                            </span>
                            <img src="<?php echo $baseurl; ?>/images/<?php echo $list['image'] . '?w=223&h=188'; ?>" alt="<?php echo StringsHelper::capitalize($list['title']); ?>" title="<?php echo StringsHelper::capitalize($list['title']); ?>"/>
						</a>
                        <?php if (!empty($list['link_title'])) {?>
                        <div class="caption8">
                            <div class="captionInner8">
                                <a href="<?=!empty($list['link']) ? $list['link'] : 'javascript:;'?>" target="_blank"><?=StringsHelper::capitalize($list['link_title'])?></a>
                            </div>
                        </div>
                        <?php }?>
						<div class="list-box-details">
							<div class="list-box-author" onclick="listAuthorClick(this, <?php echo $list['user_id']; ?>, '#wrapper')">
								<?php echo StringsHelper::capitalize($list['author']); ?>
							</div>
							
							<div class="block-hidden dashboard"></div>
                            
							<div class="list-box-info">
								<div class="list-box-star tooltip-pointer-simple" onclick="javascript:toggleFavorite(this);"></div>
								<div class="icon-list"></div>
								
								<?php echo $list['count_c']; ?><span> in </span>
								
								<div class="list-box-friends"><?php echo count($list['friends']); ?>
									<span><?php echo $user->isMember() ? "Friend" : "Follower"; if (count($list['friends']) != 1) { ?>s<?php } ?></span>
								<?php if (count($list['friends'])) { ?>
									<div class="list-box-friends-drop-down">
										<ul>
											<?php foreach($list['friends'] as $friend) { ?>
											<li><a href="<?php echo $baseurl; ?>/index.php/user/dashboard?userid=<?php echo $friend['id']; ?>"><?php echo $friend['firstname'] . ' ' . $friend['lastname']; ?></a></li>
											<?php } ?>
										</ul>
									</div>
								<?php } ?>
								</div><!-- .list-box-friends -->
							</div>
						</div>
					</div>
		
					<ol class="list-box-rows"><?php echo $rows_html; ?></ol>
					
					<div class="list-box-grab-and-drag">Grab and Drag</div>

				</div>
			</div><!-- .box-shadow-bottom --></li><!-- .list-box.box-shadow-top -->
		<?php
			}
		} else if (!$_ajax) {
		?>
			<div class="nolistfound">No lists found</div>
		<?php
		}
if (!$_ajax) {
		?>
		</ul><!-- #contentdiv -->
	<?php 
	if (count($data) > 0) {
		if (!$_popup) {
	?>			  
			<div style="clear: both;">&nbsp;</div>
			<div class="paging_officialpage" id="pagingdiv" style="display: none;"> 
			<?php
			if ($pageno > 0) {
			?>
				<a class="textdecoration" href="<?php echo $baseurl; ?>/index.php/preferences/world?pageno=<?php echo $pageno - 1; ?>">
					<img src="<?php echo $baseurl; ?>/images/freeworld/previousimg.png"/>
				</a>
			<?php
			}
			if ($pages > 0 && $pages != $pageno) {
			?>
				<a class="textdecoration" href="<?php echo $baseurl; ?>/index.php/preferences/world?pageno=<?php echo $pageno + 1; ?>">
					<img src="<?php echo $baseurl; ?>/images/freeworld/nextimg.png"/>
				</a>
			<?php
			}
			?>
			</div><!-- #pagingdiv -->
	<?php
		} //if (!$_popup)
	}
	?>
		<div class="officialtodatloding" id="officialtodatlodingdiv">
			<img src="<?php echo $baseurl; ?>/images/loading.gif" />
		</div>
	
	<?php if (!$_popup) { ?>
	</div><!-- .mainContent -->
	</div><!-- #wrapper -->

	<div style="clear: both;">&nbsp;</div>
</div> <!-- .featured_list_section -->
	<?php } //if (!$_popup) ?>
<?php
} //if (!$_ajax)
?>
	
<script type="text/javascript">
setTimeout(function() {
	$('ol.list-box-rows li').each(function() {
		var h = $(this).height() + 1,
			$ch = $(this).children();
		while ($ch.outerHeight() > h) {
			$ch.text(function (index, text) {
				return text.replace(/\W*\s(\S)*$/, '...');
			});
		}
	});
});
</script>

<?php if (!empty($help)) {?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/gallery.css" />
    <link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
    <script>
        $(function(){
            $.fancybox({
               'href' :'#modalHelp',
               afterShow: function() {},
               beforeClose: function() {}
           });
     });   
    </script>
    <div class="customModal clear" id="modalHelp" style="display:none;">
        <div id="galleryContainer" class="galleryContainer">
            <h3>Welcome to Wayo</h3>
            <div id="gallery" class="content">
                <div class="slideshow-container help" style="height: 130px;">
                    <p>Please check out a brief overview of how to use Wayo:</p>
                    <p><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf">http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf</a></p>
                    <p>The overview is always available on the Help page.</p>
                    <p>Have fun!</p>
                </div>
            </div>
        </div>
    </div>
<?php }?>