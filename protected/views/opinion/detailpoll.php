<script type="text/javascript">
    /*jQuery(document).ready(function(){
    var width =  document.getElementById("mydislist").offsetWidth    
   $(".listdetail_titlete_textfild_first").width(width);      
   
 }); */
</script>

<?php 
$this->renderPartial('sidenavigation');
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid     = Yii::app()->user->getID();
// Get client script
$cs=Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/colorbox.css');

// Add JS
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>
<div class="list_detail_outerdiv" style="padding-top: 10px;">
<div class="popup_outerdiv1">
 <?php


if(isset($data))
{ 
    $pollid      = $data->id;
    $listname    = $data->question;
    $listimage   = $data->image;    
    $created_by  = $list_creator;
    $interest    = $list_interest;    

?>
        <input type="hidden" name="listid" value="<?php echo $pollid; ?>"/>
        <input type="hidden" id="hidden_baseurl" value="<?php echo $baseurl ?>"/>
            <div style="width: 100%;" class="popup_list_title_div1">               
                <div class="popup_row_namefild_listdetail">
                    <span style="color: #333232;"><?php echo $listname; ?></span>
                </div>
            </div> 
          <div class="listdetail_flashmsg">
            <?php foreach(Yii::app()->user->getFlashes() as $key => $message) {                                                        
                                echo '<div class="flash' . $key . '">' . $message . "</div>\n";                            
                        }
                    ?>
           </div>
            <div style="width: 590px;" class="popup_listimage_maindiv">
                <div class="list_detail_row_textfild" style="margin-left: 50px;" >
                    <?php if(strpos($listimage, 'profile/') === 0){ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $listimage ?>" />
                    <?php } else if(strpos($listimage, 'freeworld/') === 0){ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $listimage ?>" />
                    <?php } else{ ?>
                        <img class="list_detail_listimage" src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo 'pollimages/list_thumbs/listdetail/'.$listimage; ?>" />
                    <?php } ?>
                </div>
                
                <?php $countpop = $countpolans;
                $createdtime = $data->modified;
                $currenttime = time();
                $difference  = $currenttime - $createdtime; 
                $years       = abs(floor($difference / 31536000));
                $days        = intval($difference / 86400); 
                $difference  = $difference % 86400;
                $hours       = intval($difference / 3600); 
                $difference  = $difference % 3600;
                $minutes     = intval($difference / 60); 
                $difference  = $difference % 60;
                $seconds     = intval($difference);                 
                ?>
                
                <div class="list_detail_listimage_authorinfo"  style="width: 400px;">
                    <span class="list_detail_author_attr">Author:</span>&nbsp;
                    <a class="textdecorationblack" href="<?php echo $baseurl;?>/index.php/user/dashboard?userid=<?php echo $data->user_id;?>">
                    <span style="text-decoration: underline;width: auto;" class="list_detail_author_val"><?php echo $created_by; ?></span>
                    </a>
                    <br/>
                    <?php if($profilestatus!= null) {?>
                    <span class="list_detail_author_attr">Status:</span>&nbsp;<span><?php echo $profilestatus;?></span><br/>
                    <?php } ?>
                    <span class="list_detail_author_attr">Category:</span>&nbsp;<span  style="width: 300px;" class="list_detail_author_val"><?php echo $interest; ?></span>   <br/>                 
                    <span class="takelist_newdis_span1">Created:</span>&nbsp;
                          <?php if($years>1){?>
                            <span><?php echo $years;?> years ago</span> <br/>

                            <?php }elseif($days>1){?>
                            <span ><?php echo $days;?> days &nbsp;ago</span><br/>

                            <?php }elseif($hours>24){?>
                            <span ><?php echo $hours;?> hours &nbsp;ago</span>  <br/>

                            <?php } elseif($minutes>1) {?>
                             <span ><?php echo $minutes?> minutes &nbsp;ago</span><br/>    

                           <?php }elseif($seconds<60) {?>
                            <span ><?php echo $seconds?> sec &nbsp;ago</span><br/>

                            <?php } ?> 
                    <span class="list_detail_author_attr">Votes:</span>
                    <div style="font-size: 11px;" class="popup_votingspan_outer">
                        <div style="padding: 4px 0 0;" class="popup_votingspan_inner"><?php echo $countpolans;?></div>
                    </div>
                </div>
                
             <?php if($data['instruction']!='' && $data['instruction']!='Enter poll Instruction') { ?>
            <div class="instruction_listdetail">
                <span><?php echo $data['instruction'];?></span>
            </div>
            <?php }?>
                
        <?php if($data['user_id']==$userid || !empty($mytakenlist)) { ?>
             
          <div style="margin-top: 20px; width:100%;margin-left: 0px;" class="popup_submitdiv" id="editdiv">
              <?php if(empty($mytakenlist)) { ?>
              <a onclick="editlist_popup('<?php echo Yii::app()->request->baseUrl; ?>/index.php/opinion/seepoll?pollid=<?php echo $pollid;?>&user_id=<?php echo $userid;?>');" href="#">
                    <div class="detailpollmargine">
                        <span style=" font-size: 22px;">Vote</span>
                    </div>
                </a>
              <?php } else { ?>
                <a onclick="editlist_popup('<?php echo Yii::app()->request->baseUrl; ?>/index.php/opinion/changepollvote?pollid=<?php echo $pollid;?>&user_id=<?php echo $userid;?>');" href="#">
                    <div class="detailpollmargine">
                        <span style=" font-size: 22px;">Change Vote</span>
                    </div>
                </a>
              <?php } ?>
              
                <a onclick="return deletepoll();" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/opinion/deletepoll?pollid=<?php echo $pollid;?>&user_id=<?php echo $userid;?>">
                    <div class="detailpollmargine">
                        <span style=" font-size: 22px;">Delete Poll</span>
                    </div>
                </a>
           </div>
       <?php } ?>
                
        </div>
        
        
            <div style="width: 390px;margin-left: 0px;" class="popup_listmaindiv">
                <div style="float: left;">
                             <div class="polldetail_design" id="mydislist">
                                 <?php $length = strlen($listname) ;
                                       if($length>25)
                                        {
                                          $title = substr($listname,0,25);  
                                 ?>
                                <span style="float: left; padding: 4px 7px 0 2px;color: white;" ><?php echo $title; ?>..</span>
                                <?php } else { ?>
                                <span style="float: left; padding: 4px 7px 0 2px;color: white;" ><?php echo $listname; ?></span>
                                <?php } ?>
                             </div>                        
                                <div class="listdetail_titlete_textfild_rate" style="background: #789bda; padding-left: 3px;text-align: left;">
                                    <div style="padding-top: 4px;">
                                    <span style="color: white;">  <?php echo 'Rating';?></span>           
                                    </div>
                                </div>
                <div class="floatleft cursorpointer" style="margin-top: -2px; margin-left: 8px;">
                 <img id="downarrow" onclick="changearrowlistdetail('downarrow')" src="<?php echo $baseurl;?>/images/freeworld/down-arrow.png"/>
                </div>
                </div>                     
                <div style="width: 370px;">
                
                    <table id="myTable" style="width: 150px;float: left; ">
                      <tr>
                    <?php $i=0;                      
                       foreach ($pollans as $value) {
                        $vote  = PollVoting::model()->findAll("pollanswer_id = '$value->id'"); 
                        
                        if(empty($vote))
                        { 
                            $countpolans = 1;
                        }                        
                        $count  = count($vote);
                        
                        $result = ($count*100)/$countpolans;
                        
                        if(empty($vote))
                        {
                            $countpolans = $countpop;
                        }
                         if($data['user_id']!=$userid && empty($mytakenlist))
                         { 
                             if($i > 2)
                             { 
                                 break;
                             }
                         }
                          
                    ?>
                        <td style="background-color: white;">
                            <div class="polldetail_textfild_first" >
                                 <span style="float: left; padding: 4px 0 0 2px;" ><?php echo  $value->value ;?></span>
                            </div>
                        </td>
                        <td style="background-color: white;">
                            <div class="listdetail_titlete_textfild" >
                                <div class="detailpoll_percentagebar">  
                                 <div class="detailpoll_percentagebariner" style="width: <?php echo abs(floor($result));?>%"> </div>
                                 <div class="floatright"><?php echo abs(floor($result)) ;?>%</div>
                                </div>
                            </div>
                        </td>
                       <?php
                        echo "</tr><tr>";
                       ?>
                      
                      <?php  $i++; } ?>
                     </tr>
                    
                    </table>
                </div>
                
                <?php if($data['user_id']!=$userid && empty($mytakenlist)) { ?>
                
                <div class="seemore_listdetail">
                    <a class="textdecoration" href="#" onclick="takelist_popup('<?php echo $baseurl;?>/index.php/opinion/seepoll?pollid=<?php echo $pollid;?>');">
                    <span> Take the poll</span>
                    </a>
                </div>
                <?php } ?>
                
            </div>
        
        
        
                   
<?php
}
else
{ ?>
   
          <div style="width: 100%; padding: 80px 0 0 230px;" class="popup_list_title_div1">               
                <div class="popup_row_namefild">
                    <span style="color: grey; font-size: 32px;">No poll created</span>
                </div>
          </div> 
                
<?php                
}
?>
</div>
</div>