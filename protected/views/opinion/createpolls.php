<?php
$baseurl = Yii::app()->request->getBaseUrl(true);
$userid  = Yii::app()->user->getID();
$siteurl = Yii::app()->params['siteurl'];
// Get client script
$cs=Yii::app()->clientScript;

// Add CSS
$cs->registerCSSFile($baseurl.'/css/colorbox.css');

// Add JS
$cs->registerScriptFile($baseurl.'/js/jquery.form.js');
$cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>

<script>
$(document).ready(function(){
  $("#searchfriend").keyup(function(){
    var c    = document.getElementById("searchfriend").value; 
    var frnd = document.getElementById("strfrnd").value; 
    if(trim(c).length>=0){     
    var url = '<?php echo $baseurl;?>'+"/index.php/preferences/searchfriendlist";
    document.getElementById('lodingforfrind').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        //dataType:'json',
        data:{
                title:c,   
                frnd:frnd
             },
             success:function(value){ 
                 document.getElementById('lodingforfrind').style.display = "none";
                 if(value!=0)
                     {
                         document.getElementById('friendser').style.display = "block";
                         document.getElementById('nofrnd').style.display = "none";
                         $('#friendser').html('').append(value)
                     }
                     else
                        {
                            document.getElementById('friendser').style.display = "none";
                            document.getElementById('nofrnd').style.display = "block";
                        }
             
    }
    }); 
   }
  });

});

</script>

<div class="widthauto">
    <?php $this->renderPartial('sidenavigation'); ?>
         <div class="outerdiv" id="liscreation">
            <div style="width: 100%;height: 60px;">
                 
             </div>
         



 <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$siteurl.'/index.php/opinion/createpolls',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',
        'onsubmit'=>'return cteatepolls();',
        'validateOnSubmit'=>true,
     ),
      ));
	  $friendsserch ='';
?>
      <div class="check">
        <div class="listmaindiv" id="mintitle" style="width:704px;margin-top: -84px;">
            <input type="hidden" id ="hidden_baseurl" value="<?php echo $baseurl ?>"/>
           
            <div id="mintitle" class="listnew_title_div1" style="width: auto;">
                <h2 class="create_new_show">Create New </h2>

                  <div class="listnew_title">
                 <div class="forgotpasdiv1" style="width: 118px;">
                     <span>*poll type</span>
                 </div>
                 <div class="type_listnew OptionselectBoxContan">
                     
                     <select  id="listtype" name="ListForm[listtype]"  class="select-list_listtitle">
                         <option value="select" selected="selected" >Select</option>
                          <option value="public" >Public</option>
                          <option value="private " >Private </option>
                          <option value="official" >Official</option>
                     </select>
                 </div>
                      <br/>
               <div class="preferences_listtitle_requretitle_select" id="listtypereq">
                    <span>This field is required </span>
                </div>
             </div>
                <div class="tutle_one">
                <div class="list_lable">
                    <span> *title: </span>
                </div>
              <!-- <div class="row_textfild">
                    <input id="title" type="text" name="ListForm[title]" class="forgotpasinpu" value="" onblur="checkdatabase('<?php //echo $baseurl;?>')"/>
                </div>-->
                  <div class="row_textfild">
                    <input id="title" type="text" name="ListForm[title]" style="margin-left: -4px;" class="forgotpasinpu" value="" onblur="checkdatabasepoll('<?php echo $baseurl;?>')" onchange="suggestion('<?php echo $baseurl;?>')"/>
                </div>
              <br/>
                <div id="loding" style="float: right; width: 16px; height: 20px; margin-top: -32px;display: none;">
                    <img src="<?php echo $baseurl;?>/images/ajax-loader.gif"/>
                </div>
                <div style="margin: -6px 0 0 142px;" class="validtitle" id="valid">
                    <span >This is valid title</span>
                </div>
                <div style="margin: -6px 0 0 142px;" class="invalidtitle" id="invalid">
                    <span style="color: red;">This is not valid title</span>
                </div>
                <div class="preferences_listtype_requretitlee" id="titlereq">
                    <span>This field is required </span>
                </div>
                 <div class="preferences_listtype_seesuggestion" id="seesuggestion">
                     
                     <a href="#" class="textdecoration" onclick="suggestion_popup('<?php echo Yii::app()->params['globelUrl'];?>/opinion/suggestionpopup');" ><!--onclick='javascript:var titless= document.getElementById("title").value;parent.$.colorbox({width:"704px",data:{title:titless}, height:"600px;", opacity:0.3, href:"<?php //echo Yii::app()->params['globelUrl'];?>/preferences/suggestionpopup"}); return false;'-->
                         <span > See Suggestion </span> 
                     </a>
                     
                </div>
                </div>
            </div>           

           <div  style="width: 482px; margin-top: 6px; height: 50px;"class="listnew_title_div_11">
                 <div class="forgotpasdiv_1" style="width: 144px;">
                     <span>
                        *pic a topic:
                     </span>
                 </div>
                 <div class="topic_listnew OptionselectBoxContan">
                     <select  id="interest_topic" name="ListForm[interest]"  class="select-list_listtitle">
                         <option value="select" selected="selected" >Select</option>
                         <?php foreach ($interest as $inter){?>
                          <option value="<?php echo $inter->id?>" ><?php echo $inter->interest?></option>
                          <?php } ?>
                     </select>
                 </div>
               <br/>
                 <div class="preferences_requretitle_select" id="interestreq">
                     <span>This field is required </span>
                 </div>
             </div>
 
           <div class="listnew_title_div1"><div class="image"><img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/header/1351743964imagefds.jpg"/></div>
                <div class="image_lable_revelent">
                    <span> Upload a Relevant Picture:</span>
                </div>
                <div class="row_textfild">
                    <input id="image" type="file" name="image" value="" accept="image/*" />
                </div>
                <div style="margin: 6px 0 0 4px; padding-left: 34px;" class="preferences_listtype_requretitlee" id="imgreq">
                    <span>Please select the image </span>
                </div>
                <div class="whole">
                <div class="pera"><p class="preferences_listtype_imgpara1">Upload a file from your computer with the <br/>following  formats: jpg, png, gif.
</p></div>
            </div>
            <div class="inst"><span>Instructions:</span></div>
            <div class="pera_2"><p class="preferences_listtype_imgpara2">Please enter responses in General to Specific order (i.e. for the list Favorite Vacation Spots, responses should be entered as Country: State/City: specific site. If necessary, please enter any list-specific instructions below.</p></div>
            <div class="area">
                <textarea rows="4" cols="46" name="instruction" id="instruction" class="preferences_listtype_area_one" style="color: black;" onfocus="hidepolltext(this.value,this.id);" onblur="hidepolltext(this.value,this.id);">Enter poll Instruction</textarea>
            </div>
            </div>
<div class="preferences_listtype_width">

       <h2 class="create_new_show">Invite Friends </h2>
       <div class="preferences_listtype_middle"><p class="preferences_listtype_middlepara">Optional Choose friend from wayo or invites friends via email</p></div>
       <div class="show">
           <div class="listtitle_newdesdiv1">
               <div class="listtitle_newdesdiv11">
                   <a href=" " class="textdecorationwhite" style="text-decoration: underline;">
                   <span>
                       Alphabetical
                   </span>
                   </a>
                   |
               </div>
               <div class="listtitle_newdesdiv12">
                   <div class="listtitle_newdesdiv121">
                       <span>
                        Group
                       </span>
                   </div>
                   <img class="cursorpointer" src="<?php echo $baseurl;?>/images/freeworld/arrow-1.png"/>
               </div>
               <div class="listtitle_newdesdiv13">
                   <div class="floatleft">
                       <input id="searchfriend" type="text" name="search_tag"  class="listtitle_newdesdiv13_input" value="Search friends" onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"/> 
                       
                   </div>
                       <div class="listtitle_newdesdiv13_imgdiv">
                           <img class="cursorpointer" style="height: 30px;" src="<?php echo $baseurl;?>/images/freeworld/arrow-1.png"/>
                        </div>
                       <div id="lodingforfrind"  style="margin-top: 6px; margin-left: -38px; float: left;display: none;">
                       <img src="<?php echo $baseurl;?>/images/ajax-loader.gif" />
                       </div>
               </div>
           </div>
           <div class="listtitle_newdesdiv2">
               <div class="listtitle_newdesdiv21_frinds"><span>Friends</span></div>
               <div class="listtitle_newdesdiv21_frinds"><span>Favorites</span></div>
             <div class="overflow_div" id="content_1">
               <div id="friendser" class="listtitle_newdesdiv21">
                   
                  <?php if(count($registered_users)>0){
                      
                        for($i=0; $i<count($registered_users); $i++){ 
                        $friend_id = $registered_users[$i]['friend_id'];
                        $uiser_id  = $registered_users[$i]['user_id'];
                           if($friend_id!=$userid)
                           {
                               $friendsserch.= $friend_id .',';
                               $user = User::model()->find("id='$friend_id'");
                   ?>
                   
                       <div class="listtitle_newdesdiv221">
                           <div class="listtitle_newdesdivcheckbox">
                               <input id="friends_<?php echo $friend_id; ?>" type="checkbox" name="friends[]" value="<?php echo $friend_id; ?>" />
                           </div>
                           <div class="listtitle_newdesdiv22img">
                              <?php if($user['image']!=''){?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/header/<?php echo $user['image'];?>"/>
                               <?php } else { 
                                  if($user['gender']=='male')
                                     {
                                ?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($user['gender']=='female') {?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                           </div>                              
                           <div class="listtitle_newdesdiv22name"> 
                           <span><?php echo $user['firstname'].' '.$user['lastname']; ?></span>
                           </div>
                       </div>
                   <?php } else {  $user = User::model()->find("id='$uiser_id'"); $friendsserch.= $uiser_id .',';?>
                   
                      <div class="listtitle_newdesdiv221">
                           <div class="listtitle_newdesdivcheckbox">
                               <input id="friends_<?php echo $uiser_id; ?>" type="checkbox" name="friends[]" value="<?php echo $uiser_id; ?>" />
                           </div>
                           <div class="listtitle_newdesdiv22img">
                              <?php if($user['image']!=''){?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/header/<?php echo $user['image'];?>"/>
                               <?php } else { 
                                  if($user['gender']=='male')
                                     {
                                ?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($user['gender']=='female') {?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                           </div>                              
                           <div class="listtitle_newdesdiv22name"> 
                           <span><?php echo $user['firstname'].' '.$user['lastname']; ?></span>
                           </div>
                       </div>
                       
                   <?php } } } else { ?>   
                   <span class="listtitle_newdesdiv22name">No friends</span>
                  <?php }?>
               </div>
                 <div style="display: none;" id="nofrnd" class="listtitle_newdesdiv21">
                     <span class="listtitle_newdesdiv22name"> 
                       No friends
                      </span>
                     <?php $strfrnd = substr($friendsserch,0,strlen($friendsserch)-1); ?>
                     <input type="hidden" value="<?php echo $strfrnd;?>" id="strfrnd" />
                 </div>
               <div class="listtitle_newdesdiv22">
                   <div class="listtitle_newdesdiv21">                  
                   
                       
                       <?php if(count($favourite)>0){
                      
                        for($i=0; $i<count($favourite); $i++){ 
                        $friend_id = $favourite[$i]['favorite_id'];
                        $uiser_id  = $favourite[$i]['user_id'];
                        $user = User::model()->find("id='$friend_id'");
                   ?>
                   
                       <div class="listtitle_newdesdiv221">
                           <div class="listtitle_newdesdivcheckbox">
                               <input id="friends_<?php echo $friend_id; ?>" type="checkbox" name="friends[]" value="<?php echo $friend_id; ?>" />
                           </div>
                           <div class="listtitle_newdesdiv22img">
                              <?php if($user['image']!=''){?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/profile/user_thumbs/header/<?php echo $user['image'];?>"/>
                               <?php } else { 
                                  if($user['gender']=='male')
                                     {
                                ?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                               <?php } elseif($user['gender']=='female') {?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                               <?php } else {?>
                               <img class="auth_header_user_img" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                               <?php } }?>
                           </div>                              
                           <div class="listtitle_newdesdiv22name"> 
                           <span><?php echo $user['firstname'].' '.$user['lastname']; ?></span>
                           </div>
                       </div>
                   <?php }                       
                     } else { ?>   
                   <span class="listtitle_newdesdiv22name">No favorites</span>
                  <?php }?>
                       
                
               </div>
               </div>
           </div>
               <div class="text_para">
                   <p class="preferences_listtype_imgpara3">Don’t see your friend on the list? Enter email addresses separated by commas in the field below to send invites to your friends!</p>
               </div>
               <div class="are"><textarea id="instruction2" onfocus="hidetextemail(this.value,this.id);" onblur="hidetextemail(this.value,this.id);" rows="6" cols="50" name="instructin" style="color: black;" class="preferences_listtype_arew">Enter email addresses</textarea></div>
           </div>
       </div>

</div>     
            <div class="listy_title_preview" id="create" >            
                <!--<input type="button" name="submit" value="submit" id="submit"/>-->
             <input id="submitform" type="image" src="<?php echo $baseurl; ?>/images/buttons/preview_poll.png" style="float: left;" />            
             <input style="display: none;float: left;" id="updatelist" type="image" src="<?php echo $baseurl; ?>/images/buttons/update_poll.png" />
             <img style="display: none;float: left; margin-top: 11px;height: 25px;" id="loadingimg" src="<?php echo $baseurl; ?>/images/loading.gif" />
           </div>
        
        </div>
         
          
           <div class="floatleft"  style="margin-left: 221px;">
                <img stylse="width: 157px; height: 515px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/advertisment.png"/>
            </div>
        </div>

<?php $this->endWidget(); ?>
             
    </div>
</div>
