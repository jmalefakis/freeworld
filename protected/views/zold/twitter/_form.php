<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'friend_name'); ?>
		<?php echo $form->textField($model,'friend_name'); ?>
		<?php echo $form->error($model,'friend_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'friend_twitterid'); ?>
		<?php echo $form->textField($model,'friend_twitterid',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'friend_twitterid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'friend_twitteimg'); ?>
		<?php echo $form->textField($model,'friend_twitteimg',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'friend_twitteimg'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified'); ?>
		<?php echo $form->textField($model,'modified'); ?>
		<?php echo $form->error($model,'modified'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->