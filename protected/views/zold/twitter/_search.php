<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'friend_name'); ?>
		 <?php echo $form->textField($model,'friend_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'friend_twitterid'); ?>
		<?php echo $form->textField($model,'friend_twitterid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'friend_twitteimg'); ?>
		<?php echo $form->textField($model,'friend_twitteimg'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php //echo $form->textField($model,'created'); 
                   $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'Twitter[created]',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;'
                    ),
                )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified'); ?>
		<?php //echo $form->textField($model,'modified'); 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'Twitter[modified]',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;'
                    ),
                )); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->