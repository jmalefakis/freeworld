<?php $baseurl = Yii::app()->request->baseUrl; 
      $userid = Yii::app()->user->getID();
?>
<div class="widthauto">    
        <?php $this->renderPartial('sidenavigation'); ?> 
    
    
    <div class="official_today">
     
 <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/user/officialtoday',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate',            
        'validateOnSubmit'=>true,
     ),
      )); 
?>        
        
        
        
        <div class="official_today_div1">
            <div class="official_today_date">
                <div class="official_today_sortdate">
                    <span> Sort by date:</span>   
                </div>
                <div class="floatleft">
                    <?php                          
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[birthdate]', 
                    'value'=> '', 
                    // additional javascript options for the date picker plugin
                    'options'=>array(                                           
                         'yearRange'=> '-102:+0',
                        'dateFormat'=>'yy-mm-dd',                        
                        //'defaultDate'=>'01/01/2012',
                        //'yearRange'=>'1960:2012', 
                        'showAnim'=>'fold',                        
                        'debug'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;width:200px;',
                         'readonly'=>"readonly",
                        'onchange'=>"sortbydate('$baseurl')",
                    ),
                )); ?> 
                </div>
            </div>
            
            
             <?php             
               if(!empty($takenlist))
               {         
                   $coun = 0;
                   foreach ($data as $value){ 
                      if($value->listtype =='public'){
                        $take = ListTaken::model()->find("user_id = $userid AND list_id = $value->id"); 
                         if($take)
                            {
                                continue;
                            }  
                        $coun++;                    
          ?>
        
          <div class="outer_see_list">
            <div class="img_outer_official">
            <div class="latest_image_official">
                <img class="image_mylist_official" src="<?php echo $baseurl;?>/images/<?php echo $value->image;?>"/>
            </div>
                <!--<a class="textdecoration" href="<?php //echo $baseurl;?>/index.php/list/takelist?listid=<?php //echo $value->id;?>"> <span>Take this list</span></a>-->
            </div>
            
            <div class="outer_latesttitle_div">
                    <div class="latest_title_div_official">
                         <span><?php echo $value->title; ?></span> <!--<a class="textdecorationblack" href="<?php //echo $baseurl;?>/index.php/list/list_detail?listid=<?php //echo $value->id;?>&userid=<?php //echo $value->user_id;?>"></a>-->
                    </div>
            </div>
              
              <div style="clear: both;"></div>
              <div class="maintable">
            <div class="list_title_div" style="margin-right: 20px;">
                <table class="tablediv">
                    <tr>
                        <td class="titlediv">
                            
                                <span><?php echo $value->title ; ?></span>
                           
                        </td>
                        <?php $datacolumn = ListColumn::model()->findAll("list_id = '$value->id' AND user_id = '$value->user_id'");
                                $count      = count($datacolumn);
                                $count      = $count+1;
                              $i=0;
                              foreach ($datacolumn as $column){
                                     if($i==3)
                                     {
                                         break;
                                     }
                        ?>                        
                        <td class="column_div">
                                    <span> <?php echo $column->value ;?> </span> 
                                    <?php $i++; ?> 
                        </td>
                         <?php }?>
                    </tr>
                    
                    <tr>
                    <?php $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2)', 'params'=>array(':x'=>$value->id,':y'=>$value->user_id)));
                    
                        $temp = 1;$p = 0;
                       foreach ($datarow as $row){ 
                             
                          if($p==3)
                           {
                              break;
                           }
                      ?>
                        <td class="row_div">
                            <span> <?php echo $row->value ;?> </span>
                        </td>
                       <?php 
                       if($count<4){
                            if($temp%$count==0)
                            {
                                echo "</tr><tr>";
                                 $p++;
                            }
                       }
                        else
                        {
                            if($temp%4==0)
                            {
                             echo "</tr><tr>";
                             $p++;
                            }
                        }
                        $temp++;
                       ?>
                      
                      <?php  }  ?>
                   </tr>
               
                </table>
           </div>
            
        </div> 
            
            
            <div class="official_today_list">
                <div class="official_today_addtolist">
                    <a class="textdecoration"  href="<?php echo $baseurl;?>/index.php/list/takelist?listid=<?php echo $value->id;?>"> <span> Add to List</span>   </a>
                </div>
                <div class="floatleft" style="margin-top: 8px;">
                    <input class="official_today_input" type="text" name="addtolist" value=""/>
                </div>
            </div>
              
              
        </div>
            
             <?php } }  ?> 
            
            <?php } else   {
                 //$data = ListForm::model()->findAll(array('order'=>'id DESC', 'condition'=>'user_id !=:x', 'params'=>array(':x'=>$userid)));
                         
                         //("user_id != '$userid'");
              $coun = 0;
                 foreach ($data as $value){ 
                     if($value->listtype =='public'){
                     $coun++;
               ?>
        <div class="outer_see_list">
         <div class="img_outer_official">
            <div class="latest_image_official">
                <img class="image_mylist_official" src="<?php echo $baseurl;?>/images/<?php echo $value->image;?>"/>
            </div>
                <!--<a class="textdecoration" href="<?php //echo $baseurl;?>/index.php/list/takelist?listid=<?php //echo $value->id;?>"> <span>Take this list</span></a>-->
         </div>
            
            <div class="outer_latesttitle_div">
                              
                    <div class="latest_title_div_official">
                       <span><?php echo $value->title; ?></span> <!--<a class="textdecorationblack" href="<?php //echo $baseurl;?>/index.php/list/list_detail?listid=<?php //echo $value->id;?>&userid=<?php //echo $value->user_id;?>"></a>-->
                    </div>
            </div>
            
            
            <div style="clear: both;"></div>
            <div class="maintable">
            <div class="list_title_div" style="margin-right: 20px;">
                <table class="tablediv">
                    <tr>
                        <td class="titlediv">
                            
                                <span><?php echo $value->title ; ?></span>
                           
                        </td>
                        <?php $datacolumn = ListColumn::model()->findAll("list_id = '$value->id' AND user_id = '$value->user_id'");
                                $count      = count($datacolumn);
                                $count      = $count+1;
                              $i=0;
                              foreach ($datacolumn as $column){
                                     if($i==3)
                                     {
                                         break;
                                     }
                        ?>                        
                        <td class="column_div">
                                    <span> <?php echo $column->value ;?> </span> 
                                    <?php $i++; ?> 
                        </td>
                         <?php }?>
                    </tr>
                    
                    <tr>
                    <?php $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0 OR serial_no=1 OR serial_no=2)', 'params'=>array(':x'=>$value->id,':y'=>$value->user_id)));
                    
                        $temp = 1;$p = 0;
                       foreach ($datarow as $row){ 
                             
                          if($p==3)
                           {
                              break;
                           }
                      ?>
                        <td class="row_div">
                            <span> <?php echo $row->value ;?> </span>
                        </td>
                       <?php 
                       if($count<4){
                            if($temp%$count==0)
                            {
                                echo "</tr><tr>";
                                 $p++;
                            }
                       }
                        else
                        {
                            if($temp%4==0)
                            {
                             echo "</tr><tr>";
                             $p++;
                            }
                        }
                        $temp++;
                       ?>
                      
                      <?php  }  ?>
                   </tr>
               
                </table>
           </div>
            
        </div> 
            
            
            <div class="official_today_list">
                <div class="official_today_addtolist">
                    <a class="textdecoration"  href="<?php echo $baseurl;?>/index.php/list/takelist?listid=<?php echo $value->id;?>"> <span> Add to List</span>   </a>
                </div>
                <div class="floatleft" style="margin-top: 8px;">
                    <input class="official_today_input" type="text" name="addtolist" value=""/>
                </div>
            </div>
            
            
        </div>
            
            <?php } } }?>
            <!--<div class="floatleft">
                <img class="official_today_bestsinger_fal" src="<?php //echo Yii::app()->request->baseUrl; ?>/images/freeworld/best.png"/>
            </div>-->
            
            
            <div class="official_today_createnew">
                <a class="textdecoration"  href="<?php echo $baseurl;?>/index.php/list/listtitle"><span> Create New</span></a>
            </div>
        </div>
        
        
        
        
        <div class="official_today_div2">
            
            <div class="official_today_date">
                <div class="official_today_sortdate">
                    <span> Sort by topic:</span>   
                </div>
                <div class="floatleft">
                   <select onchange="sortbydate()"  id="interest" name="Interest[user]"  class="official_today_droup">
                         <option selected=selected>Select your interest</option>
                         <?php foreach ($interest as $inter){?>
                          <option <?php if(isset($post)){ ?> value="<?php echo $post['interest']; ?>" selected=selected" <?php } else { ?> value="<?php echo $inter->id;?>" <?php } ?> ><?php echo $inter->interest?></option>
                          <?php }?>
                     </select>
                </div>
            </div>
            
            <?php 
                   $baseurl = Yii::app()->request->baseUrl;
                   foreach ($allpoll as $value){ 
                   $pollans = PollVoting::model()->find("poll_id = '$value->id' AND user_id = '$userid' ");
                   //echo "<pre>";print_r($pollans);
            ?>
            
            
            <div class="outer_see_list">
                 <div class="img_outer_official">
                    <div class="latest_image_official">
                        <img class="image_mylist_official" src="<?php echo $baseurl;?>/images/pollimages/<?php echo $value->image;?>"/>
                    </div>                       
                 </div>

                <div class="outer_latesttitle_div_off">
                        <div class="latest_title_div_off_poll">
                           <span><?php echo $value->question; ?></span> 
                        </div>
                </div>
                <!--<div class="floatleft">
                    <img id="downarrow<?php echo $value->id;?>" onclick="changearrowofficial('<?php echo $value->id;?>')" src="<?php echo $baseurl;?>/images/freeworld/down-arrow.png"/>
                </div>-->
                <?php if(empty($pollans)) {?>
                <div id="polldetail<?php echo $value->id;?>" class="official_polldeteil">
                    <span>What do you think ? Click on the text to started!</span>
                </div>
                <?php } else {?>
                    
                        
                <?php } ?>
            </div>

            <?php } ?>
            
            
            
           <!-- <div class="official_today_secondcol">
                <img class="official_today_singer_fal" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/best-2.png"/>
            </div>
            <div class="official_today_secondcol">
                <img class="official_today_singer_fal" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/best-2.png"/>
            </div>
            <div class="official_today_secondcol">
                <img class="official_today_singer_fal" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/best-2.png"/>
            </div>
            <div class="official_today_secondcol">
                <img class="official_today_singer_fal" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/best-2.png"/>
            </div>
            <div class="floatleft">
                <img class="official_today_bestsinger_fal" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/best.png"/>
            </div>-->
            
            <div class="official_today_list">
                <div class="official_today_addtolist">
                    <span> Add to List</span>   
                </div>
                <div class="floatleft" style="margin-top: 8px;">
                    <input class="official_today_input" type="text" name="addtolist" value=""/>
                </div>
            </div>
            
        </div>
        
                  
<?php $this->endWidget(); ?>
        
        <div class="official_today_div3">
            <div class="official_today_date">                
                <div class="official_search">
                   <?php
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/list/search',
        'method'=>'GET',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'searchform',   
        'onsubmit'=>'return searchresult();',
        'validateOnSubmit'=>true,
     ),
      )); 
?>
                <div class="search_list_official">
                    <div class="search_list_div1_official">
                        <input id="searchres" type="text" name="search"  class="forgotpasinput_official"/>
                    </div>
                    <div class="search_list_div2_official">
                        <input type="image"   src="<?php echo $baseurl;?>/images/freeworld/arrow-1.png" style="height: 27px;"/>
                    </div>  
                    <div id="requred" class="search_list_div3_official" >
                        <span >Please fill first.</span>
                    </div>
                </div>

<?php $this->endWidget(); ?>
                </div>
            </div>
            <div class="floatleft">
                <img stylse="width: 157px; height: 515px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/advertisment.png"/>
            </div>
        </div>
    </div>
