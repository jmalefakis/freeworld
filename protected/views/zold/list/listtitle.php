<div class="mainouterdiv">
    <?php $this->renderPartial('sidenavigation'); ?>    
    <div class="outerdiv1_auth">
 <?php
        $baseurl = Yii::app()->request->baseUrl; 
        $siteurl = Yii::app()->params['siteurl']; 
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$siteurl.'/index.php/list/listtitle',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate', 
        'onsubmit'=>'return listtitle();',
        'validateOnSubmit'=>true,
     ),
      )); 
?>
        
        <div class="listmaindiv" id="mintitle" style="width:795px;">
            <div id="mintitle" class="listnew_title_div1" style="width: auto;">
                <div class="list_lable">
                    <span> Title*</span>                        
                </div>
                <div class="row_textfild">
                    <input id="title" type="text" name="ListForm[title]" value="" class="forgotpasinput" onchange="suggestion('<?php echo $baseurl;?>')" onblur="checkdatabase('<?php echo $baseurl;?>')"/>
                </div>
                <div id="loding" style="float: left; width: 20px; height: 20px; margin-top: 1px; margin-left: -18px;display: none;">
                    <img src="<?php echo $baseurl;?>/images/ajax-loader.gif"/>
                </div>
                <div class="validtitle" id="valid">
                    <span >This is valid title</span>
                </div>
                <div class="invalidtitle" id="invalid">
                    <span style="color: red;">This is not valid title</span>
                </div>                
                <div class="requretitle" id="titlereq">
                    <span>This field is required </span>
                </div>
            </div>
            <div class="listnew_title_div1">
                <div class="name_lable">
                    <span> Name*</span>                        
                </div>
                <div class="row_textfild">
                    <input id="ListForm_name" type="text" name="ListForm[name]" value="" class="forgotpasinput"/>
                </div>
                <div class="requretitle" id="namereq">
                    <span>This field is required </span>
                </div>
            </div>
            
           <div class="listnew_title_div1">
                 <div class="forgotpasdiv1" style="padding-right: 15px;">
                     <span>
                         Chose category *
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="interest" name="ListForm[interest]"  class="forgotpasinput">
                         <option value="select" selected="selected" >Select</option>
                         <?php foreach ($interest as $inter){?>
                          <option value="<?php echo $inter->id?>" ><?php echo $inter->interest?></option>
                          <?php }?>
                     </select>
                 </div> 
               <div class="requretitle_select" id="interestreq">
                    <span>This field is required </span>
                </div>
             </div>
            
            <div class="listnew_title_div1">
                 <div class="forgotpasdiv1" style="width: 108px;">
                     <span>
                         List Type *
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="listtype" name="ListForm[listtype]"  class="forgotpasinput">
                         <option value="select" selected="selected" >Select</option>                         
                          <option value="public" >Public</option>
                          <option value="private " >Private </option>
                          <option value="official" >Official</option>
                     </select>
                 </div> 
               <div class="requretitle_select" id="listtypereq">
                    <span>This field is required </span>
                </div>
             </div>
            
            
            <div class="listnew_title_div1">
                <div class="image_lable_revelent">
                    <span> Upload a <br/>Relevant Picture*</span>                        
                </div>
                <div class="row_textfild">
                    <input id="image" type="file" name="image" value="" class="image_upload_reg" accept="image/*"/>
                </div> 
                <div class="requretitle" id="imgreq">
                    <span>This field is required </span>
                </div>
            </div>            
           <div class="submitdiv">
            <input class="image_buttons" type="image" src="<?php echo $baseurl; ?>/images/buttons/next.png" />
           </div>           
              
        </div>
         
<?php $this->endWidget(); ?> 
    </div>    
</div>