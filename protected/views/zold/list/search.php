 <?php $this->renderPartial('sidenavigation'); ?>   
<div class="outerdiv1_auth">
 <?php $baseurl = Yii::app()->request->baseUrl;
       $userid = Yii::app()->user->getID();
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/list/search',
        'method'=>'GET',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'searchform',            
        'validateOnSubmit'=>true,
     ),
      )); 
?>
                <div class="search_list_page">
                    <div class="search_list_inputdiv">
                        <input size="65" type="text" name="search" value="<?php if(isset ($keyword))echo $keyword; ?>"  style="height: 28px;color: #A4A4B0;font-style: italic;"/>
                    </div>
                    <div class="floatleft" style="margin-top: -5px;">
                        <input class="image_buttons" type="image"   src="<?php echo $baseurl;?>/images/buttons/search-list.png"/>
                    </div>   
                </div>

<?php $this->endWidget(); ?>


<div class="mainouterdiv">
    <?php  if(isset($_GET['search']) && $_GET['search']!='' ){
    $countsearch = count($searches);
    if($countsearch>0){
       for($p=0;$p<$countsearch;$p++){ 
    ?>
    <div class="search_show">        
        <div class="list_name"> 
            <div class="search_img">
                <div class="list_image_div">
                    <img src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $searches[$p]['image'];?>" class="image_mylist"/>
                </div>
                <div class="search_name_div">
                    <span><?php echo $searches[$p]['title'];  ?></span>
                </div>
            </div>
            <?php $myuserinfo = User::model()->find(array('condition'=>'id=:y', 'params'=>array(':y'=>$searches[$p]['user_id'])));?>
            <div class="search_img">
                <div class="search_title_cre">
                            <span>Created by:</span>
                </div>            

                <div class="latest_title_div">
                            <span><?php echo $myuserinfo->firstname.' '.$myuserinfo->lastname; ?></span>
                </div>
                <div class="search_image_div" >
                    <img src="<?php echo Yii::app()->request->baseUrl;?>/images/profile/<?php echo $myuserinfo['image'];?>" class="image_mylist"/>
                </div>
           </div>
        </div>
        <div class="list_name">            
            <?php   $datarow    = ListRow::model()->findAll(array('order'=>'col_id,serial_no', 'condition'=>'list_id=:x AND user_id=:y AND (serial_no=0)', 'params'=>array(':x'=>$searches[$p]['id'],':y'=>$searches[$p]['user_id'])));
              $i=0;
                      foreach ($datarow as $row){ 
                     
                          if($i==3)
                                     {
                                         break;
                                     }
              ?>
            <div class="search_res" >
              
               <span><?php echo $row->value ;?> ,</span>
            </div>
            
              <?php $i++; } ?>
        </div>
        
        <?php $takenlist = ListTaken::model()->find(array('condition'=>'list_id=:y AND user_id=:x', 'params'=>array(':y'=>$searches[$p]['id'],':x'=>$userid)));
              $mylist = ListForm ::model()->find(array('condition'=>'id=:y AND user_id=:x', 'params'=>array(':y'=>$searches[$p]['id'],':x'=>$userid)));
              if(!empty($takenlist) || !empty ($mylist))
              {                  
        ?>
         <div class="listtake_insearch" >
            <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/list/viewsearchlist?listid=<?php echo $searches[$p]['id'];?>"> <span> View List</span></a>
        </div>
       <?php } else {?>
        
        <div class="listtake_insearch">
            <a class="textdecoration" href="<?php echo $baseurl;?>/index.php/list/takelist?listid=<?php echo $searches[$p]['id'];?>"> <span> Take this list</span></a>
        </div>
        <?php } ?>
       </div>
     <?php } ?>
      
    <div class="paging_insearch" >  
         <?php $this->widget('CLinkPager', array(
            'pages' => $pages,
        )) ?> 
    </div>
     
     <?php } else {?>
    
    <div class="nofound_insearch" >
        <span>No Result Found</span>
    </div>

    <?php } }?>
 

</div>
</div>