<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		 <?php echo $form->textField($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'password'); ?>
		<?php echo $form->textField($model,'password',array('size'=>20,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'company'); ?>
		<?php echo $form->textField($model,'company',array('size'=>20,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'admin'); ?>
		<?php echo $form->textField($model,'admin',array('size'=>20,'maxlength'=>50)); ?>
	</div>	

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php //echo $form->textField($model,'created'); 
                   $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[created]',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;'
                    ),
                )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified'); ?>
		<?php //echo $form->textField($model,'modified'); 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[modified]',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;'
                    ),
                )); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->