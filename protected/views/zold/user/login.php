<script>
            window.fbAsyncInit = function() {
            FB.init({
                    appId      : '376997562371789',
                    status     : false, 
                    cookie     : true,
                    xfbml      : true,
                    oauth      : true
                });               
                               
            };
            
            (function(d){
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
            }(document));

          function Facebook_login () {
            FB.getLoginStatus(function(response) { 
                if (response.status === 'connected') {
                    window.location = '<?php echo Yii::app()->request->baseUrl;?>/index.php/user/login_facebook';

                }
            });
          }                
</script>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>


<?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash' . $key . '">' . $message . "</div>\n";
    }
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'Email<span style="color: red;">*</span>',$htmlOptions=array('class'=>'email_div_pas')); ?>
		<?php echo $form->textField($model,'email',$htmlOptions=array('class'=>'password_register')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',$htmlOptions=array('class'=>'password_register')); ?>
		<?php echo $form->error($model,'password'); ?>
		
	</div> 

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="submits_login">
		<input type="image"  src="<?php echo Yii::app()->request->baseUrl?>/images/login_btn.png" /> 
	</div>

<?php $this->endWidget(); ?>
        
 <div class="facebook_connect">
     <div id="fb-root"></div>
        <div class="fb-login-button" onlogin="Facebook_login()" autologoutlink="true">Login with Facebook</div>
</div>   
        
        
</div><!-- form -->
