<?php $userid   = Yii::app()->user->getID();
      $baseurl = Yii::app()->request->baseUrl;
?>

<div class="userprofilemain">
     <?php $this->renderPartial('sidenavigation'); ?>   
    <div class="outerdiv1_auth">
    <div id="uperdiv" class="userprofileuper">
        
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    Username :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['firstname'].' '.$user['lastname'];?></sapn>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    BirthDate :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['birthdate']?></sapn>
            </div>
        </div>
        <?php if(isset($_GET['userid']) && ($id != $userid)) {?>
        <div class="floatleft">            
            <div class="usernamediv2">                
                 <?php if($user->image!=''){?>
                   <img class="image_mylist" src="<?php echo $baseurl; ?>/images/profile/<?php echo $user->image;?>"/>
                   <?php } else { 
                      if($user->gender=='male')
                      {
                    ?>
                   <img class="image_mylist" src="<?php echo $baseurl; ?>/images/defaultboy.jpg"/>
                   <?php } elseif($user->gender=='female') {?>
                   <img class="image_mylist" src="<?php echo $baseurl; ?>/images/defaultgirl.jpg"/>
                   <?php } else {?>
                   <img class="image_mylist" src="<?php echo $baseurl; ?>/images/default.jpg"/>
                   <?php } }?>
            </div>
        </div>
        <?php } ?>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    Gender :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['gender']?></sapn>
            </div>
        </div>        
        
        
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    	Country :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['country']?></sapn>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    	City/State :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['city_state']?></sapn>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    Occupation :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['occupation']?></sapn>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    	Education :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['education']?></sapn>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                   Your Interest :
                </span>
            </div>
            <div class="usernamediv2">
                <sapn><?php echo $user['interest']?></sapn>
            </div>
        </div>        
        
        
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                   Groups :
                </span>
            </div>
            <?php  $countgp = count($group); 
                   for($i=0;$i<$countgp;$i++)
                   {
                       $groupid = $group[$i]['group_id'];
               //foreach($group as $gpvalue) {  ?>
            <?php $gpname   = Group::model()->find("id = '$groupid'");
                  if($gpname->type == 'public')
                  {
                      
            ?>
            <div class="usernamediv2">
                <a class="textdecoration" href="<?php echo$baseurl;?>/index.php/user/seegroup?groupid=<?php echo $groupid;?>">
                    <span>
                                <?php  $comma = ($i<$countgp-1) ? ", " : ""; 
                                 echo $gpname->groupname.$comma;?>
                    </span>
                </a>
            </div>
            <?php } }?>
            
        </div>
     <?php if(isset($_GET['userid']) && ($id != $userid)) {?>
        
<?php   
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'save-form',
        'action'=>$baseurl.'/index.php/user/crew',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'class'=>'savemeform', 
        'name'=>'saveform',    
        'validateOnSubmit'=>true,
     ),
      ));
    if(empty($crewuser)){
?>
          <div class="crew_save" >
            <a href="javascript:void(0);" onclick="javascript:document.saveform.submit();"  class="textdecoration" >
                <span>Save in crew</span>
                <input  type="hidden" name="User[crewid]" value="<?php echo $_GET['userid'];  ?>"/>
            </a>
          </div>
   
<?php } $this->endWidget(); ?>
        <?php } else {?>
          <div class="editprofile_btn" >
            <a href="javascript:void(0);"  class="editprofile_anchor" onclick="editprofile();">
                <input class="image_buttons" type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/buttons/edit.png"/>
            </a>
          </div>   
        <?php }?>
        
 <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash' . $key . '">' . $message . "</div>\n";
    }
  ?>
        
    </div>
    
    <div id="lowerdiv" class="userprofilelower">
 <?php
    $baseurl = Yii::app()->request->baseUrl;    
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'abouteditprofile-form',
        'action'=>$baseurl.'/index.php/user/editprofile',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
        'class'=>'aboutprofileform', 
        'name'=>'editprofileform',  
        'enctype'=> 'multipart/form-data',
        'validateOnSubmit'=>true,
     ),
      )); 
?>      
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    First Name :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="text" name="User[firstname]" value="<?php echo $user['firstname'];?>" class="forgotpasinput"/>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    Last Name :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="text" name="User[lastname]" value="<?php echo $user['lastname'];?>" class="forgotpasinput" />
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    BirthDate :
                </span>
            </div>
            <div class="usernamediv2">
                <?php
                     $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                     'name'=>'User[birthdate]',
                     'value'=>$user['birthdate'],
                      // additional javascript options for the date picker plugin
                     'options'=>array(
                     'changeMonth'=>true,
                     'dateFormat'=>'mm/dd/yy',
                     'changeYear'=>true,
                     'yearRange'=>'1960:1996', 
                     'showAnim'=>'fold',
                     //'showButtonPanel'=>true,
                     'debug'=>true,
                     ),
                     'htmlOptions'=>array(
                     'readonly'=>"readonly",
                     'class'=>"forgotpasinput",
                     ),
                     ));
                 ?>
            </div>
        </div>
        <!--<div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    Password :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="password" name="User[password]" value=""/>
            </div>
        </div>-->
        
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    Gender :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="text" name="User[gender]" value="<?php echo $user['gender']?>" class="forgotpasinput"/>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    	Country :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="text" name="User[country]" value="<?php echo $user['country']?>" class="forgotpasinput"/>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    	City/State :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="text" name="User[city_state]" value="<?php echo $user['city_state']?>" class="forgotpasinput"/>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    Occupation :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="text" name="User[occupation]" value="<?php echo $user['occupation']?>" class="forgotpasinput"/>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                    	Education :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="text" name="User[education]" value="<?php echo $user['education']?>" class="forgotpasinput"/>
            </div>
        </div>
        <div class="usernamediv">
            <div class="usernamediv1">
                <span>
                   Your Interest :
                </span>
            </div>
            <div class="usernamediv2">
                <select  id="interest" name="User[interest][]" class="forgotpasinput_multy" multiple="multiple">                    
                         <?php foreach ($interest as $inter){?>                          
                            <option <?php if($user['interest'] == $inter->interest ) { ?> value="<?php echo $inter->interest?>" selected=selected" <?php } ?>  ><?php echo $inter->interest?></option>
                          <?php }?>
               </select>
                
            </div>
        </div>
        
        <div class="usernamediv" style="margin-bottom: 60px;">
            <div class="usernamediv1">
                <span>
                    Upload new image :
                </span>
            </div>
            <div class="usernamediv2">
                <input type="file" name="profilepic" value=""  class="image_upload_reg"/>            
            </div>
        </div>
        
        <div class="floatleft">
            <a href="javascript:void(0);">
                <input class="image_buttons" type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/buttons/save.png"/>
            </a>
        </div>
 <?php $this->endWidget(); ?>      
    </div>
</div>
</div>
