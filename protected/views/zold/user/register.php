
<h1>Register With Us</h1>

<p>Please fill out the following  for register:</p>


                            
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>

<div style="display: none;" id="q2">
    <span style="color: red;">Password and re-enter password are different</span>
</div>
<div  id="temp" style="display: none;">
    <span id="q3" style="color: red;"></span>
</div>

<div class="form">
 <?php
    $baseurl = Yii::app()->request->baseUrl; 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'action'=>$baseurl.'/index.php/user/register',
        'method'=>'post',        
        'enableClientValidation'=>true,
        'htmlOptions'=>array(        
        'name'=>'mform',  
        'enctype'=> 'multipart/form-data',
        'onsubmit'=>'return validate_register();',
        'validateOnSubmit'=>true,
 ),
    )); 
?>  
        <div class="register_name">
            
            <?php echo $form->label($user,'<span id="fnamespan">First Name </span><span style="color: red;">*</span>:',$htmlOptions=array('class'=>'register_name_div_first')); ?>
            <?php echo $form->textField($user,'firstname',$htmlOptions=array('class'=>'forgotpasinput','onblur'=> 'registerblur()')); ?>
            <?php echo $form->error($user,'firstname'); ?>
                 <!--<div class="register_name_div_first">
                     <span>
                         First Name : 
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input id="fname" type="text" name="User[firstname]" value="<?php //if(isset($post)){ echo $post['User']['firstname'];}?>" class="forgotpasinput"/>
                 </div>-->
                
        </div>
        <div class="register_name">
             <?php echo $form->label($user,'<span id="lnamespan">Last Name </span><span style="color: red;">*</span>:',$htmlOptions=array('class'=>'register_name_div_second')); ?>
            <?php echo $form->textField($user,'lastname',$htmlOptions=array('class'=>'forgotpasinput','onblur'=> 'registerblur()')); ?>
            <?php echo $form->error($user,'lastname'); ?>
                 <!--<div class="register_name_div_second" style="padding-right: 78px;">
                     <span>
                         Last Name : &nbsp;
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input id="lname"  type="text" name="User[lastname]" class="forgotpasinput" value="<?php if(isset($post)){ echo $post['User']['lastname'];}?>"/>
                 </div>   -->              
        </div>
        <div class="register_name">
                 <div class="register_name_div1">
                     <span>
                         Birthdate : 
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                 <?php  
                        if(isset($post)){ 
                            $birth = $post['User']['birthdate'];                            
                            }
                            else
                            { 
                                $birth = '';
                            }
                           
                 
                    $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[birthdate]', 
                    'value'=> $birth, 
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'changeMonth'=>true,                        
                         'yearRange'=> '-102:+0',
                        'dateFormat'=>'mm/dd/yy',
                        'changeYear'=>true,
                        //'monthStart'=>1 ,
                        'defaultDate'=>'01/01/2012',
                        //'yearRange'=>'1960:2012', 
                        'showAnim'=>'fold',
                        'showButtonPanel'=>true,
                        'debug'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;width:170px;',
                         'readonly'=>"readonly",
                    ),
                )); ?>
                 </div>                 
        </div>
        <div class="register_name">
                 <div class="register_name_div_secondii" style="padding-right: 107px;">
                     <span>
                         Gender : 
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="gender" name="User[gender]" class="forgotpasinput">
                         <option selected=selected>---Select your gender---</option>
                         <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['gender']; ?>" selected=selected" <?php } else { ?> value="Male" <?php } ?> >Male</option>
                         <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['gender']; ?>" selected=selected" <?php } else { ?> value="Female" <?php } ?> >Female</option>
                     </select>
                     
                 </div>                 
        </div>        
        
        <div class="register_name">
            
               <div class="forgot_div_email">
                     <span id="emailspan">
                         Email<span style="color: red;">*</span> : 
                     </span>
                 </div>
              <div class="forgotpasdiv2">
                     <input id="User_email"  type="text" name="User[email]" value="<?php if(isset($post)){ echo $post['User']['email'];}?>" class="forgotpasinput" onblur="checkdatabaseregister('<?php echo $baseurl;?>')"/>
               </div>
               <div id="loding" class="register_loding_img">
                    <img src="<?php echo $baseurl;?>/images/ajax-loader.gif"/>
                </div>
            <div id="emailexist" class="emailexist" >
                <span>Email already exist</span>
            </div>
            <div id="invalidemail" class="emailexist">
                <span>Email is not valid</span>
            </div>
            <div id="blankemail" class="emailexist">                
            </div>
      </div>
    
          <div class="register_name" >            
                 <div class="forgot_div_reemail" style="padding-right: 52px;">
                     <span id="conemailspan">
                         Re-enter Email <span style="color: red;">*</span>:
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input  id="conemail" type="text" name="confirm_email" class="forgotpasinput"/>
                 </div>                 
       </div>
    
    
    <div class="register_name">
                
        
                <?php echo $form->label($user,'<span id="passwordspan">Your Password </span><span style="color: red;">*</span>:',$htmlOptions=array('class'=>'forgot_div_pas')); ?>
		<?php echo $form->passwordField($user,'password',$htmlOptions=array('class'=>'password_register')); ?>
		<?php echo $form->error($user,'password'); ?>                 
   </div>
    
    <div class="register_name">
                 <div class="forgotpasdiv1" style="padding-right: 22px;">
                     <span id="repassworden">
                         Re-enter Password <span style="color: red;">*</span>:
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input  id="conpwd" type="password" name="confirm_password" class="forgotpasinput" value="<?php if(isset($user)){ echo $user['password'];}?>"/>
                 </div>                 
             </div>
    
    <div class="register_name">
                 <div class="register_name_div5">
                     <span>
                         Education : 
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select id="education" name="User[education]" class="forgotpasinput">
                        <option selected>--Select highest level---</option>
                        <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['education']; ?>" selected=selected" <?php } else { ?> value="PhD" <?php } ?> >PhD</option>
                        <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['education']; ?>" selected=selected" <?php } else { ?> value="Master" <?php } ?>  >Master</option>
                        <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['education']; ?>" selected=selected" <?php } else { ?> value="Bachelor" <?php } ?> >Bachelor</option>
                        <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['education']; ?>" selected=selected" <?php } else { ?> value="College" <?php } ?> >College</option>
                        <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['education']; ?>" selected=selected" <?php } else { ?> value="High School" <?php } ?> >High School</option>
                        <option  <?php if(isset($post)){ ?> value="<?php echo $post['User']['education']; ?>" selected=selected" <?php } else { ?> value="School" <?php } ?> >School</option>
                    </select>
                     <!--<input id="education"  type="text" name="User[education]" class="forgotpasinput"/>-->
                 </div>                 
        </div>
    
    <div class="register_name">
                 <div class="register_name_div_second" style="padding-right: 82px;">
                     <span>
                         Occupation : 
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select id="occupation" name="User[occupation]" class="forgotpasinput">
                        <option selected>--Select your occupation---</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Accounting/Finance" <?php } ?>  >Accounting/Finance</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Advertising" <?php } ?> >Advertising</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Builder" <?php } ?> >Builder</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Computer Related" <?php } ?> >Computer Related</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Designer" <?php } ?> >Designer</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Doctor" <?php } ?> >Doctor</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Engineering/Architecture" <?php } ?> >Engineering/Architecture</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Farmer" <?php } ?> >Farmer</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Government" <?php } ?> >Government</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Industrial" <?php } ?> >Industrial</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Lawyer" <?php } ?> >Lawyer</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Media" <?php } ?> >Media</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Political" <?php } ?> >Political</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Self Employed" <?php } ?> >Self Employed</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Sports" <?php } ?> >Sports</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Student" <?php } ?> >Student</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Teaching" <?php } ?> >Teaching</option>
                        <option <?php if(isset($post)){ ?> value="<?php echo $post['User']['occupation']; ?>" selected=selected" <?php } else { ?> value="Unemployed" <?php } ?> >Unemployed</option>
                    </select>
                     <!--<input id="occupation"  type="text" name="User[occupation]" class="forgotpasinput"/>-->
                 </div>                 
        </div>
        
        
            <div class="register_name">
                 <div class="forgotpasdiv1">
                     <span>
                         Pick interests from list:&nbsp;
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="interest" name="interest"  class="forgotpasinput">
                         <option selected=selected>--Select your interest---</option>
                         <?php foreach ($interest as $inter){?>
                          <option <?php if(isset($post)){ ?> value="<?php echo $post['interest']; ?>" selected=selected" <?php } else { ?> value="<?php echo $inter->interest?>" <?php } ?> ><?php echo $inter->interest?></option>
                          <?php }?>
                     </select>
                 </div>                 
             </div>
        
        <input type="hidden" value="register" name="hiddens"/>
             
        <div class="register_name">
                 <div class="forgotpasdiv1">
                     <span>
                         Upload profile picture :&nbsp;
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input id="img" type="file" name="profilepic" class="image_upload_reg"/>
                 </div>                 
        </div>
        
        <div class="register_name">
                 <div class="forgotpasdiv1" style="padding-right: 56px;">
                     <span>
                         Select Country: &nbsp;
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select class="forgotpasinput" onchange="print_state('state',this.selectedIndex);" id="country" name ="country">
                         <option value="" selected="selected">---Select country---</option>
                     </select>
                 </div>                 
             </div>
        
        <div class="register_name">
                 <div class="forgotpasdiv1" style="padding-right: 95px;">
                     <span>
                         City/State:  &nbsp;
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select class="forgotpasinput" name ="city_state" id ="state">
                         <option value="" selected="selected">---Select city---</option></select>
                    <script language="javascript">print_country("country");</script>
                 </div>                 
       </div>        
                        
        
        
          
         <div class="register_submitw">
		<input type="image"  src="<?php echo $baseurl?>/images/save_btn.png" />                 
	</div>
	

<?php $this->endWidget(); ?>
</div>
