<div class="main_header">
    <div class="bacground_footer">
        <div class="footer_dotted">
            
        </div>
    <div class="footer_div">
        <div class="footer_about">
            <a class="textdecoration" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/aboutus">
                <span>About Us</span>
            </a>   
        </div>
        <div class="footer_Contact">
            <a class="textdecoration" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/contact">
            <span>Contact</span>
            </a>
        </div>
        <div class="footer_Privacy">
            <a class="textdecoration"  href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/privacypolicy">
            <span>Privacy Policy</span>
            </a>
        </div>
        <div class="footer_Terms">
            <a class="textdecoration" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/terms">
            <span>Terms of Services</span>
            </a>
        </div>
        <div class="footer_Help"> 
            <a class="textdecoration" target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf">
            <span>Help</span>
            </a>
        </div>
    </div>
    </div>
</div>