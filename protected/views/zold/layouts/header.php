<script>
            window.fbAsyncInit = function() {
            FB.init({
                    appId      : '376997562371789',
                    status     : false, 
                    cookie     : true,
                    xfbml      : true,
                    oauth      : true
                });               
                               
            };
            
            (function(d){
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
            }(document));

          function Facebook_login () {
            FB.getLoginStatus(function(response) { 
                if (response.status === 'connected') {
                    window.location = '<?php echo Yii::app()->request->baseUrl;?>/index.php/user/login_facebook';

                }
            });
          }                
</script>


<?php $email = Yii::app()->user->getName(); ?>
<div class="main_header">
    <div class="header_divtwo">
        <div class="cetnter_div">
        <div class="header_logo">
            <?php if($email == 'Guest'){ ?>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/user/home">
            <img  class="header_imagelogo_fal"src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/logo.png"/>
            </a>
            <?php } else { ?>
             <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/user/officialtoday">
            <img  class="header_imagelogo_fal"src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/logo.png"/>
            </a>
            <?php }?>
        </div>
        <div class="flashmessage">
        </div>
        <?php if($email == 'Guest'){ ?>
        <div class="header_login">            
<?php $baseurl = Yii::app()->request->baseUrl; 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        //'action'=>$baseurl.'/index.php/user/home',
        'method'=>'post',        
	'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'name'=>'loginform',  
        'enctype'=> 'multipart/form-data',
        'onsubmit'=>'return login_header();',
        'validateOnSubmit'=>true,
 ),
)); ?>
            <div class="header_login_div1">
                <span>Login</span>
            </div>
            <div class="header_login_div2">
                <div class="header_login_email">
                    <span id="emailspan">email:</span>
                    <input type="text" name="LoginForm[email]" id="LoginForm_email" class="inputemail"/>
	        </div>
                <div class="header_login_password">
                    <span id="passwordspan">password:</span>
                    <input type="password" name="LoginForm[password]" id="LoginForm_password" class="inputemail"/>
                </div>
            </div>            

            <div class="main_header">
                <div class="header_login_checkbox">                    
                    <input type="checkbox" name="LoginForm[checkbox]" id="LoginForm_checkbox" />
                    <span>Keep me logged in</span> 
	        </div>                
            </div>
            <div class="header_login_div3">
                <div class="login_header">
                <input type="image" name="img" src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/login-btn.png" style="margin-right: 10px;"/>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/user/forgotpassword">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/forgotpassword.png"/>
                </a>
                </div>
                 <div class="facebook_connect">
                     <div id="fb-root"></div>
                        <div class="fb-login-button" onlogin="Facebook_login()" autologoutlink="true">Login with Facebook</div>
                </div>
                
            </div>
<?php $this->endWidget(); ?>
        </div>
        
        <?php } ?>
        </div>
    </div>
</div>