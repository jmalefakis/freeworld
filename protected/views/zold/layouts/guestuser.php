<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/wayoworld.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bjqs.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/homeslider.css" />
        
        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script> 
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/validate.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/thickbox.js"></script>        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/countries2.js"></script>
        

    
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
              
        
</head>

<body class="body_gestuser">

<div class="container" id="page">
    <!--<div class="cetnter_div">-->
	      
    <div id="header">
        <?php include 'header.php';  ?>
    </div>	

	<?php echo $content; ?>

	<div id="footer">
		<?php include 'footer.php';  ?>
	</div><!-- footer -->
    
</div><!-- page -->

</body>
</html>