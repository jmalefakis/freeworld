<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <meta name="sitelock-site-verification" content="9026" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- blueprint CSS framework -->	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/wayoworld.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bjqs.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/homeslider.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_newdis.css" />
          
          
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/favicon64.ico"/>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.2.js"></script> 
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/validate.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/thickbox.js"></script>        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/countries2.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/javascript.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fancybox.pack.js"></script>
        <style type="text/css">
                textarea:focus, input:focus{
            outline: none;
        }
        </style>

    
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="norton-safeweb-site-verification" content="oiacngith5i9pllm7-teevfxvx5d6r24v5iah1htobkqidjgu7zffluz6819pzljg2w3i6g50xgeef72ja1e5p5c7zfpy27khetuifx35gjxle0h17mkdmzmwt8vr0hq" />
</head>
<?php $path =  Yii::app()->getRequest()->pathInfo; ?>
<body>  
    
<!--<body class="body_gestuser">-->

<!--<div class="container" id="page">
    <!--<div class="cetnter_div">-->
	 <div id="wrapper_new">      
            <div id="header">
                <?php include 'header.php';  ?>
            </div>	

                <?php echo $content; ?>

                <div id="footer" <?php if($path == "user/home"){echo "style='background:none;'";}?>>
                        <?php include 'footer.php';  ?>
                </div><!-- footer -->
     </div>  
    &nbsp;
</body>
</html>