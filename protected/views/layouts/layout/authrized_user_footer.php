
<div class="footer_div">
    <div class="footer_bottom">
		<div class="footer_about">
			<a
				href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/aboutus">
				<span>About Us</span>
			</a>
		</div>
		<div class="footer_Contact">
			<a
				href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/contact">
				<span>Contact</span>
			</a>
		</div>
		<div class="footer_Privacy">
			<a
				href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/privacypolicy">
				<span>Privacy Policy</span>
			</a>
		</div>
		<div class="footer_Terms">
			<a
				href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/terms">
				<span>Terms of Services</span>
			</a>
		</div>
		<div class="footer_Help">
			<a target="_blank" 
				href="<?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf">
				<span>Help</span>
			</a>
		</div>
        <a class="sitlock" href="javascript:;" onclick="window.open('https://www.sitelock.com/verify.php?site=www.wayoworld.com','SiteLock','width=600,height=600,left=160,top=170');" >
            <img alt="website security" title="SiteLock" src="//shield.sitelock.com/shield/www.wayoworld.com"/>
        </a> 
	</div>
    
	<?php
	$siteUrl = Yii::app()->params['siteurl'];
	if ($siteUrl != 'http://wayoworld.com' && $siteUrl != 'https://wayoworld.com')
	{
	?>
	
	<div style="text-align: center;">Version: 1.2</div>
	
	<?php 
	}
	?>
</div>

