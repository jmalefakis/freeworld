<script type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/enscroll-0.4.0.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jstz.min.js"></script>    
<?php
$baseurl = Yii::app()->request->baseUrl;

if (isset($_GET['userid']))
    $id = $_GET['userid'];
else {
    $id = Yii::app()->user->getID();
}

$userid = Yii::app()->user->getID();
$user = User::model()->find("id = '$userid'");
$path = Yii::app()->getRequest()->pathInfo;
$url1 = explode("/", $path);
$url = $url1[0];

$referrer = Yii::app()->request->urlReferrer;

if (in_array($url1[1], array('takelist', 'viewResult')) 
	&& (strripos($referrer, 'preferences/viewResult') === false) 
	&& (strripos($referrer, 'preferences/editlist') === false) 
	&& (strripos($referrer, 'preferences/updateEntry') === false) 
	&& (strripos($referrer, 'preferences/takelist') === false) )
{
	Yii::app()->request->cookies['fw_back'] = new CHttpCookie('fw_back', Yii::app()->request->urlReferrer);
}

$url2 = "";
if (count($url1) > 1) $url2 = $url1[1];

$workingdir = Yii::app()->params['workingdir'];
$csv = str_getcsv(file_get_contents("$workingdir/protected/vendors/Freeworldmessageflash.csv"));

$notifications = $user->unreadNotifications();
$countnoti = count($notifications);

FriendHelper::getMyFriendsIds(true);

// Get client script
$cs = Yii::app()->clientScript;

if ($url1[1] != 'myProfile' && $url1[1] != 'myProfileCompany'){
    // Add CSS
    $cs->registerCSSFile($baseurl . '/css/colorbox.css');
}

// Add JS
$cs->registerScriptFile($baseurl . '/js/jquery.form.js');
$cs->registerScriptFile($baseurl . '/js/jquery.colorbox.js');

if (isset($_GET['userid']))
    $id = $_GET['userid'];
else {
    $id = Yii::app()->user->getID();
}
?>

<script>
$(function() {
	//console.log('back=',readCookie('fw_back'));
	
	$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
		if (jqXHR.status === 403) { window.location.href = window.location.href; };
	});
	
	$('.scrollbox4').enscroll({
		showOnHover: true,
		verticalTrackClass: 'track3',
		verticalHandleClass: 'handle3'
	});
	
	getNotifications();
	setInterval(function() { getNotifications(); }, 60 * 1000);

    <?php 
    if (!isset($_SESSION["tz"]))
    {
    ?>  
        var tz = jstz.determine();
        var timezone = tz.name();
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/user/setTimezone", {tz: timezone}, function(data) {});
    <?php 
    }
    ?>

    $(".name_header, .list_header_menu").mouseenter(function() {
		$(this).find(".menu-dropdown").toggle();		
	}).mouseleave(function(){
	    $(this).find(".menu-dropdown").toggle();
	});
    
}); //ready

</script>

<!---------------------------------------------------------------Header Start--------------------------->

<input type="hidden" name="baseurl"
	id="baseurl" value="<?php echo $baseurl;?>" />
<div class="header">
	<div class=" header_top">
		<div class="header_top_internal">
			<div class="header_logo">
				<a href="<?php echo $baseurl?>/index.php/preferences/officialToday"> <img
					src="<?php echo $baseurl?>/images/newimages/header_logo_3.png" alt="Wayo"
					title="Wayo" />
				</a>
			</div>
			<div class="header_right">
				<div class="header_top_right_search">
					<div class="serach" value="search">
						<div class="search_icon cursorpointer">
							<img src="<?php echo $baseurl?>/images/search_icon.png"
								onclick="serchonrecentHeader()" />
						</div>
						<input id="serchheader" class="searchheader"
						<?php if(isset($_SESSION['recentSearch']) && $_SESSION['recentSearch'] !='') { ?>
							style='font-style: normal; color: #555555;' <?php } ?>
							type="text"
							value="<?php if(isset($_SESSION['recentSearch']) && $_SESSION['recentSearch'] !='') {  echo $_SESSION['recentSearch']; unset($_SESSION['recentSearch']); } else{ echo "Search lists by title, topics or people";}?>"
							title="Search lists by title, topics or people"
							onclick="headerSearchValue(this.value)"
							onblur="textformatserch()" onfocus="textformalstyle()" />
					</div>
					<!--div class="or">OR</div-->
					<a href="<?php echo $baseurl?>/index.php/preferences/listtitle"
						title="Create New" class="createnewanchor">
						<div class="create_new">CREATE NEW</div>
					</a>
					<div class="icons">

						<div class="notification_bird" id="secret-why2">

							<div class="cursorpointer heightclass" title="Notifications"></div>

							<input type="hidden" value="<?php echo $baseurl;?>"
								id="baseurlpp" />

							<div id="popupContact2">
								<div class="arrowimagenoti">
									<img src="<?php echo $baseurl?>/images/arrow_notification.png" />
								</div>
								<div id="popupCont2"></div>
							</div>
						</div>
						<div id="backgroundPopup2"></div>

						<div class="logoutimg" onclick="logoutdivopen()" title="Settings"></div>
						<div id="popuplogout" class="logoutdiv" style="display: none;">
							<div class="arrowimagelogout">
								<img src="<?php echo $baseurl?>/images/arrow_notification.png" />
							</div>

							<div class="floatleft">
								<?php if($user->isMember()) { ?>
								<a href="<?php echo $baseurl?>/index.php/user/myProfile"> <img
									src="<?php echo $baseurl?>/images/open_ preferences.png"
									class="inputsvae_msg" style="width: 151px;" />
								</a>
								<?php } else {?>
								<a href="<?php echo $baseurl?>/index.php/user/myProfileCompany">
									<img src="<?php echo $baseurl?>/images/open_ preferences.png"
									class="inputsvae_msg" style="width: 151px;" />
								</a>
								<?php } ?>
							</div>
							<div class="inputsvae_msg_logout">
								<a href="<?php echo $baseurl?>/index.php/user/logout"> <img
									src="<?php echo $baseurl?>/images/logout.png"
									class="inputsvae_msg" />
								</a>
							</div>
						</div>

						<div id="backgroundPopup3"></div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	$userName = User::model()->findByPk($userid);
	?>

	<div class="header_bottom">
		<div class="header_bottom_internal">
			<div class="menu">
				<ul>
					<?php if ($user->isMember()) { ?>
					<li <?php if($url == 'user'){ ?> class="activeclass_name"
					<?php } else { ?> class="name_header" <?php } ?>><a
						<?php if($url == 'user'){ ?> class="activeclassanchor_name"
						<?php } else { ?> class="name_headeranchor" <?php } ?>
						href="<?php echo $baseurl;?>/index.php/user/myDashboard"><?php echo StringsHelper::capitalize($userName['firstname'] . ' ' . $userName['lastname']);?>
					</a> <?php } else { ?> <?php  $lengthname = strlen($userName['firstname']);
					if ($userName['firstname'] == '')
					{
					    $username = "company user";
					}
					else
					{
					    $username = $userName['firstname'];
					}
					?>
					
					<li <?php if($url == 'user'){ ?> class="activeclass_name"
					<?php } else { ?> class="name_header" <?php } ?>><a
						<?php if($url == 'user'){ ?> class="activeclassanchor_name"
						<?php } else { ?> class="name_headeranchor" <?php } ?>
						href="<?php echo $baseurl;?>/index.php/user/myCreated"><?php echo StringsHelper::capitalize($username); ?>
					</a> <?php } ?> <!-- User dropdow menu --> <?php 
					if ($url != 'user')
					{
					    ?>
						<ul class="menu-dropdown">
							<?php if (!$user->isCompany()) {?>
                            <li><a href="<?php echo $baseurl; ?>/index.php/user/myDashboard">Dashboard</a>
							</li>
                            <?php }?>
							<li><a href="<?php echo $baseurl; ?>/index.php/user/myCreated">Created</a>
							</li>

							<?php 
							if ($user->isMember())
							{
							    ?>

							<li><a href="<?php echo $baseurl; ?>/index.php/user/myTaken">Taken</a>
							</li>
							<li><a href="<?php echo $baseurl; ?>/index.php/user/myFriends">Friends
									& Companies</a></li>
							<li><a
								href="<?php echo $baseurl; ?>/index.php/user/myNotifications">Notifications</a>
							</li>
							<li><a href="<?php echo $baseurl; ?>/index.php/user/myProfile">My
									Profile</a></li>

							<?php } else { ?>

							<li><a href="<?php echo $baseurl; ?>/index.php/user/myFriends">Followers
									& Companies</a></li>
							<li><a
								href="<?php echo $baseurl; ?>/index.php/user/myNotifications">Notifications</a>
							</li>
							<li><a
								href="<?php echo $baseurl; ?>/index.php/user/myProfileCompany">My
									Profile</a></li>

							<?php 
							    }
							    ?>
						</ul> <?php
					}
					?>
					</li>

					<li
					<?php if ($url == 'preferences' && !preg_match("/recentSearch/i", $url2)) { ?>
						class="activeclass_list" <?php } else { ?>
						class="list_header_menu" <?php } ?>><a
						<?php if ($url == 'preferences' && !preg_match("/recentSearch/i", $url2)){ ?>
						class="activeclassanchor_list" <?php } else { ?>
						class="list_header_anchor" <?php } ?>
						href="<?php echo $baseurl;?>/index.php/preferences/officialToday">Lists</a>

						<!-- Lits dropdow menu --> <?php 
						if ($url == 'user' || preg_match("/recentSearch/i", $url2))
						{
						    ?>
						<ul class="menu-dropdown lists">
							<li><a
								href="<?php echo $baseurl; ?>/index.php/preferences/officialToday">Official
									Today</a></li>
							<li><a
								href="<?php echo $baseurl; ?>/index.php/preferences/recommended">Recommended</a>
							</li>
							<li><a
								href="<?php echo $baseurl; ?>/index.php/preferences/friends"> <?php if ($user->isMember()) { ?>Friends<?php } else { ?>Followers<?php } ?>
							</a></li>
							<?php if ($user->isMember()) { ?>
							<li><a href="<?php echo $baseurl; ?>/index.php/preferences/followees">Followees</a>
							</li>
							<?php } ?>
							<li><a href="<?php echo $baseurl; ?>/index.php/preferences/world">Trending</a>
							</li>
						</ul> <?php
						}
						?>
					</li>

					<li
					<?php if ($url == 'preferences' && preg_match("/recentSearch/i", $url2)) { ?>
						class="activeclass_list" <?php } else { ?>
						class="list_header_menu" <?php } ?>><a
						<?php if ($url == 'preferences' && preg_match("/recentSearch/i", $url2)){ ?>
						class="activeclassanchor_list" <?php } else { ?>
						class="list_header_anchor" <?php } ?>
						href="<?php echo $baseurl;?>/index.php/preferences/recentSearch">Search</a>

						<!-- Search dropdow menu --> <?php 
						if ($url == 'user' || !preg_match("/recentSearch/i", $url2))
						{
						    ?>
						<ul class="menu-dropdown lists">
							<li><a
								href="<?php echo $baseurl; ?>/index.php/preferences/recentSearchLists">
									Lists<?php if (isset($_SESSION["searchListCount"])) echo ' (' . $_SESSION["searchListCount"] . ')';?>
							</a></li>
							<li><a
								href="<?php echo $baseurl; ?>/index.php/preferences/recentSearchPeople">
									People<?php if (isset($_SESSION["searchPeopleCount"])) echo ' (' . $_SESSION["searchPeopleCount"] . ')';?>
							</a></li>
							<li><a
								href="<?php echo $baseurl; ?>/index.php/preferences/recentSearchCompanies">
									Companies<?php if (isset($_SESSION["searchCompaniesCount"])) echo ' (' . $_SESSION["searchCompaniesCount"] . ')';?>
							</a></li>
						</ul> <?php
						}
						?>
					</li>
				</ul>
			</div>

		</div>
	</div>

	<div class="featured_full">
		<div class="featured_list_section_internal">
			<div class="navigation_part">
				<div class="navi">
					<ul <?php if($url == 'user'){ ?> class="user-navi" <?php } ?>>
						<?php 
						switch ($url) {
						    case "preferences":
						        ?>
						<?php if ($url2 == 'officialToday' || $url2 == 'recommended' || $url2 == 'world' || $url2 == 'friends' || $url2 == 'followees') { ?>
						<li <?php if ($url2 == 'officialToday') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="recomdedselect">
								<a <?php if ($url2 == 'officialToday') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/officialToday">Official
									Today</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recommended') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'recommended') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/recommended">Recommended</a>
							</div>
						</li>

						<li <?php if ($url2 == 'friends') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'friends') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/friends"> <?php if ($user->isMember()) { ?>
									Friends <?php } else { ?> Followers <?php } ?>
								</a>
							</div>
						</li>
						
						<?php if ($user->isMember()) { ?>
						<li <?php if ($url2 == 'followees') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'followees') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/followees">Followees</a>
							</div>
						</li>
						<?php } ?>
						
						<li <?php if ($url2 == 'world') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'world') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/world">Trending</a>
							</div>
						</li>

						<?php } if($url1[1]=='listtitle' || $url1[1]=='listEntry') { ?>
						
						<li <?php if($url1[1]=='listtitle') {?> class="activLi_header"
						<?php } else{?> class="inactiveli_header" <?php }?>>
							<div class="searchlistselect">
								<a <?php if($url1[1]=='listtitle') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/listtitle">Create
									List</a>
							</div>
						</li>
                        
                        <li <?php if ($url2 == 'officialToday') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="recomdedselect">
								<a <?php if ($url2 == 'officialToday') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/officialToday">Official
									Today</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recommended') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'recommended') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/recommended">Recommended</a>
							</div>
						</li>

						<li <?php if ($url2 == 'friends') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'friends') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/friends"> <?php if ($user->isMember()) { ?>
									Friends <?php } else { ?> Followers <?php } ?>
								</a>
							</div>
						</li>
						
						<?php if ($user->isMember()) { ?>
						<li <?php if ($url2 == 'followees') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'followees') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/followees">Followees</a>
							</div>
						</li>
						<?php } ?>
						
						<li <?php if ($url2 == 'world') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'world') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/world">Trending</a>
							</div>
						</li>

						<?php } if($url1[1]=='takelist') { ?>
						<li <?php if($url1[1]=='takelist') { ?> class="activLi_header" <?php } else{ ?> class="inactiveli_header" <?php } ?> >
							<div class="searchlistselect">
								<a <?php if($url1[1]=='takelist') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/takelist?listid=<?php echo $_GET['listid'];?>">Take
									List</a>
							</div>
						</li>
                        
                        <li <?php if ($url2 == 'officialToday') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="recomdedselect">
								<a <?php if ($url2 == 'officialToday') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/officialToday">Official
									Today</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recommended') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'recommended') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/recommended">Recommended</a>
							</div>
						</li>

						<li <?php if ($url2 == 'friends') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'friends') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/friends"> <?php if ($user->isMember()) { ?>
									Friends <?php } else { ?> Followers <?php } ?>
								</a>
							</div>
						</li>
						
						<?php if ($user->isMember()) { ?>
						<li <?php if ($url2 == 'followees') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'followees') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/followees">Followees</a>
							</div>
						</li>
						<?php } ?>
						
						<li <?php if ($url2 == 'world') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'world') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/world">Trending</a>
							</div>
						</li>

						<?php } if($url1[1]=='viewResult') { ?>
						<li <?php if($url1[1]=='viewResult') { ?> class="activLi_header" <?php } else { ?> class="inactiveli_header" <?php } ?> >
							<div class="searchlistselect">
								<a <?php if($url1[1]=='viewResult') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/viewResult?listid=<?php echo $_GET['listid'];?>">List
									results</a>
							</div>
                        </li>    
                        <li <?php if ($url2 == 'officialToday') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'officialToday') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/officialToday">Official
									Today</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recommended') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'recommended') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/recommended">Recommended</a>
							</div>
						</li>

						<li <?php if ($url2 == 'friends') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'friends') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/friends"> <?php if ($user->isMember()) { ?>
									Friends <?php } else { ?> Followers <?php } ?>
								</a>
							</div>
						</li>
						
						<?php if ($user->isMember()) { ?>
						<li <?php if ($url2 == 'followees') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'followees') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/followees">Followees</a>
							</div>
						</li>
						<?php } ?>
						
						<li <?php if ($url2 == 'world') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'world') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/world">Trending</a>
							</div>
						</li>

						<?php } if($url1[1]=='editlist') { ?>

                        <li <?php if($url1[1]=='editlist') { ?> class="activLi_header" <?php } else { ?> class="inactiveli_header" <?php } ?> >
							<div class="searchlistselect">
								<a <?php if($url1[1]=='editlist') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/editlist?listid=<?php echo $_GET['listid'];?>">Edit
									List</a>
							</div>
						</li>
                        
                        <li <?php if ($url2 == 'officialToday') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="recomdedselect">
								<a <?php if ($url2 == 'officialToday') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/officialToday">Official
									Today</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recommended') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'recommended') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/recommended">Recommended</a>
							</div>
						</li>

						<li <?php if ($url2 == 'friends') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'friends') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/friends"> <?php if ($user->isMember()) { ?>
									Friends <?php } else { ?> Followers <?php } ?>
								</a>
							</div>
						</li>
						
						<?php if ($user->isMember()) { ?>
						<li <?php if ($url2 == 'followees') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'followees') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/followees">Followees</a>
							</div>
						</li>
						<?php } ?>
						
						<li <?php if ($url2 == 'world') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'world') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/world">Trending</a>
							</div>
						</li>

						<?php } if($url1[1]=='updateEntry') { ?>

                        <li <?php if($url1[1]=='updateEntry') { ?> class="activLi_header" <?php } else { ?> class="inactiveli_header" <?php } ?> >
							<div class="searchlistselect">
								<a <?php if($url1[1]=='updateEntry') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/updateEntry?listid=<?php echo $_GET['listid'];?>">Update
									Entries</a>
							</div>
						</li>
                        
                        <li <?php if ($url2 == 'officialToday') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="recomdedselect">
								<a <?php if ($url2 == 'officialToday') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/officialToday">Official
									Today</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recommended') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'recommended') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/recommended">Recommended</a>
							</div>
						</li>

						<li <?php if ($url2 == 'friends') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'friends') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/friends"> <?php if ($user->isMember()) { ?>
									Friends <?php } else { ?> Followers <?php } ?>
								</a>
							</div>
						</li>
						
						<?php if ($user->isMember()) { ?>
						<li <?php if ($url2 == 'followees') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'followees') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/followees">Followees</a>
							</div>
						</li>
						<?php } ?>
						
						<li <?php if ($url2 == 'world') { ?> class="activLi_header"
						<?php } else {?> class="inactiveli_header" <?php } ?>>
							<div class="searchlistselect">
								<a <?php if ($url2 == 'world') { ?> class="activehover_subpages"
								<?php } else { ?> class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl;?>/index.php/preferences/world">Trending</a>
							</div>
						</li>

						<?php } else if ($url2 == 'recentSearch' || $url2 == 'recentSearchLists' || $url2 == 'recentSearchPeople' || $url2 == 'recentSearchCompanies') { ?>
						<li
						<?php if ($url2 == 'recentSearch' || $url2 == 'recentSearchLists') { ?>
							class="activLi_header" <?php } else{?> class="inactiveli_header"
							<?php }?>>
							<div class="recomdedselect">
								<a id="recentSearchListsLink"
								<?php if ($url2 == 'recentSearch' || $url2 == 'recentSearchLists') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/recentSearchLists">
									Lists<?php if (isset($_SESSION["searchListCount"])) echo ' (' . $_SESSION["searchListCount"] . ')';?>
								</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recentSearchPeople') { ?>
							class="activLi_header" <?php } else{?> class="inactiveli_header"
							<?php }?>>
							<div class="recomdedselect">
								<a id="recentSearchPeopleLink"
								<?php if ($url2 == 'recentSearchPeople') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/recentSearchPeople">
									People<?php if (isset($_SESSION["searchPeopleCount"])) echo ' (' . $_SESSION["searchPeopleCount"] . ')';?>
								</a>
							</div>
						</li>

						<li <?php if ($url2 == 'recentSearchCompanies') { ?>
							class="activLi_header" <?php } else{?> class="inactiveli_header"
							<?php }?>>
							<div class="recomdedselect">
								<a id="recentSearchCompaniesLink"
								<?php if ($url2 == 'recentSearchCompanies') {?>
									class="activehover_subpages" <?php } else{?>
									class="unactive_subpages" <?php }?>
									href="<?php echo $baseurl;?>/index.php/preferences/recentSearchCompanies">
									Companies<?php if (isset($_SESSION["searchCompaniesCount"])) echo ' (' . $_SESSION["searchCompaniesCount"] . ')';?>
								</a>
							</div>
						</li>
						<?php }
						break;
case "user":
    ?>
                        <?php 
                        if (!$user->isCompany()) {
                        ?>
						<li <?php if ($url2 == 'myDashboard') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="profileselect">
								<a <?php if ($url2 == 'myDashboard') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myDashboard">Dashboard</a>
							</div>
                        </li>
                        <?php }?>

						<li <?php if ($url2 == 'myCreated') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="createdselect">
								<a <?php if ($url2 == 'myCreated') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myCreated">Created</a>
							</div>
						</li>

						<?php if ($user->isMember()) { ?>
						<li <?php if ($url2 == 'myTaken') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="profileselect">
								<a <?php if ($url2 == 'myTaken') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myTaken">Taken</a>
							</div>
						</li>
						<?php } ?>

						<li <?php if ($url2 == 'myFriends') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>
							id="menu_friends">
							<div class="profileselect">
								<a <?php if ($url2 == 'myFriends') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myFriends"> <?php if ($user->isMember()) { ?>
									Friends & Companies <?php } else { ?> Followers & Companies <?php }?>
								</a>
							</div>
						</li>

						<li <?php if ($url2 == 'myNotifications') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="profileselect">
								<a <?php if ($url2 == 'myNotifications') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myNotifications">Notifications</a>
							</div>
						</li>

						<?php if ($user->isMember()) { ?>

						<li <?php if ($url2 == 'myProfile') { ?> class="activLi_header"
						<?php } else { ?> class="inactiveli_header" <?php } ?>>
							<div class="createdselect">
								<?php if ($id == $userid) { ?>
								<a <?php if ($url2 == 'myProfile') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myProfile">My
									Profile</a>
								<?php } else { ?>
								<a <?php if ($url2 == 'myProfile') { ?>
									class="unactive_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myProfile">My
									Profile</a>
								<?php } ?>
							</div>
						</li>

						<?php } else { ?>

						<li <?php if ($url2 == 'myProfileCompany') { ?>
							class="activLi_header" <?php } else { ?>
							class="inactiveli_header" <?php } ?>>
							<div class="createdselect">
								<?php if ($id == $userid) { ?>
								<a <?php if ($url2 == 'myProfileCompany') { ?>
									class="activehover_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myProfileCompany">My
									Profile</a>
								<?php } else { ?>
								<a <?php if($url2 == 'myProfileCompany') { ?>
									class="unactive_subpages" <?php } else { ?>
									class="unactive_subpages" <?php } ?>
									href="<?php echo $baseurl; ?>/index.php/user/myProfileCompany">My
									Profile</a>
								<?php } ?>
							</div>
						</li>
						<?php } ?>
						<?php break;?>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!--------------------------------Header Close------------------------------------------------------>
