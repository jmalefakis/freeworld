<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<meta name="robots" content="noindex" />
    <meta name="sitelock-site-verification" content="9026" />
    
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/favicon64.ico"/>  
	<!-- blueprint CSS framework -->	
        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset_new.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/inner-pages.css" />
		
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/innerpage.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/wayoworld.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker/jquery.datepick.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sliderkit.css" /><!--Add new style-->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sliderkit-core.css" /><!--Add new style-->
		
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css" />
       
        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.3.custom.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mousewheel.min.js"></script>
		
	<!-- custom scrollbars plugin -->
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepicker/jquery.datepick.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/validate.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dragsort.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.hrzAccordion-new.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/underscore.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tag-it.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/select-list.js"></script> 
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.sliderkit.1.9.2.pack.js"></script><!--Add new js-->
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/sliderkit.counter.1.0.pack.js"></script><!--Add new js-->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.confirm.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.placeholder.js"></script>
        
        <style type="text/css">
        textarea:focus, input:focus{ outline: none;}
        </style>

        <script>

		FW = {
			baseurl: '<?php echo Yii::app()->request->baseUrl; ?>'
		};

		
		(function($){
			$(window).load(function(){
				$("#content_1").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
                
                (function($){
			$(window).load(function(){
				$("#content_2").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
                
        (function($){
			$(window).load(function(){
				$("#content_4").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
       })(jQuery); 
       
       $(function(){
           $('input, textarea').placeholder();
       });  
</script>
           

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="body_gestuser">
    <div id="fb-root"></div>
    <script> window.fbAsyncInit = function() {
    FB.init({
      appId      : '533491073461009',
      xfbml      : true,
      version    : 'v2.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));;</script>

<div class="container" id="page">         
            
                <?php include 'authrized_user_header.php';  ?>
            
            <div class="min_hight_outer">
                <?php echo $content; ?>
            

                <div class="footerdiv_auth_uswer">
                        <?php include 'authrized_user_footer.php';  ?>
                </div><!-- footer -->
           </div>
</div><!-- page -->
<?php // Gallery
if (!empty($this->action->Id) && $this->action->Id == 'viewResult') {
?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/gallery.css" />
    <link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.galleriffic.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.opacityrollover.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/func.js"></script>   
<?php        
}
?>
</body>
</html>
