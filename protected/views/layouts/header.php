

<?php 
      $baseurl = Yii::app()->request->baseUrl;   
      $email = Yii::app()->user->getName();       
      $cs=Yii::app()->clientScript;
      $cs->registerCSSFile($baseurl.'/css/datepicker/colorbox.css');
      $cs->registerScriptFile($baseurl.'/js/jquery.form.js');
      $cs->registerScriptFile($baseurl.'/js/jquery.colorbox.js');
?>
 
<script>
            window.fbAsyncInit = function() {
            FB.init({
                    appId      : '493318360686506',
                    status     : false, 
                    cookie     : true,
                    xfbml      : true,
                    oauth      : true
                });               
                               
            };
            
            (function(d){
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
            }(document));

          function Facebook_login () {
            FB.getLoginStatus(function(response) { 
                if (response.status === 'connected') {
                    window.location = '<?php echo Yii::app()->request->baseUrl;?>/index.php/user/login_facebook';

                }
            });
          }                
</script>

<script>
    jQuery(document).ready(function(){
        
        document.getElementById('LoginForm_email').focus();
        document.getElementById('LoginForm_email').value='';
        
        $('.test').click(function(){ 
            
        var email = $("#User_email_forgot").val();
        if(email !="Email")
            {
                document.getElementById('User_email_forgot').style.color="#A9A9A9";
                document.getElementById('User_email_forgot').fontStyle="italic";
                $("#User_email_forgot").val('Email');
            }
        $("#flashmsg").html('');
        $("#flsfhhidden").val("");
        $.colorbox({inline:true,  width:"550px", height:"250px;", opacity:0.3,open:true, href:"#messageform",
                    onClosed: function() {
                         $('#messageform').hide();
                                
                                var show = $("#flsfhhidden").val();
                                if(show == 'Show') {
                                    $.colorbox({inline:true,  width:"550px", height:"220px;", opacity:0.3,open:true, href:"#fleshmessageform",
                                        onClosed: function() {
                                         $('#fleshmessageform').hide();
                                         },                            
                                        onOpen: function() {
                                             $('#fleshmessageform').show();
                                        }
                                        }); 
                                        var myDiv = $("#cboxLoadedContent"); 
                                        $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
                                        var scrollto = myDiv.offset().top - (myDiv.height() /100);
                                        $('html,body').animate({scrollTop:  scrollto}, 700);
                                }
                                
                          
                    },
                    onOpen: function() {
                         $('#messageform').show();
                    }
                    
                   }); 
                   var myDiv = $("#cboxLoadedContent"); 
                    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
                    var scrollto = myDiv.offset().top - (myDiv.height() /100);
                    $('html,body').animate({scrollTop:  scrollto}, 700);
              });
    })
</script>



<div class="header">
   <div class="header_top">
      <!--  <div class="cetnter_div">-->
       <div class="header_top_internal">
        <div class="headeer_logo">
            <?php if($email == 'Guest'){ ?>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/user/home">
            <img src="<?php echo $baseurl?>/images/newimages/header_logo_3.png" />
            </a>
            <?php } else { ?>
             <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/preferences/officialtoday">
            <img  class="header_imagelogo_fal" src="<?php echo $baseurl?>/images/newimages/header_logo_3.png" />
            </a>
            <?php }?>
        </div>
       <!-- <div class="flashmessage">
        </div>-->
        <?php  if($email == 'Guest'){ ?>
      <!--  <div class="header_login">       -->
       <div class="header_top_right" style="width: 481px;">
            <div class="header_top_right_existinguser_login">
                            <div class="existinguser_login">
                                Existing users login here:
                            </div>
                        </div>
            
<?php /*
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        //'action'=>$baseurl.'/index.php/user/home',
        'method'=>'post',        
	'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'name'=>'loginform',  
        'enctype'=> 'multipart/form-data',
        'onsubmit'=>'return login_header();',
        'validateOnSubmit'=>true,
 ),
)); 
    */?>           
            <div class="header_top_right_existinguser_login">
                 <div class="header_top_inputs_one">            
                     <input type="text" value="Email" size="29" id="LoginForm_email" onclick="loadvalue()" onblur="blurvalue(this.value)" onfocus="changeclruser()"   name="LoginForm[email]" maxlength="120"/>
                     <span class="numaicclass" id="spanusername"></span>
	         </div>               
                 <div class="header_top_inputs_one">
                     <input type="text" value="Password" size="29" id="LoginForm_password1"  onclick="loadpass(this.value)" Onblur="blurpass(this.value)" onfocus="focusrpass(this.value)" name="LoginForm[password]" maxlength="120" />
                     <input type="password" value="" size="29" id="LoginForm_password"   onclick="loadpass(this.value)" Onblur="blurpass(this.value)" onfocus="focusrpass(this.value)" name="LoginForm[password]" maxlength="120" />
                     <span class="numaicclass" id="spanuserpassword">Please fill the Password</span>
                </div>
            </div>     
           <div style="float: left; width: 115px;">
                            <input class="login_btn cursorpointer" id="login_btn"  style="font-style: normal; height: 28px;float: left;" type="submit" value="LOGIN" onclick="return login_header('<?php echo $baseurl;?>')" />
                            <input id="lodingIgif" type="image" src="<?php echo $baseurl;?>/images/loading.gif" class="lodinggif_header"/>
           </div>
           
               
              
           <div class="header_top_right_existinguser_check">
                 <div class="check">
                    <input type="checkbox" name="LoginForm[rememberMe]" id="LoginForm_rememberMe"  class=" chbox2"  value="1" />                   
                 </div>
                          <div class="keep_me_loggedin">
                                <span>Keep me logged in</span>
                            </div>
	        </div> 
         
                       <div class="forgot_your_password">
                             <span class="test"> <!--onclick="forgotpassword_popup('<?php //echo $baseurl;?>/index.php/user/forgotpassword');">-->
                                Forgot your password?
                            </span>
                   
                   
                        <div class="forgot_password_main_home" style="display: none;" id="messageform">
                            <input type="hidden" value="personalclick" id="onclick"/>                            
                            <div class="content_forgot"> 
                                <div class="message_home_forgot_flashmgz">
                                    <div id="flashmsgxxxxx" class="flash">

                                    </div>                   
                                </div>
                            <div class="content_right_forgot">
                                  <div class="free_forgotpassword_page">              
                                          <span>Please provide your email address below </span> 
                                 </div>
                                  <div class="content_right_bottom_forgot">

                                      <div class="forgotpas_new_style" >                 

                                              <div class="first_name2_forgot"> 
                                                  <input value="Email"   name="email"  id="User_email_forgot"  onclick="forgotemail()" class="email_fogtpassword"  maxlength="120" onfocus="forgotpassFocus(this.value)"  onblur="forgotpassBlur(this.value)"/>
                                                  <span class="numaicclassfor" id="spanemail_forgot" ></span>
                                              </div>

                                             <div class="register_forgotpassword" style="margin-top: 20px;">
                                                  <input id="forgotpass_btn" class="forgotpassword_submitw cursorpointer" type="submit" name="submit" value="SUBMIT" onclick="return forgot_password('<?php echo $baseurl;?>/index.php/user/forgotpassword')"/>
                                                  <input id="loding_forgot" type="image" src="<?php echo $baseurl;?>/images/loading.gif" class="lodinggif_forgotpass"/>
                                             </div>
                                    </div>

                                </div>

                            </div>
                            </div>

                        </div>
                           
                           <div class="forgot_password_flashmsgdiv" style="display: none;" id="fleshmessageform">
                               <input type="hidden" value="" id="flsfhhidden"/>
                               <div class="message_home_forgot_msg">
                                    <div id="flashmsg" class="flash">

                                    </div>                   
                                </div>
                               <div class="forgotpassword_ok_div">
                                 <input type="button" name="ok" value="Ok" class="forgotpassword_ok" onclick="javascript:parent.$.fn.colorbox.close();"/>
                               </div>
                           </div>    
                </div>
           <div style="float: left; width: 495px; font-size: 14px;">
              <div class="flashmgs" id="lofinfaild">
                  <span></span>    
             </div>
           </div>
            </div>      
<?php //$this->endWidget(); ?>
        </div>
        
        <?php } ?>
        </div>
   
   
</div>
<!--
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/gallery.css" />
<link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
<script>
    $(function(){
        $.fancybox({
           'href' :'#modalHelp',
           afterShow: function() {},
           beforeClose: function() {}
       });
 });   
</script>
<div class="customModal clear" id="modalHelp" style="display:none;">
    <div id="galleryContainer" class="galleryContainer">
        <h3>Welcome to Wayo</h3>
        <div id="gallery" class="content">
            <div class="slideshow-container help" style="height: 130px;">
                <p>Please check out a brief overview of how to use Wayo:</p>
                <p><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf">http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo Yii::app()->request->baseUrl; ?>/Instructions.pdf</a></p>
                <p>The overview is always available on the Help page.</p>
                <p>Have fun!</p>
            </div>
        </div>
    </div>
</div>-->
