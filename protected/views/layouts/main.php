<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<meta name="robots" content="noindex" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/freeworld/favicon64.ico"/>

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/thickbox.css" />
          <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/newcss/style.css" />
          
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/validate.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/thickbox.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/location.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="norton-safeweb-site-verification" content="oiacngith5i9pllm7-teevfxvx5d6r24v5iah1htobkqidjgu7zffluz6819pzljg2w3i6g50xgeef72ja1e5p5c7zfpy27khetuifx35gjxle0h17mkdmzmwt8vr0hq" />
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
        
        <div class="minmoti_check">
            <span id="noti_span"></span>
        </div>
	<div id="mbmenu">
		<?php $this->widget('application.extensions.mbmenu.MbMenu',array(
			'items'=>array(
                                array('label'=>'Twitters Friend', 'url'=>array('twitter/twitterlisting'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Facebook Friend', 'url'=>array('facebook/facebooklisting'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Message', 'url'=>array('message/inbox'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Setting','visible'=>!Yii::app()->user->isGuest,'items'=>array(
                                    array('label'=>'My Profile', 'url'=>array('user/userprofile')),
                                    array('label'=>'Change Password', 'url'=>array('user/setting')),
                                )),

                                //array('label'=>'My Profile ', 'url'=>array('user/userprofile'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Official Today', 'url'=>array('list/mylist'),'tag'=>'new', 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>"Polls", 'url'=>array('poll/mypolls'), 'visible'=>!Yii::app()->user->isGuest),
                                //array('label'=>'Search User', 'url'=>array('user/search'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'My Crews', 'url'=>array('user/mycrew'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>"Notification",'linkOptions'=>array('class'=>'bar'), 'url'=>array('list/notification'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>"My Group", 'url'=>array('user/group'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Login', 'url'=>array('user/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('user/logout'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Register', 'url'=>array('user/register'), 'visible'=>Yii::app()->user->isGuest),
                                array('label'=>'Forgot your password', 'url'=>array('user/forgotpassword'), 'visible'=>Yii::app()->user->isGuest),
                                
			),
		)); ?>
	</div><!-- mainmenu -->

<script type="text/javascript">
    jQuery(document).ready(function(){       
     persecCal() ; 
    function persecCal() { 
    jQuery.ajax({ 
    url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/list/notificationcheck",    
    dataType:"json",
    success:function (data) { 
         if(data>0)
             {
                 document.getElementById("noti_span").innerHTML = data;
             }
        },
      complete: function(data) {
             //setTimeout(persecCal,3000);
      }
    });
}
})
</script>        
        
       
    
	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	)); ?><!-- breadcrumbs -->

	<?php echo $content; ?>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>