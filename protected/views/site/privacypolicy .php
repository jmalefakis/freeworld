<?php if(Yii::app()->user->getID() != '') echo "<div id='wrapper'>" ?>
<div class="about_outerdiv1_auth" <?php if(Yii::app()->user->getID() != '') {echo "style='width: 950px'";} else{echo "style='width: 1000px'";}?> >
    <h2 class="about_header">Wayo Media LLC – Privacy Policy</h2>
    <div <?php if(Yii::app()->user->getID() == '') { ?>  class="beforlogin_aboutus" <?php } else {?> class="about_maindiv"<?php }?> >
        <br /><br />
        <p align="right">Effective Date September 8, 2014<p>
        <br />
        
        <h3 class="contact_col_left_form3">This Privacy Policy applies to our sites.</h3>
        <p>This Policy describes how we treat personal information on the websites where it is located.  It also applies to our mobile sites.</p>
        <br />
        
        <h3 class="contact_col_left_form3">We collect information about you.</h3>
        <p><b class="contact_col_left_form2">Contact information.</b> For example, we might collect your name and street address if you register on our site. We might also collect your phone number or email address.</p>
        <br />
        <p><b class="contact_col_left_form2">Information you submit or post.</b> We collect the information you post in a public space on our site. We also collect information when you contact us.</p>
        <br />
        <p><b class="contact_col_left_form2">Demographic information.</b> We may collect information like your gender and age.  We may also collect your zip code. We might collect this when you contact us or enter a promotion.</p>
        <br />
        <p><b class="contact_col_left_form2">Other information.</b>  If you use our website, we may collect information about the browser you’re using. We might look at what site you came from, or what site you visit when you leave us.</p> 
        <br />
        
        <h3 class="contact_col_left_form3">We collect information in different ways.</h3>
        <p><b class="contact_col_left_form2">We collect information directly from you.</b> For example, if you submit content or other information to the site.  We also collect information if you contact us.</p>
        <br />
        <p><b class="contact_col_left_form2">We collect information from you passively.</b> We use tracking tools like browser cookies and web beacons.</p> 
        <br />
        <p><b class="contact_col_left_form2">We get information about you from third parties.</b> For example, our business partners may give us information about you. Social media platforms may also give us information about you.</p> 
        <br /> 
        
        <h3 class="contact_col_left_form3">We use information as disclosed and described here.</h3>
        <p><b class="contact_col_left_form2">We use information to respond to your requests or questions.</b> For example, we might use your information to respond to your customer feedback.</p>
        <br />
        <p><b class="contact_col_left_form2">We use information to improve our products and services.</b> We may use your information to make our website and products better. We might use your information to customize your experience with us. We may combine information we get from you with information about you we get from third parties.</p>
        <br />
        <p><b class="contact_col_left_form2">We use information for security purposes.</b> We may use information to protect our company, our customers, and our websites.</p>
        <br />
        <p><b class="contact_col_left_form2">We use information for marketing purposes.</b> For example, we might send you information about new features or updates. These might be third party offers or products we think you might find interesting. If you register with us, we may enroll you in our email newsletter. To manage this, read the choices section below.</p>
        <br />
        <p><b class="contact_col_left_form2">We use information to communicate with you about your account or our relationship.</b> We may contact you about your account or feedback. We might also contact you about this Policy or our website Terms.</p>
        <br />
        <p><b class="contact_col_left_form2">We use information as otherwise permitted by law.</b></p>
        <br />
        
        <h3 class="contact_col_left_form3">We may share information with third parties.</h3>
        <p><b class="contact_col_left_form2">We may share information with third parties who perform services on our behalf.</b> For example, we may share information with vendors or companies that run our website or who send emails for us.</p>  
        <br />
        <p><b class="contact_col_left_form2">We will share information if we think we have to in order to comply with the law or to protect ourselves.</b> For example, we will share information to respond to a court order or subpoena. We may share it if a government agency or investigatory body requests. We might share information when we are investigating potential fraud.</p>
        <br />
        <p><b class="contact_col_left_form2">We may share information with our business partners.</b> For example, we may share information with third parties who co-sponsor a promotion. These partners may send you information about events and products by mail or email.</p>
        <br />
        <p><b class="contact_col_left_form2">We may share information with any successor to all or part of our business.</b> For example, if part of our business was sold we may give our customer list as part of that transaction.</p>
        <br />
        <p><b class="contact_col_left_form2">We may share information for other reasons we may describe to you.</b></p>
        <br />  

        <h3 class="contact_col_left_form3">You have certain choices about sharing and marketing practices.</h3>
        <p><b class="contact_col_left_form2">You can opt out of receiving our marketing emails.</b> To stop receiving our promotional emails, follow the instructions in any promotional message you get from us. Even if you opt out of getting marketing messages, we will still send you transactional messages. These include responses to your questions.</p>
        <br />
        <p><b class="contact_col_left_form2">You can control if we share information with third parties for their marketing purposes.</b> To opt out of having us share your information with third parties for their promotional purposes, email us at <a href="mailto:contact@wayoworld.com">contact@wayoworld.com</a>.</p> 
        <br />
        <p><b class="contact_col_left_form2">You can control cookies and tracking tools.</b> Your browser may give you the ability to control cookies or other tracking tools. How you do so depends on the type of tool. Certain browsers can be set to reject browser cookies. To control flash cookies, which we may use on certain websites from time to time, you can go to 
            <a style="word-wrap: break-word;" href="http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager07.html">http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager07.html</a>. Why? Because flash cookies cannot be controlled through your browser settings. If you block cookies on your browser, certain features on our sites may not work. If you block or delete cookies, not all of the tracking activities we have described here will stop. Choices you make are both browser and device-specific.</p>
        <br />
        
        <h3 class="contact_col_left_form3">Your California privacy rights.</h3>
        <p>If you reside in California, you have the right to ask us one time each year if we have shared personal information with third parties for their direct marketing purposes. To make a request, please send us an email to <a href="mailto:contact@wayoworld.com">contact@wayoworld.com</a>, or write to us at the address listed below. Indicate in your letter that you are a California resident making a “Shine the Light” inquiry.</p>
        <br />
        
        <h3 class="contact_col_left_form3">These sites and children.</h3> 
        <p>Our sites and apps are meant for adults. We do not knowingly collect personally identifiable information from children under 13 without permission from a parent or guardian. If you are a parent or legal guardian and think your child under 13 has given us information, you can email us at <a href="mailto:contact@wayoworld.com">contact@wayoworld.com</a>. You can also write to us at the address listed at the end of this policy. Please mark your inquiries “COPPA Information Request.” Parents, you can learn more about how to protect children's privacy on-line at 
            <a style="word-wrap: break-word;" href="http://www.consumer.ftc.gov/articles/0031-protecting-your-childs-privacy-online">http://www.consumer.ftc.gov/articles/0031-protecting-your-childs-privacy-online</a>.</p>
        <br />
        
        <h3 class="contact_col_left_form3">We use standard security measures.</h3>
        <p>The Internet is not 100% secure. We cannot promise that your use of our sites will be completely safe. We encourage you to use caution when using the Internet. This includes not sharing your passwords.</p>  
        <br />
        <p>We keep personal information as long as it is necessary or relevant for the practices described in this Policy. We also keep information as otherwise required by law.</p>
        <br />

        <h3 class="contact_col_left_form3">We may link to other sites or have third party services on our site we don’t control.</h3>
        <p>If you click on a link to a third party site, you will be taken to websites we do not control. This policy does not apply to the privacy practices of that website. Read the privacy policy of other websites carefully. We are not responsible for these third party sites.</p>
        <br />
        <p>Our site may also serve third party content that contains its own cookies or tracking technologies. We do not control the use of those technologies.</p>
        <br />
        
        <h3 class="contact_col_left_form3">Feel free to contact us if you have more questions.</h3>
        <p>If you have any questions about this Policy, please email us at <a href="mailto:contact@wayoworld.com">Contact@wayoworld.com</a>.</p> 
        <br />
        <div style="float:left;">
        <p>You can also write to us or call at:</p>
        </div>    
        <div style="float:left; margin-left:5px;">
            <p>Wayo Media LLC</p>
            <p>P.O. Box 29</p>
            <p>149 East 23rd Street,</p> 
            <p>New York, NY 10010</p>
            <p>Phone: (646)275-7417</p>
        </div>
        <br clear="all" >
        
        <br />
        <p>Please include your email address, name, and telephone number when you contact us so we can make sure to reach you with our response.</p> 
        <br />
        
        <h3 class="contact_col_left_form3">We may update this Policy.</h3>
        <p>From time to time we may change our privacy policies. We will notify you of any material changes to our Policy as required by law. We will also post an updated copy on our website.  Please check our site periodically for updates.</p>
        <br />
        <br />
        <p>© 2014 Wayo Media LLC. All rights reserved.</p>
        <br />
    </div>
</div>
<?php if(Yii::app()->user->getID() != '') echo "</div>" ?>
