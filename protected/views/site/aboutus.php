<?php if(Yii::app()->user->getID() != '') echo "<div id='wrapper'>" ?>
<div class="about_outerdiv1_auth" <?php if(Yii::app()->user->getID() != '') {echo "style='width: 950px'";} else {echo "style='width: 1000px'";}?> >
	<h2 class="about_header">Wayo About Us</h2>
	<div  <?php if(Yii::app()->user->getID() == '') { ?>  class="beforlogin_aboutus" <?php } else {?> class="about_maindiv" style="font-size: 17px;"<?php }?>>
		Wayo's mission is to introduce you to a world where your opinion is amplified, always heard, and more influential than ever before, 
		where you will connect genuinely with your friends over a mutual love for your favorite "things", and where you will organize, share, learn, 
		and discover new passions, not only from friends, but also from strangers and companies.
	</div>
</div>
<?php if(Yii::app()->user->getID() != '') echo "</div>" ?>
