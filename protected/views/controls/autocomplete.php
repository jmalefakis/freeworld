<?php 
//////////////////////////////////////////////////////////////////////////////
//
// Support multiple value selection: Name1, Name2, ...
//
// Params:
//
// $searchUrl - autocomplete URL for search "term"
// URL target may looks like:
// function actionAutocomplete($term = null) { ...
// Should return an array of JSON objects like:
// [{"id": "1", "name": "My Object Name", "value": "My Object HTML view in the dropdown list" }, ...
//
// $targetDivId - Id of the target div with the HTML text input control;
// $minLength - minimal input length for search. By default = 2;
//
// $data - initial JSON data [{id: "1", name: "name"}, ...]
//
//////////////////////////////////////////////////////////////////////////////

$baseurl = Yii::app()->request->baseUrl;

if (!isset($minLength)) $minLength = 2;

?>

<script>
$(function() {
    $("#<?php echo $targetDivId; ?>").tagedAutocomplete({
          source: "<?php echo $searchUrl; ?>",
          minLength: <?php echo $minLength; ?>,
          autoFocus: true
      }, <?php echo $data; ?>);
  });
  </script>
