<?php 
// params
// $uploaderId - unique string id
// $showList - show list already uploaded files
// $files - array with uploaded files data 
//
// array(
//    'id' => 'fileId',
//    'thumbnailURL' => 'url for small image'
//    'name' => 'display name of the file'
// );
//
// $deleteUrl - URL for delete file
// $uploadUrl - URL for upload file
//
// $uploadButtonClientId - external HTML element id for upload 
// $uploadButtonText - upload button prompt text
// $dragAndDropText - prompt text for D&D
//
// $fileDataName - the name for upload field
// $filterName - a name of the file filter
// $filterExtensions - file extension for filtering
//
// $onClientFilesAdded - string with javascript callback name
// $onClientResponseReceived - string with javascript callback name
//
// $responseTargetId - target element for server response
//

if (!isset($showList)) $showList = false;

if (!isset($uploadContainerId)) $uploadContainerId = 'multiFileUploadCnt';
if (!isset($uploadButtonClientId)) $uploadButtonClientId = false;
if (!isset($uploadButtonText)) $uploadButtonText = "[Upload Image]";

if (!isset($dragAndDropText)) $dragAndDropText = "Drop new picture <br/> or";

if (!isset($fileDataName)) $fileDataName = "images[]";

if (!isset($filterName)) $filterName = "Image Files";
if (!isset($filterExtensions)) $filterExtensions = "gif,jpg,jpeg,png";
                    

?>
<div id="<?php echo $uploadContainerId; ?>" class="multifile-upload" >
    <div id="<?php echo $uploaderId; ?>_dnd" class="mfile-drag">
        <?php echo $dragAndDropText; ?>
    </div>
    
    <div id="<?php echo $uploaderId; ?>_filelist" class="mfile-filelist">

    <?php 
    if ($showList)
    {
        foreach ($files as $fileInfo)
        {
    ?>
        <div class="mfile-info" id="<?php echo $fileInfo["id"]; ?>">
            <div class="mfile-thumb">
                <img alt="thumbnail" src="<?php echo $fileInfo["thumbnailURL"]; ?>">
            </div>
            <div class="mfile-name"><?php echo $fileInfo["name"]; ?>"</div>
            <div class="mfile-delete">
                <a onclick='deleteFile_<?php echo $uploaderId; ?>("<?php echo $deleteUrl; ?>", "<?php echo $fileInfo["id"]; ?>");'
                    href="javascript:;">Delete</a>
            </div>
        </div>
    <?php 
        }
    }
    ?>   
       
    </div>       
    
    <?php if (!$uploadButtonClientId)
    {    
    ?>
    
    <div class="mfile-browse-files">
        <a id="<?php echo $uploaderId; ?>_pickfiles" href="javascript:;">
            <?php 
                echo $uploadButtonText;
                $uploadButtonClientId = $uploaderId . "_pickfiles";
            ?>
        </a>
    </div>
    
    <?php 
    }
    ?>
    
    <script type="text/javascript">
        $(document).ready(function () {
            initUploader_<?php echo $uploaderId; ?>();
        });

        function initUploader_<?php echo $uploaderId; ?>() {
            $('#<?php echo $uploadButtonClientId; ?>').unbind();

            // uploader
            var options_<?php echo $uploaderId; ?> = {
                runtimes: 'html5, html4',
                file_data_name: '<?php echo $fileDataName; ?>',
                browse_button: '<?php echo $uploadButtonClientId; ?>',                
                rename: false,                
                max_file_size: '20mb',
                url: '<?php echo $uploadUrl; ?>',
                filters: [{ title: "<?php echo $filterName; ?>", extensions: "<?php echo $filterExtensions; ?>"}]
            };

            // fix link button href for IE
            $("#<?php echo $uploadButtonClientId; ?>").attr("href", "javascript:;");

            if (window.File && window.FileList && window.FileReader) {
                options_<?php echo $uploaderId; ?>.dragdrop = true;
                options_<?php echo $uploaderId; ?>.drop_element = "<?php echo $uploaderId; ?>_dnd";
                <?php if (!empty($responseTargetId)) {?>
                    options_<?php echo $uploaderId; ?>.drop_element2 = "<?=$responseTargetId?>";
                <?php }?>
                options_<?php echo $uploaderId; ?>.base_url = "<?=Yii::app()->request->getBaseUrl(true)?>";        
            }

            var uploader_<?php echo $uploaderId; ?> = new plupload.Uploader(options_<?php echo $uploaderId; ?>);
			
			<?php if (isset($onInit)) { ?>
			//uploader_<?php echo $uploaderId; ?>.bind('Init', <?php echo $onInit; ?>);
			<?php } ?>
 
            uploader_<?php echo $uploaderId; ?>.bind('FilesAdded', function (up, files) {
                var fileList = $('#<?php echo $uploaderId; ?>_filelist');
                var supportedExt = "<?php echo $filterExtensions; ?>";
                for (var i in files) {
                    var ext = files[i].name.split(".");
                    ext = ext[ext.length - 1];
                    if (supportedExt.match("\\*") || supportedExt.match(ext)) {
                        fileList.append('<div id="'
                            + files[i].id + '" class="mfile-info">' 
                            + '<div class="mfile-name">' + files[i].name
                            + '&nbsp;(size:&nbsp;' + plupload.formatSize(files[i].size) + ')&nbsp;<b>&nbsp;</b></div>'
                            + '</div>');
                    }
                }

                <?php
                if (isset($onClientFilesAdded))
                {
                ?>
                
                if (!<?php echo $onClientFilesAdded; ?>(files, '<?php echo $uploaderId; ?>')) {
                    return false;
                }
                
                <?php
                }
                ?>

                setTimeout(function () { uploader_<?php echo $uploaderId; ?>.start(); }, 300);
            });

            uploader_<?php echo $uploaderId; ?>.bind('UploadProgress', function (up, file) {
                $("#" + file.id + " b").html('<span>' + file.percent + "%</span>");
            });

            <?php /*{response: "[{                    
                    name: "file.png",
                    size: 40099,
                    type: "image/x-png",
                    url: "/Files/file.png",
                    thumbnail_url: "/Files/Thumbnail/file.png",
                    delete_type: "GET",
                    delete_url: "/upload.php?__DELETE_FILE=test.png"
                }]",
                status: 200
            }*/?>
            uploader_<?php echo $uploaderId; ?>.bind('FileUploaded', function (up, file, respObj) {
                <?php 
                if ($showList)
                {
                ?>
                
                var fileInfo = eval(respObj.response)[0];
                $("#" + file.id).html('<div class="mfile-thumb"><img src="' + fileInfo.thumbnail_url
                    + '" alt="thumbnail" /></div>'
                    + '<div class="mfile-name">' + fileInfo.name + '</div>'
                    + '<div class="mfile-delete"><a href="javascript:;" onclick="deleteFile_<?php echo $uploaderId; ?>('
                    + "'" + fileInfo.delete_url + "', '" + file.id + "'"
                    + ');">Delete</a></div>'
                );
                
                <?php  
                } else { 
                ?>
                
                $("#" + file.id).hide();
                    
                <?php 
                }
                
                if (isset($responseTargetId))
                {
                ?>
                
                $("#<?php echo $responseTargetId; ?>").html(respObj.response);
                
                <?php 
                }
                ?>
                
                <?php
                if (isset($responseTargetId2))
                {
                ?>
                
                $("#<?php echo $responseTargetId2; ?>").html(respObj.response);
                
                <?php 
                }
                ?>
                                
                <?php if (isset($onClientResponseReceived)) 
                { 
                ?>
                
                <?php echo $onClientResponseReceived ?>(file, respObj, '<?php echo $uploaderId; ?>'<?php if(isset( $responseTargetId)) { echo ' ,"' . $responseTargetId . '"'; } ?>);
                
                <?php 
                } 
                ?>
            });

            uploader_<?php echo $uploaderId; ?>.bind('Error', function (up, error) {
                if (error.message.match("extension")) {
                    alert(error.message + "\nOnly the following files supported: <?php echo $filterExtensions; ?>.");
                } else {
                    alert(error.message);
                }               
            });

            uploader_<?php echo $uploaderId; ?>.init();

            // DnD
            if (window.File && window.FileList && window.FileReader) {
                jQuery.event.props.push('dataTransfer');
                var filedrag_<?php echo $uploaderId; ?> = $("#<?php echo $uploaderId; ?>_dnd");
                filedrag_<?php echo $uploaderId; ?>.bind("dragover", function (e) {  
                    e.stopPropagation();  
                    e.preventDefault();  
                    filedrag_<?php echo $uploaderId; ?>.addClass("hover");
                });
                filedrag_<?php echo $uploaderId; ?>.bind("dragleave", function (e) {
                    e.stopPropagation();  
                    e.preventDefault();  
                    filedrag_<?php echo $uploaderId; ?>.removeClass("hover");
                });
                filedrag_<?php echo $uploaderId; ?>.bind("drop", function(e) {
                    e.stopPropagation();  
                    e.preventDefault();  
                    filedrag_<?php echo $uploaderId; ?>.removeClass("hover");
                });
                filedrag_<?php echo $uploaderId; ?>.css("display", "block");
            }

            // fix z-index
            $("#<?php echo $uploadButtonClientId; ?>").css("z-index", "1000");
        }

        function deleteFile_<?php echo $uploaderId; ?>(deleteURL, id) {
            $.ajax({
                url: deleteURL,
                cache: false
            }).done(function (data) {
                $("#" + id).replaceWith("");
            });
            return false;
        }
    </script>
</div>
