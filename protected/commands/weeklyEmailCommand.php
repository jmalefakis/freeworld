<?php
class weeklyEmailCommand extends CConsoleCommand 
{
    const LIMIT_OFFICIAL_LISTS = 2;
    const LIMIT_FRIENDS_LISTS = 2;
    const LIMIT_FOLLOWERS_LISTS = 2;
    const LIMIT_WORLD_LISTS = 2;
    
    const SUBJECT = 'Weekly Round-Up - wayoworld.com';
    const FROM = 'noreply@wayoworld.com';
    
    const DATE_LIMIT = '-1 week';
    const DATE_NOTICE_LIMIT = '-1 week';
    
    private $listsIds = array(0);
    
    public function run($args) {
        $users = $this->getUsers();
        if (sizeof($users) > 0) {
            foreach ($users as $user) {
                $lists = $this->getOfficialLists($user);
                $this->addListsIds($lists);
                
                //echo "<pre>" . print_r($this->listsIds, true) . "</pre>";
                
                $friendsLists = $this->getFriendsLists($user);
                $this->addListsIds($friendsLists);
                
                //echo "<pre>" . print_r($this->listsIds, true) . "</pre>";
                
                $followersLists = $this->getFollowersLists($user);
                $this->addListsIds($followersLists);
                
                //echo "<pre>" . print_r($this->listsIds, true) . "</pre>";
                
                $worldLists = $this->getWorldLists($user);
                $this->addListsIds($worldLists);
                
                //echo "<pre>" . print_r($this->listsIds, true) . "</pre>";
                
                $lists = array_merge($lists, $friendsLists, $followersLists, $worldLists);
                $name = $this->getFullName($user);
                $notifications = $this->getNotifications($user);
                
                $message = $this->getMessage($this->getLists($lists, $user), $name, $notifications);
                $this->sendEmail($user, $message);
                $this->listsIds = array(0);
            } 
        }
    }
    
    private function getMessage($lists, $name, $notifications) {
        
        $message = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>'. "\r\n";
        $message .= ImageHelper::imageShow(Yii::app()->params['siteurl'].'/images/newimages/', "header_logo_freworld-send2.png", 
                                                      0, 0, array('style' => 'height:180px;width:160px;')) . "
                    <br/><br/>
                    <div style='font-family:Arial;font-size: 20pt; color: #333333;'>
                    </div>
                    <div style='padding: 16px 0 18px;'>
                    Dear $name,<br/><br/>\r\n";
        if (!empty($lists)) {
            $message .= 'Your weekly round-up:<br /><br />'. "\r\n";
            $message .= $lists;
        }
        
        if (sizeof($notifications) > 0) {
            $message .= 'This week\'s <a href="'.Yii::app()->params['siteurl'].'/index.php/user/myNotifications"><b>Notifications</b></a>:'. "\r\n";
            foreach ($notifications as $notice) {
                $message .= '<p>'.$notice.'</p>'. "\r\n";
            }
            $message .= '<br/>';
        }
        
        $message .= 'Best Regards,<br/>
                     Wayo Team
                    <br /><br />
                    <span style="font-size: 8px;"> To stop receiving Notifications by email, please uncheck "Weekly Summary Email" on your <a href="'.Yii::app()->params['siteurl'].'/index.php/user/myProfile">My Account</a> page.</span>

                    </div></body></html>'. "\r\n";
        return utf8_encode ($message);
    }
    
    private function getLists($lists, $user) {
        $out = '';
        $friendsserch = $this->getUserFriends($user);
        if (!empty($lists)) {
            foreach ($lists as $list) {
                $userName = $this->getFullName(User::model()->findByPk($list->user_id));
                $myCriteria = new CDbCriteria();
                $myCriteria->addCondition('user_id = :uid');
                $myCriteria->params = array(':uid' => $user->id);	
                $isTakenByMe = $list->countTaken($myCriteria);

                $listtakencount = 0;
                $friendLabel = "Friend";
                if ($friendsserch) 
                {
                   $c = new CDbCriteria();
                   $c->addInCondition('user_id', $friendsserch);
                   $listtakencount = $list->countTaken($c);
                }

                $totalListTaken = $list->countTaken();
                $s = $listtakencount != 1 ? 's' : '';

                $out .= '
                    <div style="position: relative; margin-right: 16px; width: 561px; height: 425px; float:left;">
                    <div style="background: url(\''.Yii::app()->params['siteurl'].'/images/imgs_bg.png\') no-repeat scroll 0 0 rgba(0, 0, 0, 0); float: left; height: 360px; padding-left: 7px; width: 560px;">
                        <div style="background: none repeat scroll 0 0 #f9f8f5; color: #021a40; float: left; font-family: FjallaOne-Regular; font-size: 22px; height: 40px; margin-top: 10px; padding: 9px 0 0 10px; width: 538px;">
                            <a style="font-weight:bold; box-sizing: border-box; color: #021a40; text-decoration: none; text-transform: capitalize; float: left; padding-right: 45px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; display: block; line-height: 26px; max-width: 100%;" href="'.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$list['id'].'" class="nightife2_litle fL size">'.StringsHelper::capitalize($list['title']).'</a>';
                if ($list['participation'] != 'Everybody') {
                    $out .= '<div style="position: absolute;right: 0px;top: 0px; float:right;" title="Private List"></div>';                      
                } else if (!empty($list['badge'])) { 
                    $out .= '<div style="position: absolute;right: 10px; top: 5px; float:right; background: url(\''.Yii::app()->params['siteurl'].'/images/eagle.png\') no-repeat scroll 0 0 rgba(0, 0, 0, 0); display: block; height: 32px; width: 35px;" title="Wayo Official"></div>';
                }            
                $out .= '</div>
                        <div style="width: 551px; height: 316px; float:left;">
                            <a style="background-color: white; color: #256c89; display: block;" href="'.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$list['id'].'">
                                <img style="z-index:0; cursor: inherit; border: 0 none; box-sizing: border-box;" src="'.Yii::app()->params['siteurl'].'/images/titleimages/list_thumbs/official/big/' . $list['image'] . '?w=548&h=304" alt="'.StringsHelper::capitalize($list['title']).'" title="'.StringsHelper::capitalize($list['title']).'"> 							
                            </a>
                        </div>
                    </div>
                    
                    <div style="border: 0 none; box-sizing: border-box; font: inherit; margin: 0; padding: 0; vertical-align: baseline; cursor: default; font-family: FjallaOne-Regular !important; background: url(\''.Yii::app()->params['siteurl'].'/images/bottom.png\') no-repeat scroll 0 0 white; float: left; height: 80px; width: 560px;">
                        <div style="width: auto; float:left; color:#454545; font-family:FjallaOne-Regular; font-size: 15px; padding-left:10px; padding-top: 10px;">
                            <a style="text-transform: capitalize; font-weight:bold; color: #454545; cursor: pointer; text-decoration: none;" href="javascript:;">  '.StringsHelper::capitalize($userName).' </a>
                        </div>

                        <div style="float: right; margin-top: 6px; width: 160px;">
                            <div style="float: right; font-weight:bold; padding: 0 8px 0 24px; white-space: nowrap;">
                ';            
                if (!empty($list['link_title'])) {
                    $l = !empty($list['link']) ? $list['link'] : 'javascript:;';
                    
                    $out .= '<span style="color: #021a40; font-size: 14px;"> Photo Credit: </span>
                             <span><a style="color: #454545; font-size: 15px; padding-right:20px;" href="'.$l.'" target="_blank">'.StringsHelper::capitalize($list['link_title']).'</a></span>';
                }
                $out .= '       <span style="color: #454545; font-size: 18px;">'.$totalListTaken.'</span>
                                <span style="color: #021a40; font-size: 14px;"> In </span>
                                <span style="color: #454545; font-size: 18px;"> '.$listtakencount.' </span>
                                <span style="color: #021a40; font-size: 14px;"> '.$friendLabel.$s.'</span>
                            </div>
                        </div>
                    </div>
                </div>
                '. "\r\n";
            }
            $out .= '<br clear="all" /><br />';
        }
        return $out;
    }
    
    private function sendEmail($user, $message) {
       echo '<p>Email sent to: '.$user->email.'</p>'; 
       echo '<p>Subject: '.self::SUBJECT.'</p>'; 
       echo '<p>Body:</p><br />'.$message;
       echo '<hr />';
       return MailHelper::sendEmail($user->email, self::FROM, self::SUBJECT, $message);
    }
    
    private function getNotifications($user) {
        $notifications = array();
        $notices = Notification::model()
                   ->findAll(array('condition'=>'user_id = :user_id AND status != "deleted" AND created >= :date',
                                    'params'=> array(':user_id' => $user->id,
                                                      ':date' => date('Y-m-d',strtotime(self::DATE_NOTICE_LIMIT))),
                                    'order' => 'created DESC'));
        if (sizeof($notices) > 0) {
            foreach ($notices as $notice) {
                $text = '';
                $textType = '';
                if ($notice->notification_type == 'friends' 
                        || $notice->notification_type == 'invitation') {
                    $textType = 'friends';
                    
                } else if ($notice->notification_type == 'lists' 
                        || $notice->notification_type == 'my_created_lists'
                        || $notice->notification_type == 'invite_list'
                        || $notice->notification_type == 'my_taken_lists'
                        || $notice->notification_type == 'opinion') {
                     $textType = 'lists';
                } else if ($notice->notification_type == 'system') {
                     $textType = 'system';
                }
                
                if ($textType == 'friends') {
                    $message = $notice->message;
                    if ($message == "") 
                    {
                        $message = "wants to become a friend";
                    } 
                    else if ($message == "You are friends with ") 
                    {
                        $message = "is your friend";
                    }
                    $user2 = User::model()->findByPk($notice->myuser_id);
                    $text = '<p><a style="font-weight:bold;" href=" '.Yii::app()->params['siteurl'].'/index.php/user/myDashboard?userid='.$notice->myuser_id.' "><span>'.trim(StringsHelper::capitalize($user2->firstname.' '.$user2->lastname)).'</span></a> '.$message;
                                
                                if ($notice->notification_type == 'invitation') {
                                    $list = ListForm::model()->findByPk($notice->list_id);
                                    $text .= ' <a style="font-weight:bold;" href=" '.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$notice->list_id.' ">'.trim(StringsHelper::capitalize($list->title)).'</a>'; 
                                }    
                    $text .='.</p>';
                } else if ($textType == 'lists') {
                    $user2 = User::model()->findByPk($notice->myuser_id);   
                    $text = '<p><a style="font-weight:bold;"  href=" '.Yii::app()->params['siteurl'].'/index.php/user/myDashboard?userid='.$notice->myuser_id.' "><span>'.trim(StringsHelper::capitalize($user2->firstname.' '.$user2->lastname)).'</span></a> '.$notice->message;

                                if ($notice->notification_type == "opinion") {
                                    $opinion = Opinion::model()->findByPk($notice->list_id);
                                    $text .= ' "<span>' . StringsHelper::capitalize($opinion->value) . '</span>" list';

							        $list = ListForm::model()->findByPk($opinion->list_id);
                                    $text .= ' <a style="font-weight:bold;" href=" '.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$opinion->list_id.' ">'.trim(StringsHelper::capitalize($list->title)).'</a>';
							    } else {
                                    $list = ListForm::model()->findByPk($notice->list_id);
                                    $text .= ' <a style="font-weight:bold;" href=" '.Yii::app()->params['siteurl'].'/index.php/preferences/listdetail?listid='.$notice->list_id.' ">'.trim(StringsHelper::capitalize($list->title)).'</a>';
                                }
                    $text .= '.</p>';
                } else if ($textType == 'system') {
                    $text = '<p>'.$notice->message.'.</p>';
                }
                
                if (!empty($text)) {
                    $notifications[] = $text;
                }
            }
        }
        return $notifications;
    }
    
    private function getOfficialLists($user) {
        return ListForm::model()
                ->with(array('users' => array('select' => 'id')))
                ->findAll(array(
                    'condition'=>'t.is_official=1 AND '
                    . 't.participation = "Everybody" AND '
                    . 't.page_no = 1 AND '
                    . 't.user_id != :user_id AND '
                    //. 't.created >= :date AND '
                    . 't.id NOT IN ('.implode(',',$this->listsIds).')',
                    'params'=>array(/*':date'=>date('Y-m-d',strtotime(self::DATE_LIMIT)),*/
                                    ':user_id' => $user->id),
                    'order' => 't.position',
                    'limit' => self::LIMIT_OFFICIAL_LISTS));
    }
    
    private function getFriendsLists($userR) {
        $user = User::model()->findByPk($userR->id);
        
        if ($user->isCompany()) {
            $ids = $this->getMyFolloweesIds($userR);
            
        } else {
            $ids = $this->getUserFriends($userR);
        }
        $usersIds = '';
        if (!empty($ids)) {
            $ors = '';
            foreach ($ids as $id) {
               $ors .= ' OR user_id = '.$id; 
            }
            $usersIds = 'AND ('.substr($ors, 3).')';
        }
         
        $recommended48 = new CDbCriteria();
        $recommended48->addCondition("participation = 'Everybody'");
        $recommended48->addNotInCondition("t.id", $this->listsIds);
        $recommended48->addInCondition('t.user_id', $this->getUserFriends($userR), 'OR');
        $recommended48->addNotInCondition("t.id", $this->listsIds);

        $friendIds = $user->isMember() ? $this->getUserFriends($userR) : $this->getMyFolloweesIds($userR);
        if (!$friendIds || count($friendIds) == 0)
        {
            // no friends - no lists
            if ($count) return 0;
            return array();
        }

        // created
        $friendsCondition = new CDbCriteria();
        $friendsCondition->addInCondition('t.user_id', $friendIds);
         
        // or taken by my friends / followers
        $listsTaken = ListTaken::model()->findAll(array(
                        'select' => 'list_id',
                        'condition' => 'user_id IN (' . implode(',', $friendIds) . ')',
                        'distinct' => true
        ));
        $listsTakenIds = array();
        foreach ($listsTaken as $taken)
        {
            if (!in_array($taken->list_id, $this->listsIds)) {
                $listsTakenIds[] = $taken->list_id;
            }
        }
        $friendsCondition->addInCondition('t.id', $listsTakenIds, 'OR');        
        $recommended48->mergeWith($friendsCondition);
         
        // last 48h (2 days) created and taken
        $timeCondition = new CDbCriteria();
        $timeCondition->addCondition("t.created + INTERVAL 2 DAY >= '"
        		. DateHelper::getCurrentSQLDateTime() . "'");        
        $timeCondition->addCondition("taken.created + INTERVAL 2 DAY >= '"
                        . DateHelper::getCurrentSQLDateTime() . "'", "OR");
        $recommended48->mergeWith($timeCondition);

        $recommended48->distinct = true;
        $recommended48->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended48->group = "t.id";
        $recommended48->order = "tcc DESC, t.title";

        $count48 = ListForm::model()->with('taken')->count($recommended48);
         
        // other
        $recommended = new CDbCriteria();
        $recommended->addCondition("participation = 'Everybody'");
        //$recommended->addCondition('t.id NOT IN ('.implode(',',$this->listsIds).')');
        $recommended->addInCondition('t.user_id', $this->getUserFriends($userR), 'OR');

        // created or taken by my friends / followers
        $recommended->mergeWith($friendsCondition);
         
        // TODO: refactor
        $ids48 = $this->listsIds;
        $tmpRes = ListForm::model()->with('taken')->findAll($recommended48);
        foreach ($tmpRes as $list) $ids48[] = $list->id;
        $recommended->addNotInCondition("t.id", $ids48);

        $recommended->distinct = true;
        $recommended->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended->group = "t.id";
        $recommended->order = "tcc DESC, friendsTakens DESC";

        $limit = self::LIMIT_FRIENDS_LISTS;
        $offset = 0;
        $res = array();

        $recommended48->together = true;
        $recommended48->offset = $offset;
        $recommended48->limit = $limit;

        if ($count48 > $offset)
        {
            $res = ListForm::model()->with('taken')->findAll($recommended48);

            if ($count48 < $offset + $limit)
            {
                $limit -= $count48 - $offset;
            }
        }

        if ($count48 < $offset + self::LIMIT_FRIENDS_LISTS)
        {
            $offset -= $count48;
            if ($offset < 0) $offset = 0;

            $recommended->together = true;
            $recommended->offset = $offset;
            $recommended->limit = $limit;

            $res = array_merge($res, ListForm::model()->with('taken')->findAll($recommended));
        }
        
        if (sizeof($res) > 0) {
            foreach ($res as $k=>$t) {
                if ($t->participation != "Everybody" && $t->user_id != $userR->id && !in_array($userR->id,explode(',',$t->participation)))
                {
                    unset($res[$k]);
                    continue;
                }
            }
        }

        return $res;
        
        /*$userFriends = $this->getUserFriends($user);
        return ListForm::model()
                ->with('taken')
                ->findAll(array(
                    'select' => 'count(taken.id) as tcc, t.*',
                    'condition'=>'t.participation = "Everybody" '
                    . 'AND t.user_id IN ('.implode(',',$userFriends).') '
                    . 'AND t.user_id !=  :user_id '
                    //. 'AND taken.created >= :date '
                    . 'AND t.id NOT IN ('.implode(',',$this->listsIds).')',
                    'params'=>array(//':date'=>date('Y-m-d',strtotime(self::DATE_LIMIT)),
                                    ':user_id' => $user->id),
                    'order' => 't.created DESC',
                    'limit' => self::LIMIT_FRIENDS_LISTS,
                    'distinct' => true,
                    'group' => 't.id',
                    'together' => true));*/
    }
    
    private function getFollowersLists($user) {
        return ListForm::model()
                ->findAll(array(
                    'condition'=>'t.participation = "Everybody" '
                    . 'AND (t.user_id IN ('.implode(',', $this->getMyFolloweesIds($user)).') '
                    . 'OR t.user_id IN ('.implode(',', $this->getUserFriends($user)).')) '
                    . 'AND t.user_id !=  :user_id '
                    //. 'AND t.created >= :date '
                    . 'AND t.id NOT IN ('.implode(',',$this->listsIds).')',
                    'params'=>array(/*':date'=>date('Y-m-d',strtotime(self::DATE_LIMIT)),*/
                                    ':user_id' => $user->id),
                    'order' => 't.created DESC',
                    'limit' => self::LIMIT_FOLLOWERS_LISTS,
                    'distinct' => true,
                    'group' => 't.id',
                    'together' => true));
    }
    
    private function getWorldLists($userR) {
        $user = User::model()->findByPk($userR->id);
        if ($user->isCompany()) {
            $ids = $this->getMyFolloweesIds($userR);
            
        } else {
            $ids = $this->getUserFriends($userR);
        }
        $usersIds = '';
        if (!empty($ids)) {
            $ors = '';
            foreach ($ids as $id) {
               $ors .= ' OR user_id = '.$id; 
            }
            $usersIds = 'AND ('.substr($ors, 3).')';
        }

        // last 48h (2 days)
        $recommended48 = new CDbCriteria();
        $recommended48->addNotInCondition("t.id", $this->listsIds);
        $recommended48->addCondition("participation = 'Everybody'");
        
        $recommended48->addInCondition('t.user_id', $this->getUserFriends($userR), 'OR');
        $recommended48->addNotInCondition("t.id", $this->listsIds);
         
        $recommended48->addCondition("taken.created + INTERVAL 2 DAY >= '"
                        . DateHelper::getCurrentSQLDateTime() . "'");

        $recommended48->distinct = true;
        $recommended48->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended48->group = "t.id";
        $recommended48->order = "tcc DESC, t.title";

        $count48 = ListForm::model()->with('taken')->count($recommended48);
         
        // other
        $recommended = new CDbCriteria();
        $recommended->addCondition("participation = 'Everybody'");
        $recommended->addInCondition('t.user_id', $this->getUserFriends($userR), 'OR');
        
        // TODO: refactor
        $ids48 = $this->listsIds;
        $tmpRes = ListForm::model()->with('taken')->findAll($recommended48);
        foreach ($tmpRes as $list) {
            $ids48[] = $list->id;
        }
        $recommended->addNotInCondition("t.id", $ids48);

        $recommended->distinct = true;
        $recommended->select = "count(taken.id) as tcc, (SELECT Count(id) FROM tbl_listtaken WHERE list_id = taken.list_id ".$usersIds.") AS friendsTakens, t.*";
        $recommended->group = "t.id";
        $recommended->order = "tcc DESC, friendsTakens DESC";

        $limit = self::LIMIT_WORLD_LISTS;
        $offset = 0;
        $res = array();

        $recommended48->together = true;
        $recommended48->offset = $offset;
        $recommended48->limit = $limit;

        if ($count48 > $offset)
        {
            $res = ListForm::model()->with('taken')->findAll($recommended48);

            if ($count48 < $offset + $limit)
            {
                $limit -= $count48 - $offset;
            }
        }

        if ($count48 < $offset + self::LIMIT_WORLD_LISTS)
        {
            $offset -= $count48;
            if ($offset < 0) $offset = 0;

            $recommended->together = true;
            $recommended->offset = $offset;
            $recommended->limit = $limit;
             
            $res = array_merge($res, ListForm::model()->with('taken')->findAll($recommended));
        }
        
        if (sizeof($res) > 0) {
            foreach ($res as $k=>$t) {
                if ($t->participation != "Everybody" && $t->user_id != $userR->id && !in_array($userR->id,explode(',',$t->participation)))
                {
                    unset($res[$k]);
                    continue;
                }
            }
        }

        return $res;
        
        
        /*return ListForm::model()
                //->with('taken')
                ->findAll(array(
                    //'select' => 'count(taken.id) as tcc, t.*',
                    'condition'=>'t.participation = "Everybody" AND '
                    . 't.user_id !=  :user_id AND '
                    //. 'taken.created >= :date AND'
                    . ' t.id NOT IN ('.implode(',',$this->listsIds).')',
                    'params'=>array(//':date' => date('Y-m-d',strtotime(self::DATE_LIMIT)),
                                     ':user_id' => $user->id),
                    'order' => 't.created DESC',
                    'limit' => self::LIMIT_WORLD_LISTS,
                    'distinct' => true,
                    'group' => 't.id',
                    'together' => true));*/
    }
    
    private function getUsers() {
        $users = User::model()
                 ->findAllByAttributes(array('member'=>'member', 
                                                'status'=>'1',
                                                'emails_weekly' => 1));
        /*$users = User::model()
                 ->findAllByAttributes(array('email'=>'anskyny10@aol.com'));*/
        
        if (sizeof($users) > 0) {
            foreach ($users as $key=>$user) {
                $now = time();
                $your_date = strtotime($user->created);
                $datediff = $now - $your_date;
                $days = floor($datediff/(60*60*24));
                if ($days%7 != 0) {
                    unset($users[$key]);
                }
            }
        }
        return $users;
    }
    
    private function addListsIds($lists) {
        if (sizeof($lists) > 0) {
            foreach ($lists as $list) {
                $this->listsIds[] = $list->id;
            }
        }
    }
    
    private function getUserFriends($user) {
        $friends = FriendHelper::getFriendsByUserId($user->id,"member");
        if (sizeof($friends) > 0) {
            $out = array();
            foreach ($friends as $friend) {
                $out[] = $friend->friend_id;
            }
            return $out;
        } else {
            return array(0);
        }
    }
    
    private function getMyFolloweesIds($user)
    {
        $userId = $user->id;
        $res = array();

        foreach (Friend::model()->with("friendUser")->findall(array(
                        'select' => 'friend_id',
                        'condition' => "user_id = :x AND friendUser.member = 'company user'",
                        'params' => array(':x' => $userId),
                        'order' => 't.id ASC',
                        'distinct' => true)) as $followees)
        {
            $res[] = $followees->friend_id;
        }
        if (sizeof($res) == 0) {
            $res[] = 0;
        }
        return $res;
    }
    
    private function getFullName($user) {
        if ($user->isCompany()) {
            return StringsHelper::capitalize($user->firstname);
        } else {
            return StringsHelper::capitalize($user->firstname .' '.$user->lastname);
        }
    }
}
?>