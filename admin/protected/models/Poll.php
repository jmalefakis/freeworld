<?php

class Poll extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_poll';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() 
	{
            
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			 array('image, question, user_id, created,modified', 'required'),
                         array('question', 'unique'),
                         array('image', 'file', 'types'=>'jpg, gif, png'),
		);
	}

	/** 
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'Temp' => array(self::HAS_MANY, 'ListTaken', 'list_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'image' => 'Image',
			'question' => 'Question',
                        
		);
	}
        
       public function search($post=null)
	{
           
           if(isset($post))
          {
            $criteria=new CDbCriteria;
            $criteria->condition ='polltype= :interest_id';                            
            $criteria->params = array(
			'interest_id' => $post,                        
	     );
          }
          else
           {          
               $criteria=new CDbCriteria;
               $criteria->compare('question',$this->question);
           }
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
           
	}
}