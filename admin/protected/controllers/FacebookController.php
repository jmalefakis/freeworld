<?php

class FacebookController extends Controller
{
	public $layout='column2';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
         
        public function actionIndex()
	{
            $this->render('index');
            //$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
        
        public function actionfacebook()
	{
            Yii::import('application.vendors.*');
            require_once('facebook/facebook.php'); 
            # Creating the facebook object
            $facebook = new Facebook(array(
                'appId'  => '517388438278032',
                'secret' => 'c5dae6a849889ba93664121e906dc04e',
                'cookie' => true
            ));
            
            $userid = Yii::app()->user->getID(); 
            
            # Let's see if we have an active session
            $session = $facebook->getSession();

            if(!empty($session)) {
                # Active session, let's try getting the user id (getUser()) and user info (api->('/me'))
                try{
                    $uid = $facebook->getUser();
                    $user = $facebook->api('/me/friends');
                    $my_information = $facebook->api('/me');
                   
                } catch (Exception $e){}

                if(!empty($user)){
                    # User info ok? Let's print it (Here we will be adding the login and registering routines)
                    
                    $count = count($user['data']);
                    
                    for($i=0; $i<$count; $i++)
                    { 
                        $usertable = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_facebook')
                                            ->where('friend_facebookid=:value and user_id=:id', array(':value' =>$user['data'][$i]['id'],':id'=>$userid))
                                            ->queryRow();
                        
                        
                    if(empty($usertable))                   
                      {            
                         $model = new FacebookForm;
                         $model->user_id            = $userid;
                         $model->facebook_id        = $my_information['id'];
                         $model->friend_facebookid  = $user['data'][$i]['id']; 
                         $model->friend_name        = $user['data'][$i]['name']; 
                         $facebookimg               = 'https://graph.facebook.com/'.$user['data'][$i]['id'].'/picture'; 
                         $model->friend_facebookimg = $facebookimg;
                         $model->created            = date('Y-m-d');
                         $model->modified           = date('Y-m-d');                         
                         $model->save(false);                         
                      }   
                    } 
                    $this->redirect('facebooklisting');  
                    //echo "<pre>";print_r($my_information['id']);
                } else {
                    # For testing purposes, if there was an error, let's kill the script
                    die("There was an error.");
                }
            } else {
                # There's no active session, let's generate one
                 $login_url = $facebook->getLoginUrl();
                 header("Location: ".$login_url);
            }
            
        }
        
	 public function actionconnectfacebook()
	{
            $this->render('connectfacebook');
        }
        
        public function actionfacebooklisting()
	{             
                $id = Yii::app()->user->getID(); 
                $user=User::model()->findByPk($id);                
                
		$model=new FacebookForm('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FacebookForm']))
			$model->attributes=$_GET['FacebookForm'];               

		$this->render('facebooklisting',array(
			'model'=>$model,
		));
	}
}