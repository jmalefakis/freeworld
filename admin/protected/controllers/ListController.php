<?php

class ListController extends Controller
{
    public $layout='column1';

    public function filters()
    {
        return array(
                        'accessControl',
        );
    }


    public function accessRules() {
        return array(
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('login'),
                                        'users' => array('*'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('Index', 'totallist', 'officialtoday', 'removeofficial', 'View', 'loadModel','Update','listtitle','returnBlank',
                                                        'updation','listcreation','viewlist','seelist','checktitle','emailsubject','emailtaken','officialPage',
                                                        'setPriortyOfficial','listUnderOfficial','searchLists',
                                                        'resetPriortyOfficial', 'saveOfficial',
                                                        'AddListToOfficialToday','RemoveListFromOfficialToday','RemovePageFromOfficialToday',
                                                        'SearchListsByInterests','archiveResults','getArchiveSchedule','saveArchiveSchedule'),
                                        'users' => array('@'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                         
                        array('deny', // deny all users
                                        'users' => array('*'),
                        ),
        );
    }


    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
                        // captcha action renders the CAPTCHA image displayed on the contact page
                        'captcha'=>array(
                                        'class'=>'CCaptchaAction',
                                        'backColor'=>0xFFFFFF,
                        ),
                        // page action renders "static" pages stored under 'protected/views/site/pages'
                        // They can be accessed via: index.php?r=site/page&view=FileName
                        'page'=>array(
                                        'class'=>'CViewAction',
                        ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
        //$this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */

    public function actiontotallist()
    {
        $model =new ListForm('search');
        $model->unsetAttributes();
        $data = ListForm::model()->findAll();

        if(isset($_POST['checkbox']))
        {
            $count = count($_POST['checkbox']);
            for($i=0; $i<$count;$i++)
            {
                $listid =  $_POST['checkbox'][$i];
                $list   = ListForm::model()->find("id = '$listid'");
                $list->is_official = 1;
                $list->save(false);
            }
            $this->redirect('officialtoday');
        }

        if(isset($_POST['listtype']))
        {
            $this->render('totallist',array('model'=>$model,'data'=>$data,'post'=>$_POST['listtype']));
            exit;
        }

        $this->render('totallist',array('model'=>$model,'data'=>$data));
    }

    public function actionremoveofficial($listid=null)
    {
        $list = ListForm::model()->find("id = '$listid'");
        $list->is_official = 0;
        $list->priority    = 9999;
        $list->save(false);
        $this->redirect('officialtoday');
    }


    public function actionView($id)
    {

        $this->render('view',array(
                        'model'=>$this->loadModel($id),
        ));
    }


    public function loadModel($id)
    {
        $model=  ListForm::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    public function actionUpdate($id)
    {

        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['ListForm']))
        {
            $model->name        =$_POST['ListForm']['name'];
            $model->title       =$_POST['ListForm']['title'];
            $model->listtype    =$_POST['ListForm']['listtype'];
            $model->permission    =$_POST['ListForm']['permission'];

            if($model->save())
                $this->redirect(array('totallist'));
        }

        $this->render('update',array(
                        'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionlisttitle()
    {
        $model    = new ListForm;
        $userid   = Yii::app()->user->getID();
        $user     = User::model()->find("id = '$userid'");

        if(isset($_POST['ListForm'])&& $_POST['ListForm']['title']!='')
        {
            $model->title       = $_POST['ListForm']['title'];
            $model->name        = 'WayoWorld';
            $model->user_id     = $userid;
            $model->interest_id = $_POST['ListForm']['interest'];
            $model->listtype    = $_POST['ListForm']['listtype'];
            $model->is_official = 0;
            $model->created     = DateHelper::getCurrentSQLDateTime();
            $model->modified    = DateHelper::getCurrentSQLDateTime();

            if($_FILES['image']['tmp_name'] != '')
            {
                $file= CUploadedFile::getInstanceByName('image');

                if(is_object($file) && get_class($file)== 'CUploadedFile')
                {
                    $model->image  = 'titleimages/'.time().$file->name;
                    $result = $model->save(false);
                }

                if($result)
                {
                    $file->saveAs('../images/'.'/'.$model->image);
                    //$file->saveAs('images/titleimages'.'/'.$model->image);
                }
            }
             
            else
            {
                 
                $model->image = 'profile/'.$user['image'];
                $model->save(false);
            }
             
            $listid   = Yii::app()->db->getLastInsertId();

            $this->redirect(array('listcreation','listid'=>$listid));
        }

        $this->render('listtitle',array('model'=>$model));
    }

    public function actionupdation($listid=null)
    {

        //$userid = Yii::app()->user->getID();
        $data = ListForm::model()->find("id = '$listid'");
         
        if(isset($_POST['submit'])&& $_POST['10']!='')
        {
            $countrow = $_POST['row'];
            $countcol = $_POST['column'];
            //echo "<pre>";print_r($_POST);exit;

            for($i=1;$i<=$countcol;$i++)
            {
                if($_POST['0'.$i]!=''){
                    $column            = ListColumn::model()->find("list_id = '$listid' AND user_id = '$data->user_id' AND serial_no = '$i'");
                    $column->user_id   = $data->user_id;
                    $column->list_id   = $listid;
                    $column->value     = $_POST['0'.$i];
                    $column->serial_no = $i;
                    $column->created   = DateHelper::getCurrentSQLDateTime();
                    $column->modified  = DateHelper::getCurrentSQLDateTime();
                    $column->save(false);
                }
            }
             
            for($i=1;$i<=$countrow;$i++)
            {
                $j=$i-1;
                $row       = ListRow::model()->findAll("list_id = '$listid' AND user_id = '$data->user_id' AND col_id = '$j'");
                $tempcount = count($row);
                for($k=0;$k<$tempcount;$k++){
                    $row[$k]->value  = $_POST[$i.$k];
                    $row[$k]->save(false);
                }
            }
             
             

            $this->redirect(array('seelist','listid'=>$listid));

        }
        $this->render('viewlist',array('data'=>$data,'listid'=>$listid));
    }



    public function actionlistcreation($listid=null)
    {

        $userid = Yii::app()->user->getID();
        $data = ListForm::model()->find("id = '$listid'");
        //echo "<pre>";print_r($data);exit;
        if(isset($_POST['submit'])&& $_POST['10']!='')
        {
            //echo "<pre>";print_r($_POST);exit;
            $count    = $_POST['hid'];
            $countcol = $_POST['hidcol'];
             
             
            for($i=1;$i<=$countcol;$i++)
            {
                if($_POST['0'.$i]!=''){
                    $column            = new ListColumn;
                    $column->user_id   = $userid;
                    $column->list_id   = $_POST['listid'];
                    $column->value     = $_POST['0'.$i];
                    $column->serial_no = $i;
                    $column->created   = DateHelper::getCurrentSQLDateTime();
                    $column->modified  = DateHelper::getCurrentSQLDateTime();
                    $column->save(false);
                }
            }
            for($j=1;$j<=$count;$j++)
            {
                for($i=0;$i<=$countcol;$i++)
                {
                    if($_POST[$j.$i]!='')
                    {
                        $row            = new ListRow;
                        $row->user_id   = $userid;
                        $row->list_id   = $_POST['listid'];;
                        $row->value     = $_POST[$j.$i];
                        $row->serial_no = $i;
                        $row->col_id    = $j-1;
                        $row->created   = DateHelper::getCurrentSQLDateTime();
                        $row->modified  = DateHelper::getCurrentSQLDateTime();
                        $row->save(false);
                    }
                }
            }
            $this->redirect(array('seelist','listid'=>$listid));
             
        }
        $this->render('listcreation',array('data'=>$data,'listid'=>$listid));
    }


    public function actionseelist($listid=null)
    {
        $userid = Yii::app()->user->getID();
        $data = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$data->user_id'");
        //echo "<pre>";print_r($datacolumn);exit;
        $count = count($datacolumn);
        $count = $count+1;


        $this->render('seelist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count));
    }



    public function actionviewlist($listid=null)
    {
        //$userid = Yii::app()->user->getID();
        $data = ListForm::model()->find("id = '$listid'");

        $datacolumn = ListColumn::model()->findAll("list_id = '$listid' AND user_id = '$data->user_id'");

        $count = count($datacolumn);
        $count = $count+1;


        $this->render('viewlist',array('data'=>$data,'datacolumn'=>$datacolumn,'count'=>$count));
    }

    public function actionchecktitle()
    {
        $value = $_POST['title'];
        $title = ListForm::model()->find("title = '$value'");

        if(empty($title))
        {
            $result =1;
            echo json_encode($result);
            exit;
        }
        else
        {
            $result =0;
            echo json_encode($result);
            exit;
        }
    }


    public function actionemailsubject($listid=null)
    {
        $share   = Email::model()->find();
        if(isset($_POST['submit']))
        {

            $subject = $_POST['ListForm']['subject'];
            $message = $_POST['ListForm']['message'];
            //echo "<pre>";print_r($message);exit;
            if($subject == '')
            {
                $share->message = $message;
                $share->save(false);
            }
            else if($message == '' )
            {
                $share->subject = $subject;
                $share->save(false);
            }
            else if($message == '' && $subject == '')
            {
                $share->save(false);
            }
            else if($message != '' && $subject != '')
            {
                $share->message = $message;
                $share->subject = $subject;
                $share->save(false);
            }

            $this->redirect(array('emailsubject'));
            $this->refresh();
        }
        $this->render('emailsubject',array('share'=>$share));
    }


    public function actionemailtaken($listid=null)
    {
        $share   = Email::model()->find("id='2'");

        if(isset($_POST['submit']))
        {

            $subject = $_POST['ListForm']['subject'];
            $message = $_POST['ListForm']['message'];

            if($subject == '')
            {
                $share->message = $message;
                $share->save(false);
            }
            else if($message == '' )
            {
                $share->subject = $subject;
                $share->save(false);
            }
            else if($message == '' && $subject == '')
            {
                $share->save(false);
            }
            else if($message != '' && $subject != '')
            {
                $share->message = $message;
                $share->subject = $subject;
                $share->save(false);
            }

            $this->redirect(array('emailtaken'));
            $this->refresh();
        }
        $this->render('emailtaken',array('share'=>$share));
    }

    public function actionsaveOfficial()
    {
        if (isset($_POST["save"]))
        {
            // update ALL lists!!!
            $ofData = $_SESSION["___ofData"];
            foreach ($ofData as $ld)
            {
                if ($ld["modified"])
                {
                    /*echo $ld["id"];
                     echo "<br/>";
                    echo $ld["is_official"];
                    echo "<br/>";
                    echo $ld["page_no"];
                    echo "<br/>";
                    echo $ld["position"];
                    echo "<br/>";
                    echo "<br/>";*/

                    ListForm::model()->updateByPk($ld["id"], array(
                        'is_official' => $ld["is_official"],
                        'page_no' => $ld["page_no"],
                        'position' => $ld["position"]
                    ));
                }
            }
        }

        $this->redirect(array('officialPage'));
    }

    private function fixOfData($ofData, $page_no, $position)
    {
        foreach ($ofData as $ld)
        {
            if ($ld["page_no"] == $page_no && $ld["position"] == $position)
            {
                $ld["page_no"] = 0;
                $ld["is_official"] = 0;
                $ld["position"] = 0;
                $ld["modified"] = true;
                
                $ofData[$ld["id"]] = $ld;
            }
        }
    }

    public function actionAddListToOfficialToday() {
        if (Yii::app()->user->getModel()->admin != 1) {
            echo -1; exit;
        }

        if (isset($_POST['list_id']) && isset($_POST['page_no']) && isset($_POST['position'])) {
             
            $list_id  = intval($_POST['list_id']);
            $page_no  = intval($_POST['page_no']);
            $position = intval($_POST['position']);

            $ofData = $_SESSION["___ofData"];
            $this->fixOfData($ofData, $page_no, $position);

            //ListForm::model()->updateAll(
            //array('page_no' => 0, 'is_official' => 0, 'position' => 0),
            //'page_no = :x AND position = :y', array(':x' => $page_no, ':y' => $position));

            $ofData[$list_id]["is_official"] = 1;
            $ofData[$list_id]["page_no"] = $page_no;
            $ofData[$list_id]["position"] = $position;
            $ofData[$list_id]["modified"] = true;
            
            $_SESSION["___ofData"] = $ofData;

            //ListForm::model()->updateByPk(
            //$list_id,
            //array('is_official' => 1, 'page_no' => $page_no, 'position' => $position));
        }
    }

    public function actionRemoveListFromOfficialToday() {
        if (Yii::app()->user->getModel()->admin != 1) {
            echo -1; exit;
        }
         
        if (isset($_POST['list_id'])) {
            $list_id = intval($_POST['list_id']);
            
            $ofData = $_SESSION["___ofData"];      
                  
            $ofData[$list_id]["is_official"] = 0;
            $ofData[$list_id]["page_no"] = 0;
            $ofData[$list_id]["position"] = 0;
            $ofData[$list_id]["modified"] = true;
            
            $_SESSION["___ofData"] = $ofData;
            
            //ListForm::model()->updateByPk(
            //$list_id,
            //array('is_official' => 0, 'page_no' => 0, 'position' => 0));
        }
    }


    public function actionRemovePageFromOfficialToday() {
        if (Yii::app()->user->getModel()->admin != 1) {
            echo -1; exit;
        }
        
        $ofData = $_SESSION["___ofData"];
         
        if (isset($_POST['page_no'])) {
            $page_no = intval($_POST['page_no']);

            foreach ($ofData as $ld)
            {
            	if ($ld["page_no"] == $page_no)
            	{
            	    $ld["page_no"] = 0;
            	    $ld["is_official"] = 0;
            	    $ld["position"] = 0;
            	    $ld["modified"] = true;
            	    
            	    $ofData[$ld["id"]] = $ld;
            	}
            	
            	//ListForm::model()->updateAll(
            	//array('page_no' => 0, 'is_official' => 0, 'position' => 0),
            	//'page_no = :x', array(':x' => $page_no));
             
                if (isset($_POST['shift']) && ($_POST['shift'] == true)) 
                {
                    if ($ld["page_no"] > $page_no)
                    {
                        $ld["page_no"] = $ld["page_no"] - 1;
                        $ld["modified"] = true;
                        
                        $ofData[$ld["id"]] = $ld;
                    }
                    
                    //ListForm::model()->updateCounters(array(
                    //    'page_no' => -1),
                    //    'page_no > :x',
                    //    array(':x' => $page_no)
                    //);
                }
            }
        } 
        else 
       {
           foreach ($ofData as $ld)
           {
               if ($ld["is_official"] == 1)
               {
                   $ld["page_no"] = 0;
                   $ld["is_official"] = 0;
                   $ld["position"] = 0;
                   $ld["modified"] = true;
                   
                   $ofData[$ld["id"]] = $ld;
               }
           }
           //ListForm::model()->updateAll(array('page_no' => 0, 'is_official' => 0, 'position' => 0));
        }
        
        $_SESSION["___ofData"] = $ofData;
    }

    public function actionSearchListsByInterests()
    {
        if (Yii::app()->user->getModel()->admin != 1) {
            echo -1; exit;
        }
         
        if (isset($_POST['search_str'])) {
            $search_str = trim($_POST['search_str']);
            //$search_str="ju";
            if (strlen($search_str) > 0) {
                $sqlRegex = RegexHelper::regex($search_str, true);

                $pageno = isset($_POST['pageno']) ? $_POST['pageno'] : 1;
                $limit = 4;
                 
                //Yii::app()->log->routes['db']->enabled = true;
                $interests = Interest::model()
                ->with(array(
                                'lists' => array(
                                                'condition' => 'participation = "Everybody" AND (interests.interest RLIKE :x OR lists.title RLIKE :x)',
                                                'order' => 'interests.interest, lists.title',
                                                'params' => array(':x' => $sqlRegex),
                                                'with' => array('users' => array('select' => 'id')),
                                )))
                                ->findAll(array(
                                                'alias' => 'interests',
                                                'select' => 'interest',
                                                //'limit' => 4,
                                ));
                                //CVarDumper::dump($interests, 10, true);
                                $this->renderPartial('listsByInterests', array(
                                                'interests' => $interests,
                                                'pageno' => $pageno,
                                                'pagemax' => ceil(count($interests) / $limit),
                                                'limit' => $limit
                                                
                                ), false, true);
            }
        }
    }

    public function actionofficialPage()
    {
        if (Yii::app()->user->getModel()->admin != 1) {
            echo -1; exit;
        }

        // store all needed list data to the session before save
        $ofData = array();
        foreach(ListForm::model()->findAll() as $list)
        {
            $ofData[$list->id] = array(
                            "id" => $list->id,
                            "page_no" => $list->page_no,
                            "is_official" => $list->is_official,
                            "position" => $list->position,
                            "modified" => false
            );
        }
        $_SESSION["___ofData"] = $ofData;
         
        $data = ListForm::model()->findAll(array(
            'condition' => 'page_no > 0 AND is_official = 1 AND participation = "Everybody"',
            'order' => 'page_no, position',
        ));
         
        if (count($data) > 0) 
        {
            $pages = intval($data[count($data) - 1]->page_no);
            if ($pages === 0) $pages = 1;
            $official = array_fill(0, $pages, array_fill(0, 10, false));
            foreach($data as $list) 
            {
                if (($list->page_no > 0) && ($list->position > 0)) 
                {
                    $official[$list->page_no - 1][$list->position - 1] = $list;
                }
            }
        } 
        else 
       {
            $official = array_fill(0, 1, array_fill(0, 10, false));
        }
         
        $this->render('officialpage', array('official' => $official));
    }

    public function actionsetPriortyOfficial()
    {
        $listid  = $_POST['elementid'];
        $priorty = $_POST['targetid'];

        $Lists = ListForm::model()->find("id=$listid");
        if($Lists)
        {
            $arry  = array('image'=>$Lists->image);
            echo json_encode($arry);
            exit;
        }
        else{
            echo "1";exit;
        }
    }

    public function actionarchiveResults()
    {
        $this->render('archiveResults',array());
    }

    public function actionsearchLists()
    {
        $string       = $_POST['value'];
        $baseurl      = Yii::app()->params['imageurl'];

        $criteria     = new CDbCriteria();
        $criteria->condition ='title LIKE :title AND is_official != 1';
        $criteria->params    = array(
                        'title' => "%$string%",
        );
        $searches            = ListForm::model()->findAll($criteria);

        $HTML ="";
        if(!empty($searches))
        {
            $i = 1;
            foreach($searches as $list)
            {
                $link = $baseurl."/images/titleimages/list_thumbs/official/small/$list->image";
                $HTML.="<div id='lisbox_$i' class='outerdiv_listdiv'>
                <div class='leftdivimage11' >
                <img class='imagdiv_left' src='$link'/>
                <input type='hidden' value='$list->id' id='listimage_$i'/>
                </div>
                <div class='title_officialPage' >
                <div class='titlediv_page'>
                <a class='title_color' href='$baseurl/index.php/list/view?id=$list->id'>$list->title</a>
                </div>
                </div>
                </div>";
                $i++;
            }
             
             
        }

        else
        {
            $HTML.="<div class='nolistfound'>
                            No Results Found
                            </div>";
        }
        echo $HTML;exit;

    }

    public function actionresetPriortyOfficial()
    {
        $listid  = $_POST['elementid'];


        $Lists = ListForm::model()->find("id=$listid");
        if($Lists){
            $arry  = array('image'=>$Lists->image,'listid'=>$Lists->id,'title'=>$Lists->title);
            echo json_encode($arry);
            exit;
        }
        else{
            echo "1";exit;
        }
    }

    public function actionreturnBlank()
    {
        echo "0";
        exit;
    }

    public function actiongetArchiveSchedule($listid = null)
    {
        if ($listid == null)
        {
            echo "[]";
            exit;
        }

        $list = ListForm::model()->findByPk($listid);

        if (!$list)
        {
            echo "[]";
            exit;
        }

        echo $list->archive_schedule;
        exit;
    }

    public function actionsaveArchiveSchedule($listid = null)
    {
        if ($listid == null || !isset($_POST["schedule"]))
        {
            echo "[]";
            exit;
        }
         
        $list = ListForm::model()->findByPk($listid);
         
        if (!$list)
        {
            echo "[]";
            exit;
        }
         
        $list->archive_schedule = $_POST["schedule"];
        $list->save(false);
         
        echo ArchiveHelper::getNextArchiveDate($listid);
    }
}