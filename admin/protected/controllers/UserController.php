<?php

class UserController extends Controller
{
    public $layout='column1';

    public function filters()
    {
        return array(
                        'accessControl',
        );
    }

    public function accessRules() {
        return array(
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('login'),
                                        'users' => array('*'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('Index', 'View', 'loadModel','Update','listtitle','Login','userlisting','interest','invitationmessage','Logout',
                                                        'systemAlerts','setTimezoneOffset'),
                                        'users' => array('@'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                         
                        array('deny', // deny all users
                                        'users' => array('*'),
                        ),
        );
    }


    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
                        // captcha action renders the CAPTCHA image displayed on the contact page
                        'captcha'=>array(
                                        'class'=>'CCaptchaAction',
                                        'backColor'=>0xFFFFFF,
                        ),
                        // page action renders "static" pages stored under 'protected/views/site/pages'
                        // They can be accessed via: index.php?r=site/page&view=FileName
                        'page'=>array(
                                        'class'=>'CViewAction',
                        ),
        );
    }

    public function actionIndex()
    {
        $this->redirect('userlisting');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionView($id)
    {

        $this->render('view',array(
                        'model'=>$this->loadModel($id),
        ));
    }


    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model=new Customer;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Customer']))
        {
            $model->attributes=$_POST['Customer'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->customer));
        }

        $this->render('create',array(
                        'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {

        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['User']))
        {
            $model->firstname    =$_POST['User']['firstname'];
            $model->lastname     =$_POST['User']['lastname'];
            $model->facebook_id  =$_POST['User']['facebook_id'];
            $model->birthdate    =$_POST['User']['birthdate'];
            $model->email        =$_POST['User']['email'];
            $model->gender       =$_POST['User']['gender'];
            $model->country      =$_POST['User']['country'];
            $model->state        =$_POST['User']['state'];
            $model->member       =$_POST['User']['member'];
            //$model->education    =$_POST['User']['education'];
            $model->interest     =$_POST['User']['interest'];
            $model->admin        =$_POST['User']['admin'];
            $model->created      =$_POST['User']['created'];
            $model->modified     =$_POST['User']['modified'];
            $model->badge        =$_POST['User']['badge'];
            $model->instaview    =$_POST['User']['instaview'];


            if ($model->isCompany()) {
                $model->scenario = 'company';
            } else {
                $model->scenario = 'user';
            }
            
            if($model->save())
                $this->redirect(array('userlisting'));
        }

        $this->render('update',array(
                        'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }


    public function actionregister()
    {
        $this->layout = 'unauthrizedcol1';
        if(isset($_POST['User']) && ($_POST['User']['name'] != '') && ($_POST['email'] != '') && ($_POST['password'] != '') && ($_POST['company'] != ''))
        {
            $user = new User;
            $user->name = $_POST['User']['name'];
            $user->email = $_POST['email'];
            $user->password = md5($_POST['password']);
            $user->company = $_POST['company'];
            $user->created = DateHelper::getCurrentSQLDateTime();
            $user->modified = DateHelper::getCurrentSQLDateTime();
            $user->save(false);
            $this->redirect('login');
        }
        $this->render('register');;
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $this->layout = 'unauthrizedcol1';
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect('userlisting');
             
        }
         
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    public function actionuserlisting()
    {
        $id = Yii::app()->user->getID();
        $user=User::model()->findByPk($id);
        $_SESSION['name'] = $user['firstname'].' '.$user['lastname'];


        $model=new User('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];

        $this->render('userlisting',array(
                        'model'=>$model,
        ));
    }

    public function actioninterest()
    {

        $model    =new Interest('search');
        $model->unsetAttributes();

        //echo "<pre>";print_r($interest);exit;
        if(isset($_POST['User']) && $_POST['User']['interest']!='')
        {
            $model['interest'] =$_POST['User']['interest'];
            $model->created    = DateHelper::getCurrentSQLDateTime();
            $model->modified   = DateHelper::getCurrentSQLDateTime();
            $model->save(false);
        }
        $this->render('interest',array(
                        'model'=>$model,
        ));
    }

    public function actioninvitationmessage()
    {
        $face  = Interest::model()->find();
        if(isset($_POST['textarea1']))
        {
            $face  = Interest::model()->find();
            $face['facebook_message'] = $_POST['textarea1'];
            $face->save(false);
             
        }
        elseif(isset($_POST['textarea2']))
        {
            $twitter   = Interest::model()->find();
            $twitter['twitter_message'] = $_POST['textarea2'];
            $twitter->save(false);
        }
        $this->render('invitationmessage',array('face'=>$face));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actiontwitter()
    {
        $this->layout = 'afterlogin';
        $this->render('twitter');
    }

    public function actionsystemAlerts()
    {
        $myId = Yii::app()->user->getID();
        $user = User::model()->findByPk($myId);

        $baseurl = Yii::app()->request->baseUrl;
        $countSend = 0;

        if (isset($_POST['smessage']))
        {
            $alertMessage = $_POST['smessage'];

            if ($alertMessage)
            {
                $userIds = User::model()->findColumn("id");
                foreach ($userIds as $userId)
                {
                    // send Notification to the each User from the current Admin
                    $notification = new Notification();
                    $notification->myuser_id = $myId;
                    $notification->user_id = $userId;
                    $notification->message = $alertMessage;
                    $notification->read_notification  = 0;
                    $notification->status = 'send';
                    $notification->notification_type = 'system';
                    $notification->created = DateHelper::getCurrentSQLDateTime();
                    $notification->modified = DateHelper::getCurrentSQLDateTime();
                    $notification->save(false);

                    $countSend++;
                }
            }
        }

        $data = $user->systemNotifications();

        $this->render('systemAlerts', array(
                        'alertsLog' => $data,
                        'countSend' => $countSend,
        ));
    }

    public function actionsetTimezoneOffset()
    {
        if (isset($_POST["tzoff"]))
        {
            $_SESSION["_tzoff"] = $_POST["tzoff"];
        }
    }
}