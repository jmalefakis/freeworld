<?php

class InterestController extends Controller
{
    public $layout='column1';


    public function filters()
    {
        return array(
                        'accessControl',
        );
    }


    public function accessRules() {
        return array(
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('login'),
                                        'users' => array('*'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                        array('allow', // allow all users to perform 'index' and 'view' actions
                                        'actions' => array('Index', 'View', 'loadModel','Update','interest'),
                                        'users' => array('@'),
                                        //'expression'=>'$user->isAdmin',
                        ),
                         
                        array('deny', // deny all users
                                        'users' => array('*'),
                        ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
                        // captcha action renders the CAPTCHA image displayed on the contact page
                        'captcha'=>array(
                                        'class'=>'CCaptchaAction',
                                        'backColor'=>0xFFFFFF,
                        ),
                        // page action renders "static" pages stored under 'protected/views/site/pages'
                        // They can be accessed via: index.php?r=site/page&view=FileName
                        'page'=>array(
                                        'class'=>'CViewAction',
                        ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
        //$this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionView($id)
    {

        $this->render('view',array(
                        'model'=>$this->loadModel($id),
        ));
    }


    public function loadModel($id)
    {
        $model=Interest::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {

        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Interest']))
        {
            $model->interest     =$_POST['Interest']['interest'];


            if($model->save())
                $this->redirect(array('interest','id'=>$model->id));
        }

        $this->render('update',array(
                        'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }



    public function actioninterest()
    {

        $model    =new Interest('search');
        $model->unsetAttributes();

        //echo "<pre>";print_r($interest);exit;
        if(isset($_POST['User']) && $_POST['User']['interest']!='')
        {
            $model['interest'] =$_POST['User']['interest'];
            $model->created    = DateHelper::getCurrentSQLDateTime();
            $model->modified   = DateHelper::getCurrentSQLDateTime();
            $model->save(false);
        }
        $this->render('interest',array(
                        'model'=>$model,
        ));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */

}