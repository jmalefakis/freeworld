<?php

class TwitterController extends Controller
{
	public $layout='column2';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
        
        public function actionIndex()
	{
            $this->render('index');
            //$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
        
	public function actiontwitter_oauth()
	{    
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            session_start();
            $userid = Yii::app()->user->getID();            
            
            if(!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){  
                // We've got everything we need  
               
            } else {  
                // Something's missing, go back to twitter
                $baseurl = Yii::app()->request->baseUrl;
                header("Location: http://brihaspatitech.com/freeworld/index.php/twitter/twitter");
            } 
            // TwitterOAuth instance, with two new parameters we got in twitter_login.php
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk', $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            // Let's request the access token
            $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            // Save it in a session var
            $_SESSION['access_token'] = $access_token;
            // Let's get the user's info
            $user_info = $twitteroauth->get('account/verify_credentials');

            $user_followers = $twitteroauth->get('statuses/friends');
            $count_follwer = count($user_followers);
            
                 for($i=0; $i<$count_follwer; $i++)
                 {                      
                     
                     $user = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_twitter')
                                            ->where('friend_twitterid=:value and user_id=:id', array(':value' =>$user_followers[$i]->id,':id'=>$userid))
                                            ->queryRow();
                     if(empty($user))
                      { 
                         $model = new Twitter;
                         $model->user_id           = $userid;
                         $model->twitter_id        = $user_info->id;
                         $model->friend_twitterid  = $user_followers[$i]->id;
                         $model->friend_name       = $user_followers[$i]->screen_name;
                         $model->friend_twitteimg  = $user_followers[$i]->profile_image_url;
                         $model->created           = date('Y-m-d');
                         $model->modified          = date('Y-m-d');
                         $model->save(false); 
                     }
                 }                  
          
            $this->redirect('twitterlisting');            
        }
        
        
        public function actiontwitter()
	{
            Yii::import('application.vendors.*');
            require_once('twitteroauth/twitteroauth.php');            
            session_start();
  
             // The TwitterOAuth instance  
            $twitteroauth = new TwitterOAuth('Bn5wtQzPOHWwvzTZk690vw', 'GQkeUrCq1Hoz9s4PLLSPD18U1gG7TENchsLV5UDvk');  
            // Requesting authentication tokens, the parameter is the URL we will be redirected to  
            $request_token = $twitteroauth->getRequestToken('http://brihaspatitech.com/freeworld/index.php/twitter/twitter_oauth');  

            // Saving them into the session  
            $_SESSION['oauth_token'] = $request_token['oauth_token'];  
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];  

            // If everything goes well..  
            if($twitteroauth->http_code==200){  
                // Let's generate the URL and redirect  
                $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']); 
                header('Location: '. $url); 
            } else { 
                // It's a bad idea to kill the script, but we've got to know when there's an error.  
                die('Something wrong happened.');  
            } 
            $this->render('twitter');
        }
        
         public function actionconnecttwitter()
	{
            $this->render('connecttwitter');
        }
        
        
        public function actiontwitterlisting()
	{        
                $id = Yii::app()->user->getID(); 
                $user=User::model()->findByPk($id);
                $_SESSION['name'] = $user['name'];
                
		$model=new Twitter('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Twitter']))
			$model->attributes=$_GET['Twitter'];

		$this->render('twitterlisting',array(
			'model'=>$model,
		));
	}
}