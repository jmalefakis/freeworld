<?php

class PollController extends Controller
{
	public $layout='column2';

	/**
	 * Declares class-based actions.
	 */
        
        
         public function filters()
            {
                return array(
                    'accessControl',
                );        
            } 
                public function accessRules() {
                return array(                     
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('mypolls', 'createpolls','pollsvalue','seepoll','detailpoll','changepollvote','officialtoday','recommended','friends','followers','browse',
                                           'world','totalpoll','removeofficial',''),
                        'users' => array('@'),
                        'message'=>'Access Denied.',
                        //'expression'=>'$user->isAdmin',
                    ),
               
                    array('deny', // deny all users
                        'users' => array('*'),
                        'message'=>'Access Denied.',
                    ), 
                );
            }
        
        
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
         
        public function actionIndex()
	{
            $this->render('index');
            //$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
        
        public function actionmypolls()
        {
            $this->layout = 'authrized';
            $userid = Yii::app()->user->getID();
            
            $data   = Poll::model()->findAll(array('order'=>'id DESC','limit'=>'7','condition'=>'user_id !=:x', 'params'=>array(':x'=>$userid)));
            $mydata = Poll::model()->findAll(array('order'=>'id DESC','limit'=>'7','condition'=>'user_id =:x', 'params'=>array(':x'=>$userid)));
            $this->render('mypolls',array('data'=>$data,'mydata'=>$mydata,));
        }
        
        public function actioncreatepolls()
        {
            $this->layout = 'authrized';
            $baseurl  = Yii::app()->request->baseUrl;
            $userid   = Yii::app()->user->getID(); 
            $user     = User::model()->find("id = '$userid'");
            $name     = $user['firstname'].' '.$user['lastname'];
            $interest = Interest::model()->findAll(); 
            if(isset($_POST['Poll'])) 
            {
                $model              = new Poll;
                $model->question    = $_POST['Poll']['question'];
                $model->polltype    = $_POST['Poll']['polltype'];
                $model->interest_id = $_POST['ListForm']['interest'];
                $model->user_id     = $userid; 
                $model->created     = DateHelper::getCurrentSQLDateTime();
                $model->modified    = time(); 
                
                if($_FILES['image']['tmp_name'] != '')
                 {
                   $file= CUploadedFile::getInstanceByName('image');

                    if(is_object($file) && get_class($file)== 'CUploadedFile') 
                    {
                        $model->image  = time().$file->name; 
                        $result        = $model->save(false);
                    }
                    
                    if($result)
                    {
                        $file->saveAs('images/pollimages'.'/'.$model->image); 
                        $src = "images/pollimages/" . $model->image; 
                        $image = Yii::app()->image->load("$src");
                        $image->resize(60, 50)->quality(75)->sharpen(20);
                        $image->save(); 
                        
                        
                    }
                 }
                $pollid   = Yii::app()->db->getLastInsertId();
                $poll     = "<a href=$baseurl/index.php/opinion/seepoll?pollid=$pollid>Poll</a>";
                $nameurl  = "<a href=$baseurl/index.php/user/userprofile?userid=$userid>$name</a>";
                $notifi   = Crew::model()->findAll("user_id = '$userid'");
                $count    = count($notifi);
                
                if($count>0)
                    {
                        for($i=0;$i<$count;$i++)
                         {
                            $notification = new Notification;
                            $notification->user_id            = $notifi[$i]['otheruser_id'];
                            $notification->message            = $nameurl.' '."created".' '.$poll;                            
                            $notification->read_notification  = 0;
                            $notification->created            = DateHelper::getCurrentSQLDateTime();      
                            $notification->modified           = time();      
                            $notification->save(false);
                         }
                    }
                
                $this->redirect(array('pollsvalue','pollid'=>$pollid));
            }
            $this->render('createpolls',array('interest'=>$interest));
        }
        
        
        public function actionpollsvalue($pollid = null)
        {
            $this->layout = 'authrized';
            $userid = Yii::app()->user->getID();
            if(isset($_POST['submit'])&& $_POST['01']!='') 
            {
                $count    = $_POST['hid'];
                for($i=1;$i<=$count;$i++)
                 {  
                     if($_POST['0'.$i]!='')
                     {
                         $column            = new PollAnswer;
                         $column->user_id   = $userid;
                         $column->poll_id   = $pollid;
                         $column->value     = $_POST['0'.$i];
                         $column->serial_no = $i;
                         $column->created   = DateHelper::getCurrentSQLDateTime();
                         $column->modified  = DateHelper::getCurrentSQLDateTime();
                         $column->save(false);
                    }                 
                 }
                 
                $this->redirect(array('mypolls')); 
            }
            $this->render('pollsvalue');
        }
        
        public function actionseepoll($pollid = null)
        {
            $this->layout = 'authrized';
            $userid = Yii::app()->user->getID();
            if(isset($_POST['Poll']))
            {
                $pollanswerid     = $_POST['Poll']['pollans'];
                $pollvote         = PollVoting::model()->find("poll_id = $pollid AND user_id = $userid");
                if(empty($pollvote))
                 {
                    $pollvote = new PollVoting ;
                    $pollvote->user_id       = $userid;
                    $pollvote->pollanswer_id = $pollanswerid;
                    $pollvote->poll_id       = $pollid;
                    $pollvote->vote          = 1;
                    $pollvote->created       = DateHelper::getCurrentSQLDateTime();
                    $pollvote->modified      = DateHelper::getCurrentSQLDateTime();
                    $pollvote->save(false);
                    Yii::app()->user->setFlash('onregister', "Your vote has been counted, why not leave a comment now?"); 
                    $this->redirect(array('detailpoll','pollid'=>$pollid)); 
                 }
                 else
                 {
                    Yii::app()->user->setFlash('onregistererror', "Your vote has already been counted for this poll"); 
                    $this->redirect(array('detailpoll','pollid'=>$pollid)); 
                 }
                //echo "<pre>";print_r($pollvote['vote']);exit;
            }
            $data    = Poll::model()->find(array('condition'=>'id =:x', 'params'=>array(':x'=>$pollid)));
            $pollans = PollAnswer::model()->findAll(array('condition'=>'poll_id =:x', 'params'=>array(':x'=>$pollid)));
            $myans   = PollVoting::model()->find(array('condition'=>'poll_id =:x AND user_id = :y', 'params'=>array(':x'=>$pollid,':y'=>$userid)));
            
            $this->render('seepoll',array('data'=>$data,'pollans'=>$pollans,'myans'=>$myans,));
        }
        
        public function actiondetailpoll($pollid = null)
        {
            
            $userid      = Yii::app()->user->getID();
            $userinfo    = User::model()->find("id = '$userid'");
            $data        = Poll::model()->find(array('condition'=>'id =:x', 'params'=>array(':x'=>$pollid)));
            $pollans     = PollAnswer::model()->findAll(array('condition'=>'poll_id =:x', 'params'=>array(':x'=>$pollid)));
            $commentpoll = PollComment::model()->findAll(array('order'=>'id DESC','condition'=>'poll_id =:x', 'params'=>array(':x'=>$pollid)));
            $totalvote   = PollVoting::model()->findAll("poll_id = '$pollid'");
            $countpolans = count($totalvote);
            
            if(isset($_POST['Poll']) && $_POST['Poll']['comment']!='')
            {
                $comment               = $_POST['Poll']['comment'];
                $pollcomment           = new PollComment;
                $pollcomment->user_id  = $userid;
                $pollcomment->poll_id  = $pollid;
                $pollcomment->comment  = $comment;
                $pollcomment->created  = DateHelper::getCurrentSQLDateTime();
                $pollcomment->modified = time();
                $pollcomment->save(false);
                $this->redirect(array('detailpoll','pollid'=>$pollid)); 
            }
            $this->render('detailpoll',array('data'=>$data,'pollans'=>$pollans,'countpolans'=>$countpolans,'userinfo'=>$userinfo,'commentpoll'=>$commentpoll));
        }
        
        public function actionchangepollvote($pollid = null)
        {
            $userid  = Yii::app()->user->getID();
            $myans   = PollVoting::model()->find(array('condition'=>'poll_id =:x AND user_id = :y', 'params'=>array(':x'=>$pollid,':y'=>$userid)));
            $myans->delete();
            $this->redirect(array('seepoll','pollid'=>$pollid)); 
        }
        
            public function actiontotalpoll()
            {
                $model =new Poll('search');
                $model->unsetAttributes(); 
                $data = Poll::model()->findAll();

                if(isset($_POST['checkbox']))
                {
                    $count = count($_POST['checkbox']);
                    for($i=0; $i<$count;$i++)
                    {
                        $pollid =  $_POST['checkbox'][$i];
                        $poll   = Poll::model()->find("id = '$pollid'");    
                        $poll->is_official = 1;
                        $poll->save(false);
                    }
                  $this->redirect('officialtoday');
                }

                if(isset($_POST['listtype']))
                {
                    $this->render('totalpoll',array('model'=>$model,'data'=>$data,'post'=>$_POST['listtype']));
                    exit;
                }


                $this->render('totalpoll',array('model'=>$model,'data'=>$data));
          }
          
          public function actionofficialtoday()
	{
                  
            $data = Poll::model()->findAll("is_official = 1");
            
            if(isset($_POST['priority']) && $_POST['priority']!='' )             
            { 
                //echo "<pre>";print_r($_POST);exit;
                $count = count($_POST['priority']);
                    for($i=0; $i<$count;$i++)
                    {
                        $priority =  $_POST['priority'][$i];
                        //if($priority != 0)
                        {   
                            $pollid =  $_POST['hidden'][$i];
                            $poll   = Poll::model()->find("id = '$pollid'");    
                            $poll->priority = $priority;
                            $poll->save(false);
                        }
                    }
                  
                 $this->refresh();
            }
            
            $this->render('officialtoday',array('data'=>$data));
        }
        
        public function actionremoveofficial($pollid=null)
	{
            $poll = Poll::model()->find("id = '$pollid'"); 
            $poll->is_official = 0;
            $poll->priority    = 9999;
            $poll->save(false);
            $this->redirect('officialtoday');
        }
        
}