<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
                'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
                'name'=>'Freeworld Admin ',

                // preloading 'log' component
                'preload'=>array('log'),

                // autoloading model and component classes
                'import'=>array(
                                'application.models.*',
                                'application.components.*',
                                'application.helpers.*',
                                'application.extensions.CAdvancedArFindBehavior.CAdvancedArFindBehavior',
                ),

                'defaultController'=>'post',

                // application components
                'components'=>array(
                                'user'=>array(
                                                // enable cookie-based authentication
                                                'allowAutoLogin'=>true,
                                                'class' => 'WebUser'

                                ),
                                'db'=>array(
                                'connectionString' => 'mysql:host=192.168.111.175;dbname=freeworld_demo',
                                'emulatePrepare' => true,
                                'username' => 'freeworld',
                                'password' => 'FreeWorld123',
                                'charset' => 'utf8',
                                'tablePrefix' => 'tbl_',
                                ),

                                'errorHandler'=>array(
                                // use 'site/error' action to display errors
                                'errorAction'=>'site/error',
                                ),
                                'urlManager'=>array(
                                'urlFormat'=>'path',
                                'rules'=>array(
                                'post/<id:\d+>/<title:.*?>'=>'post/view',
                                'posts/<tag:.*?>'=>'post/index',
                                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                                ),
                                ),
                                'log'=>array(
                                'class'=>'CLogRouter',
                                'routes'=>array(
                                array(
                                'class'=>'CFileLogRoute',
                                'levels'=>'error, warning',
                                ),
                                // uncomment the following to show log messages on web pages
                                /*
                                array(
                                'class'=>'CWebLogRoute',
                                ),
                                */
                                ),
                                ),
                ),

                'params'=>array(
                // this is used in contact page
                'siteurl'=>'http://192.168.111.175:17555' /*'http://dev.freeworldco.com'*/,

                ),

                // application-level parameters that can be accessed
                // using Yii::app()->params['paramName']
                'params'=>require(dirname(__FILE__).'/params.php'),
);