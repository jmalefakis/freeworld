<?php
return array (
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Freeworld Admin ',

    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array (
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
        'application.extensions.CAdvancedArFindBehavior.CAdvancedArFindBehavior'
    ),

    'defaultController' =>'post',

    // application components
    'components' => array (
        'user' => array (
            'allowAutoLogin' => true,
            'class' => 'WebUser'
        ),

        'db' => array (
            'connectionString' => 'mysql:host=localhost;dbname=freeworld_demo',
            'emulatePrepare' => true,
            'username' => 'freeworld',
            'password' => 'FreeWorld123',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_'
        ),

        'errorHandler' => array (
            // use 'site/error' action to display errors
            'errorAction' => 'site/error'
        ),
 
        'urlManager' => array (
            'urlFormat' => 'path',
            'rules' => array (
                'post/<id:\d+>/<title:.*?>' => 'post/view',
                'posts/<tag:.*?>' => 'post/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>'
            )
        ),

        'log' => array (
            'class' => 'CLogRouter',
            'routes' => array (
                array (
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                )
            )
        )
    ),

    'params' => array (
        'siteurl' => 'http://gofreeworld.com/admin'),
        'params' => require(dirname(__FILE__) . '/params.php'
    )
);
