<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
       
    <!--link rel="stylesheet" type="text/css" href="/css/innerpage.css" /-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style-pages.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbarControlpanel.css"  />
        
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/validate.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.3.custom.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jstz.min.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<script>
    <?php 
    if (!isset($_SESSION["tz"]))
    {
    ?>  
        $(document).ready(function(){
            var tz = jstz.determine();
            var timezone = tz.name();
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/user/setTimezone", {tz: timezone}, function(data) {});
        });
    <?php 
    }
    ?>
</script>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
        
	<div id="menu-top">
		<?php 
		$this->widget('zii.widgets.CMenu',array(
		                'items'=>array(
                            array('label'=>'Users', 'url'=>array('user/userlisting'), 'visible'=>!Yii::app()->user->isGuest),
                            array('label'=>'Official Today', 'url'=>array('list/officialPage'), 'visible'=>!Yii::app()->user->isGuest),
                            array('label'=>'Categories', 'url'=>array('interest/interest'), 'visible'=>!Yii::app()->user->isGuest),
			                array('label'=>'List', 'url'=>array('list/totallist'), 'visible'=>!Yii::app()->user->isGuest),
                            
                            //array('label'=>'Manage email message','visible'=>!Yii::app()->user->isGuest,'items'=>array(
                            //    array('label'=>'List invitation ', 'url'=>array('list/emailsubject')),
                            //    array('label'=>'List taken', 'url'=>array('list/emailtaken')),
                            //)),
                            //array('label'=>'Invitation Message', 'url'=>array('user/invitationmessage'), 'visible'=>!Yii::app()->user->isGuest),
                            
                            array('label'=>'System Alerts', 'url'=>array('user/systemAlerts'), 'visible'=>!Yii::app()->user->isGuest),
                            array('label'=>'Archive Results', 'url'=>array('list/archiveResults'), 'visible'=>!Yii::app()->user->isGuest),

                            //array('label'=>'Polls', 'url'=>array('poll/totalpoll'), 'visible'=>!Yii::app()->user->isGuest),
                            //array('label'=>'Official Today Poll', 'url'=>array('poll/officialtoday'), 'visible'=>!Yii::app()->user->isGuest),
				            
                            array('label'=>'Login', 'url'=>array('user/login'), 'visible'=>Yii::app()->user->isGuest),
				            array('label'=>'Logout', 'url'=>array('site/logout'), 'visible'=>!Yii::app()->user->isGuest),
			),
		));
        ?>
	</div><!-- mainmenu -->

	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	)); ?><!-- breadcrumbs -->

	<?php echo $content; ?>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>