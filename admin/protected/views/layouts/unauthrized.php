<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
        
	<div id="menu-top">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				//array('label'=>'Home', 'url'=>array('post/index')),
                                array('label'=>'Userlisting', 'url'=>array('user/userlisting'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Categories', 'url'=>array('interest/interest'), 'visible'=>!Yii::app()->user->isGuest),
			        array('label'=>'List', 'url'=>array('list/totallist'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Manage email message', 'url'=>array('list/emailsubject'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Invitation Message', 'url'=>array('user/invitationmessage'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Official Today', 'url'=>array('list/officialtoday'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Polls', 'url'=>array('poll/totalpoll'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Official Today Poll', 'url'=>array('poll/officialtoday'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Login', 'url'=>array('user/login'), 'visible'=>Yii::app()->user->isGuest),				
                                //array('label'=>'Register', 'url'=>array('user/register'), 'visible'=>Yii::app()->user->isGuest),
			),
		)); ?>
	</div><!-- mainmenu -->

	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	)); ?><!-- breadcrumbs -->

	<?php echo $content; ?>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>