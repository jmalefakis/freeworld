<?php
$baseurl = Yii::app()->request->baseUrl;
$start = ($pageno - 1) * $limit;
$end = ($start + $limit < count($interests)) ? $start + $limit : count($interests);
if ($pageno == 1) {
	reset($interests);
	$ii = 0;
	$nn = 0;
	foreach($interests as $interest) {
		$nn++;
		foreach($interest->lists as $list) {
			if ($list->is_official != '1')	$ii++;
			if ($ii > 6) break;
		}
		if ($ii > 6) break;
	}
	if ($end < $nn) {
		$pageno = ceil($nn / $limit);
		$end = $pageno * $limit;
		$end = ($end < count($interests)) ? $end : count($interests);
	}
	//CVarDumper::dump($nn, 10, true); 
	//CVarDumper::dump('-------------', 10, true); 
	//CVarDumper::dump($ii, 10, true); 
}
?>
<script>
	OT.page_no = <?php echo $pageno; ?>;
	OT.page_max = <?php echo $pagemax; ?>;
</script>
<ul class="vertical-list-container dnd-source">
<?php
for($i = $start; $i < $end; $i++) 
{
	$interest = $interests[$i];
	if (count($interest->lists) > 0) 
    {
?>
		<li class="section-info borderbluebtm">
			<div class="section-info-name"><?php echo $interest->interest; ?></div>
			<ul class="vertical-list-container section-list">
		<?php
			foreach($interest->lists as $list) 
            {
				$image = (!((strpos($list['image'], 'profile/') === 0) || (strpos($list['image'], 'freeworld/') === 0)))
					? 'titleimages/list_thumbs/official/small/' . $list['image']
					: $list['image'];
				$image = Yii::app()->params['imageurl'] . '/images/' . $image . '?w=127&h=83';
		?>
			<li class="draggable-container" data-list-id="<?php echo $list->id; ?>">
				<!--div class="block-info-cp <?php echo ($list->is_official ? 'draggable-item' : 'draggable-item-disabled'); ?>" data-list-id="<?php echo $list->id; ?>"-->
				<div class="block-info-cp draggable-item <?php if ($list->is_official) { echo 'draggable-item-disabled'; }; ?>" data-list-id="<?php echo $list->id; ?>">
					<img class="block-info-cp-image" src="<?php echo $image; ?>" alt="<?php echo $list->title; ?>" />
					<div class="block-shortinfo-cp">
						<div class="title"><?php echo $list->title; ?></div>
						<!--ul class="plus">
						<li>Lorem ipsum dolor sit amet, consectetur</li>
						<li>Quisque pretium nisl orci, vel bibendum</li>
						</ul-->
					</div>
					<span class="cl-kr"></span>
				</div>
			</li>
		<?php } ?>
			</ul>
		</li><!-- .section-info -->
<?php
	}
}
?>
</ul>
