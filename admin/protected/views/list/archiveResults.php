<?php
$baseurl = Yii::app()->request->baseUrl;
?>

<script type="text/javascript" src="<?php echo $baseurl; ?>/js/jquery.formstyler.min.js"></script>

<script type="text/javascript" src="<?php echo $baseurl; ?>/js/jquery.deserialize.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/js/select-list.js"></script> 

<script>
var OT = { page_no: 1, page_max: 2 };
var pending_request = false;
var isLoadingSchedule = false;

$(function() {	
	$(window).load(function(){
		$('#content_control_panel').mCustomScrollbar({
			//alwaysShowScrollbar: true,
			scrollButtons: {
				enable: true
			},
			callbacks: {
				onTotalScroll: function() {
					if (pending_request || (OT.page_no >= OT.page_max)) return;
					var value = $('#searchStr').val().replace(/^\s+|\s+$/g, '');
					if (value.length > 1) {
						pending_request = true;
						$.ajax({
							url: '<?php echo $baseurl; ?>/index.php/list/SearchListsByInterests',
							type: 'POST',
							dataType: 'html',
							data: {
								search_str: value,
								pageno: OT.page_no + 1
							},
							success: function(result) {
								pending_request = false;
								if (result == '-1' ) return;

								OT.page_no++;
								var resHtml = $(result).find('li.section-info');
								$('#content_control_panel .mCSB_container ul.dnd-source').append(resHtml);
							    
							    resHtml.find('li.draggable-container').focus(onListSelected);
							    resHtml.attacheSelectOnClick();
							}
						});
					}
				}
			},
			advanced: {
				updateOnContentResize: true
			}
		});
	});
    
    $.setSearch = function(searchVal) {
        var value = searchVal.replace(/^\s+|\s+$/g, '');
        if (value.length > 0) {
            aListId = null;
            disableSchedule(true);
            $.ajax({
                url: '<?php echo $baseurl; ?>/index.php/list/SearchListsByInterests',
                type: 'POST',
                dataType: 'html',
                data: {
                    search_str: value
                },
                success: function(result) {
                    if (result == '-1' ) return;

                    $('#content_control_panel .mCSB_container').html(result);
                    if (OT.page_max == 0) return;

                    $('#searchStr').blur();
                    $('#content_control_panel .mCSB_container li.draggable-container').focus(onListSelected);
                    $('#content_control_panel .mCSB_container ul.dnd-source').selectList('li.draggable-container');
                }
            });
        }
    }
		
	$('#searchStr').on('keyup', function(event) {
		if (event.keyCode == 13) {
            $.setSearch($(this).val());
		}
	});
    
    $('#search_but').on('click', function(event) {
        $.setSearch($('#searchStr').val());
	});

    // schedule
    $('#tabs-schedule').tabs({
        activate: function(event, ui) {
            ui.newTab.find('input:radio')
                //.attr('checked', 'checked')
                .prop('checked', true)
                .change();
        }
    }).addClass('ui-tabs-vertical ui-helper-clearfix');

    $('.days, .select-button').styler();

    $("#datepicker3, #datepicker4").datepicker({
        buttonImage: '<?php echo $baseurl; ?>/images/calendar_icon.png',
        minDate: -0,
        buttonText: 'Select Date Range', 
        showOn: 'button',
        dateFormat: 'mm/dd/yy', 
        buttonImageOnly: true
    });

    // disable by default
    disableSchedule(true);

    $('#nextArchiveDate').html('UNKNOWN');

    $("#formSchedule input").change(function() {
        saveSchedule();  
    });

    $("#formSchedule select").change(function() {
        saveSchedule();  
    });

}); // ready

function disableSchedule(disable) {
    $('#formSchedule input').prop('disabled', disable);
    $('#formSchedule select').prop('disabled', disable);
}

var aListId = null;
function onListSelected(event) {
    var li = $(this);
    aListId = li.attr('data-list-id');
    li.attr('id', 'list_' + aListId);
    $('#content_control_panel').mCustomScrollbar('scrollTo', '#list_' + aListId);

    applySchedule(null);
    disableSchedule(true);
    $('#nextArchiveDate').html('UNKNOWN');
    $('#lodingImg').show();

    $.ajax({
		url: '<?php echo $baseurl; ?>/index.php/list/getArchiveSchedule?listid=' + aListId,
		type: 'POST',
		dataType: 'json',
		success: function(resp) {
		    $('#lodingImg').hide();
		    disableSchedule(false);
		    
			if (!resp || resp.length == 0) return;
			
			applySchedule(resp);
		}
	});
}

function applySchedule(schedule) {
    isLoadingSchedule = true;
    if (schedule != null) {    
        $('#formSchedule').deserialize(schedule);

        // tabs fix
        var tabArray = ['daily', 'weekly', 'monthly', 'yearly'];
        $('#tabs-schedule').tabs('option', 'active', tabArray.indexOf(schedule[0].value));
        
        // styler fix
        $('#formSchedule input:radio:checked').change();
        $('#formSchedule select').change();
    } else {
        $('#formSchedule input:text').val('');
        $('#tabs-schedule').tabs('option', 'active', 0);
        $('#nextArchiveDate').html('UNKNOWN');
    }
    isLoadingSchedule = false;

    if (schedule != null) {
        saveSchedule();
    }
}

function saveSchedule() {
    if (aListId == null || isLoadingSchedule) return;
    
    $('#lodingImg').show();
    $.ajax({
		url: '<?php echo $baseurl; ?>/index.php/list/saveArchiveSchedule?listid=' + aListId,
		type: 'POST',
		//dataType: 'json',
		data: { schedule: JSON.stringify($('#formSchedule').serializeArray()) },
		success: function(resp) {
		    $('#lodingImg').hide();
			if (!resp || resp.length == 0) {
			    $('#nextArchiveDate').html('UNKNOWN');
		    } else {
		        $('#nextArchiveDate').html(resp);
			}
		},
		error: function(err) {
		    disableSchedule(true);
		}
	});
}

</script>

<div class="info-search-cp">
	<p>Find the list you would like to archive results for</p>
	<div class="control-search">
		<input class="search-control-panel" type="text" id="searchStr"
			placeholder="Search" />
        <div id="search_but">&nbsp;</div>
	</div>
</div>

<div class="cp-content">
	<div id="content_control_panel" class="cp-col-left-other"></div>
	<!-- .cp-col-left -->

	<div class="cp-col-right-other">
		<div class="titleSchedule">
		    Edit Schedule <img style="display: none;" id="lodingImg" class="loding_image_li" src="/images/ajax-loader.gif">
	    </div>
		<div class="select-sp">
			<span>Select Interval &nbsp;</span>
		</div>
		<form method="get" action="#" class="formSchedule" id="formSchedule">
			<div id="tabs-schedule">

				<ul>
					<li><a href="#daily"> 
					    <input class="inp" id="Daily"
					        name="Schedule" type="radio" value="daily" /> 
					    <label for="Daily"><span>Daily</span></label>
					</a>
					</li>
					<li><a href="#weekly"> 
					    <input class="inp" id="Weekly"
							name="Schedule" type="radio" value="weekly" /> 
						<label for="Weekly"><span>Weekly</span></label>
					</a>
					</li>
					<li><a href="#monthly">
					    <input class="inp" id="Monthly"
							name="Schedule" type="radio" value="monthly" /> 
						<label for="Monthly"><span>Monthly</span></label>
					</a>
					</li>
					<li><a href="#yearly">
					    <input class="inp" id="Yearly"
							name="Schedule" type="radio" value="yearly" />
						<label for="Yearly"><span>Yearly</span></label>
					</a>
					</li>

				</ul>
				<div id="daily">
					<div class="dailyContent">
						Every <input id="dailyCount" type="text" name="days" maxlength="2" /> day(s)
					</div>
				</div>
				<div id="weekly">
					<div class="weekContent">
						Every <input id="weeklyCount" type="text" name="week" class="input-month"
							maxlength="2" /> Week(s) on
					</div>
					<ul class="weekdays">
						<li><input class="days" id="Sunday" type="radio"
							name="weekdays" type="radio" value="Sunday"> <label for="Sunday">Sunday</label><br>
							<input class="days" id="Thursday" type="radio" name="weekdays"
							type="radio" value="Thursday"> <label for="Thursday">Thursday</label>
						</li>
						<li><input class="days" id="Monday" type="radio" name="weekdays"
							type="radio" value="Monday"> <label for="Monday">Monday</label><br>
							<input class="days" id="Friday" type="radio" name="weekdays"
							type="radio" value="Friday"> <label for="Friday">Friday</label>
						</li>
						<li><input class="days" id="Tuesday" type="radio" name="weekdays"
							type="radio" value="Tuesday"> <label for="Tuesday">Tuesday</label><br>
							<input class="days" id="Saturday" type="radio" name="weekdays"
							type="radio" value="Saturday"> <label for="Saturday">Saturday</label>
						</li>
						<li><input class="days" id="Wednesday" type="radio"
							name="weekdays" type="radio" value="Wednesday"> <label
							for="Wednesday">Wednesday</label>
						</li>

					</ul>
				</div>
				<div id="monthly">
					<div class="monthlyContent">
						<input class="days" type="radio"
							name="monthdays" value="monthdays1"> 
					    <select class="select-button" name="monthlyDay">
							<option selected="selected">1st</option>
							<option>2st</option>
							<option>3st</option>
							<option>4st</option>
							<option>5st</option>
							<option>6st</option>
							<option>7st</option>
							<option>8st</option>
							<option>9st</option>
							
							<option>10st</option>
							<option>11st</option>
							<option>12st</option>
							<option>13st</option>
							<option>14st</option>
							<option>15st</option>
							<option>16st</option>
							<option>17st</option>
							<option>18st</option>
							<option>19st</option>
							
							<option>20st</option>
							<option>21st</option>
							<option>22st</option>
							<option>23st</option>
							<option>24st</option>
							<option>25st</option>
							<option>26st</option>
							<option>27st</option>
							<option>28st</option>
							<option>29st</option>
							
							<option>30st</option>
							<option>31st</option>
						</select> <span>of every <input type="text" name="months"
							maxlength="2" class="input-month" /> month(s)
						</span>
					</div>
					<div class="monthlyContent">
						<input id="monthdays2" class="days" type="radio" name="monthdays"
							value="monthdays2"> <span>The</span> 
						<select style="width: 70px;" class="select-button" name="monthlyNum">
							<option selected="selected">first</option>
							<option>second</option>
							<option>third</option>
							<option>fourth</option>
						</select>
						<select style="width: 100px;" class="select-button" name="monthlyWeekDay">
							<option selected="selected">Sunday</option>
							<option>Monday</option>
							<option>Tuesday</option>
							<option>Wednesday</option>
							<option>Thursday</option>
							<option>Friday</option>
							<option>Saturday</option>
						</select>
						<span>of every <input type="text" name="monthlyMonth"
							maxlength="2" class="input-month" /> month(s)
						</span>
					</div>

				</div>
				<div id="yearly">
					<div class="yearlyContent">
						<span>Every</span> 
						<select style="width: 100px;"
							class="select-button" name="yearlyMonth">
							<option selected="selected">January</option>
							<option>February</option>
							<option>March</option>
							<option>April</option>
							<option>May</option>
							<option>June</option>
							<option>July</option>
							<option>August</option>
							<option>September</option>
							<option>October</option>
							<option>November</option>
							<option>December</option>
						</select> 
						
						<select class="select-button" style="width: 100px;" name="yearlyDay">
							<option selected="selected">1st</option>
							<option>2st</option>
							<option>3st</option>
							<option>4st</option>
							<option>5st</option>
							<option>6st</option>
							<option>7st</option>
							<option>8st</option>
							<option>9st</option>
							
							<option>10st</option>
							<option>11st</option>
							<option>12st</option>
							<option>13st</option>
							<option>14st</option>
							<option>15st</option>
							<option>16st</option>
							<option>17st</option>
							<option>18st</option>
							<option>19st</option>
							
							<option>20st</option>
							<option>21st</option>
							<option>22st</option>
							<option>23st</option>
							<option>24st</option>
							<option>25st</option>
							<option>26st</option>
							<option>27st</option>
							<option>28st</option>
							<option>29st</option>
							
							<option>30st</option>
							<option>31st</option>
						</select>
					</div>

				</div>

			</div>

			<div class="select-sp">
				<span>Select Date Range &nbsp;</span>
			</div>

			<table class="formDateRange">
				<tr>
					<td class="frst">Start on or after</td>
					<td class="scd"><input type="text" id="datepicker3" name="startDate"
						class="input-calendar" /></td>
				</tr>
				<tr>
					<td class="frst">
					    <input class="days" type="radio" name="changeDate"
						    value="noEndDate" id="noEndDate"> 
						<label for="noEndDate">No end date</label></td>
					<td class="scd"></td>
				</tr>
				<tr>
					<td class="frst">
					    <input class="days" type="radio" name="changeDate"
						    value="endAfter" id="endAfter"> 
						<label for="endAfter">End after</label>
					</td>
					<td class="scd"><input type="text" class="input-calendar" name="occurrences" />occurrences</td>
				</tr>
				<tr>
					<td class="frst">
					    <input class="days" type="radio" name="changeDate"
						    value="stopAfter" id="stopAfter"> <label for="stopAfter">Stop
							after </label></td>
					<td class="scd">
					    <input type="text" id="datepicker4" name="stopDate"
						    class="input-calendar" /></td>
				</tr>
			</table>
		</form>

		<div class="titleSchedule">
		    <div style="float: left;">Next Archive Date&nbsp;&nbsp;&nbsp;</div>
		    <div id="nextArchiveDate" style="float: left;">UNKNOWN</div>
	    </div>

	</div>

</div>
<!-- .cp-content -->
