<div class="mainouterdiv">
    <div class="outerdiv1">
 <?php
        $baseurl = Yii::app()->request->baseUrl;        
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$baseurl.'/index.php/list/listtitle',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate', 
        'onsubmit'=>'return listtitle();',
        'validateOnSubmit'=>true,
     ),
      )); 
?>
        
        <div class="listmaindiv">
            <div class="listnew_title_div1">
                <div class="list_lable">
                    <span> Title*</span>                        
                </div>
                <div class="row_textfild">
                    <input id="title" type="text" name="ListForm[title]" value="" onblur="checkdatabase('<?php echo $baseurl;?>')"/>
                </div>
                <div id="loding" style="float: left; width: 20px; height: 20px; margin-top: 3px; margin-left: -41px;display: none;">
                    <img src="<?php echo $baseurl;?>/images/ajax-loader.gif"/>
                </div>
                <div class="validtitle" id="valid">
                    <span>This is valid title</span>
                </div>
                <div class="invalidtitle" id="invalid">
                    <span>This is not valid title</span>
                </div>
                <div class="requretitle" id="titlereq">
                    <span>This field is required </span>
                </div>
            </div>
            <!--<div class="listnew_title_div1">
                <div class="name_lable">
                    <span> Name*</span>                        
                </div>
                <div class="row_textfild">
                    <input id="ListForm_name" type="text" name="ListForm[name]" value="" />
                </div>
                <div class="requretitle" id="namereq">
                    <span>This field is required </span>
                </div>
            </div>-->
            
            <?php $interest = Interest::model()->findAll();?>
            
           <div class="listnew_title_div1">
                 <div class="forgotpasdiv1">
                     <span>
                         Chose category *
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="interest" name="ListForm[interest]"  style="width: 141px; height: 24px;">
                         <option value="select" selected="selected" >Select</option>
                         <?php foreach ($interest as $inter){?>
                          <option value="<?php echo $inter->id?>" ><?php echo $inter->interest?></option>
                          <?php }?>
                     </select>
                 </div> 
               <div class="requretitle" id="interestreq">
                    <span>This field is required </span>
                </div>
             </div>
            
            <div class="listnew_title_div1">
                 <div class="forgotpasdiv1" style="width: 120px;">
                     <span>
                         List Type *
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="listtype" name="ListForm[listtype]"  style="width: 141px; height: 24px;">
                         <option value="select" selected="selected" >Select</option>                         
                          <option value="public" >Public</option>
                          <option value="private " >Private </option>
                          <option value="official" >Official</option>
                     </select>
                 </div> 
               <div class="requretitle" id="listtypereq">
                    <span>This field is required </span>
                </div>
             </div>
            
            
            <div class="listnew_title_div1">
                <div class="image_lable">
                    <span> Image*</span>                        
                </div>
                <div class="row_textfild">
                    <input id="image" type="file" name="image" value="" />
                </div> 
                <div class="requretitle" id="imgreq">
                    <span>This field is required </span>
                </div>
            </div>            
           <div class="submitdiv">
            <input type="submit" name="submit" value="Next"/>
           </div>           
              
        </div>
         
<?php $this->endWidget(); ?> 
    </div>    
</div>