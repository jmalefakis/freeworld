<?php
$baseurl = Yii::app()->request->baseUrl;
?>

<script>
var OT = {
	page_no: 1,
	page_max: 2,
}
var pending_request = false;

$(function() {
	
	$(window).load(function(){
		$('#content_control_panel').mCustomScrollbar({
			//alwaysShowScrollbar: true,
			scrollButtons: {
				enable: true
			},
			callbacks: {
				onTotalScroll: function() {
					if (pending_request || (OT.page_no >= OT.page_max)) return;
					var value = $('#searchStr').val().replace(/^\s+|\s+$/g, '');
					if (value.length > 1) {
						pending_request = true;
						$.ajax({
							url: '<?php echo $baseurl; ?>/index.php/list/SearchListsByInterests',
							type: 'POST',
							dataType: 'html',
							data: {
								search_str: value,
								pageno: OT.page_no + 1
							},
							success: function(result) {
								pending_request = false;
								if (result == '-1' ) return;
								$('#content_control_panel .mCSB_container').append(result);
								$('.draggable-item').draggable({
									appendTo: 'body',
									helper: 'clone',
									revert: 'invalid',
									cursor: 'move'
								});
							}
						});
					}
				}
			},
			advanced: {
				updateOnContentResize: true
			}
		});
	});
	
	
	function restoreToSource($objects) {
		var list_ids = $.map($objects, function(obj) { return $(obj).data('list-id'); });

		for(var i = 0; i < list_ids.length; i++) {
			$('.dnd-source .block-info-cp[data-list-id="' + list_ids[i] + '"]').draggable('enable');
			$objects.remove();
		}
	}

	
	$('#tabs-pages').delegate('.cl-kr', 'click', function() {
		var $obj = $(this).closest(' .block-info-cp'),
			list_id = $obj.data('list-id');
		$.ajax({
			url: '<?php echo $baseurl; ?>/index.php/list/RemoveListFromOfficialToday',
			type: 'POST',
			dataType: 'text',
			data: {
				list_id: list_id
			},
			success: function(result) {
				if (result == '-1' ) return;
				restoreToSource($obj);
			}
		});
	});
	
	
	// TABS ****************************************************************************************
	$('#tabs-pages').tabs();
	
	
	$('#tabsAddPage').click(function() {
		var n = $('#tabs-pages ul li').size() + 1;

		$('#tabs-pages ul').append('<li><a href="#page-' + n + '">' + n + '</a></li>');

		$page = $('#page-1').clone().prop('id', 'page-' + n);
		$page.find('.droppable-item').empty().droppable(droppableOptions);
		
		$('#tabs-pages')
			.append($page)
			.tabs('refresh')
			.tabs('option', 'active', n - 1);
	});
	

	$('#tabsDelPage').click(function() {
		var page_no = 1 + $('#tabs-pages').tabs('option', 'active');
		
		$.ajax({
			url: '<?php echo $baseurl; ?>/index.php/list/RemovePageFromOfficialToday',
			type: 'POST',
			dataType: 'text',
			data: {
				page_no: page_no,
				shift: true
			},
			success: function(result) {
				if (result == '-1' ) return;
					
				restoreToSource($('#page-' + page_no + ' .block-info-cp'));
				
				if ($('#tabs-pages').find('.ui-tabs-nav li').size() > 1) {
					$('#tabs-pages').find('.ui-tabs-nav li:eq(' + (page_no - 1) + ')').remove();
				
					$('#page-' + page_no).remove();
			
					$('#tabs-pages').find('.ui-tabs-nav li a').each(function(i) {
						$(this).prop('href', '#page-' + (i + 1)).text(i + 1);
					});
			
					$('#tabs-pages').find('.ui-tabs-panel').each(function(i) {
						$(this).prop('id', 'page-' + (i + 1));
					});
				}
			
				$('#tabs-pages').tabs('option', 'active', page_no == 1 ? 0 : page_no - 2).tabs('refresh');
			}
		})
	});

	
	$('#tabsClearAllPages').click(function() {
		var $objects = $('#tabs-pages .ui-tabs-panel .block-info-cp');
		if ($objects.size() > 0) {
			$.ajax({
				url: '<?php echo $baseurl; ?>/index.php/list/RemovePageFromOfficialToday',
				type: 'POST',
				dataType: 'text',
				success: function(result) {
					if (result == '-1' ) return;
					restoreToSource($objects);
				}
			});
		}
	});
	

	$('#tabsClearThisPage').click(function() {
		var page_no  = 1 + $('#tabs-pages').tabs('option', 'active'),
			$objects = $('#page-' + page_no + ' .block-info-cp');
			
		if ($objects.size() > 0) {
			$.ajax({
				url: '<?php echo $baseurl; ?>/index.php/list/RemovePageFromOfficialToday',
				type: 'POST',
				dataType: 'text',
				data: {
					page_no: page_no
				},
				success: function(result) {
					if (result == '-1' ) return;
					restoreToSource($objects);
				}
			});
		}
	});
	

	// DRAG & DROP *********************************************************************************
	
	var droppableOptions = {
		hoverClass: 'dropable-item-hover',
		accept: function() {
			return $(this).find('.block-info-cp').size() === 0;
		},
		drop: function(event, ui) {
			var $that = $(this),
				list_id  = ui.draggable.data('list-id'),
				page_no  = 1 + $('#tabs-pages').tabs('option', 'active');
				position = $that.data('list-position');
				
			$.ajax({
				url: '<?php echo $baseurl; ?>/index.php/list/AddListToOfficialToday',
				type: 'POST',
				dataType: 'text',
				data: {
					list_id: list_id,
					page_no: page_no,
					position: position
				},
				success: function(result) {
					if (result == '-1' ) return;
					
					var clone = ui.draggable.clone();
					var img = clone.find('img');					
					if ($that.hasClass("bl-left")) {
					    img.attr('src', img.attr('src').replace('w=127&h=83', 'w=188&h=100'));
				    } else {
				        img.attr('src', img.attr('src').replace('w=127&h=83', 'w=138&h=100'));
				    }
				    
					$that.append(clone);
					ui.draggable.draggable('disable');
				}
			});
		}
	};
	$('.droppable-item').droppable(droppableOptions);
	
	
	$('#searchStr').on('keyup', function(event) {
		if (event.keyCode == 13) {
			var value = $(this).val().replace(/^\s+|\s+$/g, '');
			if (value.length > 1) {
				$.ajax({
					url: '<?php echo $baseurl; ?>/index.php/list/SearchListsByInterests',
					type: 'POST',
					dataType: 'html',
					data: {
						search_str: value
					},
					success: function(result) {
						if (result == '-1' ) return;
						$('#content_control_panel .mCSB_container').html(result);
						$('.draggable-item').draggable({
							appendTo: 'body',
							helper: 'clone',
							revert: 'invalid',
							cursor: 'move'
						}).filter('.draggable-item-disabled').draggable('disable');
					}
				});
			}
		}
	});

}); // ready

</script>

<div class="info-search-cp" >
	<p>Find the list you're interested in and then drag them to their place in the page</p>
	<div class="control-search" >
		<input class="search-control-panel" type="text" id="searchStr"  placeholder="Search" />
		<!--input type="submit" value="" class="i-sub"-->
	</div>	
</div>

<div class="cp-content">
	<div id="content_control_panel" class="cp-col-left">
	</div><!-- .cp-col-left -->

	<div class="cp-col-right">
		<div class="pagination">
			<div class="pag-all">pages</div>
			<div id="tabs-pages">
				<div class="right-pag-block">
					<div id="tabsDelPage" class="pag-list min"></div>
					<div id="tabsAddPage" class="pag-list pls"></div>
				</div>
				<ul>
				<?php for($i = 1; $i <= count($official); $i++) { ?>
					<li class="pag-list"><a href="#page-<?php echo $i ?>"><?php echo $i ?></a></li>
				<?php } ?>
				</ul>
				<?php
				for($i = 1; $i <= count($official); $i++) {
					$lists = $official[$i-1];
				?>
				<div id="page-<?php echo $i ?>" class="dnd-destination">
					<div class="img-content">
					<?php
					for($j = 1; $j <= count($lists); $j++) {
						$list   = $lists[$j-1];
						$is_big = (($j % 4 == 0) || (($j + 3) % 4 == 0));
						$image  = (!((strpos($list['image'], 'profile/') === 0) || (strpos($list['image'], 'freeworld/') === 0)))
							? 'titleimages/list_thumbs/official/'.($is_big ? 'big' : 'small').'/' . $list['image']
							: $list['image'];
						$image  = Yii::app()->params['imageurl'] . '/images/' . $image . ($is_big ? '?w=188&h=100' : '?w=138&h=100');
					?>
						<div class="<?php echo ($is_big ? 'bl-left' : 'bl-right'); ?> droppable-item" data-list-position="<?php echo $j; ?>">
						<?php if ($list) { ?>
							<div class="block-info-cp" data-list-id="<?php echo $list->id; ?>">
								<img class="block-info-cp-image" src="<?php echo $image; ?>" alt="<?php echo $list->title; ?>" />
								<div class="block-shortinfo-cp">
									<div class="title"><?php echo $list->title; ?></div>
									<!--ul class="plus">
									<li>Lorem ipsum dolor sit amet, consectetur</li>
									<li>Quisque pretium nisl orci, vel bibendum</li>
									</ul-->
								</div>
								<span class="cl-kr"></span>
							</div>
						<?php } ?>
						</div>
					<?php
					}
					?>
					</div>
				</div>
				<?php
				}
				?>
			</div><!-- #tabs-pages -->
			
			<div class="btn-cp-pages">
				<div class="clear-this-page-btn" id="tabsClearThisPage" style="margin: 0 10px 0 10px;">Clear this page</div>
				<div class="clear-all-page-btn" id="tabsClearAllPages" style="margin-right: 40px;">Clear All Pages</div>
				<div class="clear-all-page-btn" id="tabsSave" style="width: 35px;" onclick="javascript:$('#saveForm').submit();">Save</div>
			</div>
			
			<form action="<?php echo $baseurl;?>/index.php/list/saveOfficial" id="saveForm" method="post">
			    <input type="hidden" id="save" name="save" value="save" />
			</form>
			
		</div><!-- .pagination -->
	</div><!-- .cp-col-right -->
	
</div><!-- .cp-content -->				
