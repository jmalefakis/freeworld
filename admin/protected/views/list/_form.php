<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name',$htmlOptions=array('style'=>'float: left; width: 76px;')); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title',$htmlOptions=array('style'=>'float: left; width: 76px;')); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'listtype',$htmlOptions=array('style'=>'float: left; width: 76px;')); ?>
		<?php echo $form->textField($model,'listtype'); ?>
		<?php echo $form->error($model,'listtype'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'permission',$htmlOptions=array('style'=>'float: left; width: 76px;')); ?>
		<?php echo $form->textField($model,'permission'); ?>
		<?php echo $form->error($model,'permission'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->