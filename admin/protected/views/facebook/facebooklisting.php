<?php
$this->breadcrumbs=array(
	'Facebook'=>array('facebooklisting'),
	'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('customer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?> 

<div class="refresh_twiit">
    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/facebook/facebook"><span>Refresh</span></a>
</div>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--<div class="search-form" style="display:none">
<?php /*$this->renderPartial('_search',array(
	'model'=>$model,
)); */?>
</div><!-- search-form -->
<?php $userid = Yii::app()->user->getID(); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'customer-grid',         
	'dataProvider'=>new CActiveDataProvider('FacebookForm', array(
            'criteria'=>array(
                'condition'=>'user_id="'.$userid.'"',        
            ),
            'pagination'=>array(
                'pageSize'=>10,
            ),
             )),
	'filter'=>$model,
         'columns'=>array(
                //'id',
		'friend_name',
		'friend_facebookid',
		'friend_facebookimg'=>array(
                    'name'=>'friend_facebookimg',
                    'type' => 'image',
                    'value' => $model->friend_facebookimg,
                ),		
                'created',
                'modified',
		array(
			'class'=>'CButtonColumn',
		),
	),
	
)); ?>
