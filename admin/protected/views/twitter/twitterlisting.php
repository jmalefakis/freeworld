<?php
$this->breadcrumbs=array(
	'Twitter'=>array('twitterlisting'),
	'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('customer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?> 

<div class="refresh_twiit">
    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/twitter/twitter"><span>Refresh</span></a>
</div>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--<div class="search-form" style="display:none">
<?php /*$this->renderPartial('_search',array(
	'model'=>$model,
)); */?>
</div><!-- search-form -->

<?php $userid = Yii::app()->user->getID(); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'customer-grid',         
	'dataProvider'=>new CActiveDataProvider('Twitter', array(
            'criteria'=>array(
                'condition'=>'user_id="'.$userid.'"',        
            ),
            'pagination'=>array(
                'pageSize'=>10,
            ),
             )),
	'filter'=>$model,
         'columns'=>array(
                //'id',
		'friend_name',
		'friend_twitterid',
		'friend_twitteimg'=>array(
                    'name'=>'friend_twitteimg',
                    'type' => 'image',
                    'value' => $model->friend_twitteimg,
                ),		
                'created',
                'modified',
		array(
			'class'=>'CButtonColumn',
		),
	),
	
)); ?>
