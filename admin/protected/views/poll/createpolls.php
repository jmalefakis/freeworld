 <?php $this->renderPartial('sidenavigation'); ?>   
<div class="outerdiv1_auth">
    <h2 style="color: #666666;">Create a new poll to share with your friends</h2>
    
    <p style="margin-bottom: 7px;">This is the place where you can create a poll on any topic you wish and share with your friends.</p>
    <p style="margin-bottom: 30px;">Once you have created the poll you can share it with your friend.</p>
    
  <?php
        //$baseurl = Yii::app()->request->baseUrl; 
        $siteurl = Yii::app()->params['siteurl']; 
        $form=$this->beginWidget('CActiveForm', array(
        'id'=>'listcreate',
        'action'=>$siteurl.'/index.php/poll/createpolls',
        'method'=>'post',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(        
        'enctype'=> 'multipart/form-data',
        'name'=>'listcreate', 
        'onsubmit'=>'return createpolls();',
        'validateOnSubmit'=>true,
     ),
      )); 
?>
    
    <div class="listnew_title_div1" style="width: 650px;">
        <div class="create_poll" style="width: 155px;">
            <span>Enter your question *</span>
        </div>
        <div class="floatleft">
            <input id="question" name="Poll[question]" type="text" value="" style="width: 360px;height: 20px;"/> 
        </div>
        
        <div class="requretitle" id="questionreq">
                    <span>This field is required </span>
        </div>
    </div>
    
    <div class="listnew_title_div1">
                 <div class="forgotpasdiv1" style="width: 150px;">
                     <span>
                         Poll Type *
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="listtype" name="Poll[polltype]"  class="forgotpasinput">
                         <option value="select" selected="selected" >Select</option>                         
                          <option value="public" >Public</option>
                          <option value="private " >Private </option>
                          <!--<option value="official" >Official</option>-->
                     </select>
                 </div> 
               <div class="requretitle_select" id="listtypereq">
                    <span>This field is required </span>
                </div>
   </div>
    
    
    <div class="listnew_title_div1">
                 <div class="forgotpasdiv1" style="padding-right: 58px;">
                     <span>
                         Chose category *
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <select  id="interest" name="ListForm[interest]"  class="forgotpasinput">
                         <option value="select" selected="selected" >Select</option>
                         <?php foreach ($interest as $inter){?>
                          <option value="<?php echo $inter->id?>" ><?php echo $inter->interest?></option>
                          <?php }?>
                     </select>
                 </div> 
               <div class="requretitle_select" id="interestreq">
                    <span>This field is required </span>
                </div>
             </div>
    
    <div class="listnew_title_div1" >
                <div class="image_lable" style="width: 160px;padding: 0px;">
                    <span> Upload a Relevant Picture*</span>                        
                </div>
                <div class="row_textfild">
                    <input id="image" type="file" name="image" value="" class="image_upload_reg" accept="image/*" />
                </div> 
                <div class="requretitle" id="imgreq">
                    <span>This field is required </span>
                </div>
    </div>
    
    <div class="submitdiv">
            <input class="image_buttons" type="image"   src="<?php echo Yii::app()->request->baseUrl;?>/images/buttons/next.png"/> 
    </div>
    
 <?php $this->endWidget(); ?> 
    
</div>