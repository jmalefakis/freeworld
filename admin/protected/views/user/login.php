<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="contrllpannel_login">
    <span>Wayo Control Panel</span>
</div>


<div class=" form outer_div_login" style="margin: 0 auto;">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)); ?>

	

	<div class="row div1_login">
		<?php echo $form->labelEx($model,'Email',$htmlOptions=array('class'=>'login_email_cp')); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email',$htmlOptions=array('class'=>'error_email_cp')); ?>
	</div>

	<div class="row div1_login">
		<?php echo $form->labelEx($model,'password',$htmlOptions=array('class'=>'login_password_cp')); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password',$htmlOptions=array('class'=>'error_password_cp')); ?>
		
	</div>

	<div class="row rememberMe login_div_remeber" style="  margin-right: 47px;">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row submit submit_login">
		<?php echo CHtml::submitButton('Login',$htmlOptions=array('class'=>'submit_login'));  ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
