<?php
$this->breadcrumbs=array(
	'User'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Customer', 'url'=>array('index')),
	array('label'=>'Create Customer', 'url'=>array('create')),
	array('label'=>'Update Customer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Customer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Customer', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
                'member',
		'firstname',
		'lastname',
		'email',		
		'interest',
		'city',		
                'state',
                'country',  
		'gender',
		'birthdate',
                'created',
                'modified',
        array('name' => 'badge',
               'type' => 'html',
               'value' => (($model->badge == 1) ? "<img src='".Yii::app()->baseUrl."/../images/eagle.png' width='30'>" : "-")),
        array('name' => 'instaview',
               'type' => 'html',
               'value' => (($model->instaview == 1) ? "<img src='".Yii::app()->baseUrl."/../images/instaview-ico.png' width='16'>" : "-")),
	),
)); ?>
