<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'firstname',$htmlOptions=array('class'=>'div_email')); ?>
		<?php echo $form->textField($model,'firstname'); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'lastname',$htmlOptions=array('class'=>'div_email')); ?> 
		<?php echo $form->textField($model,'lastname'); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'birthdate',$htmlOptions=array('class'=>'div_email')); ?>
		<?php echo $form->textField($model,'birthdate'); ?>
		<?php echo $form->error($model,'birthdate'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'facebook_id',$htmlOptions=array('class'=>'div_email')); ?>
		<?php echo $form->textField($model,'facebook_id'); ?>
		<?php echo $form->error($model,'facebook_id'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'email',$htmlOptions=array('class'=>'div_email')); ?> 
		<?php echo $form->textField($model,'email',array('size'=>20,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
        
        
        <div class="row">
		<?php echo $form->labelEx($model,'gender',$htmlOptions=array('class'=>'div_email')); ?> 
		<?php echo $form->textField($model,'gender'); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>
        
         <div class="row">
		<?php echo $form->labelEx($model,'country',$htmlOptions=array('class'=>'div_email')); ?>
		<?php echo $form->textField($model,'country'); ?>
		<?php echo $form->error($model,'country'); ?>
	</div>
        
         <div class="row">
		<?php echo $form->labelEx($model,'state',$htmlOptions=array('class'=>'div_email')); ?>
		<?php echo $form->textField($model,'state'); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>
        
	
        
        <div class="row">
		<?php echo $form->labelEx($model,'member',$htmlOptions=array('class'=>'div_email')); ?> 
		<?php echo $form->textField($model,'member',array('size'=>20,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'member'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'interest',$htmlOptions=array('class'=>'div_email')); ?>
		<?php echo $form->textField($model,'interest',array('size'=>20,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'interest'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin',$htmlOptions=array('class'=>'div_email')); ?> 
		<?php echo $form->textField($model,'admin',array('size'=>20,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'admin'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'created',$htmlOptions=array('class'=>'div_email')); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified',$htmlOptions=array('class'=>'div_email')); ?> 
		<?php echo $form->textField($model,'modified'); ?>
		<?php echo $form->error($model,'modified'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'official badge',$htmlOptions=array('class'=>'div_email')); ?>
        <?php
            $badges = array(0 => 'No',1 => 'Yes');
        ?>
		<?php echo $form->dropDownList($model,'badge',$badges); ?>
		<?php echo $form->error($model,'badge'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'Instaview',$htmlOptions=array('class'=>'div_email')); ?>
        <?php
            $instaviews = array(0 => 'No',1 => 'Yes');
        ?>
		<?php echo $form->dropDownList($model,'instaview',$instaviews); ?>
		<?php echo $form->error($model,'instaview'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->