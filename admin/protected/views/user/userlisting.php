<?php
$this->breadcrumbs=array(
	'User'=>array('userlisting'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Customer', 'url'=>array('index')),
	array('label'=>'Create Customer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('customer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'id',
                'member',
		'firstname',
		'lastname',
		'email',		
		'interest',
                /*'education_more',
                'school',
                'degree',
                'degreeyear',*/
                'city',		
                'state',
                'country',             
		'gender',
		'birthdate',
                'created',
                //'modified',
        array('name' => 'badge',
               'type' => 'html',
               'value' => '(($data->badge == 1) ? "<img src=\'".Yii::app()->baseUrl."/../images/eagle.png\' width=\'30\'>" : "")',
               'filter'=>false),
        array('name' => 'instaview',
               'type' => 'html',
               'value' => '(($data->instaview == 1) ? "<img style=\'padding-left:20px;\' src=\'".Yii::app()->baseUrl."/../images/instaview-ico.png\' width=\'16\'>" : "")',
               'filter'=>false),
        
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
