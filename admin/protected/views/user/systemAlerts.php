<?php
$this->pageTitle = Yii::app()->name . ' - System Alerts';
$this->breadcrumbs = array('System Alerts',);

$baseurl = Yii::app()->request->baseUrl;
?>
<div class="page-system-alerts">
	<p class="info-p">Send a message to all users of Wayo</p>
	<form class="fieldMessAll" method="post" id="sform"
		action="<?php echo $baseurl; ?>/index.php/user/systemAlerts">
		<textarea name="smessage"></textarea>
		<input class="send-btn" type="submit" value="SEND">
	</form>

	<div class="content-message-log">
		<p class="content-message-log-title">Message log</p>
		<div class="content-list">
			<ul class="list-error">
			    <?php 
			    foreach ($alertsLog as $alert)
			    {
			    ?>
			
				<li>
					<div class="message-error-info">
						<?php echo $alert->message; ?>
					</div>
					<div class="message-error-time">
						<div class="icon-shedule">
						    <?php echo date('d F Y', DateHelper::getClientTime($alert["created"])); ?>
						</div>
						<div class="icon-time">
						    <?php echo date('g:i A', DateHelper::getClientTime($alert["created"])); ?>
						</div>
					</div>
				</li>
				
				<?php 
			    }
				?>
			</ul>
		</div>
	</div>
</div>
