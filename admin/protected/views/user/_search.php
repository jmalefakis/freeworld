<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		 <?php echo $form->label($model,'firstname'); ?>
		 <?php echo $form->textField($model,'firstname'); ?>
	</div>
        <div class="row">
		 <?php echo $form->label($model,'lastname'); ?>
		 <?php echo $form->textField($model,'lastname'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Email'); ?>
		<?php echo $form->textField($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'birthdate'); ?>
		<?php //echo $form->textField($model,'created'); 
                   $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[birthdate]',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'changeMonth'=>true,
                        'dateFormat'=>'mm/dd/yy',
                        'changeYear'=>true,
                        'yearRange'=>'1960:2012', 
                        'showAnim'=>'fold',
                        'showButtonPanel'=>true,
                        'debug'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;',
                         'readonly'=>"readonly",
                    ),
                )); ?>
	</div>
    
        <div class="row">
		<?php echo $form->label($model,'member'); ?>
		<?php echo $form->textField($model,'member'); ?>
	</div>
    
         <div class="row">
		<?php echo $form->label($model,'gender'); ?>
		<?php echo $form->textField($model,'gender'); ?>
	</div>
    
        <div class="row">
		<?php echo $form->label($model,'country'); ?>
		<?php echo $form->textField($model,'country'); ?>
	</div>
    
        
    
         <div class="row">
		<?php /*echo $form->label($model,'education_more'); ?>
		<?php echo $form->textField($model,'education_more'); ?>
	</div>
    
        <div class="row">
		<?php echo $form->label($model,'school'); ?>
		<?php echo $form->textField($model,'school'); ?>
	</div>
    
       <div class="row">
		<?php echo $form->label($model,'degree'); ?>
		<?php echo $form->textField($model,'degree'); ?>
	</div>
    
        <div class="row">
		<?php echo $form->label($model,'degreeyear'); ?>
		<?php echo $form->textField($model,'degreeyear'); */?>
	</div>
    
        <div class="row">
		<?php echo $form->label($model,'interest'); ?>
		<?php echo $form->textField($model,'interest'); ?>
	</div>
            

	<div class="row">
		<?php echo $form->label($model,'admin'); ?>
		<?php echo $form->textField($model,'admin',array('size'=>20,'maxlength'=>50)); ?>
	</div>	

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php //echo $form->textField($model,'created'); 
                   $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[created]',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'showAnim'=>'fold',
                        'changeMonth'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'changeYear'=>true,
                        'yearRange'=>'1960:2012', 
                        'showAnim'=>'fold',
                        'showButtonPanel'=>true,
                        'debug'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;',
                        'readonly'=>"readonly",
                        
                    ),
                )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified'); ?>
		<?php //echo $form->textField($model,'modified'); 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'User[modified]',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                       'showAnim'=>'fold',
                        'changeMonth'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'changeYear'=>true,
                        'yearRange'=>'1960:2012', 
                        'showAnim'=>'fold',
                        'showButtonPanel'=>true,
                        'debug'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px;',
                        'readonly'=>"readonly",
                        
                    ),
                )); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->