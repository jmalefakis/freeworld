<?php
$this->pageTitle=Yii::app()->name . ' - Register';
$this->breadcrumbs=array(
	'Register',
);
?>

<h1>Register With Us</h1>

<p>Please fill out the following  for register:</p>

<div class="form">
 <?php
    $baseurl = Yii::app()->request->baseUrl; 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'action'=>$baseurl.'/index.php/user/register',
        'method'=>'post',
        'enableClientValidation'=>true,
        'htmlOptions'=>array(        
        'name'=>'mform',    
        'validateOnSubmit'=>true,
 ),
    )); 
?>  
   

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="register_name">
                 <div class="register_name_div1">
                     <span>
                         Name : &nbsp;&nbsp;
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input type="text" name="User[name]" class="forgotpasinput"/>
                 </div>                 
             </div>
    
            <div class="register_name">
                 <div class="forgotpasdiv1">
                     <span>
                         Email address :
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input type="text" name="email" class="forgotpasinput"/>
                 </div>                 
             </div>
            
            <div class="register_name">
                 <div class="forgotpasdiv1">
                     <span>
                         Your Password:
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input type="password" name="password" class="forgotpasinput"/>
                 </div>                 
             </div>
               
    
            <div class="register_name">
                 <div class="forgotpasdiv1">
                     <span>
                         Your Company:
                     </span>
                 </div>
                 <div class="forgotpasdiv2">
                     <input type="text" name="company" class="forgotpasinput"/>
                 </div>                 
             </div>
    
    
         <div class="row submit">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

	

	

	

<?php $this->endWidget(); ?>
</div><!-- form -->
