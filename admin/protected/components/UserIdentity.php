<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    /**
     * Authenticates an Admin User.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        if (isset($_POST['LoginForm']))
        {
            $email = $_POST['LoginForm']['email'];
            $password = $_POST['LoginForm']['password'];
            $record = User::model()->findByAttributes(array('email' => $email));
            
            if($record == null || $record->admin != 1) 
            {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            }
            else if ($record->password !== md5($this->password))
            {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            else {
                $this->_id = $record->id;
                $this->username = $record->email;
                $this->errorCode = self::ERROR_NONE;
            }

            return !$this->errorCode;
        }
    }

    /**
     * @return integer the ID of the user record
     */
    public function getId()
    {
        return $this->_id;
    }
}