<?php
date_default_timezone_set('UTC');
// change the following paths if necessary
$yii=dirname(__FILE__) . '/../framework/yii.php';

// TODO: create and change config for you if needed
// but DO NOT COMMIT THIS CHANGES!!!
//$config=dirname(__FILE__) . '/protected/config/cherlyn.php';
//$config=dirname(__FILE__) . '/protected/config/anna.php';
//$config=dirname(__FILE__) . '/protected/config/main38.php';
//$config=dirname(__FILE__) . '/protected/config/main.php';
$config=dirname(__FILE__) . '/protected/config/production.php'; //http://gofreeworld.com/admin

// remove the following line when in production mode
//defined('YII_DEBUG') or 
//define('YII_DEBUG', true);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(-1);

require_once($yii);
Yii::createWebApplication($config)->run();
