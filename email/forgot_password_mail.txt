﻿[to]
{user_email}

[from]
noreply@dev.freeworldco.com

[subject]
Password details - FreeWorldco.com

[body]
<img src="{_siteurl}/images/freeworld/logo.png" style="height:75px;width:280px;"/><br/><br/>
<div style="font-family: Arial; font-size: 20pt; color: #333333;">
</div>
<div style="padding: 16px 0 18px;">
	Dear {user_firstname} {user_lastname},<br/>
	You can login to the website using the password mentioned below. You can change it after signing in to the site  <br/>
	<span style="font-family:Arial;font-size: 13pt; color: #333333;"> Your Password : {user_password}</span> <br/>
	You can login to the site by clicking on this link <br/>
	Login URL: <b>{_siteurl}<br/><br/>
	Best Regards,<br/>
	FreeWorld Team
</div>



