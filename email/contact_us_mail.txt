﻿[to]
{user_email}

[from]
info@dev.freeworldco.com

[subject]
ContactUs - FreeWorldco.com

[body]
<img src="{_siteurl}/images/freeworld/logo.png" style="height:75px; width:280px;" /><br/><br/> 
<div style="padding: 16px 0 18px;">
	Dear {user_firstname} {user_lastname}, <br/><br/>
	Thank you for the feedback. <br/> 
	We have received your contact details. We will contact you soon.  <br/><br/>
	Best Regards,<br/>
	FreeWorld Team
</div>