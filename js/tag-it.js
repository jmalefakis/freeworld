(function($) {
    $.fn.tagedAutocomplete = function(autocompleteOptions, initialData) {
        var oldSelect = function(event, ui) {
            return true;
        };

        if (autocompleteOptions && autocompleteOptions.select) {
            oldSelect = autocompleteOptions.select;
        }

        return this.each(function() {
            var selector = $(this), input = $('input[type=text]', this);

            selector.click(function() {
                input.focus();
            }).delegate('.tag a', 'click', function() {
                var tag = $(this).parent();
                var index = input.data("ids").indexOf(tag.data("tag-id"));
                if (index > -1) {
                    input.data("ids").splice(index, 1);
                }
                tag.remove();
                
                input.trigger('dataChanged');
            });
            
            // save selected items ids here
            input.data("ids", []);
            
            if (initialData)
            {
                // render already selected tags
                var length = initialData.length, item = null;
                for (var i = 0; i < length; i++) {
                    item = initialData[i];
                    // add tag
                    $('<span class="tag"/>').text(item.name + ' ').append(
                         '<a>×</a>').data("tag-id", item.id).insertBefore(input);
                    // save id
                    input.data("ids").push(item.id);
                }
            }
            
            // bug 3769
            input.blur(function() {
               $(this).val(''); 
            });
            
            autocompleteOptions.focus = function(event, ui) {
                return false; 
            };
            autocompleteOptions.select = function(event, ui) {
                if (ui.item == null) {
                    return false;
                }
                
                // add tag
                $('<span class="tag"/>').text(ui.item.name + ' ').append(
                     '<a>×</a>').data("tag-id", ui.item.id).insertBefore(input);
                
                // clear value
                ui.item.value = "";
                
                // store selected id
                input.data("ids").push(ui.item.id);
                
                input.trigger('dataChanged');
                
                return oldSelect(event, ui);
            };
            
            input.keydown(
                    function(e) {
                        if (e.keyCode === $.ui.keyCode.TAB
                                && $(this).data('ui-autocomplete').menu.active) {
                            e.preventDefault();
                        }
                        
                        /*if (e.keyCode === $.ui.keyCode.ENTER) {
                            $(this).data('ui-autocomplete').search();
                        }*/
                    }).autocomplete(autocompleteOptions);
            
            input.data('ui-autocomplete')._renderItem = function(ul, item) {
                if (input.data("ids").indexOf(item.id) !== -1) {
                    return { data: function() { return { value: "" }; } };
                }
                
                return $('<li/>').data('ui-autocomplete-item', item).append(
                        $('<a/>').html(item.value)).appendTo(ul);
            };
            /*input.data('ui-autocomplete')._resizeMenu = function(ul, item) {
                var ul = this.menu.element;
                ul.outerWidth(Math.max(ul.width('').outerWidth(), selector
                        .outerWidth()));
            };*/
        });
    };

})(jQuery);
