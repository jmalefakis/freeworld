function onclickimageper(baseurl)
{
    
	var input = document.getElementById("images"), 
		formdata = false;

	function showUploadedItem (source) {
  		    var list = document.getElementById("image-list"),
	  		li   = document.createElement("li"),
	  	       imgs  = document.createElement("img");
                        
  		imgs.src = source;
                
  		li.appendChild(imgs);
		//list.appendChild(li);
	}   

	if (window.FormData) {
  		formdata = new FormData(); 
  		document.getElementById("btn").style.display = "none";
	}
	
 	input.addEventListener("change", function (evt) {
 		document.getElementById("response").innerHTML = "<img src='"+baseurl+"/images/loading.gif' class='lodinf_uplod'/>";
                
 		var i = 0, len = this.files.length, img, reader, file;
	
		for ( ; i < len; i++ ) {
			file = this.files[i];
	
			if (!!file.type.match(/image.*/)) {
				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) { 
						showUploadedItem(e.target.result, file.fileName);
					};
					reader.readAsDataURL(file);
				}
				if (formdata) {
					formdata.append("images[]", file);
                                        
				}
			}	
		}
	
		if (formdata) {
                    var allowedExts = new Array ("image/gif","image/jpeg","image/jpg","image/png");
                    var temp        = $.inArray(file.type, allowedExts);
                    if(temp>=0) {
                        var currenturl = $("#currenturl").val();
                        
                        if(currenturl != "myProfile" && currenturl != "myProfileCompany"){
                         var url = baseurl+"/index.php/user/home";
                        }
                        else{ 
                        var url = baseurl+"/index.php/user/upoadImageProfile";
                        }

                            $.ajax({
                                    url: url,
                                    type: "POST",
                                    data: formdata,
                                    processData: false,
                                    contentType: false,
                                    success: function (res) { 
                                            document.getElementById("response").innerHTML = res; 
                                            document.getElementById("image_upolad").style.display="none";
                                    }
                            });
                    }
                    else
                        {
                            document.getElementById("response").innerHTML = "<img src='"+baseurl+"/images/default.jpg'/>";
                            document.getElementById("image_upolad").style.display="block";
                        }
		}
	}, false);
    
}

function onclickimage(baseurl)
      {
          
                  var input = document.getElementById("images_com"), 
                        formdata = false;

                function showUploadedItem (source) {
                        var list = document.getElementById("image-list_com"),
                                li   = document.createElement("li"),
                                imgs  = document.createElement("img");

                        imgs.src = source;

                        li.appendChild(imgs);
                        //list.appendChild(li);
                }   

                if (window.FormData) {
                        formdata = new FormData(); 
                        document.getElementById("btn_com").style.display = "none";
                }

                input.addEventListener("change", function (evt) {
                        document.getElementById("response_com").innerHTML = "<img src='"+baseurl+"/images/loading.gif' class='lodinf_uplod'/>";
                        var i = 0, len = this.files.length, img, reader, file;

                        for ( ; i < len; i++ ) {
                                file = this.files[i];

                                if (!!file.type.match(/image.*/)) {
                                        if ( window.FileReader ) {
                                                reader = new FileReader();
                                                reader.onloadend = function (e) { 
                                                        showUploadedItem(e.target.result, file.fileName);
                                                };
                                                reader.readAsDataURL(file);
                                        }
                                        if (formdata) {
                                                formdata.append("images[]", file);
                                        }
                                }	
                        }

                        if (formdata) {
                            
                            var allowedExts = new Array ("image/gif","image/jpeg","image/jpg","image/png");
                            var temp        = $.inArray(file.type, allowedExts);
                            if(temp>=0) {
                            
                                $.ajax({
                                        url: baseurl+"/index.php/user/home",
                                        type: "POST",
                                        data: formdata,
                                        processData: false,
                                        contentType: false,
                                        success: function (res) { 
                                                document.getElementById("response_com").innerHTML = res; 
                                                document.getElementById("file_uplod_com").style.color = "#565656";
                                                document.getElementById("imageuplodid").style.display='none';
                                                document.getElementById("errordiv_submit").style.display='none';
                                                document.getElementById("imageuplodid").innerHTML="Please upload image";
                                                document.getElementById("imageuplodid").style.display="none";
                                        }
                                });
                            } 
                                 else
                                {
                                    document.getElementById("response_com").innerHTML = "<img src='"+baseurl+"/images/default.jpg'/>";
                                    document.getElementById("imageuplodid").innerHTML="Please upload correct image";
                                    document.getElementById("imageuplodid").style.display="block";
                                }
                        }
                }, false);
            }
      

function listtitle_image(baseurl)
{    
	var input = document.getElementById("images"), 
	formdata = false;

	function showUploadedItem (source) {
  		    var list = document.getElementById("image-list"),
	  		li   = document.createElement("li"),
	  	       imgs  = document.createElement("img");
                        
  		imgs.src = source;
                
  		li.appendChild(imgs);
		//list.appendChild(li);
	}   

	if (window.FormData) {
  		formdata = new FormData(); 
  		document.getElementById("btn").style.display = "none";
	}
	
 	input.addEventListener("change", function (evt) { 
 		document.getElementById("response").innerHTML = "<div class='imagelod_listtitle'><img src='"+baseurl+"/images/loading.gif' class='lodinf_uplod'/></div>";
               
               /*jQuery.each(this, function(name, value) {
                    alert(name + ": " + value);
                });*/
                
 		var i = 0, len = this.files.length, img, reader, file;
	
		for ( ; i < len; i++ ) {
			file = this.files[i];
	
			if (!!file.type.match(/image.*/)) {
				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) { 
						showUploadedItem(e.target.result, file.fileName);
					};
					reader.readAsDataURL(file);
				}
				if (formdata) {
					formdata.append("images[]", file);
                                        
				}
			}	
		}
	
		if (formdata) {
                    var allowedExts = new Array ("image/gif","image/jpeg","image/jpg","image/png");
                    var temp        = $.inArray(file.type, allowedExts);
                    if(temp>=0) {
                                   
                        var url = baseurl+"/index.php/preferences/listTitleImage";                       
                            $.ajax({
                                    url: url,
                                    type: "POST",
                                    data: formdata,
                                    processData: false,
                                    contentType: false,
                                    success: function (res) { 
                                            document.getElementById("response").innerHTML = res; 
                                            document.getElementById("image_upolad").style.display="none";
                                            document.getElementById("imguplod").value="upload";
                                    }
                            });
                    }
                    else
                        {
                            document.getElementById("response").innerHTML = "<img src='"+baseurl+"/images/picture_listtitle.png'/>";
                            document.getElementById("image_upolad").style.display="block";
                            document.getElementById("imguplod").value = "";
                        }
		}
	}, false);
    
}



/**************** For IE***********************************/


function $m(theVar){
	return document.getElementById(theVar)
}
function remove(theVar){
	
}
function addEvent(obj, evType, fn){
	if(obj.addEventListener)
	    obj.addEventListener(evType, fn, true)
	if(obj.attachEvent)
	    obj.attachEvent("on"+evType, fn)
}
function removeEvent(obj, type, fn){
	if(obj.detachEvent){
		obj.detachEvent('on'+type, fn);
	}else{
		obj.removeEventListener(type, fn, false);
	}
}
function isWebKit(){
	return RegExp(" AppleWebKit/").test(navigator.userAgent);
}
function ajaxUpload(form,url_action,id_element,html_show_loading,html_error_http,baseurl){
	var detectWebKit = isWebKit();
	form = typeof(form)=="string"?$m(form):form;
	var erro="";
	if(form==null || typeof(form)=="undefined"){
		erro += "The form of 1st parameter does not exists.\n";
	}else if(form.nodeName.toLowerCase()!="form"){
		erro += "The form of 1st parameter its not a form.\n";
	}
	if($m(id_element)==null){
		erro += "The element of 3rd parameter does not exists.\n";
	}
	if(erro.length>0){
		alert("Error in call ajaxUpload:\n" + erro);
		return;
	}
	var iframe = document.createElement("iframe");
	iframe.setAttribute("id","ajax-temp");
	iframe.setAttribute("name","ajax-temp");
	iframe.setAttribute("width","0");
	iframe.setAttribute("height","0");
	iframe.setAttribute("border","0");
	iframe.setAttribute("style","width: 0; height: 0; border: none;");
	form.parentNode.appendChild(iframe);
	window.frames['ajax-temp'].name="ajax-temp";
	var doUpload = function(){
		removeEvent($m('ajax-temp'),"load", doUpload);
		var cross = "javascript: ";
		cross += "window.parent.$m('"+id_element+"').innerHTML = document.body.innerHTML; void(0);";
		$m(id_element).innerHTML = html_error_http;
		$m('ajax-temp').src = cross;
		if(detectWebKit){
        	remove($m('ajax-temp'));
        }else{
        	setTimeout(function(){ remove($m('ajax-temp'))}, 250);
        }
    }
	addEvent($m('ajax-temp'),"load", doUpload);
	form.setAttribute("target","ajax-temp");
	form.setAttribute("action",url_action);
	form.setAttribute("method","post");
	form.setAttribute("enctype","multipart/form-data");
	form.setAttribute("encoding","multipart/form-data");
	if(html_show_loading.length > 0){
                document.getElementById("imguplod").value="upload";
		$m(id_element).innerHTML = "<div class='imagelod_listtitle'><img src='"+baseurl+"/images/loading.gif' class='lodinf_uplod'/></div>";
	}
	form.submit();
}
