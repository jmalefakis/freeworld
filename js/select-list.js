(function($) {
    $.fn.selectList = function(selector) {
        var ul = $(this);
        var liSelected = null;
        
        // mouse support
        ul.attacheSelectOnClick = function() {
            ul.find(selector).each(function(ind, el) {
                if (!$(el).data('selectable-on-click')) {                
                    $(el).click(function(e) {
                        ul.find(selector).removeClass('selected').blur();
                        liSelected = $(this);
                        liSelected.addClass('selected').focus();
                    });
                    $(el).data('selectable-on-click', true);
                }
            });
        };
        
        // TODO: optional
        ul.attacheSelectOnClick();
        ul.click(function(e) {
            ul.attacheSelectOnClick();
        });
        
        // one item should be selected
        if (ul.find(selector).find('.selected').length == 0) {
            liSelected = ul.find(selector).eq(0);
            liSelected.addClass('selected').focus();
        }
        
        // keyboard handling
        $(window).keydown(function(e) {
            if (ul.data('selectListDisabled') === true) { return; }
            
            if (e.which === 40 || e.which === 13) {
                if (e.which !== 13) {
                    e.preventDefault();
                } else {
                    if (ul.find(e.target).length == 0) {
                        return;
                    }
                }
                
                if (liSelected) {
                    var idx = ul.find(selector).index(liSelected);
                    liSelected.removeClass('selected').blur();
                    next = ul.find(selector).eq(idx + 1);
                    if (next.length > 0) {
                        liSelected = next.addClass('selected').focus();
                    } else {
                        liSelected = ul.find(selector).eq(0).addClass('selected').focus();
                    }
                } else {
                    liSelected = ul.find(selector).eq(0).addClass('selected').focus();
                }
            } else if (e.which === 38) {
                e.preventDefault();
                if (liSelected) {
                    var idx = ul.find(selector).index(liSelected);
                    liSelected.removeClass('selected').blur();
                    next = ul.find(selector).eq(idx - 1);
                    if (next.length > 0) {
                        liSelected = next.addClass('selected').focus();
                    } else {
                        liSelected = ul.find(selector).last().addClass('selected').focus();
                    }
                } else {
                    liSelected = ul.find(selector).last().addClass('selected').focus();
                }
            }
        });
        
        $.fn.selectList.setClick = function(currItem) {
            ul.find(selector).each(function(ind, el) {            
                ul.find(selector).removeClass('selected');
                liSelected = currItem;
                liSelected.addClass('selected');
            });
        }
    }
    
})(jQuery);