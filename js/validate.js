function validate_register()
{ 
   
    var k=document.getElementById("User_firstname").value;
    var j=document.getElementById("User_lastname").value;  
    
    if(trim(k)==null || trim(k)=="")
        {  
            document.mform.User_firstname.focus();
            document.getElementById("temp").style.display="block";
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";
            document.getElementById("User_firstname_em_").style.display="block";
            document.getElementById("User_firstname_em_").innerHTML="Firstname cannot be blank";           
            return false;
        }
     else{
         document.getElementById("temp").style.display="none";
         
     }
     if(trim(j)==null || trim(j)=="")
        {  
            document.mform.User_firstname.focus();
            document.getElementById("temp").style.display="block";
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";
            document.getElementById("User_lastname_em_").style.display="block";
            document.getElementById("User_lastname_em_").innerHTML="Lastname cannot be blank";            
            return false;
        }
     else{
         document.getElementById("temp").style.display="none";         
     }
    
    var x=document.getElementById("User_email").value;
    var y=document.getElementById("conemail").value;  
    
    if(trim(x)==null || trim(x)=="")
        {  
            document.getElementById("temp").style.display="block";
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";
            document.getElementById("blankemail").style.display="block";
            document.getElementById("blankemail").innerHTML="Email can not be blank";
            document.getElementById("User_email").style.backgroundColor = "#FFEEEE";
            document.getElementById("User_email").style.borderColor = "#CC0000";
            document.getElementById("emailspan").style.color = "#CC0000";
            return false;
        }
     else{
         document.getElementById("temp").style.display="none";
         document.getElementById("blankemail").style.display="none";
     }
     
    if (x.toLowerCase()!=y.toLowerCase())
       { 
        document.mform.User_email.focus();
        document.getElementById("invalidemail").style.display="none";
        document.getElementById("temp").style.display="block";
        document.getElementById("q3").style.display="block";
        document.getElementById("q3").innerHTML="Please correct messages below";
        document.getElementById("blankemail").style.display="block";
        document.getElementById("blankemail").innerHTML="Email and re-enter email are different";
        document.getElementById("User_email").style.backgroundColor = "#FFEEEE";
        document.getElementById("User_email").style.borderColor = "#CC0000";
        document.getElementById("emailspan").style.color = "#CC0000";
        document.getElementById("conemail").style.backgroundColor = "#FFEEEE";
        document.getElementById("conemail").style.borderColor = "#CC0000";
        document.getElementById("conemailspan").style.color = "#CC0000";
        return false;
       }
    else
       {    
           document.getElementById("temp").style.display="none";
           document.getElementById("blankemail").style.display="none";
           document.getElementById("User_email").style.backgroundColor = "#E6EFC2";
           document.getElementById("User_email").style.borderColor = "#C6D880";
           document.getElementById("emailspan").style.color = "#666666";
           document.getElementById("conemail").style.backgroundColor = "#E6EFC2";
           document.getElementById("conemail").style.borderColor = "#C6D880";
           document.getElementById("conemailspan").style.color = "#666666";
       }
       
     
     
     if(trim(y)==null || trim(y)=="")
        {
            document.getElementById("temp").style.display="block";
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Re enter email cannot be blank";
            return false;
        }
     else{
         document.getElementById("temp").style.display="none";
     }            
       
  
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var par=document.getElementById("User_email").value; 
   if(reg.test(par) == false)
    { 
     document.getElementById("temp").style.display="block";
     document.getElementById("q3").style.display="block";
     document.getElementById("q3").innerHTML="Please correct messages below";
     document.getElementById("invalidemail").style.display="block";
     document.getElementById("emailexist").style.display="none";     
        
        document.getElementById("User_email").style.backgroundColor = "#FFEEEE";
        document.getElementById("User_email").style.borderColor = "#CC0000";
        document.getElementById("emailspan").style.color = "#CC0000";
        document.getElementById("conemail").style.backgroundColor = "#FFEEEE";
        document.getElementById("conemail").style.borderColor = "#CC0000";
        document.getElementById("conemailspan").style.color = "#CC0000";
     //document.getElementById("User_email").setAttribute("style","border:3px solid red");
     document.mform.User_email.focus();
     return false;
    }
    else
       { 
           document.getElementById("invalidemail").style.display="none";
           document.getElementById("temp").style.display="none";
       }
       
       var a=document.getElementById("User_password").value; 
       var b=document.getElementById("conpwd").value; 
    
  if(trim(a)==null || trim(a)=="")
        { 
            document.getElementById("temp").style.display="block";
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";
            document.getElementById("User_password_em_").style.display="block";
            document.getElementById("User_password_em_").innerHTML="Password cannot be blank";
            document.getElementById("User_password").style.backgroundColor = "#FFEEEE";
            document.getElementById("User_password").style.borderColor = "#CC0000";
            document.getElementById("passwordspan").style.color = "#CC0000";
            return false;
        }
     else{
         document.getElementById("temp").style.display="none";
         document.getElementById("User_password_em_").innerHTML="Password and re-enter password are different";
     }
     
     
    if (a!=b)
       { 
        document.mform.User_password.focus();
        document.getElementById("temp").style.display="block";
        document.getElementById("q3").style.display="block";
        document.getElementById("q3").innerHTML="Please correct messages below";
        document.getElementById("User_password_em_").style.display="block";
        document.getElementById("User_password_em_").innerHTML="Password and re-enter password are different";
        document.getElementById("User_password").style.backgroundColor = "#FFEEEE";
        document.getElementById("User_password").style.borderColor = "#CC0000";
        document.getElementById("passwordspan").style.color = "#CC0000";
        document.getElementById("conpwd").style.backgroundColor = "#FFEEEE";
        document.getElementById("conpwd").style.borderColor = "#CC0000";
        document.getElementById("repassworden").style.color = "#CC0000";
        
        return false;
       } 
       
       else
       {
           document.getElementById("q2").style.display="none";
       }    
     
     if(trim(b)==null || trim(b)=="")
        {
            document.getElementById("temp").style.display="block";
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Re enter password cannot be blank";
            return false;
        }
     else{
         document.getElementById("temp").style.display="none";
     }
       
    return true;
}

function editprofile()
{
    document.getElementById("lowerdiv").style.display="block";
    document.getElementById("uperdiv").style.display="none";
}

function addcolagain()
{ 
    var value = parseInt(document.getElementById('hidcol').value, 10);
    value = isNaN(value) ? 0 : value;
    
    var rating_div = $(".popup_rating_div").width();
    var new_width = parseInt(rating_div) + parseInt('157');
    $(".popup_rating_div").width(new_width);
    
    document.getElementById('removecol').style.display = "block";    
    
    if(value<10)
    { 
        value++;
        document.getElementById('hidcol').value = value; 
        
        var row = document.getElementById("myTable").rows.length;
        
        for(var i=0;i<row;i++){ 
        
        if(i==0)
         {
            var firstRow=document.getElementById("myTable").rows[i];
            var x=firstRow.insertCell(-1);
             x.innerHTML="<div class='titlete_textfild'> <input type='text' class='popup_table_input_background' size='21' name='"+i+value+"' id='"+i+value+"' value=''/></div>" 
         }
         else
             {
               var firstRow=document.getElementById("myTable").rows[i];
               var x=firstRow.insertCell(-1);
               x.innerHTML="<input type='text' class='popup_table_input' size='21' name='"+i+value+"' id='"+i+value+"' value=''/>"   
             }      
        
        }
        
        if(value == 10)
             {
                 document.getElementById('addcol').style.display = "none";
                 document.getElementById('lastcoladd').style.display = "block";                 
             }
    }
}

function addrowagain()
{
    var value = parseInt(document.getElementById('hid').value, 10);
    value = isNaN(value) ? 0 : value; 
    document.getElementById('removerow').style.display = "block";
   
   var baseurl = document.getElementById('hidden_baseurl').value ;
    
   
        value++;
        document.getElementById('hid').value = value;
        var table = document.getElementById("myTable");
        //alert(table.rows.length);
        var max = 0;
        for(var i=0;i<table.rows.length;i++) {
            if(max < table.rows[i].cells.length)
                max = table.rows[i].cells.length;
        }
   
    var row=table.insertRow(table.rows.length);     
    for(var i=0;i<parseInt(max);i++){
    var cell1=row.insertCell(i); 
    if(i==0)
     {
         cell1.innerHTML="<div class='listcreation_spancount'><div class='div_span_padding'><span id='span"+value+"' >"+value+".</span></div></div>"; 
     }
    else if(i==1)
      {  
           var k = i-1;           
          cell1.innerHTML="<ul class='popuplistcreation_ul'><li class='popuplistcreation_ul_li'><input type='text' value='Enter Selection.' onclick='if (this.defaultValue==this.value) this.value=\"\" '  onblur='if (this.value==\"\") this.value=this.defaultValue' class='popup_table_input' size='21' autocomplete='off' onkeyup='nameautofil("+'"'+baseurl+'"'+","+'"'+value+k+'"'+")'  name='value["+value+"]' id='input"+value+k+"'   value=''/><div id='div"+value+k+"'   class='listdiv'></div></li></ul>";              
      }
    else         
     {var j = i-1;         
         cell1.innerHTML="<input type='text' class='popup_table_input' size='21'  value='Enter 1-10 rating(10 highest).' onclick='if (this.defaultValue==this.value) this.value=\"\" '  onblur='if (this.value==\"\") this.value=this.defaultValue'   name='rating["+value+"]' id='input"+value+j+"' onkeyup='maximumvaluecheck("+'"'+value+j+'"'+")'  value=''/>"; 
     }
    }
}

function removerowagain()
{
    var value = parseInt(document.getElementById('hid').value, 10);
    value = isNaN(value) ? 0 : value;  
    
    if(value>0)
    {   
        document.getElementById('hid').value = value-1;      
        jQuery("#myTable tr:last").remove();
        if(value <=10)
             {
                 document.getElementById('addrow').style.display = "block";
                 document.getElementById('lastdivadd').style.display = "none";
                 if(value==2)
                  {
                    document.getElementById('removerow').style.display = "none";
                  }
             }
    }
   
}


function addrowedit_popup()
{
    var value = parseInt(document.getElementById('hid').value, 10);
    value = isNaN(value) ? 0 : value; 
    document.getElementById('removerow').style.display = "block";
   
   var baseurl = document.getElementById('hidden_baseurl').value ;
    
   
        value++;
        document.getElementById('hid').value = value;
        var table = document.getElementById("myTable");
        //alert(table.rows.length);
        var max = 0;
        for(var i=0;i<table.rows.length;i++) {
            if(max < table.rows[i].cells.length)
                max = table.rows[i].cells.length;
        }
   
    var row=table.insertRow(table.rows.length);     
    for(var i=0;i<parseInt(max);i++){
    var cell1=row.insertCell(i); 
    if(i==0)
     {
          var k = i;           
          cell1.innerHTML="<ul class='popuplistcreation_ul'><li class='popuplistcreation_ul_li'><input type='text' value='Enter Selection.' onclick='if(this.defaultValue==this.value) this.value=\"\" '  onblur='if(this.value==\"\")this.value=this.defaultValue' class='popup_table_input' size='21' autocomplete='off' onkeyup='nameautofiledit("+'"'+baseurl+'"'+","+'"'+value+k+'"'+")'  name='editvalue["+value+"]' id='editinput"+value+k+"'   value=''/><div id='editdiv"+value+k+"'   class='listdiv'></div></li></ul>";              
     }    
    else         
     {var j = i;         
         cell1.innerHTML="<input type='text' class='popup_table_input' size='21'  value='Enter 1-10 rating(10 highest).' onclick='if (this.defaultValue==this.value) this.value=\"\" '  onblur='if (this.value==\"\") this.value=this.defaultValue'   name='editrating["+value+"]' id='editinput"+value+j+"' onkeyup='maximumvaluecheckedit("+'"'+value+j+'"'+")'  value=''/>"; 
     }
    }
}


function nameautofiledit(baseurl,id)
{
    var color = document.getElementById("editinput"+id).style.backgroundColor;
         
         if(color == "rgb(255, 162, 162)")
             {
                 document.getElementById('editinput'+id).style.backgroundColor = "#E6EFC2";
                 document.getElementById('editinput'+id).style.border = "0px";
                 document.getElementById('editinput'+id).style.height = "30px";
                 document.getElementById('editinput'+id).style.width = "260px";
                 
             }
    document.getElementById('editinput'+id).style.backgroundColor = "#E6EFC2";
    document.getElementById('editinput'+id).style.borderColor = "#C6D880";
    
  var title  = document.getElementById("editinput"+id).value;  
  var listid = document.getElementById("listid").value; 
  
   var url = baseurl+'/index.php/preferences/nameautofil?listid='+listid
    jQuery.ajax({ 
        type:'POST',
        url:url,
        //dataType:'json',
        data:{
                title:title                  
             },
             success:function(value){                  
                 if(value!=1) 
                  {                      
                          jQuery("#editdiv"+id).html(value).show();                      
                  } 
                
                  
             
    }
    }); 
}


function maximumvaluecheckedit(id)
{
    var value = document.getElementById('editinput'+id).value;    
    var temp = isNaN(value);    
    if(temp==true)
        {
            document.getElementById('editinput'+id).style.backgroundColor = "#FFA2A2";
            document.getElementById('editinput'+id).style.border = "1px solid #CC0000";
            document.getElementById('editinput'+id).style.height = "28px";
            document.getElementById('editinput'+id).style.width = "258px";
            return false;
        }
    else
        {
           if(value>10)
               { 
                   document.getElementById('editinput'+id).style.backgroundColor = "#FFA2A2";
                   document.getElementById('editinput'+id).style.border = "1px solid #CC0000";
                   document.getElementById('editinput'+id).style.height = "28px";
                   document.getElementById('editinput'+id).style.width = "258px";
                   return false;
               }
               else
                   {
                       document.getElementById('editinput'+id).style.backgroundColor = "#E6EFC2";
                       document.getElementById('editinput'+id).style.border = "0px";
                       document.getElementById('editinput'+id).style.height = "30px";
                       document.getElementById('editinput'+id).style.width = "260px";
                       return true;
                   }
        }
}

function removecolagain()
{
    
    var value = document.getElementById('hidcol').value; 
    
    var rating_div = $(".popup_rating_div").width(); 
    if(parseInt(rating_div) > '313')
    {
        var new_width = parseInt(rating_div) - parseInt('157');
        $(".popup_rating_div").width(new_width);
        if(parseInt(rating_div) == '470')
        {
           $("#removecol").hide();     
        }
    }
    
    if(value>0)
        {   
            document.getElementById('hidcol').value = value-1; 
            
            $('#myTable td:last-child').remove();
            
            if(value <=10)
                 {
                     document.getElementById('addcol').style.display = "block";
                     document.getElementById('lastcoladd').style.display = "none";
                     if(value==1)
                      {
                        document.getElementById('removecol').style.display = "none";
                      }
                 }
                 
                 
            /*var table = document.getElementById("myTable");
            var max = 0;
                for(var i=0;i<table.rows.length;i++) {
                    if(max < table.rows[i].cells.length)
                        max = table.rows[i].cells.length;
                } 
                
            var temp = max-1;
            var tables;
            tables = document.getElementsByTagName('table');
             //alert(tables[0].rows.length);exit;
            for (var i = tables[0].rows.length - 1; i >= 0; i--) {
            tables[0].rows[i].deleteCell(temp);
            //alert(max)
              if(value <=10)
                 {
                     document.getElementById('addcol').style.display = "block";
                     document.getElementById('lastcoladd').style.display = "none";
                     if(value==1)
                      {
                        document.getElementById('removecol').style.display = "none";
                      }
                 }
             
        }*/
    }
}

function checkdatabaseregister(baseurl)
{
    var c=document.getElementById("User_email").value;  
    if(c!=''){
    var url = baseurl+"/index.php/user/checkdatabase";
    document.getElementById('loding').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        datatype:'json',
        data:{
                email:c                  
             },
             success:function(value){ 
                 document.getElementById('loding').style.display = "none";
                 if(value!=1) 
                  {                      
                    document.getElementById('emailexist').style.display = "block";                               
                  } 
                  else{
                      document.getElementById('emailexist').style.display = "none"; 
                  }
             
    }
    }); 
    return false;
    }
}

function checkdatabase(baseurl)
{
    
    var c=document.getElementById("title").value;  
    
    
    if(trim(c).length >50)
         {
             document.getElementById("valid").style.display='none'; 
             document.getElementById("invalid").style.display='none'; 
             document.getElementById("titlereq").style.display='block';   
             document.getElementById("titlereq").innerHTML="List title should be less than 50 characters";
             document.getElementById("title").focus();             
             document.getElementById("title").style.background = "#FFA2A2"; 
             document.getElementById("title").style.color = "black";
             return false;
         }
     else
         {       
             
             document.getElementById("seesuggestion").style.display='none';       
             document.getElementById("titlereq").style.display='none';       
             document.getElementById("valid").style.display='none';       
             document.getElementById("title").style.background = "#FFFFFF";
             document.getElementById("title").style.color = "#555555";
             document.getElementById("invalid").style.display='none';             
             document.getElementById("titlereq").innerHTML="This field is required";
           
         }
    
         
    if(c!=''){
    if(trim(c).length>0){ 
    var url = baseurl+"/index.php/preferences/checktitle";
    document.getElementById('loding').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        dataType:'json',
        data:{
                title:c                  
             },
             success:function(value){ 
                 document.getElementById('loding').style.display = "none";
                 if(value==1) 
                 {
                    document.getElementById('title').style.border     = '1px solid #CED2D8';  
                    document.getElementById('valid').style.display    = "block";
                    document.getElementById('invalid').style.display  = "none";  
                    document.getElementById('titlereq').style.display = "none"; 
                    document.getElementById("titlereq").innerHTML     =" This is not valid title";
                    $("#seesuggestion").hide();
                    document.getElementById("returnfalse").value= "valid";
                 }
                 else
                 {    
                     
                       document.getElementById('title').style.border     = '1px solid red';
                       document.getElementById('titlereq').style.display  = "block";
                       document.getElementById("titlereq").innerHTML      = "List with title '"+c+"'"+" already exists";
                       document.getElementById('valid').style.display    = "none";   
                       document.getElementById('invalid').style.display = "none"; 
                       $("#seesuggestion").show();
                       $("#seesuggestion").html('');
                       $("#seesuggestion").html(value.html);
                       document.getElementById("returnfalse").value= "False";
                       return false;
                 }
             
    }
    }); 
    
    }
    }
    else
        { 
         document.getElementById("title").value = "List title";  
         document.getElementById('valid').style.display = "none";   
        }
        
       if( $("#returnfalse").val()=='False')
       {
           return false;
       }
    
    
}

function checkDatabaseListTitle(that, baseurl) {
    var $that = $(that),
		$wrapper = $that.closest('.ctrl-wrapper').removeClass('ctrl-error').data('data-error-msg', ''),
		$error = $wrapper.closest('.ctrl-box').find('.ctrl-wrapper-error').hide(),
		c = $.trim($that.val());

	$wrapper.attr('data-valid-msg','');
    $wrapper.removeClass('ctrl-error');
    $that.data('error','0');
    
	if (c.length > 50) {
		$that.focus();
		$wrapper.addClass('ctrl-error').attr('data-error-msg','List title should be less than 50 characters');
		return false;
	} else if (c != '' && trim(c).length > 0) {
         if ($('input[name=participation]:checked', '.form-list').val() == 'My friends') { //!$('#participation').prop('checked')
            $wrapper.attr('data-valid-msg','This is valid title');
         } else {
             $.ajax({ 
                type: 'POST',
                url: baseurl + '/index.php/preferences/checkUniqTitle',
                data: {
                    title: trim(c)
                },
                success: function(resp) {
                    if (resp == 1) {
                        if ($('#radiobtn2').prop('checked')) {
                            var url = baseurl + '/index.php/preferences/checktitle';
                            $wrapper.addClass('ctrl-ajax-loading');
                            $.ajax({ 
                                type: 'POST',
                                url: url,
                                dataType: 'json',
                                data: {
                                    keyword: c.replace(/[^\s-\w]+|^[\w]{1,2}\s|\s[\w]{1,2}\s|\s[\w]{1,2}$/g, '').replace(/[\s]{2,}/g, ' ')
                                },
                                success: function(response) { 
                                    $wrapper.removeClass('ctrl-ajax-loading');
                                    if (response == 1) {
                                        $wrapper.attr('data-valid-msg','This is valid title');
                                    } else {
                                        $wrapper.addClass('ctrl-error');
                                        $error.html(response.html).show();
                                        return false;
                                    }
                                }
                            });
                        } else {
                            $wrapper.attr('data-valid-msg','This is valid title');
                        }
                    } else {
                         $wrapper.addClass('ctrl-error');   
                         $error.html('This title already exists').show();
                         $that.data('error','1');
                    }
                }
            });
         } 
	}
	return true;
}

function checkUniqTitle(baseurl, title) {
    var resp = 0;
    $.ajax({ 
        type: 'POST',
        url: baseurl + '/index.php/preferences/checkUniqTitle',
        data: {
            title: title
        },
        success: function(response) {
            resp = response;
        }
    });
    return resp;
}


function checkdatabasepoll(baseurl)
{
    var c=document.getElementById("title").value;  
    if(trim(c).length>0){ 
    var url = baseurl+"/index.php/opinion/checkpolltitle";
    document.getElementById('loding').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        dataType:'json',
        data:{
                title:c                  
             },
             success:function(value){ 
                 document.getElementById('loding').style.display = "none";
                 if(value==1) 
                 {
                    document.getElementById('title').style.border = 'none';  
                    document.getElementById('valid').style.display = "block";
                    document.getElementById('invalid').style.display = "none";  
                    document.getElementById('titlereq').style.display = "none"; 
                 }
                 else
                 {    
                       document.getElementById('title').style.border = '1px solid red';
                       document.getElementById('invalid').style.display = "block";
                       document.getElementById('valid').style.display = "none";   
                       document.getElementById('titlereq').style.display = "none"; 
                 }
             
    }
    }); 
    return false;
    }
    return true;
}


function trim( value )
{
    return LTrim(RTrim(value));
}
function LTrim( value )
{
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");	
}
function RTrim( value ) 
{	
    var re = /((\s*\S+)*)\s*/;
    return value.replace(re, "$1");	
}

function listtitle111111() { 
	var baseurl = $('#hidden_baseurl').val();
	var a = trim($('#title').val());	
	var c = trim($('#interest_topic').val());
	var y = trim($('#instruction').val());
		
	if ($('#returnfalse').val() == 'False') {
		//$('#title").focus();
		return false;
	}
	
	if ((a == '') || (a.length > 50)) {
		var msg = (a == '') ? 'This field is required' : 'List title should be less than 50 characters';
		$('#valid').hide();
		$('#titlereq').text(msg).show();			
		$('#title').focus().val('').css({background: '#FFA2A2', color: 'black'});
		return false;
	} else {			
		$('#title').css({background: '#FFF', color: '#555'});
		$('#titlereq').hide();		
		$('#invalid').hide();		
	}
	
	if ((trim(c) == '') || (c == 'select')) {			
		$('#spaninterest').show();			
		$('#interest_topic').focus();
		
		$('#div_typing').css('background', 'url(../../images/error_interestdp.png) no-repeat scroll right center white');	
		$('#interest_topic').css('color', 'black');
		return false;
	} else {
		$('#div_typing').css('background', 'url(../../images/interestdrpdwn.png) no-repeat scroll right center white'); 
		$('#interest_topic').css('color', '#555');
		$('#spaninterest').hide();		
	}
	/*картинка
	if(trim(f) == '')
		{			
			$('#image_upolad").style.display='block';			
			$('#file_uplod").focus();
			$('#file_uplod").style.background = "url(../../images/latest_img/browse2.png) no-repeat"; 
			return false;
		}
	else{
			
			$('#file_uplod").style.background = "url(../../images/latest_img/browse.png) no-repeat"; 
			$('#title").style.color = "#555555";
			$('#titlereq").style.display='none';		
			
	}
	*/
	if (y == '') {			
		$('#instructionreq').show();			
		$('#instruction').focus().css({background: '#FFA2A2', color: 'black'});
		return false;
	} else if (trim(y).length > 4000) {
		$('#instructionreq').hide();  
		$('#wordlimit').show();			
		$('#instruction').focus().css({background: '#FFA2A2', color: 'black'});
		return false;
	} else {
		$('#instruction').hide().css({background: '#FFF', color: '#555'});
		$('#instructionreq').hide();		
		$('#wordlimit').hide();	
	}
	
	var currenturl = $('#currenturl').val();
	if (currenturl == 'listtitle') {
		if ($('input[name=myfriends]:checked').length > 0) {
			$('#chekspante').css('backgroundColor', 'none');
			$('#chekspante_2').css('backgroundColor', 'none');
			$('#friendreq').hide();
		} else {
			$('#friendreq').show();
			$('#chekspante').css('backgroundColor', '#FFA2A2');
			$('#chekspante_2').css('backgroundColor', '#FFA2A2');
			return false;
		}
	} else {
		return true;
	}
			
	var title       = $('#title').val();	
	var interest    = $('#interest_topic').val();
	var instruction = $('#instruction').val();
	var myfriends   = $('#radiobtn1').prop('checked') ? 'My friends' : 'Everybody';
	
	$('#loding_listtitle').show();
	var url = baseurl + '/index.php/preferences/listtitle';
	jQuery.ajax({ 
		type: 'POST',
		url: url,		
		data: {
			title: title,
			interest: interest,
			instruction: instruction,
			myfriends: myfriends
		},
		success: function(value) { 
			$('#loding_listtitle').hide();
			if (value != 0 ) {	
				document.location.href = baseurl + '/index.php/preferences/listEntry?listid=' + value;
			} else {
				document.location.href= baseurl + '/index.php/preferences/listtitle';
			}
		}
	});	
}

function popuplistsaves()
{
    var value  = document.getElementById("input10").value;
     
     if(value=='Enter Selection.')
         {
             document.getElementById("input10").style.backgroundColor = "#FFA2A2";
             document.getElementById("input10").style.border = "1px solid #CC0000";
             document.getElementById("input10").style.height = "28px";
             document.getElementById("input10").style.width = "258px";
             return false;
             
         }
      else
        {
             document.getElementById("input10").style.backgroundColor = "#E6EFC2";
             document.getElementById("input10").style.borderColor = "#C6D880";
        }
        
        
        var value2  = document.getElementById("input11").value;
        
        if(value2=='Enter 1-10 rating(10 highest).')
         {
             document.getElementById("input11").style.backgroundColor = "#FFA2A2";
             document.getElementById("input11").style.border = "1px solid #CC0000";
             document.getElementById("input11").style.height = "28px";
             document.getElementById("input11").style.width = "258px";
             return false;
         }
      else
        {
             document.getElementById("input11").style.backgroundColor = "#E6EFC2";
             document.getElementById("input11").style.borderColor = "#C6D880";
        }
    $('#lodinggif').show(); 
     $.ajax({
            type: $('#popuplistsave').attr('method'),
            url:  $('#popuplistsave').attr('action'),
            data: $('#popuplistsave').serialize(),
            dataType: "json",
            success: function (data) { 
                $('#lodinggif').hide(); 
                var action = $('#popuplistsave').attr('action').split('preferences');
                var target = action[0]+"preferences/listdetail?listid="+data.listid;
                parent.$.fn.colorbox.close();
                self.parent.location=target;
            }
        });
      return false;
}

function update_list()
{
    document.getElementById('lodinggif').style.display = "block";
    $.ajax({
            type: $('#updatelist').attr('method'),
            url:  $('#updatelist').attr('action'),
            data: $('#updatelist').serialize(),
            dataType: "json",
            success: function (data) { 
                document.getElementById('lodinggif').style.display = "none";
                var action = $('#updatelist').attr('action').split('preferences');
                var target = action[0]+"preferences/listdetail?listid="+data.listid;               
                parent.$.fn.colorbox.close();
                self.parent.location=target;
            }
        });
      return false;
}

function suggestion(baseurl)
{
    var c=document.getElementById("title").value;  
    if(trim(c).length>0){ 
    var url = baseurl+"/index.php/preferences/suggestion";
    document.getElementById('loding').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        dataType:"json",
        data:{
                title:c                  
             },
             success:function(value){ 
                 if(value!=0)
                 {document.getElementById('loding').style.display = "none";
                     document.getElementById('seesuggestion').style.display = "block"; 
                 }
                 else
                     {
                         document.getElementById('seesuggestion').style.display = "none"; 
                     }
                
        }
    });     
    }
    return false;
}

function searchresult()
{
    var c=document.getElementById("searchres").value;
    if((trim(c)==null || trim(c)==""))
    {
         document.searchform.searchres.focus();
        return false;
    }
    else
    {
        return true;
    }
    
}

 function emailshares()
{ 
    var x = document.getElementById("Listemails").value;
     
    if(trim(x)==null || trim(x)=="")
        { 
             document.getElementById("Listemails").style.backgroundColor = "#FFA2A2";
             document.getElementById("Listemails").style.borderColor = "#CC0000";
        }
            
     else{       
               document.getElementById("Listemails").style.backgroundColor = "#E6EFC2";
               document.getElementById("Listemails").style.borderColor = "#C6D880";
         }
     
     
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/; 
   var par=document.getElementById("Listemails").value; 
   
   if(reg.test(par) == false)
    {  
             document.getElementById("Listemails").style.backgroundColor = "#FFA2A2";
             document.getElementById("Listemails").style.borderColor = "#CC0000";
             document.emailshare.Listemails.focus();
     return false;
    }
    else
       {  
             document.getElementById("Listemails").style.backgroundColor = "#E6EFC2";
             document.getElementById("Listemails").style.borderColor = "#C6D880";
       }
       return true;
}

function forgot_password(url)
{  
    var x = document.getElementById("User_email_forgot").value;
     
    if(x == 'Email')
        { 
            document.getElementById("User_email_forgot").focus();   
            document.getElementById("User_email_forgot").style.background ="none";            
            document.getElementById("User_email_forgot").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
            document.getElementById("User_email_forgot").style.borderColor = "#CC0000";    
            document.getElementById("User_email_forgot").style.color = "black"; 
            document.getElementById("User_email_forgot").value='';
            document.getElementById('flashmsg').style.display = "none";
            return false;
             
        }
            
     else{       
             document.getElementById("User_email_forgot").style.background = "url(../../newimages/email_bg.png)";  
             document.getElementById("User_email_forgot").style.color = "#565656";
         }
     
     
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/; 
   var par=document.getElementById("User_email_forgot").value; 
   
   if(reg.test(par) == false)
    {  
            document.getElementById("User_email_forgot").focus();   
            document.getElementById("User_email_forgot").style.background ="none";            
            document.getElementById("User_email_forgot").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
            document.getElementById("User_email_forgot").style.borderColor = "#CC0000";    
            document.getElementById("User_email_forgot").style.color = "black";             
            document.getElementById('flashmsg').style.display = "none";
             
     return false;
    }
    else
       {  
             document.getElementById("User_email_forgot").style.background = "url(../../newimages/email_bg.png)";  
             document.getElementById("User_email_forgot").style.color = "#565656";
       }
      
    document.getElementById('loding_forgot').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        dataType:"json",
        data:{
                email:x                  
             },
             success:function(value){ 
                 if(value!=0)
                 {  
                     document.getElementById('loding_forgot').style.display = "none";
                     $("#flsfhhidden").val("Show");
                     parent.$.fn.colorbox.close();    
                     document.getElementById('flashmsg').style.display = "block";                     
                     $("#flashmsg").removeClass("flash").addClass("flash"+value.id).text(value.result);  
                     
                     window.setTimeout(function() {
                     parent.$.fn.colorbox.close();
                     }, 2500);
                 }
                 
                
        }
    }); 
    
       
       
       return true;
}

function deletecrew(id,baseurl)
{
     var url = baseurl+"/index.php/user/removecrew";
 var r=confirm("Do you want to delete crew!");
 if (r==true)
  {  
      document.getElementById("loding"+id).style.display="block";
      jQuery.ajax({ 
        type:'POST',
        url:url,
        datatype:'json',
        data:{
                remove:id                 
             },
             success:function(value){ 
                 document.getElementById("loding"+id).style.display="none";
                 if(value==1) 
                  {
                      self.parent.location.reload(true); 
                  }
    }
    });
   return true;
  }
 else
  {
  alert("No crew is deleted");
  }
  return false;
}

function changepassword()
{
    var x=document.getElementById("oldpasword").value; 
    
    if(trim(x)==null || trim(x)=="")
        {                     
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";
            document.getElementById("spanoldpwd").innerHTML="Old password can not be blank";
            document.getElementById("oldpasword").style.backgroundColor = "#FFEEEE";
            document.getElementById("oldpasword").style.borderColor = "#CC0000";
            document.getElementById("oldspan").style.color = "#CC0000";
            return false;
        }
     else{
            document.getElementById("spanoldpwd").style.display="none";
            document.getElementById("q3").style.display="none";
            document.getElementById("oldpasword").style.backgroundColor = "#E6EFC2";
            document.getElementById("oldpasword").style.borderColor = "#C6D880";
            document.getElementById("oldspan").style.color = "#666666";
     }
    
    
    var a=document.getElementById("newpasword").value; 
    var b=document.getElementById("connewpasword").value; 
    
    if(trim(a)==null || trim(a)=="")
        { 
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";
            document.getElementById("spannewpwd").innerHTML="New password can not be blank";
            document.getElementById("newpasword").style.backgroundColor = "#FFEEEE";
            document.getElementById("newpasword").style.borderColor = "#CC0000";
            document.getElementById("newspan").style.color = "#CC0000";
            return false;
        }
     else{         
            document.getElementById("spannewpwd").innerHTML="";
            document.getElementById("q3").style.display="none";
            document.getElementById("newpasword").style.backgroundColor = "#E6EFC2";
            document.getElementById("newpasword").style.borderColor = "#C6D880";
            document.getElementById("newspan").style.color = "#666666";
     }
     
     if(trim(b)==null || trim(b)=="")
        {
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";
            document.getElementById("spanconnewpwd").innerHTML="Re-enter password can not be blank";
            document.getElementById("connewpasword").style.backgroundColor = "#FFEEEE";
            document.getElementById("connewpasword").style.borderColor = "#CC0000";
            document.getElementById("newrespan").style.color = "#CC0000";
            return false;
        }
     else{
            document.getElementById("spanconnewpwd").style.display="none";
            document.getElementById("q3").style.display="none";
            document.getElementById("connewpasword").style.backgroundColor = "#E6EFC2";
            document.getElementById("connewpasword").style.borderColor = "#C6D880";
            document.getElementById("newrespan").style.color = "#666666";
     }
     
    if (a!=b)
       { 
            document.mform.newpasword.focus();
            document.getElementById("q3").style.display="block";
            document.getElementById("q3").innerHTML="Please correct messages below";            
            document.getElementById("newpasword").style.backgroundColor = "#FFEEEE";
            document.getElementById("newpasword").style.borderColor = "#CC0000";
            document.getElementById("newspan").style.color = "#CC0000";
            document.getElementById("spannewpwd").innerHTML="Password and Re-enter password does not match";
            document.getElementById("connewpasword").style.backgroundColor = "#FFEEEE";
            document.getElementById("connewpasword").style.borderColor = "#CC0000";
            document.getElementById("newrespan").style.color = "#CC0000";
            return false;
       } 
       
       else
       {
            document.getElementById("spannewpwd").innerHTML="";
            document.getElementById("q3").style.display="none";
            document.getElementById("newpasword").style.backgroundColor = "#E6EFC2";
            document.getElementById("newpasword").style.borderColor = "#C6D880";
            document.getElementById("newspan").style.color = "#666666";           
            document.getElementById("connewpasword").style.backgroundColor = "#E6EFC2";
            document.getElementById("connewpasword").style.borderColor = "#C6D880";
            document.getElementById("newrespan").style.color = "#666666";
       }
      return true;
}

function changeclass(id)
{
    document.getElementById("background"+id).className ="notification_outer";
}
function changeclassagain(id)
{
    document.getElementById("background"+id).className ="notification_outer_white";
}
function leavegroup()
{
    var r=confirm("Do you want to leave group!");
    if (r==true)
    {  
        return true;
    }
    else
    {
        return false;
    }
}

function createpolls()
{
     var a=document.getElementById("question").value;
     var b=document.getElementById("image").value;
     var c=document.getElementById("listtype").value;
     var d=document.getElementById("interest").value;         
     
     if((trim(a)==null || trim(a)=="")) 
        { 
                document.getElementById('questionreq').style.display = "block";               
                document.listcreate.question.focus();
                return false;
        }
        else
        {
                document.getElementById('questionreq').style.display = "none";
        }     
        
    if((trim(c)==null || trim(c)=="" || c == 'select')) 
        { 
                document.getElementById('listtypereq').style.display = "block";
                document.listcreate.listtype.focus();
                return false;
        }
        else
        {
                document.getElementById('listtypereq').style.display = "none";
        }
        
        if((trim(d)==null || trim(d)=="" || d == 'select')) 
        {  
                document.getElementById('interestreq').style.display = "block";
                document.listcreate.interest.focus();
                return false;
        }
        else
        {
                document.getElementById('interestreq').style.display = "none";
        }
        
        if((trim(b)==null || trim(b)=="")) 
        { 
                document.getElementById('imgreq').style.display = "block";
                document.listcreate.image.focus();
                return false;
        }
        else
        {
                document.getElementById('imgreq').style.display = "none";
        }
        
        
     
     
    return true;
}

function addanswer()
{
    var value = parseInt(document.getElementById('hid').value, 10);
    value = isNaN(value) ? 0 : value;
    document.getElementById('removerow').style.display = "block";
    
    
    
        value++;
        document.getElementById('hid').value = value;
        var table = document.getElementById("myTables");
        //alert(table.rows.length);
        var max = 0;
        for(var i=0;i<table.rows.length;i++) {
            if(max < table.rows[i].cells.length)
                max = table.rows[i].cells.length;
        }
    
    var row=table.insertRow(table.rows.length);
    for(var i=0;i<max;i++){
    var cell1=row.insertCell(i);    
    cell1.innerHTML="<input type='text' style='height: 25px;' size='45' class='popup_table_input_opininon' value='Enter Selection.' onclick='if (this.defaultValue==this.value) this.value=\"\" '  onblur='if (this.value==\"\") this.value=this.defaultValue'  name='"+i+value+"' id='"+i+value+"'   value=''/>"; 
    }
      
   
}

function removeanswer()
{
    var value = parseInt(document.getElementById('hid').value, 10);
    value = isNaN(value) ? 0 : value;   
    
         document.getElementById('hid').value = value-1;      
         jQuery("#myTables tr:last").remove();
   
}
function seepollradio()
{
    document.getElementById('seepolsubmit').style.display = "block";
}

function seepollover(id)
{
    document.getElementById("seepoll"+id).className ="seepoll_radio_over";
}
function seepollout(id)
{
    document.getElementById("seepoll"+id).className ="seepoll_radio";
}
function radiochackedseepoll(id)
{
    document.getElementById("radio"+id).checked=true;
    document.getElementById('seepolsubmit').style.display = "block";
}

/**********Home page*****************/

function validate_register_home(baseurl)
{
    var onclick = document.getElementById("onclick").value;
    if (onclick == 'personalclick') {
        var k=document.getElementById("User_firstname").value;
        var j=document.getElementById("User_lastname").value;  

        if( k == 'First Name')
        { 
            document.getElementById("User_firstname").focus();
            document.getElementById("spanfirst").style.display='block';
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="First Name field is missing";
            document.getElementById("User_firstname").style.background ="none";
            document.getElementById("User_firstname").value ="";        
            document.getElementById("User_firstname").style.background = "url(../../newimages/input1_red.png) no-repeat"; 


            return false;
        } else {         

            document.getElementById("User_firstname").style.background = "url(../../newimages/input1.png) no-repeat";  
            document.getElementById("spanfirst").style.display='none';
            document.getElementById("errordiv_submit").style.display='none';
        }
        if(j == 'Last Name') {  

            document.getElementById("User_lastname").focus();
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("spanlast").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Last Name field is missing";
            document.getElementById("User_lastname").style.background ="none";
            document.getElementById("User_lastname").style.background = "url(../../newimages/input1_red.png) no-repeat"; 
            document.getElementById("User_lastname").value ="";  
            return false;
        } else{
            document.getElementById("User_lastname").style.background = "url(../../newimages/input1.png) no-repeat"; 
            document.getElementById("spanlast").style.display='none';
            document.getElementById("errordiv_submit").style.display='none';
        }

        if ($('input[name=gender]:checked').length > 0) {

            document.getElementById("chekspante").style.backgroundColor = "#fff";
            document.getElementById("chekspante_2").style.backgroundColor = "#fff";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("spangender").style.display='none';
        } else {
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("spangender").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please select the Gender ";
            document.getElementById("chekspante").style.backgroundColor = "#FFA2A2";
            document.getElementById("chekspante_2").style.backgroundColor = "#FFA2A2";
            return false;
        }


        var jj = document.getElementById("monthsid").value;

        var kk = document.getElementById("daysid").value;  
        var mm = document.getElementById("yearsid").value;

        if(jj == 'Month')
        {  
            document.getElementById("spanmonth").style.display='block';
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please select the Month ";
            document.getElementById("monthsid").style.background ="none";
            document.getElementById("monthsid").focus();
            document.getElementById("monthsid").style.background = "url(../../newimages/year_red.png) no-repeat";  
            document.getElementById("monthsid").style.borderRadius = "0";    
            document.getElementById("spanageunder").style.display = "none";   
            return false;
        } else{
            document.getElementById("monthsid").style.background = "url(../../newimages/year.png) no-repeat";  
            document.getElementById("spanmonth").style.display='none';
            document.getElementById("errordiv_submit").style.display='none';
        }

        if(kk == 'Day')
        {  
            document.getElementById("spanday").style.display='block';
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please select the Day ";
            document.getElementById("daysid").style.background ="none";
            document.getElementById("daysid").focus();
            document.getElementById("daysid").style.background = "url(../../newimages/year_red.png) no-repeat";  
            document.getElementById("daysid").style.borderRadius = "0";    
            document.getElementById("spanageunder").style.display = "none"; 
            return false;
        } else{
            document.getElementById("daysid").style.background = "url(../../newimages/year.png) no-repeat";  
            document.getElementById("spanday").style.display='none';
            document.getElementById("errordiv_submit").style.display='none';
        }


        if(mm == 'Year')
        {  
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Select the Year";
            document.getElementById("yearsid").style.background ="none";
            document.getElementById("yearsid").focus();
            document.getElementById('yearsid').value='';
            document.getElementById("yearsid").style.background = "url(../../newimages/input11_red.png) no-repeat";  

            return false;
        }
        else{
            document.getElementById("yearsid").style.background = "url(../../newimages/input11.png) no-repeat";  
            //document.getElementById("yearsid").style.color = "#A9A9A9";
            document.getElementById("errordiv_submit").style.display='none';

            var currentTime = new Date()
            var year = currentTime.getFullYear();

            var sarting  = 1898;
            var lastdate = year-13;
            var val = document.getElementById('yearsid').value;
            if (val=='')
            { 
                document.getElementById("spanageunder").style.display='none';        
                document.getElementById("yearsid").style.color = "#A9A9A9";
                document.getElementById('yearsid').value='Year';
                document.getElementById("yearsid").style.fontStyle = "italic"; 
            }
            else{
                if(sarting<val)
                {
                    //if(val<lastdate)
                    if(currentTime > new Date(parseInt(mm) + 13, document.getElementById("monthsid").selectedIndex - 1, parseInt(kk)))
                    {
                        document.getElementById("numaicspan").style.display='none';
                        document.getElementById("spanageunder").style.display='none'; 
                        document.getElementById("yearsid").style.background = "url(../../newimages/input11.png) no-repeat";                                              
                    }
                    else
                    {
                        document.getElementById("yearsid").style.background ="none";
                        document.getElementById("spanmonth").style.display ="none";
                        document.getElementById("spanday").style.display ="none";
                        document.getElementById("numaicspan").style.display='none';
                        document.getElementById("spanageunder").style.display='block';     
                        document.getElementById("yearsid").style.background = "url(../../newimages/input11_red.png) no-repeat";
                        jQuery("#yearsid").focus();
                        return false 
                    }

                }
                else
                {
                    document.getElementById("spanageunder").style.display='none';        
                    document.getElementById("yearsid").style.background ="none";
                    document.getElementById("numaicspan").style.display='block';
                    document.getElementById("numaicspan").innerHTML="Enter valid year";
                    document.getElementById("yearsid").style.background = "url(../../newimages/input11_red.png) no-repeat";  
                    //window.setTimeout(function (){
                    jQuery("#yearsid").focus();  
                    //},50)
                    return false 
                }
            }
        }


        var x=document.getElementById("User_email").value;

        var y=document.getElementById("conemail").value;  

        if(x == 'Email')
        {  
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Email field is missing"; 
            document.getElementById("spanemail").style.display='block';
            document.getElementById("spanemail").innerHTML="Email field is missing";    
            document.getElementById("User_email").style.background ="none";
            document.getElementById("User_email").focus();
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
            document.getElementById("User_email").value='';
            return false;
        }
        else{
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png) no-repeat";  
            //document.getElementById("User_email").style.color = "#A9A9A9";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("spanemail").style.display='none';
        }

        if (x.toLowerCase()!=y.toLowerCase())
        { 
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Your emails do not match";   
            document.getElementById("spanemail").style.display='block';
            document.getElementById("spanemail").innerHTML="Your emails do not match";    
            document.getElementById("conemail").style.background ="none";
            document.getElementById("User_email").style.background ="none";
            //document.getElementById("User_email").focus();
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
            document.getElementById("conemail").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
            return false;
        }
        else
        {    
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png) no-repeat";   
            document.getElementById("conemail").style.background = "url(../../newimages/email_bg.png) no-repeat";            
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("spanemail").style.display='none';
        }


        if(y == 'Verify Email')   
        {
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Verify Email field is missing";   
            document.getElementById("spanverify").style.display='block';
            document.getElementById("spanverify").innerHTML="Verify Email field is missing";  
            document.getElementById("conemail").focus();
            document.getElementById("conemail").value='';
            document.getElementById("conemail").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
            return false;
        }
        else{           
            document.getElementById("conemail").style.background = "url(../../newimages/email_bg.png) no-repeat"; 
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("spanverify").style.display='none';
        }            


        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var par=document.getElementById("User_email").value; 
        if(reg.test(par) == false)
        { 
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
            document.getElementById("spanemail").style.display='block';
            document.getElementById("spanemail").innerHTML="Invalid email";  
            document.getElementById("User_email").focus();
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Invalid email";   

            return false;
        }
        else
        { 
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png) no-repeat";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("spanemail").style.display='none';
        }

        var a=document.getElementById("User_password").value;        

        if(trim(a) == '')
        { 
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Password field is missing";  
            document.getElementById("spanpassword").style.display='block';
            document.getElementById("spanpassword").innerHTML="Password field is missing";  
            document.getElementById("User_password").style.background ="none";
            document.getElementById("User_password1").focus();
            document.getElementById("User_password").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
            document.getElementById("User_password").value=""; 
            return false;
        }
        else{
            document.getElementById("User_password").style.background = "url(../../newimages/email_bg.png) no-repeat";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("spanpassword").style.display='none';
        }

        var interest=document.getElementById("hiddeninterest").value; 

        if(trim(interest) == '')
        {
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Interests field is missing"; 
            document.getElementById("spaninterest").style.display='block';           
            document.getElementById("typings").focus();
			$('#div_typing').addClass('error');
			/*
            document.getElementById("div_typing").style.background = "url(../../newimages/email_bg_red.png) no-repeat";
            document.getElementById("div_typing").style.borderLeft='0'; 
            document.getElementById("div_typing").style.borderBottom='0'; 
            document.getElementById("div_typing").style.borderRight='0'; 
            document.getElementById("div_typing").style.borderRadius='0'; 
			*/
            document.getElementById("typings").value="";             

            return false;
        }
        else{
			$('#div_typing').removeClass('error');
			/*
            document.getElementById("div_typing").style.background = "url(../../newimages/email_bg_interest.png) no-repeat";
            document.getElementById("div_typing").style.borderLeft='2px solid #E0E0E0'; 
            document.getElementById("div_typing").style.borderBottom='1px solid #E0E0E0'; 
            document.getElementById("div_typing").style.borderRight='1px solid #E0E0E0'; 
            document.getElementById("div_typing").style.borderRadius='11px 0px 0px 11px'; 
            document.getElementById("div_typing").style.borderColor = "#E0E0E0";  
			*/
            document.getElementById("typings").style.background = "none";
            document.getElementById("spaninterest").style.display='none';       
            document.getElementById("errordiv_submit").style.display='none';
        }     

        var style = document.getElementById("detail_s").style.display; 
        if(style=='block')
        {             
            var country2 = document.getElementById("country2").value; 

            /*if(country2 == 'City, State/Province, Country')
                 {      alert('ffff');
                        document.getElementById("errordiv_submit").style.display='block';
                        document.getElementById("errordiv_submit").innerHTML="Please provide your location"; 
                        document.getElementById("country2").style.background ="none";
                        document.mform.country2.focus();
                        document.getElementById("country2").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
                        document.getElementById("country2").style.borderColor = "#CC0000";    
                        document.getElementById("country2").style.color = "black"; 
                        document.getElementById("country2").value='';
                        return false;
                 }*/
            if(country2 != 'City, State/Province, Country')
            {
                document.getElementById("country2").style.background = "url(../../newimages/email_bg.png) no-repeat";  
                document.getElementById("country2").style.color = "#A9A9A9";
                document.getElementById("errordiv_submit").style.display='none';

                var search = country2.search(",");
                if(search>0)
                {
                    document.getElementById("country2").style.background = "url(../../newimages/email_bg.png) no-repeat";  
                    document.getElementById("country2").style.color = "#A9A9A9";

                    var split    = country2.split(","); 
                    var city     = split[0];
                    var state    = split[1];
                    var country  = split[2];                                
                    var state1   = trim(state).toUpperCase();

                    var countryarray = new Array("Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", 
                            "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", 
                            "China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", 
                            "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", 
                            "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", 
                            "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", 
                            "Mexico", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", 
                            "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", 
                            "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", 
                            "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe");


                    var countryarray1 = $.map(countryarray, function(item, index) {
                        return item.toUpperCase();
                    });

                    var temp = $.inArray(state1, countryarray1);


                    if(temp>=0)
                    {
                        document.getElementById("cityerror").style.display ="none";
                        return true;

                    }


                    if(trim(city)=='')
                    {
                        document.getElementById("cityerror").style.display ="block";
                        document.getElementById("cityerror").innerHTML="Please provide location as City, State/Province, Country";
                        document.getElementById("country2").style.background ="none";                                             
                        document.getElementById("country2").style.background = "url(../../newimages/email_bg_red.png)"; 
                        return false;
                    }


                    var statearray = new Array("AL","Alabama", "AK" ,"Alaska", "AZ","Arizona", "AR","Arkansas", "CA","California", "CO","Colorado", "CT","Connecticut", "DE","Delaware", "DC","District Of Columbia", "FL","Florida", "GA","Georgia", "HI","Hawaii", "ID","Idaho", "IL","Illinois", "IN", "Indiana", "IA", "Iowa", "KS", "Kansas", "KY", "Kentucky", "LA", "Louisiana", "ME", "Maine", "MD", "Maryland", "MA", "Massachusetts", "MI", "Michigan", "MN", "Minnesota", "MS", "Mississippi", "MO", "Missouri", "MT", "Montana","NE", "Nebraska","NV", "Nevada","NH", "New Hampshire","NJ", "New Jersey","NM", "New Mexico","NY", "New York","NC", "North Carolina","ND", "North Dakota","OH", "Ohio", "OK", "Oklahoma", "OR", "Oregon", "PA", "Pennsylvania", "RI", "Rhode Island", "SC", "South Carolina", "SD", "South Dakota","TN", "Tennessee", "TX", "Texas", "UT", "Utah", "VT", "Vermont", "VA", "Virginia", "WA", "Washington", "WV", "West Virginia", "WI", "Wisconsin", "WY", "Wyoming" );
                    //var statearray = {AL: "Alabama", AK: "Alaska", AZ: "Arizona", AR: "Arkansas", CA: "California", CO: "Colorado", CT: "Connecticut", DE: "Delaware", DC: "District Of Columbia", FL: "Florida", GA: "Georgia", HI: "Hawaii", ID: "Idaho", IL: "Illinois", IN: "Indiana", IA: "Iowa", KS: "Kansas", KY: "Kentucky", LA: "Louisiana", ME: "Maine", MD: "Maryland", MA: "Massachusetts", MI: "Michigan", MN: "Minnesota", MS: "Mississippi", MO: "Missouri", MT: "Montana",NE: "Nebraska",NV: "Nevada",NH: "New Hampshire",NJ: "New Jersey",NM: "New Mexico",NY: "New York",NC: "North Carolina",ND: "North Dakota",OH: "Ohio", OK: "Oklahoma", OR: "Oregon", PA: "Pennsylvania", RI: "Rhode Island", SC: "South Carolina", SD: "South Dakota",TN: "Tennessee", TX: "Texas", UT: "Utah", VT: "Vermont", VA: "Virginia", WA: "Washington", WV: "West Virginia", WI: "Wisconsin", WY: "Wyoming" };

                    var statearray1 = $.map(statearray, function(item, index) {
                        return item.toUpperCase();
                    });


                    if(country==undefined ||country=='' || trim(country).length==0)
                    {
                        country = "USA";       
                    }

                    if(country == "USA")
                    {

                        var temp = $.inArray(state1, statearray1);
                        if(temp<0)
                        {
                            document.getElementById("cityerror").style.display ="block";
                            document.getElementById("cityerror").innerHTML="Please provide valid state";
                            document.getElementById("country2").style.background ="none";
                            document.getElementById("country2").focus();
                            document.getElementById("country2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  

                            return false;
                        }
                        else
                        {
                            document.getElementById("cityerror").style.display ="none";
                        }


                        document.getElementById("country2").value= city+","+state+", "+country;
                    }
                    document.getElementById("cityerror").style.display ="none";

                }
                else
                {
                    document.getElementById("cityerror").style.display ="block";
                    document.getElementById("cityerror").innerHTML="Please provide location as City, State/Province, Country";
                    document.getElementById("country2").style.background ="none";
                    document.getElementById("country2").focus();
                    document.getElementById("country2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
                    return false;
                }

            }
            else
            {
                document.getElementById("country2").style.background = "url(../../newimages/email_bg.png) no-repeat"; 
                document.getElementById("cityerror").style.display ="none";
            }


            var degree2 = document.getElementById("degree2").value;

            if(trim(degree2) != 'School, Degree, Year')
            {

                document.getElementById("errordiv_submit").style.display='none';
                document.getElementById("degree2").style.background = "url(../../newimages/email_bg.png) no-repeat";  
                document.getElementById("degree2").style.color = "#A9A9A9";                  


                var search = degree2.search(",");
                if(search>0)
                {
                    document.getElementById("degree2").style.background = "url(../../newimages/email_bg.png) no-repeat";  
                    document.getElementById("degree2").style.color = "#A9A9A9";

                    var split    = degree2.split(","); 
                    var school   = split[0];
                    var degree   = split[1];
                    var year     = split[2];            
                    var sarting  = 1898;

                    if(year>new Date().getFullYear())
                    {

                        document.getElementById("schoolerror").style.display ="block";
                        document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                        document.getElementById("degree2").style.background ="none";
                        document.getElementById("degree2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
                        return false;
                    }

                    if(trim(school) == '')
                    {
                        document.getElementById("schoolerror").style.display ="block";
                        document.getElementById("schoolerror").innerHTML="Please enter your School";
                        document.getElementById("degree2").focus();
                        document.getElementById("degree2").style.background ="none";
                        document.getElementById("degree2").style.background = "url(../../newimages/email_bg_red.png)";  
                        return false;
                    }

                    if(degree==undefined || trim(degree)=='')
                    {
                        document.getElementById("schoolerror").style.display ="block";
                        document.getElementById("schoolerror").innerHTML="Please enter your Degree";
                        document.getElementById("degree2").style.background ="none";
                        document.getElementById("degree2").focus();
                        document.getElementById("degree2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
                        return false;
                    }

                    if(year==undefined || trim(year)=='')
                    {
                        document.getElementById("schoolerror").style.display ="block";
                        document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                        document.getElementById("degree2").style.background ="none";
                        document.getElementById("degree2").focus();
                        document.getElementById("degree2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
                        return false;
                    }
                    else
                    {
                        var tempyear = isNaN(year);
                        if(tempyear == true){

                            document.getElementById("schoolerror").style.display ="block";
                            document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                            document.getElementById("degree2").style.background ="none";
                            //document.getElementById("degree2").focus();
                            document.getElementById("degree2").style.background = "url(../../newimages/email_bg_red.png)";  
                            return false;
                        }
                        else{
                            if(sarting>year)
                            { 

                                document.getElementById("schoolerror").style.display ="block";
                                document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                                document.getElementById("degree2").style.background ="none";
                                //document.getElementById("degree2").focus();
                                document.getElementById("degree2").style.background = "url(../../newimages/email_bg_red.png)";  
                                return false;

                            }
                            else
                            { 
                                document.getElementById("schoolerror").style.display ="none";
                            }


                        }
                    }


                }
                else
                {
                    document.getElementById("schoolerror").style.display ="block";
                    document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                    document.getElementById("degree2").style.background ="none";
                    document.getElementById("degree2").focus();
                    document.getElementById("degree2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";                             
                    return false;
                }
            }
            else
            {
                document.getElementById("degree2").style.background = "url(../../newimages/email_bg.png) no-repeat"; 
                document.getElementById("schoolerror").style.display ="none";
            }




            var degreehidden = document.getElementById("addmore_div2").style.display;
            if(degreehidden == "block")
            { 
                var val = document.getElementById('hiddeneducation').value;
                if (val != 'School, Degree, Year')
                { 
                    var degree2 = document.getElementById("hiddeneducation").value;
                    if(trim(degree2) != 'School, Degree, Year')
                    {

                        document.getElementById("errordiv_submit").style.display='none';
                        document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg.png)";  
                        document.getElementById("hiddeneducation").style.color = "#A9A9A9";                  


                        var search = degree2.search(",");
                        if(search>0)
                        {
                            document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg.png)";  
                            document.getElementById("hiddeneducation").style.color = "#A9A9A9";

                            var split    = degree2.split(","); 
                            var school   = split[0];
                            var degree   = split[1];
                            var year     = split[2];            
                            var sarting  = 1898;

                            if(year>new Date().getFullYear())
                            {

                                document.getElementById("schoolerror").style.display ="block";
                                document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                                document.getElementById("hiddeneducation").focus();
                                document.getElementById("hiddeneducation").style.background ="none";                                       
                                document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg_red.png)";  
                                return false;
                            }

                            if(trim(school) == '')
                            {
                                document.getElementById("schoolerror").style.display ="block";
                                document.getElementById("schoolerror").innerHTML="Please enter your School";
                                document.getElementById("hiddeneducation").focus();
                                document.getElementById("hiddeneducation").style.background ="none";
                                document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg_red.png)";  
                                return false;
                            }

                            if(degree==undefined || trim(degree)=='')
                            {
                                document.getElementById("schoolerror").style.display ="block";
                                document.getElementById("schoolerror").innerHTML="Please enter your Degree";
                                document.getElementById("hiddeneducation").style.background ="none";
                                document.getElementById("hiddeneducation").focus();
                                document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg_red.png)";  
                                return false;
                            }

                            if(year==undefined || trim(year)=='')
                            {
                                document.getElementById("schoolerror").style.display ="block";
                                document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                                document.getElementById("hiddeneducation").style.background ="none";
                                //document.getElementById("degree2").focus();
                                document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg_red.png)";  
                                return false;
                            }
                            else
                            {
                                var tempyear = isNaN(year);
                                if(tempyear == true){

                                    document.getElementById("schoolerror").style.display ="block";
                                    document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                                    document.getElementById("hiddeneducation").style.background ="none";
                                    //document.getElementById("degree2").focus();
                                    document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg_red.png)";  
                                    return false;
                                }
                                else{
                                    if(sarting>year)
                                    { 

                                        document.getElementById("schoolerror").style.display ="block";
                                        document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                                        document.getElementById("hiddeneducation").style.background ="none";
                                        //document.getElementById("degree2").focus();
                                        document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg_red.png)";  
                                        return false;

                                    }
                                    else
                                    { 
                                        document.getElementById("schoolerror").style.display ="none";
                                    }


                                }
                            }


                        }
                        else
                        {
                            document.getElementById("schoolerror").style.display ="block";
                            document.getElementById("schoolerror").innerHTML="Please provide your information as School, Degree, Year. If you're still in school, please use the future graduation year.";
                            document.getElementById("hiddeneducation").style.background ="none";
                            //document.getElementById("degree2").focus();
                            document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg_red.png)";                             
                            return false;
                        }
                    }
                }
                else
                {
                    document.getElementById("hiddeneducation").style.background = "url(../../newimages/email_bg.png) no-repeat"; 
                    document.getElementById("hiddeneducation").style.color = "#565656";
                    document.getElementById("schoolerror").style.display ="none";
                    document.getElementById("hiddeneducation").style.fontStyle = "normal"; 
                }
            }


            var industry5 = document.getElementById("industry5").value; 
            /*if(trim(industry5) == 'Company/Industry')
                {
                    document.getElementById("errordiv_submit").style.display='block';
                    document.getElementById("errordiv_submit").innerHTML="Please describe your Company/Industry";  
                    document.mform.industry5.focus();
                    document.getElementById("industry5").value="";             
                    document.getElementById("industry5").style.background = "#FFA2A2"; 
                    document.getElementById("industry5").style.color = "black";
                    return false;
                }*/
            if(trim(industry5) != 'Company, Industry')
            { 
                document.getElementById("industry5").style.background = "url(../../newimages/email_bg.png) no-repeat ";  
                document.getElementById("industry5").style.color = "#A9A9A9"; 
                document.getElementById("errordiv_submit").style.display ="none";

                var search = industry5.search(",");
                if(search>0)
                {
                    document.getElementById("industry5").style.background = "url(../../newimages/email_bg.png) no-repeat";  
                    document.getElementById("industry5").style.color = "#A9A9A9";

                    var splits     = industry5.split(",");                                 
                    var icomp      = splits[0];
                    var industry   = splits[1];

                    if(trim(icomp)==undefined || trim(icomp)=='')
                    {
                        document.getElementById("industry_error").style.display ="block";
                        document.getElementById("industry_error").innerHTML="Please enter your Company, Industry";
                        document.getElementById("industry5").focus();
                        document.getElementById("industry5").style.background ="none";                                         
                        document.getElementById("industry5").style.background = "url(../../newimages/email_bg_red.png) no-repeat";                      
                        return false;
                    } 

                    if(trim(industry)==undefined || trim(industry)=='')
                    {
                        document.getElementById("industry_error").style.display ="block";
                        document.getElementById("industry_error").innerHTML="Please enter your Company, Industry";
                        document.getElementById("industry5").style.background ="none";
                        document.getElementById("industry5").focus();
                        document.getElementById("industry5").style.background = "url(../../newimages/email_bg_red.png) no-repeat ";                      
                        return false;
                    }                                

                    else
                    {
                        document.getElementById("industry_error").style.display ="none";
                    }


                }
                else
                {
                    document.getElementById("industry_error").style.display ="block";
                    document.getElementById("industry_error").innerHTML="Please describe your Company, Industry";
                    document.getElementById("industry5").style.background ="none";
                    document.getElementById("industry5").focus();
                    document.getElementById("industry5").style.background = "url(../../newimages/email_bg_red.png) no-repeat";                              
                    return false;
                }

            } 
            else
            {
                document.getElementById("industry5").style.background = "url(../../newimages/email_bg.png) no-repeat "; 
                document.getElementById("industry_error").style.display ="none";
            }


        }

        var checkbox = document.getElementById("checkbox").checked; 

        if(checkbox == false)
        {
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("spantermsservice").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please accept the Terms of Services and Privacy Policy";  
            document.getElementById("checkspan").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
            document.getElementById("checkspan").style.borderColor = "#CC0000";              
            return false;
        }
        else{
            document.getElementById("checkspan").style.backgroundColor = "#E6EFC2";
            document.getElementById("checkspan").style.borderColor = "#C6D880";
            document.getElementById("errordiv_submit").style.display ="none";
            document.getElementById("spantermsservice").style.display='none';
        }

        var capta=document.getElementById("recaptcha_response_field").value; 

        if(trim(capta)== null || trim(capta)== "")
        {  
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("spancapthae").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please fill the captcha correctly ";  
            document.getElementById("recaptcha_response_field").style.background = "#FFA2A2";
            document.getElementById("recaptcha_response_field").style.borderColor = "#CC0000"; 
            document.getElementById("spancapthae").style.display='block';

            return false;
        }
        else{
            document.getElementById("recaptcha_response_field").style.backgroundColor = "#E6EFC2";
            document.getElementById("recaptcha_response_field").style.borderColor = "#C6D880";
            document.getElementById("errordiv_submit").style.display ="none";
            document.getElementById("spancapthae").style.display='none';
        }

        var error = document.getElementById("errordiv_submit_email").style.display;
        if(error== 'block')
        {
            return false;
        }

        var firstname      = document.getElementById("User_firstname").value;
        var lastname       = document.getElementById("User_lastname").value;      
        var month          = document.getElementById("monthsid").value;    
        var day            = document.getElementById("daysid").value;  
        var years          = document.getElementById("yearsid").value; 
        var email          = document.getElementById("User_email").value;    
        var password       = document.getElementById("User_password").value;  
        var interests      = document.getElementById("hiddeninterest").value; 
        var address        = document.getElementById("country2").value; 
        var education      = document.getElementById("degree2").value;
        var education_more = document.getElementById("hiddeneducation").value;
        var company        = document.getElementById("industry5").value; 
        var captaha        = document.getElementById("recaptcha_response_field").value;
        var challengeField = $("input#recaptcha_challenge_field").val();
        var test = document.getElementById('radiobtn1').checked;     
        if(test==true)
        {
            var gender  = 'female';
        }
        else
        {
            gender  = 'male';
        }
        var user = 'user';
        var url = baseurl+"/index.php/user/home";
        document.getElementById('submit_lod').style.display = "block";

        jQuery.ajax({ 
            type:'POST',
            url:url,
            dataType:"json",
            data:{
                User:user,  
                email:email,
                password:password,
                birthmonth:month,  
                birthday:day,
                birthyear:years,
                firstname:firstname,  
                lastname:lastname,
                gender:gender,
                occupation:company,  
                education:education,
                education_more:education_more,
                recaptcha_response_field:captaha,
                interest: interests,
                city_state:address,
                recaptcha_challeng_e_field:challengeField
            },
            success:function(value){ 
                document.getElementById('submit_lod').style.display = "none";
                if(value == 'save')
                {                    
                    jQuery('.resettext').val('');
                    jQuery('input[type=checkbox]').attr('checked', false);
                    jQuery('input[type=radio]').attr('checked', false); 
                    jQuery('#recaptcha_response_field').val('');
                    document.getElementById("mainspan").innerHTML= '';

                    document.getElementById('User_password').setAttribute('type', 'text');
                    document.getElementById("User_firstname").value = 'First Name';
                    document.getElementById("User_lastname").value = 'Last Name'; 
                    document.getElementById("yearsid").value = 'Year'; 
                    document.getElementById("User_email").value = 'Email'; 
                    document.getElementById("conemail").value = 'Verify Email';
                    document.getElementById("User_password").value = 'Password'; 
                    document.getElementById("typings").value = 'Click the arrow'; 
                    document.getElementById("country2").value = 'City, State/Province, Country';  
                    document.getElementById("degree2").value = 'School, Degree, Year'; 
                    document.getElementById("industry5").value = 'Company, Industry'; 
                    document.getElementById("response").innerHTML= "<img  src='"+baseurl+"/images/default.jpg'>";

                    $("#errordiv_submit").hide();
                    $("#registersuc").show();

                    // bug #3656
                    var i = window.location.hostname.indexOf('dev.');
                    if (i >= 0) {
                        window.location = 'http://' + window.location.hostname + baseurl + "/index.php/user/myDashboard";
                    } else {
                        window.location = 'https://' + window.location.hostname + baseurl + "/index.php/user/myDashboard";
                    }
                }
                else if(value=='Captacha')
                {
                    document.getElementById("errordiv_submit").style.display='block';
                    document.getElementById("spancapthae").style.display='block';
                    document.getElementById("errordiv_submit").innerHTML="Please fill the captcha correctly ";  
                }
                else
                {
                    document.getElementById("spancapthae").style.display='block';
                    document.getElementById("errordiv_submit").innerHTML="Please fill all the field";  
                }



            }
        }); 

    }
    else{
        var company_name = document.getElementById("company_name").value; 

        if(trim(company_name) == 'Company Name')
        {  
            document.getElementById("company_name").value='';
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Company Name field is missing ";  
            document.getElementById("company_name").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
            document.getElementById("spancompanynm").style.display='block';
            document.getElementById("company_name").style.color = "#565656";     
            document.getElementById("company_name").focus(); 
            return false;
        }
        else{ 
            document.getElementById("company_name").style.background = "url(../../newimages/email_bg.png) no-repeat ";  
            document.getElementById("spancompanynm").style.display='none';
            document.getElementById("errordiv_submit").style.display ="none";
        }

        var company_email    = document.getElementById("company_email").value;

        var company_conemail = document.getElementById("company_conemail").value;  

        if(company_email == 'Email')
        {  
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Email field is missing"; 
            document.getElementById("span_com_email").style.display='block';
            document.getElementById("span_com_email").innerHTML="Email field is missing";  
            document.getElementById("company_email").style.background ="none";
            document.getElementById("company_email").focus();
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat ";   
            document.getElementById("company_email").style.color = "#565656"; 
            document.getElementById("company_email").value='';
            return false;
        }
        else{
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg.png) no-repeat ";  
            //document.getElementById("User_email").style.color = "#A9A9A9";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("span_com_email").style.display='none';
        }

        if (company_email!=company_conemail)
        { 
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Your emails do not match"; 
            document.getElementById("span_com_email").style.display='block';
            document.getElementById("span_com_email").innerHTML="Your emails do not match"; 
            document.getElementById("company_conemail").style.background ="none";
            document.getElementById("company_email").style.background ="none";
            //document.getElementById("company_email").focus();
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat ";  
            document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg_red.png) no-repeat ";  
            document.getElementById("company_email").style.color = "#565656"; 
            document.getElementById("company_conemail").style.color = "#565656"; 
            return false;
        }
        else
        {    
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg.png) no-repeat ";   
            document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg.png) no-repeat "; 
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("span_com_email").style.display='none';
        }


        if(company_conemail == 'Verify Email')   
        {
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Verify Email field is missing"; 
            document.getElementById("span_com_verifyemail").style.display='block';
            document.getElementById("span_com_verifyemail").innerHTML="Verify Email field is missing";
            document.getElementById("company_conemail").focus();
            document.getElementById("company_conemail").value='';
            document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg_red.png) no-repeat ";  
            document.getElementById("company_conemail").style.color = "#565656"; 
            return false;
        }
        else{           
            document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg.png) no-repeat "; 
            // document.getElementById("conemail").style.color = "#A9A9A9";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("span_com_verifyemail").style.display='none';
        }            


        var reg1 = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var parcompany_email =document.getElementById("company_email").value; 
        if(reg1.test(parcompany_email) == false)
        { 
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat ";   
            document.getElementById("company_email").style.color = "#565656"; 
            document.getElementById("company_email").focus();
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Invalid email"; 
            document.getElementById("span_com_email").style.display='block';
            document.getElementById("span_com_email").innerHTML="Invalid email"; 

            return false;
        }
        else
        { 
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg.png) no-repeat ";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("span_com_email").style.display='none';
        }

        var company_password=document.getElementById("company_password").value;        

        if(company_password == 'Password')
        { 
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Password field is missing";
            document.getElementById("span_com_password").style.display='block';
            document.getElementById("span_com_password").innerHTML="Password field is missing"; 
            document.getElementById("company_password").style.background ="none";
            document.getElementById("company_password").focus();
            document.getElementById("company_password").style.background = "url(../../newimages/email_bg_red.png) no-repeat";    
            document.getElementById("company_password").style.color = "#565656"; 
            document.getElementById("company_password").value=""; 
            return false;
        }
        else{
            document.getElementById("company_password").style.background = "url(../../newimages/email_bg.png) no-repeat";
            document.getElementById("errordiv_submit").style.display='none';
            document.getElementById("span_com_password").style.display='none';
        }

        var companytypings = document.getElementById("hiddeninterest_company").value; 

        if(trim(companytypings) == '')
        {
            document.getElementById("spaninterest_com").style.display='block';
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Interest field is missing";  
            document.getElementById("companytypings").focus();
            document.getElementById("companytypings").value="";             
			$('#div_companytypings').addClass('error');
			/*
            document.getElementById("div_companytypings").style.background = "url(../../newimages/email_bg_red.png) no-repeat";
            document.getElementById("div_companytypings").style.borderLeft='0'; 
            document.getElementById("div_companytypings").style.borderBottom='0'; 
            document.getElementById("div_companytypings").style.borderRight='0'; 
            document.getElementById("div_companytypings").style.borderRadius='0'
			*/
            document.getElementById("companytypings").style.color = "#565656";

            return false;
        }
        else{
            document.getElementById("spaninterest_com").style.display='none';
			
			$('#div_companytypings').removeClass('error');
			/*
            document.getElementById("div_companytypings").style.background = "url(../../newimages/email_bg_interest.png) no-repeat";
            document.getElementById("div_companytypings").style.borderLeft='2px solid #E0E0E0'; 
            document.getElementById("div_companytypings").style.borderBottom='1px solid #E0E0E0'; 
            document.getElementById("div_companytypings").style.borderRight='1px solid #E0E0E0'; 
            document.getElementById("div_companytypings").style.borderRadius='11px 0px 0px 11px'; 
			*/
            document.getElementById("companytypings").style.background = "none";
            //document.getElementById("div_typing").style.borderColor = "#E0E0E0";  
            //document.getElementById("div_typing").style.color = "#A9A9A9"; 
            document.getElementById("errordiv_submit").style.display='none';
        }     

        var images_com = document.getElementById("images_com").value; 

        if(trim(images_com)=='')
        {
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please upload image";
            document.getElementById("imageuplodid").innerHTML='Please upload image'; 
            document.getElementById("imageuplodid").style.display='block';                   
            document.getElementById("images_com").focus();
            return false;
        }

        else
        {
            document.getElementById("file_uplod_com").style.color = "#565656";
            document.getElementById("imageuplodid").style.display='none';
            document.getElementById("errordiv_submit").style.display='none';
        }


        var Street = document.getElementById("companyStreet").value;      

        if(trim(Street)=='Street address')
        {
            document.getElementById("companyStreet").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
            document.getElementById("companyStreet").style.fontStyle = "italic"; 
            document.getElementById("companyStreet").value = '';
            document.getElementById("companyStreet").focus();  
            document.getElementById("companyStreet").style.color = "#a9a9a9";
            document.getElementById("companystreeterror").style.display = "block"; 
            document.getElementById("companystreeterror").innerHTML="Please describe your Street address";
            return false;
        }
        else
        {
            document.getElementById("companyStreet").style.fontStyle = "normal"; 
            document.getElementById("companyStreet").style.color = "#565656";
            document.getElementById("companystreeterror").style.display = "none"; 
            document.getElementById("companyStreet").style.background = "none"; 
            document.getElementById("companyStreet").style.background = "url(../../newimages/email_bg.png) no-repeat"; 
        }



        var companycountry2 = document.getElementById("companycountry2").value; 

        if(companycountry2 == 'City, State/Province, Country')
        {      
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please provide location as City, State/Province, Country";                            
            document.getElementById("companycountry2").focus();
            document.getElementById("companycountry2").value='';
            document.getElementById("companycountry2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";
            document.getElementById("companycityerror").style.display='block';
            document.getElementById("companycityerror").innerHTML="Please provide location as City, State/Province, Country";
            return false;
        }

        if(companycountry2 != 'City, State/Province, Country')
        {
            document.getElementById("companycountry2").style.background = "url(../../newimages/email_bg.png) no-repeat ";  
            document.getElementById("companycountry2").style.color = "#A9A9A9";
            document.getElementById("errordiv_submit").style.display='none';

            var search = companycountry2.search(",");
            if(search>0)
            {
                document.getElementById("companycountry2").style.background = "url(../../newimages/email_bg.png) no-repeat ";  
                document.getElementById("companycountry2").style.color = "#A9A9A9";

                var split    = companycountry2.split(","); 
                var city     = split[0];
                var state    = split[1];
                var country  = split[2];                                
                var state1   = trim(state).toUpperCase();

                var state1   = trim(state).toUpperCase();

                var countryarray = new Array("Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", 
                        "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", 
                        "China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", 
                        "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", 
                        "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", 
                        "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", 
                        "Mexico", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", 
                        "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", 
                        "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", 
                        "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe");


                var countryarray1 = $.map(countryarray, function(item, index) {
                    return item.toUpperCase();
                });

                var temp = $.inArray(state1, countryarray1);


                if(temp>=0)
                {
                    document.getElementById("companycityerror").style.display ="none";
                    return true;

                }


                if(trim(city)=='')
                {
                    document.getElementById("companycityerror").style.display ="block";
                    document.getElementById("companycityerror").innerHTML="Please provide location as City, State/Province, Country";
                    document.getElementById("companycountry2").style.background ="none";                                             
                    document.getElementById("companycountry2").style.background = "url(../../newimages/email_bg_red.png)"; 
                    return false;
                }


                var statearray = new Array("AL","Alabama", "AK" ,"Alaska", "AZ","Arizona", "AR","Arkansas", "CA","California", "CO","Colorado", "CT","Connecticut", "DE","Delaware", "DC","District Of Columbia", "FL","Florida", "GA","Georgia", "HI","Hawaii", "ID","Idaho", "IL","Illinois", "IN", "Indiana", "IA", "Iowa", "KS", "Kansas", "KY", "Kentucky", "LA", "Louisiana", "ME", "Maine", "MD", "Maryland", "MA", "Massachusetts", "MI", "Michigan", "MN", "Minnesota", "MS", "Mississippi", "MO", "Missouri", "MT", "Montana","NE", "Nebraska","NV", "Nevada","NH", "New Hampshire","NJ", "New Jersey","NM", "New Mexico","NY", "New York","NC", "North Carolina","ND", "North Dakota","OH", "Ohio", "OK", "Oklahoma", "OR", "Oregon", "PA", "Pennsylvania", "RI", "Rhode Island", "SC", "South Carolina", "SD", "South Dakota","TN", "Tennessee", "TX", "Texas", "UT", "Utah", "VT", "Vermont", "VA", "Virginia", "WA", "Washington", "WV", "West Virginia", "WI", "Wisconsin", "WY", "Wyoming" );
                //var statearray = {AL: "Alabama", AK: "Alaska", AZ: "Arizona", AR: "Arkansas", CA: "California", CO: "Colorado", CT: "Connecticut", DE: "Delaware", DC: "District Of Columbia", FL: "Florida", GA: "Georgia", HI: "Hawaii", ID: "Idaho", IL: "Illinois", IN: "Indiana", IA: "Iowa", KS: "Kansas", KY: "Kentucky", LA: "Louisiana", ME: "Maine", MD: "Maryland", MA: "Massachusetts", MI: "Michigan", MN: "Minnesota", MS: "Mississippi", MO: "Missouri", MT: "Montana",NE: "Nebraska",NV: "Nevada",NH: "New Hampshire",NJ: "New Jersey",NM: "New Mexico",NY: "New York",NC: "North Carolina",ND: "North Dakota",OH: "Ohio", OK: "Oklahoma", OR: "Oregon", PA: "Pennsylvania", RI: "Rhode Island", SC: "South Carolina", SD: "South Dakota",TN: "Tennessee", TX: "Texas", UT: "Utah", VT: "Vermont", VA: "Virginia", WA: "Washington", WV: "West Virginia", WI: "Wisconsin", WY: "Wyoming" };

                var statearray1 = $.map(statearray, function(item, index) {
                    return item.toUpperCase();
                });


                if(country==undefined ||country=='' || trim(country).length==0)
                {
                    country = "USA";       
                }

                if(country == "USA")
                {

                    var temp = $.inArray(state1, statearray1);
                    if(temp<0)
                    {
                        document.getElementById("companycityerror").style.display ="block";
                        document.getElementById("companycityerror").innerHTML="Please provide valid state";
                        document.getElementById("companycountry2").style.background ="none";
                        document.getElementById("companycountry2").focus();
                        document.getElementById("companycountry2").style.background = "url(../../newimages/email_bg_red.png) no-repeat ";  
                        return false;
                    }
                    else
                    {
                        document.getElementById("companycityerror").style.display ="none";
                    }


                    document.getElementById("companycountry2").value= city+","+state+", "+country;
                }
                document.getElementById("companycityerror").style.display ="none";

            }
            else
            {
                document.getElementById("companycityerror").style.display ="block";
                document.getElementById("companycityerror").innerHTML="Please provide location as City, State/Province, Country";
                document.getElementById("companycountry2").style.background ="none";
                document.getElementById("companycountry2").focus();
                document.getElementById("companycountry2").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
                return false;
            }

        }
        else
        {
            document.getElementById("companycountry2").style.background = "url(../../newimages/email_bg.png) no-repeat "; 
            document.getElementById("companycityerror").style.display ="none";
        }

        var companyindustry5 = document.getElementById("companyindustry5").value; 
        if(trim(companyindustry5) == 'Industry')
        {
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please describe your Industry";  
            document.getElementById("companyindustry5").focus();
            document.getElementById("companyindustry5").value="";             
            document.getElementById("companyindustry5").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
            document.getElementById("companyindustry_error").style.display='block';
            document.getElementById("companyindustry_error").innerHTML="Please describe your Industry";  
            return false;
        }
        else
        {
            document.getElementById("errordiv_submit").style.display='none';                                    
            document.getElementById("companyindustry5").style.background = "url(../../newimages/email_bg.png) no-repeat"; 
            document.getElementById("companyindustry_error").style.display='none'; 
            document.getElementById("companyindustry5").style.fontStyle = "normal"; 
            document.getElementById("companyindustry5").style.color = "#565656"; 
        }


        var checkbox = document.getElementById("checkbox").checked; 

        if(checkbox == false)
        {
            document.getElementById("spantermsservice").style.display='block';
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please accept the Terms of Services and Privacy Policy";  
            document.getElementById("checkspan").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
            document.getElementById("checkspan").style.borderColor = "#CC0000";              
            return false;
        }
        else{
            document.getElementById("spantermsservice").style.display='none';
            document.getElementById("checkspan").style.backgroundColor = "#E6EFC2";
            document.getElementById("checkspan").style.borderColor = "#C6D880";
            document.getElementById("errordiv_submit").style.display ="none";
        }

        var capta=document.getElementById("recaptcha_response_field").value; 

        if(trim(capta)== null || trim(capta)== "")
        { 
            document.getElementById("spancapthae").style.display='block';
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Please fill the captcha correctly ";  
            document.getElementById("recaptcha_response_field").style.background = "#FFA2A2";
            document.getElementById("recaptcha_response_field").style.borderColor = "#CC0000";  

            return false;
        }
        else{
            document.getElementById("spancapthae").style.display='none';
            document.getElementById("recaptcha_response_field").style.backgroundColor = "#E6EFC2";
            document.getElementById("recaptcha_response_field").style.borderColor = "#C6D880";
            document.getElementById("errordiv_submit").style.display ="none";
        }

    /////////////////////////////}


    var compnyname     = document.getElementById("company_name").value;     
    var email          = document.getElementById("company_email").value;    
    var password       = document.getElementById("company_password").value;  
    var interests      = document.getElementById("hiddeninterest_company").value; 
    var address        = document.getElementById("companycountry2").value;      
    var company        = document.getElementById("companyindustry5").value; 
    var captaha        = document.getElementById("recaptcha_response_field").value;
    var Street         = document.getElementById("companyStreet").value;      
    var challengeField = $("input#recaptcha_challenge_field").val();


    var url = baseurl+"/index.php/user/home";
    document.getElementById('submit_lod').style.display = "block";

    jQuery.ajax({ 
        type:'POST',
        url:url,
        dataType:"json",
        data:{
            Company:compnyname,  
            email:email,
            password:password,                
            occupation:company, 
            recaptcha_response_field:captaha,
            interest: interests,
            city_state:address,
            street:Street,
            recaptcha_challeng_e_field:challengeField
        },
        success:function(value){ 
            document.getElementById('submit_lod').style.display = "none";
            if(value == 'save')
            {                       
                jQuery('.resettext_com').val('');
                jQuery('input[type=checkbox]').attr('checked', false);                      
                jQuery('#recaptcha_response_field').val('');
                document.getElementById("mainspan_com").innerHTML= '';

                document.getElementById('company_password').setAttribute('type', 'text');
                document.getElementById("company_name").value = 'Company Name';                     

                document.getElementById("company_email").value = 'Email'; 
                document.getElementById("company_conemail").value = 'Verify Email';
                document.getElementById("company_password").value = 'Password'; 
                document.getElementById("companytypings").value = 'Click the arrow'; 
                document.getElementById("companyStreet").value  = "Street address";
                document.getElementById("companycountry2").value = 'City, State/Province, Country';
                document.getElementById("companyindustry5").value = 'Industry'; 
                document.getElementById("response_com").innerHTML= "<img  src='"+baseurl+"/images/default.jpg'>";

                $("#errordiv_submit").hide();
                $("#registersuc").show();
                
                // bug #3656
                var i = window.location.hostname.indexOf('dev.');
                if (i >= 0) {
                    window.location = 'http://' + window.location.hostname + baseurl + "/index.php/user/myCreated";
                } else {
                    window.location = 'https://' + window.location.hostname + baseurl + "/index.php/user/myCreated";
                }
            }
            else if(value=='Captacha')
            {
                document.getElementById("errordiv_submit").style.display='block';
                document.getElementById("spancapthae").style.display='block';
                document.getElementById("errordiv_submit").innerHTML="Please fill the captcha correctly ";  
            }
            else
            {
                document.getElementById("spancapthae").style.display='block';
                document.getElementById("errordiv_submit").innerHTML="Please fill all the field";  
            }
        }
    });
	}
    return true;
}

function checkdatabaseregister_home(baseurl)
{
    
  var onclick = document.getElementById("onclick").value;
  if(onclick == 'personalclick'){
    var x=document.getElementById("User_email").value;    
    
   
        if(x == 'Email')
        {  
            //document.mform.User_email.focus();
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
            document.getElementById("spanemail").style.display = "block"; 
            document.getElementById("spanemail").innerHTML="Email field is missing";    
            return false;
        }
     else{
           document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png) no-repeat";  
           //document.getElementById("User_email").style.color = "#A9A9A9";
           document.getElementById("errordiv_submit").style.display='none';
           document.getElementById("spanemail").style.display = "none"; 
     } 
  
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var par=document.getElementById("User_email").value; 
   if(reg.test(par) == false)
    { 
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat";   
            //document.getElementById("User_email").focus();
            document.getElementById("spanemail").style.display='block';
            document.getElementById("spanemail").innerHTML="Invalid email";  
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Invalid email";   
           
            return false;
    }
    else
       { 
           document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png) no-repeat";
           //document.getElementById("User_email").style.color = "#A9A9A9"; 
           document.getElementById("errordiv_submit").style.display='none';
           document.getElementById("spanemail").style.display='none';
       }
       
    
    var c = document.getElementById("User_email").value;  
  }
  else{
      var x=document.getElementById("company_email").value;    
    
   
        if(x == 'Email')
        {  
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
            document.getElementById("span_com_email").style.display = "block"; 
            document.getElementById("span_com_email").innerHTML="Email field is missing";
            //document.mform.company_email.focus();
             
            return false;
        }
     else{
           document.getElementById("company_email").style.background = "url(../../newimages/email_bg.png) no-repeat";  
           //document.getElementById("User_email").style.color = "#A9A9A9";
           document.getElementById("errordiv_submit").style.display='none';
     } 
  
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var par=document.getElementById("company_email").value; 
   if(reg.test(par) == false)
    { 
            document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
            //document.getElementById("company_email").focus();
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Invalid email"; 
            document.getElementById("span_com_email").style.display='block';
            document.getElementById("span_com_email").innerHTML="Invalid email";             
            return false;
    }
    else
       { 
           document.getElementById("company_email").style.background = "url(../../newimages/email_bg.png) no-repeat";
           //document.getElementById("User_email").style.color = "#A9A9A9";    lodingcompany
           document.getElementById("errordiv_submit").style.display='none';
           document.getElementById("span_com_email").style.display='none';
       }
       
    
    var c = document.getElementById("company_email").value; 
  }
    
    if(c!=''){
    var url = baseurl+"/index.php/user/checkdatabase";
    var onclick = document.getElementById("onclick").value;
    if(onclick == 'personalclick'){
    document.getElementById('loding').style.display = "block";
    }
    else
        {
            document.getElementById('lodingcompany').style.display = "block";
        }
    jQuery.ajax({ 
        type:'POST',
        url:url,
        dataType:'json',
        data:{
                email:c                  
             },
             success:function(value){ 
                  var onclick = document.getElementById("onclick").value;
                  if(onclick == 'personalclick'){
                      document.getElementById('loding').style.display = "none";
                  }
                  else
                  {
                      document.getElementById('lodingcompany').style.display = "none";
                  }
                 if(value!=1) 
                  {                
                     document.getElementById("errordiv_submit_email").style.display='block';
                     document.getElementById("errordiv_submit_email").innerHTML=value;
                    
                     if(onclick == 'personalclick'){
                     
                     //document.getElementById("User_email").focus();
                     document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
                     document.getElementById("spanemail").style.display='block';
                     document.getElementById("spanemail").innerHTML=value;
                     }
                     else
                         {
                              
                             //document.getElementById("company_email").focus();
                             document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
                             document.getElementById("span_com_email").style.display='block';
                             document.getElementById("span_com_email").innerHTML=value;
                         }
                  } 
                 else{         
                      document.getElementById("errordiv_submit_email").style.display= "none";                       
                  }
             
    }
    }); 
    return false;
    }
    
    return true;
}

function veryifyconemail()
{
  
  var onclick = document.getElementById("onclick").value;
    if(onclick == 'personalclick'){
     var x=document.getElementById("User_email").value;
    
     var y=document.getElementById("conemail").value;  
    
    if(y == 'Verify Email')   
        {
            //document.mform.conemail.focus();
            document.getElementById("conemail").value='Verify Email';
            document.getElementById("conemail").style.background = "url(../../newimages/email_bg_red.png)";
            document.getElementById("spanverify").style.display = "block"; 
            document.getElementById("spanverify").innerHTML="Verify Email field is missing";
            return false;
        }
     else{           
           document.getElementById("conemail").style.background = "url(../../newimages/email_bg.png)"; 
           //document.getElementById("conemail").style.color = "#A9A9A9";
           document.getElementById("errordiv_submit").style.display='none'; 
           document.getElementById("spanverify").style.display = "none"; 
     } 
     
     if (x.toLowerCase()!=y.toLowerCase())
       {  
           
            //document.getElementById("User_email").focus();
            document.getElementById("errordiv_submit").style.display='block';
            document.getElementById("errordiv_submit").innerHTML="Your emails do not match"; 
            document.getElementById("spanemail").style.display='block';
            document.getElementById("spanemail").innerHTML="Your emails do not match";
            document.getElementById("conemail").style.background ="none";
            document.getElementById("User_email").style.background ="none";           
            document.getElementById("conemail").style.background = "url(../../newimages/email_bg_red.png)";         
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png)";         
            return false;
       }
    else
       {    
           document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png)";   
           document.getElementById("conemail").style.background = "url(../../newimages/email_bg.png)";            
           document.getElementById("errordiv_submit").style.display='none';
           document.getElementById("spanemail").style.display='none';
       }
       
       
       var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
       var par=document.getElementById("User_email").value; 
       if(reg.test(par) == false)
        { 
                document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png)";         
                //document.getElementById("User_email").focus();
                document.getElementById("spanemail").style.display='block';
                document.getElementById("spanemail").innerHTML="Invalid email";
                document.getElementById("errordiv_submit").style.display='block';
                document.getElementById("errordiv_submit").innerHTML="Invalid email";   

                return false;
        }
        else
           { 
               document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png)";
               document.getElementById("spanemail").style.display='none';
               document.getElementById("errordiv_submit").style.display='none';
           }
    }
    else
     {
             var x=document.getElementById("company_email").value;
    
             var y=document.getElementById("company_conemail").value;  

            if(y == 'Verify Email')   
                {
                    //document.mform.company_conemail.focus();
                    document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg_red.png)";
                    document.getElementById("company_conemail").value='Verify Email';
                    document.getElementById("span_com_verifyemail").style.display = "block"; 
                    document.getElementById("span_com_verifyemail").innerHTML="Verify Email field is missing";
                    return false;
                }
             else{           
                   document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg.png)"; 
                   document.getElementById("errordiv_submit").style.display='none';
                   document.getElementById("span_com_verifyemail").style.display = "none"; 
             } 

             if (x.toLowerCase()!=y.toLowerCase())
               {  
        
                    //document.getElementById("company_conemail").focus();
                    document.getElementById("errordiv_submit").style.display='block';
                    document.getElementById("errordiv_submit").innerHTML="Your emails do not match";   
                    document.getElementById("span_com_email").style.display='block';
                    document.getElementById("span_com_email").innerHTML="Your emails do not match";   
                    document.getElementById("company_email").style.background ="none";
                    document.getElementById("company_email").style.background ="none";           
                    document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png)";   
                    document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg_red.png)";  
                    return false;
               }
            else
               {    
                   document.getElementById("company_email").style.background = "url(../../newimages/email_bg.png)";   
                   document.getElementById("company_conemail").style.background = "url(../../newimages/email_bg.png)"; 
                   document.getElementById("errordiv_submit").style.display='none';
                   document.getElementById("span_com_email").style.display='none';
               }


               var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
               var par=document.getElementById("company_email").value; 
               if(reg.test(par) == false)
                { 
                        document.getElementById("company_email").style.background = "url(../../newimages/email_bg_red.png)";   
                        document.getElementById("company_email").focus();
                        document.getElementById("span_com_email").style.display='block';
                        document.getElementById("span_com_email").innerHTML="Invalid email"; 
                        document.getElementById("errordiv_submit").style.display='block';
                        document.getElementById("errordiv_submit").innerHTML="Invalid email";   

                        return false;
                }
                else
                   { 
                       document.getElementById("company_email").style.background = "url(../../newimages/email_bg.png)";
                       document.getElementById("errordiv_submit").style.display='none';
                       document.getElementById("span_com_email").style.display='none';
                   }
     }
     return true;
}


/*function sortbydate()
{  
    
    $(".datepick-trigger").attr("src","../../images/freeworld/calarrow-1.png");   
    var value = document.getElementById("popupDatepicker").value;    
    
    if(value !='')
     {   
         document.getElementById("listcreate").submit();
     }
}*/



function sortbydate(com,pageno)
{
     
    var request_pending = true; 
    var value1          = document.getElementById("popupDatepicker").value;    
    
    
    if(value1 !='')
     {   
         if(com!='' && com!=2)
            {
                             jQuery("#value_calender").val(value1); 
                             document.getElementById('pageno_off').value = 0;
                             $(window).unbind();
                             var request_pending = false;
            }
         
         
         
         
         $(window).scroll(function() {
            if (request_pending) {
                return;
            }               
            if (at_bottom_of_page_old()) { 
               request_pending = false;
                var pageno         = document.getElementById('pageno_off').value;
                var maxpageid      = document.getElementById('maximaumpageno').value;
                                   
                      if(pageno <= maxpageid){                           
                        sortbydate(2,pageno);                        
                        }
                  
                   }
             }); 
             
             
               
                
                var pageno         = document.getElementById('pageno_off').value;
                var maxpageid      = document.getElementById('maximaumpageno').value;
                
                if(pageno <= maxpageid){

                    
                    document.getElementById('officialtodatlodingdiv').style.display = "block";

                        
                     
                     if(pageno==0)
                         {
                             jQuery("#contentdiv").html('');   
                         }
         
                         var baseurl = $("#baseurl").val();

                         var url = baseurl+"/index.php/preferences/officialDateSorter";

                         jQuery.ajax({ 
                            type:'POST',
                            url:url,
                            dataType:"json",
                            data:{
                                      value:value1 ,
                                      pageno:pageno
                                 },
                                 success:function(value){ 
                                      document.getElementById('officialtodatlodingdiv').style.display = "none";
                                         if(value.html!=1)
                                         {                             
                                            jQuery("#contentdiv").append(value.html);
                                            
                                            document.getElementById('pageno_off').value    = parseInt(value.pageno_s);
                                            document.getElementById('maximaumpageno').value = parseInt(value.maxpageid);
                                             request_pending = false;
                                                 //alert(value.pageno_s)
                                                     if(pageno == maxpageid){                                                         
                                                         var page =  document.getElementById('pageno_off').value;                                                         
                                                         var maxpagevalue = parseInt(page)+1;
                                                         document.getElementById('pageno_off').value = maxpagevalue;
                                                     }
                                         }
                                         else
                                          {

                                              var value2 = value1.split("-");
                                              var date2  = parseInt(value2[2]) - 1;
                                              value1     = value2[0]+"-"+value2[1]+"-"+date2


                                              var olddateid = new Array();


                                                 for(var kk = 1;kk<=7;kk++){
                                                 var myDate=new Date();
                                                  myDate.setDate(myDate.getDate()-kk);                                  


                                                    var finalyr1  = ""+myDate.getFullYear()+"";

                                                    var finalyr = finalyr1.replace(20,''); 


                                                    if(myDate.getMonth()>9)
                                                    {
                                                     var finalmo = parseInt(myDate.getMonth()) + 1;
                                                    }
                                                    else{                                        
                                                    var abc = parseInt(myDate.getMonth())+1
                                                    var finalmo  = "0"+abc;
                                                    }                                   


                                                    if(myDate.getDate()>9)
                                                    {
                                                        var finalda = myDate.getDate();
                                                    }
                                                    else{
                                                    var finalda = "0"+parseInt(myDate.getDate());
                                                    }

                                                  olddateid[kk] = finalyr+'-'+ finalmo+'-'+finalda;
                                                 } 

                                               var value2       = document.getElementById("popupDatepicker").value;  
                                               var index        = olddateid.indexOf(value2); 
                                              if(index+1<8){
                                                  document.getElementById("popupDatepicker").value = olddateid[index+1];
                                                  sortbydate('',pageno);
                                             }
                                             else
                                                 {
                                                     value = "<div class='nolistfound'>No lists found</div>";
                                                     jQuery("#contentdiv").html(value).show(); 
                                                 }

                                          }

                            }
                        }); 
        
                }
            
            
           
    
    
     }
}

function sortbydateworld(com,pageno)
{
    
    var value1  = document.getElementById("popupDatepicker").value;    
    
    if(value1 !='')
     {          
        
        if(com!='' && com!=2)
            {
               document.getElementById('pageno_off').value = 0;
            }
         
         $('#page').unbind();
         
         $('#page').bind('mousewheel', function(event, delta) {
            
           
               if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {                  
                  var scrollTop = "run";
               }
               
                var pageno         = document.getElementById('pageno_off').value;
                var maxpageid      = document.getElementById('maximaumpageno').value;
            
            
                 if(scrollTop){                

                        if(pageno <= maxpageid){
                        sortbydateworld(2,pageno);
                        }
                  scrollTop='';
                   }
             }); 
         
        var pageno         = document.getElementById('pageno_off').value;
        var maxpageid      = document.getElementById('maximaumpageno').value;
        
        if(pageno <= maxpageid){
            
        
        if(pageno==0)
        {
            jQuery("#contentdiv").html('');   
        }
        
        document.getElementById('officialtodatlodingdiv').style.display = "block";
        
              
         
         
         var baseurl = $("#baseurl").val();
         
         var url = baseurl+"/index.php/preferences/sortByDateWorld";
         
         jQuery.ajax({ 
            type:'POST',
            url:url,
            dataType:"json",
            data:{
                      value:value1 ,
                      pageno:pageno
                 },
                 success:function(value){ 
                      document.getElementById('officialtodatlodingdiv').style.display = "none";
                         if(value.html!=1)
                         {                             
                            jQuery("#contentdiv").append(value.html);
                            
                            document.getElementById('pageno_off').value    = parseInt(document.getElementById('pageno_off').value)+1;
                            document.getElementById('maximaumpageno').value = parseInt(value.maxpageid);
                         }
                 }
         })
       }
     
     }
                         
}

function sortbytopic()
{  
         document.getElementById("browsform").submit();
}  

function login_header(baseurl)
{
    var k=document.getElementById("LoginForm_email").value;
    var j=document.getElementById("LoginForm_password").value;  
    
    if(trim(k)==null || trim(k)=="" || trim(k)=="user name")
        { 
            document.getElementById("LoginForm_email").style.background = "url(../../newimages/input_login.png) no-repeat"; 
            document.getElementById("LoginForm_email").style.color = "black"; 
            document.getElementById("LoginForm_email").value  = ""; 
            document.getElementById("LoginForm_email").focus();  
            return false;
        }
     else{
           document.getElementById("LoginForm_email").style.background = "#FFFFFF"; 
           document.getElementById("LoginForm_email").style.borderColor = "#C6D880";
     }   
     
     var regular = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
       var par=document.getElementById("LoginForm_email").value; 
       if(regular.test(par) == false)
        { 
            document.getElementById("LoginForm_email").style.background = "url(../../newimages/input_login.png) no-repeat"; 
            document.getElementById("LoginForm_email").style.color = "black"; 
            document.getElementById("LoginForm_email").focus();  
            return false;
        }
        else
           { 
               document.getElementById("LoginForm_email").style.background = "#FFFFFF"; 
               document.getElementById("LoginForm_email").style.borderColor = "#C6D880";
           }
           
                 
     if(trim(j)==null || trim(j)=="" || trim(j)=="Password" )
        { 
            document.getElementById("LoginForm_password").style.background = "url(../../newimages/input_login.png) no-repeat"; 
            document.getElementById("LoginForm_password").style.color = "black";
            document.getElementById("LoginForm_password").value  = ""; 
            document.getElementById("LoginForm_password").focus();  
            return false;
        }
     else{
           document.getElementById("LoginForm_password").style.background = "#FFFFFF"; 
           document.getElementById("LoginForm_password").style.borderColor = "#C6D880";
     }
     
     var checkbox = document.getElementById("LoginForm_rememberMe").checked; 
    
     if(checkbox == false)
        {
            var checkbox2 ='';
        }
     else{
           var checkbox2 = true;
     }
     
        var LoginForm = new Array();
        LoginForm['email'] = k;
        LoginForm['password'] = j;
        LoginForm['rememberMe'] = checkbox2;
     
      document.getElementById('lodingIgif').style.display = "block";
      
      var url = baseurl+"/index.php/user/home";
      
    $.ajax({ 
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {
            LoginForm: {
				email: k,
				password: j,
				rememberMe: checkbox2
			}      
				/*
                LoginForm:{email:k},  
                LoginFormPass:{password:j},
                LoginFormRememberMe:{rememberMe:checkbox2}
				*/
        },
        success: function(value) { 
            $('#lodingIgif').hide();
            if(value.error) {
                $('#lofinfaild').removeClass('flashmgs').addClass('flash' + value.id).text(value.result);                   
            } else {
                document.location.href = baseurl + value.redirect;
                //document.location.href=baseurl+"/index.php/preferences/recommended";
            }
        }
    }); 
           
    return true;
}


function changearrowlistdetail(id)
{
   var x = document.getElementById("myTable").style.display;
   
   if(x == "none")
    {        
        document.getElementById("myTable").style.display = "block";
        document.getElementById("downarrow").src = "../../images/freeworld/down-arrow.png";
    }
    else
    {      
       document.getElementById("myTable").style.display = "none";
       document.getElementById("downarrow").src = "../../images/freeworld/arrow-1.png";
    }
    
}


function changearrowofficial(id)
{
   var x = document.getElementById("listdetail"+id).style.display;
   
   if(x == "none")
    {        
        document.getElementById("listdetail"+id).style.display = "block";
        document.getElementById("downarrow"+id).src = "../../images/freeworld/down-arrow.png";
    }
    else
    {      
       document.getElementById("listdetail"+id).style.display = "none";
       document.getElementById("downarrow"+id).src = "../../images/freeworld/arrow-1.png";
    }
    
}
function changepollvote()
{
    var r=confirm("Do you want to change your vote!");
    if (r==true)
      {  
          return true;
      }
      else
        {
            return false;
        }
}

function deletelist(name)
{
    var r=confirm("Do you want to delete "+'"'+name+'"' +" ?");
    if (r==true)
      {  
          return true;
      }
      else
        {
            return false;
        }
}

function deletemessage()
{
    var r=confirm("Do you want to delete message!");
    if (r==true)
      {  
          return true;
      }
      else
        {
            return false;
        }
}

function resendemail()
{
    document.getElementById("listcreate").submit();
}


function validate_composemsg()
{
    var k=document.getElementById("receiver").value;
    var j=document.getElementById("Message_subject").value;
    var l=document.getElementById("Message_body").value; 
    
    if(trim(k)==null || trim(k)=="")
        { 
            document.getElementById("receiver").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
            document.getElementById("receiver").style.borderColor = "#CC0000";       
            document.compose.receiver.focus();
            return false;
        }
     else{
           document.getElementById("receiver").style.backgroundColor = "#E6EFC2";
           document.getElementById("receiver").style.borderColor = "#C6D880";
     }   
      
           
           
     if(trim(j)==null || trim(j)=="")
        { 
            document.getElementById("Message_subject").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
            document.getElementById("Message_subject").style.borderColor = "#CC0000";   
            document.compose.Message_subject.focus();
            return false;
        }
     else{
           document.getElementById("Message_subject").style.backgroundColor = "#E6EFC2";
           document.getElementById("Message_subject").style.borderColor = "#C6D880";
     }
     
     if(trim(l)==null || trim(l)=="")
        { 
            document.getElementById("Message_body").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
            document.getElementById("Message_body").style.borderColor = "#CC0000";   
            document.compose.Message_body.focus();
            return false;
        }
     else{
           document.getElementById("Message_body").style.backgroundColor = "#E6EFC2";
           document.getElementById("Message_body").style.borderColor = "#C6D880";
     }
     
     return true;
}


function validate_replymsg()
{
    var k=document.getElementById("Message_body").value;
    var l=document.getElementById("browse").value;
    
    if(l)
      {
        return true;
      }
      else
          {
              if(trim(k)==null || trim(k)=="")
            { 
                document.getElementById("Message_body").style.backgroundColor = "#FFA2A2";//"#FFEEEE";
                document.getElementById("Message_body").style.borderColor = "#CC0000";       
                document.compose.Message_body.focus();
                return false;
            }
         else{
               document.getElementById("Message_body").style.backgroundColor = "#E6EFC2";
               document.getElementById("Message_body").style.borderColor = "#C6D880";
         } 
          }
     
      return true;
}
function delete_reply()
{
   
    var count = $("input:checkbox:checked").length;    
    if(count == 0)
     {
         alert("Plese select the checkbox to delete message ")
         return false;
     }
     
    var value = $("input[name=checkbox[]]:checked").map(function () {return this.value;}).get().join(",");     
    var splits = value.split(",");        
    
    var checkbox = document.getElementById("checkbox"+splits[0]).checked; 
    
    
    if(checkbox == false)
        {
            
           alert("Plese select the checkbox to delete message ")
        }
     else{
           var r=confirm("Do you want to delete message!");
            if (r==true)
              {  
                  
                  document.getElementById("message-delete-form").submit();
                  return true;
              }
              else
                {
                    return false;
                }
     }
     return false;
}

function forword_message(baseurl)
{     
    var count = $("input:checkbox:checked").length;    
    if(count == 0)
     {
         alert("Plese select the checkbox to forword message ")
         return false;
     }
    else
    {        
    if(count>1)
     {
         alert("You can forword one message at a time")
         return false;   
     }
     else
         {
              var checkbox= $("input[name=checkbox[]]:checked").map(function () {return this.value;}).get().join(",");              
              $("a").attr("href", baseurl+"/index.php/message/compose?id="+checkbox)
             return true;   
         }
         
     }
    return true;   
}

function hidetext(val,id)
{
    if(val == 'Enter List Instruction')
    {
          $('#'+id).val('');  
    }
    else if(val == '')
    {
          $('#'+id).val('Enter List Instruction');  
    }
}


function hidetextemail(val,id)
{
    if(val == 'Enter email addresses')
    {
          $('#'+id).val('');  
    }
    else if(val == '')
    {
          $('#'+id).val('Enter email addresses');  
    }
}

function hidepolltext(val,id)
{
    if(val == 'Enter poll Instruction')
    {
          $('#'+id).val('');  
    }
    else if(val == '')
    {
          $('#'+id).val('Enter poll Instruction');  
    }
}


function list_fav_check()
{
    var checkbox = document.getElementById("list_fav_check").checked; 
    if(checkbox == true)
    {
        document.getElementById("list_fav_check").value = '1';    
    }
    else
    {
        document.getElementById("list_fav_check").value = '0';
    }
}

function editlist_popup(url)
{
    $.colorbox({iframe:true, width:"704px", height:"650px;", opacity:0.3, href:url});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('10'));
    var scrollto = myDiv.offset().top - (myDiv.height() / 3);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}

function editprofile_popup(url)
{
    $.colorbox({iframe:true, width:"704px", height:"720px;", opacity:0.3, href:url});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
    var scrollto = myDiv.offset().top - (myDiv.height() / 100);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}
function editprofile_notification(url)
{
    $.colorbox({iframe:true, width:"704px", height:"420px;", opacity:0.3, href:url});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
    var scrollto = myDiv.offset().top - (myDiv.height() / 100);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}

function hidestatus(val,id)
{
    if(val == 'Update Status')
    {
          $('#'+id).val('');  
    }
    else if(val == '')
    {
          $('#'+id).val('Update Status');  
    }
}

function hidesearch(val,id)
{
    if(val == 'Search')
    {
          $('#'+id).val('');  
    }
    else if(val == '')
    {
          $('#'+id).val('Search');  
    }
}

function update_profilestatus()
{
     $('#loadingstatusimg').show();
     $.ajax({
            type: $('#status-form').attr('method'),
            url:  $('#status-form').attr('action'),
            data: $('#status-form').serialize(),
            dataType: "json",
            success: function (data) { 
                if(data == '1')
                {
                    $('#loadingstatusimg').hide();
                    $("#profile_status").html('Your status is updated.'); 
                    $("#profile_status").css('color','#333232');
                }
                else if(data == 'empty')
                {
                    $('#loadingstatusimg').hide();
                    $("#profile_status").html('Please fill the status to update.'); 
                    $("#profile_status").css('color','red');
                }
            }
        });
      return false;
}

var popupStatus = 0;
function loadPopup1(){
   if(popupStatus==0){
        jQuery("#backgroundPopup1").css({
                                  "opacity": "0.7"
                                 });
        jQuery("#backgroundPopup1").fadeIn("slow");
        jQuery("#popupContact1").fadeIn("slow");
        popupStatus = 1;
    }
}
function disablePopup1(){
   if(popupStatus==1){
       jQuery("#backgroundPopup1").fadeOut("slow");
       jQuery("#popupContact1").fadeOut("slow");
       popupStatus = 0;
   }
}
function centerPopup1(){  
    
   var windowWidth = document.documentElement.clientWidth;
   var windowHeight = document.documentElement.clientHeight;
   var popupHeight = jQuery("#popupContact1").height();
   var popupWidth = jQuery("#popupContact1").width();
   
   jQuery("#popupContact1").css({
                           "position": "absolute"
                           //"top": windowHeight/2-popupHeight/2,
                           //"left": windowWidth/2-popupWidth/2
                          });
   jQuery("#backgroundPopup1").css({
                              "height": windowHeight
                            });
}


jQuery(document).ready(function(){
    jQuery("#button").click(function(){
        centerPopup1();
        loadPopup1();
    });
    
    
    $('#User_password1').show();
                $('#User_password').hide();

                $('#User_password1').focus(function() {
                    $('#User_password1').hide();
                    $('#User_password').show();
                    $('#User_password').focus();
                });
                $('#User_password').blur(function() { 
                    if($('#User_password').val() == 'Password') {
                        $('#User_password1').show();
                        $('#User_password').hide();
                    }
                });
     
    $('#company_password1').show();
                $('#company_password').hide();

                $('#company_password1').focus(function() {
                    $('#company_password1').hide();
                    $('#company_password').show();
                    $('#company_password').focus();
                });
                $('#company_password').blur(function() { 
                    if($('#company_password').val() == 'Password') {
                        $('#company_password1').show();
                        $('#company_password').hide();
                    }
                });
                
    
    $('#LoginForm_password1').show();
	$('#LoginForm_password').hide();
	 
	$('#LoginForm_password1').focus(function() {
	    $('#LoginForm_password1').hide();
	    $('#LoginForm_password').show();
	    $('#LoginForm_password').focus();
	});
	$('#LoginForm_password').blur(function() {
	    if($('#LoginForm_password').val() == '') {
	        $('#LoginForm_password1').show();
	        $('#LoginForm_password').hide();
	    }
	});
        
        
    $('#User_email_forgot').keyup(function(e){ 
     if(e.which==13)
     {
        $('#forgotpass_btn').click();        
     }
    })
    
    $('#LoginForm_password').keyup(function(e){
     if(e.which==13)
     {
        $('#login_btn').click();        
     }
    })
    
    
    $('#newpassword2').keyup(function(e){
     if(e.which==13)
     {
        $('#submitchangepassword').click();      
     }
    })
    
    jQuery(document).bind('click', function(e) {
            var jQueryclicked = jQuery(e.target);
            if (! jQueryclicked.parents().hasClass("listdiv")){
                jQuery(".listdiv").hide();               
                }
        })
        
    jQuery(document).bind('click', function(e) {
            var jQueryclicked = jQuery(e.target);
            if (! jQueryclicked.parents().hasClass("home_ulli_interest")){ 
                 if($(".home_ulli_interest").is(":visible")==true)
                 {
                     $(".home_ulli_interest").html('');
                 }
                 jQuery(".home_ulli_interest").hide();  
                }
        })
      jQuery(document).ready(function(){
         var n = 0;
        $("#popupContact1_msgclick").mouseenter(function() {
        n = 0;

        }).mouseleave(function() {
        n = 1;
        });

        $("html").click(function(){ 
        if (n == 1) {
        jQuery(".popupContact1_msgclicks").hide();   
        n=0;
        }
        });
         });

		jQuery(document).ready(function(){
			/*
			var n = 0;
			$("#popuplogout").mouseenter(function() {
				n = 0;
			}).mouseleave(function() {
				n = 1;
			});
			*/
			$("html").click(function(event){
				if (event.target === $("#backgroundPopup3")[0]) {
					$(".logoutdiv").hide();  
					$("#backgroundPopup3").hide();  
				}
				/*
				if (n == 1) {
					$(".logoutdiv").hide();  
					$("#backgroundPopup3").hide();  
					n=0;
				}
				*/
			});
		});
         
         
         jQuery(document).ready(function(){
         var n = 0;
        $("#popupfavorite").mouseenter(function() {
        n = 0;

        }).mouseleave(function() {
        n = 1;
        });

        $("html").click(function(){ 
        if (n == 1) {
        jQuery("#popupfavorite").hide();  
        jQuery("#backgroundPopup4").hide(); 
        $('#favoritediv_id').removeClass('activeclass_favorite').addClass('cursorpointer');
        var baseurl = document.getElementById('baseurl').value;
        var image = baseurl+"/images/drop_down.png";
        $('#droupdownimg').attr('src',image);
        n=0;
        }
        });
         });
         

        
      jQuery(document).bind('click', function(e) {
            var jQueryclicked = jQuery(e.target);
            if (! jQueryclicked.parents().hasClass("born_s_int")){
                if($("#showdiv_interest").is(":visible")==true)
                  {  if($("#currenturl").val()=="home")
                      {
                          applyinterest();
                      }
                      else
                       {
                             applyinterest_myprofile($("#baseurl").val()); 
                       }
                      jQuery("#showdiv_interest").hide(); 
                  }
                
             }
        })
        
        jQuery(document).bind('click', function(e) {
            var jQueryclicked = jQuery(e.target);
            if (! jQueryclicked.parents().hasClass("born_s_int")){
                
                if($("#showdiv_interest_com").is(":visible")==true)
                  {                
                     applyinterest();
                     jQuery("#showdiv_interest_com").hide();  
                  }
                }
        })
        
    jQuery("#secret-why1").click(function(){
        centerPopup1();
        loadPopup1();        
    });
	
	
	$('#popupContact2').on('click', 'a.link-base', function(e) {
		$.ajax({
			async: false,
			type: 'POST',
			url: '/index.php/user/readNotifications?tab=all',
			dataType: 'text',
			data: { }, 
			success: function (resp) {}
		});
	});
	
	
    jQuery("#popupContactClose1").click(function(){
        disablePopup1();
    });
    jQuery("#backgroundPopup1").click(function(){
        disablePopup1();
    });
    jQuery(document).keypress(function(e){
        if(e.keyCode==27 && popupStatus==1){
           disablePopup1();
        }
    });
    
    jQuery("#popupContact1_msgclick").click(function(){
        $("#recever1").val('');
    });
    
    jQuery("#page").click(function(){
        $("#companytypings").val('');
    });
    
    jQuery("#button").click(function(){
        centerPopup2();
        loadPopup2();
    });
    jQuery("#secret-why2").click(function(){
        centerPopup2();
        loadPopup2();
    });
    jQuery("#popupContactClose").click(function(){
        disablePopup2();
    });
    jQuery("#backgroundPopup2").click(function(){
        disablePopup2();
    });
    jQuery(document).keypress(function(e){
        if(e.keyCode==27 && popupStatus2==1){
           disablePopup2();
        }
    });
});

var popupStatus2 = 0;
function loadPopup2(){
   if(popupStatus2==0){
        jQuery("#backgroundPopup2").css({
                                  "opacity": "0.7"
                                 });
        jQuery("#backgroundPopup2").fadeIn("slow");
        jQuery("#popupContact2").fadeIn("slow");
        popupStatus2 = 1;
    }
}
function disablePopup2(){
   if(popupStatus2==1){
       jQuery("#backgroundPopup2").fadeOut("slow");
       jQuery("#popupContact2").fadeOut("slow");
       popupStatus2 = 0;
	   if ($.trim($('#secret-why2 .cursorpointer.heightclass').text()) !== '') {
		   $.ajax({
			   type: 'POST',
			   url: '/index.php/user/readNotifications?tab=all',
			   dataType: 'text',
			   data: { }, 
			   success: function (resp) {
				   try { updateSummaryPanel(); } catch (ex) { }
				   $('#secret-why2 .cursorpointer.heightclass').text('');
				   $('#popupCont2').html('<div class="notification_merginediv1"><div class="no_notification_div"><a href="' + FW.baseurl + '/index.php/user/myNotifications" class="blue-btn">Show My Notifications</a></div></div>');
				   //$('#popupCont2').html('<div class="notification_merginediv1"><div class="no_notification_div"><span>No Notification Received</span></div></div>');
			   }
		   });
	   }
   }
}
function centerPopup2(){
   var windowWidth = document.documentElement.clientWidth;
   var windowHeight = document.documentElement.clientHeight;
   var popupHeight = jQuery("#popupContact2").height();
   var popupWidth = jQuery("#popupContact2").width();
   
   jQuery("#popupContact2").css({
                           "position": "absolute"                           
                          });
   jQuery("#backgroundPopup2").css({
                              "height": windowHeight
                            });
   /*
   $.ajax({
       type: 'POST',
       url: '/index.php/user/readNotifications?tab=all',
       dataType: 'text',
       data: { }, 
       success: function (resp) {
           //try { updateSummaryPanel(); } catch (ex) { }
           //$('#secret-why2 .cursorpointer.heightclass').text('');
           //$('#popupCont2').html('<div class="no_notification_div"><span>No Notification Received</span></div>');
       }
   });
   */
}

function take_list()
{
    
    
    var rows = $('#myTable tr').length;
    var i=0;
    for(i=1;i<rows;i++)
     {
         var color = document.getElementById("input"+i+"1").style.backgroundColor;
         
         if(color == "rgb(255, 162, 162)")
             {
                 return false;
                 break;
             }
     }
     
     var value  = document.getElementById("input10").value;
     
     if(value=='Enter Selection.')
         {
             document.getElementById("input10").style.backgroundColor = "#FFA2A2";
             document.getElementById("input10").style.border = "1px solid #CC0000";
             document.getElementById("input10").style.height = "28px";
             document.getElementById("input10").style.width = "258px";
             return false;
             
         }
      else
        {
             document.getElementById("input10").style.backgroundColor = "#E6EFC2";
             document.getElementById("input10").style.borderColor = "#C6D880";
        }
        
        
        var value2  = document.getElementById("input11").value;
        
        if(value2=='Enter 1-10 rating(10 highest).')
         {
             document.getElementById("input11").style.backgroundColor = "#FFA2A2";
             document.getElementById("input11").style.border = "1px solid #CC0000";
             document.getElementById("input11").style.height = "28px";
             document.getElementById("input11").style.width = "258px";
             return false;
         }
      else
        {
             document.getElementById("input11").style.backgroundColor = "#E6EFC2";
             document.getElementById("input11").style.borderColor = "#C6D880";
        }
        
        
    document.getElementById("lodinggiftake").style.display = "block";
    $.ajax({ 
            
            type: $('#takelist').attr('method'),
            url:  $('#takelist').attr('action'),
            data: $('#takelist').serialize(),
            dataType: "json",
            success: function (data) { 
                document.getElementById("lodinggiftake").style.display = "none";
                var action = $('#takelist').attr('action').split('preferences');
                var target = action[0]+"preferences/listdetail?listid="+data.listid;
                parent.$.fn.colorbox.close();
                self.parent.location=target;
            }
        });
      return false;
}

function takelist_popup(url)
{    
    $.colorbox({iframe:true, width:"704px", height:"714px;", opacity:0.3, href:url});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
    var scrollto = myDiv.offset().top - (myDiv.height() /100);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}


function forgotpassword_popup(url)
{    
    $.colorbox({iframe:true, width:"550px", height:"250px;", opacity:0.3, href:url});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
    var scrollto = myDiv.offset().top - (myDiv.height() /100);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}


function update_profileinterest()
{
     $('#loadinginterestimg').show();
     $.ajax({
            type: $('#interest-form').attr('method'),
            url:  $('#interest-form').attr('action'),
            data: $('#interest-form').serialize(),
            dataType: "json",
            success: function (data) { 
                if(data == '1')
                {
                    $('#loadinginterestimg').hide();
                    $("#profile_interest").html('Your interests are saved.'); 
                    $("#profile_interest").css('color','#333232');
                }
                else if(data == 'empty')
                {
                    $('#loadinginterestimg').hide();
                    $("#profile_interest").html('Please select the interests to save.'); 
                    $("#profile_interest").css('color','red');
                }
            }
        });
      return false;
}

function update_displaylist()
{
     $('#loadingdplistimg').show();
     $.ajax({
            type: $('#choselist-form').attr('method'),
            url:  $('#choselist-form').attr('action'),
            data: $('#choselist-form').serialize(),
            dataType: "json",
            success: function (data) { 
                if(data == '1')
                {
                    $('#loadingdplistimg').hide();
                }
            }
        });
      return false;
}

function suggestion_popup(url)
{
    var titless= document.getElementById("title").value;
    var newurl;
    newurl = url+'/title/'+titless;    
    $.colorbox({iframe:true, width:"704px",data:{title:titless}, height:"700px;", opacity:0.3, href:newurl});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
    var scrollto = myDiv.offset().top - (myDiv.height() /100);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}

function invitefriends()
{
    var count       = $("input:checkbox:checked").length;  
    var instruction = document.getElementById("instruction2").value;
    
    if(count == 0 && (trim(instruction)==null || trim(instruction)==""))
       {
         alert("Please select the checkbox or enter the email address to invite your friends. ");
         return false;
       }    
    
   return true;
}


function nameautofil_take(baseurl,id,e)
{
  var unicode=e.keyCode? e.keyCode : e.charCode
  if(unicode!=38 && unicode!=40)
   {
    var color = document.getElementById("entry"+id).style.backgroundColor;
         
         if(color == "rgb(255, 162, 162)")
             {
                 document.getElementById('entry'+id).style.backgroundColor = "#EEEEEE";                 
             }
    
  var title    = document.getElementById("entry"+id).value;
  var oldentry = document.getElementById("listvalue_hidden").value;
  var listid   = document.getElementById("listid").value; 
  
   var url = baseurl+'/index.php/preferences/nameautofil?listid='+listid
    jQuery.ajax({ 
        type:'POST',
        url:url,        
        data:{
                title:title,
                oldentry:oldentry
             },
             success:function(value){  
                 
                 jQuery("#div"+id).hide(); 
                 if(value!=1 && value !="<ul  class='popuplistcreation_ul' ></ul>" && value != "") 
                  {            
                          jQuery("#div"+id).show(); 
                          jQuery("#div"+id).html(value).show();                      
                  }
                  else
                   {   
                       jQuery("#div"+id).hide(); 
                   }
                  
             
    }
    }); 
}
}


function nameautofil(baseurl,id)
{
    var baseurl = $("#hidden_baseurl").val();
    var color = document.getElementById("entry"+id).style.backgroundColor;
         
         if(color == "rgb(255, 162, 162)")
             {
                 document.getElementById('entry'+id).style.backgroundColor = "#EEEEEE";                 
             }
    
  var title    = document.getElementById("entry"+id).value;
  var oldentry = document.getElementById("listvalue_hidden").value;
  var listid   = document.getElementById("listid").value; 
  
   var url = baseurl+'/index.php/preferences/nameautofil?listid='+listid
    jQuery.ajax({ 
        type:'POST',
        url:url,        
        data:{
                title:title,
                oldentry:oldentry
             },
             success:function(value){  
                 
                 jQuery("#div"+id).hide(); 
                 if(value!=1 && value !="<ul  class='popuplistcreation_ul' ></ul>" && value != "") 
                  {            
                          jQuery("#div"+id).show(); 
                          jQuery("#div"+id).html(value).show();                      
                  }
                  else
                   {   
                       jQuery("#div"+id).hide(); 
                   }
                  
             
    }
    }); 
}


function nameautofilover(id)
{
    document.getElementById(id).style.backgroundColor = "#DDD875";
}

function nameautofilout(id)
{
    
    document.getElementById(id).style.backgroundColor = "#95BCF2";
}

function nameautofilover_interest(id)
{
    document.getElementById(id).style.backgroundColor = "#DDD875";
    document.getElementById(id).style.color = "#fff";
}

function nameautofilout_interest(id)
{
	if (document.getElementById(id)) {
		document.getElementById(id).style.color = "#565656";
		document.getElementById(id).style.backgroundColor = "#e0e0e0";
	}
}


function namesuggestion(id)
{    
    var name = trim($(id).text());
    
    jQuery(id).parents("td").find("input").val(name);
    
    jQuery(id).parents(".listdiv_takelist").hide();
    
    jQuery(id).parents(".listdiv_takelist").html('');
    
    var listvalue_hidden=  jQuery("#listvalue_hidden").val();
    if(listvalue_hidden == "")
        {
            name = name+",";
            jQuery("#listvalue_hidden").val(name);
        }
   else
      {
          
            var listvalue_hidden_kk = listvalue_hidden+name+",";
            jQuery("#listvalue_hidden").val(listvalue_hidden_kk);
      }
}

function interstnameuser(ob)
{ 
        var pp = jQuery(ob).text();
        var id = jQuery(ob).find('.testclassheader').val();
        
       var baseurlclick = document.getElementById("baseurl").value;    
       
       jQuery(ob).parents("ul").find("#compose_heaserspan").append("<li><span>"+pp+"</span><input type='hidden' value='"+id+"' class='cmpsemsgcls'/><a href='javascript:void(0);' onclick='removeli_header(this)'><img  src='"+baseurlclick+"/images/cross.png' class='imagespan_official'/></a></li>");
                
        
        jQuery(ob).parents(".home_ulli_interest").hide();


        var hidden = jQuery("#receveridmessage").val();
        if(hidden=='')
          {
              var string  = id+',';
              jQuery("#receveridmessage").val(trim(string));
          }
        else
          {

             var string1  = hidden+id+',';
             jQuery("#receveridmessage").val(trim(string1));
          }

}


function interstsuggestion(ob)
{
    var onclick = document.getElementById("onclick").value;
    
    if(onclick == 'personalclick'){
        
        var pp = jQuery(ob).text();
        
        var currenturl = document.getElementById("currenturl").value;
        if(currenturl != "myProfile" && currenturl != "listtitle"){
         jQuery(ob).parents("ul").find("#mainspan").append("<li><span>"+pp+"</span><a href='javascript:void(0);' onclick='removeli(this)'>X</a></li>");
        }
        else{
             var baseurl = document.getElementById("baseurl").value;
             jQuery(ob).parents("ul").find("#mainspan").append("<li><span>"+pp+"</span><a href='javascript:void(0);' onclick='removeli(this)'><img class='imagespan_profile' src='"+baseurl+"/images/cross.png'</a></li>");
       }
        jQuery(ob).parents(".home_ulli_interest").hide();
        $(".home_ulli_interest").html('');

        var hidden = jQuery("#hiddeninterest").val();
        if(hidden=='')
          {
              var string  = pp;
              jQuery("#hiddeninterest").val(trim(string));
          }
        else
          {

             var string1  = hidden+','+pp;
             jQuery("#hiddeninterest").val(trim(string1));
          }

   
        jQuery("#typings").val('').focus();
    }
    else
    {   
        
        var pp = jQuery(ob).text();
        
        if(onclick=='official')
          {    
                var idlist =   jQuery(ob).attr('id').split('.');
                var idmain = idlist[0].replace("li", "");
                var baseurlclick = document.getElementById("baseurlclick").value;                
                jQuery(ob).parents("ul").find("#mainspan_com").append("<li><span>"+pp+"</span><a href='javascript:void(0);' onclick='removeli_recent(this,"+idmain+")'><img  src='"+baseurlclick+"/images/cross.png' class='imagespan_official'/></a></li>");
                document.getElementById("companytypings_off_recent").value = '';
                $(".home_ulli_interest_recentsearch").hide();
                $(".home_ulli_interest1112").html('');
                var hiddeninterest = jQuery("#hiddeninterest").val();
                if(hiddeninterest=='')
                  {
                      var strings  = idmain+',';
                      jQuery("#hiddeninterest").val(trim(strings));              
                  }
                else
                  {

                     var strings  = hiddeninterest+idmain+',';
                     jQuery("#hiddeninterest").val(trim(strings));
                  }
          }
        else
          {
              jQuery(ob).parents("ul").find("#mainspan_com").append("<li><span>"+pp+"</span><a href='javascript:void(0);' onclick='removeli_com(this)'>X</a></li>");
          }
        
        jQuery(ob).parents(".home_ulli_interest").hide();
        $(".home_ulli_interest").html('');

        var hidden = jQuery("#hiddeninterest_company").val();
        if(hidden=='')
          {
              var string  = pp;
              jQuery("#hiddeninterest_company").val(trim(string));              
          }
        else
          {

             var string1  = hidden+','+pp;
             jQuery("#hiddeninterest_company").val(trim(string1));
          }


        jQuery("#companytypings").val('').focus();
    }
    
    var radiobtn  = jQuery("#radio1tw").val();
    
    if(radiobtn=='taken' || radiobtn=='recent')
    {
       refineFilterSearch();
    }
}
function removeli(ob)
{
      var remtext   = $(ob).parents('li').find('span').text();
      $(ob).parents('li').remove();     
      var recipients    =   $('#hiddeninterest').val();   
      if(recipients.indexOf(',') != -1){
          var LeftRecipient =   recipients.replace(','+remtext,'');      
      }
      else{
          var LeftRecipient =   recipients.replace(remtext,'');      
      }
      
      $('#hiddeninterest').val(LeftRecipient);
      
     var temp = $('#hiddeninterest').val();
     if(temp=='')
         {
              $('#typings').val('Click the arrow');
         }
}
function removeli_com(ob)
{
      var remtext   = $(ob).parents('li').find('span').text();
      $(ob).parents('li').remove();  
      var recipients    =   $('#hiddeninterest_company').val(); 
      if(recipients.indexOf(',') != -1){
         var LeftRecipient =   recipients.replace(','+remtext,'');  
      }
      else{
          var LeftRecipient =   recipients.replace(remtext,'');  
      }
      $('#hiddeninterest_company').val(LeftRecipient);
      
      var temp = $('#hiddeninterest_company').val();
     if(temp=='')
         {
              $('#companytypings').val('Click the arrow');
         }
    
}

function removeli_recent(ob,id)
{
      var remtext   = $(ob).parents('li').find('span').text();
      $(ob).parents('li').remove();  
      var recipients    =   $('#hiddeninterest_company').val(); 
      if(recipients.indexOf(',') != -1){
         var LeftRecipient =   recipients.replace(','+remtext,'');  
      }
      else{
          var LeftRecipient =   recipients.replace(remtext,'');  
      }
      $('#hiddeninterest_company').val(LeftRecipient);
      
      var recipients1    =   $('#hiddeninterest').val(); 
      var LeftRecipient1 =   recipients1.replace(id+',','');  
      $('#hiddeninterest').val(LeftRecipient1);
      
      var radiobtn  = jQuery("#radio1tw").val();
    
        if(radiobtn=='taken' || radiobtn == 'recent')
        {
           refineFilterSearch();
        }
}

function removeli_header(ob)
{
    ///////alert(ob);
      var remtext   = $(ob).parents('li').find('span').text();
      var id        = $(ob).parents('li').find('.cmpsemsgcls').val();
      
      $(ob).parents('li').remove();  
      var recipients    =   $('#receveridmessage').val(); 
      
      var LeftRecipient =   recipients.replace(id+',','');  
      
      
      $('#receveridmessage').val(LeftRecipient);      
     
    
}

function maximumvaluecheck(id)
{
    var value = document.getElementById('input'+id).value;    
    var temp = isNaN(value);    
    if(temp==true)
        {
            document.getElementById('input'+id).style.backgroundColor = "#FFA2A2";
            document.getElementById('input'+id).style.border = "1px solid #CC0000";
            document.getElementById('input'+id).style.height = "28px";
            document.getElementById('input'+id).style.width = "258px";
            return false;
        }
    else
        {
           if(value>10)
               { 
                   document.getElementById('input'+id).style.backgroundColor = "#FFA2A2";
                   document.getElementById('input'+id).style.border = "1px solid #CC0000";
                   document.getElementById('input'+id).style.height = "28px";
                   document.getElementById('input'+id).style.width = "258px";
                   return false;
               }
               else
                   {
                       document.getElementById('input'+id).style.backgroundColor = "#E6EFC2";
                       document.getElementById('input'+id).style.border = "0px";
                       document.getElementById('input'+id).style.height = "30px";
                       document.getElementById('input'+id).style.width = "260px";
                       return true;
                   }
        }
}


function autherclick(baseurl,id)
{
    var target = baseurl+"/index.php/user/dashboard?userid="+id;
    parent.$.fn.colorbox.close();
    self.parent.location=target;
}


function validatecontact_form()
{
    var contact_name = document.getElementById("contact_name").value; 
    var contact_email = document.getElementById("contact_email").value;
    var contact_telephone = document.getElementById("contact_telephone").value;
    var contact_comment = document.getElementById("contact_comment").value;
    
    if(trim(contact_name)==null || trim(contact_name)=="")
    { 
            document.getElementById("contact_name_req").style.display = "block";
            document.getElementById("contact_name").style.border = "1px solid red";
            return false;
    }
    else
    {
            document.getElementById("contact_name_req").style.display = "none";
            document.getElementById("contact_name").style.border = "1px solid #b6b6b6";
    }
    
    if(trim(contact_email)==null || trim(contact_email)=="")
    { 
            document.getElementById("contact_email_req").style.display = "block";
            document.getElementById("contact_email").style.border = "1px solid red";
            return false;
    }
    else
    {
            document.getElementById("contact_email_req").style.display = "none";
            document.getElementById("contact_email").style.border = "1px solid #b6b6b6";
    }
    
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(contact_email) == false)
    { 
            document.getElementById("contact_email_req").style.display = "block";
            document.getElementById("contact_email").style.border = "1px solid red";
            document.getElementById("contact_email_req").innerHTML = "Email address is not valid"
            return false;
    }
    else
    { 
           document.getElementById("contact_email_req").style.display = "none";
           document.getElementById("contact_email").style.border = "1px solid #b6b6b6";
    }
    /*
    if(trim(contact_telephone)==null || trim(contact_telephone)=="")
    { 
            document.getElementById("contact_telephone_req").style.display = "block";
            document.getElementById("contact_telephone").style.border = "1px solid red";
            return false;
    }
    else
    {
            document.getElementById("contact_telephone_req").style.display = "none";
            document.getElementById("contact_telephone").style.border = "1px solid #b6b6b6";
    }
	*/
    if(trim(contact_telephone)!=null && trim(contact_telephone)!="") {
 
		var reg_phone = /[0-9]{6,15}/; 
		if(reg_phone.test(contact_telephone) == false) 
		{
			   document.getElementById("contact_telephone_req").style.display = "block";
			   document.getElementById("contact_telephone").style.border = "1px solid red";
			   document.getElementById("contact_telephone_req").innerHTML = "Telephone is not valid"
			   return false;
		}  
		else
		{
			   document.getElementById("contact_telephone_req").style.display = "none";
			   document.getElementById("contact_telephone").style.border = "1px solid #b6b6b6"; 
		}
    }
	
    if(trim(contact_comment)==null || trim(contact_comment)=="")
    { 
            document.getElementById("Comment_email_req").style.display = "block";
            document.getElementById("contact_comment").style.border = "1px solid red";
            return false;
    }
    else
    {
            document.getElementById("Comment_email_req").style.display = "none";
            document.getElementById("contact_comment").style.border = "1px solid #b6b6b6";
    }
    
    
  $('#loadingajaximg').show();  
  $.ajax({
            type: $('#contactus-form').attr('method'),
            url:  $('#contactus-form').attr('action'),
            data: $('#contactus-form').serialize(),
            dataType: "json",
            success: function (data) { 
                if(data == '1')
                {
                    $('#loadingajaximg').hide();
                    $(".contact_successmsg").show();
                }
                else
                {
                    $('#loadingajaximg').hide();
                    $(".contact_successmsg").html("There is some problem in sending mail");
                    $(".contact_successmsg").show();    
                }
            }
        });
      return false;  
}

function currentdatechange()
{     
    setTimeout(function(){        
        $("td a.datepick-today").removeClass('datepick-today')
        $("td span").removeClass('datepick-highlight')
    },100);    
}



function invitefrnds_popup(url,count)
{   
    var hight = count*90;  
    
    if(count==1)
        {
            hight = 460;
        }
   else if(count==2)
        { 
            hight = 505;
        }
     else if(count==3)
         {
             hight = 550;
         }
    else if(count==4)
         {
             hight = 595;
         }
    else if(count==5)
         {
             hight = 655;
         }
    else if(count==6)
         {
             hight = 710;
         }
     else 
         { 
             hight = 750;
         }
         
    $.colorbox({iframe:true, width:"704px", height:hight+"px", opacity:0.3, href:url});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
    var scrollto = myDiv.offset().top - (myDiv.height() /100);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}



function opencalender()
{
    
   //$(".datepick-trigger").attr("src","../../images/freeworld/calright-arrow.png");    
    
    var value = document.getElementById('popupDatepicker').value;  
    if(value!=""){
    var kk    = $('#popupDatepicker').val().split('/');
    var temp  = kk[0].split('-');
    var today = new Date();
    var date  = today.getDate();
    
    var temp2 = kk[1].split('-');
    
    if(temp[2]!=date && temp2[2]!=date)
     { 
      if(value!='')
        {
            setTimeout(function(){    
                 $("td span.datepick-today").css("backgroundColor",'#777');
                 $("td span.datepick-today").css("color",'#fff');  
                 
                 
                 $("td a.datepick-today").css("backgroundColor",'#777');
                 $("td a.datepick-selected").removeClass('datepick-selected').addClass('testcls');
                 $('.testcls:first,.testcls:last').addClass('datepick-selected');
                 $("td a").removeClass('testcls');
                 },100);
        }   
     }
     else
         {
             if(value!='')
                {
                    setTimeout(function(){  
                         $("td span.datepick-today").css("backgroundColor",'#777');
                         $("td span.datepick-today").css("color",'#fff');  
                         
                         
                         $("td a.datepick-selected").removeClass('datepick-selected').addClass('testcls');
                         $('.testcls:first,.testcls:last').addClass('datepick-selected');
                         $("td a").removeClass('testcls');
                         },100);
                } 
         }
  }
}



function openCalenders()
{
   
     var value  = document.getElementById("popupDatepicker").value;  
     /*if(value!='')
       {
                var date1  = value.split("-");
                
                var finalyr = "20"+date1[0];
                var temp1   = date1[1].split("");
                var finalmo = parseInt(temp1[1]) - 1;
                var temp2   = date1[2].split("");
                
                if(temp2[0]!=0)
                {
                    var finalda = date1[2];
                }
                else{
                var finalda = parseInt(temp2[1]);
                }
                var finaldate = finalyr+'-'+ finalmo+'-'+finalda;
       }
     
     
     var olddateid = new Array();
    
     
     for(var kk = 1;kk<=7;kk++){
     var myDate=new Date();
      myDate.setDate(myDate.getDate()-kk);      
      olddateid[kk] = myDate.getFullYear()+'-'+ myDate.getMonth()+'-'+myDate.getDate();
     }     */
    
      setTimeout(function(){ 
       
          //for(var i=1;i<=7;i++)
              {                  
                   //jQuery("#"+olddateid[i]).addClass('datepick-selected');  
                   
                   if(value!='')
                   {
                       $("td span.datepick-today").css("backgroundColor",'#777');
                       $("td span.datepick-today").css("color",'#fff');
                       
                       var value1  = document.getElementById("value_calender").value; 
                       
                       if(value1!='')
                           {
                               $("td a.datepick-selected").removeClass('datepick-selected').addClass('testcls');
                               
                               if(value!='')
                                   {
                                            var date1  = value1.split("-");

                                            var finalyr = "20"+date1[0];
                                            var temp1   = date1[1].split("");
                                            var finalmo = parseInt(temp1[1]) - 1;
                                            var temp2   = date1[2].split("");

                                            if(temp2[0]!=0)
                                            {
                                                var finalda = date1[2];
                                            }
                                            else{
                                            var finalda = parseInt(temp2[1]);
                                            }
                                            var finaldate = finalyr+'-'+ finalmo+'-'+finalda;
                                   }
                                   jQuery("#"+finaldate).addClass('datepick-selected');  
                           }
                       
                   }
                   
                   
                   
             }
             
       },100);


    
}



function friendnotification_popup(url)
{   
    $.colorbox({iframe:true, width:"704px", height:"225px;", opacity:0.3, href:url});
    var myDiv = $("#cboxLoadedContent"); 
    $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
    var scrollto = myDiv.offset().top - (myDiv.height() /100);
    $('html,body').animate({scrollTop:  scrollto}, 700);
}


function replyofmsg()
{
    var valuesub  = document.getElementById("Message_subject").value;
    var valuebody = document.getElementById("Message_body").value;
    
    if(valuesub=='')
         {
             document.getElementById("Message_subject").style.backgroundColor = "#FFA2A2";
             document.getElementById("Message_subject").style.border = "1px solid #CC0000";             
             return false;
             
         }
      else
        {
             document.getElementById("Message_subject").style.backgroundColor = "#E6EFC2";
             document.getElementById("Message_subject").style.borderColor = "#C6D880";
        }
        
    if(valuebody=='')
         {
             document.getElementById("Message_body").style.backgroundColor = "#FFA2A2";
             document.getElementById("Message_body").style.border = "1px solid #CC0000";             
             return false;
             
         }
      else
        {
             document.getElementById("Message_body").style.backgroundColor = "#E6EFC2";
             document.getElementById("Message_body").style.borderColor = "#C6D880";
        }
        
        return true;
}



function cteatepolls()
{ 
     var baseurl = document.getElementById('hidden_baseurl').value;
     var a       = document.getElementById("title").value;
          
     var c=document.getElementById("interest_topic").value; 
     var d=document.getElementById("image").value;
     var e=document.getElementById("listtype").value;
     var y=document.getElementById("instruction").value;
     var f=document.getElementById('invalid').style.display;
     var update = document.getElementById('updatelist').style.display;
     
     if((trim(e)==null || trim(e)=="" || e == 'select')) 
        { 
                document.getElementById('listtypereq').style.display = "block";
                document.getElementById('listtype').style.border = '1px solid red';
                document.listcreate.listtype.focus();
                return false;
        }
        else
        {
                document.getElementById('listtype').style.border = 'none';
                document.getElementById('listtypereq').style.display = "none";
        }
     
     if(update == 'none')
     {
        if((trim(a)==null || trim(a)=="")) 
        { 
                document.getElementById('titlereq').style.display = "block";
                document.getElementById('valid').style.display = "none";
                document.getElementById('invalid').style.display = "none";
                document.getElementById('title').style.border = '1px solid red';
                document.listcreate.title.focus();
                return false;
        }
        else if(trim(a).length>0)
        {
               var url = baseurl+"/index.php/preferences/checktitle";
               document.getElementById('loding').style.display = "block";
               var test;
               jQuery.ajax({ 
                    type:'POST',
                    url:url,
                    datatype:'json',
                    async: false,
                    data:{
                            title:a                  
                         },
                         success:function(value){ 
                             document.getElementById('loding').style.display = "none";
                             if(value==1) 
                             {
                                document.getElementById('title').style.border = 'none';  
                                document.getElementById('valid').style.display = "block";
                                document.getElementById('invalid').style.display = "none";  
                                document.getElementById('titlereq').style.display = "none"; 
                                test = true
                             }
                             else
                             {    
                                   document.getElementById('title').style.border = '1px solid red';
                                   document.getElementById('invalid').style.display = "block";
                                   document.getElementById('valid').style.display = "none";   
                                   document.getElementById('titlereq').style.display = "none"; 
                                   test = false
                             }
                
                }
                });
                if(test == false)
                {
                   return false;      
                }
        }
        else
        {
                document.getElementById('title').style.border = 'none';
                document.getElementById('titlereq').style.display = "none";
        }
     }
     
        if((trim(c)==null || trim(c)=="" || c == 'select')) 
        { 
                document.getElementById('interestreq').style.display = "block";
                document.getElementById('interest_topic').style.border = '1px solid red';
                document.listcreate.interest_topic.focus();
                return false;
        }
        else
        {
                document.getElementById('interest_topic').style.border = 'none';
                document.getElementById('interestreq').style.display = "none";
        }
     
    
   
        
        
        if(e=='official')
          {
        
              if((trim(d)==null || trim(d)=="")) 
                    { 
                            document.getElementById('imgreq').style.display = "block";
                            document.getElementById('image').style.border = '1px solid red';
                            document.listcreate.image.focus();
                            return false;
                    }
                    else
                    {        
                            document.getElementById('imgreq').style.display = "none";
                            document.getElementById('image').style.border = 'none';
                            if( f == "block")
                                {
                                    document.getElementById('invalid').style.display = "block"
                                    document.listcreate.title.focus();
                                    return false;
                                }
                    }
          }
 
 $('#loadingimg').show(); 
       $('#listcreate').ajaxSubmit({
                dataType: "json",
		success: function(data)
                {
                     if(data != 0)
                     {
                          $('#submitform').hide();
                          $('#loadingimg').hide();
                          $('#updatelist').show();
                          $('#title').replaceWith('<input type="text" value="'+data.question+'" readonly="readonly" class="forgotpasinpu" style="margin-left: -4px;" name="ListForm[title]" id="title">');
                          
                          if(data.friends != 'undefined')
                          {
                             var url = baseurl+"/index.php/opinion/pollsvalue?pollid="+data.id+"&question="+data.question+"&image="+data.image+"&interest="+data.interest+"&creator="+data.creator+"&friends="+data.friends;     
                          }
                          else
                          {
                             var url = baseurl+"/index.php/opinion/pollsvalue?pollid="+data.id+"&question="+data.question+"&image="+data.image+"&interest="+data.interest+"&creator="+data.creator;     
                          }
                          $.colorbox({iframe:true, width:"704px", height:"820px;", opacity:0.3, href:url});
                          var myDiv = $("#cboxLoadedContent");  
                          $("#cboxLoadedContent").height(myDiv.height()+parseInt('28'));
                     }
                     else
                     {
                          $('#loadingimg').hide();
                          $('#updatelist').hide();
                          $('#submitform').show();   
                     }
                }
      });
       return false;
}
function popuppollsaves()
{
     $('#lodinggif').show(); 
     $.ajax({
            type: $('#popuplistsave').attr('method'),
            url:  $('#popuplistsave').attr('action'),
            data: $('#popuplistsave').serialize(),
            dataType: "json",
            success: function (data) {  
                $('#lodinggif').hide(); 
                var action = $('#popuplistsave').attr('action').split('opinion');
                var target = action[0]+"opinion/detailpoll?pollid="+data.pollid;
                parent.$.fn.colorbox.close();
                self.parent.location=target;
            }
        });
      return false;
}
function checkradionbtn()
{
    var vals = $("#listcreate input[type='radio']:checked").val();
    
    if(vals == undefined)
        {
            alert("Please select your answer first.");
            return false;
        }
        else
          {
              $('#lodinggif').show(); 
              return true;
          }
        
           
}

function deletepoll()
{
    var r=confirm("Do you want to delete poll!");
    if (r==true)
      {  
          return true;
      }
      else
        {
            return false;
        }
}


function showplushover()
{
   document.getElementById("plussign").src = "../../newimages/plus_hover.png";   
    
}
function hideplushover()
{
   document.getElementById("plussign").src = "../../newimages/plus.png";   
    
}
function showminushover()
{
   document.getElementById("minussign").src = "../../newimages/minus_hover.png";   
    
}
function hideminushover()
{
   document.getElementById("minussign").src = "../../newimages/minus.png";   
    
}
function showcompany(id)
{
    if(id=="personal")
        {
            //document.getElementById('conternt_left').style.height = "1047px";
            document.getElementById(id).style.display = "block";
            document.getElementById('company').style.display = "none";
            document.getElementById('detail_s').style.display = "none";
            document.getElementById('minus').style.display = "block";
            document.getElementById('plus').style.display = "none";
            document.getElementById('onclick').value = '';
            document.getElementById('onclick').value = id+'click';
            document.getElementById('errordiv_submit').style.display = "none";
        }
    else
        {
            //document.getElementById('conternt_left').style.height = "1215px";
            document.getElementById(id).style.display = "block";
            document.getElementById('personal').style.display = "none";
            document.getElementById('onclick').value = '';
            document.getElementById('onclick').value = id+'click';
            document.getElementById('errordiv_submit').style.display = "none";
        }
    
}

function searchofficial_recent()
{ 
     var string         = document.getElementById("companytypings_off_recent").value;
            
     var arraystring    = document.getElementById("hiddeninterest_company").value;
     
     jQuery("#editdiv").hide();   
     jQuery("#editdiv2").hide();     
     
      baseurl = document.getElementById("baseurlclick").value;
     
     if(trim(string).length>0)
     {          
         document.getElementById('loding_li_img').style.display = "block";
         
          var url = baseurl+'/index.php/user/userinterest';
          jQuery.ajax({ 
                type:'POST',
                url:url,
                //datatype:'json',
                data:{
                        string:string ,
                        arraystring:arraystring
                     },
                     success:function(value){
                         document.getElementById('loding_li_img').style.display = "none"; 
                         if(value!=1) 
                          {  
                              jQuery("#editdiv").show();     
                              jQuery("#editdiv2").show();     
                              jQuery("#editdiv2").html(value).show();   
                              $("#editdiv2").scrollTop(0) 
                              var countItms = $("#userselectinterest li").size();
                                      if(countItms<13)
                                       { 
                                              if(countItms==1)
                                                  {
                                                      $("#editdiv2").css('height','32px');
                                                      $("#editdiv").css('overflow','hidden');
                                                      
                                                  }
                                               else if(countItms==2)
                                                  {
                                                      $("#editdiv2").css('height','54px');
                                                      $("#editdiv").css('overflow','hidden');
                                                  }
                                               else if(countItms==3)
                                                  {
                                                      $("#editdiv2").css('height','80px');
                                                      $("#editdiv").css('overflow','hidden');
                                                  }
                                               else if(countItms==4)
                                                  {
                                                      $("#editdiv2").css('height','107px');
                                                      $("#editdiv").css('overflow','hidden');
                                                  }
                                               else if(countItms==5)
                                                  {
                                                      $("#editdiv2").css('height','135px'); 
                                                      $("#editdiv2").css('overflow','hidden');
                                                  }
                                              else if(countItms==6)
                                                  {
                                                      $("#editdiv2").css('height','160px');  
                                                      $("#editdiv").css('overflow','hidden');
                                                  }
                                               else if(countItms==7)
                                                  {
                                                      $("#editdiv2").css('height','183px'); 
                                                      $("#editdiv").css('overflow','hidden');
                                                  }
                                               else if(countItms==8)
                                                  {
                                                      $("#editdiv2").css('height','208px'); 
                                                      $("#editdiv").css('overflow','hidden');
                                                  }
                                               else if(countItms==9)
                                                  {
                                                      $("#editdiv2").css('height','238px');
                                                      $("#editdiv").css('overflow','hidden');
                                                      
                                                  }
                                              else if(countItms==10)
                                                  {
                                                      $("#editdiv2").css('height','265px');
                                                      $("#editdiv").css('overflow','hidden');
                                                      
                                                  }
                                              else if(countItms==11)
                                                  {
                                                      $("#editdiv2").css('height','288px');
                                                      $("#editdiv").css('overflow','hidden');
                                                      
                                                  }
                                                  
                                              else if(countItms==12)
                                                  {
                                                      $("#editdiv2").css('height','315px');
                                                      $("#editdiv").css('overflow','hidden');
                                                      
                                                  }
                                       }
                                       else
                                          {
                                               $("#editdiv2").css('height','340px');
                                               $("#editdiv").css('overflow','auto');
                                          }
                          } 


            }
            }); 
    
     }
}


function searchofficial()
{ 
     var string         = document.getElementById("companytypings_off").value;
            
     var arraystring    = document.getElementById("hiddeninterest_company").value;
     
     jQuery("#editdiv").hide();     
     
      baseurl = document.getElementById("baseurlclick").value;
     
     if(trim(string).length>0)
     {          
         document.getElementById('loding_li_img').style.display = "block";
         
          var url = baseurl+'/index.php/user/userinterest';
          jQuery.ajax({ 
                type:'POST',
                url:url,
                //datatype:'json',
                data:{
                        string:string ,
                        arraystring:arraystring
                     },
                     success:function(value){              
                         if(value!=1) 
                          {  
                              document.getElementById('loding_li_img').style.display = "none";                              
                              jQuery("#editdiv").html(value).show();                               
                          } 


            }
            }); 
    
     }
}
function interstauto(baseurl)
{    
    var onclick = document.getElementById("onclick").value;
    if(onclick == 'personalclick'){
    
     var string         = document.getElementById("typings").value;
     
     var arraystring    = document.getElementById("hiddeninterest").value;
     var currenturl    = document.getElementById("currenturl").value;
   
     if(trim(string).length>0)
     { 
         if(currenturl!='myProfile' && currenturl!="listtitle"){
         if(arraystring!='')
             {
				$('#div_typing').removeClass('error');
				/*
                   document.getElementById("div_typing").style.background = "url(../../newimages/email_bg_interest.png) no-repeat";
                   document.getElementById("div_typing").style.borderLeft='2px solid #E0E0E0'; 
                   document.getElementById("div_typing").style.borderBottom='1px solid #E0E0E0'; 
                   document.getElementById("div_typing").style.borderRight='1px solid #E0E0E0'; 
                   document.getElementById("div_typing").style.borderRadius='11px 0px 0px 11px';
				*/
                   document.getElementById('spaninterest').style.display = "none";
             }
         }
         //document.getElementById('loding_li_img').style.display = "block";
         baseurl  = document.getElementById("baseurl").value;
          var url = baseurl+'/index.php/user/userinterest';
          jQuery.ajax({ 
                type:'POST',
                url:url,
                //datatype:'json',
                data:{
                        string:string ,
                        arraystring:arraystring
                     },
                     success:function(value){                  
                         if(value!=1) 
                          {   
                              //document.getElementById('loding_li_img').style.display = "none";                              
                              jQuery("#editdiv").html(value).show();                      
                          } 


            }
            }); 
    
     }
    }
    else
      {
             var companytypings         = document.getElementById("companytypings").value;
     
             var arraycompanytypings    = document.getElementById("hiddeninterest_company").value;

             if(trim(companytypings).length>0)
             {
                 
                 
                 if(arraycompanytypings!='')
                 {
					$('#div_companytypings').removeClass('error');
					/*
                   document.getElementById("div_companytypings").style.background = "url(../../newimages/email_bg_interest.png) no-repeat";
                   document.getElementById("div_companytypings").style.borderLeft='2px solid #E0E0E0'; 
                   document.getElementById("div_companytypings").style.borderBottom='1px solid #E0E0E0'; 
                   document.getElementById("div_companytypings").style.borderRight='1px solid #E0E0E0'; 
                   document.getElementById("div_companytypings").style.borderRadius='11px 0px 0px 11px';
					*/
                   document.getElementById('spaninterest_com').style.display = "none";
                 }
             
                 
                 //document.getElementById('loding_li_img_company').style.display = "block";
                 baseurl  = document.getElementById("baseurl").value;
                  var urls = baseurl+'/index.php/user/userinterest';
                  jQuery.ajax({ 
                        type:'POST',
                        url:urls,
                        //datatype:'json',
                        data:{
                                string:companytypings ,
                                arraystring:arraycompanytypings
                             },
                             success:function(value){                  
                                 if(value!=1) 
                                  {   
                                      //document.getElementById('loding_li_img_company').style.display = "none";                              
                                      jQuery("#editdiv_company").html(value).show();                      
                                  } 


                    }
                    }); 

             }
            }
      }
    

function showavailable()
{
   jQuery('#showdiv_interest').toggle();
}

function showavailable_com()
{
    var show = document.getElementById('showdiv_interest_com').style.display;
    if(show=='none')
        {
            document.getElementById('showdiv_interest_com').style.display = 'block';
        }
    else
        {
            document.getElementById('showdiv_interest_com').style.display = 'none';
        }
}

function applyinterest()
{
    var onclick = document.getElementById("onclick").value;
    if(onclick == 'personalclick'){
		$('#div_typing').removeClass('error');
	    /*
        document.getElementById("div_typing").style.background   = "url(../../newimages/email_bg_interest.png) no-repeat";
        document.getElementById("div_typing").style.borderLeft   ='2px solid #E0E0E0'; 
        document.getElementById("div_typing").style.borderBottom ='1px solid #E0E0E0'; 
        document.getElementById("div_typing").style.borderRight  ='1px solid #E0E0E0'; 
        document.getElementById("div_typing").style.borderRadius ='11px 0px 0px 11px';
	    */
        document.getElementById('spaninterest').style.display    = 'none';
		
        var value = $("input[name=availablecheckbox]:checked").map(function () {return this.value;}).get().join(","); 

        var splits = value.split(",");

  
        if(value == '')
            { 
                //document.getElementById('showdiv_interest').style.display = 'none';
                //document.getElementById("hiddeninterest").value= '';            
                //document.getElementById("mainspan").innerHTML= '';

            }
        else
            {                
                document.getElementById("hiddeninterest").value= '';

                document.getElementById("mainspan").innerHTML= '';

                var i=0;
                for(i=0;i<splits.length;i++)
                    {                  
                
                               jQuery("#mainspan").append("<li><span>"+splits[i]+"</span><a href='javascript:void(0);' onclick='removeli(this)'>X</a></li>");

                               var hidden = jQuery("#hiddeninterest").val();
                                if(hidden=='')
                                  {
                                      var string  = splits[i];
                                      jQuery("#hiddeninterest").val(trim(string));
                                  }                           
                                else
                                  {

                                     var string1 = hidden+','+splits[i];
                                     jQuery("#hiddeninterest").val(trim(string1));
                                  }
                       

                    }
                    
                 document.getElementById('showdiv_interest').style.display = 'none';
                document.getElementById("typings").value = '';
            }
    }
    else
    {
		$('#div_companytypings').removeClass('error');
		/*
        document.getElementById("div_companytypings").style.background = "url(../../newimages/email_bg_interest.png) no-repeat";
        document.getElementById("div_companytypings").style.borderLeft='2px solid #E0E0E0'; 
        document.getElementById("div_companytypings").style.borderBottom='1px solid #E0E0E0'; 
        document.getElementById("div_companytypings").style.borderRight='1px solid #E0E0E0'; 
        document.getElementById("div_companytypings").style.borderRadius='11px 0px 0px 11px';
		*/
        document.getElementById('spaninterest_com').style.display = 'none';
        var values = $("input[name=availablecheckbox_com]:checked").map(function () {return this.value;}).get().join(","); 
         
        var splits_com = values.split(",");


        if(value == '')
            {
                //document.getElementById('showdiv_interest_com').style.display = 'none';
                //document.getElementById("hiddeninterest_company").value= '';            
                //document.getElementById("mainspan_com").innerHTML= '';

            }
        else
            {
                document.getElementById("hiddeninterest_company").value= '';

                document.getElementById("mainspan_com").innerHTML= '';

                var i=0;
                for(i=0;i<splits_com.length;i++)
                    {  
                        
                               jQuery("#mainspan_com").append("<li><span>"+splits_com[i]+"</span><a href='javascript:void(0);' onclick='removeli_com(this)'>X</a></li>");

                               var hidden = jQuery("#hiddeninterest_company").val();
                                if(hidden=='')
                                  {
                                      var string  = splits_com[i];
                                      jQuery("#hiddeninterest_company").val(trim(string));
                                  }                           
                                else
                                  {

                                     var string1 = hidden+','+splits_com[i];
                                     jQuery("#hiddeninterest_company").val(trim(string1));
                                  }
                        

                    }
                 document.getElementById('showdiv_interest_com').style.display = 'none';
                document.getElementById("companytypings").value = '';
            }
    }
}



function searchinterest(baseurl)
{
    var onclick = document.getElementById("onclick").value;
    if(onclick == 'personalclick'){
        var str = document.getElementById('serchint').value;
        //if(trim(str).length>0)
         {
             var currenturl = $("#currenturl").val();
             if(currenturl == 'myProfileCompany' || currenturl == 'myProfile')
             {
                 var applyinterest = "apply";
             }
             else
               {
                   var applyinterest = "";
               }
              var url = baseurl+'/index.php/user/searchinterestlist';
              jQuery.ajax({ 
                    type:'POST',
                    url:url,
                    //datatype:'json',
                    data:{
                            string:str,                     
                            applyinterest:applyinterest
                         },
                         success:function(value){                  
                             if(value!=1) 
                              {      
                                  jQuery(".interest_div_CHECK").remove();       
                                  jQuery("#showdiv_interest").html(value).show();                      
                              } 


                }
                });
         }
    }
    else
     { 
         var company ="company";
             var str1 = document.getElementById('serchint_com').value;
            //if(trim(str).length>0)
            
             {
                  var urls = baseurl+'/index.php/user/searchinterestlist';
                  jQuery.ajax({ 
                        type:'POST',
                        url:urls,
                        //datatype:'json',
                        data:{
                                string:str1,
                                company:company
                             },
                             success:function(value){                  
                                 if(value!=1) 
                                  {      
                                      jQuery(".interest_div_CHECK_com").remove();       
                                      jQuery("#showdiv_interest_com").html(value).show();                      
                                  } 


                    }
                    });
             }
     }
}
function applyeducation()
{ 
    var value = $("input[name=addmorecheckbox]:checked").map(function () {return this.value;}).get().join(","); 
    
    var splits = value.split(",");
    
     
    if(value == '')
        {
            
            document.getElementById('addmoremain').style.display = 'none';
            document.getElementById("hiddeneducation").value= '';            
            
        }
    else
        {            
            document.getElementById("hiddeneducation").value= '';       
            
            var i =0; 
            for(i=0;i<splits.length;i++)
                { 
                         
                           var hidden = jQuery("#hiddeneducation").val();
                           
                            if(hidden == '')
                              {
                                  var string  = splits[i];
                                  jQuery("#hiddeneducation").val(trim(string));
                              }                            
                            else
                              {                                   
                                   var string1  = hidden+','+splits[i];
                                   jQuery("#hiddeneducation").val(trim(string1));
                              }
                    
                   
                }
             document.getElementById('addmoremain').style.display = 'none';
            
        }
}

function showmoreeducation()
{
     var show = document.getElementById('addmore_div2').style.display;
    if(show=='none')
        {
            document.getElementById('addmor').innerHTML = "Less";
            document.getElementById('addmore_div2').style.display = 'block';
            //document.getElementById('hiddeneducation').value = "";
        }
    else
        {
            document.getElementById('addmor').innerHTML = "Add more...";
            document.getElementById('addmore_div2').style.display = 'none';
            //document.getElementById('schoolerror').style.display = 'none';
        }
}



function messagepopup_open(id,name)
{
    if(id){
    document.getElementById('popupContact2').style.display = 'none';
    document.getElementById('messagesent').style.display = 'none';
    document.getElementById('popupContact1_msgclick').style.display = 'block';
    document.getElementById('popupContact1_msgclick').style.position = 'absolute';    
    document.getElementById('receveridmessage').value = id+',';
    document.getElementById('recever_div_header1').style.border="2px solid #CED2D8"; 
    document.getElementById('compo').style.border="2px solid #CED2D8";           
    var textarea = document.getElementById('testaraheader').value;
    if(textarea!='Telling them what?')
        {
            document.getElementById('testaraheader').value = 'Telling them what?';
        }
    $('#compose_heaserspan').html('');
    var baseurlclick = document.getElementById("baseurl").value;
    $("#compose_heaserspan").append("<li><span>"+name+"</span><input type='hidden' value='"+id+"' class='cmpsemsgcls'/><a href='javascript:void(0);' onclick='removeli_header(this)'><img  src='"+baseurlclick+"/images/cross.png' class='imagespan_official'/></a></li>");
         
    
    }
    else{
        
    document.getElementById('messagesent').style.display = 'none';    
    document.getElementById('popupContact1').style.display = 'none';
    document.getElementById('popupContact1_msgclick').style.display = 'block';
    document.getElementById('popupContact1_msgclick').style.position = 'absolute';
    document.getElementById('recever_div_header1').style.border="2px solid #CED2D8";      
    document.getElementById('compo').style.border="2px solid #CED2D8";           
    document.getElementById('recever1').value = '';
    var textarea = document.getElementById('testaraheader').value;
    if(textarea!='Telling them what?')
        {
            document.getElementById('testaraheader').value = 'Telling them what?';
        }
    $('#compose_heaserspan').html('');
    }
      
}
function messagepopup_input(baseurl)
{
    var string = document.getElementById('recever1').value;
    baseurl = document.getElementById("baseurl").value;
    //jQuery("#editdivss").hide();   
    
    if(string.length>0)
        {
            var myfriendsid = document.getElementById('myfriendsid').value;
            var arrayid     = document.getElementById('receveridmessage').value;
            var url = baseurl+"/index.php/preferences/composemsg"
            document.getElementById('loding_imgheader').style.display = 'block';
            
            jQuery.ajax({ 
                        type:'POST',
                        url:url,
                        //dataType:'json',
                        data:{
                                title:string,
                                frnd:myfriendsid,
                                arrayid:arrayid
                             },
                             success:function(value){   
                                 document.getElementById('loding_imgheader').style.display = 'none';
                                 if(value!=1) 
                                  {    
                                      //document.getElementById('recever1').value = '';
                                      document.getElementById('recever1').focus();
                                      jQuery("#editdivss").html(value).show();           
                                  } 


                    }
                    });
        }
}

function saveheadermsg(baseurl)
{
    var value   = document.getElementById('testaraheader').value;
    var recever = document.getElementById('receveridmessage').value;
    
    var recever1   = document.getElementById('receveridmessage').value;
    if(recever1=='')
      {
          document.getElementById('recever_div_header1').style.border="2px solid red";
          document.getElementById('messagesent').style.color = 'red';
          document.getElementById('messagesent').style.display = 'block';
          document.getElementById('messagesent').innerHTML = "Please enter To address";
          return false;
      }
      
      else
        {
            document.getElementById('recever_div_header1').style.border="2px solid #CED2D8";            
            document.getElementById('messagesent').style.color = 'green';
            document.getElementById('messagesent').style.display = 'none';
            document.getElementById('messagesent').innerHTML = "Message sent successfully";
        }
        
     if(value == 'Telling them what?')
     {
         document.getElementById('compo').style.border="2px solid red";
         document.getElementById('messagesent').style.color = 'red';
         document.getElementById('messagesent').style.display = 'block';
         document.getElementById('messagesent').innerHTML = "Pease enter Message";
         return false;
     }
    
    else
      {
            document.getElementById('compo').style.border="2px solid #CED2D8";            
            document.getElementById('messagesent').style.color = 'green';
            document.getElementById('messagesent').style.display = 'none';
            document.getElementById('messagesent').innerHTML = "Message sent successfully";
      }
      
     if(trim(value)=='')
     {
         document.getElementById('compo').style.border="2px solid red";
         document.getElementById('messagesent').style.color = 'red';
         document.getElementById('messagesent').style.display = 'block';
         document.getElementById('messagesent').innerHTML = "Pease enter Message";
         return false;
     }
     else
      {
            document.getElementById('compo').style.border="2px solid #CED2D8";            
            document.getElementById('messagesent').style.color = 'green';
            document.getElementById('messagesent').style.display = 'none';
            document.getElementById('messagesent').innerHTML = "Message sent successfully";
      }
    if(trim(value).length>0 && value != 'Telling them what?' && recever.length>0)
        {
            
            var url = baseurl+"/index.php/preferences/headermessage"
            document.getElementById('msglodingf').style.display = 'block';
            
            jQuery.ajax({ 
                        type:'POST',
                        url:url,
                        dataType:'json',
                        data:{
                                recever:recever,
                                body:value
                             },
                             success:function(value){   
                                 document.getElementById('msglodingf').style.display = 'none';
                                 document.getElementById('backgroundPopup1').style.display = 'none';  
                                 if(value!=1) 
                                  {      
                                     
                                     document.getElementById('messagesent').style.display = 'block';  
                                     setTimeout(function(){
                                          document.getElementById('popupContact1_msgclick').style.display = 'none';
                                       }, 2000);
                                     
                                  } 
                                  


                    }
                    });
        }
}

var defSearchText = "Search lists by title, topics or people";
function textformatserch() 
{
    var value = document.getElementById('serchheader').value;   
    if (value == '')
    {
        document.getElementById('serchheader').value = defSearchText; 
        document.getElementById('serchheader').style.fontStyle = "italic"; 
        document.getElementById('serchheader').style.color = "#8B8B8B"; 
    }
    else
    { 
        document.getElementById('serchheader').style.fontStyle = "normal";
        document.getElementById('serchheader').style.color = "#565656"; 
    }
}

function headerSearchValue(val)
{
	if (val == defSearchText)
    {
        document.getElementById('serchheader').value = "";
    }
}

function textformalstyle()
{
    document.getElementById('serchheader').style.fontStyle = "normal"; 
    document.getElementById('serchheader').style.color = "#565656";     
}


function textformatserch_recentsearch() 
{
    var value   = document.getElementById('serchheader_recentsearch').value;
    if(value=='')
    {
        document.getElementById('serchheader_recentsearch').value = defSearchText; 
        document.getElementById('serchheader_recentsearch').style.fontStyle = "italic"; 
        document.getElementById('serchheader_recentsearch').style.color = "#8B8B8B"; 
    }
    else
    { 
        document.getElementById('serchheader_recentsearch').style.fontStyle = "normal";
        document.getElementById('serchheader_recentsearch').style.color = "#565656"; 
    }
}

function textformalstyle_recentsearch()
{
    document.getElementById('serchheader_recentsearch').style.fontStyle = "normal"; 
    document.getElementById('serchheader_recentsearch').style.color = "#565656";     
}

function logoutdivopen()
{
    var show = document.getElementById('popuplogout').style.display;
    if(show =='none')
        {            
            document.getElementById('backgroundPopup3').style.display = 'block';
            document.getElementById('popuplogout').style.display = 'block';
        }
    else
        {            
            document.getElementById('popuplogout').style.display = 'none';
        }
}

function favoritedivopen()
{
    
    var show = document.getElementById('popupfavorite').style.display;
    if(show =='none')
        {    
            $('#favoritediv_id').removeClass('cursorpointer').addClass('activeclass_favorite');
            var baseurl = document.getElementById('baseurl').value;
            var image = baseurl+"/images/drop_down_black.png";
            $('#droupdownimg').attr('src',image);
            document.getElementById('backgroundPopup4').style.display = 'block';
            document.getElementById('popupfavorite').style.display = 'block';
        }
    else
        {   
            $('#favoritediv_id').removeClass('activeclass_favorite').addClass('cursorpointer');
            var baseurl = document.getElementById('baseurl').value;
            var image = baseurl+"/images/drop_down.png";
            $('#droupdownimg').attr('src',image);
            document.getElementById('popupfavorite').style.display = 'none';
        }
}

function clickfocuinterst()
{
    document.getElementById('typings').focus();
}


function focusontopics()
{
    document.getElementById('companytypings_off_recent').focus();
}

function cmpgmsgfocus()
{
    document.getElementById('recever1').focus();
}
function recentmsgfocus()
{
    document.getElementById('friend_name_recent').focus();
}
function recent_about_focus()
{
    document.getElementById('friend_about_recent').focus();
}

function setSearchValues() {
    $("#serchheader")
        .css("fontStyle", "normal")
        .css("color", "#565656")
        .focus()
        .val('') // move cursor to the end of text
        .val($("#serchheader_recentsearch").val());
}

function getSearchValues() {
    var values = document.getElementById('serchheader').value;
    if (values != defSearchText) { 
        return values;
    }
    return "";
}

function serchonrecentHeader() {
    var values = document.getElementById('serchheader').value;
    if (values != defSearchText) { 
        serchofficiallist();
    }    
}    

function serchofficiallist()
{
    var values = $('#serchheader').val();
    var tval = values.replace(/^\s+|\s+$/g, '');
    
    var baseurlclick = $('#baseurl').val();
    var pathArray = window.location.pathname.split( '/' );
    var currenturl = pathArray[pathArray.length - 1];

    if (currenturl.lastIndexOf("recentSearch", 0) === 0) {     
        if (tval.length >= 0)  
        {   
            $('#officialtodatlodingdiv').hide();
            $("#pagingdiv").hide();
            
            $("#serchheader_recentsearch").val(values);

            var url = baseurlclick + "/index.php/preferences/" + currenturl + "Pagination" ;
   
            // new code
            ajaxSearch(url, 
                    currenturl === "recentSearch" || currenturl === "recentSearchLists"
                        ? 'contentdiv' : '_sr', values);
        }
    } else {
        var recentSearch = "recentSearch";
        var url = baseurlclick + "/index.php/preferences/saveSearchString";
        jQuery.ajax({ 
            type: 'POST',
            url: url,
            data: {
                value: values,
                recentSearch: recentSearch
            }, 
            success: function(value) {   
                document.location.href = baseurlclick + "/index.php/preferences/recentSearch";
            }
        });
    }
}

function updateSearchCounts(jsonResp) {   
    if (jsonResp == undefined) {
        $("#recentSearchListsLink").text("Lists (" + $("#searchListCount").val() + ")");
        $("#recentSearchPeopleLink").text("People (" + $("#searchPeopleCount").val() + ")");
        $("#recentSearchCompaniesLink").text("Companies (" + $("#searchCompaniesCount").val() + ")");
    } else {
        $("#recentSearchListsLink").text("Lists (" + jsonResp.searchListCount + ")");
        $("#recentSearchPeopleLink").text("People (" + jsonResp.searchPeopleCount + ")");
        $("#recentSearchCompaniesLink").text("Companies (" + jsonResp.searchCompaniesCount + ")");
    }
}

function ajaxSearch(searchUrl, targetId, searchStr, data) {
    if ($('#officialtodatlodingdiv').css("display") != "none") {
        return;
    }
    
    $('#officialtodatlodingdiv').show();
    
	if (data == undefined) data = { };
	data.pageno = 0;
	data.value = searchStr;

	// clear search result content
	target = $('#' + targetId);
	target.empty();
	
    $.ajax({
        type: 'POST',
        url: searchUrl,
        data: data, 
        success: function (resp) {
    			if (typeof FW != 'undefined'
    			    && typeof FW.templates != 'undefined'
    			    && FW.templates.items) {
    				var resp_json = $.parseJSON(resp);
    				target.append(FW.toHTML(resp_json.data, 'items', true));
    				updateSearchCounts(resp_json);
    			} else {
    			    target.append(resp);    		
    				updateSearchCounts();
    			}
    			
                resetPagination(target);
                $('#officialtodatlodingdiv').hide();
        }
    });
}

function at_bottom_of_page_old() {
    return $(window).scrollTop() + $(window).height() > $(document).height() - 200;
}

function initPagination_old(contentEl, pagingUrl) {
    contentEl.attr("pagePending", false);
    contentEl.attr("hasNextPage", true);    

    $(window).scroll(function() { 
        if (contentEl.attr("pagePending") == "true" || contentEl.attr("hasNextPage") == "false") {            
            return;
        } 
                      
        if (at_bottom_of_page_old()) {
            contentEl.attr("pagePending", true);
            
            var pageno = $('#pageno_off').val();                
            $('#officialtodatlodingdiv').css("display", "block");
            
            $.ajax({ 
                type:'POST',
                url: pagingUrl,
                data: { pageno: pageno }, 
                success: function (resp) {   
                    $('#officialtodatlodingdiv').css("display", "none");
                    contentEl.attr("pagePending", false);
                    if (resp) {
                        if (typeof FW != 'undefined'
                            && typeof FW.templates != 'undefined'
                            && FW.templates.items
                            && (resp[0] === '{')) {
							var resp_json = $.parseJSON(resp);
							if (resp_json.data.length === 0) {
								contentEl.attr("hasNextPage", false);
							} else {
								contentEl.append(FW.toHTML(resp_json.data, 'items', true));
								$('#pageno_off').val(parseInt($('#pageno_off').val(), 10) + 1);                    
							}
							updateSearchCounts(resp_json);
						} else {
							contentEl.append(resp); 
							$('#pageno_off').val(parseInt($('#pageno_off').val(), 10) + 1);
							updateSearchCounts();
						}
                    } else {
                        contentEl.attr("hasNextPage", false);
                        // TODO: enable by timer again
                    }
                }
            });
        }
    });
}

function resetPagination(contentEl) {
    $('#pageno_off').val("1");
    contentEl.attr("hasNextPage", true);
}


$(function() {
    $(document).on('keyup', '#serchheader', function(evnt)
      {
            if(evnt.which==13)
              {
                 serchofficiallist();
              }
                
      })
      
      
      $(document).on('keyup', '#serchheader_recentsearch', function(evnt)
      {
            if(evnt.which==13)
              {
                 recentSearch();
              }
                
      })
      
       $(document).on('keyup', '#serchheader_recentsearch2', function(evnt)
      {
            if(evnt.which==13)
              {
                 recentSearch2();
              }
                
      })
    
    
    
    $(document).on('keyup', '#companytypings_off', function(evnt)
      { 
          
          if(evnt.which==40 || evnt.which==38)
                {
                    return false;   //@stop ajax request to create the new list again
                }
            if(evnt.which==13 && $("#editdiv").is(':visible')==true)
              {
                  $(".interest_div_li.active").click();                  
                  return false;
              }
              
            if(evnt.which==13)
              {
                  officialshuffel();                  
              }
            
                searchofficial('base',evnt.keyCode);
          
      })
      
      
      
      $(document).on('keyup', '#companytypings_off_recent', function(evnt)
      { 
          
          if(evnt.which==40 || evnt.which==38)
                {
                    return false;   //@stop ajax request to create the new list again
                }
            if(evnt.which==13 && $("#editdiv").is(':visible')==true)
              {
                  $(".interest_div_li.active").click();                  
                  return false;
              }
              
            if(evnt.which==13)
              {
                  officialshuffel_recent();                  
              }
            
                searchofficial_recent('base',evnt.keyCode);
          
      })
      
      
      $(document).on('keyup', '#recever1', function(evnt)
      { 
          
          if(evnt.which==40 || evnt.which==38)
                {
                    return false;   //@stop ajax request to create the new list again
                }
            if(evnt.which==13)
              {
                  $(".interest_div_li_msg.active").click();                  
                  return false;
              }
                messagepopup_input('base',evnt.keyCode);
          
      })
      
      $(document).on('keyup', '#typings,#companytypings', function(evnt)
      { 
          
          if(evnt.which==40 || evnt.which==38)
                {
                    return false;   //@stop ajax request to create the new list again
                }
            if(evnt.which==13)
              {
                  $(".interest_div_li.active").click();
                  return false;
              }
                interstauto('base',evnt.keyCode);
          
      })
      
      
      $(document).on('keyup', '#friend_name_recent', function(evnt)
      { 
          
          if(evnt.which==40 || evnt.which==38)
                {
                    return false;   //@stop ajax request to create the new list again
                }
            if(evnt.which==13 && $("#value_div").is(':visible')==true)
              {
                  $(".interest_div_li_msg.active").click();                  
                  return false;
              }
              
            if(evnt.which==13)
              {
                  refineFilterSearch();
              }
                
                recentSearch_myfrnd('base',evnt.keyCode);
          
      })
      
      
  
    
    $(document).on('keydown', '#companytypings,#typings,#companytypings_off,#companytypings_off_recent', function(evnt)
    { 
     var  prevKeyPressCode = evnt.keyCode;
      
      switch(prevKeyPressCode) {
                                    case 38: // up
                                        
                                                   evnt.preventDefault();
                                                   var ulid = '#userselectinterest';
                                                   UpDownKeySelection("up",ulid);
                                                   break;
                                    case 40: // down
                                                   evnt.preventDefault();
                                                   var ulid = '#userselectinterest';
                                                   UpDownKeySelection("down",ulid);
                                                   break;
                                                   
                                 
                            }
  })
  
  
   $(document).on('keydown', '#recever1,#friend_name_recent', function(evt)
    { 
      var prevKeyPressCode = evt.keyCode;
      
      switch(prevKeyPressCode) {
                                    case 38: // up
                                        
                                                   evt.preventDefault();
                                                   var ulid = '#headercompgmsg';
                                                   
                                                   UpDownKeySelection("up",ulid);
                                                   break;
                                    case 40: // down
                                                   evt.preventDefault();
                                                   var ulid = '#headercompgmsg';
                                                   
                                                   UpDownKeySelection("down",ulid);
                                                   break;
                                                   
                                 
                            }
  })
    



  $(document).on('keydown', '.entry_listentryodd,.entry_listentryeven', function(evnt)
    { 
      prevKeyPressCode = evnt.keyCode;
      
      switch(prevKeyPressCode) {
                                    case 38: // up  
                                        
                                                   evnt.preventDefault();
                                                   var ulid = '.popuplistcreation_ul';
                                                   UpDownKeySelection("up",ulid);
                                                   break;
                                    case 40: // down
                                        
                                                   evnt.preventDefault();
                                                   var ulid = '.popuplistcreation_ul';
                                                   UpDownKeySelection("down",ulid);
                                                   break;
                                    case 13: // down
                                            $(".popuplistcreation_ul_li_controller_new.active").click();      
                                            $(".popuplistcreation_ul_li_controller.active").click();           
                                 
                            }
  })
  
}); //ready

function UpDownKeySelection(direction,id){
 
        if($(id).is(':visible')==true && $(id).length > 0){
         
                var lis = $(id+" li");
                if(direction == "down"){
                       var start =  $(id+" li:first");
                     
                       if($("#currenturl").val()=='recentSearch')
                           {    
                               if($('li.active').index() != -1)                              
                                  {
                                      if(id=='#headercompgmsg')
                                      {
                                          var y = $("#value_div2").scrollTop(); 
                                          $("#value_div2").scrollTop(y+30);
                                      }
                                         
                                      else
                                         {
                                             var y = $("#editdiv2").scrollTop(); 
                                             $("#editdiv2").scrollTop(y+15);
                                         }
                                  }
                                  
                                
                           }
                      
                            
                } else {
                       var start =  $(id+" li:last");
                       
                       if($("#currenturl").val()=='recentSearch')
                           {
                               var count = $(id+' li').size()-1;
                       
                               if($('li.active').index() != count)         
                                  {
                                      if(id=='#headercompgmsg')
                                      {
                                          var y = $("#value_div2").scrollTop(); 
                                          $("#value_div2").scrollTop(y-30);
                                      }
                                         
                                      else
                                         {
                                             var y = $("#editdiv2").scrollTop(); 
                                             $("#editdiv2").scrollTop(y-15);
                                         }
                                  }
                                
                           }
                }	
                                
                
                var active = $("li.active:first");
                if(active.length > 0){
                        if(direction == "down"){
                        start = active.next();
                        } else {
                                start = active.prev();
                        }	
                }
                lis.removeClass("active");
                start.addClass("active");
              
      }
        
    }

function recentSearch()
{
    var value   = document.getElementById('serchheader_recentsearch').value;
    var baseurlclick = document.getElementById('baseurl').value;
    
        
    if(trim(value).length>=0)  
      {   
          
          document.getElementById('pageno_off').value = 1;
          var pageno      = document.getElementById('pageno_off').value-1;
          
          var recentpage  = 'recent';
          var myfriendsid = document.getElementById('myfriendsid').value;
          var url         = baseurlclick+"/index.php/preferences/searchOnRecentSection";
          jQuery("#contentdiv").html('');
          document.getElementById('officialtodatlodingdiv').style.display = 'block';
          
          jQuery.ajax({ 
                        type:'POST',
                        url:url,
                        dataType:'json',
                        data:{
                                value:value,
                                myfriendsid:myfriendsid,
                                recent:recentpage,
                                pageno:pageno
                             },
                             success:function(value){   
                                 document.getElementById('officialtodatlodingdiv').style.display = 'none';
                                 if(value.html!=1) 
                                  { 
                                      
                                     jQuery("#contentdiv").html(value.html).show();  
                                     
                                     $("#maximaumpageno").val(value.record)
                                  }
                                  
                    }
                    });
      }
}


function open_recentSearch_frndallfriends()
{
    var show = document.getElementById('space_recentdiv').style.display;
    if(show =='none')
        {  
            document.getElementById('space_recentdiv').style.display = 'block';
        }
    else
        { 
            document.getElementById('space_recentdiv').style.display = 'none';
        }
}


function recentSearch_myfrnd(baseurl)
{  
    var string = document.getElementById('friend_name_recent').value;
    baseurl = document.getElementById("baseurl").value;
    jQuery("#value_div2").hide();  
    jQuery("#value_div").hide(); 
    var arrayid      = document.getElementById('receverid_friend').value;
    
    if(trim(string).length>0)
        {    
            var recent       = 'recent';
            
            var myfriendsid  = document.getElementById('myfriendsid_recent').value;
            var url          = baseurl+"/index.php/preferences/composemsg";
            document.getElementById('loding_img_recent').style.display = 'block';
            
            jQuery.ajax({ 
                        type:'POST',
                        url:url,
                        //dataType:'json',
                        data:{
                                title:string,
                                frnd:myfriendsid,
                                arrayid:arrayid,
                                recent:recent
                             },
                             success:function(value){   
                                 document.getElementById('loding_img_recent').style.display = 'none';
                                 if(value!=1) 
                                  {  
                                      jQuery("#value_div").show(); 
                                      jQuery("#value_div1").show();     
                                      document.getElementById('friend_name_recent').focus();
                                      jQuery("#value_div2").html(value).show(); 
                                      $("#value_div2").scrollTop(0)
                                      var countItms = $("#headercompgmsg li").size();
                                      if(countItms<10)
                                       { 
                                              if(countItms==1)
                                                  {
                                                      $("#value_div").css('height','45px');
                                                      $("#value_div").css('overflow','hidden');
                                                      
                                                  }
                                               else if(countItms==2)
                                                  {
                                                      $("#value_div").css('height','90px');
                                                     $("#value_div").css('overflow','hidden');
                                                  }
                                               else if(countItms==3)
                                                  {
                                                      $("#value_div").css('height','125px');
                                                      $("#value_div").css('overflow','hidden');
                                                  }
                                               else if(countItms==4)
                                                  {
                                                      $("#value_div").css('height','157px');
                                                      $("#value_div").css('overflow','hidden');
                                                  }
                                               else if(countItms==5)
                                                  {
                                                      $("#value_div").css('height','185px'); 
                                                      $("#value_div").css('overflow','hidden');
                                                  }
                                              else if(countItms==6)
                                                  {
                                                      $("#value_div").css('height','220px');  
                                                      $("#value_div").css('overflow','hidden');
                                                  }
                                               else if(countItms==7)
                                                  {
                                                      $("#value_div").css('height','253px'); 
                                                      $("#value_div").css('overflow','hidden');
                                                  }
                                               else if(countItms==8)
                                                  {
                                                      $("#value_div").css('height','288px'); 
                                                      $("#value_div").css('overflow','hidden');
                                                  }
                                               else if(countItms==9)
                                                  {
                                                      $("#value_div").css('height','315px');
                                                      $("#value_div").css('overflow','hidden');
                                                      
                                                  }
                                       }
                                       else
                                          {
                                               $("#value_div").css('height','340px');
                                               $("#value_div").css('overflow','auto');
                                          }
                                  }

                    }
                    });
        }
}


function interstname_recentSearch(ob)
{ 
        var pp = jQuery(ob).text();
        var id = jQuery(ob).find('.testclassheader').val();
        
       var baseurlclick = document.getElementById("baseurl").value;    
       
       jQuery(ob).parents("ul").find("#recentsearch_spanname").append("<li><span>"+pp+"</span><input type='hidden' value='"+id+"' class='cmpsemsgcls'/><a href='javascript:void(0);' onclick='removeli_recentSearch(this)'><img  src='"+baseurlclick+"/images/cross.png' class='imagespan_official'/></a></li>");
                
        
        jQuery(ob).parents(".home_ulli_interest_recentsearch").hide();
        $(".home_ulli_interest1112").html('');


        var hidden = jQuery("#receverid_friend").val();
        if(hidden=='')
          {
              var string  = id+',';
              jQuery("#receverid_friend").val(trim(string));
          }
        else
          {

             var string1  = hidden+id+',';
             jQuery("#receverid_friend").val(trim(string1));
          }
          
          
          $("#myfriends").attr('checked', false);
          document.getElementById('friend_name_recent').value = '';
          //htmlunder_recent();

}


function removeli_recentSearch(ob)
{  
      
      var id            = $(ob).parents('li').find('.cmpsemsgcls').val();
      
      $(ob).parents('li').remove();  
      var recipients    =   $('#receverid_friend').val(); 
      
      var LeftRecipient =   recipients.replace(id+',',''); 
      
      $('#receverid_friend').val(LeftRecipient);      
     
     if($('#receverid_friend').val()=='')
     {
        // $("#myfriends").attr('checked', true);
     }
     //htmlunder_recent();
}

function htmlunder_recent(friendsid,radio)
{
    
    var value        = document.getElementById('serchheader_recentsearch').value;
    var baseurlclick = document.getElementById('baseurl').value;
   jQuery("#pagingdiv").hide(); 
    if(trim(value).length>=0)  
      {
          
          var radio = $("input[name='twintymost']:checked").val();
          var myfriendsid = null;
          if(friendsid)
            {
                myfriendsid = friendsid;
                var check = document.getElementById('myfriends').checked;                
            }
          else
            {       
                    var myfriendsid = document.getElementById('receverid_friend').value;
            }
            
          var about = $("#hiddeninterest").val();
          
          var strings = about.substring(0, about.length - 1);
          
          
          var url = baseurlclick+"/index.php/preferences/searchOnRefineFilter";
          
          jQuery("#contentdiv").html('');
          document.getElementById('officialtodatlodingdiv').style.display = 'block';
          
          jQuery.ajax({ 
                        type:'POST',
                        url:url,                       
                        data:{
                                value:value,
                                myfriendsid:myfriendsid,
                                radiobtn:radio,
                                about:strings                                
                                
                             },
                             success:function(value){   
                                 document.getElementById('officialtodatlodingdiv').style.display = 'none';
                                 if(value!=1) 
                                  {      
                                     jQuery("#contentdiv").html(value).show();          
                                  }
                                  
                    }
                    });
      }
}

/****************Show by topic***********************/

function officialshuffel(pageno)
{
    var value        = document.getElementById('hiddeninterest').value;
    var baseurlclick = document.getElementById('baseurl').value;    
        
    if(trim(value).length>=0)  
      {
          if(pageno == undefined )
           {
               var pageno = "0";
           }      
           
          jQuery("#pagingdiv").hide();
          var currentpage        = document.getElementById('currentpage').value;
          
          if(currentpage=='officialtoday')
              {
                  var url = baseurlclick+"/index.php/preferences/showByTopicOfficial";
              }
          else if(currentpage=='recommended')
              {
                  var url = baseurlclick+"/index.php/preferences/showByTopicRecommended";
              }
          else if(currentpage=='friends')
              {
                  var url = baseurlclick+"/index.php/preferences/showByTopicFriends";
              }
          else if(currentpage=='world')
              {
                  var url = baseurlclick+"/index.php/preferences/showByTopicWorld";
              }
          else if(currentpage=='showAllFavorite')
              {
                  var url = baseurlclick+"/index.php/preferences/showByTopicFavorite";
              }
          jQuery("#contentdiv").html('');          
          document.getElementById('officialtodatlodingdiv').style.display = 'block';
          
          jQuery.ajax({ 
                        type:'POST',
                        url:url,                       
                        data:{
                                value:value,
                                pageno:pageno
                             }, 
                             success:function(valuess){   
                                 document.getElementById('officialtodatlodingdiv').style.display = 'none';
                                 if(valuess!=1) 
                                  {      
                                     jQuery("#contentdiv").html(valuess).show();                                      
                                  }
                                  
                    }
                    });
      }
}

function favoriteover()
{
    $('#favoritediv_id').removeClass('cursorpointer').addClass('activeclass_favorite');
    var baseurl = document.getElementById('baseurl').value;
    var image = baseurl+"/images/drop_down_black.png";
    $('#droupdownimg').attr('src',image);
}

function favoriteout()
{
    $('#favoritediv_id').removeClass('activeclass_favorite').addClass('cursorpointer');
    var baseurl = document.getElementById('baseurl').value;
    var image = baseurl+"/images/drop_down.png";
    $('#droupdownimg').attr('src',image);
}


function officialPageination(pageno)
{
    var baseurlclick = document.getElementById('baseurl').value;    
    document.getElementById('officialtodatlodingdiv').style.display = 'block';
    document.getElementById('pagingdiv').style.display = 'none';
    jQuery("#contentdiv").hide();   
    var url = baseurlclick+"/index.php/preferences/officialPageination";
      jQuery.ajax({ 
                        type:'POST',
                        url:url,                       
                        data:{                               
                                pageno:pageno
                             }, 
                             success:function(valuess){   
                                 document.getElementById('officialtodatlodingdiv').style.display = 'none';
                                 if(valuess!=1) 
                                  {      
                                     jQuery("#contentdiv").html(valuess).show();                                      
                                  }
                                  
                    }
                    });
}

function favourite_out(id)
{
    document.getElementById("favourites"+id).className ="showalldiv_1_out";
    
}

function favourite_over(id)
{
    document.getElementById("favourites"+id).className ="showalldiv_1_over";
    //
}

function shortlistimg(id)
{
    var img = $('#shortimg'+id).first().attr('src').split("/");
    if(img[img.length-1] == 'on_btn1.png')
        {
            document.getElementById("shortimg"+id).src = "../../images/off_btn1.png";
            document.getElementById("shortlist_"+id).value =id+',off';
        }
   else
       {
           document.getElementById("shortimg"+id).src = "../../images/on_btn1.png";
           document.getElementById("shortlist_"+id).value = id+',on';
       }
    
}

function showunfavour(id)
{
    var block = document.getElementById("unfavourdiv"+id).style.display;
    if(block == 'block')
    {
        document.getElementById("unfavourdiv"+id).style.display ="none";
    }
    else{
      document.getElementById("unfavourdiv"+id).style.display ="block";
    }
}

function unfavourlist(id)
{
    var r=confirm("Are you sure!");
    var baseurlclick = $("#baseurlclick").val()
     if (r==true)
      {  
          document.location.href=baseurlclick+"/index.php/preferences/unfavourList?listid="+id;
          return true;
      }
     else
      {
          return false;
      }
}
function applyinterest_myprofile(baseurl)
{
    
          
        var value = $("input[name=availablecheckbox]:checked").map(function () {return this.value;}).get().join(","); 

        var splits = value.split(",");


        if(value == '')
            {
                document.getElementById('showdiv_interest').style.display = 'none';
                document.getElementById("hiddeninterest").value= '';            
                document.getElementById("mainspan").innerHTML= '';

            }
        else
            {                
                document.getElementById("hiddeninterest").value= '';

                document.getElementById("mainspan").innerHTML= '';

                var i=0;
                for(i=0;i<splits.length;i++)
                    {                  
                
                               jQuery("#mainspan").append("<li><span>"+splits[i]+"</span><a href='javascript:void(0);' onclick='removeli(this)'><img class='imagespan_profile' src='"+baseurl+"/images/cross.png' /></a></li>");

                               var hidden = jQuery("#hiddeninterest").val();
                                if(hidden=='')
                                  {
                                      var string  = ','+splits[i];
                                      jQuery("#hiddeninterest").val(trim(string));
                                  }                           
                                else
                                  {

                                     var string1 = hidden+','+splits[i];
                                     jQuery("#hiddeninterest").val(trim(string1));
                                  }
                       

                    }
                 document.getElementById('showdiv_interest').style.display = 'none';
                document.getElementById("typings").value = '';
            }
    

}

function start_typing3_profile()
{
    
    var val = document.getElementById('hiddeninterest').value;
    
    if (val == '')
         {
             document.getElementById('typings').value="";
         }

}

function typing4_profile(val)
{
   var val = $("#hiddeninterest").val();
   if (val=='')
    {                   
       document.getElementById('typings').value='Click the arrow';                   
   }

}

function changepassword(url)
{  
    var a = document.getElementById("oldpassword").value;
    var x = document.getElementById("newpassword1").value;
    var y = document.getElementById("newpassword2").value;
    
    if(trim(a)==null || trim(a)=="")
        {  
            document.getElementById("oldpassword").style.background = "#FFA2A2";
            document.getElementById("oldpassword").style.borderColor= "#CC0000";
            document.getElementById("flashmsg").style.display="block";
            document.getElementById("flashmsg").innerHTML="Old password cannot be blank";  
            document.getElementById("flashmsg").style.color="red";
            return false;
        }
    else
        {
            document.getElementById("oldpassword").style.background = "#E6EFC2";
            document.getElementById("oldpassword").style.borderColor= "#C6D880";
            document.getElementById("flashmsg").style.display="none";
        }
        
   if(trim(x)==null || trim(x)=="")
        {  
            document.getElementById("newpassword1").style.background = "#FFA2A2";
            document.getElementById("newpassword1").style.borderColor= "#CC0000";
            document.getElementById("flashmsg").style.display="block";
            document.getElementById("flashmsg").innerHTML="New password cannot be blank";  
            document.getElementById("flashmsg").style.color="red";
            return false;
        }
    else
        {
            document.getElementById("newpassword1").style.background = "#E6EFC2";
            document.getElementById("newpassword1").style.borderColor= "#C6D880";
            document.getElementById("flashmsg").style.display="none";
        }
        
        
        
    if(trim(y)==null || trim(y)=="")
        {  
            document.getElementById("newpassword2").style.background = "#FFA2A2";
            document.getElementById("newpassword2").style.borderColor= "#CC0000";
            document.getElementById("flashmsg").style.display="block";
            document.getElementById("flashmsg").innerHTML="Confirm password cannot be blank";  
            document.getElementById("flashmsg").style.color="red";
            return false;
        }
    else
        {
            document.getElementById("newpassword2").style.background = "#E6EFC2";
            document.getElementById("newpassword2").style.borderColor= "#C6D880";
            document.getElementById("flashmsg").style.display="none";
        }
    
    if(trim(x) != trim(y))
        {
            document.getElementById("newpassword1").style.background = "#FFA2A2";
            document.getElementById("newpassword2").style.background = "#FFA2A2";
            document.getElementById("newpassword1").style.borderColor= "#CC0000";
            document.getElementById("newpassword2").style.borderColor= "#CC0000";
            document.getElementById("flashmsg").style.display="block";
            document.getElementById("flashmsg").innerHTML="Password and confirm password are different";  
            document.getElementById("flashmsg").style.color="red";
            return false;
            
        }
    else
       {
            document.getElementById("newpassword1").style.background = "#E6EFC2";
            document.getElementById("newpassword1").style.borderColor= "#C6D880";
            document.getElementById("newpassword2").style.background = "#E6EFC2";
            document.getElementById("newpassword2").style.borderColor= "#C6D880";
            document.getElementById("flashmsg").style.display="none";
       }
    
    
      
    document.getElementById('loding').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        dataType:"json",
        data:{
                oldpassword:a,
                newpassword:x
             },
             success:function(value){ 
                 document.getElementById('loding').style.display = "none";
                 
                 if(value.id =="colorsucess")
                 {  
                     document.getElementById("flashmsg").style.color   = "green";
                     document.getElementById('flashmsg').style.display = "block";
                     document.getElementById("flashmsg").innerHTML     = value.result;    
                     window.setTimeout(function() {
                     parent.$.fn.colorbox.close();
                     }, 1500);
                 }
                 else
                  {
                     document.getElementById("flashmsg").style.color   = "red";
                     document.getElementById('flashmsg').style.display = "block";
                     document.getElementById("flashmsg").innerHTML     = value.result;    
                  }
                 
                
        }
    }); 
    
       
       
       return true;
}
function selectAllCheckBox(prm)
{
    if($(prm).is(":checked")==false){
        $(".checkbox_show").prop('checked',false);
        $("#selectallspan").html("Select all");
    }
    else{
        $(".checkbox_show").prop('checked',true);  
        $("#selectallspan").html("Unselect all");
    }               
   
}
function selectAllCheckBox_com(prm)
{
    if($(prm).is(":checked")==false){
        $(".checkbox_show_com").prop('checked',false);
        $("#selectallspan_com").html("Select all");
    }
    else{
        $(".checkbox_show_com").prop('checked',true); 
        $("#selectallspan_com").html("Unselect all");
    }

}

function monthchange()
{
    var data = document.getElementById('monthsid').value;
    
    if(data!='Month')
        {
            var currenturl = document.getElementById("currenturl").value;
            if(currenturl != "myProfile"){
            document.getElementById("monthsid").style.color = "#565656";
            }
            document.getElementById("monthsid").style.fontStyle = "normal"; 
            
            var montharray = new Array("January","February","March","April","May","June","July","August","September","October","November","December")
            var days       = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
            var index      = montharray.indexOf(data); 
            
            $("#daysid").html("");
            var html = "<option>Day</option>";
            for(var i = 1;i<=days[index];i++)
                {
                   html += "<option value='"+i+"'>"+i+"</option>";     
                }
            $("#daysid").html(html);
        }
    else
       { 
           
            document.getElementById("monthsid").style.color     = "#A9A9A9";
            document.getElementById("monthsid").style.fontStyle = "italic"; 
            
            
       }
}


function validatorFunctions() {

	this._checkValue = function (value, defs) {
		return ((typeof value === 'undefined') || (value === null)) ? defs : value;
	}
	
	this._getCountries = function () {
		var countries = new Array(
			"Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", 
			"Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", 
			"China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", 
			"Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", 
			"Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", 
			"Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", 
			"Mexico", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", 
			"Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", 
			"Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", 
			"Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"
		);
		return $.map(countries, function(item, index) { return item.toUpperCase() });
	}
	
	
	this._getStates = function () {
		var states = new Array("AL","Alabama", "AK" ,"Alaska", "AZ","Arizona", "AR","Arkansas", "CA","California", "CO","Colorado", "CT","Connecticut", "DE","Delaware", "DC","District Of Columbia", "FL","Florida", "GA","Georgia", "HI","Hawaii", "ID","Idaho", "IL","Illinois", "IN", "Indiana", "IA", "Iowa", "KS", "Kansas", "KY", "Kentucky", "LA", "Louisiana", "ME", "Maine", "MD", "Maryland", "MA", "Massachusetts", "MI", "Michigan", "MN", "Minnesota", "MS", "Mississippi", "MO", "Missouri", "MT", "Montana","NE", "Nebraska","NV", "Nevada","NH", "New Hampshire","NJ", "New Jersey","NM", "New Mexico","NY", "New York","NC", "North Carolina","ND", "North Dakota","OH", "Ohio", "OK", "Oklahoma", "OR", "Oregon", "PA", "Pennsylvania", "RI", "Rhode Island", "SC", "South Carolina", "SD", "South Dakota","TN", "Tennessee", "TX", "Texas", "UT", "Utah", "VT", "Vermont", "VA", "Virginia", "WA", "Washington", "WV", "West Virginia", "WI", "Wisconsin", "WY", "Wyoming" );
		//var states = {AL: "Alabama", AK: "Alaska", AZ: "Arizona", AR: "Arkansas", CA: "California", CO: "Colorado", CT: "Connecticut", DE: "Delaware", DC: "District Of Columbia", FL: "Florida", GA: "Georgia", HI: "Hawaii", ID: "Idaho", IL: "Illinois", IN: "Indiana", IA: "Iowa", KS: "Kansas", KY: "Kentucky", LA: "Louisiana", ME: "Maine", MD: "Maryland", MA: "Massachusetts", MI: "Michigan", MN: "Minnesota", MS: "Mississippi", MO: "Missouri", MT: "Montana",NE: "Nebraska",NV: "Nevada",NH: "New Hampshire",NJ: "New Jersey",NM: "New Mexico",NY: "New York",NC: "North Carolina",ND: "North Dakota",OH: "Ohio", OK: "Oklahoma", OR: "Oregon", PA: "Pennsylvania", RI: "Rhode Island", SC: "South Carolina", SD: "South Dakota",TN: "Tennessee", TX: "Texas", UT: "Utah", VT: "Vermont", VA: "Virginia", WA: "Washington", WV: "West Virginia", WI: "Wisconsin", WY: "Wyoming" };
		return $.map(states, function(item, index) { return item.toUpperCase() });
	}
	
	
	this.check_error = function ($c, result, msg) {
		if ((typeof msg === 'undefined') || (msg === null)) { msg = '%name% field is missing' }

		if (result) { 
			$c.closest('.ctrl-wrapper').removeClass('ctrl-error');
		} else {		 
			$c.focus().closest('.ctrl-wrapper').addClass('ctrl-error')
				.attr('data-error-msg', msg.replace('%name%', $c.attr('placeholder')));
		}
		return result;
	}

	
	this.validate_custom = function (id, msg, func) {
		var $c = $('#' + id),
			v = $.trim($c.val());
		return this.check_error($c, $c.size() > 0 ? func(v) : true, msg); //if not found elem - skip validation
	}
    
	this.validate_url = function (id, msg) {
		msg  = this._checkValue(msg, 'Invalid link');
		return this.validate_custom(id, msg, function(v) {
			return /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/.test(v);
		});
	}
	
	this.validate_required = function (id, msg, defs) {
		defs = this._checkValue(defs, '');
		msg  = this._checkValue(msg, '%name% field is missing');
		return this.validate_custom(id, msg, function(v){ return v != defs; });
	}
	
	this.validate_size = function (id, msg, defs) {
		defs = this._checkValue(defs, 120);
		msg  = this._checkValue(msg, '%name% should be less than %size% characters').replace('%size%', defs);
		return this.validate_custom(id, msg, function(v){ return v.length < defs; });
	}
	
	this.validate_email = function (id, msg) {
		msg  = this._checkValue(msg, 'Invalid email');
		return this.validate_custom(id, msg, function(v) {
			return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(v);
		});
	}
	
	this.validate_image = function (id, msg) {
		msg  = this._checkValue(msg, 'Please select image');
		return this.validate_custom(id, msg, function(v) {
			return $('#'+id).children().size() > 0;
		});
	}
	
	this.validate_degree = function (id, msg, required) {
		if ((required !== true) && ($.trim($('#' + id).val()).length === 0)) return true;
		msg  = this._checkValue(msg, 'Please provide your information as %name%. If you\'re still in school, please use the future graduation year.');
		
		return this.validate_custom(id, msg, function(v) {
			var result = v && (v != '') && (v.search(',') > 0);
			
			if (result) {
				var d = v.split(','); /* School, Degree, Year */
				d[0] = $.trim(d[0]);
				d[1] = $.trim(d[1]);
				d[2] = $.trim(d[2]);
				result = d[0] && d[1] && d[2] 
					&& (!isNaN(d[2]) 
					&& (d[2] <= new Date().getFullYear()))
					&& (d[2] > 1898);
			}
			return result;
		});
	}
	
	this.validate_address_new = function (id, msg, required) {
		if ((required !== true) && ($.trim($('#' + id).val()).length === 0)) return true;
		msg  = this._checkValue(msg, 'Please provide location as City, State/Province, Country');
		var that = this;
		return this.validate_custom(id, msg, function(v) {
			var r = /^[a-zA-Z0-9 -]*[a-zA-Z]+[a-zA-Z0-9 -]*\,[a-zA-Z0-9 -]*[a-zA-Z]+[a-zA-Z0-9 -]*\,[a-zA-Z0-9 -]*[a-zA-Z]+[a-zA-Z0-9 -]*$/;
			return r.test(v);
		});
	}

	this.validate_address = function (id, msg, required) {
		if ((required !== true) && ($.trim($('#' + id).val()).length === 0)) return true;
		msg  = this._checkValue(msg, 'Please provide valid state');
		var that = this;
		return this.validate_custom(id, msg, function(v) {
			var d = v.split(',');
			
			d[0] = $.trim(d[0]);
			d[1] = $.trim(d[1]).toUpperCase();
			
			var validCountry = ($.inArray(d[1], that._getCountries()) >= 0);
			if (!validCountry) {
				d[2] = $.trim(d[2]) || 'USA';
			}

			var result = validCountry || ((d[2] == 'USA') && ($.inArray(d[1], that._getStates()) >= 0));
			result && $('#' + id).val(d.join(', '));
			return result;
		});
	}

	this.validate_company = function (id, msg, required) {
		if ((required !== true) && ($.trim($('#' + id).val()).length === 0)) return true;
		msg  = this._checkValue(msg, 'Please enter your %name%');
		return this.validate_custom(id, msg, function(v) {
			var result = (v.search(',') > 0);
			if (result) {
				var d = v.split(',');
				result = d[0] && d[1];
			}
			return result;
		});
	};
} //validatorFunctions


if (!String.prototype.format) {
	String.prototype.format = function() {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function(match, number) { 
			return typeof args[number] != 'undefined'
			? args[number]
			: match;
		});
	};
	/*
	String.prototype.format = function() {
		var args = arguments;
		return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
			if (m == "{{") { return "{"; }
			if (m == "}}") { return "}"; }
			return args[n];
		});
	};
	*/
}


function list_validate() {
	var v = new validatorFunctions();
	var baseurl = $('#hidden_baseurl').val();
	
	if (!v.validate_required('listTitle')) return false;
	//if (!v.validate_size('listTitle', '%name% should be less than 50 characters', 50)) return false;
	if (!v.validate_size('listTitle', null, 50)) return false;
	if (!v.validate_required('listInterest')) return false;
	if (!v.validate_required('listInstruction', 'Rules for participants field is missing')) return false;
	//if (!v.validate_size('listInstruction', 'Rules for participants should be less than 4000 characters', 4000)) return false;
	if (!v.validate_size('listInstruction', 'Rules for participants should be less than %size% characters', 4000)) return false;
	if (!v.validate_image('listImage')) return false;
    /*if ($('input[name=participation]:checked', '.form-list').val() == 'My friends') {
        if (!v.validate_required('participationFriends', 'Friends field is missing')) return false;
    }*/
    /*if ($('#listLink').val() != '') {
        if (!v.validate_url('listLink')) return false;
    }*/
    
	$('#saveListAjaxLoading').show();
} //list_validate



function myprofile_vaidate(is_company) {
	var v = new validatorFunctions();
	
	if (!v.validate_required('firstname')) return false;
	if (!v.validate_size('firstname')) return false;
	if (!v.validate_required('lastname')) return false;
	if (!v.validate_size('lastname')) return false;
    
    if (is_company) {
        if (!v.validate_required('street')) return false;
        if (!v.validate_required('country2')) return false;
    }
    
	if (!v.validate_required('monthsid', 'Please select the Month')) return false;
	if (!v.validate_required('daysid', 'Please select the Day', 'Day')) return false;
	if (!v.validate_custom('yearsid', 'Please enter valid Year', function(v) { 
		return (v > 1898) && (v < new Date().getFullYear() - 5);
	})) return false;

	if (!v.validate_required('User_email')) return false;
	if (!v.validate_email('User_email')) return false;

	if (!v.validate_custom('hiddeninterest', null, function(v) {
		var result = (v != '') && (v != ',');
		$('#div_typing')[result ? 'removeClass' : 'addClass']('ctrl-error');
		return result;
	})) return false;
	
	//if (!v.validate_address('country2', null)) return false;
	if (!v.validate_address_new('country2', null)) return false;
	
	/* degree */
	if (!v.validate_degree('degree2')) return false;
	if ($('#hiddeneducation').is(':visible')) {
		//if (!v.validate_degree('hiddeneducation', null, true)) return false;
		if (!v.validate_degree('hiddeneducation')) return false;
	}
	
	if (!is_company) {
		if (!v.validate_company('industry5')) return false;
	} else {
        if (!v.validate_required('industry5')) return false;
    }

	
	return true;
} //myprofile_vaidate


function count(a,i){
 var result = 0;
 for(var o in a)
  if(a[o] == i)
   result++;
 return result;
}


function list_createEntry_old()
{
   
    var validation   = document.getElementById("validation").value;
    
    var value = $(".updatelist").map(function () { if(this.value !='') { return this.value ;}}).get(); 
    
    var valueOrder = $(".updatelistOrder").map(function () { if(this.value !='') { return this.value ;}}).get(); 
    var noofrows   = $("#no_of_rows").val();
    
    var number = new Array();
    for(var kk =0; kk<noofrows;kk++)
      {
          var nm =parseInt(kk)+1;
          number[kk] = ""+nm;
      }
    //var number = Array("1","2","3","4","5","6","7","8","9","10");
    
    
    var odd = '';
    var arr      = new Array();
    for(var i =0;i<validation;i++){
        
        if(i%2 ==0 )
        {
            odd = "#F6F9FD";                                   
        }
        else
        {
           odd = "#Fff";
                                      
        }
                                
        var order0   = document.getElementById("order"+i).value; 
       
        var hello     = count(valueOrder,order0);
         
        if(hello==2)
            {   
                  var second = valueOrder.lastIndexOf(order0)                  
                  document.getElementById("opnerequred").style.display ="block";
                  document.getElementById("opnerequred").style.marginRight ="160px";
                  document.getElementById("opnerequred").innerHTML="Duplicate Order Nos. are not allowed.";
                  document.getElementById("order"+second).focus();
                  return false;
            }
        
        var orderNumber     = $.inArray(order0,number);    
       
        if(orderNumber<0)
            {  
                  document.getElementById("opnerequred").style.display ="block";
                  document.getElementById("opnerequred").style.marginRight ="220px";
                  document.getElementById("opnerequred").innerHTML="Please assign order again.";
                  document.getElementById("order"+i).focus();
                  return false;
            }
        
        if(i == 0)
         {  
             arr.push(order0);
         }
        else
         {            
             var tempss     = $.inArray(order0,arr);
             if(tempss>=0)
              {
                  document.getElementById("opnerequred").style.display ="block";
                  document.getElementById("opnerequred").style.marginRight ="188px";
                  document.getElementById("opnerequred").innerHTML="Order position already assigned.";
                  document.getElementById("order"+i).focus();
                  return false;
              }
              else
                  {
                      arr.push(order0);
                  }
             
         }
         /*var oneExtra = parseInt(order0);
         var oneExtraI = parseInt(i)+1;         
         if(oneExtraI != oneExtra)
             {  
                  document.getElementById("opnerequred").style.display ="block";
                  document.getElementById("opnerequred").style.marginRight ="220px";
                  document.getElementById("opnerequred").innerHTML="Please assign order again.";
                  document.getElementById("order"+i).focus();
                  return false;
             }*/
         
        
        
              
            if( trim(order0)==null || trim(order0)=="")
                {  
                  document.getElementById("opnerequred").style.display ="block";
                  document.getElementById("opnerequred").style.marginRight ="262px";
                  document.getElementById("opnerequred").innerHTML="This field is required";
                  document.getElementById("order"+i).focus();
                  document.getElementById("order"+i).style.background = "none repeat scroll 0 0 #FFA2A2";
                  return false;
                }
             else{         
                    var temp = isNaN(order0);   

                    if(temp==true)
                        {
                            
                            document.getElementById("opnerequred").style.display ="block";
                            document.getElementById("opnerequred").style.marginRight ="178px";
                            document.getElementById("opnerequred").innerHTML = "You can enter only numeric values";
                            document.getElementById("order"+i).focus();
                            document.getElementById("order"+i).style.background = "none repeat scroll 0 0 #FFA2A2";
                            return false;
                        }

                   document.getElementById("order"+i).style.background = "#F6F9FD"; 
                   document.getElementById("opnerequred").style.display ="none";
                   document.getElementById("opnerequred").style.marginRight ="262px";

             }
       
       
    }
    
    var arr2      = new Array();
    
    if(value=='')
       {
           value= validation;
       }
    
    for(var i =0;i<value.length;i++){
        
        if(i%2 ==0 )
        {
            odd = "#F6F9FD";                                   
        }
        else
        {
           odd = "#Fff";
                                      
        }
        
        
        var entry0   = document.getElementById("entry"+i).value; 
        
        if(i == 0)
         {  
             arr2.push(entry0);
             
         }
        else
         {           
             var tempssf     = $.inArray(entry0,arr2);
             if(tempssf>=0)
              {
                  document.getElementById("opnerequred").style.display ="block";
                  document.getElementById("opnerequred").style.marginRight ="213px";
                  document.getElementById("opnerequred").innerHTML="List Choice already entered.";
                  document.getElementById("entry"+i).focus();
                  return false;
              }
              else
                  {
                      arr2.push(entry0);
                  }
             
         }
        
        
        if(i==0){
            
             if( trim(entry0)==null || trim(entry0)=="")
                { 
                  document.getElementById("opnerequred").style.display ="block";
                  document.getElementById("opnerequred").innerHTML="This field is required";
                  document.getElementById("entry"+i).focus();
                  document.getElementById("entry"+i).style.background = "none repeat scroll 0 0 #FFA2A2";
                  return false;
                }
             else{         
                   document.getElementById("opnerequred").style.display ="none";
                   document.getElementById("entry"+i).style.background = odd; 

             }
         
        }        
        
    }
    
     
}

function check_opinions() {
	var i, v, p,
		results = [],
		values = [],
		positions = [],
		empty = true;

	$('input[name*="opinions"]').each(function(){
		v = this.name.match(/[^\[\]]+/g);
		i = parseInt(v[1]);
		if (!results[i]) results[i] = {};
		results[i][v[2]] = $.trim(this.value);
	});
	
	for(i = 0; i < results.length; i++) {
		v = results[i].value.toLowerCase();
		if (v != '') {
			empty = false;
			//console.log(i, results[i].value, results[i].position);
			if ($.inArray(v, values) !== -1) {
				$('#opnerequred').text('List Choice "%s" already entered.'.replace('%s', v)).show();
				$('input[name="opinions[' + i + '][value]"]').focus();
				return false;
			} else {
				values.push(v);
			}
			
			p = results[i].position;
			if (p == '') {
				$('#opnerequred').text('This field is required.').show();
				$('input[name="opinions[' + i + '][position]"]').focus();
				return false;
			} else if (isNaN(p)) {
				$('#opnerequred').text('You can enter only numeric values.').show();
				$('input[name="opinions[' + i + '][position]"]').focus();
				return false;
			} else if ($.inArray(p, positions) !== -1) {
				$('#opnerequred').text('Duplicate Order Nos. are not allowed.').show();
				$('input[name="opinions[' + i + '][position]"]').focus();
				return false;
			} else if (parseInt(p) < 1) {
				$('#opnerequred').text('Please assign order again.').show();
				$('input[name="opinions[' + i + '][position]"]').focus();
				return false;
			} else {
				positions.push(p);
			}
		}
		
		if (empty) {
			$('#opnerequred').text('Must enter an opinion.').show();
			return false;
		}
	}
	
	$('#opnerequred').hide();
	return true;
}


function increaseValidation(value)
{
    var id = parseInt(value)-1;
   
    var order0   = document.getElementById("order"+id).value; 
            
    if(order0 != '')
      document.getElementById("validation").value = value; 
    else
      {
          var id2 = parseInt(id)-1;
          
          if(id2 != -1){
          
          var order1   = document.getElementById("order"+id2).value; 
         
          if(order1 != '')
              {
                  document.getElementById("validation").value = id; 
              }
          else
              {
                  document.getElementById("validation").value = id; 
              }
          }
          
      }
    
}

function ChangeViewResult()
{    
    var listid = $("#listid").val();
    
    var value = $(".checked12:checked").map(function () {return this.value;}).get().join(",");     
    
    if(value=='')
      {
          $("#openshowall").hide();
          return false;
      }
    var valuesarray = value.split(",");
    
    var input =''; var input1 = ''; var friendid1 = '';var friendid = 0;
    
    if(valuesarray[0] !='0_Show my friends')
      {
        for(var k=0;k<valuesarray.length;k++)
            {
                input1     = valuesarray[k].split("_");
                friendid1 += input1[0]+",";
                input     += input1[1]+",";
            }
            var finalname = input.substring(0, input.length - 1);
            var friendid = friendid1.substring(0, friendid1.length - 1);
      }
      else
       {
         var finalname = 'Show my friends';         
       }
    
    
    
    
     $("#showwallinputid").val(finalname);
   
    $("#openshowall").hide();
    
    var url = $("#hidden_baseurl").val()+"/index.php/preferences/viewResultShowAll?listid="+listid;
    $("#tableid").html('');
    document.getElementById('loding').style.display = "block";
    jQuery.ajax({ 
        type:'POST',
        url:url,
        //dataType:"json",
        data:{
                value:finalname,
                friendid:friendid
             },
             success:function(value){ 
                 document.getElementById('loding').style.display = "none";
                 
                 if(value !="xxx")
                 {  
                     jQuery("#tableid").html(value).show();                     
                            
                 }
                 
                
        }
    }); 
    
}


function ChangeViewResultZ() {    
	$('#openshowallZ').css('visibility','hidden');
	
	if ($('#show_all').prop('checked')) {
		$('#showwallinputid').val('Show all');
		return [];
	}
	
	var data, ids = [], names = [];
	$('input[data-friend-id]:checked').each(function() { 
		data = $(this).data(); 
		ids.push(data.friendId);
		names.push(data.friendName);
	});

	if ($('#show_my_friends').prop('checked')) {
		$('#showwallinputid').val('Show my friends');
	} else if ($('#show_my').prop('checked')) {
		$('#showwallinputid').val('Show my');
	} else {
		$('#showwallinputid').val(names.join(','));
	}
	
	return ids;
}


function myprofilecompany_vaidate()
{
    var k=document.getElementById("firstname").value;      
   
    if( trim(k)==null || trim(k)=="")
        { 
          document.getElementById("firstname").focus();
          document.getElementById("spanfirst").style.display='block';
          document.getElementById("spanfirst").innerHTML="Company name field is missing";
          document.getElementById("firstname").style.background ="none";          
          document.getElementById("firstname").style.background = "url(../../newimages/email_bg_red.png) no-repeat";           
         
            return false;
        }
     else{         
          
           document.getElementById("firstname").style.background = "url(../../newimages/email_bg.png) no-repeat";  
           document.getElementById("spanfirst").style.display='none';           
     }
     
     
     if( trim(k).length>120)
        { 
          document.getElementById("firstname").focus();
          document.getElementById("spanfirst").style.display='block';
          document.getElementById("spanfirst").innerHTML="Company name should be less than 120 characters";
          document.getElementById("firstname").style.background ="none";          
          document.getElementById("firstname").style.background = "url(../../newimages/email_bg_red.png) no-repeat";           
         
            return false;
        }
     else{         
          
           document.getElementById("firstname").style.background = "url(../../newimages/email_bg.png) no-repeat";  
           document.getElementById("spanfirst").style.display='none';           
     }
     
     
     var x=document.getElementById("User_email").value;
   
   
        if(trim(x)==null || trim(x)=="")
        { 
            document.getElementById("spanemail").style.display='block';
            document.getElementById("spanemail").innerHTML="Email field is missing";    
            document.getElementById("User_email").style.background ="none";
            document.getElementById("User_email").focus();
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat";              
            return false;
        }
     else{
           document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png) no-repeat";             
           document.getElementById("spanemail").style.display='none';
     }  
  
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var par=document.getElementById("User_email").value; 
   if(reg.test(par) == false)
    { 
            document.getElementById("User_email").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
            document.getElementById("spanemail").style.display='block';
            document.getElementById("spanemail").innerHTML="Invalid email";  
            document.getElementById("User_email").focus();            
            return false;
    }
    else
       { 
           document.getElementById("User_email").style.background = "url(../../newimages/email_bg.png) no-repeat";
           document.getElementById("spanemail").style.display='none';
       }
       
       
   var interest=document.getElementById("hiddeninterest").value; 
    
     if(trim(interest) == '')
        {           
            document.getElementById("spaninterest").style.display='block';           
            document.getElementById("typings").focus();
            document.getElementById("div_typing").style.background = "#FFA2A2";    
            document.getElementById("typings").style.background = "#FFA2A2";  
            document.getElementById("typings").style.color = "#FFFFFF";
            return false;
        }
     else{
           document.getElementById("div_typing").style.background = "white";           
           document.getElementById("typings").style.background = "#FFFFFF";
           document.getElementById("typings").style.color = "#8B8B8B";
           document.getElementById("spaninterest").style.display='none';       
           
     }  
     
     var  street = document.getElementById("companyStreet").value; 
    
     if(trim(street) == '')
        {           
            document.getElementById("streeterror").style.display='block';     
            document.getElementById("streeterror").innerHTML="Please describe your Street address";            
            document.getElementById("companyStreet").focus();   
            document.getElementById("companyStreet").style.background = "";  
            document.getElementById("companyStreet").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
            return false;
        }
     else{
           document.getElementById("companyStreet").style.background = "url(../../newimages/email_bg.png) no-repeat";  
           document.getElementById("typings").style.color = "#555555";
           document.getElementById("streeterror").style.display='none';       
           
     }
     
     var country2 = document.getElementById("country2").value; 
     
     if(country2 != 'City, State/Province, Country')
                 {
                        document.getElementById("country2").style.background = "url(../../newimages/email_bg.png) no-repeat";  
                        document.getElementById("country2").style.color = "#A9A9A9";                        
                        
                        var search = country2.search(",");
                        if(search>0)
                            {
                                document.getElementById("country2").style.background = "url(../../newimages/email_bg.png) no-repeat";  
                                document.getElementById("country2").style.color = "#A9A9A9";
                                
                                var split    = country2.split(","); 
                                var city     = split[0];
                                var state    = split[1];
                                var country  = split[2];                                
                                var state1   = trim(state).toUpperCase();
                                
                                 var countryarray = new Array("Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", 
                                                             "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", 
                                                             "China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", 
                                                             "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", 
                                                             "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", 
                                                             "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", 
                                                             "Mexico", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", 
                                                             "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", 
                                                             "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", 
                                                             "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe");
                                
                                
                                 var countryarray1 = $.map(countryarray, function(item, index) {
                                                        return item.toUpperCase();
                                                       });
                                
                                var temp = $.inArray(state1, countryarray1);
                                        
                                        
                                if(temp>=0)
                                    {
                                        document.getElementById("cityerror").style.display ="none";
                                        return true;
                                        
                                    }
                                var statearray = new Array("AL","Alabama", "AK" ,"Alaska", "AZ","Arizona", "AR","Arkansas", "CA","California", "CO","Colorado", "CT","Connecticut", "DE","Delaware", "DC","District Of Columbia", "FL","Florida", "GA","Georgia", "HI","Hawaii", "ID","Idaho", "IL","Illinois", "IN", "Indiana", "IA", "Iowa", "KS", "Kansas", "KY", "Kentucky", "LA", "Louisiana", "ME", "Maine", "MD", "Maryland", "MA", "Massachusetts", "MI", "Michigan", "MN", "Minnesota", "MS", "Mississippi", "MO", "Missouri", "MT", "Montana","NE", "Nebraska","NV", "Nevada","NH", "New Hampshire","NJ", "New Jersey","NM", "New Mexico","NY", "New York","NC", "North Carolina","ND", "North Dakota","OH", "Ohio", "OK", "Oklahoma", "OR", "Oregon", "PA", "Pennsylvania", "RI", "Rhode Island", "SC", "South Carolina", "SD", "South Dakota","TN", "Tennessee", "TX", "Texas", "UT", "Utah", "VT", "Vermont", "VA", "Virginia", "WA", "Washington", "WV", "West Virginia", "WI", "Wisconsin", "WY", "Wyoming" );
                                //var statearray = {AL: "Alabama", AK: "Alaska", AZ: "Arizona", AR: "Arkansas", CA: "California", CO: "Colorado", CT: "Connecticut", DE: "Delaware", DC: "District Of Columbia", FL: "Florida", GA: "Georgia", HI: "Hawaii", ID: "Idaho", IL: "Illinois", IN: "Indiana", IA: "Iowa", KS: "Kansas", KY: "Kentucky", LA: "Louisiana", ME: "Maine", MD: "Maryland", MA: "Massachusetts", MI: "Michigan", MN: "Minnesota", MS: "Mississippi", MO: "Missouri", MT: "Montana",NE: "Nebraska",NV: "Nevada",NH: "New Hampshire",NJ: "New Jersey",NM: "New Mexico",NY: "New York",NC: "North Carolina",ND: "North Dakota",OH: "Ohio", OK: "Oklahoma", OR: "Oregon", PA: "Pennsylvania", RI: "Rhode Island", SC: "South Carolina", SD: "South Dakota",TN: "Tennessee", TX: "Texas", UT: "Utah", VT: "Vermont", VA: "Virginia", WA: "Washington", WV: "West Virginia", WI: "Wisconsin", WY: "Wyoming" };
                                   
                                 var statearray1 = $.map(statearray, function(item, index) {
                                                        return item.toUpperCase();
                                                       });
                                 
                                 
                                if(country==undefined ||country=='')
                                 {
                                    country = "USA";       
                                 }
                                 
                                 if(country == "USA")
                                  {
                                        
                                      var temp = $.inArray(state1, statearray1);
                                      if(temp<0)
                                          {
                                              document.getElementById("cityerror").style.display ="block";
                                              document.getElementById("cityerror").innerHTML="Please provide valid state";
                                              document.getElementById("country2").style.background ="none";
                                              document.getElementById("country2").focus();
                                              document.getElementById("country2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
                                              
                                              return false;
                                          }
                                       else
                                          {
                                               document.getElementById("cityerror").style.display ="none";
                                          }
                                         
                                         
                                         document.getElementById("country2").value= city+","+state+", "+country;
                                  }
                                  document.getElementById("cityerror").style.display ="none";
                                 
                            }
                        else
                            {
                                document.getElementById("cityerror").style.display ="block";
                                document.getElementById("cityerror").innerHTML="Please provide location as City, State/Province, Country";
                                document.getElementById("country2").style.background ="none";
                                document.getElementById("country2").focus();
                                document.getElementById("country2").style.background = "url(../../newimages/email_bg_red.png) no-repeat";  
                                return false;
                            }
                            
                 }
                 else
                     {
                         document.getElementById("country2").style.background = "url(../../newimages/email_bg.png) no-repeat"; 
                         document.getElementById("cityerror").style.display ="none";
                     }
                     
                     
                     
                
            var industry5 = document.getElementById("industry5").value;  
            
            if(trim(industry5) == '')
                        {  
                            document.getElementById("industry5").focus();                              
                            document.getElementById("industry5").style.background = "url(../../newimages/email_bg_red.png) no-repeat"; 
                            document.getElementById("industry_error").style.display='block';
                            document.getElementById("industry_error").innerHTML="Please describe your Industry";  
                            return false;
                        }
                    else
                        {
                            document.getElementById("industry_error").style.display='none';                                    
                            document.getElementById("industry5").style.background = "url(../../newimages/email_bg.png) no-repeat";                                                     
                        }
                        
             
              
     return true;
}

function addfriend(id) {
    $("#addfrndloding").show();
    var baseurl = $("#baseurl").val();    
    var url = baseurl + "/index.php/user/add_profilefriend";
    $.ajax({ 
        type: 'POST',
        url: url,   
        dataType: "json",
        data: {
            value: id
        },
        success: function(value) { 
            $("#addfrndloding").hide();                 

            if (value.result != "1")
            {                
                $("#addimage").attr("src",baseurl+"/images/latest_img/friend_req.png");
                $("#fakeboxspan").html("Cancel Request !");
                $("#yescancel").attr("src",baseurl+"/images/latest_img/yes.png");
                $("#nocancel").attr("src",baseurl+"/images/latest_img/no.png");
                $("#yescancel").attr("onClick","friendRequestSentYes('"+id+"','"+value.id+"')");
                $("#nocancel").attr("onClick","friendRequestSentNo()");
                $("#addimage").attr("onClick","friendRequestSent()");
                $("#msgaddfrnd").html(value.result);      
            }
        },
        error: function(resp) {
            $("html").html(resp.responseText);
        }
    });
}

function removefriendYes(id)
{ 
    $("#addfrndloding").show();
    var baseurl = $("#baseurl").val();
    var url = baseurl+"/index.php/user/remove_profilefriend";
    
    jQuery.ajax({ 
        type:'POST',
        url:url,        
        data:{
                value:id
             },
             success:function(value){ 
                 $("#addfrndloding").hide();                 
                 if(value !="1")
                 {             
                      $("#div_hide").hide();                      
                      $("#addimage").attr("src",baseurl+"/images/latest_img/ADD.png");
                      $("#addimage").attr("onClick","addfriend('"+id+"')");
                      $("#msgaddfrnd").html(value);      
                 }
                 
                
        }
    });
}

function removefriendNo(id)
{ 
    $("#div_hide").hide();
}

function removefriend(id)
{ 
    $("#div_hide").toggle();
}
function friendRequestSentYes(id,notificationid)
{ 
    $("#addfrndloding").show();
    var baseurl = $("#baseurl").val();
    
    var url = baseurl+"/index.php/user/removeFriendRequest?notificationid="+notificationid;
    
    jQuery.ajax({ 
        type:'POST',
        url:url,        
        data:{
                value:id
             },
             success:function(value){ 
                 $("#addfrndloding").hide();                 
                 if(value !="1")
                 {              
                      $("#div_hide").hide();
                      $("#addimage").attr("src",baseurl+"/images/latest_img/ADD.png");
                      $("#addimage").attr("onClick","addfriend('"+id+"')");
                      $("#msgaddfrnd").html(value);      
                 }
                 
                
        }
    });
}
function friendRequestSentNo()
{ 
    $("#div_hide").hide();
}
function friendRequestSent(id,notificationid)
{    
    $("#div_hide").toggle();
}
function acceptfrndrequest2(id)
{
    $("#addfrndloding").show();      
    var nameofuser = $("#nameofuser").val();    
    var baseurl = $("#baseurl").val();    
    var url = baseurl+"/index.php/user/respondFrndRequest";
    
    jQuery.ajax({ 
        type:'POST',
        url:url,        
        data:{
                value:id
             },
             success:function(value){ 
                 $("#addfrndloding").hide();                 
                 if(value !="1")
                 {         
                      $("#div_hide").hide(); 
                      $("#fakeboxspan").html("Do you want to remove '"+nameofuser+"' from your friends list!");
                      $("#addimage").attr("src",baseurl+"/images/latest_img/REMOVE.png");
                      $("#yescancel").attr("src",baseurl+"/images/latest_img/yes.png");
                      $("#nocancel").attr("src",baseurl+"/images/latest_img/no.png");
                      $("#addimage").attr("onClick","removefriend('"+id+"')");
                      $("#yescancel").attr("onClick","removefriendYes('"+id+"')");
                      $("#nocancel").attr("onClick","removefriendNo()");
                      $("#msgaddfrnd").html(value);      
                 }
                 
                
        }
    });
}

function declinefrndRequestPage(userid,notificationid)
{
    $("#addfrndloding").show();
    var baseurl = $("#baseurl").val();
    var url = baseurl+"/index.php/user/rejectfrndrequest?userid="+userid+"&notificationid="+notificationid;
                  jQuery.ajax({ 
                                    type:'POST',
                                    url:url, 
                                    data:{                               
                                            
                                         }, 
                                         success:function(value){   
                                             $("#addfrndloding").hide();                 
                                             if(value=='delete')
                                                 {
                                                     $("#div_hide").hide(); 
                                                     $("#addimage").attr("src",baseurl+"/images/latest_img/ADD.png");
                                                     $("#addimage").attr("onClick","addfriend('"+userid+"')");
                                                 }

                                }
               });



}

function respondFriendRequest()
{  
    $("#div_hide").toggle();
}

function forgotpassFocus(val)
{      
    if( val == 'Email')
      { 
          document.getElementById('User_email_forgot').style.color="#565656";
          document.getElementById('User_email_forgot').style.fontStyle="normal";
      }
    
    
}
function forgotpassBlur(val)
{
    if(val == '')
      {
          document.getElementById("User_email_forgot").style.background = "white";
          document.getElementById("spanemail_forgot").style.display='none';
          document.getElementById('User_email_forgot').value="Email";
          document.getElementById('User_email_forgot').style.color="#A9A9A9";
          document.getElementById('User_email_forgot').fontStyle="italic";
          return false;
      }
   else
      {          
          document.getElementById('User_email_forgot').style.color="#565656";
          document.getElementById('User_email_forgot').style.fontStyle="normal";
      }
      
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var par=document.getElementById("User_email_forgot").value; 
   if(reg.test(par) == false)
    { 
          document.getElementById("User_email_forgot").style.background = "#FFA2A2";
          document.getElementById("spanemail_forgot").style.display='block';
          document.getElementById("spanemail_forgot").innerHTML="Invalid email"; 
          document.getElementById('User_email_forgot').style.color="#565656";
          document.getElementById('User_email_forgot').style.fontStyle="normal";
     
     return false;
    }
    else
       { 
           document.getElementById("User_email_forgot").style.background = "white";
           document.getElementById("spanemail_forgot").style.display='none';
           document.getElementById('User_email_forgot').style.color="#565656";
           document.getElementById('User_email_forgot').style.fontStyle="normal";
       }
}

function textarealisttitle()
{
    var value = $("#instruction").val();
    if(value == "Please explain how you would like your participants to format their input. E.g. for the best vacation spot, you may recommend them to enter results as Country, City, State.")
      {
          $("#instruction").val('');
      }
}

function textarealisttitleBlur()
{
    var value = $("#instruction").val();
    if(trim(value) == "")
      {
          document.getElementById('instruction').style.fontStyle="italic";
          $("#instruction").val('Please explain how you would like your participants to format their input. E.g. for the best vacation spot, you may recommend them to enter results as Country, City, State.');
      }
    else
      {
          document.getElementById('instruction').style.fontStyle="normal";
      }
    
}

function textarealisttitleFocus()
{
    var value = $("#instruction").val();
    if(value == "Please explain how you would like your participants to format their input. E.g. for the best vacation spot, you may recommend them to enter results as Country, City, State.")
      {
          document.getElementById('instruction').style.fontStyle="normal";
          $("#instruction").val('');
      }
    else
      {
          document.getElementById('instruction').style.fontStyle="normal";
      }
}
function showallFilter()
{
    jQuery('#openshowall').toggle();
}

function showmyfriendsViewResults()
{
    
   var checkbox = document.getElementById('show_my_friends').checked;
   if(checkbox==true)
     {       
        $('.show_all').prop('checked',false);
        $('.show_my').prop('checked',false);
        $('.friends_name').prop('checked',true);
     }
   else
     {
        $('.show_all').prop('checked',false);
        $('.show_my').prop('checked',false);
        $('.friends_name').prop('checked',false);
     }
}

function showallViewResults()
{
    $('.show_my,.show_my_friends,.friends_name').prop('checked',false);
}

function friendsViewResults()
{
    $('.show_my,.show_my_friends,.show_all').prop('checked',false);
}

function showmyViewResults() {
    $('.show_all,.show_my_friends,.friends_name').prop('checked',false);
}

//   actionadd_profilefriend
function acceptfrndRequest(userid,notificationid)
{
    var baseurl = $("#baseurl").val();
    $("#frndreqaccept_"+notificationid).html('');    
   
    var url = baseurl+"/index.php/user/acceptfrndrequest?userid="+userid+"&notificationid="+notificationid;
                  jQuery.ajax({ 
                                    type:'POST',
                                    url:url, 
                                    data:{                               
                                            
                                         }, 
                                         success:function(value){   
                                             
                                             if(value!='1')
                                                 {
                                                     var htmlfrnd = "<div class='frndnameacept'>You are friends with <a class='text_blue' href='"+baseurl+"/index.php/user/myDashboard?userid="+userid+"'><span>"+value+"</span></a></div>"    
                                                     $('#testidnoti_'+notificationid).after(htmlfrnd);
                                                 }

                                }
               });
}

function declinefrndRequest(userid,notificationid,name)
{
    var baseurl = $("#baseurl").val();
    var url = baseurl+"/index.php/user/rejectfrndrequest?userid="+userid+"&notificationid="+notificationid;
                  jQuery.ajax({ 
                                    type:'POST',
                                    url:url, 
                                    data:{                               
                                            
                                         }, 
                                         success:function(value){   
                                             
                                             if(value=='delete')
                                                 {
                                                     
                                                     $('#notiouter_'+notificationid).html('');
                                                     $('#notiline_'+notificationid).html('');
                                                 }

                                }
               });



}

function hideautonameList(id)
{
   
    $("#div"+id).hide();
}

function focusOnInputType(id)
{
   
    document.getElementById(id).focus();
}

function addRowListEntry(id)
{
    var value = parseInt(document.getElementById('addrow').value, 10);
    value = isNaN(value) ? 0 : value; 
    var odd = '';
    
   if(id==value)
      {
           
        if(value%2 ==0 )
        {
             odd = "odd";                                   
        }
        else
         {
             odd = "even";              
         }
         
         var kww = null;
         
         value++;
         document.getElementById('addrow').value = value;
         var newvalue = value+1;
         document.getElementById('no_of_rows').value = newvalue;
         var cureenturl  = $("#currenturl").val();
         
         var html  = "<tr class='tr1_"+odd+"'>";
         if(cureenturl=='listEntry')
         {             
             html     += "<td class='th1_listentry'><input type='text' name='order_"+value+"'  value='"+newvalue+"'  id='order"+value+"'   class='updatelistOrder oder_listentry"+odd+"' onblur='increaseValidation("+newvalue+")'/></td>";
             html     += "<td class='th2_listentry'><input type='text' name='entry_"+value+"'  value=''  id='entry"+value+"'   class='updatelist entry_listentry"+odd+"' onfocus='addRowListEntry("+value+")' autocomplete='off' /></td>";
             html     += "<td class='th3_listentry_comment'><input type='text' name='comment_"+value+"'  value=''  id='comment"+value+"'   class='comment_listentry"+odd+"' autocomplete='off'/></td>";
         }
         else if(cureenturl=='updateEntry' || cureenturl=='takelist')
         {             
             html     += "<td class='th1_listentry'><input type='text' name='order_"+value+"'  value='"+newvalue+"'  id='order"+value+"'   class='updatelistOrder oder_listentry"+odd+"' onblur='increaseValidation("+newvalue+")'/></td>";
             html     += "<td class='th2_listentry'><input type='text' name='entry_"+value+"'  value=''  id='entry"+value+"'   class='updatelist entry_listentry"+odd+"' onkeyup='nameautofil("+kww+","+value+",event)'  onfocus='addRowListEntry("+value+")' autocomplete='off' /><div class='listdiv_takelist' id='div"+value+"' style='display: none;'></div></td>"
             html     += "<td class='th3_listentry_comment'><input type='text' name='comment_"+value+"'  value=''  id='comment"+value+"'   class='comment_listentry"+odd+"'  autocomplete='off' onfocus='hideautonameList("+value+")' /></td>";
         }
         
          html     += "</tr>";
         

        $('#myTable > tbody:last').append(html); 
    
       }
}




/**************************************************************************************************/
var request_pending = false;

function at_bottom_of_page() {    
	return $(window).scrollTop() + $(window).height() > $(document).height() - 100;
}


function removeListFromFavorite(me) {
	var baseurl = $('#baseurl').val(),
		$owner = $(me).closest('[data-subject-type="list"]'),
		list = $owner.data();
	
	if (list && list.id) {
		$.ajax({ 
			type: 'POST',
			url: baseurl + '/index.php/preferences/removeListFromFavorite',
			data: {
				list_id: list.id
			}, 
			success: function(favorite) {
				if (favorite == 0) {
					if (list.hideNonFavorite === true) {
						$owner.fadeOut(500, function() { $owner.remove(); });
					} else {
						$owner.data('isFavorite', false).attr('data-is-favorite', false);
						$(me).closest('.list-box-alert-panel').fadeOut(500);
					}
				}
			}
		});
	}
}


function toggleFavorite(me) {
	var baseurl = $('#baseurl').val(),
		$owner = $(me).closest('[data-subject-type="list"]'),
		list = $owner.data();
		
	if (list) {
		if (!list.isTaken) {
			window.location.href = baseurl + '/index.php/preferences/takelist?listid=' + list.id;
		} else {
			var $f = $owner.find('.list-box-alert-panel');
			if (list.isFavorite) {
				if ($f.size() > 0) {
					$f.fadeIn(600);
				} else {
					removeListFromFavorite(me);
				}
			} else if (list.id) {
				$.ajax({ 
					type: 'POST',
					url: baseurl + '/index.php/preferences/addListToFavorite',
					data: {
						list_id: list.id
					}, 
					success: function(favorite) {
						if (favorite > 0) {
							$owner.data('isFavorite', true).attr('data-is-favorite', true);
						}
					}
				});
			}
		}
	}
}


function updateListOrder() {
	var baseurl = $('#baseurl').val();
	var ids = $('.list-boxes-dragable li').map(function() {
		return $(this).data('id');
	}).get();
		
	$.ajax({ 
		type: 'POST',
		url: baseurl + '/index.php/user/myDashboardUpdateOrder',
		data: {
			ids: ids
		}, 
		success: function(res) {},
		error: function(res) {}
	});
}
	

function initGlobals(userId, settings) {
	if (!window.FW) {
		window.FW = {
		}
	}
	FW.currentUser = userId;
	if (typeof FW.users === 'undefined') {
		FW.users = {}
	}

	if (!FW.users[userId]) {
		FW.users[userId] = {}
	}
	$.extend(FW.users[userId], settings);
}


function initPagination(userId, page) {
	if ((!request_pending) && at_bottom_of_page() && FW.users[userId][page]) {
		var pageno    = FW.users[userId][page].pageno;
		var maxpageid = FW.users[userId][page].maxpageid;
		
		if (pageno <= maxpageid) {
			request_pending = true;
			$('#officialtodatlodingdiv').css('display', 'block');
				
			$.ajax({ 
				type: 'POST',
				url: FW.baseurl + '/index.php/'+ FW.users[userId][page].action,
				data: {
					pageno: pageno,
					user_id: userId,
					_ajax: true
				}, 
				success: function(html) {
					$('#officialtodatlodingdiv').css('display', 'none');
					if (html != 1) {
						$(FW.users[userId][page].container).append(html);
						request_pending = false;
						FW.users[userId][page].pageno += 1;
					}
				}
			});
		}
	}
}


function showUserBriefs(userId, wrapperId) {
	if (userId) {
		$.ajax({ 
			type: 'POST',
			url: FW.baseurl + '/index.php/user/showUserBriefs',
			data: {
				user_id: userId
			}, 
			success: function(html) {
				var $popup = $('#popupUserBriefs');
				if ($popup.size() === 0) {
					$popup = $('<div id="popupUserBriefs"></div>').addClass('popup-user-briefs').hide();
					$(wrapperId).append($popup);
				}
				if (html.length > 0) {
					$('#mainContent').hide();
					//$(wrapperId).children().hide();
					//FW.usersMain = $.extend({}, FW.users);
					FW.currentUserMain = FW.currentUser;
					FW.usersMain = FW.users;
					FW.users = {};
					$popup.html(html).show();
					$(window).scrollTop(0);
				}
			}
		});
	}
}


function switchUserBriefs(page) {
	var user = FW.users[FW.currentUser];
	
	user[user.currentPage] && $(user[user.currentPage].container).parent().hide();
	user[page] && $(user[page].container).parent().show();
	user.currentPage = page;
}


function closeUserBriefs() {
	$('#popupUserBriefs').hide();
	FW.users = FW.usersMain;
	FW.currentUser = FW.currentUserMain;
	
	$('#mainContent').show();
}

var listAuthorClicked = false;
function listAuthorClick(el, userId, contentSel) {
    if (listAuthorClicked) return;
    
    if ($('#invitePopup_' + userId).length > 0) {
        $('#invitePopup_' + userId).remove();
    }
    
    listAuthorClicked = true;

    $.ajax({
        type: 'POST',
        url: FW.baseurl+'/index.php/user/inviteFriend',
        dataType: 'text',
        data: {
            userid: userId,
            contentsel: contentSel
        }, 
        success: function (resp) {
            if (resp == "friend") {
                showUserBriefs(userId, contentSel);
            } else {
                var popupHolder = $(el).parent().parent();
                popupHolder = popupHolder.find('.block-hidden');
                popupHolder.removeClass('invite-message')
                    .removeClass('invite-pending').html(resp);
            }
            
            listAuthorClicked = false;
        },
        error: function (resp) {
            listAuthorClicked = false;
        }
    });
}

function listAuthorFollowClick(el, userId) {
    $(el).hide();
    
    $.ajax({
        type: 'POST',
        url: '/index.php/preferences/followCompany',
        dataType: 'text',
        data: { company_id: userId }, 
        success: function (resp) {
            $('#invitePopup_' + userId).parent().hide();
            $('#inviteSendConfirmation_' + userId).show();
        },
        error: function (resp) {
        }
    });
}


function listAuthorInviteClick(el, userId) {
    $(el).hide();
    $('#inviteMessageBlock_' + userId).show();
    $('#invitePopup_' + userId).parent().addClass("invite-message");
}

function listAuthorInviteSendClick(el, userId) {
    $(el).hide();
    
    $.ajax({
        type: 'POST',
        url: '/index.php/preferences/invitePeople',
        dataType: 'text',
        data: {
            message: $('#inviteMessage_' + userId).val(),
            people_id: userId
        }, 
        success: function (resp) {
            $('#inviteMessageBlock_' + userId).hide();
            $('#invitePopup_' + userId).parent().removeClass("invite-message");
            $('#invitePopup_' + userId).parent().addClass("invite-pending");
            $('#inviteSendConfirmation_' + userId).show();            
        },
        error: function (resp) {
        }
    });
}

function getNotifications() {
	$.ajax({ 
		type: 'POST',
		url: FW.baseurl + '/index.php/user/getNotifications',
		dataType: 'json',
		success: function(data) {
			if (data.length === 0) {
				$('#secret-why2 .cursorpointer.heightclass').text('');
			} else {
				var s = '<div id="noticount" class="notication_count"><div class="notication_count_div">' + data.length + '</div></div>';
				$('#secret-why2 .cursorpointer.heightclass').html(s);
			}
			$('#popupCont2').html(renderNotifications(data));
		}
	});
}

function renderNotifications(notifications) {
	var baseurl = FW.baseurl,
		n = notifications.length > 99 ? 99 : notifications.length,
		s = '', data, sender, image, gender, i;
	for(i = 0; i < n; i++)	{
		data = notifications[i];
		sender = data.sender;
		s += '<div class="notification_div1" id="notiouter_' + data.id + '">';
		
		if (data.notification_type !== 'system') {
			image = sender.image;
			gender = sender.gender.toLowerCase();
			if (image !== '') { 
				image = '/images/profile/user_thumbs/listcreation/' + image + '?w=56&h=44';
			} else if (gender === 'male')	{
				image = '/images/defaultboy.jpg';
			} else if (gender === 'female') {
				image = '/images/defaultgirl.jpg';
			} else { 
				image = '/images/default.jpg';
			}
			s += '<div class="userimage_notificationdiv2"><img class="notification_userimg" src="' + baseurl + image + '" /></div>';
		}

		s += '<div class="notification_msg" id="testidnoti_' + data.id + '">';
			
		if (data.notification_type === 'friends') {
			s += '<div>';
			s += 	'<a class="link-base bold" href="' + baseurl + '/index.php/user/dashboard?userid=' + sender.id + '">' + sender.fullName + '</a>';
			s += 	' <span>' + data.message + '</span>';
			s += '</div>';
			if (data.status === 'waiting') {
				s += '<div class1="floatright" id="frndreqaccept_'+ data.id+'" style="padding-top: 4px;">';
				s += 	'<img src="' + baseurl + '/images/latest_img/ACCEPT.png" class="accepttext" onclick="acceptfrndRequest(' + sender.id + ',' + data.id + ')" />';
				s += 	'<img src="' + baseurl + '/images/latest_img/decline.png" class="declinetext" onclick="declinefrndRequest(' + sender.id + ',' + data.id +')" />';
				s += '</div>';
			}
		} else if (/^(my_created_lists|lists|my_taken_lists|invitation|invite_list)$/.test(data.notification_type)) {
				s += '<div>';
				s += 	'<a class="link-base bold" href="' + baseurl + '/index.php/user/dashboard?userid=' + sender.id + '">' + sender.fullName + '</a>';
				s += 	' <span>' + data.message + '</span>';
				s += 	' "<a class="link-base capitalize" href="' + baseurl + '/index.php/preferences/listdetail?listid=' + data.list_id + '">' + data.list_title + '</a>"';
				s += '</div>';
		} else {
			s += '<div><span>' + data.message + '</span></div>';
		}
		s += '</div><!-- .notification_msg -->';
		s += '</div><!-- .notification_div1 -->';
	}
	if (s === '') {
		return '<div class="notification_merginediv1"><div class="no_notification_div"><a href="' + FW.baseurl + '/index.php/user/myNotifications" class="blue-btn">Show My Notifications</a></div></div>';
	} else {
		return '<a href="' + FW.baseurl + '/index.php/user/myNotifications" class="blue-btn" style="margin-bottom: 6px; text-align: center;">Show My Notifications</a><div class="notification_merginediv1">' + s + '</div>';
	}
}


function readCookie(cookieName) {
	var theCookie = '' + document.cookie;
	var ind = theCookie.indexOf(cookieName);
	var ind1 = theCookie.indexOf(';', ind);
	if(ind1 == -1) ind1 = theCookie.length;
 
	if(ind == -1 || cookieName == '') return '';
 
	return unescape(theCookie.substring(ind + cookieName.length + 1, ind1));
}


function setCookie(cookieName, cookieValue, nDays) {
	var today = new Date();
	var expire = new Date();
 
	if(nDays == null || nDays == 0) nDays = 1;
 
	expire.setTime(today.getTime() + 3600000 * 24 * nDays);
 
	document.cookie = cookieName + '=' + escape(cookieValue) + ';expires=' + expire.toGMTString();
}


function goBack(val) {
	if (typeof val === 'undefined') {
		var referrer = readCookie('fw_back');
		if (referrer) window.location.href = referrer;
	} else if (!isNaN(val)) {
		history.go(val);
	}
	return false;
}


function getTimeElapsedString(target) {    
    x = new Date()
    currentTimeZoneOffsetInHours = -x.getTimezoneOffset()/60;
    
	function _getDaysInMonth(month ,year) {     
		if( typeof year === 'undefined') year = 1999;
		var currmon = new Date(year, month),     
			nextmon = new Date(year, month + 1);
		return Math.floor((nextmon.getTime() - currmon.getTime()) / (24*3600*1000));
	}
	
    var now = new Date(), yd, md, dd, hd, nd, sd, result = '';
    target = new Date(target - (currentTimeZoneOffsetInHours*3600*1000));
    if (target > now) target = now;
    
    yd = now.getFullYear() - target.getFullYear();
    md = now.getMonth() - target.getMonth();
    dd = now.getDate() - target.getDate();
    hd = now.getHours() - target.getHours();
    nd = now.getMinutes() - target.getMinutes();
    sd = now.getSeconds() - target.getSeconds(); 

    if( md < 0) {yd--; md += 12;}
    if( dd < 0) {
        //md--;
        dd += _getDaysInMonth(now.getMonth()-1, now.getFullYear());
    }
    if( hd < 0) {dd--; hd += 24;}
    if( nd < 0) {hd--; nd += 60;}
    if( sd < 0) {nd--; sd += 60;}

    if( yd > 0) { result = yd + ' year' + (yd == 1 ? '' : 's'); } else
    if( md > 0) { result = md + ' month' + (md == 1 ? '' : 's'); } else
    if( dd > 0) { result = dd + ' day' + (dd == 1 ? '' : 's'); } else
    if( hd > 0) { result = hd + ' hour' + (hd == 1 ? '' : 's'); } else
    if( nd > 0) { result = nd + ' minute' + (nd == 1 ? '' : 's'); } else
    if( sd > 0) { result = sd + ' second' + (sd == 1 ? '' : 's'); }
    return (result === '' ? 'just now!' : result + ' ago');
}
