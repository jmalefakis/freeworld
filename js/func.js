$(function(){
    $(".openGallery").fancybox({
        'href' :'#modal',
		afterShow: function() { $.galleriffic.captureInput = true;
            if ($('#trmb'+$(this.element).data('option')).length > 0) {
                var childPos = $('#trmb'+$(this.element).data('option')).offset();
                var parentPos = $('#trmb'+$(this.element).data('option')).parent().offset();
                if ($(this.element).data('last') == 1) {
                    $('.mCSB_container').css('top', (childPos.top - parentPos.top-120)*-1);
                    $('.mCSB_dragger').css('top', (((childPos.top - parentPos.top-120)*-1)/6.6)*-1);
                } else {
                    $('.mCSB_container').css('top', (childPos.top - parentPos.top-60)*-1);
                    $('.mCSB_dragger').css('top', (((childPos.top - parentPos.top-60)*-1)/6.6)*-1);
                }
                $('#trmb'+$(this.element).data('option')).find('li').first().addClass('selected');
            } else {
                $('.mCSB_container').css('top', 0);
                $('#trmb0').find('li').first().addClass('selected');
            }
        },
		beforeClose: function() { 
            $.galleriffic.captureInput = false;
            if (parseInt($('#pageRedirect').val())) {
                document.location.href = document.location.href.split("#")[0];
            }
        }
	});
    $(".openGalleryMin").fancybox({
        'href' :'#modal',
		afterShow: function() { $.galleriffic.captureInput = true; 
            if ($('#trmb'+$(this.element).data('option')).length > 0) {
                var childPos = $('#trmb'+$(this.element).data('option')).offset();
                var parentPos = $('#trmb'+$(this.element).data('option')).parent().offset();
                if ($(this.element).data('last') == 1) {
                    $('.mCSB_container').css('top', (childPos.top - parentPos.top-120)*-1);
                    $('.mCSB_dragger').css('top', (((childPos.top - parentPos.top-120)*-1)/6.6)*-1);
                } else {
                    $('.mCSB_container').css('top', (childPos.top - parentPos.top-60)*-1);
                    $('.mCSB_dragger').css('top', (((childPos.top - parentPos.top-60)*-1)/6.6)*-1);
                }
                $('#trmb'+$(this.element).data('option')).find('li').first().addClass('selected');
            } else {
                $('.mCSB_container').css('top', 0);
                $('#trmb0').find('li').first().addClass('selected');
            }
        },
		beforeClose: function() { $.galleriffic.captureInput = false; 
            if (parseInt($('#pageRedirect').val())) {
                document.location.href = document.location.href.split("#")[0];
            }
        }   
	});
    
	// We only want these styles applied when javascript is enabled
	//$('div.navigation').css({'width' : '300px', 'float' : 'left'});
	$('div.content').css('display', 'block');
    $('#caption').css('display', 'none');
    
    $.initGallery = function() {

        // Initially set opacity on thumbs and add
        // additional styling for hover effect on thumbs
        var onMouseOutOpacity = 0.67;
        $('#thumbs ul.thumbs li').opacityrollover({
            mouseOutOpacity:   onMouseOutOpacity,
            mouseOverOpacity:  1.0,
            fadeSpeed:         'fast',
            exemptionSelector: '.selected'
        });

        // Initialize Advanced Galleriffic Gallery
        var gallery = $('#thumbs').galleriffic({
            delay:                     2500,
            numThumbs:                 200,
            preloadAhead:              10,
            enableTopPager:            false,
            enableBottomPager:         false,
            maxPagesToShow:            100,
            imageContainerSel:         '#slideshow',
            controlsContainerSel:      '#controls',
            captionContainerSel:       '#caption',
            loadingContainerSel:       '#loading',
            renderSSControls:          true,
            renderNavControls:         true,
            playLinkText:              'Play Slideshow',
            pauseLinkText:             'Pause Slideshow',
            prevLinkText:              '&lsaquo; Previous Photo',
            nextLinkText:              'Next Photo &rsaquo;',
            nextPageLinkText:          'Next &rsaquo;',
            prevPageLinkText:          '&lsaquo; Prev',
            enableHistory:             false,
            autoStart:                 false,
            syncTransitions:           true,
            defaultTransitionDuration: 900,
            onSlideChange:             function(prevIndex, nextIndex) {
                // 'this' refers to the gallery, which is an extension of $('#thumbs')
                this.find('ul.thumbs').children()
                    .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                    .eq(nextIndex).fadeTo('fast', 1.0);
            },
            onPageTransitionOut:       function(callback) {
                this.fadeTo('fast', 0.0, callback);
            },
            onPageTransitionIn:        function() {
                this.fadeTo('fast', 1.0);
            }
        });
        
        $('.customModal .navigation').mCustomScrollbar({
            advanced:{
                updateOnContentResize: true
            }
        });
        
        $(".openGallery").click(function(){
            var $that = $(this),
                img = $that.attr('data-image'),
                idx = parseInt($that.attr('data-index')),
                n = gallery.data.length;
                
            for(var i=0; i<n; i++) {
                if (gallery.data[i].slideUrl.indexOf(img) !== -1) {
                    idx = i;
                    break;
                }
            }
            gallery.gotoIndex(idx);
        });
        
        $("#gallery").mouseover(function(){
            $('#caption').show('fast');
        });
        
        $("#thumbs").mouseout(function(){
            $('#caption').hide('fast');
        });
        
        $(".openGalleryMin").click(function(){
            var needImg = $(this).attr('data-image');
            var isset = 0;
            $(".thumb").each(function() {
                if ($(this).find("img").attr('src').indexOf(needImg) != -1) {
                    gallery.gotoIndex(parseInt($(this).attr('href').replace('#',''))-1);
                    
                    //var rows = jQuery.parseJSON($('#rowsLi').val());
                    //var k = 128*rows[$(this).attr('data-scroll-index')];
                    //$('.mCSB_container').css('top',$(this).attr('data-scroll-index')*k*-1);
                    
                    isset = 1;
                    return false;
                }
            });
            if (isset == 0) {
                gallery.gotoIndex(0);
            }
        });
        
        $(".removeGalleryImg").click(function(event){
            event.stopPropagation();
            var that = this;
            $.ajax({
                url: $('#hidden_baseurl').attr('value')+'/index.php/preferences/removeGalleryImg',
                type: 'POST',
                data: {
                    image: $(that).attr('data-image'),
                    list_id: $(that).attr('data-list')      
                },
                success: function(result) {
                    gallery.removeImageByHash($(that).parent().attr('href'));
                    $('#pageRedirect').attr('value',1);
                }
            })
            
        });
    }
    $.initGallery();
    
    
});

function closeFancy() {
    parent.$.fancybox.close();
}
